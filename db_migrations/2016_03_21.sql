ALTER TABLE grab_resources
  ADD COLUMN current_round INT(11) NOT NULL DEFAULT 1 COMMENT 'Текущий раунд с которого парсятся матчи' AFTER is_active;