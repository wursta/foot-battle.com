--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 7.0.54.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 21.07.2016 9:06:27
-- Версия сервера: 5.5.44
-- Версия клиента: 4.1
-- Пожалуйста, сохраните резервную копию вашей базы перед запуском этого скрипта
--


SET NAMES 'utf8';

USE betbattle;


--
-- Изменить таблицу "billing_operations"
--
ALTER TABLE billing_operations
  ADD COLUMN balance_info TEXT DEFAULT NULL COMMENT 'Информация о балансах пользователя (user_id) перед проведением операции' AFTER user_agent;