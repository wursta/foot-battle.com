--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 7.0.54.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 23.07.2016 12:03:19
-- Версия сервера: 5.5.48
-- Версия клиента: 4.1
-- Пожалуйста, сохраните резервную копию вашей базы перед запуском этого скрипта
--


SET NAMES 'utf8';

USE betbattle;


--
-- Изменить таблицу "billing_operations"
--
ALTER TABLE billing_operations
  ADD COLUMN operation_ok_datetime DATETIME DEFAULT NULL COMMENT 'Дата/Время успешного выполнения операции' AFTER balance_info,
  ADD COLUMN operation_cancel_datetime DATETIME DEFAULT NULL COMMENT 'Дата/Время отмены операции' AFTER operation_ok_datetime;