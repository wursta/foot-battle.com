--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 7.0.54.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 25.07.2016 10:17:52
-- Версия сервера: 5.5.48
-- Версия клиента: 4.1
-- Пожалуйста, сохраните резервную копию вашей базы перед запуском этого скрипта
--


SET NAMES 'utf8';

USE betbattle;


--
-- Создать таблицу "points_schemas_correct_scrores"
--
CREATE TABLE points_schemas (
  id int(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор',
  created datetime NOT NULL COMMENT 'Дата/Время создания',
  base float NOT NULL COMMENT 'База',
  add_diff_0 float NOT NULL COMMENT 'Доп. очки при угаданой разнице и ничьи',
  add_diff_1 float NOT NULL COMMENT 'Доп. очки при угаданой разнице в 1 мяч',
  add_diff_2 float NOT NULL COMMENT 'Доп. очки при угаданой разнице в 2 мяча',
  add_diff_3 float NOT NULL COMMENT 'Доп. очки при угаданой разнице в 3 мяча',
  add_diff_4 float NOT NULL COMMENT 'Доп. очки при угаданой разнице в 4 мяча',
  another_correct_score_points float NOT NULL COMMENT 'Очки за любой счёт не входящий в correct_scores',
  is_default tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Является схемой по-умолчанию',
  PRIMARY KEY (id),
  INDEX IDX_points_schemas_created (created),
  INDEX IDX_points_schemas_is_default (is_default)
)
ENGINE = INNODB
COMMENT = 'Схемы начисления очков';

CREATE TABLE points_schemas_correct_scrores (
  id int(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор',
  points_scheme_id int(11) NOT NULL COMMENT 'ID схемы начисления очков',
  score varchar(255) NOT NULL COMMENT 'Счёт',
  points float NOT NULL COMMENT 'Очки',
  PRIMARY KEY (id),
  CONSTRAINT FK_points_schemas_correct_scrores_points_schemas_id FOREIGN KEY (points_scheme_id)
  REFERENCES points_schemas (id) ON DELETE CASCADE ON UPDATE RESTRICT
)
ENGINE = INNODB
COMMENT = 'Схемы начисления очков: Точные счета';