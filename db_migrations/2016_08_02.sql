CREATE TABLE policies (
  id int(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор',
  locale char(2) NOT NULL DEFAULT 'ru' COMMENT 'Локаль',
  number varchar(255) NOT NULL COMMENT 'Номер редакции',
  edition_date date NOT NULL COMMENT 'Дата редакции',
  points_scheme_id int(11) NOT NULL COMMENT 'Ссылка на схему начисления очков (points_schemas)',
  terms text NOT NULL COMMENT 'Пользовательское соглашение',
  regulations text NOT NULL COMMENT 'Правила игры',
  privacy text NOT NULL COMMENT 'Политика конфиденциальности',
  is_default tinyint(1) NOT NULL DEFAULT 0 COMMENT 'По-умолчанию',
  PRIMARY KEY (id),
  INDEX IDX_policies_locale (locale),
  INDEX IDX_policies_is_default (is_default),
  CONSTRAINT FK_policies_points_schemas_id FOREIGN KEY (points_scheme_id)
  REFERENCES points_schemas (id) ON DELETE CASCADE ON UPDATE RESTRICT
)
ENGINE = INNODB
COMMENT = 'Редакции пользовательского соглашения';