--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 7.0.54.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 08.08.2016 21:38:50
-- Версия сервера: 5.5.48
-- Версия клиента: 4.1
-- Пожалуйста, сохраните резервную копию вашей базы перед запуском этого скрипта
--


SET NAMES 'utf8';

USE betbattle;


--
-- Создать таблицу "regulations"
--
CREATE TABLE regulations (
  id INT NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор',
  locale char(2) NOT NULL DEFAULT 'ru' COMMENT 'Локаль',
  tournament_format_id INT(11) NOT NULL COMMENT 'Формат турнира',
  edition_date DATE NOT NULL COMMENT 'Дата редакции',
  is_default BOOL NOT NULL DEFAULT 0 COMMENT 'По-умолчанию',
  text TEXT NOT NULL COMMENT 'Текст регламента',
  PRIMARY KEY (id),
  INDEX IDX_regulations_edition_date (edition_date),
  INDEX IDX_regulations_is_default (is_default),
  INDEX IDX_policies_locale (locale),
  CONSTRAINT FK_regulations_tournament_formats_id FOREIGN KEY (tournament_format_id)
    REFERENCES tournament_formats(id) ON DELETE CASCADE ON UPDATE RESTRICT
)
ENGINE = INNODB
COMMENT = 'Редакции регламентов турниров';