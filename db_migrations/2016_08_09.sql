--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 7.0.54.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 09.08.2016 21:38:21
-- Версия сервера: 5.5.48
-- Версия клиента: 4.1
-- Пожалуйста, сохраните резервную копию вашей базы перед запуском этого скрипта
--


SET NAMES 'utf8';

USE betbattle;


--
-- Изменить таблицу "policies"
--
ALTER TABLE policies
  ADD COLUMN regulation_championship_id INT DEFAULT NULL COMMENT 'Идентификатор регламента турнира типа "Чемпионат"' AFTER regulations,
  ADD COLUMN regulation_round_id INT DEFAULT NULL COMMENT 'Идентификатор регламента турнира типа "Круговой"' AFTER regulation_championship_id,
  ADD COLUMN regulation_playoff_id INT DEFAULT NULL COMMENT 'Идентификатор регламента турнира типа "Плей-офф"' AFTER regulation_round_id,
  ADD COLUMN regulation_championsleague_id INT DEFAULT NULL COMMENT 'Идентификатор регламента турнира типа "Лига чемпионов"' AFTER regulation_playoff_id;

--
-- Изменить таблицу "policies"
--
ALTER TABLE policies
  ADD CONSTRAINT FK_policies_regulations_championship_id FOREIGN KEY (regulation_championship_id)
    REFERENCES regulations(id) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE policies
  ADD CONSTRAINT FK_policies_regulations_round_id FOREIGN KEY (regulation_round_id)
    REFERENCES regulations(id) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE policies
  ADD CONSTRAINT FK_policies_regulations_playoff_id FOREIGN KEY (regulation_playoff_id)
    REFERENCES regulations(id) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE policies
  ADD CONSTRAINT FK_policies_regulations_championsleague_id FOREIGN KEY (regulation_championsleague_id)
    REFERENCES regulations(id) ON DELETE RESTRICT ON UPDATE RESTRICT;