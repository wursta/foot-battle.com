--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 7.0.54.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 11.08.2016 21:55:38
-- Версия сервера: 5.5.48
-- Версия клиента: 4.1
-- Пожалуйста, сохраните резервную копию вашей базы перед запуском этого скрипта
--


SET NAMES 'utf8';

USE betbattle;


--
-- Изменить таблицу "tournaments"
--
ALTER TABLE tournaments
  ADD COLUMN regulation_id INT NOT NULL COMMENT 'ID регламента турнира' AFTER with_confirmation;
  
UPDATE tournaments SET regulation_id = format_id;

--
-- Изменить таблицу "tournaments"
--
ALTER TABLE tournaments
  ADD CONSTRAINT FK_tournaments_regulations_id FOREIGN KEY (regulation_id)
    REFERENCES regulations(id) ON DELETE CASCADE ON UPDATE RESTRICT;
    
--
-- Изменить таблицу "tournaments"
--
ALTER TABLE tournaments
  ADD COLUMN points_scheme_id INT NOT NULL COMMENT 'ID схемы начисления очков' AFTER regulation_id;
  
UPDATE tournaments SET points_scheme_id = 1;

--
-- Изменить таблицу "tournaments"
--
ALTER TABLE tournaments
  ADD CONSTRAINT FK_tournaments_points_schemas_id FOREIGN KEY (points_scheme_id)
    REFERENCES points_schemas(id) ON DELETE CASCADE ON UPDATE RESTRICT;