--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 7.1.13.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 04.09.2016 21:10:24
-- Версия сервера: 5.5.48
-- Версия клиента: 4.1
-- Пожалуйста, сохраните резервную копию вашей базы перед запуском этого скрипта
--


SET NAMES 'utf8';

USE betbattle;


--
-- Изменить таблицу "tournaments"
--
ALTER TABLE tournaments
  ADD COLUMN max_users_count INT(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Максимальное количество участников в турнире' AFTER commission,
  ADD COLUMN current_round INT(11) UNSIGNED NOT NULL DEFAULT 1 COMMENT 'Текущий раунд' AFTER max_users_count;
  
DELIMITER $$

--
-- Создать процедуру "updateTournamentMaxUsersCount"
--
CREATE PROCEDURE updateTournamentMaxUsersCount(IN TournamentId INT, IN MaxUsersCount INT)
  DETERMINISTIC
  SQL SECURITY INVOKER
  MODIFIES SQL DATA
BEGIN
  UPDATE tournaments SET max_users_count = MaxUsersCount WHERE id = TournamentId;
END
$$

DELIMITER ;

DELIMITER $$

--
-- Создать процедуру "updateTournamentCurrentRound"
--
CREATE PROCEDURE updateTournamentCurrentRound(IN TournamentId INT, IN CurrentRound INT)
  DETERMINISTIC
  SQL SECURITY INVOKER
  MODIFIES SQL DATA
BEGIN
  UPDATE tournaments SET current_round = CurrentRound WHERE id = TournamentId;
END
$$

DELIMITER ;

DELIMITER $$

--
-- Создать триггер "tournaments_championship_AfterInsert"
--
CREATE TRIGGER tournaments_championship_AfterInsert
    AFTER INSERT
    ON tournaments_championship
    FOR EACH ROW
BEGIN
  call updateTournamentMaxUsersCount(NEW.tournament_id, NEW.max_users_count);
  call updateTournamentCurrentRound(NEW.tournament_id, NEW.current_round);
END
$$

DELIMITER ;

DELIMITER $$

--
-- Создать триггер "tournaments_championship_AfterUpdate"
--
CREATE TRIGGER tournaments_championship_AfterUpdate
    AFTER UPDATE
    ON tournaments_championship
    FOR EACH ROW
BEGIN
  call updateTournamentMaxUsersCount(NEW.tournament_id, NEW.max_users_count);
  call updateTournamentCurrentRound(NEW.tournament_id, NEW.current_round);
END
$$

DELIMITER ;

DELIMITER $$

--
-- Создать триггер "tournaments_round_AfterInsert"
--
CREATE TRIGGER tournaments_round_AfterInsert
    AFTER INSERT
    ON tournaments_round
    FOR EACH ROW
BEGIN
  call updateTournamentMaxUsersCount(NEW.tournament_id, NEW.max_users_count);
  call updateTournamentCurrentRound(NEW.tournament_id, NEW.current_round);
END
$$

DELIMITER ;

DELIMITER $$

--
-- Создать триггер "tournaments_round_AfterUpdate"
--
CREATE TRIGGER tournaments_round_AfterUpdate
    AFTER UPDATE
    ON tournaments_round
    FOR EACH ROW
BEGIN
  call updateTournamentMaxUsersCount(NEW.tournament_id, NEW.max_users_count);
  call updateTournamentCurrentRound(NEW.tournament_id, NEW.current_round);
END
$$

DELIMITER ;

DELIMITER $$

--
-- Создать триггер "tournaments_playoff_AfterInsert"
--
CREATE TRIGGER tournaments_playoff_AfterInsert
    AFTER INSERT
    ON tournaments_playoff
    FOR EACH ROW
BEGIN
  call updateTournamentMaxUsersCount(NEW.tournament_id, NEW.max_users_count);
  call updateTournamentCurrentRound(NEW.tournament_id, NEW.current_round);
END
$$

DELIMITER ;

DELIMITER $$

--
-- Создать триггер "tournaments_playoff_AfterUpdate"
--
CREATE TRIGGER tournaments_playoff_AfterUpdate
    AFTER UPDATE
    ON tournaments_playoff
    FOR EACH ROW
BEGIN
  call updateTournamentMaxUsersCount(NEW.tournament_id, NEW.max_users_count);
  call updateTournamentCurrentRound(NEW.tournament_id, NEW.current_round);
END
$$


DELIMITER ;

DELIMITER $$

--
-- Создать триггер "tournaments_championsleague_AfterInsert"
--
CREATE TRIGGER tournaments_championsleague_AfterInsert
    AFTER INSERT
    ON tournaments_championsleague
    FOR EACH ROW
BEGIN
  call updateTournamentMaxUsersCount(NEW.tournament_id, NEW.max_users_count);
  call updateTournamentCurrentRound(NEW.tournament_id, NEW.current_round);
END
$$

DELIMITER ;

DELIMITER $$

--
-- Создать триггер "tournaments_championsleague_AfterUpdate"
--
CREATE TRIGGER tournaments_championsleague_AfterUpdate
    AFTER UPDATE
    ON tournaments_championsleague
    FOR EACH ROW
BEGIN
  call updateTournamentMaxUsersCount(NEW.tournament_id, NEW.max_users_count);
  call updateTournamentCurrentRound(NEW.tournament_id, NEW.current_round);
END
$$

DELIMITER ;

UPDATE tournaments_championship tc SET tc.max_users_count = tc.max_users_count;
UPDATE tournaments_round tc SET tc.max_users_count = tc.max_users_count;
UPDATE tournaments_playoff tc SET tc.max_users_count = tc.max_users_count;
UPDATE tournaments_championsleague tc SET tc.max_users_count = tc.max_users_count;