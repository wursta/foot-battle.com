<?php
namespace AppBundle\Admin\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

class Balance
{
    private $amount;
    private $note;
    
    public function getAmount()
    {
        return round(abs($this->amount), 2);
    }
    
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }
    
    public function getNote()
    {
        return $this->note;
    }
    
    public function setNote($note)
    {
        $this->note = $note;
    }
}