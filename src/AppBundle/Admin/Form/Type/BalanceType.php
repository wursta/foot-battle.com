<?php
namespace AppBundle\Admin\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BalanceType extends AbstractType
{        
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('amount', 'number');
        
        $builder->add('note', 'textarea');

        $builder->add('deposit', 'submit');
        $builder->add('withdraw', 'submit');
        $builder->add('cashout', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(            
            // a unique key to help generate the secret token
            'intention'       => 'balance',
        ));
    }

    public function getName()
    {
        return 'balance';
    }
}