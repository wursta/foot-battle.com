<?php
namespace AppBundle\Admin\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GrabResourceType extends AbstractType
{    
    private $newResource;
    
    public function __construct($newResource = false)
    {
        $this->newResource = $newResource;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {        
        $builder->add('is_active', 'checkbox', array('required' => false));                
        
        $builder->add('league', 'entity', array(
            'class' => 'AppBundle\Entity\League',
            'property' => 'league_name'            
        ));
        
        $builder->add('internal_name', 'text');
        $builder->add('title', 'text');
        $builder->add('teams_url', 'url', array('required' => false));
        $builder->add('calendar_url', 'url', array('required' => false));
        $builder->add('results_url', 'url', array('required' => false));        
        
        if(!$this->newResource)
        {
            $builder->add('resource_teams', 'collection', array(
                    'type'   => new ResourceTeamType(),
                    'cascade_validation' => true,
                    'required' => false
                    ));
        }
        
        $builder->add('save', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\GrabResource'
        ));
    }
    
    public function getName()
    {
        return 'grab_resource';
    }
}