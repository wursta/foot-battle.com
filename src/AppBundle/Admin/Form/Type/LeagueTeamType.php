<?php
namespace AppBundle\Admin\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LeagueTeamType extends AbstractType
{
    private $leagueCountry;
    
    public function __construct($country)
    {
        $this->leagueCountry = $country;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $country = $this->leagueCountry;
        
        $builder->add('teams', 'entity' , array(
                'class' => 'AppBundle:Team',                
                'query_builder' => function(EntityRepository $er) use ( $country ) {
                    $query = $er->createQueryBuilder('t');                    
                    if(!empty($country))
                        $query = $query->where('t.country = :country')->setParameter('country', $country);
                    
                    $query = $query->orderBy('t.title', 'ASC');
                    
                    return $query;
                },
                'multiple' => true,
                'expanded' => true
                ));        

        $builder->add('save', 'submit');
    }    

    public function getName()
    {
        return 'league_team';
    }
}