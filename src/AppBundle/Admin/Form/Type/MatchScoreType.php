<?php
namespace AppBundle\Admin\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MatchScoreType extends AbstractType
{        
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('score_left', 'integer', array("attr" => array("min" => 0)));
        $builder->add('score_right', 'integer', array("attr" => array("min" => 0)));
        $builder->add('save', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Match'
        ));
    }

    public function getName()
    {
        return 'match_score';
    }    
}