<?php
namespace AppBundle\Admin\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NewsType extends AbstractType
{        
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', 'text', array(
                'trim' => true,
                'attr' => array(
                    'min' => 2,
                    'max' => 100
                )
            )
        );
        $builder->add('summary', 'textarea', array(
                'trim' => true,
                'attr' => array(
                    'min' => 2,
                    'max' => 250
                )
            )
        );
        $builder->add('text', 'textarea', array('required' => false));        
        $builder->add('is_active', 'checkbox', array('required' => false));
        $builder->add('save', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\News'
        ));
    }

    public function getName()
    {
        return 'news';
    }    
}