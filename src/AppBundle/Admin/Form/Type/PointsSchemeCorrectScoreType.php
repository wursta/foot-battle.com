<?php
namespace AppBundle\Admin\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PointsSchemeCorrectScoreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('score', 'text', array(
            'label' => 'Счёт'
        ));
        $builder->add('points', 'number', array(
            'label' => 'Очки'
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\PointsSchemeCorrectScore',
            // a unique key to help generate the secret token
            'intention'       => 'points_scheme_correct_score',
        ));
    }

    public function getName()
    {
        return 'points_scheme_correct_score';
    }
}