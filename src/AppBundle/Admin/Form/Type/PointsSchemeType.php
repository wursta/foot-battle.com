<?php
namespace AppBundle\Admin\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use AppBundle\Admin\Form\Type\PointsSchemeCorrectScoreType;

class PointsSchemeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('base', 'number');
        $builder->add('addDiff0', 'number');
        $builder->add('addDiff1', 'number');
        $builder->add('addDiff2', 'number');
        $builder->add('addDiff3', 'number');
        $builder->add('addDiff4', 'number');

        $builder->add("correctScores", "collection", array(
                    "type" => new PointsSchemeCorrectScoreType($options["data"]),
                    "cascade_validation" => true,
                    'by_reference' => false,
                    "allow_add" => true,
                    "allow_delete" => false
                )
        );

        $builder->add('anotherCorrectScorePoints', 'number');

        $builder->add('submit', 'submit');

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\PointsScheme',
            // a unique key to help generate the secret token
            'intention'       => 'points_scheme',
        ));
    }

    public function getName()
    {
        return 'points_scheme';
    }
}