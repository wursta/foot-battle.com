<?php
namespace AppBundle\Admin\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class PolicyType extends AbstractType
{
    protected $localeHelper;

    public function __construct(\AppBundle\Services\LocaleHelper $localeHelper)
    {
        $this->localeHelper = $localeHelper;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('locale', 'locale', array(
            'choices' => $this->localeHelper->getLocales()
        ));

        $builder->add('number');
        $builder->add('editionDate', 'date');

        $builder->add('pointsScheme', 'entity', array(
            'class' => 'AppBundle\Entity\PointsScheme',
            'property' => 'title'
        ));
        
        $builder->add('regulationChampionship', 'entity', array(
            'class' => 'AppBundle\Entity\Regulation',
            'query_builder' => function(EntityRepository $er) {                
                return $er->createQueryBuilder('r')
                    ->leftJoin('r.tournamentFormat', 'f')
                    ->where('f.internal_name = :formatInternalName')
                    ->setParameter('formatInternalName', 'championship');
            },
            'property' => 'title'
        ));
        
        $builder->add('regulationRound', 'entity', array(
            'class' => 'AppBundle\Entity\Regulation',
            'query_builder' => function(EntityRepository $er) {                
                return $er->createQueryBuilder('r')
                    ->leftJoin('r.tournamentFormat', 'f')
                    ->where('f.internal_name = :formatInternalName')
                    ->setParameter('formatInternalName', 'round');
            },
            'property' => 'title'
        ));
        
        $builder->add('regulationPlayoff', 'entity', array(
            'class' => 'AppBundle\Entity\Regulation',
            'query_builder' => function(EntityRepository $er) {                
                return $er->createQueryBuilder('r')
                    ->leftJoin('r.tournamentFormat', 'f')
                    ->where('f.internal_name = :formatInternalName')
                    ->setParameter('formatInternalName', 'playoff');
            },
            'property' => 'title'
        ));
        
        $builder->add('regulationChampionsleague', 'entity', array(
            'class' => 'AppBundle\Entity\Regulation',
            'query_builder' => function(EntityRepository $er) {                
                return $er->createQueryBuilder('r')
                    ->leftJoin('r.tournamentFormat', 'f')
                    ->where('f.internal_name = :formatInternalName')
                    ->setParameter('formatInternalName', 'championsleague');
            },
            'property' => 'title'
        ));

        $builder->add('terms', 'textarea');
        $builder->add('regulations', 'textarea');
        $builder->add('privacy', 'textarea');

        $builder->add('submit', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Policy',
            // a unique key to help generate the secret token
            'intention'       => 'policy',
        ));
    }

    public function getName()
    {
        return 'policy';
    }
}