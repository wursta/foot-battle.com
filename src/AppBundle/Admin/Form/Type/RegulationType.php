<?php
namespace AppBundle\Admin\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RegulationType extends AbstractType
{
    protected $localeHelper;

    public function __construct(\AppBundle\Services\LocaleHelper $localeHelper)
    {
        $this->localeHelper = $localeHelper;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('locale', 'locale', array(
            'choices' => $this->localeHelper->getLocales()
        ));
        
        $builder->add('tournamentFormat', 'entity', array(
            'class' => 'AppBundle\Entity\TournamentFormat',
            'property' => 'title'
        ));
        
        $builder->add('editionDate', 'date');
        
        $builder->add('text', 'textarea');        
        $builder->add('submit', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Regulation',
            // a unique key to help generate the secret token
            'intention'       => 'regulation',
        ));
    }

    public function getName()
    {
        return 'regulation';
    }
}