<?php
    namespace AppBundle\Admin\Form\Type;

    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\OptionsResolver\OptionsResolverInterface;
    
    use Doctrine\ORM\EntityRepository;
    
    class ResourceTeamType extends AbstractType
    {    
        private $resource;
        
        public function __construct(\AppBundle\Entity\GrabResource $resource)
        {
            $this->resource = $resource;
        }
        
        public function buildForm(FormBuilderInterface $builder, array $options)
        {
            $country = $this->resource->getLeague()->getCountry();
            $builder->add('team', 'entity', array(
                'class' => 'AppBundle:Team',
                'query_builder' => function (EntityRepository $er) use ($country) {
                    $query = $er->createQueryBuilder('t');
                    
                    if(!empty($country))
                        $query = $query->where('t.country = :country')->setParameter('country', $country);
                    
                    $query = $query->orderBy('t.title', 'ASC');
                    
                    return $query;
                }
            ));
            $builder->add('title', 'text');
            $builder->add('save', 'submit');
        }

        public function setDefaultOptions(OptionsResolverInterface $resolver)
        {
            $resolver->setDefaults(array(
                'data_class' => 'AppBundle\Entity\ResourceTeam'
            ));
        }

        public function getName()
        {
            return 'grab_resource';
        }
}