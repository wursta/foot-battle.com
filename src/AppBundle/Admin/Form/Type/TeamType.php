<?php
namespace AppBundle\Admin\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TeamType extends AbstractType
{        
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('country', 'country');
        $builder->add('city', 'text');
        $builder->add('title', 'text');
        $builder->add('is_active', 'checkbox', array('required' => false));
        $builder->add('save', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Team'
        ));
    }

    public function getName()
    {
        return 'team';
    }    
}