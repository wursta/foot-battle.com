<?php
namespace AppBundle\Admin\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    /**
    * Сервис для работы с локалями.
    *
    * @var \AppBundle\Services\LocaleHelper
    */
    protected $localeHelperService;

    /**
    * Сервис для работы с ролями пользователей.
    *
    * @var \AppBundle\Services\RoleHelper
    */
    protected $rolesHelperService;

    /**
    * Сценарий
    *
    * @var string
    */
    protected $scenario;

    const SCENARIO_ADD = 'ADD';
    const SCENARIO_UPDATE = 'UPDATE';

    public function __construct($localeHelperService, $rolesHelperService, $scenario = "ADD")
    {
        $this->localeHelperService = $localeHelperService;
        $this->rolesHelperService = $rolesHelperService;
        $this->scenario = $scenario;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('avatar_file', 'file', array("required" => false));
        $builder->add('avatar_delete', 'checkbox', array("required" => false, "mapped" => false));
        $builder->add('email', 'email');
        $builder->add('nick', 'text');

        if($this->scenario == self::SCENARIO_ADD)
        {
            $builder->add('password', 'repeated', array(
               'first_name'  => 'password',
               'second_name' => 'confirm',
               'type'        => 'password'
            ));
        }

        $builder->add('role', 'choice', array(
                'choices' => $this->rolesHelperService->getRoles()
            ));

        $builder->add('locale', 'locale', array(
                'choices' => $this->localeHelperService->getLocales()
            ));

        $builder->add('first_name', 'text', array("required" => false));
        $builder->add('last_name', 'text', array("required" => false));
        $builder->add('second_name', 'text', array("required" => false));
        $builder->add('birthdate', 'birthday', array("required" => false));
        $builder->add('country', 'country', array("required" => false));
        $builder->add('city', 'text', array("required" => false));

        $builder->add('is_active', 'checkbox', array("required" => false));
        $builder->add('is_confirmed', 'checkbox', array("required" => false));
        $builder->add('is_agreed', 'checkbox', array("required" => false));

        $builder->add('save', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User',
            // a unique key to help generate the secret token
            'intention'       => 'user',
        ));
    }

    public function getName()
    {
        return 'user';
    }
}