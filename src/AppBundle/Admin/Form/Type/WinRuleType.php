<?php
namespace AppBundle\Admin\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class WinRuleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('place', 'integer', array(
            'label' => 'Место'
        ));
        $builder->add('percent', 'number', array(
            'label' => 'Процент'
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\WinRule',
            // a unique key to help generate the secret token
            'intention'       => 'win_rule',
        ));
    }

    public function getName()
    {
        return 'win_rule';
    }
}