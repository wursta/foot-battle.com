<?php
namespace AppBundle\Admin\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use AppBundle\Admin\Form\Type\WinRuleType;

class WinRulesGroupType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('format', 'entity', array(
            'class' => 'AppBundle\Entity\TournamentFormat',
            'property' => 'title'
        ));

        $builder->add('min_users', 'integer', array("attr" => array("min" => 1)));
        $builder->add('max_users', 'integer', array("attr" => array("min" => 2)));


        $builder->add("rules", "collection", array(
                    "type" => new WinRuleType(),
                    "cascade_validation" => true,
                    'by_reference' => false,
                    "allow_add" => true,
                    "allow_delete" => true,
                )
        );

        $builder->add('save', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\WinRulesGroup',
            // a unique key to help generate the secret token
            'intention'       => 'win_rules_group',
        ));
    }

    public function getName()
    {
        return 'win_rules_group';
    }
}