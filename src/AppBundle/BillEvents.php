<?php
namespace AppBundle;

final class BillEvents
{
  /**
   * Событие зачисления средств на счёт   
   * В обработчик события передаётся объект класса AppBunle\Event\BillEvent
   *
   * @var string
   */
  const DEPOSIT = 'bill.deposit';
}