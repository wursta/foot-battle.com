<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class BillingCancelOldDepositsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('billing:cancelOldDeposits')
            ->setDescription('Отменяет старые операции пополнения счёта, которые не были завершены.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {    
        /** @var \AppBundle\Services\MailerHelper */
        $mailerHelper = $this->getContainer()->get("mailer_helper");

        try
        {
            /** @var $em \Doctrine\ORM\EntityManager */
            $em = $this->getContainer()->get('doctrine')->getManager();
            /** @var \AppBundle\Services\BalanceHelper */
            $balanceHelper = $this->getContainer()->get('balance_helper');

            /** @var \AppBundle\Entity\BillingOperation */
            $billingOperationsRep = $em->getRepository('AppBundle\Entity\BillingOperation');

            $date = new \DateTime();
            $date->modify('-1 day');
            
            $billingOperations = $billingOperationsRep->getDepositsInProgressOlderThan($date);
            
            foreach ($billingOperations as $bill) {
                $balanceHelper->cancelBill($bill);
            }
        } catch(\Exception $e) {
            $mailerHelper->sendErrorMail($e->getMessage());
        }
    }
}