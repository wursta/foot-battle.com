<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class LogsRotateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('logs:rotate')
            ->setDescription('Ротация логов');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {    
        /**                
        * @var $em \Doctrine\ORM\EntityManager
        */
        $em = $this->getContainer()->get('doctrine')->getManager();
        
        /**                
        * @var $cronLogRep \AppBundle\Entity\Logs\CronLogRepository
        */
        $cronLogRep = $em->getRepository("AppBundle\Entity\Logs\CronLog");
        
        $fromDate = new \DateTime(date("Y-m-d", time() - 7*24*3600));
        
        $cronLogRep->removeLogsFromDate($fromDate);
    }
}