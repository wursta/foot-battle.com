<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use AppBundle\Entity\Match;

class MatchesRejectCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('matches:reject')
            ->setDescription('Отмена матчей');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /**
        * Отменяем матчи только с 12:00 по 23:00
        */        
        if(date("G") <= 12 || date("G") >= 22)
            return true;
        
        /**        
        * @var $cronLogHelper \AppBundle\Services\Logs\CronLogHelper
        */
        $cronLogHelper = $this->getContainer()->get("cronlog_helper");
        $cronLogHelper->start();
        
        /**
        * @var \AppBundle\Services\MatchesHelper
        */
        $matchesHelper = $this->getContainer()->get("matches_helper");
        
        /**            
        * @var \AppBundle\Services\MailerHelper
        */
        $mailerHelper = $this->getContainer()->get("mailer_helper");
        
        try
        {                        
            $matchesHelper->notifyAboutReject($mailerHelper);
            $matchesHelper->rejectMatches();
        } catch(\Exception $ex) {
            $mailerHelper->sendErrorMail($ex->getMessage());
        }                
        
        $cronLogHelper->save($this->getName(), $this->getDescription());    
    }
}