<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use AppBundle\Entity\TournamentResult;

class MatchesSetPointsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('matches:set:points')
            ->setDescription('Начисление очков за ставки');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $em = $this->getContainer()->get('doctrine')->getManager();
        
        /** @var \AppBundle\Entity\BetRepository */
        $betRep = $em->getRepository('AppBundle\Entity\Bet');
        
        /** @var \AppBundle\Entity\Bet[] */
        $bets = $betRep->findByFinishedOrRejectedMatchesAndEmptyPoints();                
        
        /** @var \AppBundle\Services\PointsRuler */
        $pointsRuler = $this->getContainer()->get('points_ruler');
        
        /** @var \AppBundle\Services\MailerHelper */
        $mailerHelper = $this->getContainer()->get("mailer_helper");
            
        try {
            $em->getConnection()->beginTransaction();
            
            foreach($bets as $bet)
            {
                $points = $pointsRuler->calculatePoints($bet);
                
                $bet->setPoints($points);            

                $em->persist($bet);
                $em->flush();                        
                
                $user = $bet->getUser();                                
                
                $user->setTotalBets($user->getTotalBets() + 1);
                $user->setTotalPoints($user->getTotalPoints() + $bet->getPoints());
                
                if($user->getTotalBets() > 20)
                    $user->setRating($user->getTotalPoints()/$user->getTotalBets());

                $em->persist($user);                
            }
            
            $em->flush();
            $em->getConnection()->commit();
        } catch (Exception $e) {
            $em->getConnection()->rollback();
            $mailerHelper->sendErrorMail($e->getMessage(), $e->getFile(), $e->getLine(), $e->getTraceAsString());
        }
    }
}