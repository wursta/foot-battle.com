<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use AppBundle\Entity\Match;

class MatchesStartCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('matches:start')
            ->setDescription('Старт матчей');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {                        
        /**        
        * @var $cronLogHelper \AppBundle\Services\Logs\CronLogHelper
        */
        $cronLogHelper = $this->getContainer()->get("cronlog_helper");
        $cronLogHelper->start();
        
        $em = $this->getContainer()->get('doctrine')->getManager();
        
        //увеличиваем текущее время на 15 минут.
        $now = new \DateTime(date('Y-m-d H:i:s', time() + 15*60));        
                
        $matches = $em->getRepository('AppBundle\Entity\Match')->findThatNeedsToStart($now);
        
        foreach($matches as $match)
        {            
            $match->setIsStarted(true);

            $em->persist($match);            
        }        
        $em->flush();
        
        $cronLogHelper->save($this->getName(), $this->getDescription());
    }
}