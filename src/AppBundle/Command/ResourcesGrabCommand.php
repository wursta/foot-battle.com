<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ResourcesGrabCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('resources:grab')
            ->setDescription('Парсинг матчей')
            ->addArgument(
                'league_id',
                InputArgument::OPTIONAL,
                'ID лиги'
            );
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /**        
        * @var \AppBundle\Services\Logs\CronLogHelper
        */
        $cronLogHelper = $this->getContainer()->get("cronlog_helper");
        $cronLogHelper->start();
        
        /**        
        * @var \AppBundle\Services\ResourceGrabber
        */
        $resourceGrabber = $this->getContainer()->get('RecourceGrabber');
        
        /**        
        * @var \AppBundle\Services\MailerHelper
        */
        $mailerHelper = $this->getContainer()->get("mailer_helper");
        
        /**        
        * @var \AppBundle\Services\MatchesHelper
        */
        $matchesHelper = $this->getContainer()->get("matches_helper");
        
        $leagueId = $input->getArgument("league_id");
        
        try
        {
            $matches = $resourceGrabber->grab($leagueId);            
            $matchesHelper->fillMatches($matches);            
        } catch(\Exception $ex) {
            $mailerHelper->sendErrorMail($ex->getMessage());
        }
        
        $cronLogHelper->save($this->getName(), $this->getDescription());
    }
}