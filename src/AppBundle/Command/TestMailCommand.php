<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TestMailCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('test:mail')
            ->setDescription('Тестирование функции отправки писем');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {    
        /**                
        * @var \AppBundle\Services\MailerHelper
        */
        $mailerHelper = $this->getContainer()->get('mailer_helper');
        $mailerHelper->sendTestMail();
    }
}