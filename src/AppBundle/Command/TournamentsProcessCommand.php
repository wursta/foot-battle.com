<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\ArrayInput;

use AppBundle\Entity\TournamentResult;

class TournamentsProcessCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('tournaments:process')
            ->setDescription('Процессинг турниров');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /**        
        * @var $cronLogHelper \AppBundle\Services\Logs\CronLogHelper
        */
        $cronLogHelper = $this->getContainer()->get("cronlog_helper");
        $cronLogHelper->start();
        
        $rootDir = $this->getContainer()->getParameter('kernel.root_dir');
        $lockFile = $rootDir."/cron.lock";
        
        if(file_exists($lockFile))        
            exit;        
        
        if(file_put_contents($lockFile, date("m.d.Y H:i:s").": tournaments:process") === false)
            exit;
        
        try {        
            $input = new ArrayInput(array(''));
            
            $matchesSetPointsCmd = $this->getApplication()->find('matches:set:points');
            $tournamentsSetResultCmd = $this->getApplication()->find('tournaments:set:results');
            $tournamentsSetWinDistributionCmd = $this->getApplication()->find('tournaments:set:win_distribution');
            
            $matchesSetPointsCmd->run($input, $output);
            $tournamentsSetResultCmd->run($input, $output);
            $tournamentsSetWinDistributionCmd->run($input, $output);
        } catch(\Exception $e) {
            $mailerHelper->sendErrorMail($e->getMessage(), $e->getFile(), $e->getLine(), $e->getTraceAsString());
        }
        
        if(file_exists($lockFile))
            unlink($lockFile);
            
        $cronLogHelper->save($this->getName(), $this->getDescription());
    }
}