<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\Event;
use AppBundle\Services\TournamentFactory;
use AppBundle\TournamentEvents;
use AppBundle\Event\TournamentEvent;

class TournamentsSetResultsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('tournaments:set:results')
            ->setDescription('Процессинг турниров');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        /** @var \AppBundle\Services\MailerHelper */
        $mailerHelper = $this->getContainer()->get("mailer_helper");
        
        $em = $this->getContainer()->get('doctrine')->getManager();
        
        $tournamentRep = $em->getRepository('AppBundle\Entity\Tournament');
        $tournaments = $tournamentRep->findByIsStartedAndIsOver(true, false);
        
        try {
            foreach($tournaments as $tournament)
            {
                $format = $tournament->getFormat()->getInternalName();
                
                $tournamentService = $this->getContainer()->get('tournament.'.$format);            
                $tournamentService->processResults($tournament);
            }
        } catch(\Exception $e) {
            $mailerHelper->sendErrorMail($e->getMessage(), $e->getFile(), $e->getLine(), $e->getTraceAsString());
        }
    }
}