<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\Event;
use AppBundle\Services\TournamentFactory;
use AppBundle\TournamentEvents;
use AppBundle\Event\TournamentEvent;

class TournamentsSetStartCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('tournaments:set:start')
            ->setDescription('Старт турниров');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /**        
        * @var $cronLogHelper \AppBundle\Services\Logs\CronLogHelper
        */
        $cronLogHelper = $this->getContainer()->get("cronlog_helper");
        $cronLogHelper->start();
        
        $now = new \DateTime(date("Y-m-d H:i:s"));
        
        $pointsRuler = $this->getContainer()->get('points_ruler');
        $em = $this->getContainer()->get('doctrine')->getManager();
        
        /**        
        * @var \AppBundle\Entity\TournamentRepository
        */
        $tournamentRep = $em->getRepository('AppBundle\Entity\Tournament');
        
        /**
        * @var \AppBundle\Entity\Tournament[]
        */
        $tournaments = $tournamentRep->findNeedsToStart($now);

        $eventDispatcher = $this->getContainer()->get('event_dispatcher');

        foreach($tournaments as $initTournament)
        {        
            $format = $initTournament->getFormat()->getInternalName();
            
            $tournamentHelper = $this->getContainer()->get('tournament.'.$format);
            
            $tournament = $tournamentHelper->tournamentRep->findLazy($initTournament->getId());
            
            $event = new TournamentEvent($tournament);
            
            $initTournament->setIsAccessible(false);
            
            if(!$tournamentHelper->isTournamentCanStart($initTournament))
            {                
                $initTournament->setIsReject(true);

                $em->persist($initTournament);
                $em->flush($initTournament);
                
                $eventDispatcher->dispatch(TournamentEvents::TOURNAMENT_REJECTED, $event);
                $eventDispatcher->dispatch(TournamentEvents::getTournamentRejectedEvent($format), $event);                
                continue;
            }
                                   
            $initTournament->setIsStarted(true);
            $initTournament->setIsReject(false);

            $em->persist($initTournament);
            $em->flush($initTournament);                                    
                        
            $eventDispatcher->dispatch(TournamentEvents::TOURNAMENT_STARTED, $event);
            $eventDispatcher->dispatch(TournamentEvents::getTournamentStartedEvent($format), $event);
        }
        
        $cronLogHelper->save($this->getName(), $this->getDescription());
    }
}