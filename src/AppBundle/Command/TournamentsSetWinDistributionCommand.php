<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\Event;
use AppBundle\TournamentEvents;
use AppBundle\Event\TournamentEvent;
use AppBundle\Services\BalanceHelper;

class TournamentsSetWinDistributionCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('tournaments:set:win_distribution')
            ->setDescription('Зачисление выигрышей');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        $em = $this->getContainer()->get('doctrine')->getManager();        
        
        /**        
        * @var \AppBundle\Services\TournamentsHelper
        */
        $tournamentsHelper = $this->getContainer()->get('tournaments_helper');
        
        /**        
        * @var \AppBundle\Services\BalanceHelper
        */
        $BalanceHelper = $this->getContainer()->get('balance_helper');
        
        /** @var \AppBundle\Services\MailerHelper */
        $mailerHelper = $this->getContainer()->get("mailer_helper");
        
        /**        
        * @var \AppBundle\Entity\TournamentRepository
        */
        $tournamentRep = $em->getRepository('AppBundle\Entity\Tournament');
        
        /**        
        * @var \AppBundle\Entity\WinRuleRepository
        */
        $winRulesRep = $em->getRepository('AppBundle\Entity\WinRule');
        
        /**        
        * @var \AppBundle\Entity\Tournament[]
        */
        $tournaments = $tournamentRep->findReadyForWinDistribution();
        
        try {
            foreach($tournaments as $tournament)
            {
                $format = $tournament->getFormat()->getInternalName();
                $isPromo = $tournament->getIsPromo();
                
                $usersCount = count($tournament->getUsers());

                $winRules = $winRulesRep->findByFormatAndWinnersCount($format, $usersCount);
                
                if(empty($winRules))
                    continue;
                
                $tournamentBudget = $tournamentsHelper->getTournamentBudget($tournament);            
                $systemCommissionReal = $tournamentsHelper->getSystemCommision($tournamentBudget, $tournament->getCommission());
                $totalWinSum = $tournamentBudget - $systemCommissionReal;        
                
                $rules = array();
                foreach($winRules as $rule)
                    $rules[$rule->getPlace()] = $rule->getPercent();

                $fee = $tournament->getFee();            
                if($fee > 0)
                {
                    foreach($tournament->getUsers() as $user)                    
                    {                       
                        $note = $BalanceHelper->getFeeOffNote($tournament, $fee);
                        $bill = $BalanceHelper->createTournamentFeeOffBill($user, $fee, $note);                                        
                        $BalanceHelper->processBill($bill);
                    }
                }
                
                //Считаем реальные суммы
                foreach($tournament->getResults() as $result)
                {
                    $place = $result->getPlace();

                    if(!isset($rules[$place]))
                        $percent = 0;
                    else
                        $percent = $rules[$place];

                    $sum = round(($totalWinSum*$percent)/100, 2);
                    
                    $result->setPercent($percent);
                    $result->setSum($sum);

                    $em->persist($result);
                }

                $tournament->setIsWinPrizeDistributed(true);
                $em->persist($tournament);

                $em->flush();

                foreach($tournament->getResults() as $result)
                {
                    if($result->getSum() > 0)
                    {
                      if($isPromo)
                      {
                        $note = $BalanceHelper->getPromoWinNote($tournament, $result->getSum());
                        $bill = $BalanceHelper->createPromoTournamentWinBill($result->getUser(), $result->getSum(), $note);                    
                      }
                      else
                      {
                        $note = $BalanceHelper->getWinNote($tournament, $result->getSum());
                        $bill = $BalanceHelper->createTournamentWinBill($result->getUser(), $result->getSum(), $note);
                      }
                      $BalanceHelper->processBill($bill);
                    }
                }
            }
        } catch(\Exception $e) {
            $mailerHelper->sendErrorMail($e->getMessage());
        }
    }
}