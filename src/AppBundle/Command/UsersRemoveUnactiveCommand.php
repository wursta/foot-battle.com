<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UsersRemoveUnactiveCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('users:removeUnactive')
            ->setDescription('Удаляет неактивных пользователей не подтвердивших свою учётную запись по email.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var \AppBundle\Services\MailerHelper */
        $mailerHelper = $this->getContainer()->get("mailer_helper");

        try
        {
            /** @var $em \Doctrine\ORM\EntityManager */
            $em = $this->getContainer()->get('doctrine')->getManager();

            /** @var \AppBundle\Entity\UserRepository */
            $usersRep = $em->getRepository('AppBundle\Entity\User');

            $date = new \DateTime();
            $date->modify('-1 day');

            $users = $usersRep->findForRemoveByDate($date);

            foreach($users as $user) {
                $em->remove($user);
            }
            $em->flush();
        } catch(\Exception $e) {
            $mailerHelper->sendErrorMail($e->getMessage());
        }
    }
}