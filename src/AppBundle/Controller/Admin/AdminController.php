<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AdminController extends Controller
{    
    public function indexAction()
    {    
        /**        
        * @var \AppBundle\Entity\UserRepository
        */
        $userRep = $this->getDoctrine()->getRepository("AppBundle\Entity\User");
        
        $availableFunds = $userRep->getAvailableFunds();
        $onUsersAccountsFunds = $userRep->getOnUserAccountsFunds();
        $blockedFunds = $userRep->getBlockedFunds();
        
        $startDateTime = new \DateTime();
        $startDateTime->setTimestamp(time() - 7*24*3600);
        $startDateTime->setTime(0, 0, 0);
        
        $endDateTime = new \DateTime();        
        $lastWeekRegistredUsers = $userRep->getRegistredUsersCountByPeriod($startDateTime, $endDateTime);
        
        
        return $this->render('AppBundle:admin:index.html.twig', array(
            "on_users_account_funds" => $onUsersAccountsFunds,
            "blocked_funds" => $blockedFunds,
            "available_funds" => $availableFunds,
            "last_week_registred_users" => $lastWeekRegistredUsers
        ));
    }                
}