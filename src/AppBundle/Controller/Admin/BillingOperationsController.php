<?php
namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class BillingOperationsController extends Controller
{
    public function userLogAction($page, $user_id)
    {
        $_POST["user_id"] = $user_id;

        return $this->logAction($page);
    }

    public function logAction($page)
    {
        $request = $this->getRequest();

        $requestPayload = json_decode($request->getContent(), true);

        $rowsPerPage = 20;

        $arFilter = $requestPayload["filter"];
        $arOrder = $requestPayload["order"];

        /**
        * @var \AppBundle\Entity\BillingOperationRepository
        */
        $BillingOperationsRep = $this->getDoctrine()->getRepository("AppBundle\Entity\BillingOperation");

        $rowset = $BillingOperationsRep->findPaginatorByFilter($arOrder, $arFilter, $page, $rowsPerPage);

        $totalItems = count($rowset);

        $logItems = iterator_to_array($rowset);

        /** @var \AppBundle\Services\UserHelper */
        $userHelper = $this->get('user_helper');

        $encoder = new JsonEncoder();
        $normalizer = new GetSetMethodNormalizer();
        $normalizer->setIgnoredAttributes(array('hash', 'paysystemInternalId'));
        $normalizer->setCallbacks(array(
            'user' => function($user) use ($userHelper) {
                return $userHelper->getForNormalizer($user);
            },
            'operationDatetime' => function(\DateTime $datetime) {
                return $datetime->format("c");
            },
            'operationOkDatetime' => function(\DateTime $datetime = null) {
                if(!$datetime)
                    return null;

                return $datetime->format("c");
            },
            'operationCancelDatetime' => function(\DateTime $datetime = null) {
                if(!$datetime)
                    return null;

                return $datetime->format("c");
            },
            'userRecipient' => function($user) use ($userHelper) {
                return $userHelper->getForNormalizer($user);
            },
        ));
        $serializer = new Serializer(array($normalizer), array($encoder));

        $response = new JsonResponse(array(
            "items" => $serializer->normalize($logItems, 'json'),
            "total" => $totalItems
        ));

        return $response;
    }
}