<?php
namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class FBController extends Controller
{
    /**
    * Возвращает параметр переданный в json формате в теле запроса.
    * 
    * @param Request $request Объект запроса.
    * @param string  $key     Наименование параметра.
    * @param mixed   $default Значение возвращаемое "по-умолчанию", если параметр не был передан в запросе.
    * 
    * @return mixed
    */
    public function getParam(Request $request, $key, $default = null)
    {
        $requestPayload = json_decode($request->getContent(), true);

        if (isset($requestPayload[$key])) {
            return $requestPayload[$key];
        } else {
            return $default;
        }

    }
    
    /**
    * Возвращает стандартный ответ в json формате.
    * 
    * @param bool  $success Признак успешного ответа.
    * @param array $data    Дополнительные данные ответа.
    * 
    * @return JsonResponse
    */
    public function returnJsonResponse($success, array $data = array())
    {
        $response = array("success" => $success);
        
        if (!empty($data)) {
            $response = array_merge($response, $data);
        }
        
        return new JsonResponse($response);
    }
    
    /**
    * Возвращает стандартный ответ с ошибкой в json формате.
    *
    * @param string $errorText Текст ошибки.
    *
    * @return JsonResponse
    */
    public function returnErrorJsonResponse($errorText)
    {
        return $this->returnJsonResponse(false, array('error' => $errorText));
    }                                                  
}
