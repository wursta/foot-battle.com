<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Entity\GrabResource;
use AppBundle\Admin\Form\Type\GrabResourceType;
use AppBundle\Entity\ResourceTeam;

class GrabResourcesController extends Controller
{    
    public function indexAction()
    {
        $GrabResourceRepository = $this->getDoctrine()->getRepository('AppBundle\Entity\GrabResource');
        $resources_list = $GrabResourceRepository->findAll();
        
        return $this->render('admin/grab_resources/index.html.twig', array(
                'resources_list' => $resources_list
        ));
    }                

    public function addAction()
    {
        $GrabResource = new GrabResource();
        
        $form = $this->createForm(new GrabResourceType(true), $GrabResource);
        
        $request = $this->getRequest();
        
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($GrabResource);
            $em->flush();
            
            $leagueTeams = $GrabResource->getLeague()->getTeams();
            foreach($leagueTeams as $Team)
            {
                $ResourceTeam = new ResourceTeam();
                $ResourceTeam->setResource($GrabResource);
                $ResourceTeam->setTeam($Team);
                $ResourceTeam->setTitle($Team->getTitle());
                
                $em->persist($ResourceTeam);
                $em->flush();
                
                $GrabResource->addResourceTeam($ResourceTeam);
            }                
                            
            $em->persist($GrabResource);
            $em->flush();
            
            $request->getSession()->getFlashBag()->add('new_resource_added', 'new_resource_added');
            
            return $this->redirect($this->generateUrl('admin_grab_resource_edit', array('resource_id' => $GrabResource->getId())));
        }
        
        return $this->render('admin/grab_resources/add.html.twig', array(                
                'form' => $form->createView()
            ));
    }
    
    public function editAction($resource_id)
    {
        $GrabResource = $this->getDoctrine()->getRepository('AppBundle\Entity\GrabResource')->find($resource_id);        
        
        $form = $this->createForm(new GrabResourceType(), $GrabResource);

        $request = $this->getRequest();
        
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($GrabResource);
            $em->flush();
            
            return $this->redirect($this->generateUrl('admin_grab_resources'));
        }

        return $this->render('admin/grab_resources/edit.html.twig', array(                
                'form' => $form->createView()                
        ));
    }
    
    public function deleteAction($resource_id)
    {
        $GrabResource = $this->getDoctrine()->getRepository('AppBundle\Entity\GrabResource')->find($resource_id);        
        
        if(!$GrabResource)
            $success = false;
        else
        {
            $em = $this->getDoctrine()->getManager();
            $em->remove($GrabResource);
            $em->flush();
            $success = true;
        }                            
        
        $response = new JsonResponse();
        $response->setData(array(
            'success' => $success
        ));

        return $response;
    }
}