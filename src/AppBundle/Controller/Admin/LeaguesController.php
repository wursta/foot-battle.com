<?php       

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Entity;
use AppBundle\Entity\League;
use AppBundle\Admin\Form\Type\LeagueType;
use AppBundle\Admin\Form\Type\LeagueTeamType;

class LeaguesController extends Controller
{    
    
    public function indexAction()
    {
        /**        
        * @var AppBundle\Entity\LeagueRepository
        */
        $leagueRep = $this->getDoctrine()->getRepository('AppBundle\Entity\League');
        $leaguesList = $leagueRep->findAll(array("isActive" => "DESC", "country" => "ASC"));
        
        return $this->render('AppBundle:admin/leagues:index.html.twig', array(
                'leaguesList' => $leaguesList
        ));
    }
    
    public function editAction($league_id)
    {
        $league = $this->getDoctrine()->getRepository('AppBundle\Entity\League')->find($league_id);
        
        $form = $this->createForm(new LeagueType(), $league);
        
        $request = $this->getRequest();
        
        $form->handleRequest($request);        

        if ($form->isValid()) {                        
            $em = $this->getDoctrine()->getManager();                        

            $em->persist($league);
            $em->flush();
            
            return $this->redirect($this->generateUrl('admin/leagues/index'));
        }
        
        return $this->render('AppBundle:admin/leagues:edit.html.twig', array(
                'form' => $form->createView()
        ));
    }

    public function deleteAction($league_id)
    {
        $league = $this->getDoctrine()->getRepository('AppBundle\Entity\League')->find($league_id);
        
        if(!$league)
            $success = false;
        else
        {
            $em = $this->getDoctrine()->getManager();
            $em->remove($league);
            $em->flush();
            $success = true;
        }                            
        
        $response = new JsonResponse();
        $response->setData(array(
            'success' => $success
        ));

        return $response;
    }
    
    public function teamsAction($league_id)
    {
        $league = $this->getDoctrine()->getRepository('AppBundle\Entity\League')->find($league_id);
                 
        $form = $this->createForm(new LeagueTeamType($league->getCountry()), $league);
        
        $request = $this->getRequest();
        
        $form->handleRequest($request);
        
        if ($form->isValid()) {            
            $em = $this->getDoctrine()->getManager();
            $em->persist($league);
            $em->flush();
            
            return $this->redirect($this->generateUrl('admin/leagues/index'));
        }
        
        return $this->render('AppBundle:admin/leagues:teams.html.twig', array(
                'currentLeague' => $league,
                'form' => $form->createView()
        ));        
    }
    
    public function matchesAction($league_id)
    {
        $league = $this->getDoctrine()->getRepository('AppBundle\Entity\League')->find($league_id);
        
        if(!$league)
            throw $this->createNotFoundException("Лига не найдена");
        
        $matches = $this->getDoctrine()->getRepository('AppBundle\Entity\Match')->findByLeagueSortInverted($league_id);
        
        return $this->render('AppBundle:admin/leagues:matches.html.twig', array(
            'league' => $league,
            'matches' => $matches
        ));
    }
    
    public function editMatchAction($match_id)
    {        
        /**        
        * @var $match \AppBundle\Entity\Match
        */
        $match = $this->getDoctrine()->getRepository('AppBundle\Entity\Match')->findById($match_id);        
        
        if(!$match)
            throw $this->createNotFoundException("Матч не найден!");
        
        if($match->getIsOver() || $match->getIsRejected())
            throw $this->createNotFoundException("Матч завершён или отменён!");
        
        $request = $this->getRequest();
        
        if($formData = $request->get("Match"))
        {
            $match->setScoreLeft($formData["score_left"]);
            $match->setScoreRight($formData["score_right"]);
            $match->setIsOver(true);
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($match);
            $em->flush();
            
            $response = new JsonResponse();
            $response->setData(array(
                'success' => true
            ));
            
            return $response;        
        }

        
        return $this->render('AppBundle:admin/leagues:edit_match.html.twig', array(
            'match' => $match            
        ));                
    }
    
    /*public function editTeamsAction($league_id)
    {
        $League = $this->getDoctrine()->getRepository('AppBundle\Entity\League')->find($league_id);
                 
        $form = $this->createForm(new LeagueTeamType($League->getCountry()), $League);
        
        $request = $this->getRequest();
        
        $form->handleRequest($request);
        
        if ($form->isValid()) {            
            $em = $this->getDoctrine()->getManager();
            $em->persist($League);
            $em->flush();
            
            return $this->redirect($this->generateUrl('admin_leagues_teams', array('league_id' => $League->getId())));
        }
        
        return $this->render('admin/league/edit_teams.html.twig', array(
                'currentLeague' => $League,
                'form' => $form->createView()
        ));
    }
    */    
}