<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Session\Session;

use AppBundle\Form\Type\MatchSearchType;
use AppBundle\Admin\Form\Type\MatchScoreType;

class MatchesController extends Controller
{
    /**
    * Поиск матчей в контексте турнира
    * 
    * @return \Symfony\Component\HttpFoundation\Response  
    */
    public function searchAction()
    {
      $session = new Session();
      $request = $this->getRequest();
      
      $tournamentId = $request->get("tournament_id", null);
      $leagueId = $request->get("league_id", $session->get("matchesSearch[".$tournamentId."][league_id]", null));
      
      if(!$tournamentId)
        $this->createNotFoundException("Не передан необходимый параметр tournament_id");                  
                  
      $form = $this->createForm(new MatchSearchType($tournamentId));
      
      $form->handleRequest($request);
      
      $arFilter = array(
        "tournament_id" => $tournamentId
      );

      /**      
      * @var \AppBundle\Entity\LeagueRepository
      */
      $leaguesRep = $this->getDoctrine()->getRepository('AppBundle\Entity\League');
      
      //Если лига не указана, то нужно взять первую из доступных
      if(!$leagueId)
      {
        $leagues = $leaguesRep->findActiveByTournament($tournamentId);
        
        if(!empty($leagues[0]))
        {
          $league = $leagues[0];
          $leagueId = $league->getId();
        }
      }
      else
      {
        $league = $leaguesRep->find($leagueId);        
      }
      
      if(!$form->isSubmitted())
      {
        $form->get('league')->setData($league);
      }
      
      $arFilter["league_id"] = $league->getId();
      
      if($form->isValid())
      {
        $formData = $form->getData(); 
        
        if(!empty($formData["league"]))
        {
          $league = $formData["league"];
          $arFilter["league_id"] = $league->getId();
          
          $session->set("matchesSearch[".$tournamentId."][league_id]", $arFilter["league_id"]);
        }
      }
      
      /**      
      * @var \AppBundle\Entity\MatchRepository
      */
      $matchesRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Match');
      
      /**      
      * @var \AppBundle\Entity\TournamentRepository
      */
      $tournamentRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament');
            
      $format = $tournamentRep->getFormatByTournament($tournamentId);
      
      $format = ucfirst(strtolower($format));
      
      $tournamentMatchesRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament'.$format.'Match');
      
      //Ищем уже добавленные матчи и исключаем их из поиска
      $excludeMatches = array();
      if($tournamentMatchesRep)
      {
        $excludeMatches = $tournamentMatchesRep->findBy(array(
          "tournament" => $tournamentId
        ));
      }
            
      $excludeMatchesIds = array();
      foreach($excludeMatches as $tournamentMatch)
        $excludeMatchesIds[] = $tournamentMatch->getMatch()->getId();
            
      $arFilter["isStarted"] = false;
      $arFilter["exclude_matches"] = $excludeMatchesIds;
      
      $arOrder = array(array("match_date" => "ASC"), array("match_time" => "ASC"));
      
      $matches = $matchesRep->search($arFilter, $arOrder);
      
      return $this->render('AppBundle:admin/matches:search.html.twig', array(
          'form' => $form->createView(),
          'tournamentId' => $tournamentId,
          'matches' => $matches
      ));
    }  
    
    public function indexAction($league_id)
    {
        $MatchRepository = $this->getDoctrine()->getRepository('AppBundle\Entity\Match');
        $matchesList = $MatchRepository->findByLeague($league_id);
        
        return $this->render('admin/matches/index.html.twig', array(
                'matches_list' => $matchesList
        ));
    }
    
    public function setScoreAction($match_id)
    {
        $match = $this->getDoctrine()->getRepository('AppBundle\Entity\Match')->find($match_id);

        if(!$match)
            throw $this->createNotFoundException('Матч не найден!');

        $form = $this->createForm(new MatchScoreType(), $match);

        $request = $this->getRequest();
        
        $form->handleRequest($request);

        if ($form->isValid()) {
            $match->setIsStarted(true);
            $match->setIsOver(true);
            $match->setIsRejected(false);

            $em = $this->getDoctrine()->getManager();
            $em->persist($match);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_matches', array('league_id' => $match->getLeague()->getId())));
        }

        return $this->render('admin/match/set_score.html.twig', array(
                'form' => $form->createView(),
                'match' => $match
        ));
    }
}