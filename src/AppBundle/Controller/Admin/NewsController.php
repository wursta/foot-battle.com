<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\News;
use AppBundle\Admin\Form\Type\NewsType;

class NewsController extends Controller
{
    public function indexAction()
    {
        $news = $this->getDoctrine()->getRepository('AppBundle\Entity\News')->findAll();        

        return $this->render('AppBundle:admin/news:index.html.twig', array(
                'newsList' => $news
            )
        );
    }

    public function addAction()
    {
        $news = new News();

        $form = $this->createForm(new NewsType(), $news);

        $request = $this->getRequest();
        
        $form->handleRequest($request);

        if ($form->isValid()) {
            
            $news->setCreateDatetime( new \DateTime() );

            $em = $this->getDoctrine()->getManager();
            $em->persist($news);
            $em->flush();
            
            return $this->redirect($this->generateUrl('admin/news/index'));
        }

        return $this->render('AppBundle:admin/news:add.html.twig', array(
                'form' => $form->createView(),
            )
        );
    }

    public function editAction($news_id)
    {
        $news = $this->getDoctrine()->getRepository('AppBundle\Entity\News')->find($news_id);

        if(!$news)
            throw $this->createNotFoundException('Новость не найдена!');

        $form = $this->createForm(new NewsType(), $news);

        $request = $this->getRequest();
        
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($news);
            $em->flush();
            
            return $this->redirect($this->generateUrl('admin/news/index'));
        }

        return $this->render('AppBundle:admin/news:edit.html.twig', array(
                'form' => $form->createView(),
            )
        );
    }

    public function deleteAction($news_id)
    {        
        $news = $this->getDoctrine()->getRepository('AppBundle\Entity\News')->find($news_id);
        
        if(!$news)
            throw $this->createNotFoundException('Новость не найдена!');
        
        $em = $this->getDoctrine()->getManager();
        $em->remove($news);
        $em->flush();
            
        return $this->redirect($this->generateUrl('admin/news/index'));        
    }
}