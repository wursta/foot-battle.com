<?php

namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\FormError;

use AppBundle\Entity\PointsScheme;
use AppBundle\Entity\PointsSchemeCorrectScore;
use AppBundle\Admin\Form\Type\PointsSchemeType;

use AppBundle\Entity\Regulation;
use AppBundle\Admin\Form\Type\RegulationType;

use AppBundle\Entity\Policy;
use AppBundle\Admin\Form\Type\PolicyType;

use AppBundle\Entity\TournamentFormat;

class PoliciesController extends Controller
{
    /**
    * Страница Пользовательское соглашение/Редакции
    */
    public function termsAction()
    {
        return $this->render('AppBundle:admin/policies:terms.html.twig');
    }

    /**
    * Страница добавления редакции пользовательского соглашения
    */
    public function addTermsAction()
    {
        $policy = new Policy();

        $form = $this->createForm(new PolicyType($this->get('locale_helper')), $policy);

        $request = $this->getRequest();

        $form->handleRequest($request);

        if($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($policy);
            $em->flush();

            return $this->redirect($this->generateUrl('admin/policies/terms'));
        }

        return $this->render('AppBundle:admin/policies/terms:add.html.twig', array(
            "form" => $form->createView()
        ));
    }

    /**
    * Страница редактирования редакции пользовательского соглашения
    */
    public function editTermsAction($id)
    {
        /** @var \AppBundle\Entity\PolicyRepository */
        $policiesRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Policy');

        /** @var \AppBundle\Entity\Policy */
        $policy = $policiesRep->find($id);

        if (!$policy) {
            return $this->createNotFoundException('Пользовательское соглашение не найдено');
        }
                
        $pointsScheme = $policy->getPointsScheme();
        $locale = $policy->getLocale();
        $regulationChampionship = $policy->getRegulationChampionship();        
        $regulationRound = $policy->getRegulationRound();
        $regulationPlayoff = $policy->getRegulationPlayoff();
        $regulationChampionsleague = $policy->getRegulationChampionsleague();
        
        $form = $this->createForm(new PolicyType($this->get('locale_helper')), $policy);

        $request = $this->getRequest();
        $form->handleRequest($request);
        
        $policy->setPointsScheme($pointsScheme);        
        $policy->setLocale($locale);
        $policy->setRegulationChampionship($regulationChampionship);        
        $policy->setRegulationRound($regulationRound);
        $policy->setRegulationPlayoff($regulationPlayoff);
        $policy->setRegulationChampionsleague($regulationChampionsleague);
        
        if($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($policy);
            $em->flush();

            return $this->redirect($this->generateUrl('admin/policies/terms'));
        }

        return $this->render('AppBundle:admin/policies/terms:edit.html.twig', array(
            "form" => $form->createView(),
            "policy" => $policy
        ));
    }

    /**
    * Возвращает данные пользовательского соглашения по-умолчанию    
    */
    public function getDefaultTermsAction()
    {
        /** @var \AppBundle\Services\PolicyHelper */
        $policyHelper = $this->get('policy_helper');

        /** @var \AppBundle\Entity\Policy */
        $policy = $policyHelper->getDefault($this->container->getParameter('locale'));
        /** @var \AppBundle\Entity\PointsScheme */
        $pointsScheme = $policyHelper->getDefaultPointsScheme();
        
        if (!$policy) {
            return $this->createNotFoundException('Не найдено пользовательского соглашения по-умолчанию.');
        }
        
        /** @var \AppBundle\Entity\TournamentFormatRepository */
        $tournamentFormatRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentFormat');
        $formats = $tournamentFormatRep->findAll();
        $formatsIds = array();
        foreach ($formats as $format) {
            $formatsIds[$format->getInternalName()] = $format->getId();
        }
        
        $regulationChampionship = $policyHelper->getDefaultFormatRegulation(
            $policy->getLocale(),
            $formatsIds['championship']
        );
        $regulationRound = $policyHelper->getDefaultFormatRegulation(
            $policy->getLocale(),
            $formatsIds['round']
        );
        $regulationPlayoff = $policyHelper->getDefaultFormatRegulation(
            $policy->getLocale(),
            $formatsIds['playoff']
        );
        $regulationChampionsleague = $policyHelper->getDefaultFormatRegulation(
            $policy->getLocale(),
            $formatsIds['championsleague']
        );

        return new JsonResponse(array(            
            "pointsSchemeId" => $pointsScheme->getId(),
            "regulationChampionshipId" => $regulationChampionship->getId(),
            "regulationRoundId" => $regulationRound->getId(),
            "regulationPlayoffId" => $regulationPlayoff->getId(),
            "regulationChampionsleagueId" => $regulationChampionsleague->getId(),
            "terms" => $policy->getTerms(),
            "regulations" => $policy->getRegulations(),
            "privacy" => $policy->getPrivacy()
        ));
    }

    /**
    * Поиск редакций пользовательских соглашений
    *
    * @param int $page
    */
    public function searchTermsAction($page = 1)
    {
        $request = $this->getRequest();

        $requestPayload = json_decode($request->getContent(), true);

        $rowsPerPage = 20;

        $arFilter = $requestPayload["filter"];
        $arOrder = $requestPayload["order"];

        /** @var \AppBundle\Entity\PolicyRepository */
        $policiesRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Policy');
        $rowset = $policiesRep->findPaginator($arOrder, $arFilter, $page, $rowsPerPage);

        $items = array();
        $total = count($rowset);

        foreach($rowset as $row)
        {
            $items[] = array(
                'id' => $row->getId(),
                'number' => $row->getNumber(),
                'editionDate' => $row->getEditionDate()->format('d.m.Y'),
                'isDefault' => $row->getIsDefault()
            );
        }

        return new JsonResponse(array(
            'items' => $items,
            'total' => $total
        ));
    }

    /**
    * Установка текущего пользовательского соглашения
    *
    * @param int $id Идентификатор
    */
    public function setCurrentPolicyAction($id)
    {
        /** @var \AppBundle\Entity\PointsSchemeRepository */
        $policiesRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Policy');

        $policies = $policiesRep->findAll();

        /** @var \Doctrine\ORM\EntityManager */
        $em = $this->getDoctrine()->getManager();

        $em->beginTransaction();

        foreach($policies as $policy) {
            $policy->setIsDefault(false);
            $em->persist($policy);
        }

        /** @var \AppBundle\Entity\Policy */
        $policy = $policiesRep->find($id);
        $policy->setIsDefault(true);
        $em->persist($policy);
        $em->flush();

        $em->commit();

        return $this->redirect($this->generateUrl('admin/policies/terms'));
    }

    /**
    * Возвращает данные о пользовательском соглашении
    *
    * @param int $id Идентификатор редакции.
    *
    * @return JsonResponse
    */
    public function viewPolicyAction($id)
    {
        /** @var \AppBundle\Entity\PolicyRepository */
        $policiesRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Policy');

        /** @var \AppBundle\Entity\Policy */
        $policy = $policiesRep->find($id);

        if (!$policy) {
            return $this->createNotFoundException('Пользовательское соглашение не найдено');
        }

        /** @var \AppBundle\Services\PolicyHelper */
        $policyHelper = $this->get('policy_helper');

        $policyHelper->render($policy);

        return new JsonResponse(array(
            'number' => $policy->getNumber(),
            'editionDate' => $policy->getEditionDate()->format('d.m.Y'),
            'terms' => $policy->getTerms(),
            'regulations' => $policy->getRegulations(),
            'privacy' => $policy->getPrivacy()
        ));
    }

    /**
    * Страница Правила/Схемы начисления очков
    */
    public function pointsSchemasAction()
    {
        return $this->render('AppBundle:admin/policies:pointsSchemas.html.twig');
    }

    /**
    * Поиск схем начисления очков
    *
    * @param int $page
    */
    public function searchPointsSchemasAction($page = 1)
    {
        $request = $this->getRequest();

        $requestPayload = json_decode($request->getContent(), true);

        $rowsPerPage = 20;

        $arFilter = $requestPayload["filter"];
        $arOrder = $requestPayload["order"];

        /** @var \AppBundle\Entity\PointsSchemeRepository */
        $pointsSchemasRep = $this->getDoctrine()->getRepository('AppBundle\Entity\PointsScheme');
        $rowset = $pointsSchemasRep->findPaginator($arOrder, $arFilter, $page, $rowsPerPage);

        $items = array();
        $total = count($rowset);

        foreach($rowset as $row)
        {
            $items[] = array(
                'id' => $row->getId(),
                'created' => $row->getCreated()->format('c'),
                'isDefault' => $row->getIsDefault()
            );
        }

        return new JsonResponse(array(
            'items' => $items,
            'total' => $total
        ));
    }

    /**
    * Форма добавления схемы начисления очков
    */
    public function addPointsSchemaAction()
    {
        $pointsScheme = new PointsScheme();

        $form = $this->createForm(new PointsSchemeType(), $pointsScheme);

        $request = $this->getRequest();

        $form->handleRequest($request);

        if($form->isValid()) {
            $pointsScheme->setCreated(new \DateTime());

            foreach ($pointsScheme->getCorrectScores() as $correctScore) {
                $correctScore->setPointsScheme($pointsScheme);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($pointsScheme);
            $em->flush();

            return $this->redirect($this->generateUrl('admin/policies/pointsSchemas'));
        }

        return $this->render('AppBundle:admin/policies/pointsSchemas:add.html.twig', array(
            "form" => $form->createView()
        ));
    }

    /**
    * Просмотр схемы начисления очков
    *
    * @param int $id Идентификатор
    */
    public function viewPointsSchemeAction($id)
    {
        /** @var \AppBundle\Entity\PointsSchemeRepository */
        $pointsSchemasRep = $this->getDoctrine()->getRepository('AppBundle\Entity\PointsScheme');

        /** @var \AppBundle\Entity\PointsScheme */
        $pointsScheme = $pointsSchemasRep->find($id);

        $data = array(
            "base" => $pointsScheme->getBase(),
            "addDiff0" => $pointsScheme->getAddDiff0(),
            "addDiff1" => $pointsScheme->getAddDiff1(),
            "addDiff2" => $pointsScheme->getAddDiff2(),
            "addDiff3" => $pointsScheme->getAddDiff3(),
            "addDiff4" => $pointsScheme->getAddDiff4(),            
            "correctScores" => array(),
            "anotherCorrectScorePoints" => $pointsScheme->getAnotherCorrectScorePoints()
        );
        $correctScores = $pointsScheme->getCorrectScores();
        foreach($correctScores as $correctScore) {
            $data["correctScores"][] = array(
                "score" => $correctScore->getScore(),
                "points" => $correctScore->getPoints()
            );
        }

        return new JsonResponse($data);
    }

    /**
    * Установка схемы используемой по-умолчанию
    *
    * @param int $id Идентификатор
    */
    public function setCurrentPointsSchemaAction($id)
    {
        /** @var \AppBundle\Entity\PointsSchemeRepository */
        $pointsSchemasRep = $this->getDoctrine()->getRepository('AppBundle\Entity\PointsScheme');

        $pointsSchemes = $pointsSchemasRep->findAll();

        /** @var \Doctrine\ORM\EntityManager */
        $em = $this->getDoctrine()->getManager();

        $em->beginTransaction();

        foreach($pointsSchemes as $pointsScheme) {
            $pointsScheme->setIsDefault(false);
            $em->persist($pointsScheme);
        }

        /** @var \AppBundle\Entity\PointsScheme */
        $pointsScheme = $pointsSchemasRep->find($id);
        $pointsScheme->setIsDefault(true);
        $em->persist($pointsScheme);
        $em->flush();

        $em->commit();

        return $this->redirect($this->generateUrl('admin/policies/pointsSchemas'));
    }
    
    /**
    * Страница Регламенты турниров    
    */
    public function regulationsAction()
    {
        return $this->render('AppBundle:admin/policies:regulations.html.twig');
    }
    
    public function searchRegulationsAction($page)
    {
        $request = $this->getRequest();

        $requestPayload = json_decode($request->getContent(), true);

        $rowsPerPage = 20;

        $arFilter = $requestPayload["filter"];
        $arOrder = $requestPayload["order"];

        /** @var \AppBundle\Entity\RegulationRepository */
        $regulationRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Regulation');
        $rowset = $regulationRep->findPaginator($arOrder, $arFilter, $page, $rowsPerPage);

        $items = array();
        $total = count($rowset);

        foreach($rowset as $row)
        {
            $items[] = array(
                'id' => $row->getId(),
                'editionDate' => $row->getEditionDate()->format('d.m.Y'),
                'tournamentFormat' => $row->getTournamentFormat()->getTitle(),
                'isDefault' => $row->getIsDefault()
            );
        }

        return new JsonResponse(array(
            'items' => $items,
            'total' => $total
        ));
    }
    
    public function viewRegulationAction($id)
    {
        /** @var \AppBundle\Entity\RegulationRepository */
        $regulationsRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Regulation');

        /** @var \AppBundle\Entity\Regulation */
        $regulation = $regulationsRep->find($id);

        if (!$regulation) {
            return $this->createNotFoundException('Редакция регламента турнира не найдена');
        }
        
        /** @var \AppBundle\Services\PolicyHelper */
        $policyHelper = $this->get('policy_helper');

        $policyHelper->renderRegulation($regulation);
        
        return new JsonResponse(array(            
            'title' => $regulation->getTitle(),
            'tournamentFormatTitle' => $regulation->getTournamentFormat()->getTitle(),
            'text' => $regulation->getText()
        ));
    }
    
    /**
    * Форма добавления регламента турнира
    */
    public function addRegulationAction()
    {
        $regulation = new Regulation();
        
        $form = $this->createForm(new RegulationType($this->get('locale_helper')), $regulation);

        $request = $this->getRequest();

        $form->handleRequest($request);

        if($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($regulation);
            $em->flush();

            return $this->redirect($this->generateUrl('admin/policies/regulations'));
        }

        return $this->render('AppBundle:admin/policies/regulations:add.html.twig', array(
            "form" => $form->createView()
        ));
    }
    
    /**
    * Форма редактирования регламента турнира
    * 
    * @param int $id Идентификатор регламента турнира    
    */
    public function editRegulationAction($id)
    {
        /** @var \AppBundle\Entity\RegulationRepository */
        $regulationRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Regulation');
        
        /** @var \AppBundle\Entity\Regulation */
        $regulation = $regulationRep->find($id);        
        
        $locale = $regulation->getLocale();
        $tournamentFormat = $regulation->getTournamentFormat();
        
        $form = $this->createForm(new RegulationType($this->get('locale_helper')), $regulation);

        $request = $this->getRequest();

        $form->handleRequest($request);

        $regulation->setLocale($locale);
        $regulation->setTournamentFormat($tournamentFormat);
        
        if($form->isValid()) {                    
            $em = $this->getDoctrine()->getManager();
            $em->persist($regulation);
            $em->flush();

            return $this->redirect($this->generateUrl('admin/policies/regulations'));
        }

        return $this->render('AppBundle:admin/policies/regulations:edit.html.twig', array(
            "form" => $form->createView()
        ));
    }
    
    /**
    * Установка регламента турнира по-умолчанию
    * 
    * @param int $id Идентификатор регламента турнира
    * 
    * @return \Symfony\Component\HttpFoundation\RedirectResponse
    */
    public function setDefaultRegulationAction($id)
    {        
        /** @var \AppBundle\Entity\RegulationRepository */
        $regulationRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Regulation');
        
        /** @var \AppBundle\Entity\Regulation */
        $regulation = $regulationRep->find($id);
        
        if (!$regulation) {
            return $this->createNotFoundException("Регламент не найден.");
        }
        
        /** @var \AppBundle\Entity\Regulation[] */
        $regulations = $regulationRep->findBy(array(
            "tournamentFormat" => $regulation->getTournamentFormat()->getId()
        ));
        
        $em = $this->getDoctrine()->getManager();
            
        $em->beginTransaction();
            
        foreach($regulations as $reg) {
            $reg->setIsDefault(false);
            $em->persist($reg);
        }
        
        $regulation->setIsDefault(true);
        $em->persist($regulation);
        
        $em->flush();        
        
        $em->commit();
        
        return $this->redirect($this->generateUrl('admin/policies/regulations'));
    }
    
    /**
    * Возвращает редакцию регламента турнира заданного типа по-умолчанию
    * 
    * @param int $tournamentFormatId ID формата турнира.
    * 
    * @return JsonResponse
    */
    public function getDefaultRegulationAction($tournamentFormatId)
    {
        
        /** @var \AppBundle\Services\PolicyHelper */
        $policyHelper = $this->get('policy_helper');
                
        /** @var \AppBundle\Entity\Regulation */
        $regulation = $policyHelper->getDefaultFormatRegulation(
            $this->container->getParameter('locale'),
            $tournamentFormatId
        );
                
        if(!$regulation) {
            return $this->createNotFoundException('Не найдено регламента трунира переданного формата по-умолчанию.');
        }

        return new JsonResponse(array("text" => $regulation->getText()));
    }
}