<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\ResourceTeam;
use AppBundle\Admin\Form\Type\ResourceTeamType;

class ResourcesController extends Controller
{
    public function indexAction()
    {
        $resources = $this->getDoctrine()->getRepository('AppBundle\Entity\GrabResource')->findAll();        

        return $this->render('AppBundle:admin/resources:index.html.twig', array(
                'resources' => $resources
            )
        );
    }
    
    public function teamsAction($resource_id)
    {
        $resource = $this->getDoctrine()->getRepository('AppBundle\Entity\GrabResource')->findById($resource_id);
            
        $teams = $this->getDoctrine()->getRepository('AppBundle\Entity\ResourceTeam')->findByResource($resource->getId());                
        
        return $this->render('AppBundle:admin/resources:teams.html.twig', array(
                'resource' => $resource,
                'teams' => $teams
            )
        );
    }
    
    public function addTeamAction($resource_id)
    {
        $resource = $this->getDoctrine()->getRepository('AppBundle\Entity\GrabResource')->findById($resource_id);
        
        $team = new ResourceTeam();                
        $team->setResource($resource);
        
        $form = $this->createForm(new ResourceTeamType($resource), $team);

        $request = $this->getRequest();
        
        $form->handleRequest($request);

        if ($form->isValid()) {                        
            $em = $this->getDoctrine()->getManager();
            $em->persist($team);
            $em->flush();
            
            return $this->redirect($this->generateUrl('admin/resources/teams', array("resource_id" => $resource->getId())));
        }
        
        return $this->render('AppBundle:admin/resources:add_team.html.twig', array(
                'resource' => $resource,
                'form' => $form->createView()
            )
        );
    }
    
    public function editTeamAction($resource_team_id)
    {
        $team = $this->getDoctrine()->getRepository('AppBundle\Entity\ResourceTeam')->findById($resource_team_id);
        $resource = $team->getResource();        
        
        $form = $this->createForm(new ResourceTeamType($resource), $team);

        $request = $this->getRequest();
        
        $form->handleRequest($request);

        if ($form->isValid()) {                        
            $em = $this->getDoctrine()->getManager();
            $em->persist($team);
            $em->flush();
            
            return $this->redirect($this->generateUrl('admin/resources/teams', array("resource_id" => $resource->getId())));
        }
        
        return $this->render('AppBundle:admin/resources:add_team.html.twig', array(
                'resource' => $resource,
                'form' => $form->createView()
            )
        );
    }
}