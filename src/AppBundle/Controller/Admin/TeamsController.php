<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Entity\Team;
use AppBundle\Admin\Form\Type\TeamType;

class TeamsController extends Controller
{    
    public function indexAction()
    {                
        $TeamRepository = $this->getDoctrine()->getRepository('AppBundle\Entity\Team');        
        $filter = array();        
        
        $teamFilter = $this->createFormBuilder()
                            ->add('country', 'country', array('required' => false))
                            ->add('search', 'submit')
                            ->getForm();
        
        $request = $this->getRequest();
                
        $teamFilter->handleRequest($request);
                
        if ($teamFilter->isValid()) {
            $filter = $teamFilter->getData();            
        }
        
        $teamList = $TeamRepository->findTeamsByFilter($filter);
        
        return $this->render('AppBundle:admin/teams:index.html.twig', array(           
            'filter_form' => $teamFilter->createView(),
            'teams_list' => $teamList
        ));
    }                

    public function addAction()
    {                
        $team = new Team();        

        $form = $this->createForm(new TeamType(), $team);

        $request = $this->getRequest();
        
        $form->handleRequest($request);

        if ($form->isValid()) {                        
            $em = $this->getDoctrine()->getManager();
            $em->persist($team);
            $em->flush();
            
            return $this->redirect($this->generateUrl('admin_teams'));
        }

        return $this->render('AppBundle:admin/teams:add.html.twig', array(                
                'form' => $form->createView()
        ));
    }

    public function editAction($team_id)
    {        
        $team = $this->getDoctrine()->getRepository('AppBundle\Entity\Team')->find($team_id);        

        $form = $this->createForm(new TeamType(), $team);

        $request = $this->getRequest();
        
        $form->handleRequest($request);

        if ($form->isValid()) {                        
            $em = $this->getDoctrine()->getManager();
            $em->persist($team);
            $em->flush();
            
            return $this->redirect($this->generateUrl('admin_teams'));
        }

        return $this->render('AppBundle:admin/teams:edit.html.twig', array(                
                'form' => $form->createView()
        ));
    }
    
    public function deleteAction($team_id)
    {
        $matches = $this->getDoctrine()->getRepository('AppBundle\Entity\Match')->findByTeam($team_id);
        
        $response = new JsonResponse();        
        
        if(count($matches) != 0)
        {
            $response->setData(array(
                'success' => false,
                'msg' => 'Команду невозможно удалить, так как она участвует в матчах.'
            ));
        }
        else        
        {            
            $team = $this->getDoctrine()->getRepository('AppBundle\Entity\Team')->find($team_id);
            
            if(!$team)
                $success = false;
            else
            {
                $em = $this->getDoctrine()->getManager();
                $em->remove($team);
                $em->flush();
                $success = true;
            }                            
                                   
            $response->setData(array(
                'success' => $success
            ));            
        }
        
        return $response;            
    }
}