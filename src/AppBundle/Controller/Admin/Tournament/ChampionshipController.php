<?php
namespace AppBundle\Controller\Admin\Tournament;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Entity\TournamentChampionship;
use AppBundle\Entity\TournamentChampionshipMatch;
use AppBundle\Form\Type\Tournament\ChampionshipType;
use AppBundle\Event\TournamentEvent;
use AppBundle\TournamentEvents;

class ChampionshipController extends Controller
{
  /**
  * Форма с настройками турнира
  * 
  * @param int $tournament_id ID основного турнира
  * 
  * @return \Symfony\Component\HttpFoundation\Response
  */
  public function editAction($tournament_id)
  {
    /**    
    * @var \AppBundle\Entity\TournamentRepository
    */
    $tournamentRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament');
      
    /**    
    * @var \AppBundle\Entity\TournamentChampionshipRepository
    */
    $championshipRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentChampionship');
    
    /**    
    * @var \AppBundle\Services\Tournaments\Championship
    */
    $championshipHelper = $this->get("tournament.championship");    
        
    /**    
    * @var \AppBundle\Entity\TournamentChampionship
    */
    $tournament = $championshipRep->find($tournament_id);
    
    if(!$tournament)
    {
      $tournament = new TournamentChampionship();
      $initTournament = $tournamentRep->find($tournament_id);
      $tournament->setTournament($initTournament);
    }
    
    if($tournament->getTournament()->getIsReady())
      throw $this->createNotFoundException('Турнир уже опубликован!');        
      
    $form = $this->createForm(new ChampionshipType(), $tournament);
    
    $request = $this->getRequest();
    
    $form->handleRequest($request);
    
    if($form->isValid())
    {
      try {
        $championshipHelper->validate($tournament);
        
        $tournament->getTournament()->setStep(3);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($tournament);
        $em->persist($tournament->getTournament());
        $em->flush();
        
        return $this->redirect($this->generateUrl('admin/tournaments/championship/matches', array('tournament_id' => $tournament->getTournament()->getId())));
      } catch (\Exception $ex) {
        $request->getSession()->getFlashBag()->add("validation_error", $ex->getMessage());
      }
    }
    
    return $this->render('AppBundle:admin/tournaments/edit/championship:edit.html.twig', array(
      "form" => $form->createView()
    ));
  }
  
  /**
  * Таблица с матчами разбитыми по турам
  * 
  * @param int $tournament_id ID основного турнира
  * 
  * @return \Symfony\Component\HttpFoundation\Response
  */
  public function matchesAction($tournament_id)
  {
    /**    
    * @var \AppBundle\Entity\TournamentRepository
    */
    $tournamentRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament');
      
    /**    
    * @var \AppBundle\Entity\TournamentChampionshipRepository
    */
    $championshipRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentChampionship');
    
    /**    
    * @var \AppBundle\Entity\TournamentChampionshipMatchRepository
    */
    $championshipMatchesRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentChampionshipMatch');
    
    /**    
    * @var \AppBundle\Services\Tournaments\Championship
    */
    $championshipHelper = $this->get("tournament.championship");    
        
    /**    
    * @var \AppBundle\Entity\TournamentChampionship
    */
    $tournament = $championshipRep->find($tournament_id);
    
    $tournamentMatches = $championshipMatchesRep->findByTournament($tournament_id);
    
    $matchesByRounds = $championshipHelper->chunkMatches($tournamentMatches, $tournament->getRoundsCount(), $tournament->getMatchesInRound());
    
    $request = $this->getRequest();
    
    if($request->get('publish') == 'publish')
    {
      try {
        $championshipHelper->checkMatches($tournament);
        
        $tournament->getTournament()->setIsReady(true);
        $tournament->getTournament()->setIsAccessible(true);
        $tournament->getTournament()->setStep(null);

        $em = $this->getDoctrine()->getManager();
        $em->persist($tournament->getTournament());
        $em->flush($tournament->getTournament());

        return $this->redirect($this->generateUrl('admin/tournaments/published'));
        
      } catch (\Exception $ex) {
          $request->getSession()->getFlashBag()->add("validation_error", $ex->getMessage());
      }      
    }
        
    return $this->render('AppBundle:admin/tournaments/edit/championship:matches.html.twig', array(
      'tournament' => $tournament,
      'matchesByRounds' => $matchesByRounds
    ));
  }
  
  /**
  * Добавляет матч в турпир
  * 
  * Возвращает ответ в формате JSON
  * В случае успеха возвращает { success: true }
  * В случае ошибки возвращет { success: false, msg: error }
  * 
  * @param \AppBundle\Entity\Match $matchId Матч
  * @param int $tournamentId ID турнира
  * @param int $roundNum Номер тура
  * 
  * @return JsonResponse
  */
  public function addMatchAction(\AppBundle\Entity\Match $match, $tournamentId, $roundNum)
  {
    $success = false;
    $errorMsg = 'Матч не добавлен. Произошла непредвиденная ошибка.';
    
    /**    
    * @var \AppBundle\Entity\TournamentChampionshipMatchRepository
    */
    $championshipMatchesRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentChampionshipMatch');
    
    /**    
    * @var \AppBundle\Entity\TournamentChampionshipRepository
    */
    $championshipRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentChampionship');
    
    /**    
    * @var \AppBundle\Entity\TournamentChampionship
    */
    $championship = $championshipRep->findOneBy(array(
      "tournament" => $tournamentId
    ));
        
    /**    
    * @var \AppBundle\Entity\Tournament
    */
    $tournament = $championship->getTournament();
      
    $matchInTournament = (bool) count($championshipMatchesRep->findBy(array(
      "tournament" => $tournament->getId(),
      "match" => $match->getId()
    )));
    
    if($matchInTournament)
    {
      $errorMsg = 'Данный матч уже присутствует в турнире.';
    }
    else
    {
      $tournamentMatch = new TournamentChampionshipMatch();
      $tournamentMatch->setRound($roundNum);
      $tournamentMatch->setTournament($tournament);
      $tournamentMatch->setMatch($match);      
      
      try
      {
        $em = $this->getDoctrine()->getManager();
        $em->persist($tournamentMatch);
        $em->flush($tournamentMatch);
        
        /**        
        * @var \Symfony\Component\HttpKernel\Debug\TraceableEventDispatcher
        */
        $eventDispatcher = $this->get('event_dispatcher');
        $event = new TournamentEvent($championship);
        $eventDispatcher->dispatch(TournamentEvents::CHAMPIONSHIP_MATCH_ADDED, $event);
        
        $success = true;      
      } catch(\Exception $e) {
        $errorMsg = $e->getMessage();
      }
    }
    
    if($success)
      $responseData = array('success' => true);
    else
      $responseData = array('success' => false, 'msg' => $errorMsg);
    
    $response = new JsonResponse();
    $response->setData($responseData);
    
    return $response;
  }
  
  /**
  * Удаляет матч из турпира
  * 
  * Возвращает ответ в формате JSON
  * В случае успеха возвращает { success: true }
  * В случае ошибки возвращет { success: false, msg: error }
  * 
  * @param \AppBundle\Entity\Match $matchId Матч
  * @param int $tournamentId ID турнира  
  * 
  * @return JsonResponse
  */
  public function deleteMatchAction(\AppBundle\Entity\Match $match, $tournamentId)
  {
    $success = false;
    $errorMsg = 'Матч не удалён. Произошла непредвиденная ошибка.';
    
    /**    
    * @var \AppBundle\Entity\TournamentChampionshipMatchRepository
    */
    $championshipMatchesRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentChampionshipMatch');
    
    /**    
    * @var \AppBundle\Entity\TournamentChampionshipRepository
    */
    $championshipRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentChampionship');
    
    /**    
    * @var \AppBundle\Entity\TournamentChampionship
    */
    $championship = $championshipRep->findOneBy(array(
      "tournament" => $tournamentId
    ));
        
    /**    
    * @var \AppBundle\Entity\Tournament
    */
    $tournament = $championship->getTournament();
    
    $tournamentMatchesCount = $championshipMatchesRep->findMathesCountByTournament($tournament->getId());
    
    $tournamentMatch = $championshipMatchesRep->findOneBy(array(
      "tournament" => $tournament->getId(),
      "match" => $match->getId()
      )
    );
    
    if(!$tournamentMatch)
    {
      $errorMsg = 'Данный матч не присутствует в турнире.';
    }
    elseif($tournament->getIsReady() && $tournamentMatchesCount <= 1)
    {
      $errorMsg = 'Нельзя удалить единственный матч в опубликованном турнире.';
    }
    else
    {
      try
      {
        $em = $this->getDoctrine()->getManager();
        $em->remove($tournamentMatch);
        $em->flush($tournamentMatch);
        
        /**        
        * @var \Symfony\Component\HttpKernel\Debug\TraceableEventDispatcher
        */
        $eventDispatcher = $this->get('event_dispatcher');
        $event = new TournamentEvent($championship);
        $eventDispatcher->dispatch(TournamentEvents::CHAMPIONSHIP_MATCH_DELETED, $event);
        
        $success = true;
      } catch(\Exception $e) {
        $errorMsg = $e->getMessage();
      }
    }
    
    if($success)
      $responseData = array('success' => true);
    else
      $responseData = array('success' => false, 'msg' => $errorMsg);
    
    $response = new JsonResponse();
    $response->setData($responseData);
    
    return $response;
  }
}
?>
