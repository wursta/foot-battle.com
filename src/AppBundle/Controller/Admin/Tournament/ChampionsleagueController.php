<?php
namespace AppBundle\Controller\Admin\Tournament;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Entity\TournamentChampionsleague;
use AppBundle\Entity\TournamentChampionsleagueMatch;
use AppBundle\Form\Type\Tournament\ChampionsleagueType;
use AppBundle\Event\TournamentEvent;
use AppBundle\TournamentEvents;

class ChampionsleagueController extends Controller
{
  /**
  * Форма с настройками турнира
  * 
  * @param int $tournament_id ID основного турнира
  * 
  * @return \Symfony\Component\HttpFoundation\Response
  */
  public function editAction($tournament_id)
  {
    /**    
    * @var \AppBundle\Entity\TournamentRepository
    */
    $tournamentRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament');
      
    /**    
    * @var \AppBundle\Entity\TournamentChampionsleagueRepository
    */
    $championsleagueRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentChampionsleague');
    
    /**    
    * @var \AppBundle\Services\Tournaments\Championsleague
    */
    $championsleagueHelper = $this->get("tournament.championsleague");    
        
    /**    
    * @var \AppBundle\Entity\TournamentChampionsleague
    */
    $tournament = $championsleagueRep->find($tournament_id);
    
    if(!$tournament)
    {
      $tournament = new TournamentChampionsleague();
      $initTournament = $tournamentRep->find($tournament_id);
      $tournament->setTournament($initTournament);
      $tournament->setStage(TournamentChampionsleague::STAGE_QUALIFICATION);
    }
    
    if($tournament->getTournament()->getIsReady())
      throw $this->createNotFoundException('Турнир уже опубликован!');
      
    $form = $this->createForm(new ChampionsleagueType(), $tournament);
    
    $request = $this->getRequest();
    
    $form->handleRequest($request);
    
    if($form->isValid())
    {
      try {
        $championsleagueHelper->validate($tournament);
        
        $tournament->getTournament()->setStep(3);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($tournament);
        $em->persist($tournament->getTournament());
        $em->flush();
        
        return $this->redirect($this->generateUrl('admin/tournaments/championsleague/matches', array('tournament_id' => $tournament->getTournament()->getId())));
      } catch (\Exception $ex) {
        $request->getSession()->getFlashBag()->add("validation_error", $ex->getMessage());
      }
    }
    
    return $this->render('AppBundle:admin/tournaments/edit/championsleague:edit.html.twig', array(
      "form" => $form->createView()
    ));
  }
  
  /**
  * Таблица с матчами разбитыми по турам
  * 
  * @param int $tournament_id ID основного турнира
  * 
  * @return \Symfony\Component\HttpFoundation\Response
  */
  public function matchesAction($tournament_id)
  {
    /**    
    * @var \AppBundle\Entity\TournamentRepository
    */
    $tournamentRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament');
      
    /**    
    * @var \AppBundle\Entity\TournamentChampionsleagueRepository
    */
    $championsleagueRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentChampionsleague');
    
    /**    
    * @var \AppBundle\Entity\TournamentChampionsleagueMatchRepository
    */
    $championsleagueMatchesRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentChampionsleagueMatch');
    
    /**    
    * @var \AppBundle\Services\Tournaments\Championsleague
    */
    $championsleagueHelper = $this->get("tournament.championsleague");
        
    /**    
    * @var \AppBundle\Entity\TournamentChampionsleague
    */
    $tournament = $championsleagueRep->find($tournament_id);
    
    $tournamentQualificationMatches = $championsleagueMatchesRep->findQualificationByTournament($tournament_id);    
    $qualificationMatchesByRounds = $championsleagueHelper->chunkMatches($tournamentQualificationMatches, $tournament->getQualificationRoundsCount(), $tournament->getMatchesInQualification());
    
    $tournamentGroupMatches = $championsleagueMatchesRep->findGroupByTournament($tournament_id);
    
    $groupRoundsCount = $tournament->getRealGroupRoundsCount();
    if(!$groupRoundsCount)
      $groupRoundsCount = $championsleagueHelper->getMaxGroupRoundsCount($tournament->getMaxUsersCount(), $tournament->getMaxGroupsCount());
        
    $groupMatchesByRounds = $championsleagueHelper->chunkMatches($tournamentGroupMatches, $groupRoundsCount, $tournament->getMatchesInGame());
    
    
    $tournamentPlayoffMatches = $championsleagueMatchesRep->findPlayoffByTournament($tournament_id);
    
    $playoffRoundsCount = $tournament->getRealPlayoffRoundsCount();
    if(!$playoffRoundsCount)
      $playoffRoundsCount = $championsleagueHelper->getMaxPlayoffRoundsCount($tournament->getMaxUsersCount(), $tournament->getMaxGroupsCount(), $tournament->getUsersOutOfGroup());      
    
    $playoffMatchesByRounds = $championsleagueHelper->chunkMatches($tournamentPlayoffMatches, $playoffRoundsCount, $tournament->getMatchesInPlayoff());
    
    $request = $this->getRequest();
    
    if($request->get('publish') == 'publish')
    {
      try {
        $championsleagueHelper->checkMatches($tournament);
        
        $tournament->getTournament()->setIsReady(true);
        $tournament->getTournament()->setIsAccessible(true);
        $tournament->getTournament()->setStep(null);

        $em = $this->getDoctrine()->getManager();
        $em->persist($tournament->getTournament());
        $em->flush($tournament->getTournament());

        return $this->redirect($this->generateUrl('admin/tournaments/published'));
        
      } catch (\Exception $ex) {
          $request->getSession()->getFlashBag()->add("validation_error", $ex->getMessage());
      }      
    }
        
    return $this->render('AppBundle:admin/tournaments/edit/championsleague:matches.html.twig', array(
      'tournament' => $tournament,
      'qualificationMatchesByRounds' => $qualificationMatchesByRounds,
      'groupMatchesByRounds' => $groupMatchesByRounds,
      'playoffMatchesByRounds' => $playoffMatchesByRounds
    ));
  }
  
  /**
  * Добавляет матч в турпир
  * 
  * Возвращает ответ в формате JSON
  * В случае успеха возвращает { success: true }
  * В случае ошибки возвращет { success: false, msg: error }
  * 
  * @param \AppBundle\Entity\Match $matchId Матч
  * @param int $tournamentId ID турнира
  * @param int $roundNum Номер тура
  * @param bool $qualificationMatch Флаг - является ли добавляемый матч квалификационным
  * @param bool $groupMatch Флаг - является ли добавляемый матч групповым
  * 
  * @return JsonResponse
  */
  public function addMatchAction(\AppBundle\Entity\Match $match, $tournamentId, $roundNum, $qualificationMatch, $groupMatch)
  {    
    $success = false;
    $errorMsg = 'Матч не добавлен. Произошла непредвиденная ошибка.';
    
    /**    
    * @var \AppBundle\Entity\TournamentChampionsleagueMatchRepository
    */
    $championsleagueMatchesRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentChampionsleagueMatch');
    
    /**    
    * @var \AppBundle\Entity\TournamentChampionsleagueRepository
    */
    $championsleagueRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentChampionsleague');
    
    /**    
    * @var \AppBundle\Entity\TournamentChampionsleague
    */
    $championsleague = $championsleagueRep->findOneBy(array(
      "tournament" => $tournamentId
    ));
        
    /**    
    * @var \AppBundle\Entity\Tournament
    */
    $tournament = $championsleague->getTournament();
    
    $matchInTournament = (bool) count($championsleagueMatchesRep->findBy(array(
      "tournament" => $tournament->getId(),
      "match" => $match->getId()
    )));
    
    if($matchInTournament)
    {
      $errorMsg = 'Данный матч уже присутствует в турнире.';
    }
    else
    {
      $tournamentMatch = new TournamentChampionsleagueMatch();
      $tournamentMatch->setRound($roundNum);
      $tournamentMatch->setTournament($tournament);
      $tournamentMatch->setMatch($match);      
      $tournamentMatch->setIsQualificationMatch($qualificationMatch);
      $tournamentMatch->setIsGroupMatch($groupMatch);
      
      try
      {
        $em = $this->getDoctrine()->getManager();
        $em->persist($tournamentMatch);
        $em->flush($tournamentMatch);
        
        /**        
        * @var \Symfony\Component\HttpKernel\Debug\TraceableEventDispatcher
        */
        $eventDispatcher = $this->get('event_dispatcher');
        $event = new TournamentEvent($championsleague);
        $eventDispatcher->dispatch(TournamentEvents::CHAMPIONSLEAGUE_MATCH_ADDED, $event);
        
        $success = true;      
      } catch(\Exception $e) {
        $errorMsg = $e->getMessage();
      }
    }
    
    if($success)
      $responseData = array('success' => true);
    else
      $responseData = array('success' => false, 'msg' => $errorMsg);
    
    $response = new JsonResponse();
    $response->setData($responseData);
    
    return $response;
  }
  
  /**
  * Удаляет матч из турпира
  * 
  * Возвращает ответ в формате JSON
  * В случае успеха возвращает { success: true }
  * В случае ошибки возвращет { success: false, msg: error }
  * 
  * @param \AppBundle\Entity\Match $matchId Матч
  * @param int $tournamentId ID турнира  
  * 
  * @return JsonResponse
  */
  public function deleteMatchAction(\AppBundle\Entity\Match $match, $tournamentId)
  {
    $success = false;
    $errorMsg = 'Матч не удалён. Произошла непредвиденная ошибка.';
    
    /**    
    * @var \AppBundle\Entity\TournamentChampionsleagueMatchRepository
    */
    $championsleagueMatchesRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentChampionsleagueMatch');
    
    /**    
    * @var \AppBundle\Entity\TournamentChampionsleagueRepository
    */
    $championsleagueRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentChampionsleague');
    
    /**    
    * @var \AppBundle\Entity\TournamentChampionsleague
    */
    $championsleague = $championsleagueRep->findOneBy(array(
      "tournament" => $tournamentId
    ));
        
    /**    
    * @var \AppBundle\Entity\Tournament
    */
    $tournament = $championsleague->getTournament();
    
    $tournamentMatchesCount = $championsleagueMatchesRep->findMathesCountByTournament($tournament->getId());
    
    $tournamentMatch = $championsleagueMatchesRep->findOneBy(array(
      "tournament" => $tournament->getId(),
      "match" => $match->getId()
      )
    );
    
    if(!$tournamentMatch)
    {
      $errorMsg = 'Данный матч не присутствует в турнире.';
    }
    elseif($tournament->getIsReady() && $tournamentMatchesCount <= 1)
    {
      $errorMsg = 'Нельзя удалить единственный матч в опубликованном турнире.';
    }
    else
    {
      try
      {
        $em = $this->getDoctrine()->getManager();
        $em->remove($tournamentMatch);
        $em->flush($tournamentMatch);
        
        /**        
        * @var \Symfony\Component\HttpKernel\Debug\TraceableEventDispatcher
        */
        $eventDispatcher = $this->get('event_dispatcher');
        $event = new TournamentEvent($championsleague);
        $eventDispatcher->dispatch(TournamentEvents::CHAMPIONSLEAGUE_MATCH_DELETED, $event);
        
        $success = true;
      } catch(\Exception $e) {
        $errorMsg = $e->getMessage();
      }
    }
    
    if($success)
      $responseData = array('success' => true);
    else
      $responseData = array('success' => false, 'msg' => $errorMsg);
    
    $response = new JsonResponse();
    $response->setData($responseData);
    
    return $response;
  }
}
?>
