<?php
namespace AppBundle\Controller\Admin\Tournament;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Entity\TournamentPlayoff;
use AppBundle\Entity\TournamentPlayoffMatch;
use AppBundle\Form\Type\Tournament\PlayoffType;
use AppBundle\Event\TournamentEvent;
use AppBundle\TournamentEvents;

class PlayoffController extends Controller
{
  /**
  * Форма с настройками турнира
  * 
  * @param int $tournament_id ID основного турнира
  * 
  * @return \Symfony\Component\HttpFoundation\Response
  */
  public function editAction($tournament_id)
  {    
    /**    
    * @var \AppBundle\Entity\TournamentRepository
    */
    $tournamentRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament');
      
    /**    
    * @var \AppBundle\Entity\TournamentPlayoffRepository
    */
    $playoffRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentPlayoff');
    
    /**    
    * @var \AppBundle\Services\Tournaments\Playoff
    */
    $playoffHelper = $this->get("tournament.playoff");    
        
    /**    
    * @var \AppBundle\Entity\TournamentPlayoff
    */
    $tournament = $playoffRep->find($tournament_id);
    
    if(!$tournament)
    {
      $tournament = new TournamentPlayoff();
      $initTournament = $tournamentRep->find($tournament_id);
      $tournament->setTournament($initTournament);
      $tournament->setStage(TournamentPlayoff::STAGE_QUALIFICATION);
    }
    
    if($tournament->getTournament()->getIsReady())
      throw $this->createNotFoundException('Турнир уже опубликован!');
      
    $form = $this->createForm(new PlayoffType(), $tournament);
    
    $request = $this->getRequest();
    
    $form->handleRequest($request);
    
    if($form->isValid())
    {
      try {
        $playoffHelper->validate($tournament);
        
        $tournament->getTournament()->setStep(3);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($tournament);
        $em->persist($tournament->getTournament());
        $em->flush();
        
        return $this->redirect($this->generateUrl('admin/tournaments/playoff/matches', array('tournament_id' => $tournament->getTournament()->getId())));
      } catch (\Exception $ex) {
        $request->getSession()->getFlashBag()->add("validation_error", $ex->getMessage());
      }
    }
    
    return $this->render('AppBundle:admin/tournaments/edit/playoff:edit.html.twig', array(
      "form" => $form->createView()
    ));
  }
  
  /**
  * Таблица с матчами разбитыми по турам
  * 
  * @param int $tournament_id ID основного турнира
  * 
  * @return \Symfony\Component\HttpFoundation\Response
  */
  public function matchesAction($tournament_id)
  {
    /**    
    * @var \AppBundle\Entity\TournamentRepository
    */
    $tournamentRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament');
      
    /**    
    * @var \AppBundle\Entity\TournamentPlayoffRepository
    */
    $playoffRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentPlayoff');
    
    /**    
    * @var \AppBundle\Entity\TournamentPlayoffMatchRepository
    */
    $playoffMatchesRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentPlayoffMatch');
    
    /**    
    * @var \AppBundle\Services\Tournaments\Playoff
    */
    $playoffHelper = $this->get("tournament.playoff");
        
    /**    
    * @var \AppBundle\Entity\TournamentPlayoff
    */
    $tournament = $playoffRep->find($tournament_id);
    
    $tournamentQualificationMatches = $playoffMatchesRep->findQualificationByTournament($tournament_id);
    $tournamentPlayoffMatches = $playoffMatchesRep->findPlayoffByTournament($tournament_id);
        
    $qualificationMatchesByRounds = $playoffHelper->chunkMatches($tournamentQualificationMatches, $tournament->getQualificationRoundsCount(), $tournament->getMatchesInQualification());
    
    $playoffRoundsCount = $tournament->getPlayoffRoundsCount();    
    if(!$playoffRoundsCount)
      $playoffRoundsCount = $playoffHelper->getPlayoffRoundsCount($tournament->getMaxUsersCount());
    
    $playoffMatchesByRounds = $playoffHelper->chunkMatches($tournamentPlayoffMatches, $playoffRoundsCount, $tournament->getMatchesInGame());
    
    $request = $this->getRequest();
    
    if($request->get('publish') == 'publish')
    {
      try {
        $playoffHelper->checkMatches($tournament);
        
        $tournament->getTournament()->setIsReady(true);
        $tournament->getTournament()->setIsAccessible(true);
        $tournament->getTournament()->setStep(null);

        $em = $this->getDoctrine()->getManager();
        $em->persist($tournament->getTournament());
        $em->flush($tournament->getTournament());

        return $this->redirect($this->generateUrl('admin/tournaments/published'));
        
      } catch (\Exception $ex) {
          $request->getSession()->getFlashBag()->add("validation_error", $ex->getMessage());
      }      
    }
        
    return $this->render('AppBundle:admin/tournaments/edit/playoff:matches.html.twig', array(
      'tournament' => $tournament,
      'qualificationMatchesByRounds' => $qualificationMatchesByRounds,
      'playoffMatchesByRounds' => $playoffMatchesByRounds
    ));
  }
  
  /**
  * Добавляет матч в турпир
  * 
  * Возвращает ответ в формате JSON
  * В случае успеха возвращает { success: true }
  * В случае ошибки возвращет { success: false, msg: error }
  * 
  * @param \AppBundle\Entity\Match $matchId Матч
  * @param int $tournamentId ID турнира
  * @param int $roundNum Номер тура
  * @param bool $qualificationMatch Флаг - является ли добавляемый матч квалификационным
  * 
  * @return JsonResponse
  */
  public function addMatchAction(\AppBundle\Entity\Match $match, $tournamentId, $roundNum, $qualificationMatch)
  {    
    $success = false;
    $errorMsg = 'Матч не добавлен. Произошла непредвиденная ошибка.';
    
    /**    
    * @var \AppBundle\Entity\TournamentPlayoffMatchRepository
    */
    $playoffMatchesRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentPlayoffMatch');
    
    /**    
    * @var \AppBundle\Entity\TournamentPlayoffRepository
    */
    $playoffRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentPlayoff');
    
    /**    
    * @var \AppBundle\Entity\TournamentPlayoff
    */
    $playoff = $playoffRep->findOneBy(array(
      "tournament" => $tournamentId
    ));
        
    /**    
    * @var \AppBundle\Entity\Tournament
    */
    $tournament = $playoff->getTournament();
    
    $matchInTournament = (bool) count($playoffMatchesRep->findBy(array(
      "tournament" => $tournament->getId(),
      "match" => $match->getId()
    )));
    
    if($matchInTournament)
    {
      $errorMsg = 'Данный матч уже присутствует в турнире.';
    }
    else
    {
      $tournamentMatch = new TournamentPlayoffMatch();
      $tournamentMatch->setRound($roundNum);
      $tournamentMatch->setTournament($tournament);
      $tournamentMatch->setMatch($match);      
      $tournamentMatch->setIsQualificationMatch($qualificationMatch);
      
      try
      {
        $em = $this->getDoctrine()->getManager();
        $em->persist($tournamentMatch);
        $em->flush($tournamentMatch);
        
        /**        
        * @var \Symfony\Component\HttpKernel\Debug\TraceableEventDispatcher
        */
        $eventDispatcher = $this->get('event_dispatcher');
        $event = new TournamentEvent($playoff);
        $eventDispatcher->dispatch(TournamentEvents::PLAYOFF_MATCH_ADDED, $event);
        
        $success = true;      
      } catch(\Exception $e) {
        $errorMsg = $e->getMessage();
      }
    }
    
    if($success)
      $responseData = array('success' => true);
    else
      $responseData = array('success' => false, 'msg' => $errorMsg);
    
    $response = new JsonResponse();
    $response->setData($responseData);
    
    return $response;
  }
  
  /**
  * Удаляет матч из турпира
  * 
  * Возвращает ответ в формате JSON
  * В случае успеха возвращает { success: true }
  * В случае ошибки возвращет { success: false, msg: error }
  * 
  * @param \AppBundle\Entity\Match $matchId Матч
  * @param int $tournamentId ID турнира  
  * 
  * @return JsonResponse
  */
  public function deleteMatchAction(\AppBundle\Entity\Match $match, $tournamentId)
  {
    $success = false;
    $errorMsg = 'Матч не удалён. Произошла непредвиденная ошибка.';
    
    /**    
    * @var \AppBundle\Entity\TournamentPlayoffMatchRepository
    */
    $playoffMatchesRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentPlayoffMatch');
    
    /**    
    * @var \AppBundle\Entity\TournamentPlayoffRepository
    */
    $playoffRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentPlayoff');
    
    /**    
    * @var \AppBundle\Entity\TournamentPlayoff
    */
    $playoff = $playoffRep->findOneBy(array(
      "tournament" => $tournamentId
    ));
        
    /**    
    * @var \AppBundle\Entity\Tournament
    */
    $tournament = $playoff->getTournament();
    
    $tournamentMatchesCount = $playoffMatchesRep->findMathesCountByTournament($tournament->getId());
    
    $tournamentMatch = $playoffMatchesRep->findOneBy(array(
      "tournament" => $tournament->getId(),
      "match" => $match->getId()
      )
    );
    
    if(!$tournamentMatch)
    {
      $errorMsg = 'Данный матч не присутствует в турнире.';
    }
    elseif($tournament->getIsReady() && $tournamentMatchesCount <= 1)
    {
      $errorMsg = 'Нельзя удалить единственный матч в опубликованном турнире.';
    }
    else
    {
      try
      {
        $em = $this->getDoctrine()->getManager();
        $em->remove($tournamentMatch);
        $em->flush($tournamentMatch);
        
        /**        
        * @var \Symfony\Component\HttpKernel\Debug\TraceableEventDispatcher
        */
        $eventDispatcher = $this->get('event_dispatcher');
        $event = new TournamentEvent($playoff);
        $eventDispatcher->dispatch(TournamentEvents::PLAYOFF_MATCH_DELETED, $event);
        
        $success = true;
      } catch(\Exception $e) {
        $errorMsg = $e->getMessage();
      }
    }
    
    if($success)
      $responseData = array('success' => true);
    else
      $responseData = array('success' => false, 'msg' => $errorMsg);
    
    $response = new JsonResponse();
    $response->setData($responseData);
    
    return $response;
  }
}
?>
