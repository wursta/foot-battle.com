<?php
namespace AppBundle\Controller\Admin\Tournament;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Entity\TournamentRound;
use AppBundle\Entity\TournamentRoundMatch;
use AppBundle\Form\Type\Tournament\RoundType;
use AppBundle\Event\TournamentEvent;
use AppBundle\TournamentEvents;

class RoundController extends Controller
{
  /**
  * Форма с настройками турнира
  * 
  * @param int $tournament_id ID основного турнира
  * 
  * @return \Symfony\Component\HttpFoundation\Response
  */
  public function editAction($tournament_id)
  {
    /**    
    * @var \AppBundle\Entity\TournamentRepository
    */
    $tournamentRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament');
      
    /**    
    * @var \AppBundle\Entity\TournamentRoundRepository
    */
    $roundRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentRound');
    
    /**    
    * @var \AppBundle\Services\Tournaments\Round
    */
    $roundHelper = $this->get("tournament.round");    
        
    /**    
    * @var \AppBundle\Entity\TournamentRound
    */
    $tournament = $roundRep->find($tournament_id);
    
    if(!$tournament)
    {
      $tournament = new TournamentRound();
      $initTournament = $tournamentRep->find($tournament_id);
      $tournament->setTournament($initTournament);
    }
    
    if($tournament->getTournament()->getIsReady())
      throw $this->createNotFoundException('Турнир уже опубликован!');        
      
    $form = $this->createForm(new RoundType(), $tournament);
    
    $request = $this->getRequest();
    
    $form->handleRequest($request);
    
    if($form->isValid())
    {
      try {
        $roundHelper->validate($tournament);
        
        $tournament->getTournament()->setStep(3);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($tournament);
        $em->persist($tournament->getTournament());
        $em->flush();
        
        return $this->redirect($this->generateUrl('admin/tournaments/round/matches', array('tournament_id' => $tournament->getTournament()->getId())));
      } catch (\Exception $ex) {
        $request->getSession()->getFlashBag()->add("validation_error", $ex->getMessage());
      }
    }
    
    return $this->render('AppBundle:admin/tournaments/edit/round:edit.html.twig', array(
      "form" => $form->createView()
    ));
  }
  
  /**
  * Таблица с матчами разбитыми по турам
  * 
  * @param int $tournament_id ID основного турнира
  * 
  * @return \Symfony\Component\HttpFoundation\Response
  */
  public function matchesAction($tournament_id)
  {
    /**    
    * @var \AppBundle\Entity\TournamentRepository
    */
    $tournamentRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament');
      
    /**    
    * @var \AppBundle\Entity\TournamentRoundRepository
    */
    $roundRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentRound');
    
    /**    
    * @var \AppBundle\Entity\TournamentRoundMatchRepository
    */
    $roundMatchesRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentRoundMatch');
    
    /**    
    * @var \AppBundle\Services\Tournaments\Championship
    */
    $roundHelper = $this->get("tournament.round");    
        
    /**    
    * @var \AppBundle\Entity\TournamentRound
    */
    $tournament = $roundRep->find($tournament_id);
    
    $tournamentMatches = $roundMatchesRep->findByTournament($tournament_id);
    
    $roundsCount = $tournament->getRealRoundsCount();
    if(!$roundsCount)
      $roundsCount = $roundHelper->getRoundsCount($tournament->getMaxUsersCount());
    
    $matchesByRounds = $roundHelper->chunkMatches($tournamentMatches, $roundsCount, $tournament->getMatchesInGame());
    
    $request = $this->getRequest();
    
    if($request->get('publish') == 'publish')
    {
      try {
        $roundHelper->checkMatches($tournament);
        
        $tournament->getTournament()->setIsReady(true);
        $tournament->getTournament()->setIsAccessible(true);
        $tournament->getTournament()->setStep(null);

        $em = $this->getDoctrine()->getManager();
        $em->persist($tournament->getTournament());
        $em->flush($tournament->getTournament());

        return $this->redirect($this->generateUrl('admin/tournaments/published'));
        
      } catch (\Exception $ex) {
          $request->getSession()->getFlashBag()->add("validation_error", $ex->getMessage());
      }      
    }
        
    return $this->render('AppBundle:admin/tournaments/edit/round:matches.html.twig', array(
      'tournament' => $tournament,
      'matchesByRounds' => $matchesByRounds
    ));
  }
  
  /**
  * Добавляет матч в турпир
  * 
  * Возвращает ответ в формате JSON
  * В случае успеха возвращает { success: true }
  * В случае ошибки возвращет { success: false, msg: error }
  * 
  * @param \AppBundle\Entity\Match $matchId Матч
  * @param int $tournamentId ID турнира
  * @param int $roundNum Номер тура
  * 
  * @return JsonResponse
  */
  public function addMatchAction(\AppBundle\Entity\Match $match, $tournamentId, $roundNum)
  {
    $success = false;
    $errorMsg = 'Матч не добавлен. Произошла непредвиденная ошибка.';
    
    /**    
    * @var \AppBundle\Entity\TournamentRoundMatchRepository
    */
    $roundMatchesRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentRoundMatch');
    
    /**    
    * @var \AppBundle\Entity\TournamentRoundRepository
    */
    $roundRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentRound');
    
    /**    
    * @var \AppBundle\Entity\TournamentRound
    */
    $round = $roundRep->findOneBy(array(
      "tournament" => $tournamentId
    ));
        
    /**    
    * @var \AppBundle\Entity\Tournament
    */
    $tournament = $round->getTournament();
      
    $matchInTournament = (bool) count($roundMatchesRep->findBy(array(
      "tournament" => $tournament->getId(),
      "match" => $match->getId()
    )));
    
    if($matchInTournament)
    {
      $errorMsg = 'Данный матч уже присутствует в турнире.';
    }
    else
    {
      $tournamentMatch = new TournamentRoundMatch();
      $tournamentMatch->setRound($roundNum);
      $tournamentMatch->setTournament($tournament);
      $tournamentMatch->setMatch($match);      
      
      try
      {
        $em = $this->getDoctrine()->getManager();
        $em->persist($tournamentMatch);
        $em->flush($tournamentMatch);
        
        /**        
        * @var \Symfony\Component\HttpKernel\Debug\TraceableEventDispatcher
        */
        $eventDispatcher = $this->get('event_dispatcher');
        $event = new TournamentEvent($round);
        $eventDispatcher->dispatch(TournamentEvents::ROUND_MATCH_ADDED, $event);
        
        $success = true;      
      } catch(\Exception $e) {
        $errorMsg = $e->getMessage();
      }
    }
    
    if($success)
      $responseData = array('success' => true);
    else
      $responseData = array('success' => false, 'msg' => $errorMsg);
    
    $response = new JsonResponse();
    $response->setData($responseData);
    
    return $response;
  }
  
  /**
  * Удаляет матч из турпира
  * 
  * Возвращает ответ в формате JSON
  * В случае успеха возвращает { success: true }
  * В случае ошибки возвращет { success: false, msg: error }
  * 
  * @param \AppBundle\Entity\Match $matchId Матч
  * @param int $tournamentId ID турнира  
  * 
  * @return JsonResponse
  */
  public function deleteMatchAction(\AppBundle\Entity\Match $match, $tournamentId)
  {
    $success = false;
    $errorMsg = 'Матч не удалён. Произошла непредвиденная ошибка.';
    
    /**    
    * @var \AppBundle\Entity\TournamentRoundMatchRepository
    */
    $roundMatchesRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentRoundMatch');
    
    /**    
    * @var \AppBundle\Entity\TournamentRoundRepository
    */
    $roundRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentRound');
    
    /**    
    * @var \AppBundle\Entity\TournamentRound
    */
    $round = $roundRep->findOneBy(array(
      "tournament" => $tournamentId
    ));
        
    /**    
    * @var \AppBundle\Entity\Tournament
    */
    $tournament = $round->getTournament();
    
    $tournamentMatchesCount = $roundMatchesRep->findMathesCountByTournament($tournament->getId());
    
    $tournamentMatch = $roundMatchesRep->findOneBy(array(
      "tournament" => $tournament->getId(),
      "match" => $match->getId()
      )
    );
    
    if(!$tournamentMatch)
    {
      $errorMsg = 'Данный матч не присутствует в турнире.';
    }
    elseif($tournament->getIsReady() && $tournamentMatchesCount <= 1)
    {
      $errorMsg = 'Нельзя удалить единственный матч в опубликованном турнире.';
    }
    else
    {
      try
      {
        $em = $this->getDoctrine()->getManager();
        $em->remove($tournamentMatch);
        $em->flush($tournamentMatch);
        
        /**        
        * @var \Symfony\Component\HttpKernel\Debug\TraceableEventDispatcher
        */
        $eventDispatcher = $this->get('event_dispatcher');
        $event = new TournamentEvent($round);
        $eventDispatcher->dispatch(TournamentEvents::ROUND_MATCH_DELETED, $event);
        
        $success = true;
      } catch(\Exception $e) {
        $errorMsg = $e->getMessage();
      }
    }
    
    if($success)
      $responseData = array('success' => true);
    else
      $responseData = array('success' => false, 'msg' => $errorMsg);
    
    $response = new JsonResponse();
    $response->setData($responseData);
    
    return $response;
  }
}
?>
