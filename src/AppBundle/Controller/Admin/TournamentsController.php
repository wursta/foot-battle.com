<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\Event;

use AppBundle\Entity\Tournament;
use AppBundle\Entity\Bet;

use AppBundle\Form\Type\TournamentType;

use AppBundle\Services\TournamentFactory;

use AppBundle\TournamentEvents;
use AppBundle\Event\TournamentEvent;

use AppBundle\TournamentUserEvents;
use AppBundle\Event\TournamentUserEvent;

class TournamentsController extends FBController
{
    /**
    * Выводит список неопубликованных турниров (Черновики)
    */
    public function draftAction()
    {
        $tournaments = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament')->findDraft();

        return $this->render('AppBundle:admin/tournaments:draft.html.twig', array(
                'tournaments' => $tournaments
            ));
    }

    /**
    * Выводит список опубликованных турниров
    */
    public function publishedAction()
    {
        //$tournaments = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament')->findPublished();

        return $this->render('AppBundle:admin/tournaments:published.html.twig', array(
            //'tournaments' => $tournaments
        ));
    }

    /**
    * Выводит список оконченных турниров
    */
    public function finishedAction()
    {
        $tournaments = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament')->findFinished();

        return $this->render('AppBundle:admin/tournaments:finished.html.twig', array(
            'tournaments' => $tournaments
        ));
    }

    /**
    * Возвращает список турниров по фильтру
    *
    * @param int     $page    Номер страницы.
    * @param Request $request Объект запроса.
    *
    * @return JsonResponse
    */
    public function searchAction($page, Request $request)
    {
        /** @var \AppBundle\Entity\TournamentRepository */
        $tournamentsRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament');

        $requestPayload = json_decode($request->getContent(), true);

        $rowsPerPage = 25;

        $arFilter = $requestPayload["filter"];
        $arOrder = $requestPayload["order"];

        /** @var \AppBundle\Entity\Tournament[] */
        $rowset = $tournamentsRep->findPaginator($arOrder, $arFilter, $page, $rowsPerPage);

        $totalItems = count($rowset);

        $tournaments = array();

        /** @var \AppBundle\Services\TournamentsHelper */
        $tournamentsHelper = $this->get('tournaments_helper');

        foreach($rowset as $tournament) {
            $tournaments[] = array(
                'id' => $tournament->getId(),
                'format' => array(
                    'internalName' => $tournament->getFormat()->getInternalName(),
                    'title' => $tournament->getFormat()->getTitle()
                ),
                'title' => $tournament->getTitle(),
                'currentRound' => $tournament->getCurrentRound(),
                'maxUsersCount' => $tournament->getMaxUsersCount(),
                'startDatetime' => $tournament->getStartDatetime()->format('c'),
                'isAccessible' => $tournament->getIsAccessible(),
                'isOpen' => $tournament->getIsOpen(),
                'withConfirmation' => $tournament->getWithConfirmation(),
                'fee' => $tournament->getFee(),
                'usersCount' => count($tournament->getUsers()),
                'userRequestsCount' => count($tournament->getUserRequests()),
                'author' => $tournament->getAuthor()->getNick(),
                'status' => $tournamentsHelper->getTournamentStatus($tournament)
            );
        }

        $response = new JsonResponse(array(
            "items" => $tournaments,
            "total" => $totalItems
        ));

        return $response;
    }

    /**
    * Выводит/Обрабатывает форму добавления турнира
    */
    public function addAction()
    {
        $request = $this->getRequest();

        $tournament = new Tournament();

        $form = $this->createForm(new TournamentType(), $tournament);

        $form->handleRequest($request);

        if($form->isValid())
        {
            /** @var \AppBundle\Services\PolicyHelper */
            $policyHelper = $this->get('policy_helper');
            $defaultRegulation = $policyHelper->getDefaultFormatRegulation(
                $this->container->getParameter('locale'),
                $tournament->getFormat()->getId()
            );

            $defaultPointsScheme = $policyHelper->getDefaultPointsScheme();

            $tournament->setRegulation($defaultRegulation);
            $tournament->setPointsScheme($defaultPointsScheme);
            $tournament->setAuthor($this->getUser());
            $tournament->setIsReady(false);
            $tournament->setUsersCountType('LIMIT');
            $tournament->setStep(2);

            $em = $this->getDoctrine()->getManager();
            $em->persist($tournament);
            $em->flush();

            return $this->redirect($this->generateUrl('admin/tournaments/'.$tournament->getFormat()->getInternalName().'/edit', array('tournament_id' => $tournament->getId())));
        }

        return $this->render('AppBundle:admin/tournaments:add.html.twig', array(
                'form' => $form->createView()
            )
        );
    }

    /**
    * Удаление турнира
    *
    * @param int $tournament_id
    *
    * @return \Symfony\Component\HttpFoundation\RedirectResponse
    */
    public function deleteAction($tournament_id)
    {
        $tournament = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament')->find($tournament_id);

        if(!$tournament)
            throw $this->createNotFoundException('Турнир не существует!');

        $em = $this->getDoctrine()->getManager();
        $em->remove($tournament);
        $em->flush();

        return $this->redirect($this->generateUrl('admin/tournaments/draft'));
    }

    /**
    * Добавление матча в турнир
    */
    public function addMatchAction()
    {
        $request = $this->getRequest();

        $matchId = $request->get('match_id', null);
        $tournamentId = $request->get('tournament_id', null);
        $roundNum = $request->get('round_num', null);
        $qualificationMatch = (bool) $request->get('qualification_match', false);
        $groupMatch = (bool) $request->get('group_match', false);

        if(!$matchId || !$tournamentId || !$roundNum)
          $this->createNotFoundException("Переданы не все обязательные параметры: match_id, tournament_id, round_num");

        /**
        * @var \AppBundle\Entity\MatchRepository
        */
        $matchesRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Match');
        $match = $matchesRep->find($matchId);

        if(!$match)
          $this->createNotFoundException("Матч не найден");

        /**
        * @var \AppBundle\Entity\TournamentRepository
        */
        $tournamentsRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament');

        $format = $tournamentsRep->getFormatByTournament($tournamentId);

        if(!$format)
            throw $this->createNotFoundException('Турнир не найден!');

        $format = ucfirst(strtolower($format));

        return $this->forward('AppBundle:Admin/Tournament/'.$format.":addMatch", array(
          'match' => $match,
          'tournamentId' => $tournamentId,
          'roundNum' => $roundNum,
          'qualificationMatch' => $qualificationMatch,
          'groupMatch' => $groupMatch
        ));
    }

    /**
    * Удаление матча из турнира
    */
    public function deleteMatchAction()
    {
      $request = $this->getRequest();

      $matchId = $request->get('match_id', null);
      $tournamentId = $request->get('tournament_id', null);

      if(!$matchId || !$tournamentId)
          $this->createNotFoundException("Переданы не все обязательные параметры: match_id, tournament_id");

      /**
      * @var \AppBundle\Entity\MatchRepository
      */
      $matchesRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Match');

      /**
      * @var \AppBundle\Entity\Match
      */
      $match = $matchesRep->find($matchId);

      if(!$match)
        $this->createNotFoundException("Матч не найден");

      /**
      * @var \AppBundle\Entity\TournamentRepository
      */
      $tournamentsRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament');

      $format = $tournamentsRep->getFormatByTournament($tournamentId);

      if(!$format)
          throw $this->createNotFoundException('Турнир не найден!');

      $format = ucfirst(strtolower($format));

      /**
      * @var \AppBundle\Entity\BetRepository
      */
      $betsRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Bet');

      $betsExists = (bool) count($betsRep->findBy(array(
        "tournament" => $tournamentId,
        "match" => $match->getId()
      )));

      if($betsExists)
      {
        $response = new JsonResponse();
        $response->setData(array(
          "success" => false,
          "msg" => 'Нельзя удалить данный матч, так как на него уже сделаны прогнозы участниками турнира.'
        ));

        return $response;
      }

      return $this->forward('AppBundle:Admin/Tournament/'.$format.":deleteMatch", array(
        'match' => $match,
        'tournamentId' => $tournamentId
      ));
    }

    /**
    * Выводит заявки пользователей на турнир
    *
    * @param int $tournament_id
    *
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function requestsAction($tournament_id)
    {
        $tournament = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament')->find($tournament_id);

        return $this->render('AppBundle:admin/tournaments:requests.html.twig', array(
            'tournament' => $tournament
        ));
    }

    /**
    * Выводит форму с поиском пользователей для приглашения в турнир
    *
    * @param int $tournament_id
    *
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function invitationsAction($tournament_id)
    {
        $tournament = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament')->find($tournament_id);

        return $this->render('AppBundle:admin/tournaments:invitations.html.twig', array(
            'tournament' => $tournament
        ));
    }

    /**
    * Обработка формы поиска пользователей для приглашения в турнир
    *
    * @param int $tournament_id
    *
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function searchUsersForInvitationAction($tournament_id)
    {
        $tournament = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament')->find($tournament_id);

        $request = $this->getRequest();
        $query = $request->get("query");

        $results = array();

        if(mb_strlen($query) > 3)
            $results = $this->getDoctrine()->getRepository('AppBundle\Entity\User')->search($query);

        return $this->render('AppBundle:admin/tournaments/invitations:search_results.html.twig', array(
            "tournament" => $tournament,
            "query" => $query,
            "results" => $results
          ));
    }

    /**
    * Приглашение пользователя в турнир
    *
    * @param int $tournament_id
    * @param int $user_id
    *
    * @return \Symfony\Component\HttpFoundation\RedirectResponse
    */
    public function inviteUserAction($tournament_id, $user_id)
    {
        $format = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament')->getFormatByTournament($tournament_id);

        if(!$format)
            throw $this->createNotFoundException('Туринир не найден!');

        $format = ucfirst(strtolower($format));

        $tournament = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament'.$format)->findLazy($tournament_id);

        if($tournament->getTournament()->getWithConfirmation())
        {
            /**
            * @todo Отправка приглашения на участие в турнире из админки.
            */
            exit('need to realise');
        }
        else
        {
            $user = $this->getDoctrine()->getRepository('AppBundle\Entity\User')->find($user_id);

            if(!$user)
                $this->createNotFoundException('Пользователь не существует!');

            $tournament->getTournament()->addUser($user);

            $em = $this->getDoctrine()->getManager();
            $em->persist($tournament->getTournament());
            $em->flush();

            $dispatcher = $this->get('event_dispatcher');
            $event = new TournamentEvent($tournament);
            $dispatcher->dispatch(TournamentEvents::getUserAddedEvent(strtolower($format)), $event);

            return $this->redirect($this->generateUrl('admin/tournaments/published'));
        }
    }

    public function requestConfirmAction($tournament_id, $user_id)
    {
        $format = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament')->getFormatByTournament($tournament_id);

        if(!$format)
            throw $this->createNotFoundException('Турнир не существует!');

        $tournamentRep = TournamentFactory::getRepository($format, $this->getDoctrine());
        $tournament = $tournamentRep->findLazy($tournament_id);

        $tournamentHelper = TournamentFactory::getService($format, $this->getDoctrine()->getManager(), $this->get("points_ruler"), $this->get("partner_program_helper"), $this->get("event_dispatcher"));

        $user = $this->getDoctrine()->getRepository('AppBundle\Entity\User')->find($user_id);

        if(!$tournamentHelper->isMaxUsersReached($tournament))
        {
            $tournament->getTournament()->addUser($user);
            $tournament->getTournament()->removeUserRequest($user);

            $em = $this->getDoctrine()->getManager();
            $em->persist($tournament->getTournament());
            $em->flush();

            $dispatcher = $this->get('event_dispatcher');
            $event = new TournamentEvent($tournament);
            $dispatcher->dispatch(TournamentEvents::getUserAddedEvent($format), $event);

            $event = new TournamentUserEvent($user, $tournament);
            $dispatcher->dispatch(TournamentUserEvents::TOURNAMENT_USER_ADDED, $event);
        }
        else
        {
            $this->getRequest()->getSession()->getFlashBag()->add('tournament_users_full', 'tournament_users_full');
        }

        if(count($tournament->getTournament()->getUserRequests()) == 0)
            return $this->redirect($this->generateUrl('admin/tournaments/published'));
        else
            return $this->redirect($this->generateUrl('admin/tournament/requests', array('tournament_id' => $tournament->getTournament()->getId())));
    }

    public function requestDismissAction($tournament_id, $user_id)
    {
        $format = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament')->getFormatByTournament($tournament_id);

        if(!$format)
            throw $this->createNotFoundException('Турнир не существует!');

        $tournamentRep = TournamentFactory::getRepository($format, $this->getDoctrine());
        $tournament = $tournamentRep->findLazy($tournament_id);

        $user = $this->getDoctrine()->getRepository('AppBundle\Entity\User')->find($user_id);

        if(!$user)
            throw $this->createNotFoundException('Пользователь не существует!');

        $tournament->getTournament()->removeUserRequest($user);

        $em = $this->getDoctrine()->getManager();
        $em->persist($tournament->getTournament());
        $em->flush();

        $dispatcher = $this->get('event_dispatcher');
        $event = new TournamentUserEvent($user, $tournament);
        $dispatcher->dispatch(TournamentUserEvents::TOURNAMENT_REQUEST_DISMISSED, $event);

        if(count($tournament->getTournament()->getUserRequests()) == 0)
            return $this->redirect($this->generateUrl('admin/tournaments/published', array('tournament_id' => $tournament->getTournament()->getId())));
        else
            return $this->redirect($this->generateUrl('admin/tournament/requests', array('tournament_id' => $tournament->getTournament()->getId())));
    }

    /**
    * Страница "Управление прогнозами участников"
    *
    * @param int $tournamentId Идентификатор турнира.
    *
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function betsManagementAction($tournamentId)
    {
        /** @var \AppBundle\Entity\TournamentRepository */
        $tournamentRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament');

        /** @var \AppBundle\Entity\Tournament */
        $tournament = $tournamentRep->find($tournamentId);

        if (!$tournament) {
            return $this->createNotFoundException('Турнир не найден.');
        }

        return $this->render(
            'AppBundle:admin/tournaments:bets_management.html.twig',
            array(
                'tournament' => $tournament
            )
        );
    }

    /**
    * Добавление пользователей в турнир по идентификаторам переданным в запросе.
    *
    * @param Request $request
    */
    public function addByIdsAction(Request $request)
    {
        $tournamentId = $this->getParam($request, 'tournamentId');
        $usersIds = $this->getParam($request, 'users');
                
        /** @var \AppBundle\Entity\UserRepository */
        $usersRep = $this->getDoctrine()->getRepository('AppBundle\Entity\User');
        $users = $usersRep->findBy(array(
            'id' => $usersIds
        ));

        /** @var \AppBundle\Entity\TournamentRepository */
        $tournamentsRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament');

        /** @var \AppBundle\Entity\Tournament */
        $tournament = $tournamentsRep->findOneById($tournamentId);

        if (!$tournament) {
            return $this->createNotFoundException('Турнир не найден.');
        }
               
        $availableUsersCount = $tournament->getMaxUsersCount() - count($tournament->getUsers());
        
        /** @var \Doctrine\ORM\EntityManager */
        $em = $this->getDoctrine()->getManager();

        $em->beginTransaction();
        
        try {
            foreach ($users as $user) {
                if ($availableUsersCount == 0) {
                    $notice = 'Некоторые боты не были добавлены,' .
                        'так как был превышен максимальный лимит участников.';
                    break;
                }
                
                $tournament->addUser($user);
                $availableUsersCount--;
            }
            
            $em->persist($tournament);
            $em->flush();
            $em->commit();
            return $this->returnJsonResponse(true, (isset($notice) ? $notice : array()));
        } catch (\Exception $e) {
            $em->rollback();
            return $this->returnErrorJsonResponse($e->getMessage());
        } 
    }
    
    /**
    * Возвращает список пользователей в турнире.
    *
    * @param int $tournamentId Идентификатор турнира
    * @param Request $request Объект запроса
    * 
    * @return JsonResponse
    */
    public function usersListAction($tournamentId, Request $request)
    {
        $filter = $this->getParam($request, 'filter', array());
        $order = $this->getParam($request, 'order', array());
        
        /** @var \AppBundle\Entity\UserRepository */
        $usersRep = $this->getDoctrine()->getRepository('AppBundle\Entity\User');

        /** @var \AppBundle\Entity\User[] */
        $rowset = $usersRep->findByTournament($tournamentId, $filter, $order);
        
        $users = array();
        foreach ($rowset as $user) {
            $users[] = array(
                'id' => $user->getId(),
                'nick' => $user->getNick(),
                'avatar' => $user->getAvatar()
            );
        }
        
        
        return $this->returnJsonResponse(true, array('users' => $users));
    }
    
    /**
    * Возвращает список матчей в турнире.
    *
    * @param int $tournamentId Идентификатор турнира.
    * @param Request $request Объект запроса.
    * 
    * @return JsonResponse
    */
    public function matchesListAction($tournamentId, Request $request)
    {
        $filter = $this->getParam($request, 'filter', array());
        
        /** @var \AppBundle\Entity\MatchRepository */
        $matchesRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Match');
        
        /** @var \AppBundle\Entity\Match[] */
        $rowset = $matchesRep->findByTournament($tournamentId, $filter);
        
        /** @var \AppBundle\Services\MatchesHelper */
        $matchesHelper = $this->get('matches_helper');
        
        $matches = array();        
        foreach ($rowset as $match) {
            $matches[] = array(
                "id" => $match->getId(),
                "leagueTitle" => $match->getLeague()->getLeagueName(),
                "startDatetime" => $matchesHelper->getMatchStartDatetime(
                    $match->getMatchDate(),
                    $match->getMatchTime()
                )->format('c'),
                "homeTeam" => $match->getHomeTeam()->getTitle(),
                "guestTeam" => $match->getGuestTeam()->getTitle()                
            );
        }
        
        return $this->returnJsonResponse(true, array('matches' => $matches));
    }
    
    /**
    * Возвращает список прогнозов пользователя в турнире.
    *
    * @param int $tournamentId Идентификатор турнира.
    * @param int $userId Идентификатор пользователя.
    * @param Request $request Объект запроса.
    * 
    * @return JsonResponse
    */
    public function userBetsListAction($tournamentId, $userId, Request $request)
    {
        $filter = $this->getParam($request, 'filter', array());
        
        /** @var \AppBundle\Entity\BetRepository */
        $betsRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Bet');
        
        /** @var \AppBundle\Entity\Bet[] */
        $rowset = $betsRep->findByTournamentAndUser($tournamentId, $userId);
        
        $bets = array();
        foreach ($rowset as $bet) {
            $bets[$bet->getMatch()->getId()] = array(
                'id' => $bet->getId(),                
                'scoreLeft' => $bet->getScoreLeft(),
                'scoreRight' => $bet->getScoreRight()
            );
        }
        
        return $this->returnJsonResponse(true, array('bets' => $bets));
    }
    
    public function saveUserBetAction($tournamentId, $userId, $matchId, Request $request)
    {
        try {
            $scoreLeft = $this->getParam($request, 'scoreLeft', 0);
            $scoreRight = $this->getParam($request, 'scoreRight', 0);
            
            /** @var \AppBundle\Entity\BetRepository */
            $betsRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Bet');
            
            $bet = $betsRep->findByTournamentMatchAndUser($tournamentId, $userId, $matchId);
            if (!$bet) {
                /** @var \AppBundle\Entity\TournamentRepository */
                $tournamentRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament');
                /** @var \AppBundle\Entity\UserRepository */
                $usersRep = $this->getDoctrine()->getRepository('AppBundle\Entity\User');
                /** @var \AppBundle\Entity\MatchRepository */
                $matchesRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Match');            
                
                $tournament = $tournamentRep->find($tournamentId);
                $user = $usersRep->find($userId);
                $match = $matchesRep->find($matchId);
                
                $bet = new Bet();
                $bet->setTournament($tournament);
                $bet->setUser($user);
                $bet->setMatch($match);            
            }
            
            $bet->setScoreLeft($scoreLeft);
            $bet->setScoreRight($scoreRight);
            
            /** @var \Doctrine\ORM\EntityManager */
            $em = $this->getDoctrine()->getManager();
            $em->persist($bet);
            $em->flush();
            
            return $this->returnJsonResponse(true);
        } catch (\Exception $e) {
            return $this->returnErrorJsonResponse($e->getMessage());
        }        
    }
}