<?php
namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\Form\FormError;
use AppBundle\Admin\Form\Type\UserType;
use AppBundle\Admin\Form\Model\Balance;
use AppBundle\Admin\Form\Type\BalanceType;
use AppBundle\Entity\User;
use AppBundle\Entity\BillingOperation;

class UsersController extends FBController
{
    /**
    * Страница "Пользователи"
    */
    public function indexAction()
    {
        return $this->render('AppBundle:admin/users:index.html.twig');
    }

    /**
    * Поиск ботов которых нет в тунрире.
    *
    * @param int $tournamentId Идентификатор турнира.
    *
    * @return JsonResponse
    */
    public function findBotsNotInTournamentAction($tournamentId, Request $request)
    {
        /** @var \AppBundle\Entity\UserRepository */
        $usersRep = $this->getDoctrine()->getRepository('AppBundle\Entity\User');

        /** @var \AppBundle\Entity\User[] */
        $rowset = $usersRep->findBotsNotInTournament($tournamentId);

        $users = array();
        foreach($rowset as $user) {
            $users[] = array(
                "id" => $user->getId(),
                "email" => $user->getEmail(),
                "nick" => $user->getNick(),
                "balance" => $user->getBalance(),
                "reserved" => $user->getReserved(),
                "freeFunds" => $user->getFreeFunds(),
                "isActive" => $user->getIsActive(),
                "isConfirmed" => $user->getIsConfirmed(),
                "registrationDate" => $user->getRegistrationDate()
            );
        }

        return $this->returnJsonResponse(true, array('bots' => $users));
    }

    /**
    * Возвращает список пользователь постранично в json.
    *
    * @param int $page Номер страницы.
    *
    * @return JsonResponse
    */
    public function listAction($page)
    {
        $request = $this->getRequest();

        $requestPayload = json_decode($request->getContent(), true);

        $rowsPerPage = 20;

        $arFilter = $requestPayload["filter"];
        $arOrder = $requestPayload["order"];

        /** @var \AppBundle\Entity\UserRepository */
        $usersRep = $this->getDoctrine()->getRepository('AppBundle\Entity\User');

        /** @var \AppBundle\Services\UserHelper */
        $userHelper = $this->get('user_helper');
        /** @var \AppBundle\Services\TimezoneHelper */
        $timezoneHelper = $this->get('timezone_helper');

        if(!empty($arFilter["registrationDate"]["startDate"]) && !empty($arFilter["registrationDate"]["endDate"])) {
            $startDate = new \DateTime($arFilter["registrationDate"]["startDate"]);
            $startDate->setTimezone($timezoneHelper->getDefaultTimezone());

            $endDate = new \DateTime($arFilter["registrationDate"]["endDate"]);
            $endDate->setTimezone($timezoneHelper->getDefaultTimezone());

            $arFilter[">=:registration_date"] = $startDate;
            $arFilter["<=:registration_date"] = $endDate;
        }
        unset($arFilter["registrationDate"]);

        $rowset = $usersRep->findPaginatorByFilter($arOrder, $arFilter, $page, $rowsPerPage);

        $totalItems = count($rowset);

        $users = array();

        foreach($rowset as $user) {
            $referralUser = array();

            if ($user->getReferralUser()) {
                $referralUser = array(
                    "id" => $user->getReferralUser()->getId(),
                    "nick" => $user->getReferralUser()->getNick()
                );
            }

            $users[] = array(
                "id" => $user->getId(),
                "email" => $user->getEmail(),
                "nick" => $user->getNick(),
                "role" => $user->getRole(),
                "balance" => $user->getBalance(),
                "reserved" => $user->getReserved(),
                "freeFunds" => $user->getFreeFunds(),
                "isActive" => $user->getIsActive(),
                "isConfirmed" => $user->getIsConfirmed(),
                "registrationDate" => $user->getRegistrationDate(),
                "referralUser" => $referralUser
            );
        }

        $response = new JsonResponse(array(
            "items" => $users,
            "total" => $totalItems
        ));

        return $response;
    }

    /**
    * Форма добавления пользователя.
    */
    public function addAction()
    {
        $user = new User();

        $form = $this->createForm(new UserType($this->get('locale_helper'), $this->get('role_helper')), $user);

        $request = $this->getRequest();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $encoder = $this->get('security.password_encoder');
            $user->setPassword( $encoder->encodePassword($user, $user->getPassword()) );

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $user->setAvatarFile($form->get('avatar_file')->getData());

            /** @var \AppBundle\Services\UserHelper */
            $userHelper = $this->get('user_helper');
            $user->setReferralCode($userHelper->generateReferralCode($user->getId()));

            $user->uploadAvatar();
            $em->persist($user);
            $em->flush();

            return $this->redirect($this->generateUrl('admin/users/index'));
        }

        return $this->render('AppBundle:admin/users:add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
    * Форма редактирования пользователя.
    *
    * @param int $user_id ID пользователя.
    */
    public function editAction($user_id)
    {
        $user = $this->getDoctrine()->getRepository('AppBundle\Entity\User')->find($user_id);

        $form = $this->createForm(
            new UserType($this->get('locale_helper'), $this->get('role_helper'), UserType::SCENARIO_UPDATE),
            $user
        );

        $request = $this->getRequest();

        $form->handleRequest($request);

        if ($form->isValid()) {

            if ($form->get("avatar_delete")->getData()) {
                if (is_file($user->getAbsolutePath())) {
                    unlink($user->getAbsolutePath());
                }

                $user->setAvatar(null);
            }

            $user->uploadAvatar();
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirect($this->generateUrl('admin/users/index'));
        }

        return $this->render('AppBundle:admin/users:edit.html.twig', array(
            'form' => $form->createView(),
            'user' => $user
        ));
    }

    /**
    * Управление счётом пользователя
    *
    * @param int $user_id ID пользователя
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function balanceAction($user_id)
    {
        /**
        * @var \AppBundle\Entity\UserRepository
        */
        $userRep = $this->getDoctrine()->getRepository('AppBundle\Entity\User');

        /**
        * @var \AppBundle\Entity\User
        */
        $user = $userRep->find($user_id);

        if(!$user)
            throw $this->createNotFoundException('Пользователь не найден!');

        /**
        * @var \AppBundle\Services\BalanceHelper
        */
        $BalanceHelper = $this->get("balance_helper");

        /**
        * @var \AppBundle\Entity\BillingOperationRepository
        */
        $BillingOperationRep = $this->getDoctrine()->getRepository('AppBundle\Entity\BillingOperation');

        $arOrder = array("operation_datetime" => "ASC");
        $arFilter = array("user" => $user->getId(), "status" => $BalanceHelper::STATUS_IN_PROGRESS, "type" => $BalanceHelper::TYPE_REQUEST);
        $cashoutRequestsCnt = count($BillingOperationRep->findBy($arFilter, $arOrder));

        $balance = new Balance();

        $form = $this->createForm(new BalanceType(), $balance);

        $request = $this->getRequest();

        $form->handleRequest($request);

        /**
        * @var \AppBundle\Services\BalanceHelper
        */
        $BalanceHelper = $this->get('balance_helper');
        $systemUser = $BalanceHelper->getSystemUser();
        $systemBalance = $BalanceHelper->getSystemBalance();

        $success = false;

        if ($form->isValid()) {
            try
            {
                $actionClicked = null;

                if($form->get('deposit')->isClicked())
                    $actionClicked = $BalanceHelper::ACTION_DEPOSIT;
                elseif($form->get('withdraw')->isClicked())
                    $actionClicked = $BalanceHelper::ACTION_WITHDRAW;
                elseif($form->get('cashout')->isClicked())
                    $actionClicked = $BalanceHelper::ACTION_CASHOUT;

                if(!$actionClicked)
                {
                    throw new \Exception("Данное действие не поддерживается");
                    exit;
                }

                $amount = $balance->getAmount();

                //Закомментировано, так как уход в минус стал возможен
                /*if($actionClicked == $BalanceHelper::ACTION_DEPOSIT && ($systemBalance - $amount) < 0)
                {
                    throw new \Exception("На счету системы недостаточно средств для зачисления");
                    exit;
                }*/

                if($actionClicked == $BalanceHelper::ACTION_WITHDRAW && ($user->getBalance() - $amount) < 0)
                {
                    throw new \Exception("На счету пользователя недостаточно средств для списания");
                    exit;
                }

                if($actionClicked == $BalanceHelper::ACTION_DEPOSIT)
                {
                    $userFrom = $systemUser;
                    $userRecipient = $user;
                }
                elseif($actionClicked == $BalanceHelper::ACTION_WITHDRAW || $actionClicked == $BalanceHelper::ACTION_CASHOUT)
                {
                    $userFrom = $user;
                    $userRecipient = $systemUser;
                }

                //Все операции являются переводом средств на счёт системы
                //Но только если ввод(DEPOSIT) или вывод(WITHDRAW, CASHOUT) совершается не самой системой
                if(!$BalanceHelper->isSystem($user))
                {
                    $BillingOperation = $BalanceHelper->createTransferBill($userFrom,
                                                                           $userRecipient,
                                                                           $amount,
                                                                           $balance->getNote(),
                                                                           array("user_ip" => $request->getClientIp(), "user_agent" => $request->headers->get("User-Agent"))
                                                                          );

                    $BalanceHelper->processBill($BillingOperation);
                }

                //Если ввод совершается системой(из наличных средств)
                if($BalanceHelper->isSystem($user) && $actionClicked == $BalanceHelper::ACTION_DEPOSIT)
                {
                    $BillingOperation = $BalanceHelper->createSystemDepositBill($userFrom,
                                                                                $amount,
                                                                                $balance->getNote(),
                                                                                array("user_ip" => $request->getClientIp(),
                                                                                      "user_agent" => $request->headers->get("User-Agent"))
                                                                               );

                    $BalanceHelper->processBill($BillingOperation);
                }

                //Если операция ручного вывода, то после перевода на счёт Системы необходимо списать средства со счёта системы
                if($actionClicked == $BalanceHelper::ACTION_CASHOUT)
                {
                    $commissionPercent = 0;
                    $CashoutOperation = $BalanceHelper->createCashoutBill($systemUser,
                                                                          $amount,
                                                                          $commissionPercent,
                                                                          $balance->getNote(),
                                                                          array("user_ip" => $request->getClientIp(), "user_agent" => $request->headers->get("User-Agent"))
                                                                         );

                    $BalanceHelper->reservMoneyForCashout($systemUser, $amount);

                    $BalanceHelper->processBill($CashoutOperation);
                }

                $this->addFlash('transaction_success', 'Операция успешно выполнена!');
                $success = true;
            }
            catch(\Exception $ex)
            {
                $form->get('amount')->addError(new FormError($ex->getMessage()));
            }
        }

        if($success)
            return $this->redirect($this->generateUrl('admin/users/balance', array("user_id" => $user->getId())));

        return $this->render('AppBundle:admin/users:balance.html.twig', array(
            'cashoutRequestsCnt' => $cashoutRequestsCnt,
            'user' => $user,
            'systemUser' => $systemUser,
            'systemBalance' => $systemBalance,
            'form' => $form->createView()
        ));
    }

    /**
    * Заявки на вывод средств пользователем.
    *
    * @param int $user_id ID пользователя.
    *
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function cashoutRequestsAction($user_id)
    {
      /**
      * @var \AppBundle\Entity\UserRepository
      */
      $userRep = $this->getDoctrine()->getRepository("AppBundle\Entity\User");

      $user = $userRep->findById($user_id);

      /**
      * @var \AppBundle\Services\BalanceHelper
      */
      $BalanceHelper = $this->get("balance_helper");

      /**
      * @var \AppBundle\Entity\BillingOperationRepository
      */
      $BillingOperationRep = $this->getDoctrine()->getRepository('AppBundle\Entity\BillingOperation');

      $arOrder = array("operation_datetime" => "ASC");
      $arFilter = array("user" => $user->getId(), "status" => $BalanceHelper::STATUS_IN_PROGRESS, "type" => $BalanceHelper::TYPE_REQUEST);
      $cashoutRequests = $BillingOperationRep->findBy($arFilter, $arOrder);

      return $this->render('AppBundle:admin/users:cashout_requests.html.twig', array(
          'cashoutRequestsCnt' => count($cashoutRequests),
          'cashoutRequests' => $cashoutRequests,
          'user' => $user,
      ));
    }

    /**
    * Отклонение заявки на вывод
    *
    * @param int $bill_id ID счёта
    */
    public function cashoutRequestCancelAction($bill_id)
    {
      /**
      * @var \AppBundle\Services\BalanceHelper
      */
      $BalanceHelper = $this->get("balance_helper");

      /**
      * @var \AppBundle\Entity\BillingOperationRepository
      */
      $BillingOperationRep = $this->getDoctrine()->getRepository('AppBundle\Entity\BillingOperation');

      /**
      * @var \AppBundle\Entity\BillingOperation
      */
      $bill = $BillingOperationRep->find($bill_id);

      if(!$bill)
        $this->createNotFoundException("Счёт не найден");

      if($bill->getStatus() != $BalanceHelper::STATUS_IN_PROGRESS)
        $this->createNotFoundException("Статус данного счёта не позволяет его обработать повторно.");

      $user = $bill->getUser();

      $BalanceHelper->cancelBill($bill);

      $this->addFlash('success', 'Запрос на вывод успешно отклонён. Заблокированные для вывода средства возвращены на игровой счёт, а также на счёт для вывода пользователя.');
      return $this->redirect($this->generateUrl('admin/users/cashout_requests', array("user_id" => $user->getId())));
    }

    /**
    * Одобрение заявки на вывод
    *
    * @param int $bill_id ID счёта
    */
    public function cashoutRequestConfirmAction($bill_id)
    {
      /**
      * @var \AppBundle\Services\BalanceHelper
      */
      $BalanceHelper = $this->get("balance_helper");

      /**
      * @var \AppBundle\Entity\BillingOperationRepository
      */
      $BillingOperationRep = $this->getDoctrine()->getRepository('AppBundle\Entity\BillingOperation');

      /**
      * @var \AppBundle\Entity\BillingOperation
      */
      $bill = $BillingOperationRep->find($bill_id);

      if(!$bill)
        $this->createNotFoundException("Счёт не найден");

      if($bill->getStatus() != $BalanceHelper::STATUS_IN_PROGRESS)
        $this->createNotFoundException("Статус данного счёта не позволяет его обработать повторно.");

      $user = $bill->getUser();

      $BalanceHelper->processBill($bill);
      $this->addFlash('success', 'Запрос на вывод успешно одобрен. Заблокированные для вывода средства успешно списаны.');

      return $this->redirect($this->generateUrl('admin/users/cashout_requests', array("user_id" => $user->getId())));
    }
}