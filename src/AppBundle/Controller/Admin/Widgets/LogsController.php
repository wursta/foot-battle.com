<?php
namespace AppBundle\Controller\Admin\Widgets;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class LogsController extends Controller
{    
    public function cronLogTimechartAction()
    {    
        /**        
        * @var $cronLogsRep \AppBundle\Entity\Logs\CronLogRepository
        */
        $cronLogsRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Logs\CronLog');
        
        $fromDatetime = new \DateTime();
        $fromDatetime = $fromDatetime->createFromFormat('Y-m-d H:i:s', date('Y-m-d', time() - 7*24*60*60)." 00:00:00");
        
        /**
        * @var $result \AppBundle\Entity\Logs\CronLog[]
        */
        $result = $cronLogsRep->findOneWeek($fromDatetime, new \DateTime);
        $series = array();                        
        $commandsDescriptions = array();
        foreach($result as $log)
        {
            $commandsDescriptions[$log->getCommand()] = $log->getCommandDescription();
            $series[$log->getCommand()][] = $log;
        }
        return $this->render('AppBundle:admin/widgets/logs:cronlog_timechart.html.twig', array(
            'series' => $series,
            'commands_descriptions' => $commandsDescriptions
        ));
    }                
    
    public function cronLogMemoryUsageAction()
    {    
        /**        
        * @var $cronLogsRep \AppBundle\Entity\Logs\CronLogRepository
        */
        $cronLogsRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Logs\CronLog');                
        
        /**
        * @var $result \AppBundle\Entity\Logs\CronLog[]
        */
        $result = $cronLogsRep->getAverageMemoryUsage();
        
        return $this->render('AppBundle:admin/widgets/logs:cronlog_memory_usage.html.twig', array(
            'results' => $result            
        ));
    }
}