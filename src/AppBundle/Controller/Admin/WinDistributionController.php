<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\Common\Collections\ArrayCollection;

use AppBundle\Entity\WinRulesGroup;
use AppBundle\Entity\WinRule;
use AppBundle\Admin\Form\Type\WinRulesGroupType;
use AppBundle\Admin\Form\Type\WinRuleType;

class WinDistributionController extends Controller
{
    /**
    * Страница Распределение призовых
    */
    public function indexAction()
    {
        $formatList = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentFormat')->findAll();

        return $this->render('AppBundle:admin/win_distribution:index.html.twig', array(
            'formatList' => $formatList
        ));
    }

    /**
    * Список распределений по формату
    *
    * @param int $format_id Идентификатор формата турниров.
    *
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function rulesAction($format_id)
    {
        $rulesGroupsList = $this->getDoctrine()->getRepository('AppBundle\Entity\WinRulesGroup')->findByFormat($format_id);

        return $this->render('AppBundle:admin/win_distribution:rules_list.html.twig', array(
            'rulesGroupsList' => $rulesGroupsList
        ));
    }

    /**
    * Добавление распределения призовых
    *
    * @param mixed $group_id Идентификатор группы распределения
    *
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function addGroupAction()
    {
        $winRulesGroup = new WinRulesGroup();
        $form = $this->createForm(new WinRulesGroupType(), $winRulesGroup);

        $request = $this->getRequest();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            foreach ($winRulesGroup->getRules() as $rule) {
                $rule->setGroup($winRulesGroup);
            }

            $em->persist($winRulesGroup);
            $em->flush();

            return $this->redirect($this->generateUrl('admin/win_distribution/index'));
        }

        return $this->render('AppBundle:admin/win_distribution:edit_group.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
    * Редактирование распределения призовых
    *
    * @param mixed $group_id Идентификатор группы распределения
    *
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function editGroupAction($group_id)
    {
        /** @var \AppBundle\Entity\WinRulesGroupRepository */
        $winRulesGroupRep = $this->getDoctrine()->getRepository('AppBundle\Entity\WinRulesGroup');

        /** @var \AppBundle\Entity\WinRulesGroup */
        $winRulesGroup = $winRulesGroupRep->findById($group_id);

        $form = $this->createForm(new WinRulesGroupType(), $winRulesGroup);

        $originalRules = new ArrayCollection();
        foreach ($winRulesGroup->getRules() as $rule) {
            $originalRules->add($rule);
        }

        $request = $this->getRequest();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            foreach ($winRulesGroup->getRules() as $rule) {
                $rule->setGroup($winRulesGroup);
            }

            foreach ($originalRules as $rule) {
                if ($winRulesGroup->getRules()->contains($rule) === false) {
                    $winRulesGroup->removeRule($rule);
                    $em->persist($winRulesGroup);
                    $em->remove($rule);
                }
            }

            $em->persist($winRulesGroup);
            $em->flush();

            return $this->redirect($this->generateUrl('admin/win_distribution/index'));
        }

        return $this->render('AppBundle:admin/win_distribution:edit_group.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}