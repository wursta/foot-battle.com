<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class BillingOperationsController extends Controller
{
    public function monthLogAction($page = 1)
    {
        /**
        * @var \AppBundle\Entity\User
        */
        $currenUser = $this->getUser();

        $rowsPerPage = 5;

        //месяц назад
        $start = \DateTime::createFromFormat("U", time() - 1*30*24*3600)->setTime(0, 0, 0);
        $end = new \DateTime();

        /**
        * @var \AppBundle\Entity\BillingOperationRepository
        */
        $BillingOperationsRep = $this->getDoctrine()->getRepository("AppBundle\Entity\BillingOperation");

        /**
        * @var \AppBundle\Entity\BillingOperation[]
        */
        $result = $BillingOperationsRep->findPaginatorForIntervalByUser($currenUser->getId(), $start, $end, $page, $rowsPerPage);

        $total = count($result);
        $items = array();

        foreach($result as $row)
        {
          $userRecipient = null;

          if($row->getUserRecipient())
          {
            $userRecipient = array(
              "id" => $row->getUserRecipient()->getId(),
              "nick" => $row->getUserRecipient()->getNick()
            );
          }

          $items[] = array(
            "operationDatetime" => $row->getOperationDatetime()->format('c'),
            "action" => $row->getAction(),
            "status" => $row->getStatus(),
            "user" => array(
              "id" => $row->getUser()->getId(),
              "nick" => $row->getUser()->getNick(),
            ),
            "userRecipient" => $userRecipient,
            "currency" => $row->getCurrency(),
            "amountWithComission" => $row->getAmountWithCommision(),
            "note" => $row->getNote()
          );
        }

        $response = new JsonResponse();
        $response->setData(array(
          "total" => $total,
          "items" => $items
        ));

        return $response;
    }
}