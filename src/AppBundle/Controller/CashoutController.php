<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;

use AppBundle\Services\DOLHelper;
use AppBundle\Services\BalanceHelper;
use AppBundle\Entity\BillingOperation;
use AppBundle\Form\Model\CashoutRequest;
use AppBundle\Form\Type\CashoutRequestType;
use AppBundle\Form\Model\CashoutQiwi;
use AppBundle\Form\Type\CashoutQiwiType;

class CashoutController extends Controller
{
    public function indexAction($_locale)
    {
      $qiwi_commission_percent = DOLHelper::GSG_COMMISSION_QIWI_PERCENT;
      $qiwi_min_cashout = DOLHelper::GSG_QIWI_MIN_CASHOUT;
      $qiwi_max_cashout = DOLHelper::GSG_QIWI_MAX_CASHOUT;
      
      return $this->render('AppBundle:frontend/personal:cashout.html.twig', array(
        'qiwi_commission_percent' => $qiwi_commission_percent,
        'qiwi_min_cashout' => $qiwi_min_cashout,
        'qiwi_max_cashout' => $qiwi_max_cashout,
      ));
    }
    
    public function requestAction($_locale)
    {
      /**
      * @var \AppBundle\Services\BalanceHelper
      */
      $BalanceHelper = $this->get("balance_helper");
            
      /**        
      * @var \AppBundle\Entity\User
      */
      $user = $this->getUser();
      
      $freeFunds = $user->getFreeFunds();
      $moneyRate = $BalanceHelper::MONEY_RATE;
      $commission = $BalanceHelper::COMMISSION_CASHOUT_MANUAL_REQUEST;
      $minCashout = $BalanceHelper::MIN_CASHOUT_MANUAL_REQUEST;
      
      $cashoutRequest = new CashoutRequest();
      $form = $this->createForm(new CashoutRequestType(), $cashoutRequest);
      
      $request = $this->getRequest();
      
      $form->handleRequest($request);
                  
      if ($form->isValid()) {
        $cashoutRequest->setCardNumber(preg_replace("/\s/", "", $cashoutRequest->getCardNumber()));
        $cashoutRequest->setAmount(round((float) $cashoutRequest->getAmount(), 2));
        
        try
        {
          /**        
          * @var \Symfony\Bundle\FrameworkBundle\Translation\Translator
          */
          $translator = $this->get("translator");
          
          if($cashoutRequest->getAmount() < $minCashout)
          {
            $form->get("amount")->addError(new FormError($translator->trans("Минимальное количество для обмена составляет %s% золотых монет.", array("%s%" => $minCashout), $_locale)));          
            throw new \Exception('Validation error');
          }
          
          if(!$BalanceHelper->firstCashoutAtDay($user, new \DateTime()))
          {          
            $this->addFlash('error', $translator->trans("Обмен золотых монет доступен 1 раз в сутки.", array(), $_locale));
            return $this->redirect($this->generateUrl("personal/balance"));
          }
          
          if(!$BalanceHelper->cashoutIsAvailable($user, $cashoutRequest->getAmount(), $moneyRate))
          {
            $form->get("amount")->addError(new FormError($translator->trans("На вашем счету недостаточно золотых монет для обмена", array(), $_locale)));
            throw new \Exception('Validation error');
          }          
          $operationId = $BalanceHelper->generateOperationId($user->getId());
        
          $note = $BalanceHelper->getRequestCashoutNote($cashoutRequest->getAmount());
          
          /**                
          * @var BillingOperation
          */
          $bill = $BalanceHelper->createCashoutBill($user, $cashoutRequest->getAmount(), $commission, $note, array(
              "operation_id" => $operationId,
              "type" => BalanceHelper::TYPE_REQUEST,
              "paysystem" => BalanceHelper::PAYSYSTEM_INTERNAL,
              "user_ip" => $request->getClientIp(),
              "user_agent" => $request->headers->get("User-Agent")
          ));
          
          if($bill)
          {
            $BalanceHelper->reservMoneyForCashout($user, $cashoutRequest->getAmount());
            
            /**          
            * @var \AppBundle\Services\MailerHelper
            */
            $mailerHelper = $this->get("mailer_helper");
            $mailerHelper->sendCashoutRequestMail($bill, $cashoutRequest);
            
            $this->addFlash('info', $translator->trans("Заявка на вывод средств сформирована. Запрашиваемое для обмена количество золотых монет заблокировано. Ожидайте перевода в течение 5 рабочих дней."));
            return $this->redirect($this->generateUrl("personal/balance"));
          }
          
        } catch(\Exception $ex)
        {
          //nothing to do
        }                
      }
      
      return $this->render('AppBundle:frontend/personal/cashout:request.html.twig', array(
        'form' => $form->createView(),
        'freeFunds' => $freeFunds,
        'moneyRate' => $moneyRate,
        'commission' => $commission,
        'minCashout' => $minCashout
      ));
    }
    
    public function requestConfirmAction($_locale)
    {
      /**
      * @var \AppBundle\Services\BalanceHelper
      */
      $BalanceHelper = $this->get("balance_helper");
            
      /**        
      * @var \AppBundle\Entity\User
      */
      $user = $this->getUser();
      
      $freeFunds = $user->getFreeFunds();
      $moneyRate = $BalanceHelper::MONEY_RATE;
      $commission = $BalanceHelper::COMMISSION_CASHOUT_MANUAL_REQUEST;
      $minCashout = $BalanceHelper::MIN_CASHOUT_MANUAL_REQUEST;
      
      $cashoutRequest = new CashoutRequest();
      $form = $this->createForm(new CashoutRequestType(), $cashoutRequest);
      
      $request = $this->getRequest();
      
      $form->handleRequest($request);
                  
      if ($form->isValid()) {
        $cashoutRequest->setCardNumber(preg_replace("/\s/", "", $cashoutRequest->getCardNumber()));
        $cashoutRequest->setAmount(round((float) $cashoutRequest->getAmount(), 2));
        
        $commissionPercent = $BalanceHelper::COMMISSION_CASHOUT_MANUAL_REQUEST;        
        $commissionReal = $BalanceHelper->getCommissionForAmount($cashoutRequest->getAmount(), $commissionPercent);
        $amountWithCommission = $cashoutRequest->getAmount() - $commissionReal;
        
      }
      
      return $this->render('AppBundle:frontend/personal/cashout:request_confirm.html.twig', array(
        'cashoutRequest' => $cashoutRequest,        
        'commissionPercent' => $commissionPercent,
        'commissionReal' => $commissionReal,
        'amountWithCommission' => $amountWithCommission
      ));
    }
    
    public function qiwiAction($_locale)
    {
        /**
        * @var \AppBundle\Services\DOLHelper
        */
        $DOLHelper = $this->get('dol_helper');
        
        /**
        * @var \AppBundle\Services\BalanceHelper
        */
        $BalanceHelper = $this->get("balance_helper");
        
        /**                
        * @var \Symfony\Component\Translation\LoggingTranslator
        */
        $translator = $this->get("translator");
        
        if(!$systemBalance = $DOLHelper->checkCashoutAbility())
        {
            return $this->render('AppBundle:frontend/personal/cashout:error.html.twig');
        }
        
        /**
        * TODO: Необходимо сделать проверку на вывод средств 1 раз в сутки
        */
                
        /**        
        * @var \AppBundle\Entity\User
        */
        $user = $this->getUser();        
                
        $freeFunds = $user->getFreeFunds();
        $moneyRate = $BalanceHelper::MONEY_RATE;
        $commissionPercent = $DOLHelper::GSG_COMMISSION_QIWI_PERCENT;
        $minCashout = $DOLHelper::GSG_QIWI_MIN_CASHOUT;
        $maxCashout = $DOLHelper::GSG_QIWI_MAX_CASHOUT;
        
        $cashoutQiwi = new CashoutQiwi();
        $form = $this->createForm(new CashoutQiwiType(), $cashoutQiwi);
        
        $request = $this->getRequest();
        
        $form->handleRequest($request);

        if ($form->isValid()) {
            /**                
            * @var \AppBundle\Services\BalanceHelper
            */
            $BalanceHelper = $this->get("balance_helper");
                        
            $commissionPercent = $DOLHelper::GSG_COMMISSION_QIWI_PERCENT;
            
            $amount = round((float) $cashoutQiwi->getAmount(), 2);
            $commissionReal = $BalanceHelper->getCommissionForAmount($amount, $commissionPercent);
            $amountWithCommission = $amount - $commissionReal;
            
            try{
                if($amount < $minCashout)
                {
                    throw new \Exception($translator->trans("Минимальная сумма для вывода составляет %s% руб.", array("%s%" => $minCashout)));
                }
                
                if($amount > $maxCashout)
                {
                    throw new \Exception($translator->trans("Максимальная сумма для вывода составляет %s% руб.", array("%s%" => $maxCashout)));
                }
                      
                if(!$BalanceHelper->cashoutIsAvailable($user, $amount, $moneyRate))
                {
                    throw new \Exception($translator->trans("На вашем счету недостаточно средств для вывода %s% руб.", array("%s%" => $amount)));
                }
                
                
                if(($systemBalance - $amount) < 0)
                {
                    throw new \Exception($translator->trans("К сожалению в данный момент вывод средств невозможен по техническим причинам. Проблема уже решается нашими техническими специалистами и будет устранена в ближайшее время."));
                    
                    
                    //Необходимо послать уведомление на email администрации о том, что на системном счёте не хватает средств для вывода                    
                }            
                
                $operationId = $BalanceHelper->generateOperationId($user->getId());
                
                $checkData = $DOLHelper->gsgCheck($cashoutQiwi->getQiwiPhone(), $amountWithCommission, $operationId);
                
                $note = $BalanceHelper->getQIWICashoutNote($cashoutQiwi->getQiwiPhone(), $amount);
                /**                
                * @var BillingOperation
                */            
                $billOperation = $BalanceHelper->createCashoutBill($user, $amount, $commissionPercent, $note, array(
                    "operation_id" => $operationId,
                    "type" => BalanceHelper::TYPE_QIWI,
                    "paysystem" => BalanceHelper::PAYSYSTEM_DOL,                    
                    "user_ip" => $request->getClientIp(),
                    "user_agent" => $request->headers->get("User-Agent")
                ));
                
                $payData = $DOLHelper->gsgPay($checkData["invoice"], $operationId, $amountWithCommission, $note);
                                
                if($payData["status"] == DOLHelper::STATUS_OK)
                    $BalanceHelper->processBill($bill, $payData);
                
                return $this->redirect($this->generateUrl("personal/balance"));                
            } catch(\Exception $ex) {
                $this->addFlash("error", $ex->getMessage());
            }
        }
        
        return $this->render('AppBundle:frontend/personal/cashout:qiwi.html.twig', array(
            "form" => $form->createView(),
            "free_funds" => $freeFunds,
            "commission" => $commissionPercent,
            "min_cashout" => $minCashout,
            "max_cashout" => $maxCashout,
            "money_rate" => $moneyRate
        ));
    }
    
    public function qiwiConfirmAction($_locale)
    {
        /**
        * @var \AppBundle\Services\DOLHelper
        */
        $DOLHelper = $this->get('dol_helper');
            
        $cashoutQiwi = new CashoutQiwi();
        $form = $this->createForm(new CashoutQiwiType(), $cashoutQiwi);
        
        $request = $this->getRequest();
        
        $form->handleRequest($request);

        if ($form->isValid()) {                        
            /**                
            * @var \AppBundle\Services\BalanceHelper
            */
            $BalanceHelper = $this->get("balance_helper");
                        
            $commissionPercent = $DOLHelper::GSG_COMMISSION_QIWI_PERCENT;
            
            $amount = round((float) $cashoutQiwi->getAmount(), 2);
            $commissionReal = $BalanceHelper->getCommissionForAmount($amount, $commissionPercent);
            $amountWithCommission = $amount - $commissionReal;
        }
        else
        {
            return $this->render('AppBundle:frontend/personal/cashout:qiwi_confirm_error.html.twig');
        }
                
        return $this->render('AppBundle:frontend/personal/cashout:qiwi_confirm.html.twig', array(
            'qiwiPhone' => $cashoutQiwi->getQiwiPhone(),
            'amount' => $amount,
            'commissionPercent' => $commissionPercent,
            'commissionReal' => $commissionReal,
            'amountWithCommission' => $amountWithCommission
        ));
    }
    
    public function qiwi2Action($_locale)
    {        
        /**
        * @var \AppBundle\Services\DOLHelper
        */
        $DOLHelper = $this->get('dol_helper');
        
        /**
        * @var \AppBundle\Services\BalanceHelper
        */
        $BalanceHelper = $this->get("balance_helper");
        
        /**                
        * @var \Symfony\Component\Translation\LoggingTranslator
        */
        $translator = $this->get("translator");                
        
        /*if(!$systemBalance = $DOLHelper->checkCashoutAbility())
        {
            return $this->render('AppBundle:frontend/personal/cashout:error.html.twig');
        }*/
        
        /**
        * TODO: Необходимо сделать проверку на вывод средств 1 раз в сутки
        */
                
        /**        
        * @var \AppBundle\Entity\User
        */
        $user = $this->getUser();
                
        $freeFunds = $user->getFreeFunds();
        $commissionPercent = $DOLHelper::GSG_COMMISSION_CARDS_PERCENT;
        $moneyRate = $BalanceHelper::MONEY_RATE;
        $minDeposit = $DOLHelper::QIWI_MIN_DEPOSIT;        
        
        $cashoutCard = new CashoutCard();
        $form = $this->createForm(new CashoutCardType());
        
        $request = $this->getRequest();
        
        $form->handleRequest($request);

        if ($form->isValid()) {
            
            $formData = $form->getData();            
            
            $formData["card_number"] = trim(str_replace(" ", "", $formData["card_number"]));
            
            try{
                /**                
                * @var \AppBundle\Services\BalanceHelper
                */
                $BalanceHelper = $this->get("balance_helper");

                $amount = round((float) $formData["amount"], 2);
                $commissionReal = round((float) $BalanceHelper->getCommissionForAmount($amount, $commissionPercent), 2);
                $amountWithCommission = round((float) $amount - $commissionReal, 2);
                
                if($amount < $minCashout)
                {
                    throw new \Exception($translator->trans("Минимальная сумма для вывода составляет %s% руб.", array("%s%" => $minCashout)));
                }
                
                if($amount > $maxCashout)
                {
                    throw new \Exception($translator->trans("Максимальная сумма для вывода составляет %s% руб.", array("%s%" => $maxCashout)));
                }
                      
                if(!$BalanceHelper->cashoutIsAvailable($user, $amount, $moneyRate))
                {
                    throw new \Exception($translator->trans("На вашем счету недостаточно средств для вывода %s% руб.", array("%s%" => $amount)));
                }
                
                if(($systemBalance - $amount) < 0)
                {
                    throw new \Exception($translator->trans("К сожалению в данный момент вывод средств невозможен по техническим причинам. Проблема уже решается нашими техническими специалистами и будет устранена в ближайшее время."));
                    
                    /**
                    * TODO: Необходимо послать уведомление на email администрации о том, что на системном счёте не хватает средств для вывода
                    */
                }
                
                $operationId = $BalanceHelper->generateOperationId($this->getUser()->getId());
                
                $note = $translator->trans(BalanceHelper::BILLING_OPERATION_NOTE_CASHOUT_CREDIT_CARD);
                
                $checkData = $DOLHelper->check(array(
                    "paysystem" => $DOLConfig["cashout"]["cards"]["paysystem_id"],
                    "account" => $formData["card_number"],
                    "amount" => $amountWithCommission,
                    "txn_id" => $operationId
                ));
                
                /**                
                * @var BillingOperation
                */                
                $bill = $BalanceHelper->createBill($this->getUser(), array(
                    "id" => $operationId,                    
                    "action" => BalanceHelper::ACTION_CASHOUT,                    
                    "type" => BalanceHelper::TYPE_CARD,                    
                    "money_rate" => $moneyRate,
                    "paysystem" => "DOL",
                    "amount" => $amount,
                    "amount_with_commision" => $amountWithCommission,
                    "commission_percent" => $commissionPercent,
                    "currency" => $DOLHelper->getDefaultCurrency(),
                    "note" => $note,
                    "status" => BalanceHelper::STATUS_IN_PROGRESS,
                    "user_ip" => $request->getClientIp(),
                    "user_agent" => $request->headers->get("User-Agent")
                ));
                
                $payData = $DOLHelper->pay(array(
                    "invoice" => $checkData["invoice"],
                    "txn_id" => $operationId,
                    "paysystem" => $DOLConfig["cashout"]["cards"]["paysystem_id"],
                    "account" => $formData["card_number"],
                    "amount" => $amountWithCommission
                ));
                
                if($payData["status"] == DOLHelper::STATUS_OK)
                    $BalanceHelper->processBill($bill, $payData);
                
                return $this->redirect($this->generateUrl("personal/balance"));
            } catch(\Exception $ex) {
                $this->addFlash("error", $ex->getMessage());
            }                        
        }
        
        return $this->render('AppBundle:frontend/personal/cashout:card.html.twig', array(
            "form" => $form->createView(),
            "free_funds" => $freeFunds,
            "commission" => $commissionPercent,
            "min_cashout" => $minCashout,
            "money_rate" => $moneyRate
        ));
    }
}
