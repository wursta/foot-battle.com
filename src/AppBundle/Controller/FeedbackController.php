<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Form\Model\Feedback;
use AppBundle\Form\Type\FeedbackType;

class FeedbackController extends Controller
{    
    public function indexAction(Request $request)
    {        
        $feedback = new Feedback();
        
        $form = $this->createForm(new FeedbackType(), $feedback);
                
        $form->handleRequest($request);                

        if ($form->isValid()) {
            
            /**            
            * @var $mailerHelper \AppBundle\Services\MailerHelper
            */
            $mailerHelper = $this->get('mailer_helper');
                        
            $feedback->setCreated(new \DateTime());
            $feedback->setIpAddress($request->getClientIp());
            $feedback->setMessage(strip_tags(nl2br($feedback->getMessage()), "<br><br/>"));
            
            $mailerHelper->sendFeedbackMail($feedback);
                        
            $feedback = new Feedback();
            $form = $this->createForm(new FeedbackType(), $feedback);
            
            $this->addFlash('feedback_sended', 'feedback_sended');
        }
                
        return $this->render('AppBundle:frontend:feedback_form.html.twig', array(
            'form' => $form->createView(),
        ));
    }                
}
