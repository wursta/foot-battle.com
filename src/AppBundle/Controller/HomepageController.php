<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Services\TournamentFactory;

class HomepageController extends Controller
{
    /**
    * Лэндинг пейдж.
    * Если пользователь авторизован, то перенаправляем его на поиск турниров
    *
    * @param string $_locale Текущая локаль.
    *
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function indexAction($_locale)
    {
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('tournaments/search');
        }

        return $this->render('AppBundle:frontend:homepage.html.twig');
    }

    public function brandbookAction()
    {
      return $this->render('AppBundle:frontend:brandbook.html.twig');
    }
}
