<?php

namespace AppBundle\Controller\Html;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Services\TournamentFactory;

class HtmlController extends Controller
{    
    public function showSetBetFormAction()
    {            
        $request = $this->getRequest();
        
        if(!$request->get('match_id'))
            throw $this->createNotFoundException('Матч не найден!');
            
        $match_id = $request->get('match_id');
        $match = $this->getDoctrine()->getRepository('AppBundle\Entity\Match')->find($match_id);
        
        if(!$match)
            throw $this->createNotFoundException('Матч не найден!');
        
        $bet = null;
        $currentUser = $this->get('security.context')->getToken()->getUser();
        
        if(is_a($currentUser, '\AppBundle\Entity\User') && $request->get('tournament_id'))
        {            
            $bet = $this->getDoctrine()->getRepository('AppBundle\Entity\Bet')->findByTournamentMatchAndUser($request->get('tournament_id'), $currentUser, $match_id);
        }                
        
        return $this->render('html/bet/set_form.html.twig', array(
          "match" => $match,
          "bet" => $bet
        ));
    }
    
    public function tournamentMatchBetsDataAction()
    {
        $request = $this->getRequest();
        
        $match_id = $request->get('match_id');
        $tournament_id = $request->get('tournament_id');
        
        if(!$match_id || !$tournament_id)
            throw $this->createNotFoundException('Не заданы параметры!');
        
        $format = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament')->getFormatByTournament($tournament_id);
        if(!$format)
            throw $this->createNotFoundException('Туринир не найден!');

        $tournamentRep = TournamentFactory::getRepository($format, $this->getDoctrine());        
        $tournament = $tournamentRep->findLazy($tournament_id);        
        
        if(!$tournament)
            throw $this->createNotFoundException('Туринир не найден!');            
        
        $match = $this->getDoctrine()->getRepository('AppBundle\Entity\Match')->find($match_id);                
        
        if(!$match)
            throw $this->createNotFoundException('Матч не найден!');
        
        $currentUser = $this->get('security.context')->getToken()->getUser();                                    
        
        if(!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
        {
            $tournaments_helper = $this->get('tournaments_helper');
            
            if(!$tournaments_helper->isUserInTournament($tournament, $currentUser))
                throw $this->createNotFoundException('Вы не участвуете в данном турнире!');
                            
            if(!$tournaments_helper->userCanViewUsersBetsOnMatch($match, $currentUser))
                throw $this->createNotFoundException('Вы не можете просмотреть ставки других пользователей на данный матч!');
        }
        
        $bets = $this->getDoctrine()->getRepository('AppBundle\Entity\Bet')->findByTournamentAndMatch($tournament_id, $match_id);
        
        return $this->render('html/bet/users_bets_table.html.twig', array("bets" => $bets, "match" => $match));
    }
    
    public function teamsLastMatchesAction()
    {
        $request = $this->getRequest();
        
        $leftTeamId = $request->get('leftTeam');
        $rightTeamId = $request->get('rightTeam');
        
        $teamRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Team');

        $leftTeam = $teamRep->find($leftTeamId);
        $rightTeam = $teamRep->find($rightTeamId);

        if(!$leftTeam || !$rightTeam)
            throw $this->createNotFoundException('Одна из команд не найдена!');

        $matchesRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Match');

        $leftTeamLastMatches = $matchesRep->findLastMatches($leftTeam->getId(), 5);
        $rightTeamLastMatches = $matchesRep->findLastMatches($rightTeam->getId(), 5);

        $matchesBetweenTeams = $matchesRep->findMatchesBetweenTeams($leftTeam->getId(), $rightTeam->getId(), 3);

        return $this->render('html/teams/last_matches.html.twig', array(
                'leftTeam' => $leftTeam,
                'rightTeam' => $rightTeam,
                'leftTeamLastMatches' => $leftTeamLastMatches,
                'rightTeamLastMatches' => $rightTeamLastMatches,
                'matchesBetweenTeams' => $matchesBetweenTeams
            )
        );
    }
}
