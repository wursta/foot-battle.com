<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class NewsController extends Controller
{
    public function recentAction($count = 3)
    {
        $newsList = $this->getDoctrine()->getRepository('AppBundle\Entity\News')->findActive($count);        

        return $this->render('AppBundle:frontend/news:recent.html.twig', array(
                'newsList' => $newsList
            )
        );
    }

    public function detailAction($news_id)
    {
        $news = $this->getDoctrine()->getRepository('AppBundle\Entity\News')->find($news_id);

        if(!$news)
            throw $this->createNotFoundException('Новость не найдена!');

        return $this->render('AppBundle:frontend/news:detail.html.twig', array(
                'news' => $news
            )
        );
    }
}