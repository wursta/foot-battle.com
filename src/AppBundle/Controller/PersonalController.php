<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Form\Type\UserProfileType;
use AppBundle\Form\Type\ChangePasswordType;
use AppBundle\Form\Type\ChangeEmailType;
use AppBundle\Entity\User;

class PersonalController extends Controller
{
    /**
    * Раздел "Мои турниры"
    *
    * @param string $_locale Текущая локаль.
    *
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function tournamentsAction($_locale)
    {
        return $this->render('AppBundle:frontend/personal:tournaments.html.twig');
    }

    /**
    * Поиск турниров текущего авторизованного пользователя
    *
    * @param string $_locale Текущая локаль.
    *
    * @return JsonResponse
    */
    public function tournamentsSearchAction($_locale)
    {
        /**
        * Текущий пользователь
        *
        * @var User
        */
        $user = $this->getUser();

        $request = $this->getRequest();
        $payload = json_decode($request->getContent(), true);

        $page = (isset($payload["page"])) ? $payload["page"] : 1;
        $filter = (isset($payload["filter"])) ? $payload["filter"] : array();
        $order = (isset($payload["order"])) ? $payload["order"] : array();
        $options = (isset($payload["options"])) ? $payload["options"] : array("pageSize" => 10);

        $filter["user_id"] = $user->getId();

        /** @var \AppBundle\Entity\TournamentRepository */
        $tournamentsRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament');

        /** @var \AppBundle\Entity\Tournament[] */
        $resultSet = $tournamentsRep->findPaginator($order, $filter, $page, $options["pageSize"]);

        $totalCount = count($resultSet);
        $tournaments = array();

        /** @var \Symfony\Bundle\FrameworkBundle\Translation\Translator */
        $translator = $this->get("translator");

        /** @var \AppBundle\Services\TournamentsHelper */
        $tournamentsHelper = $this->get("tournaments_helper");

        foreach($resultSet as $tournament)
        {            
            $tournaments[] = array(
                "id" => $tournament->getId(),
                "url" => $this->generateUrl("tournament/view", array( "_locale" => $_locale, "tournament_id" =>  $tournament->getId())),
                "title" => $tournament->getTitle(),
                "fee" => $tournament->getFee(),
                "format" => $translator->trans($tournament->getFormat()->getTitle(), array(), null, $_locale),
                "startDatetime" => $tournament->getStartDatetime()->format("c"),
                "usersCount" => count($tournament->getUsers()),
                "prizePool" => $tournamentsHelper->getTournamentPrizePool($tournament),
                "isPromo" => $tournament->getIsPromo(),
                "status" => $tournamentsHelper->getTournamentStatus($tournament)
            );
        }

        $response = new JsonResponse();
        $response->setData(array(
            "total" => $totalCount,
            "items" => $tournaments
        ));

        return $response;
    }

    /**
    * Профиль пользователя
    * Обработка формы редактирования профиля
    *
    * @param string $_locale Текущая локаль
    *
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function profileAction($_locale)
    {
        /**
        * @var \AppBundle\Entity\User
        */
        $user = $this->getUser();

        $form = $this->createForm(new UserProfileType($user));

        $request = $this->getRequest();

        $form->handleRequest($request);

        /**
        * @var \AppBundle\Services\UserHelper
        */
        $userHelper = $this->get("user_helper");

        /**
        * @var \Symfony\Bundle\FrameworkBundle\Translation\Translator
        */
        $translator = $this->get("translator");

        /**
        * @var \AppBundle\Entity\UserRepository
        */
        $userRep = $this->getDoctrine()->getRepository("AppBundle\Entity\User");

        if($form->isSubmitted())
        {
            if(!$userHelper->isUniqueUsername($form->get('nick')->getData(), $user->getId()))
            {
                $errorMsg = $translator->trans("Пользователь с таким никнеймом уже зарегистрирован на сайте.", array(), null, $_locale);
                $form->get("nick")->addError(new FormError($errorMsg));
            }

            if(!$userHelper->isValidUsername($form->get('nick')->getData()))
            {
                $errorMsg = $translator->trans("Никнейм может содержать только латинские / кириллические символы, цифры и подчёркивания. Длина никнейма не может превышать %username_maxlength% символов.", array("%username_maxlength%" => $userHelper::USERNAME_MAXLENGTH), null, $_locale);
                $form->get("nick")->addError(new FormError($errorMsg));
            }
        }

        if ($form->isValid())
        {
            if($form->get("avatar_delete")->getData())
            {
                if(is_file($user->getAbsolutePath()))
                    unlink($user->getAbsolutePath());

                $user->setAvatar(null);
            }

            if($form->get("avatar_file")->getData())
            {
                $user->setAvatarFile($form->get("avatar_file")->getData());
                $user->uploadAvatar();
            }

            $user->setNick($form->get("nick")->getData());
            $user->setFirstName($form->get("first_name")->getData());
            $user->setLastName($form->get("last_name")->getData());
            $user->setSecondName($form->get("second_name")->getData());
            $user->setCountry($form->get("country")->getData());
            $user->setCity($form->get("city")->getData());
            $user->setBirthdate($form->get("birthdate")->getData());

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $request->getSession()->getFlashBag()->add('profile_saved', 'profile_saved');
            return $this->redirect($this->generateUrl('personal/profile'));
        }

        return $this->render('AppBundle:frontend/personal:profile.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

    /**
    * Настройки пользователя
    *
    * @param string $_locale Текущая локаль
    *
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function settingsAction($_locale)
    {
        $user = $this->get('security.context')->getToken()->getUser();

        $changePasswordForm = $this->createForm(new ChangePasswordType(), $user);
        $changeEmailForm = $this->createForm(new ChangeEmailType(), $user);

        $request = $this->getRequest();

        $changePasswordForm->handleRequest($request);
        if ($changePasswordForm->isValid()) {
            $encoder = $this->get('security.password_encoder');
            $user->setPassword( $encoder->encodePassword($user, $user->getPassword()) );

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $request->getSession()->getFlashBag()->add('password_changed', 'password_changed');

            return $this->redirect($this->generateUrl('personal/settings'));
        }

        $changeEmailForm->handleRequest($request);
        if ($changeEmailForm->isValid()) {
            $user->setUsername($user->getEmail());
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $request->getSession()->getFlashBag()->add('email_changed', 'email_changed');

            return $this->redirect($this->generateUrl('personal/settings'));
        }

        return $this->render('AppBundle:frontend/personal:settings.html.twig', array(
            'changePasswordForm' => $changePasswordForm->createView(),
            'changeEmailForm'    => $changeEmailForm->createView(),
        ));
    }

    /**
    * Управление счётом
    *
    * @param string $_locale Текущая локаль
    *
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function balanceAction($_locale)
    {
        /**
        * @var \AppBundle\Services\BalanceHelper
        */
        $BalanceHelper = $this->get("balance_helper");

        $requestCommissionPercent = $BalanceHelper::COMMISSION_CASHOUT_MANUAL_REQUEST;
        $cashoutMinPercent = min(array($requestCommissionPercent));

        return $this->render('AppBundle:frontend/personal:balance.html.twig', array(
            'cashoutMinPercent' => $cashoutMinPercent
        ));
    }

    /**
    * Вывод главной старницы "Партнёрская программа"
    *
    * @param string $_locale Текущая локаль
    */
    public function partnerAction($_locale)
    {
        /**
        * @var \AppBundle\Entity\User
        */
        $currentUser = $this->getUser();

        $urlParams = array("_locale" => $currentUser->getLocale());
        $referralRegistrationUrl = $this->generateUrl(
            "homepage",
            $urlParams,
            \Symfony\Component\Routing\Generator\UrlGeneratorInterface::ABSOLUTE_URL
        );

        $referralRegistrationUrl .= '#' . $currentUser->getReferralCode();

        return $this->render('AppBundle:frontend/personal:partner.html.twig', array(
            "referralRegistrationUrl" => $referralRegistrationUrl,
        ));
    }

    /**
    * Выводит "лог" реферальных вознаграждений с паджинацией
    *
    * @param int $page
    */
    public function partnerChargesLogAction($page = 1)
    {
        /**
        * @var \AppBundle\Entity\User
        */
        $currentUser = $this->getUser();

        $rowsPerPage = 10;

        /**
        * @var \AppBundle\Entity\PartnerCharge
        */
        $partnerChargesRep = $this->getDoctrine()->getRepository('AppBundle\Entity\PartnerCharge');

        $result = $partnerChargesRep->findPartnerChargesByUser($currentUser->getId(), $page, $rowsPerPage);

        $totalPages = ceil(count($result)/$rowsPerPage);

        return $this->render('AppBundle:frontend/personal/partner:charges_log.html.twig', array(
            "partnerCharges" => $result,
            "current_page" => $page,
            "totalPages" => $totalPages
        ));
    }
}
