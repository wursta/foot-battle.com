<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PoliciesController extends Controller
{
    public function termsAction($_locale)
    {
        $policy = $this->getPolicy($_locale);
        return $this->render('AppBundle:frontend/policies:terms.html.twig', array(
            'policy' => $policy
        ));
    }

    public function regulationsAction($_locale)
    {
        $policy = $this->getPolicy($_locale);
        return $this->render('AppBundle:frontend/policies:regulations.html.twig', array(
            'policy' => $policy
        ));
    }

    public function privacyAction($_locale)
    {
        $policy = $this->getPolicy($_locale);
        return $this->render('AppBundle:frontend/policies:privacy.html.twig', array(
            'policy' => $policy
        ));
    }

    private function getPolicy($locale)
    {
        /** @var \AppBundle\Services\PolicyHelper */
        $policyHelper = $this->get('policy_helper');

        $policy = $policyHelper->getDefault($locale);

        $policyHelper->render($policy);

        return $policy;
    }
}
