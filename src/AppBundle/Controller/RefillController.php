<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Services\BalanceHelper;
use AppBundle\Entity\BillingOperation;
use AppBundle\Form\Model\RefillQiwi;
use AppBundle\Form\Type\RefillQiwiType;

class RefillController extends Controller
{
    /**
    * Успешное пополнение счёта
    *
    * @param string $_locale Текущая локаль
    *
    * @return \Symfony\Component\HttpFoundation\RedirectResponse
    */
    public function successAction($_locale)
    {
        $request = $this->getRequest();

        $paymentId = $request->get('paymentid', false);

        if(!$paymentId) {
            return $this->redirect($this->generateUrl('personal/balance'));
        }

        /** @var \AppBundle\Entity\BillingOperationRepository */
        $billingOperationsRep = $this->getDoctrine()->getRepository('AppBundle\Entity\BillingOperation');

        /** @var \AppBundle\Entity\BillingOperation */
        $bill = $billingOperationsRep->findByPaymentId($paymentId);

        if(!$bill) {
            return $this->redirect($this->generateUrl('personal/balance'));
        }

        /** @var \Symfony\Component\Translation\LoggingTranslator */
        $translator = $this->get("translator");

        if($bill->getStatus() == BalanceHelper::STATUS_OK) {
            $message = $translator->trans(
                'Операция по покупке золотых монет успешно выполнена. Ваш игровой счёт пополнен!',
                array(),
                null,
                $_locale
            );
        } elseif($bill->getStatus() == BalanceHelper::STATUS_IN_PROGRESS) {
            $message = $translator->trans(
                'Операция по покупке золотых монет отправлена в обработку.
                Ожидайте пополнения игрового счёта в ближайшее время!',
                array(),
                null,
                $_locale
            );
        }

        $this->addFlash('refillSuccess', $message);

        return $this->redirect($this->generateUrl('personal/balance'));
    }

    public function failAction()
    {
        $this->addFlash('refillFail', 'refillFail');

        return $this->redirect($this->generateUrl('personal/balance'));
    }

    /**
    * Форма пополнения через Qiwi-кошелёк.
    *
    * Обработка формы:
    *
    * @param string $_locale Текущая локаль
    *
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function qiwiAction($_locale)
    {
        /** @var \AppBundle\Services\DOLHelper */
        $DOLHelper = $this->get("dol_helper");

        /** @var \AppBundle\Services\BalanceHelper */
        $BalanceHelper = $this->get("balance_helper");

        $commissionPercent = $DOLHelper::COMMISSION_QIWI_PERCENT;
        $moneyRate = $BalanceHelper::MONEY_RATE;
        $minDeposit = $DOLHelper::MIN_DEPOSIT_QIWI;

        $refillQiwi = new RefillQiwi();
        $form = $this->createForm(new RefillQiwiType(), $refillQiwi);

        return $this->render('AppBundle:frontend/billing/refill/qiwi:modal.html.twig', array(
            "form" => $form->createView(),
            'money_rate' => $moneyRate,
            'commission' => $commissionPercent,
            'min_deposit' => $minDeposit
        ));
    }

    /**
    * Валидация данных для пополнения счёта через Qiwi-кошелёк
    * В случае успеха возвращает информацию по пополнению.
    *
    * @param string $_locale Текущая локаль.
    *
    * @return JsonResponse
    */
    public function qiwiValidateAction($_locale)
    {
        /** @var \AppBundle\Services\DOLHelper */
        $DOLHelper = $this->get("dol_helper");

        /** @var \AppBundle\Services\BalanceHelper */
        $BalanceHelper = $this->get("balance_helper");

        /** @var \Symfony\Component\Translation\LoggingTranslator */
        $translator = $this->get("translator");

        $request = $this->getRequest();

        $qiwiPhone = $request->get('qiwiPhone', '');
        $amount = $BalanceHelper->getAmount($request->get('amount', 0));
        $currencySign = $this->container->getParameter('currency_sign');

        $response = new JsonResponse();

        if (!$DOLHelper->isQiwiPhoneValid($qiwiPhone)) {
            $response->setData(array(
                "success" => false,
                "error" => $translator->trans('Номер QIWI-кошелька не валиден!', array(), null, $_locale)
            ));
            return $response;
        }

        if(!$DOLHelper->greaterThanQiwiMinDeposit($amount)) {
            $response->setData(array(
                "success" => false,
                "error" => $translator->trans('Минимальная сумма покупки ЗМ составляет %s% %currency_sign%', array("%s%" => $DOLHelper::MIN_DEPOSIT_QIWI, "%currency_sign%" => $currencySign), null, $_locale)
            ));
            return $response;
        }

        $commissionPercent = $DOLHelper::COMMISSION_QIWI_PERCENT;
        $commissionReal = $BalanceHelper->getCommissionForAmount($amount, $commissionPercent);
        $amountWithCommission = $amount + $commissionReal;
        $moneyAmount = $BalanceHelper->getMoneyFromAmount($amount);

        $response->setData(array(
            'success' => true,
            'data' => array(
                'qiwiPhone' => $qiwiPhone,
                'amount' => $amount,
                'commissionPercent' => $commissionPercent,
                'commissionReal' => $commissionReal,
                'amountWithCommission' => $amountWithCommission,
                'moneyAmount' => $moneyAmount
            )
        ));

        return $response;
    }


    public function qiwiSubmitAction($_locale)
    {
        /** @var \AppBundle\Services\DOLHelper */
        $DOLHelper = $this->get("dol_helper");

        /** @var \AppBundle\Services\BalanceHelper */
        $BalanceHelper = $this->get("balance_helper");

        /** @var \Symfony\Component\Translation\LoggingTranslator */
        $translator = $this->get("translator");

        $request = $this->getRequest();

        $qiwiPhone = $request->get('qiwiPhone', '');
        $amount = $BalanceHelper->getAmount($request->get('amount', 0));

        $response = new JsonResponse();

        if (!$DOLHelper->isQiwiPhoneValid($qiwiPhone) || !$DOLHelper->greaterThanQiwiMinDeposit($amount)) {
            $response->setData(array(
                "success" => false,
                "error" => $translator->trans('Произошла ошибка', array(), null, $_locale)
            ));
            return $response;
        }

        $commissionPercent = $DOLHelper::COMMISSION_QIWI_PERCENT;
        $moneyRate = $BalanceHelper::MONEY_RATE;

        try {
            $commissionReal = $BalanceHelper->getCommissionForAmount($amount, $commissionPercent);
            $amountWithCommission = $amount + $commissionReal;
            $moneyAmount = $BalanceHelper->getMoneyFromAmount($amount);

            /**
            * @var \AppBundle\Entity\User
            */
            $currentUser = $this->getUser();

            $operationId = $BalanceHelper->generateOperationId($currentUser->getId());

            $billingOperation = new BillingOperation();
            $billingOperation->setHash($operationId);
            $billingOperation->setUser($currentUser);
            $billingOperation->setAction($BalanceHelper::ACTION_DEPOSIT);
            $billingOperation->setType($BalanceHelper::TYPE_QIWI);
            $billingOperation->setPaysystem($BalanceHelper::PAYSYSTEM_DOL);
            $billingOperation->setMoneyRate($moneyRate);
            $billingOperation->setAmount($amount);
            $billingOperation->setAmountWithCommision($amountWithCommission);
            $billingOperation->setCommissionPercent($commissionPercent);
            $billingOperation->setCurrency($DOLHelper->getDefaultCurrency());
            $billingOperation->setOperationDatetime(new \DateTime());
            $billingOperation->setNote($BalanceHelper->getQIWIOperationNote($qiwiPhone, $moneyAmount));
            $billingOperation->setStatus($BalanceHelper::STATUS_IN_PROGRESS);
            $billingOperation->setUserIp($request->getClientIp());
            $billingOperation->setUserAgent($request->headers->get("User-Agent"));

            $DOLResponse = $DOLHelper->createBillForQIWI($billingOperation, $qiwiPhone);

            $billingOperation->setPaysystemInternalId($DOLResponse["ODpaymentID"]);

            $em = $this->getDoctrine()->getManager();
            $em->persist($billingOperation);
            $em->flush($billingOperation);

            $response->setData(array(
                "success" => true,
                "data" => array(
                    "iframeUrl" => $DOLResponse["iframeUrl"]
                )
            ));

            return $response;

        } catch(\Exception $e) {
            //отменяем операцию в случае ошибки
            $BalanceHelper->cancelBill($billingOperation);

            $response->setData(array(
                "success" => false,
                "error" => $e->getMessage()
            ));
            return $response;
        }
    }
}