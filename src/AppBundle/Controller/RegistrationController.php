<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\ExpressionLanguage\Expression;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;

use \AppBundle\Form\Type\RegistrationType;
use \AppBundle\Form\Model\Registration;

class RegistrationController extends Controller
{
    /**
    * Вывод формы регистрации
    *
    * @param string $_locale Текущая локаль
    *
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function formAction($_locale)
    {
        $registration = new Registration();
        $form = $this->createForm(new RegistrationType(), $registration);

        return $this->render('AppBundle:frontend/registration:modal.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
    * Сабмит формы регистрации в iFrame
    *
    * @param string $_locale Текущая локаль
    *
    * @return JsonResponse
    */
    public function submitAction($_locale)
    {
        $registration = new Registration();
        $form = $this->createForm(new RegistrationType(), $registration);

        $request = $this->getRequest();

        $form->handleRequest($request);

        if(!$form->isSubmitted())
            exit;

        $response = new JsonResponse();

        /**
        * @var \AppBundle\Services\UserHelper
        */
        $userHelper = $this->get("user_helper");

        /**
        * @var \Symfony\Bundle\FrameworkBundle\Translation\Translator
        */
        $translator = $this->get("translator");

        $errors = array();

        if($form->isSubmitted())
        {
          if(!$userHelper->isUniqueUsername($registration->getUser()->getNick()))
          {
            $errorMsg = $translator->trans("Пользователь с таким никнеймом уже зарегистрирован на сайте.", array(), null, $_locale);
            $form->get("user")->get("username")->addError(new FormError($errorMsg));
            $errors[] = $errorMsg;
          }

          if(!$userHelper->isValidUsername($registration->getUser()->getNick()))
          {
            $errorMsg = $translator->trans("Никнейм может содержать только латинские / кириллические символы, цифры и подчёркивания. Длина никнейма не может превышать %username_maxlength% символов.", array("%username_maxlength%" => $userHelper::USERNAME_MAXLENGTH), null, $_locale);
            $form->get("user")->get("username")->addError(new FormError($errorMsg));
            $errors[] = $errorMsg;
          }
        }

        if ($form->isValid()) {

            $now = new \DateTime();

            $user = $registration->getUser();

            if($registration->getReferralCode()) {
                $referralUser = $userHelper->getUserByReferralCode($registration->getReferralCode());
                $registration->getUser()->setReferralUser($referralUser);
            }

            $user->setRole("ROLE_USER");
            $user->setLocale($request->getLocale());
            $user->setRegistrationDate($now);

            $encoder = $this->get('security.password_encoder');
            $user->setPassword( $encoder->encodePassword($user, $user->getPassword()) );

            $em = $this->getDoctrine()->getManager();

            $em->getConnection()->beginTransaction();

            try {
                $em->persist($user);
                $em->flush();

                $user->setReferralCode( $userHelper->generateReferralCode($user->getId()) );
                $em->persist($user);
                $em->flush();
                $em->getConnection()->commit();
            } catch (\Exception $e) {
                $em->getConnection()->rollback();
                $em->close();
                throw $e;
            }


            $userHelper = $this->get('user_helper');
            $userHelper->sendConfirmationEmailToUser($user);

            $response->setData(array(
                "success" => true
            ));

        } else {
            $response->setData(array(
                "success" => false,
                "errors" => $errors
            ));
        }

        //Так как ответ приходит в iframe, то чтобы не было ошибки возвращаем его в виде текста.
        $response->headers->set('Content-Type', 'text/plain');
        return $response;
    }

    public function confirmAction($_locale, $hash)
    {
        if(!$this->userHasAccess())
            return $this->redirect($this->generateUrl('homepage'));

        $userReporsitory = $this->getDoctrine()->getEntityManager()->getRepository("AppBundle\Entity\User");
        $user = $userReporsitory->loadUserByHash($hash);

        if($user->getIsActive())
            return $this->redirect($this->generateUrl('homepage'));

        $user->setIsActive(true);
        $user->setIsConfirmed(true);

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return $this->redirect($this->generateUrl('registration/confirm_message', array("type" => "success")));
    }

    public function confirmMessageAction($type)
    {
        if(!$this->userHasAccess())
            return $this->redirect($this->generateUrl('homepage'));
        switch($type)
        {
            case "info":
                return $this->render('AppBundle:frontend/registration:confirm_message_info.html.twig');
            case "success":
                return $this->render('AppBundle:frontend/registration:confirm_message_success.html.twig');
        }
    }

    private function userHasAccess()
    {
        $ac = $this->get('security.authorization_checker');
        $hasAccess = !$ac->isGranted(new Expression(
            'is_authenticated() or is_fully_authenticated()'
        ));

        return $hasAccess;
    }
}
