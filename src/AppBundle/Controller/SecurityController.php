<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\ExpressionLanguage\Expression;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use AppBundle\Form\Type\PasswordRecoveryType;

use AppBundle\Event\BillEvent;
use AppBundle\BillEvents;

class SecurityController extends Controller
{
    /**
    * Обработка ошибок авторизации.
    *
    * @param string $_locale Текущая локаль.
    *
    * @return JsonResponse
    */
    public function loginAction($_locale)
    {
        $response = new JsonResponse();

        /**
        * AuthorizationChecker
        *
        * @var \Symfony\Component\Security\Core\Authorization\AuthorizationChecker
        */
        $ac = $this->get('security.authorization_checker');
        $isAuthenticated = $ac->isGranted(new Expression(
            'is_authenticated()'
        ));

        if($isAuthenticated)
        {
            $response->setData(array(
                "success" => true
            ));

            return $response;
        }


        $request = $this->getRequest();
        $session = $request->getSession();

        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
        }

        /**
        * @var \Symfony\Bundle\FrameworkBundle\Translation\Translator
        */
        $translator = $this->get("translator");

        $response->setData(array(
                "success" => false,
                "error" => $translator->trans($error->getMessage(), array(), 'login_messages', $_locale)
            ));

        return $response;
    }

    /**
    * Проверка email'а
    *
    * @param string $_locale Текущая локаль
    */
    public function checkEmailAction($_locale)
    {
        $request = $this->getRequest();
        $email = $request->get("email");

        /**
        * @var \Symfony\Bundle\FrameworkBundle\Translation\Translator
        */
        $translator = $this->get("translator");

        /**
        * UserHelper
        *
        * @var \AppBundle\Services\UserHelper
        */
        $userHelper = $this->get('user_helper');

        $response = new JsonResponse();

        $errorMsg = false;
        if(!$userHelper->isValidEmail($email))
        {
            $response->setData(array(
                'success' => false,
                'errorMsg' => $translator->trans('Введите корректный E-mail!', array(), null, $_locale),
            ));
            return $response;
        }
        elseif(!$userHelper->isUniqueEmail($email))
        {
            $response->setData(array(
                'success' => false,
                'errorMsg' => $translator->trans('Пользователь с таким E-mail\'ом уже зарегистрирован!', array(), null, $_locale),
            ));
            return $response;
        }

        $response->setData(array(
            'success' => true
        ));

        return $response;
    }

    /**
    * Проверка username'а
    *
    * @param string $_locale Текущая локаль
    */
    public function checkUsernameAction($_locale)
    {
        $request = $this->getRequest();
        $username = $request->get("username");

        $excludeUserId = $request->get("excludeUserId", false);
        /**
        * @var \Symfony\Bundle\FrameworkBundle\Translation\Translator
        */
        $translator = $this->get("translator");

        /**
        * UserHelper
        *
        * @var \AppBundle\Services\UserHelper
        */
        $userHelper = $this->get('user_helper');

        $response = new JsonResponse();

        $errorMsg = false;
        if(!$userHelper->isValidUsername($username))
        {
            $response->setData(array(
                'success' => false,
                'errorMsg' => $translator->trans("Никнейм может содержать только латинские символы, цифры и подчёркивания. Длина никнейма не может превышать %username_maxlength% символов.", array("%username_maxlength%" => $userHelper::USERNAME_MAXLENGTH), null, $_locale),
            ));
            return $response;
        }
        elseif(!$userHelper->isUniqueUsername($username, $excludeUserId))
        {
            $response->setData(array(
                'success' => false,
                'errorMsg' => $translator->trans("Пользователь с таким никнеймом уже зарегистрирован на сайте.", array(), null, $_locale),
            ));
            return $response;
        }

        $response->setData(array(
            'success' => true
        ));

        return $response;
    }

    /**
    * Возвращает данные пользователя по реферальному коду
    *
    * @param string $_locale      Текущая локаль.
    * @param string $referralCode Реферальный код.
    *
    * @return JsonResponse
    */
    public function getUserByReferralCodeAction($_locale, $referralCode)
    {
        /**
        * UserHelper
        *
        * @var \AppBundle\Services\UserHelper
        */
        $userHelper = $this->get('user_helper');

        $user = $userHelper->getUserByReferralCode($referralCode);

        $response = new JsonResponse(array(
            "id" => $user->getId(),
            "nick" => $user->getNick(),
            "avatar" => $user->getAvatar()
        ));

        return $response;
    }

    public function passwordRecoveryAction($_locale)
    {
        $request = $this->getRequest();

        $email = $request->get('email');

        /**
        * @var \Symfony\Bundle\FrameworkBundle\Translation\Translator
        */
        $translator = $this->get("translator");

        $response = new JsonResponse();

        if (empty($email)) {
            $response->setData(array(
                "success" => false,
                "error" => $translator->trans('Введите корректный E-mail!', array(), null, $_locale)
            ));

            return $response;
        }

        /**
        * @var \AppBundle\Entity\UserRepository
        */
        $userRep = $this->getDoctrine()->getRepository('AppBundle\Entity\User');

        $user = $userRep->findUserByEmail($email);

        if (!$user) {
            $response->setData(array(
                "success" => false,
                "error" => $translator->trans('Пользователь с таким E-mail\'ом не зарегистрирован!', array(), null, $_locale)
            ));

            return $response;
        }

        /**
        * UserHelper
        *
        * @var \AppBundle\Services\UserHelper
        */
        $userHelper = $this->get('user_helper');

        $newPassword = $userHelper->generatePassword();

        /**
        * UserPasswordEncoder
        *
        * @var \Symfony\Component\Security\Core\Encoder\UserPasswordEncoder
        */
        $encoder = $this->get('security.password_encoder');

        $user->setPassword( $encoder->encodePassword($user, $newPassword) );

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        /**
        * MailerHelper
        *
        * @var \AppBundle\Services\MailerHelper
        */
        $mailerHelper = $this->get('mailer_helper');
        $mailerHelper->sendPasswordRecoveryEmail($user, $newPassword);

        $response->setData(array(
            "success" => true,
            "successMsg" => $translator->trans('Новый пароль сгенерирован и отправлен на указанный e-mail!', array(), null, $_locale)
        ));

        return $response;
    }

    public function passwordRecoveryDeprecatedAction()
    {
        $form = $this->createForm(new PasswordRecoveryType());

        $request = $this->getRequest();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $formData = $form->getData();

            $user = $this->getDoctrine()->getRepository('AppBundle\Entity\User')->findUserByEmail($formData["email"]);

            if(!$user)
            {
                $request->getSession()->getFlashBag()->add('email_not_found', 'email_not_found');
                return $this->redirect($this->generateUrl('password_recovery'));
            }

            $userHelper = $this->get('user_helper');
            $newPassword = $userHelper->generatePassword();

            $encoder = $this->get('security.password_encoder');
            $user->setPassword( $encoder->encodePassword($user, $newPassword) );

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $userHelper->sendPasswordRecoveryEmailToUser($user, $newPassword);

            $request->getSession()->getFlashBag()->add('password_recovered', 'password_recovered');
            return $this->redirect($this->generateUrl('login'));
        }

        return $this->render('AppBundle:frontend:password_recovery.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
    * Проверка идентификатора пользователя из ДОЛ
    */
    public function billingCheckAction()
    {
        /**
        * @var \AppBundle\Services\DOLHelper
        */
        $DOLHelper = $this->get("dol_helper");

        $request = $this->getRequest();

        $amount = $request->get("amount", 0);
        $userId = $request->get("userid", 0);
        $userIdExtra = $request->get("userid_extra", 0);
        $orderId = $request->get("orderid", 0);
        $paymentId = $request->get("paymentid", 0);
        $key = $request->get("key", 0);

        $code = 'NO';

        if($DOLHelper->checkCheckKey($key, $userId))
            $code = 'YES';

        $data = array('code' => $code);

        $encoder = new XmlEncoder("result");
        $normalizer = new GetSetMethodNormalizer();
        $serializer = new Serializer(array($normalizer), array($encoder));

        $response = new Response($serializer->serialize($data, "xml", array("xml_version" => "1.0", "xml_encoding" => "UTF-8")));
        $response->headers->set('Content-Type', 'text/xml');

        return $response;
    }

    /**
    * Оповещения о проведении платежа из ДОЛ
    */
    public function billingCallbackAction()
    {
        file_put_contents($this->container->getParameter("kernel.cache_dir")."/billing_callback.log", "---------\n".date("Y-m-d H:i:s")."\n---------\n".print_r($_REQUEST, true)."\n\r---------\n\r", FILE_APPEND);

        $request = $this->getRequest();

        $amount = $request->get("amount", 0);
        $userId = $request->get("userid", 0);
        $paymentId = $request->get("paymentid", 0);
        $key = $request->get("key", 0);
        $paymode = $request->get("paymode", 0);
        $userid_extra = $request->get("userid_extra", 0);

        $data = array('code' => "NO", "comment" => "Платёж не найден.");

        /**
        * @var \AppBundle\Services\DOLHelper
        */
        $DOLHelper = $this->get("dol_helper");

        /**
        * @var \AppBundle\Services\BalanceHelper
        */
        $BalanceHelper = $this->get("balance_helper");

        /**
        * @var \AppBundle\Entity\BillingOperationRepository
        */
        $BilingOperationRep = $this->getDoctrine()->getRepository("AppBundle\Entity\BillingOperation");

        /**
        * @var \AppBundle\Entity\BillingOperation
        */
        $BillingOperation = $BilingOperationRep->findByPaymentId($paymentId);

        //Если платёж не в процессе оплаты, то ничего делать не нужно
        if($BillingOperation->getStatus() != $BalanceHelper::STATUS_IN_PROGRESS)
        {
            $data = array('code' => "NO", "comment" => "Платёж уже совершён.");
            $BillingOperation = null;
        }

        if($BillingOperation)
        {
            if($DOLHelper->checkCallbackKey($key, $BillingOperation))
            {
                $BalanceHelper->processBill($BillingOperation);
                $data = array('id' => $BillingOperation->getHash(), 'code' => "YES", "course" => $BillingOperation->getMoneyRate());
            }
        }

        $encoder = new XmlEncoder("result");
        $normalizer = new GetSetMethodNormalizer();
        $serializer = new Serializer(array($normalizer), array($encoder));

        $response = new Response($serializer->serialize($data, "xml", array("xml_version" => "1.0", "xml_encoding" => "UTF-8")));
        $response->headers->set('Content-Type', 'text/xml');

        return $response;
    }
}