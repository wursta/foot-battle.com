<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TeamsController extends Controller
{    
    public function statisticAction($_locale, $teamLeftId, $teamRightId)
    {   
        $teamRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Team');

        $leftTeam = $teamRep->find($teamLeftId);
        $rightTeam = $teamRep->find($teamRightId);

        if(!$leftTeam || !$rightTeam)
            throw $this->createNotFoundException('Одна из команд не найдена!');
        
        $matchesRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Match');

        $leftTeamLastMatches = $matchesRep->findLastMatches($leftTeam->getId(), 5);
        $rightTeamLastMatches = $matchesRep->findLastMatches($rightTeam->getId(), 5);

        $matchesBetweenTeams = $matchesRep->findMatchesBetweenTeams($leftTeam->getId(), $rightTeam->getId(), 3);
                
        return $this->render('AppBundle:common/team:statistic.html.twig', array(
                'leftTeam' => $leftTeam,
                'rightTeam' => $rightTeam,
                'leftTeamLastMatches' => $leftTeamLastMatches,
                'rightTeamLastMatches' => $rightTeamLastMatches,
                'matchesBetweenTeams' => $matchesBetweenTeams
        ));
    }    
}