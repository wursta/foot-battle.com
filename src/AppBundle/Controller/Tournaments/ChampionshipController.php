<?php
namespace AppBundle\Controller\Tournaments;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Entity\Bet;

class ChampionshipController extends Controller
{
    /**
    * Карточка турнира
    *
    * @param string $_locale Текущая локаль
    * @param int $tournament_id ID турнира
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function viewAction($_locale, $tournament_id)
    {
        /**
        * @var \AppBundle\Entity\TournamentChampionshipRepository $tournamentRep
        */
        $tournamentRep = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentChampionship");

        /**
        * @var \AppBundle\Entity\TournamentChampionship $tournament
        */
        $tournament = $tournamentRep->findLazy($tournament_id);

        /**
        * @var \AppBundle\Services\TournamentsHelper $tournamentsHelper
        */
        $tournamentsHelper = $this->get('tournaments_helper');

        /**
        * @var \AppBundle\Entity\User $currentUser
        */
        $currentUser = $this->get('security.context')->getToken()->getUser();

        if(!$tournamentsHelper->userCanViewTournament($tournament, $currentUser))
            throw $this->createNotFoundException('Туринир не найден!');

        return $this->render("AppBundle:frontend/tournaments/championship:view.html.twig", array(
            "tournament" => $tournament,
            "tournamentStatus" => $tournamentsHelper->getTournamentStatus($tournament->getTournament())
        ));
    }

    /**
    * Данные с результатами турнира по туру
    *
    * @param string $_locale Текущая локаль
    * @param int $tournmentId ID турнира
    * @param int $roundNum Номер тура
    * @return JsonResponse
    */
    public function resultsAction($_locale, $tournamentId, $roundNum)
    {
        /**
        * @var \AppBundle\Entity\TournamentChampionshipRepository
        */
        $championshipRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentChampionship');

        /**
        * @var TournamentChampionship $championship
        */
        $championship = $championshipRep->findLazy($tournamentId);

        if(!$championship)
            $this->createNotFoundException("Турнир не найден");

        /**
        * @var \AppBundle\Entity\TournamentChampionshipResultRepository
        */
        $championshipResultsRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentChampionshipResult');

        $results = array();

        /**
        * @var \AppBundle\Entity\TournamentChampionshipResult[]
        */
        $resultsSet = $championshipResultsRep->findByTournamentAndRound($tournamentId, $roundNum);

        if(empty($resultsSet))
        {
            /**
            * @var \AppBundle\Services\Tournaments\Championship
            */
            $championshipHelper = $this->get("tournament.championship");
            $resultsSet = $championshipHelper->initResults($championship);
        }

        foreach($resultsSet as $result)
        {
            $results[] = array(
                "place" => $result->getPlace(),
                "user" => array(
                    "id" => $result->getUser()->getId(),
                    "nick" => $result->getUser()->getNick(),
                    "avatar" => $result->getUser()->getAvatar(),
                ),
                "totalGuessedResults" => $result->getTotalGuessedResults(),
                "totalGuessedDifferences" => $result->getTotalGuessedDifferences(),
                "totalGuessedOutcomes" => $result->getTotalGuessedOutcomes(),
                "userRating" => $result->getUserRating(),
                "points" => $result->getTotalPoints()
            );
        }

        $response = new JsonResponse();
        $response->setData($results);

        return $response;
    }

    /**
    * Список матчей распределённых по турам
    *
    * @param string $_locale Текущая локаль
    * @param mixed $tournamentId
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function matchesAction($_locale, $tournamentId)
    {
        /**
        * @var \AppBundle\Entity\TournamentChampionshipRepository
        */
        $championshipRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentChampionship');

        /**
        * @var TournamentChampionship
        */
        $championship = $championshipRep->findLazy($tournamentId);

        if(!$championship)
            $this->createNotFoundException("Турнир не найден");

        /**
        * @var \AppBundle\Entity\TournamentChampionshipMatchRepository
        */
        $matchesRep = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentChampionshipMatch");
        $tournamentMatches = $matchesRep->findByTournament($championship->getTournament()->getId());

        /**
        * @var \AppBundle\Services\Tournaments\Championship
        */
        $championshipHelper = $this->get("tournament.championship");

        $tournamentMatches = $championshipHelper->chunkMatches($tournamentMatches, $championship->getRoundsCount(), $championship->getMatchesInRound());

        /**
        * @var \AppBundle\Services\TournamentsHelper
        */
        $tournamentsHelper = $this->get("tournaments_helper");

        /**
        * @var \AppBundle\Entity\User
        */
        $currentUser = $this->getUser();

        $usersBets = array();

        if($tournamentsHelper->isUserInTournament($championship, $currentUser))
        {
            /**
            * @var \AppBundle\Entity\BetRepository
            */
            $betsRep = $this->getDoctrine()->getRepository("AppBundle\Entity\Bet");

            /**
            * @var \AppBundle\Services\BetsHelper
            */
            $betsHelper = $this->get("bets_helper");

            $bets = $betsRep->findByTournamentAndUser($championship->getTournament()->getId(), $currentUser->getId());

            $usersBets = $betsHelper->rebuildByMatches($bets);
        }

        /**
        * @var \Symfony\Bundle\FrameworkBundle\Translation\Translator
        */
        $translator = $this->get("translator");

        $results = array();

        foreach($tournamentMatches as $roundNum => $tournamentMatch)
        {
            $results[$roundNum] = array(
                "roundTitle" => $translator->trans("Тур %s%", array("%s%" => $roundNum), null, $_locale)
            );

            foreach($tournamentMatch as $matchNum => $tMatch)
            {
                $options = array(
                    "showBetControls" => $tournamentsHelper->isUserInTournament($championship, $currentUser)
                );

                if(empty($tMatch))
                {
                    $results[$roundNum]["matches"][$matchNum] = array();
                    continue;
                }

                /**
                * @var \AppBundle\Entity\Match
                */
                $match = $tMatch->getMatch();

                $matchDatetime = $match->getMatchDate()->setTime($match->getMatchTime()->format("H"), $match->getMatchTime()->format("i"), $match->getMatchTime()->format("s"));

                $betData = false;

                if($currentUser && !empty($usersBets[$match->getId()][$currentUser->getId()]))
                {
                    /**
                    * @var \AppBundle\Entity\Bet
                    */
                    $bet = $usersBets[$match->getId()][$currentUser->getId()];

                    $betData = array(
                        "id" => $bet->getId(),
                        "scoreLeft" => $bet->getScoreLeft(),
                        "scoreRight" => $bet->getScoreRight(),
                        "points" => $bet->getPoints()
                    );
                }

                $results[$roundNum]["matches"][$matchNum] = array(
                    "id" => $match->getId(),
                    "league" => array(
                        "title" => $match->getLeague()->getLeagueName(),
                        "icon" => $match->getLeague()->getIcon(),
                    ),
                    "isStarted" => $match->getIsStarted(),
                    "isRejected" => $match->getIsRejected(),
                    "startDatetime" => $matchDatetime->format("c"),
                    "homeTeamTitle" => $match->getHomeTeam()->getTitle(),
                    "guestTeamTitle" => $match->getGuestTeam()->getTitle(),
                    "scoreLeft" => $match->getScoreLeft(),
                    "scoreRight" => $match->getScoreRight(),
                    "bet" => $betData,
                    "options" => $options
                );
            }
        }

        $response = new JsonResponse();
        $response->setData($results);

        return $response;
    }

    /**
    * Сохранение прогноза на матч
    *
    * @param string $_locale Текущая локаль
    * @param \AppBundle\Entity\Tournament $tournament
    * @param \AppBundle\Entity\Match $match
    * @param int $scoreLeft
    * @param int $scoreRight
    * @return JsonResponse
    */
    public function saveBetAction($_locale, $tournament, $match, $scoreLeft, $scoreRight)
    {
        /**
        * @var \AppBundle\Entity\TournamentChampionshipMatchRepository
        */
        $championshipMatchRep = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentChampionshipMatch");

        if(!$championshipMatchRep->isMatchInTournament($tournament->getId(), $match->getId()))
            throw $this->createNotFoundException('Данный матч не найден в данном турнире или туре!');

        /**
        * @var \AppBundle\Entity\TournamentChampionship
        */
        $championship = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentChampionship")->findLazy($tournament->getId());

        /**
        * @var \AppBundle\Services\TournamentsHelper $tournamentsHelper
        */
        $tournamentsHelper = $this->get('tournaments_helper');

        /**
        * @var \AppBundle\Entity\User $currentUser
        */
        $currentUser = $this->get('security.context')->getToken()->getUser();

        if(!$tournamentsHelper->isUserInTournament($championship, $currentUser))
            throw $this->createNotFoundException('Текущий пользователь не участвует в данном турнире!');

        /**
        * @var \AppBundle\Entity\Bet $bet
        */
        if(!$bet = $this->getDoctrine()->getRepository('AppBundle\Entity\Bet')->findByTournamentMatchAndUser($tournament->getId(), $currentUser->getId(), $match->getId()))
        {
            $bet = new Bet();
            $bet->setTournament($tournament);
            $bet->setUser($currentUser);
            $bet->setMatch($match);
        }

        $bet->setScoreLeft($scoreLeft);
        $bet->setScoreRight($scoreRight);

        try
        {
            /**
            * @var \Doctrine\ORM\EntityManager $em
            */
            $em = $this->getDoctrine()->getManager();
            $em->persist($bet);
            $em->flush();

            $response = new JsonResponse();
            $response->setData(array("success" => true));

            return $response;
        } catch (Exception $e) {
            throw $this->createNotFoundException('Произошла непредвиденная ошибка во время сохранения прогноза!');
        }
    }

    public function infoAction($_locale, $tournament_id)
    {
        /**
        * @var \AppBundle\Entity\TournamentChampionshipRepository $tournamentRep
        */
        $tournamentRep = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentChampionship");

        /**
        * @var \AppBundle\Entity\TournamentChampionship $tournament
        */
        $tournament = $tournamentRep->findLazy($tournament_id);

        $usersCount = count($tournament->getTournament()->getUsers());
        if($usersCount == 0)
            $usersCount = $tournament->getMaxUsersCount();

        $winDistribution = $this->getDoctrine()->getRepository("AppBundle\Entity\WinRule")->findByFormatAndWinnersCount("championship", $usersCount);
        return $this->render("AppBundle:frontend/tournaments/championship:info.html.twig", array(
            "tournament" => $tournament,
            "winDistribution" => $winDistribution
        ));
    }

    public function windistributionAction($_locale, $tournament_id)
    {
        /**
        * @var \AppBundle\Entity\TournamentChampionshipRepository $tournamentRep
        */
        $tournamentRep = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentChampionship");

        /**
        * @var \AppBundle\Entity\TournamentChampionship $tournament
        */
        $tournament = $tournamentRep->findLazy($tournament_id);

        $usersCount = count($tournament->getTournament()->getUsers());
        if($usersCount == 0)
            $usersCount = $tournament->getMaxUsersCount();

        $winDistribution = $this->getDoctrine()->getRepository("AppBundle\Entity\WinRule")->findByFormatAndWinnersCount("championship", $usersCount);
        return $this->render("AppBundle:frontend/tournaments/championship:windistribution.html.twig", array(
            "tournament" => $tournament,
            "winDistribution" => $winDistribution
        ));
    }
}
