<?php
namespace AppBundle\Controller\Tournaments;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Entity\Bet;
use AppBundle\Entity\TournamentChampionsleague;

class ChampionsleagueController extends Controller
{
    public function infoAction($_locale, $tournament_id)
    {
        /**        
        * @var \AppBundle\Entity\TournamentChampionsleagueRepository $tournamentRep
        */
        $tournamentRep = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentChampionsleague");
        
        /**        
        * @var \AppBundle\Entity\TournamentChampionsleague $tournament
        */
        $tournament = $tournamentRep->findLazy($tournament_id);                
        
        if(!$tournament->getTournament()->getIsStarted())        
        {
            $usersCount = $tournament->getMaxUsersCount();
            $groupsCount = $tournament->getMaxGroupsCount();
            $groupsRoundsCount = ($tournament->getMaxUsersCount() % 2 == 0) ? $tournament->getMaxUsersCount() - 1 : $tournament->getMaxUsersCount();
            $usersOutOfGroup = $tournament->getUsersOutOfGroup();
            $playoffRoundsCount = round(log($tournament->getMaxGroupsCount()*$tournament->getUsersOutOfGroup(), 2));
        }
        else
        {
            $usersCount = $tournament->getRealUsersCount();
            $groupsCount = $tournament->getRealGroupsCount();
            $groupsRoundsCount = $tournament->getRealGroupRoundsCount();            
            $usersOutOfGroup = $tournament->getRealUsersOutOfGroup();
            $playoffRoundsCount = $tournament->getRealPlayoffRoundsCount();
        }
        
        $winDistribution = $this->getDoctrine()->getRepository("AppBundle\Entity\WinRule")->findByFormatAndWinnersCount("championsleague", $usersCount);
        return $this->render("AppBundle:frontend/tournaments/championsleague:info.html.twig", array(
            "tournament" => $tournament,
            "winDistribution" => $winDistribution,
            "groupsCount" => $groupsCount,
            "groupsRoundsCount" => $groupsRoundsCount,
            "usersOutOfGroup" => $usersOutOfGroup,
            "playoffRoundsCount" => $playoffRoundsCount
        ));
    }
    
    public function viewAction($_locale, $tournament_id)
    {
        /**        
        * @var \AppBundle\Entity\TournamentChampionsleagueRepository $tournamentRep
        */
        $tournamentRep = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentChampionsleague");
        
        /**        
        * @var \AppBundle\Entity\TournamentChampionsleague $tournament
        */
        $tournament = $tournamentRep->findLazy($tournament_id);
        
        /**        
        * @var \AppBundle\Services\TournamentsHelper $tournamentsHelper
        */
        $tournamentsHelper = $this->get('tournaments_helper');                
        
        /**
        * @var \AppBundle\Entity\User $currentUser
        */
        $currentUser = $this->get('security.context')->getToken()->getUser();
        
        if(!$tournamentsHelper->userCanViewTournament($tournament, $currentUser))
            throw $this->createNotFoundException('Туринир не найден!');
        
        $userGroup = 1;
        
        $userInGroup = false;
        if($tournament->getTournament()->getIsStarted())
        {
            $playoffRoundsCount = $tournament->getRealPlayoffRoundsCount();
        }
        else
        {
            $usersInPlayoffCount = $tournament->getMaxGroupsCount()*$tournament->getUsersOutOfGroup();
            $playoffRoundsCount = round(log($usersInPlayoffCount, 2));
        }
        
        $userInPlayoffRounds = array_fill(1, $playoffRoundsCount, false);
        
        if($tournamentsHelper->isUserInTournament($tournament, $currentUser))            
        {
            $userInGroup = true;
            $userInPlayoffRounds = array_fill(1, $playoffRoundsCount, true);
        }
            
        if($tournament->getStage() == TournamentChampionsleague::STAGE_GROUP && is_a($currentUser, '\AppBundle\Entity\User'))
        {
            $userInGroup = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentChampionsleagueGroupResult")->isUserInGroupStage($tournament_id, $currentUser->getId());                        
            
            if($userInGroup)
                $userGroup = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentChampionsleagueGroupGame")->findUserGroup($tournament_id, $currentUser->getId());
            else
                $userInPlayoffRounds = array_fill(1, $playoffRoundsCount, false);
            
        }
        
        if($tournament->getStage() == TournamentChampionsleague::STAGE_PLAYOFF && is_a($currentUser, '\AppBundle\Entity\User'))
        {            
            $userInGroup = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentChampionsleagueGroupResult")->isUserInGroupStage($tournament_id, $currentUser->getId());
            if($userInGroup)
                $userGroup = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentChampionsleagueGroupGame")->findUserGroup($tournament_id, $currentUser->getId());
            
            for($roundNum = 1; $roundNum <= $tournament->getCurrentRound(); $roundNum++)
                $userInPlayoffRounds[$roundNum] = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentChampionsleaguePlayoffGame")->isUserInPlayoffRound($tournament_id, $currentUser->getId(), $roundNum);
        }
        
        return $this->render("AppBundle:frontend/tournaments/championsleague:view.html.twig", array(
            "tournament" => $tournament,
            "userGroup" => $userGroup,
            "userInGroup" => $userInGroup,
            "userInPlayoffRounds" => $userInPlayoffRounds
        ));
    }
    
    public function matchesAction(\AppBundle\Entity\TournamentChampionsleague $tournament, $userInQualification = false, $userInGroup = false, $userInPlayoffRounds = array(), $userGroup = 1)
    {
        $qualificationMatches = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentChampionsleagueMatch")->findQualificationByTournament($tournament->getTournament()->getId());
        $qualificationMatches = $this->get("tournament.championship")->chunkMatches($qualificationMatches, $tournament->getQualificationRoundsCount(), $tournament->getMatchesInQualification());
        
        $groupMatches = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentChampionsleagueMatch")->findGroupByTournament($tournament->getTournament()->getId());
        $usersCount = ($tournament->getTournament()->getIsStarted()) ? $tournament->getRealUsersOutOfQualification() : $tournament->getMaxUsersCount();
        $groupsCount = ($tournament->getTournament()->getIsStarted()) ? $tournament->getRealGroupsCount() : $tournament->getMaxGroupsCount();
        $groupMatches = $this->get("tournament.round")->chunkMatches($groupMatches, $usersCount/$groupsCount, $tournament->getMatchesInGame());        
        
        $playoffMatches = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentChampionsleagueMatch")->findPlayoffByTournament($tournament->getTournament()->getId());
        $usersInPlayoff = ($tournament->getTournament()->getIsStarted()) ? $tournament->getRealGroupsCount()*$tournament->getRealUsersOutOfGroup() : $tournament->getMaxGroupsCount()*$tournament->getUsersOutOfGroup();
        $roundsCount = round(log($usersInPlayoff, 2));        
        $playoffMatches = $this->get("tournament.playoff")->chunkMatches($playoffMatches, $roundsCount, $tournament->getMatchesInPlayoff());
        
        $userBets = null;
        if($userInQualification)
        {
            /**
            * @var \AppBundle\Entity\User $currentUser
            */
            $currentUser = $this->get('security.context')->getToken()->getUser();
            $bets = $this->getDoctrine()->getRepository("AppBundle\Entity\Bet")->findByTournamentAndUser($tournament->getTournament()->getId(), $currentUser->getId());
            $userBets = $this->get("tournaments_helper")->userBetsWalk($bets);             
        }
        
        /**        
        * @var Symfony\Component\Form\Extension\Csrf\CsrfProvider\SessionCsrfProvider $csrf
        */
        $csrf = $this->get('form.csrf_provider');        
        $token = $csrf->generateCsrfToken('bet_form');
        
        return $this->render("AppBundle:frontend/tournaments/championsleague:matches.html.twig", array(
            "qualificationMatches" => $qualificationMatches,
            "groupMatches" => $groupMatches,
            "playoffMatches" => $playoffMatches,
            "userBets" => $userBets,
            "tournament" => $tournament,
            "userInQualification" => $userInQualification,
            "userInGroup" => $userInGroup,
            "userInPlayoffRounds" => $userInPlayoffRounds,
            "userGroup" => (int) $userGroup,
            "csrfToken" => $token
        ));
    }
    
    public function betAction($_locale, $tournament_id, $match_id, $round_num, $score_left, $score_right)
    {                        
        $request = $this->getRequest();
        
        $qualificationMatch = (bool) $request->get("qualificationMatch", 0);
        $groupMatch = (bool) $request->get("groupMatch", 0);
        
        if(!$this->getDoctrine()->getRepository("AppBundle\Entity\TournamentChampionsleagueMatch")->isMatchInTournamentAndRound($tournament_id, $match_id, $round_num, $qualificationMatch, $groupMatch))
            throw $this->createNotFoundException('Данный матч не найден в данном турнире или туре!');                
        
        /**        
        * @var \AppBundle\Entity\TournamentChampionship $tournament
        */        
        $tournament = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentChampionsleague")->findLazy($tournament_id);                
        
        /**        
        * @var \AppBundle\Services\TournamentsHelper $tournamentsHelper
        */
        $tournamentsHelper = $this->get('tournaments_helper');                
        
        /**
        * @var \AppBundle\Entity\User $currentUser
        */
        $currentUser = $this->get('security.context')->getToken()->getUser();
        
        if(!$tournamentsHelper->isUserInTournament($tournament, $currentUser))
            throw $this->createNotFoundException('Текущий пользователь не участвует в данном турнире!');
        
        if($tournament->getStage() == TournamentChampionsleague::STAGE_GROUP)
        {
            $userInGroup = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentChampionsleagueGroupResult")->isUserInGroupStage($tournament_id, $currentUser->getId());            
            if(!$userInGroup)
                throw $this->createNotFoundException('Текущий пользователь выбыл из данного турнира на этапе квалификации!');
        }
        
        if($tournament->getStage() == TournamentChampionsleague::STAGE_PLAYOFF)
        {
            $userInPlayoff = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentChampionsleaguePlayoffGame")->isUserInPlayoffRound($tournament_id, $currentUser->getId(), $tournament->getCurrentRound());
            if(!$userInPlayoff)
                throw $this->createNotFoundException('Текущий пользователь выбыл из данного турнира на этапе группового этапа или в раунде плей-офф!');
        }                
        
        $match = $this->getDoctrine()->getRepository('AppBundle\Entity\Match')->find($match_id);                
        
        /**        
        * @var \AppBundle\Entity\Bet $bet
        */
        if(!$bet = $this->getDoctrine()->getRepository('AppBundle\Entity\Bet')->findByTournamentMatchAndUser($tournament->getTournament()->getId(), $currentUser->getId(), $match->getId()))
        {
            $bet = new Bet();
            $bet->setTournament($tournament->getTournament());
            $bet->setUser($currentUser);
            $bet->setMatch($match);
        }
        
        $bet->setScoreLeft($score_left);
        $bet->setScoreRight($score_right);
        
        try
        {
            /**        
            * @var \Doctrine\ORM\EntityManager $em
            */
            $em = $this->getDoctrine()->getManager();        
            $em->persist($bet);
            $em->flush();            
            
            return new JsonResponse(array("success" => true));
        } catch (Exception $e) {
            throw $this->createNotFoundException('Произошла непредвиденная ошибка во время сохранения прогноза!');
        }
    }
    
    public function usersBetsAction($_locale, $tournament_id, \AppBundle\Entity\Match $match, $round)
    {
        $request = $this->getRequest();
        
        $qualificationMatch = (bool) $request->get("qualificationMatch", 0);
        $groupMatch = (bool) $request->get("groupMatch", 0);
        
        if(!$this->getDoctrine()->getRepository("AppBundle\Entity\TournamentChampionsleagueMatch")->isMatchInTournamentAndRound($tournament_id, $match->getId(), $round, $qualificationMatch, $groupMatch))
            throw $this->createNotFoundException('Данный матч не найден в данном турнире или туре!');
        
        /**        
        * @var \AppBundle\Entity\TournamentChampionship $tournament
        */        
        $tournament = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentChampionsleague")->findLazy($tournament_id);                
                
        if($qualificationMatch = 0 && $groupMatch == 1)
        {
            $userInGroup = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentChampionsleagueGroupResult")->isUserInGroupStage($tournament_id, $currentUser->getId());
            if(!$userInGroup)
                throw $this->createNotFoundException('Текущий пользователь выбыл из данного турнира на этапе квалификации!');
        }
        
        if($qualificationMatch = 0 && $groupMatch == 0)
        {
            $userInPlayoff = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentChampionsleaguePlayoffGame")->isUserInPlayoffRound($tournament_id, $currentUser->getId(), $tournament->getCurrentRound());
            if(!$userInPlayoff)
                throw $this->createNotFoundException('Текущий пользователь выбыл из данного турнира на этапе группового этапа или в раунде плей-офф!');
        }
        
        $bets = $this->getDoctrine()->getRepository('AppBundle\Entity\Bet')->findByTournamentAndMatch($tournament_id, $match->getId());
            
        return $this->render("AppBundle:frontend/tournaments:userbets.html.twig", array(
            'bets' => $bets,
            'match' => $match
        ));
    }
    
    public function qualificationResultsAction($tournament_id, $round = 1)
    {
        /**        
        * @var \AppBundle\Entity\TournamentChampionsleague $championsleague
        */        
        if(!is_a($tournament_id, "\AppBundle\Entity\TournamentChampionsleague"))        
            $championsleague = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentChampionsleague")->findLazy($tournament_id);                
        else
            $championsleague = $tournament_id;
        
        if(empty($round))
            $round = 1;                
        
        $results = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentChampionsleagueQualificationResult')->findByTournamentAndRound($championsleague->getTournament()->getId(), $round);
        
        if(!$results)
        {
            $results = $this->get('tournament.championsleague')->initQualificationResults($championsleague);            
        }
        
        return $this->render("AppBundle:frontend/tournaments/championsleague:qualification_results.html.twig", array(
            "round" => $round,
            "tournament" => $championsleague,
            "results" => $results
        ));
    }
    
    public function groupResultsAction($_locale, $tournament_id, $round = 1, $group = 1)
    {
        /**        
        * @var \AppBundle\Entity\TournamentChampionsleague $championsleague
        */        
        if(!is_a($tournament_id, "\AppBundle\Entity\TournamentChampionsleague"))        
            $championsleague = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentChampionsleague")->findLazy($tournament_id);                
        else
            $championsleague = $tournament_id;
        
        if(empty($round))
            $round = 1;                
        
        if(empty($group))
            $group = 1;                
        
        $results = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentChampionsleagueGroupResult')->findByTournamentAndGroupAndRound($championsleague->getTournament()->getId(), $round, $group);
        
        if(!$results)
            throw $this->createNotFoundException('Результаты данного тура ещё не доступны!');
        
        return $this->render("AppBundle:frontend/tournaments/championsleague:group_results.html.twig", array(
            "round" => $round,
            "group" => $group,
            "tournament" => $championsleague,
            "results" => $results
        ));
    }
    
    public function groupGamesAction($_locale, $tournament_id, $group = 1)
    {
        /**        
        * @var \AppBundle\Entity\TournamentChampionsleague $championsleague
        */        
        if(!is_a($tournament_id, "\AppBundle\Entity\TournamentChampionsleague"))        
            $championsleague = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentChampionsleague")->findLazy($tournament_id);                
        else
            $championsleague = $tournament_id;        
        
        if(empty($group))
            $group = 1;
        
        $games = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentChampionsleagueGroupGame')->findByTournamentAndGroup($championsleague->getTournament()->getId(), $group);        
        
        if(!$games)
            throw $this->createNotFoundException('Игры данной группы ещё не доступны!');
        
        $gamesByRounds = array();        
        array_walk($games, function(&$game) use (&$gamesByRounds){
            $gamesByRounds[$game->getRound()][] = $game;
        });        
        unset($games);
        
        $matchesWithBets = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentChampionsleagueMatch')->findGroupMatchesAndBetsByTournament($championsleague->getTournament()->getId(), $group);
        $matchesByRounds = array();
        $usersBets = array();
        
        foreach($matchesWithBets as $tournamentMatch)
        {
            $matchesByRounds[$tournamentMatch->getRound()][] = $tournamentMatch->getMatch();
            foreach($tournamentMatch->getMatch()->getBets() as $bet)
                $usersBets[$bet->getMatch()->getId()][$bet->getUser()->getId()] = $bet;
        }        
        
        return $this->render("AppBundle:frontend/tournaments/championsleague:group_games.html.twig", array(            
            "group" => $group,
            "tournament" => $championsleague,
            "gamesByRounds" => $gamesByRounds,
            "matchesByRounds" => $matchesByRounds,
            "usersBets" => $usersBets
        ));
    }
    
    public function playoffGamesAction($_locale, $tournament_id, $round = 1)
    {
        /**        
        * @var \AppBundle\Entity\TournamentChampionsleague $championsleague
        */        
        if(!is_a($tournament_id, "\AppBundle\Entity\TournamentChampionsleague"))        
            $championsleague = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentChampionsleague")->findLazy($tournament_id);                
        else
            $championsleague = $tournament_id;        
        
        if(empty($round))
            $round = 1;
        
        $games = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentChampionsleaguePlayoffGame')->findByTournamentAndRound($championsleague->getTournament()->getId(), $round);
        
        if(!$games)
            throw $this->createNotFoundException('Игры данного тура ещё не доступны!');
        
        $matchesWithBets = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentChampionsleagueMatch')->findPlayoffMatchesAndBetsByTournamentAndRound($championsleague->getTournament()->getId(), $round);        
        $roundMatches = array();
        $usersBets = array();        
        foreach($matchesWithBets as $tournamentMatch)
        {
            $roundMatches[] = $tournamentMatch->getMatch();
            foreach($tournamentMatch->getMatch()->getBets() as $bet)
                $usersBets[$bet->getMatch()->getId()][$bet->getUser()->getId()] = $bet;
        }
        
        return $this->render("AppBundle:frontend/tournaments/championsleague:playoff_games.html.twig", array(
            "tournament" => $championsleague,
            "round" => $round,
            "games" => $games,
            "roundMatches" => $roundMatches,
            "usersBets" => $usersBets
        ));
    }
    
    public function winnersAction($_locale, \AppBundle\Entity\Tournament $tournament)
    {
        /**        
        * @var TournamentChampionsleague $championsleague
        */
        $championsleague = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentChampionsleague')->findLazy($tournament->getId());
        
        $results = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentResult')->findDistributed($tournament->getId());

        return $this->render('AppBundle:frontend/tournaments/championsleague:winners.html.twig', array(
            'tournament' => $championsleague,
            'results' => $results
        ));
    }
    
    public function windistributionAction($_locale, $tournament_id)
    {
        /**        
        * @var \AppBundle\Entity\TournamentChampionshipRepository $tournamentRep
        */
        $tournamentRep = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentChampionsleague");
        
        /**        
        * @var \AppBundle\Entity\TournamentChampionship $tournament
        */
        $tournament = $tournamentRep->findLazy($tournament_id);
        
        if(!$tournament->getTournament()->getIsStarted())        
            $usersCount = $tournament->getMaxUsersCount();
        else
            $usersCount = $tournament->getRealUsersCount();                    
        
        $winDistribution = $this->getDoctrine()->getRepository("AppBundle\Entity\WinRule")->findByFormatAndWinnersCount("championsleague", $usersCount);
        return $this->render("AppBundle:frontend/tournaments/championsleague:windistribution.html.twig", array(
            "tournament" => $tournament,
            "winDistribution" => $winDistribution
        ));
    }    
}
