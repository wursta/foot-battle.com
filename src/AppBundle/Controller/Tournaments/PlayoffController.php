<?php
namespace AppBundle\Controller\Tournaments;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Entity\Bet;
use AppBundle\Entity\TournamentPlayoff;

class PlayoffController extends Controller
{
    /**
    * Карточка турнира
    *
    * @param string $_locale Текущая локаль
    * @param int $tournament_id ID турнира
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function viewAction($_locale, $tournament_id)
    {
        /**
        * @var \AppBundle\Entity\TournamentPlayoffRepository $tournamentRep
        */
        $tournamentRep = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentPlayoff");

        /**
        * @var \AppBundle\Entity\TournamentPlayoff $tournament
        */
        $tournament = $tournamentRep->findLazy($tournament_id);

        /**
        * @var \AppBundle\Services\TournamentsHelper $tournamentsHelper
        */
        $tournamentsHelper = $this->get('tournaments_helper');

        /**
        * @var \AppBundle\Entity\User $currentUser
        */
        $currentUser = $this->get('security.context')->getToken()->getUser();

        if(!$tournamentsHelper->userCanViewTournament($tournament, $currentUser))
            throw $this->createNotFoundException('Туринир не найден!');

        return $this->render("AppBundle:frontend/tournaments/playoff:view.html.twig", array(
            "tournament" => $tournament,
            "tournamentStatus" => $tournamentsHelper->getTournamentStatus($tournament->getTournament())
        ));
    }

    /**
    * Список матчей стадии квалификации распределённых по турам
    *
    * @param string $_locale Текущая локаль
    * @param mixed $tournamentId
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function qualificationMatchesAction($_locale, $tournamentId)
    {
        /**
        * @var \AppBundle\Entity\TournamentPlayoffRepository
        */
        $playoffRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentPlayoff');

        /**
        * @var TournamentPlayoff
        */
        $playoff = $playoffRep->findLazy($tournamentId);

        if(!$playoff)
            $this->createNotFoundException("Турнир не найден");

        /**
        * @var \AppBundle\Entity\TournamentPlayoffMatchRepository
        */
        $matchesRep = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentPlayoffMatch");
        $qualificationTournamentMatches = $matchesRep->findQualificationByTournament($playoff->getTournament()->getId());

        /**
        * @var \AppBundle\Services\Tournaments\Playoff
        */
        $playoffHelper = $this->get("tournament.playoff");

        $tournamentMatches = $playoffHelper->chunkMatches($qualificationTournamentMatches, $playoff->getQualificationRoundsCount(), $playoff->getMatchesInQualification());

        /**
        * @var \AppBundle\Services\TournamentsHelper
        */
        $tournamentsHelper = $this->get("tournaments_helper");

        /**
        * @var \AppBundle\Entity\User
        */
        $currentUser = $this->getUser();

        $usersBets = array();

        if($tournamentsHelper->isUserInTournament($playoff, $currentUser))
        {
            /**
            * @var \AppBundle\Entity\BetRepository
            */
            $betsRep = $this->getDoctrine()->getRepository("AppBundle\Entity\Bet");

            /**
            * @var \AppBundle\Services\BetsHelper
            */
            $betsHelper = $this->get("bets_helper");

            $bets = $betsRep->findByTournamentAndUser($playoff->getTournament()->getId(), $currentUser->getId());

            $usersBets = $betsHelper->rebuildByMatches($bets);
        }

        /**
        * @var \Symfony\Bundle\FrameworkBundle\Translation\Translator
        */
        $translator = $this->get("translator");

        $results = array();

        foreach($tournamentMatches as $roundNum => $tournamentMatch)
        {
            $results[$roundNum] = array(
                "roundTitle" => $translator->trans("Тур %s%", array("%s%" => $roundNum), null, $_locale)
            );

            foreach($tournamentMatch as $matchNum => $tMatch)
            {
                if(empty($tMatch))
                {
                    $results[$roundNum]["matches"][$matchNum] = array();
                    continue;
                }

                /**
                * @var \AppBundle\Entity\Match
                */
                $match = $tMatch->getMatch();

                $matchDatetime = $match->getMatchDate()->setTime($match->getMatchTime()->format("H"), $match->getMatchTime()->format("i"), $match->getMatchTime()->format("s"));

                $betData = false;

                if($currentUser && !empty($usersBets[$match->getId()][$currentUser->getId()]))
                {
                    /**
                    * @var \AppBundle\Entity\Bet
                    */
                    $bet = $usersBets[$match->getId()][$currentUser->getId()];

                    $betData = array(
                        "id" => $bet->getId(),
                        "scoreLeft" => $bet->getScoreLeft(),
                        "scoreRight" => $bet->getScoreRight(),
                        "points" => $bet->getPoints()
                    );
                }

                $results[$roundNum]["matches"][$matchNum] = array(
                    "id" => $match->getId(),
                    "league" => array(
                        "title" => $match->getLeague()->getLeagueName(),
                        "icon" => $match->getLeague()->getIcon(),
                    ),
                    "isStarted" => $match->getIsStarted(),
                    "isRejected" => $match->getIsRejected(),
                    "startDatetime" => $matchDatetime->format("c"),
                    "homeTeamTitle" => $match->getHomeTeam()->getTitle(),
                    "guestTeamTitle" => $match->getGuestTeam()->getTitle(),
                    "scoreLeft" => $match->getScoreLeft(),
                    "scoreRight" => $match->getScoreRight(),
                    "bet" => $betData
                );
            }
        }

        $response = new JsonResponse();
        $response->setData($results);

        return $response;
    }

    /**
    * Список матчей стадии плей-офф распределённых по турам
    *
    * @param string $_locale      Текущая локаль
    * @param mixed  $tournamentId ID турнира
    *
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function playoffMatchesAction($_locale, $tournamentId)
    {
        /**
        * @var \AppBundle\Entity\TournamentPlayoffRepository
        */
        $playoffRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentPlayoff');

        /**
        * @var TournamentPlayoff
        */
        $playoff = $playoffRep->findLazy($tournamentId);

        if(!$playoff)
            $this->createNotFoundException("Турнир не найден");

        /**
        * @var \AppBundle\Entity\TournamentPlayoffMatchRepository
        */
        $matchesRep = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentPlayoffMatch");
        $playoffTournamentMatches = $matchesRep->findPlayoffByTournament($playoff->getTournament()->getId());

        /**
        * @var \AppBundle\Services\Tournaments\Playoff
        */
        $playoffHelper = $this->get("tournament.playoff");

        $playoffRoundsCount = $playoff->getPlayoffRoundsCount();

        $tournamentMatches = $playoffHelper->chunkMatches($playoffTournamentMatches, $playoffRoundsCount, $playoff->getMatchesInGame());

        /**
        * @var \AppBundle\Services\TournamentsHelper
        */
        $tournamentsHelper = $this->get("tournaments_helper");

        /**
        * @var \AppBundle\Entity\User
        */
        $currentUser = $this->getUser();

        $usersBets = array();

        if ($tournamentsHelper->isUserInTournament($playoff, $currentUser)) {
            /**
            * @var \AppBundle\Entity\BetRepository
            */
            $betsRep = $this->getDoctrine()->getRepository("AppBundle\Entity\Bet");

            /**
            * @var \AppBundle\Services\BetsHelper
            */
            $betsHelper = $this->get("bets_helper");

            $bets = $betsRep->findByTournamentAndUser($playoff->getTournament()->getId(), $currentUser->getId());

            $usersBets = $betsHelper->rebuildByMatches($bets);
        }

        /**
        * @var \Symfony\Bundle\FrameworkBundle\Translation\Translator
        */
        $translator = $this->get("translator");

        $results = array();

        $twigExt = new \AppBundle\Twig\PlayoffRoundNameExtension();

        foreach ($tournamentMatches as $roundNum => $tournamentMatch) {
            $results[$roundNum] = array(
                "roundTitle" => $twigExt->playoffRoundName(
                    $roundNum,
                    $playoffRoundsCount,
                    $translator->trans("Финал", array(), null, $_locale),
                    $translator->trans("Полуфинал", array(), null, $_locale),
                    $translator->trans("финала", array(), null, $_locale)
                )
            );

            foreach ($tournamentMatch as $matchNum => $tMatch) {

                $options = array(
                    "showBetControls" => (
                        $tournamentsHelper->isUserInTournament($playoff, $currentUser) &&
                        $playoffHelper->userInPlayoffRound($currentUser->getId(), $playoff->getTournament()->getId(), $roundNum)
                    )
                );

                if (empty($tMatch))
                {
                    $results[$roundNum]["matches"][$matchNum] = array();
                    continue;
                }

                /**
                * @var \AppBundle\Entity\Match
                */
                $match = $tMatch->getMatch();

                $matchDatetime = $match->getMatchDate()->setTime($match->getMatchTime()->format("H"), $match->getMatchTime()->format("i"), $match->getMatchTime()->format("s"));

                $betData = false;

                if ($currentUser && !empty($usersBets[$match->getId()][$currentUser->getId()]))
                {
                    /**
                    * @var \AppBundle\Entity\Bet
                    */
                    $bet = $usersBets[$match->getId()][$currentUser->getId()];

                    $betData = array(
                        "id" => $bet->getId(),
                        "scoreLeft" => $bet->getScoreLeft(),
                        "scoreRight" => $bet->getScoreRight(),
                        "points" => $bet->getPoints()
                    );
                }

                $results[$roundNum]["matches"][$matchNum] = array(
                    "id" => $match->getId(),
                    "league" => array(
                        "title" => $match->getLeague()->getLeagueName(),
                        "icon" => $match->getLeague()->getIcon(),
                    ),
                    "isStarted" => $match->getIsStarted(),
                    "isRejected" => $match->getIsRejected(),
                    "startDatetime" => $matchDatetime->format("c"),
                    "homeTeamTitle" => $match->getHomeTeam()->getTitle(),
                    "guestTeamTitle" => $match->getGuestTeam()->getTitle(),
                    "scoreLeft" => $match->getScoreLeft(),
                    "scoreRight" => $match->getScoreRight(),
                    "bet" => $betData,
                    "options" => $options
                );
            }
        }

        $response = new JsonResponse();
        $response->setData($results);

        return $response;
    }

    /**
    * Данные с результатами квалификации по туру
    *
    * @param string $_locale Текущая локаль
    * @param int $tournmentId ID турнира
    * @param int $roundNum Номер тура
    * @return JsonResponse
    */
    public function qualificationResultsAction($_locale, $tournamentId, $roundNum)
    {
        /**
        * @var \AppBundle\Entity\TournamentPlayoffRepository
        */
        $playoffRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentPlayoff');

        /**
        * @var TournamentPlayoff
        */
        $playoff = $playoffRep->findLazy($tournamentId);

        if(!$playoff)
            $this->createNotFoundException("Турнир не найден");

        /**
        * @var \AppBundle\Entity\TournamentPlayoffQualificationResultRepository
        */
        $playoffQualificationResultsRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentPlayoffQualificationResult');

        $results = array();

        /**
        * @var \AppBundle\Entity\TournamentChampionshipResult[]
        */
        $resultsSet = $playoffQualificationResultsRep->findByTournamentAndRound($tournamentId, $roundNum);

        if(empty($resultsSet))
        {
            /**
            * @var \AppBundle\Services\Tournaments\Playoff
            */
            $playoffHelper = $this->get("tournament.playoff");
            $resultsSet = $playoffHelper->initQualificationResults($playoff);
        }

        foreach($resultsSet as $result)
        {
            $results[] = array(
                "place" => $result->getPlace(),
                "user" => array(
                    "id" => $result->getUser()->getId(),
                    "nick" => $result->getUser()->getNick(),
                    "avatar" => $result->getUser()->getAvatar(),
                ),
                "totalGuessedResults" => $result->getTotalGuessedResults(),
                "totalGuessedDifferences" => $result->getTotalGuessedDifferences(),
                "totalGuessedOutcomes" => $result->getTotalGuessedOutcomes(),
                "userRating" => $result->getUserRating(),
                "points" => $result->getTotalPoints()
            );
        }

        $response = new JsonResponse();
        $response->setData($results);

        return $response;
    }


    public function gamesAction($_locale, $tournamentId, $roundNum)
    {
        /** @var \AppBundle\Entity\TournamentPlayoffRepository */
        $playoffRep = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentPlayoff");

        $playoff = $playoffRep->findLazy($tournamentId);

        /** @var \AppBundle\Entity\TournamentPlayoffGameRepository */
        $gamesRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentPlayoffGame');

        $games = $gamesRep->findByTournamentAndRound($playoff->getTournament()->getId(), $roundNum);

        if(!$games)
            throw $this->createNotFoundException('Игры данного тура ещё не доступны!');

        $games = array_chunk($games, 2);

        $encoder = new JsonEncoder();
        $normalizer = new GetSetMethodNormalizer();
        $normalizer->setIgnoredAttributes(array('tournament'));
        $normalizer->setCallbacks(array(
            'userLeft' => function(\AppBundle\Entity\User $user) {
                return array(
                    "id" => $user->getId(),
                    "nick" => $user->getNick(),
                    "avatar" => $user->getAvatar(),
                );
            },
            'userRight' => function(\AppBundle\Entity\User $user) {
                return array(
                    "id" => $user->getId(),
                    "nick" => $user->getNick(),
                    "avatar" => $user->getAvatar(),
                );
            }
        ));
        $serializer = new Serializer(array($normalizer), array($encoder));

        $response = new Response($serializer->serialize($games, 'json'));
        return $response;
    }


    public function gameInfoAction($_locale, $tournamentId, $gameId)
    {
        /** @var \AppBundle\Entity\TournamentPlayoffGameRepository */
        $gamesRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentPlayoffGame');
        /** @var \AppBundle\Entity\TournamentPlayoffGame */

        $game = $gamesRep->findOneBy(array('id' => $gameId));

        /**
        * @var \AppBundle\Services\Tournaments\Playoff
        */
        $playoffHelper = $this->get('tournament.playoff');

        $gameInfo = array(
            "userLeft" => array(
                "id" => $game->getUserLeft()->getId(),
                "nick" => $game->getUserLeft()->getNick(),
                "avatar" => $game->getUserLeft()->getAvatar(),
                "totalGuessedResults" => $game->getTotalGuessedResultsLeft(),
                "totalGuessedDiff" => $game->getTotalGuessedDifferencesLeft(),
                "totalGuessedOutcomes" => $game->getTotalGuessedOutcomesLeft(),
                "rating" => $game->getUserRatingLeft()
            ),
            "userRight" => array(
                "id" => $game->getUserRight()->getId(),
                "nick" => $game->getUserRight()->getNick(),
                "avatar" => $game->getUserRight()->getAvatar(),
                "totalGuessedResults" => $game->getTotalGuessedResultsRight(),
                "totalGuessedDiff" => $game->getTotalGuessedDifferencesRight(),
                "totalGuessedOutcomes" => $game->getTotalGuessedOutcomesRight(),
                "rating" => $game->getUserRatingRight()
            ),
            "matches" => $playoffHelper->getGameMatchesWithBets($game)
        );

        $response = new JsonResponse();
        $response->setData($gameInfo);

        return $response;
    }

    /**
    * Сохранение прогноза на матч
    *
    * @param string $_locale Текущая локаль
    * @param \AppBundle\Entity\Tournament $tournament
    * @param \AppBundle\Entity\Match $match
    * @param int $scoreLeft
    * @param int $scoreRight
    * @return JsonResponse
    */
    public function saveBetAction($_locale, $tournament, $match, $scoreLeft, $scoreRight)
    {
        /**
        * @var \AppBundle\Entity\TournamentPlayoffMatchRepository
        */
        $playoffMatchRep = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentPlayoffMatch");

        if(!$playoffMatchRep->isMatchInTournament($tournament->getId(), $match->getId()))
            throw $this->createNotFoundException('Данный матч не найден в данном турнире!');

        /**
        * @var \AppBundle\Entity\TournamentPlayoff
        */
        $playoff = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentPlayoff")->findLazy($tournament->getId());

        /**
        * @var \AppBundle\Services\TournamentsHelper
        */
        $tournamentsHelper = $this->get('tournaments_helper');

        /**
        * @var \AppBundle\Services\Tournaments\Playoff
        */
        $playoffHelper = $this->get("tournament.playoff");

        /**
        * @var \AppBundle\Entity\User $currentUser
        */
        $currentUser = $this->get('security.context')->getToken()->getUser();

        if (!$tournamentsHelper->isUserInTournament($playoff, $currentUser)) {
            throw $this->createNotFoundException('Текущий пользователь не участвует в данном турнире!');
        }

        /** @var \AppBundle\Entity\TournamentPlayoffMatch */
        $playoffMatch = $playoffMatchRep->findOneBy(array(
            "tournament" => $tournament->getId(),
            "match" => $match->getId()
        ));

        if ($playoff->getStage() == $playoff::STAGE_PLAYOFF && !$playoffHelper->userInPlayoffRound($currentUser->getId(), $tournament->getId(), $playoffMatch->getRound())) {
            throw $this->createNotFoundException('Текущий пользователь не участвует в данном туре!');
        }

        /**
        * @var \AppBundle\Entity\Bet $bet
        */
        if(!$bet = $this->getDoctrine()->getRepository('AppBundle\Entity\Bet')->findByTournamentMatchAndUser($tournament->getId(), $currentUser->getId(), $match->getId()))
        {
            $bet = new Bet();
            $bet->setTournament($tournament);
            $bet->setUser($currentUser);
            $bet->setMatch($match);
        }

        $bet->setScoreLeft($scoreLeft);
        $bet->setScoreRight($scoreRight);

        try
        {
            /**
            * @var \Doctrine\ORM\EntityManager $em
            */
            $em = $this->getDoctrine()->getManager();
            $em->persist($bet);
            $em->flush();

            $response = new JsonResponse();
            $response->setData(array("success" => true));

            return $response;
        } catch (Exception $e) {
            throw $this->createNotFoundException('Произошла непредвиденная ошибка во время сохранения прогноза!');
        }
    }

    public function infoAction($_locale, $tournament_id)
    {
        /**
        * @var \AppBundle\Entity\TournamentPlayoffRepository $tournamentRep
        */
        $tournamentRep = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentPlayoff");

        /**
        * @var \AppBundle\Entity\TournamentPlayoff $tournament
        */
        $tournament = $tournamentRep->findLazy($tournament_id);

        if($tournament->getTournament()->getIsStarted())
        {
            $usersCount = count($tournament->getTournament()->getUsers());
            $playoffRoundsCount = round(log($tournament->getRealUsersOutOfQualification(), 2));
        }
        else
        {
            $usersCount = $tournament->getMaxUsersCount();
            $playoffRoundsCount = round(log($tournament->getMaxUsersCount(), 2));
        }

        $winDistribution = $this->getDoctrine()->getRepository("AppBundle\Entity\WinRule")->findByFormatAndWinnersCount("playoff", $usersCount);
        return $this->render("AppBundle:frontend/tournaments/playoff:info.html.twig", array(
            "tournament" => $tournament,
            "winDistribution" => $winDistribution,
            "playoffRoundsCount" => $playoffRoundsCount
        ));
    }

    public function winnersAction($_locale, \AppBundle\Entity\Tournament $tournament)
    {
        /**
        * @var TournamentPlayoff $playoff
        */
        $playoff = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentPlayoff')->findLazy($tournament->getId());

        $results = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentResult')->findDistributed($tournament->getId());

        return $this->render('AppBundle:frontend/tournaments/playoff:winners.html.twig', array(
            'tournament' => $playoff,
            'results' => $results
        ));
    }

    public function windistributionAction($_locale, $tournament_id)
    {
        /**
        * @var \AppBundle\Entity\TournamentPlayoffRepository $tournamentRep
        */
        $tournamentRep = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentPlayoff");

        /**
        * @var \AppBundle\Entity\TournamentPlayoff $tournament
        */
        $tournament = $tournamentRep->findLazy($tournament_id);

        if(!$tournament->getTournament()->getIsStarted())
            $usersCount = $tournament->getMaxUsersCount();
        else
            $usersCount = $tournament->getRealUsersOutOfQualification();

        $winDistribution = $this->getDoctrine()->getRepository("AppBundle\Entity\WinRule")->findByFormatAndWinnersCount("championsleague", $usersCount);
        return $this->render("AppBundle:frontend/tournaments/playoff:windistribution.html.twig", array(
            "tournament" => $tournament,
            "winDistribution" => $winDistribution
        ));
    }
}