<?php
namespace AppBundle\Controller\Tournaments;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Entity\Bet;

class RoundController extends Controller
{
    /**
    * Карточка турнира
    *
    * @param string $_locale Текущая локаль
    * @param int $tournament_id ID турнира
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function viewAction($_locale, $tournament_id)
    {
        /**
        * @var \AppBundle\Entity\TournamentChampionshipRepository $tournamentRep
        */
        $tournamentRep = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentRound");

        /**
        * @var \AppBundle\Entity\TournamentChampionship $tournament
        */
        $tournament = $tournamentRep->findLazy($tournament_id);

        /**
        * @var \AppBundle\Services\TournamentsHelper $tournamentsHelper
        */
        $tournamentsHelper = $this->get('tournaments_helper');

        /**
        * @var \AppBundle\Entity\User $currentUser
        */
        $currentUser = $this->get('security.context')->getToken()->getUser();

        if(!$tournamentsHelper->userCanViewTournament($tournament, $currentUser))
            throw $this->createNotFoundException('Туринир не найден!');

        return $this->render("AppBundle:frontend/tournaments/round:view.html.twig", array(
            "tournament" => $tournament,
            "tournamentStatus" => $tournamentsHelper->getTournamentStatus($tournament->getTournament())
        ));
    }

    /**
    * Список матчей распределённых по турам
    *
    * @param string $_locale Текущая локаль
    * @param mixed $tournamentId
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function matchesAction($_locale, $tournamentId)
    {
        /**
        * @var \AppBundle\Entity\TournamentRoundRepository
        */
        $roundRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentRound');

        /**
        * @var \AppBundle\Entity\TournamentRound $round
        */
        $round = $roundRep->findLazy($tournamentId);

        if(!$round)
            $this->createNotFoundException("Турнир не найден");

        /**
        * @var \AppBundle\Entity\TournamentRoundMatchRepository
        */
        $matchesRep = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentRoundMatch");
        $tournamentMatches = $matchesRep->findByTournament($round->getTournament()->getId());

        /**
        * @var \AppBundle\Services\Tournaments\Round
        */
        $roundHelper = $this->get("tournament.round");

        $roundsCount = $round->getMaxUsersCount();
        if($round->getTournament()->getIsStarted())
            $roundsCount = $round->getRealRoundsCount();

        $tournamentMatches = $roundHelper->chunkMatches($tournamentMatches, $roundsCount, $round->getMatchesInGame());

        /**
        * @var \AppBundle\Services\TournamentsHelper
        */
        $tournamentsHelper = $this->get("tournaments_helper");

        /**
        * @var \AppBundle\Entity\User
        */
        $currentUser = $this->getUser();

        $usersBets = array();

        if($tournamentsHelper->isUserInTournament($round, $currentUser))
        {
            /**
            * @var \AppBundle\Entity\BetRepository
            */
            $betsRep = $this->getDoctrine()->getRepository("AppBundle\Entity\Bet");

            /**
            * @var \AppBundle\Services\BetsHelper
            */
            $betsHelper = $this->get("bets_helper");

            $bets = $betsRep->findByTournamentAndUser($round->getTournament()->getId(), $currentUser->getId());

            $usersBets = $betsHelper->rebuildByMatches($bets);
        }

        /**
        * @var \Symfony\Bundle\FrameworkBundle\Translation\Translator
        */
        $translator = $this->get("translator");

        $results = array();

        foreach($tournamentMatches as $roundNum => $tournamentMatch)
        {
            $results[$roundNum] = array(
                "roundTitle" => $translator->trans("Тур %s%", array("%s%" => $roundNum), null, $_locale)
            );

            foreach($tournamentMatch as $matchNum => $tMatch)
            {
                $options = array(
                    "showBetControls" => $tournamentsHelper->isUserInTournament($round, $currentUser)
                );

                if(empty($tMatch))
                {
                    $results[$roundNum]["matches"][$matchNum] = array();
                    continue;
                }

                /**
                * @var \AppBundle\Entity\Match
                */
                $match = $tMatch->getMatch();

                $matchDatetime = $match->getMatchDate()->setTime($match->getMatchTime()->format("H"), $match->getMatchTime()->format("i"), $match->getMatchTime()->format("s"));

                $betData = false;

                if($currentUser && !empty($usersBets[$match->getId()][$currentUser->getId()]))
                {
                    /**
                    * @var \AppBundle\Entity\Bet
                    */
                    $bet = $usersBets[$match->getId()][$currentUser->getId()];

                    $betData = array(
                        "id" => $bet->getId(),
                        "scoreLeft" => $bet->getScoreLeft(),
                        "scoreRight" => $bet->getScoreRight(),
                        "points" => $bet->getPoints()
                    );
                }

                $results[$roundNum]["matches"][$matchNum] = array(
                    "id" => $match->getId(),
                    "league" => array(
                        "title" => $match->getLeague()->getLeagueName(),
                        "icon" => $match->getLeague()->getIcon(),
                    ),
                    "isStarted" => $match->getIsStarted(),
                    "isRejected" => $match->getIsRejected(),
                    "startDatetime" => $matchDatetime->format("c"),
                    "homeTeamTitle" => $match->getHomeTeam()->getTitle(),
                    "guestTeamTitle" => $match->getGuestTeam()->getTitle(),
                    "scoreLeft" => $match->getScoreLeft(),
                    "scoreRight" => $match->getScoreRight(),
                    "bet" => $betData,
                    "options" => $options
                );
            }
        }

        $response = new JsonResponse();
        $response->setData($results);

        return $response;
    }

    /**
    * Данные с результатами турнира по туру
    *
    * @param string $_locale Текущая локаль
    * @param int $tournmentId ID турнира
    * @param int $roundNum Номер тура
    * @return JsonResponse
    */
    public function resultsAction($_locale, $tournamentId, $roundNum)
    {
        $request = $this->getRequest();

        /**
        * @var \AppBundle\Entity\TournamentRoundRepository
        */
        $roundRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentRound');

        /**
        * @var TournamentRound
        */
        $round = $roundRep->findLazy($tournamentId);

        if(!$round)
            $this->createNotFoundException("Турнир не найден");

        /**
        * @var \AppBundle\Entity\TournamentRoundResultRepository
        */
        $roundResultsRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentRoundResult');

        $results = array();

        /**
        * @var \AppBundle\Entity\TournamentRoundResult[]
        */
        $resultsSet = $roundResultsRep->findByTournamentAndRound($tournamentId, $roundNum);

        if(empty($resultsSet))
        {
            /**
            * @var \AppBundle\Services\Tournaments\Round
            */
            $roundHelper = $this->get("tournament.round");
            $resultsSet = $roundHelper->initResults($round);
        }

        foreach($resultsSet as $result)
        {
            $results[] = array(
                "place" => $result->getPlace(),
                "user" => array(
                    "id" => $result->getUser()->getId(),
                    "nick" => $result->getUser()->getNick(),
                    "avatar" => $result->getUser()->getAvatar(),
                ),
                "totalGames" => $result->getTotalGames(),
                "totalWins" => $result->getTotalWins(),
                "totalDraws" => $result->getTotalDraws(),
                "totalLoss" => $result->getTotalLoss(),
                "totalGoalsAgainst" => $result->getTotalGoalsAgainst(),
                "totalGoalsScored" => $result->getTotalGoalsScored(),
                "totalGoalsDifference" => $result->getTotalGoalsDifference(),
                "userRating" => $result->getUserRating(),
                "points" => $result->getTotalPoints()
            );
        }

        $response = new JsonResponse();
        $response->setData($results);

        return $response;
    }

    /**
    * Список игр
    *
    * @param string $_locale Текущая локаль
    * @param int $tournmentId ID турнира
    * @param int $roundNum Номер тура
    * @return JsonResponse
    */
    public function gamesAction($_locale, $tournamentId, $roundNum = 1)
    {
        /**
        * @var TournamentRound
        */
        $roundTournament = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentRound')->findLazy($tournamentId);

        /**
        * @var \AppBundle\Entity\TournamentRoundGameRepository
        */
        $gamesRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentRoundGame');

        /**
        * @var \AppBundle\Entity\TournamentRoundGame[]
        */
        $gamesArr = $gamesRep->findByTournamentAndRound($tournamentId, $roundNum);

        $roundMatches = array();

        if($roundNum != 1 && !$gamesArr)
            $this->createNotFoundException("Игры для данного тура ещё не сгенерированы");

        if(!$gamesArr)
        {
            /**
            * @var \AppBundle\Services\Tournaments\Round
            */
            $roundHelper = $this->get('tournament.round');
            $gamesArr = $roundHelper->initGames($roundTournament);
        }

        $games = array();
        foreach($gamesArr as $game)
        {
            $games[] = array(
                "id" => $game->getId(),
                "userLeft" => array(
                    "id" => $game->getUserLeft()->getId(),
                    "nick" => $game->getUserLeft()->getNick(),
                    "avatar" => $game->getUserLeft()->getAvatar(),
                ),
                "userRight" => array(
                    "id" => $game->getUserRight()->getId(),
                    "nick" => $game->getUserRight()->getNick(),
                    "avatar" => $game->getUserRight()->getAvatar(),
                ),
                "scoreLeft" => $game->getPointsLeft(),
                "scoreRight" => $game->getPointsRight()
            );
        }

        $response = new JsonResponse();
        $response->setData($games);

        return $response;
    }

    /**
    * Информация о матчах и ставках участников игры
    *
    * @param string $_locale Текущая локаль
    * @param int $tournamentId ID турнира
    * @param int $gameId ID игры
    * @return JsonResponse
    */
    public function gameInfoAction($_locale, $tournamentId, $gameId)
    {
        /**
        * @var \AppBundle\Entity\TournamentRoundGameRepository
        */
        $gamesRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentRoundGame');

        /**
        * @var \AppBundle\Entity\TournamentRoundGame
        */
        $game = $gamesRep->find($gameId);

        if(!$game)
            $this->createNotFoundException("Игра не найдена");

        /**
        * @var \AppBundle\Services\Tournaments\Round
        */
        $roundHelper = $this->get("tournament.round");

        $matchesWithBets = $roundHelper->getGameMatchesWithBets($game);

        $response = new JsonResponse();
        $response->setData($matchesWithBets);

        return $response;
    }

    public function infoAction($_locale, $tournament_id)
    {
        /**
        * @var \AppBundle\Entity\TournamentRoundRepository $tournamentRep
        */
        $tournamentRep = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentRound");

        /**
        * @var \AppBundle\Entity\TournamentRound $tournament
        */
        $tournament = $tournamentRep->findLazy($tournament_id);

        if(!$tournament->getTournament()->getIsStarted())
            $usersCount = $tournament->getMaxUsersCount();
        else
            $usersCount = count($tournament->getTournament()->getUsers());

        $winDistribution = $this->getDoctrine()->getRepository("AppBundle\Entity\WinRule")->findByFormatAndWinnersCount("round", $usersCount);
        return $this->render("AppBundle:frontend/tournaments/round:info.html.twig", array(
            "tournament" => $tournament,
            "winDistribution" => $winDistribution
        ));
    }

    /**
    * Сохранение прогноза на матч
    *
    * @param string $_locale Текущая локаль
    * @param \AppBundle\Entity\Tournament $tournament
    * @param \AppBundle\Entity\Match $match
    * @param int $scoreLeft
    * @param int $scoreRight
    * @return JsonResponse
    */
    public function saveBetAction($_locale, $tournament, $match, $scoreLeft, $scoreRight)
    {
        /**
        * @var \AppBundle\Entity\TournamentRoundMatchRepository
        */
        $roundMatchRep = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentRoundMatch");

        if(!$roundMatchRep->isMatchInTournament($tournament->getId(), $match->getId()))
            throw $this->createNotFoundException('Данный матч не найден в данном турнире или туре!');

        /**
        * @var \AppBundle\Entity\TournamentRound
        */
        $round = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentRound")->findLazy($tournament->getId());

        /**
        * @var \AppBundle\Services\TournamentsHelper $tournamentsHelper
        */
        $tournamentsHelper = $this->get('tournaments_helper');

        /**
        * @var \AppBundle\Entity\User $currentUser
        */
        $currentUser = $this->get('security.context')->getToken()->getUser();

        if(!$tournamentsHelper->isUserInTournament($round, $currentUser))
            throw $this->createNotFoundException('Текущий пользователь не участвует в данном турнире!');

        /**
        * @var \AppBundle\Entity\Bet $bet
        */
        if(!$bet = $this->getDoctrine()->getRepository('AppBundle\Entity\Bet')->findByTournamentMatchAndUser($tournament->getId(), $currentUser->getId(), $match->getId()))
        {
            $bet = new Bet();
            $bet->setTournament($tournament);
            $bet->setUser($currentUser);
            $bet->setMatch($match);
        }

        $bet->setScoreLeft($scoreLeft);
        $bet->setScoreRight($scoreRight);

        try
        {
            /**
            * @var \Doctrine\ORM\EntityManager $em
            */
            $em = $this->getDoctrine()->getManager();
            $em->persist($bet);
            $em->flush();

            $response = new JsonResponse();
            $response->setData(array("success" => true));

            return $response;
        } catch (Exception $e) {
            throw $this->createNotFoundException('Произошла непредвиденная ошибка во время сохранения прогноза!');
        }
    }

    public function winnersAction($_locale, \AppBundle\Entity\Tournament $tournament)
    {
        /**
        * @var TournamentRound $roundTournament
        */
        $roundTournament = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentRound')->findLazy($tournament->getId());

        $results = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentResult')->findDistributed($tournament->getId());

        return $this->render('AppBundle:frontend/tournaments/round:winners.html.twig', array(
            'tournament' => $roundTournament,
            'results' => $results
        ));
    }

    public function windistributionAction($_locale, $tournament_id)
    {
        /**
        * @var \AppBundle\Entity\TournamentRoundRepository $tournamentRep
        */
        $tournamentRep = $this->getDoctrine()->getRepository("AppBundle\Entity\TournamentRound");

        /**
        * @var \AppBundle\Entity\TournamentChampionship $tournament
        */
        $tournament = $tournamentRep->findLazy($tournament_id);

        if(!$tournament->getTournament()->getIsStarted())
            $usersCount = $tournament->getMaxUsersCount();
        else
            $usersCount = count($tournament->getTournament()->getUsers());

        $winDistribution = $this->getDoctrine()->getRepository("AppBundle\Entity\WinRule")->findByFormatAndWinnersCount("championship", $usersCount);
        return $this->render("AppBundle:frontend/tournaments/round:windistribution.html.twig", array(
            "tournament" => $tournament,
            "winDistribution" => $winDistribution
        ));
    }
}