<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class TournamentsController extends Controller
{
    /**
    * Максимальный размер выдаваемого кол-ва результатов на странице
    */
    const MAX_PAGE_SIZE = 100;

    /**
    * Поиск турниров
    *
    * @param string $_locale Текущая локаль.
    *
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function searchAction($_locale)
    {
        return $this->render('AppBundle:frontend/tournaments:search.html.twig');
    }

    /**
    * Поиск турниров
    *
    * @param string $_locale Текущая локаль
    * @param string $page Текущая страница паджинации
    * @return JsonResponse
    */
    public function findAction($_locale)
    {
        $request = $this->getRequest();

        $payload = json_decode($request->getContent(), true);


        $page = (isset($payload["page"])) ? $payload["page"] : 1;
        $filter = (isset($payload["filter"])) ? $payload["filter"] : array();
        $order = (isset($payload["order"])) ? $payload["order"] : array();
        $options = (isset($payload["options"])) ? $payload["options"] : array("pageSize" => 10);

        $filter['isReady'] = true;

        /**
        * @var \Symfony\Bundle\FrameworkBundle\Translation\Translator
        */
        $translator = $this->get("translator");

        /**
        * @var \AppBundle\Services\TournamentsHelper
        */
        $tournamentsHelper = $this->get("tournaments_helper");

        /**
        * @var \AppBundle\Entity\TournamentRepository
        */
        $tournamentsRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament');
        
        /** @var \AppBundle\Entity\Tournament[] */
        $resultSet = $tournamentsRep->findPaginator($order, $filter, $page, $options["pageSize"]);

        $total = count($resultSet);

        $tournaments = array();
        foreach($resultSet as $tournament)
        {
            $tournaments[] = array(
                "id" => $tournament->getId(),
                "url" => $this->generateUrl("tournament/view", array( "_locale" => $_locale, "tournament_id" =>  $tournament->getId())),
                "title" => $tournament->getTitle(),
                "fee" => $tournament->getFee(),
                "format" => $translator->trans($tournament->getFormat()->getTitle(), array(), null, $_locale),
                "startDatetime" => $tournament->getStartDatetime()->format("c"),
                "usersCount" => count($tournament->getUsers()),
                "prizePool" => $tournamentsHelper->getTournamentPrizePool($tournament),
                "isPromo" => $tournament->getIsPromo(),
                "status" => $tournamentsHelper->getTournamentStatus($tournament)
            );
        }

        $response = new JsonResponse();
        $response->setData(array(
            "total" => $total,
            "items" => $tournaments
        ));

        return $response;
    }

    /**
    * Просмотр турнира
    *
    * @param string $_locale Текущая локаль
    * @param int $tournament_id ID турнира
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function viewAction($_locale, $tournament_id)
    {
        $format = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament')->getFormatByTournament($tournament_id);

        if(!$format)
            throw $this->createNotFoundException('Туринир не найден!');

        $format = ucfirst(strtolower($format));

        $response = $this->forward('AppBundle:Tournaments/'.$format.':view', array(
            '_locale'  => $_locale,
            'tournament_id' => $tournament_id,
        ));

        return $response;
    }

    /**
    * Лиги турнира
    *
    * @param string $_locale Текущая локаль
    * @param int $tournament_id ID турнира
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function tournamentLeaguesAction($_locale, $tournament_id)
    {
        $tournamentLeagues = $this->getDoctrine()->getRepository('AppBundle\Entity\League')->findByTournament($tournament_id);

        $leaguesByIcons = array();
        /**
        * @var $tournamentLeagues \AppBundle\Entity\League[]
        */
        foreach($tournamentLeagues as $league)
        {
            $leaguesByIcons[$league->getIcon()][$league->getId()] = $league->getTournament();
        }

        return $this->render("AppBundle:common/tournament:geography.html.twig", array(
            "leaguesByIcons" => $leaguesByIcons
        ));
    }

    /**
    * Победители турнира
    *
    * @param string $_locale Текущая локаль
    * @param int $tournamentId ID турнира
    * @return JsonResponse
    */
    public function winnersAction($_locale, $tournamentId)
    {
        $request = $this->getRequest();

        /**
        * @var \AppBundle\Entity\TournamentResultRepository
        */
        $tournamentResultRep = $this->getDoctrine()->getRepository('AppBundle\Entity\TournamentResult');

        /**
        * @var \AppBundle\Entity\TournamentResult[]
        */
        $results = $tournamentResultRep->findDistributed($tournamentId);

        $winners = array();
        foreach($results as $winResult)
        {
            $winners[] = array(
                "place" => $winResult->getPlace(),
                "user" => array(
                    "id" => $winResult->getUser()->getId(),
                    "nick" => $winResult->getUser()->getNick(),
                    "avatar" => $winResult->getUser()->getAvatar(),
                ),
                "percent" => $winResult->getPercent(),
                "sum" => $winResult->getSum()
            );
        }

        $response = new JsonResponse();
        $response->setData($winners);

        return $response;
    }

    /**
    * Список матчей распределённых по турам
    *
    * @param string $_locale Текущая локаль
    * @param mixed $tournamentId
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function matchesAction($_locale, $tournamentId)
    {
        $format = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament')->getFormatByTournament($tournamentId);

        if(!$format)
            throw $this->createNotFoundException('Туринир не найден!');

        $format = ucfirst(strtolower($format));

        return $this->forward('AppBundle:Tournaments/'.$format.':matches', array(
            '_locale'  => $_locale,
            'tournamentId' => $tournamentId
        ));
    }

    /**
    * Сохранение прогнорза
    *
    * @param string $_locale Текущая локаль
    * @param int tournamentId  ID турнира
    * @param int matchId ID матча
    * @return JsonResponse
    */
    public function saveBetAction($_locale, $tournamentId, $matchId)
    {
        $request = $this->getRequest();

        $requestData = json_decode($request->getContent(), true);

        if(isset($requestData["scoreLeft"]))
            $scoreLeft = $requestData["scoreLeft"];
        if(isset($requestData["scoreRight"]))
            $scoreRight = $requestData["scoreRight"];

        if(!isset($scoreLeft) || !isset($scoreRight))
            throw $this->createNotFoundException('Не переданы данные прогнозов!');

        $scoreLeft = abs((int) $scoreLeft);
        $scoreRight = abs((int) $scoreRight);

        /**
        * @var \AppBundle\Entity\Match $match
        */
        $match = $this->getDoctrine()->getRepository('AppBundle\Entity\Match')->find($matchId);
        if(!$match)
            throw $this->createNotFoundException('Матч с переданным id не существует!');

        if($match->getIsStarted())
        {
            /**
            * @var \Symfony\Bundle\FrameworkBundle\Translation\Translator
            */
            $translator = $this->get("translator");
            return new JsonResponse(array("success" => false, "msg" => $translator->trans("Матч уже начался. Приём прогнозов завершён.", array(), null, $_locale)));
        }

        /**
        * @var \AppBundle\Entity\Tournament $tournament
        */
        $tournament = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament')->find($tournamentId);

        if(!$tournament || $tournament->getIsOver() || $tournament->getIsReject())
            throw $this->createNotFoundException('Турнир окончен или отменён!');

        $format = ucfirst(strtolower($tournament->getFormat()->getInternalName()));

        return $this->forward('AppBundle:Tournaments/'.$format.':saveBet', array(
            '_locale'  => $_locale,
            'tournament' => $tournament,
            'match' => $match,
            'scoreLeft' => $scoreLeft,
            'scoreRight' => $scoreRight,
        ));
    }

    /**
    * Список прогнозов на матч
    *
    * @param string $_locale Текущая локаль
    * @param int tournamentId  ID турнира
    * @param int matchId ID матча
    * @return \Symfony\Component\HttpFoundation\Response
    */
    public function usersBetsAction($_locale, $tournamentId, $matchId)
    {
        /**
        * @var \AppBundle\Entity\Match $match
        */
        $match = $this->getDoctrine()->getRepository('AppBundle\Entity\Match')->find($matchId);

        if(!$match)
            throw $this->createNotFoundException('Матч не найден!');

        if(!$match->getIsStarted())
            throw $this->createNotFoundException('Нельзя увидеть ставки участников пока матч не начался!');

        /**
        * @var \AppBundle\Entity\Bet[]
        */
        $bets = $this->getDoctrine()->getRepository('AppBundle\Entity\Bet')->findByTournamentAndMatch($tournamentId, $match->getId());

        $usersBets = array();

        foreach($bets as $bet)
        {
            $usersBets[] = array(
                "user" => array(
                    "id" => $bet->getUser()->getId(),
                    "nick" => $bet->getUser()->getNick(),
                ),
                "score" => $bet->getScoreLeft() . " — " . $bet->getScoreRight(),
                "points" => $bet->getPoints()
            );
        }

        $response = new JsonResponse();
        $response->setData($usersBets);

        return $response;
    }


    /**
    * Регламент турнира
    *
    * @param string $_locale Текущая локаль
    * @param int $tournamentId ID турнира
    * @return JsonResponse
    */
    public function regulationsAction($_locale, $tournamentId)
    {
        /**
        * @var \AppBundle\Entity\TournamentRepository
        */
        $tournamentRep = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament');
        /**
        * @var \AppBundle\Entity\Tournament
        */
        $tournament = $tournamentRep->find($tournamentId);

        if(!$tournament) {
            throw $this->createNotFoundException('Туринир не найден!');
        }

        $format = $tournament->getFormat();        
        $regulation = $tournament->getRegulation();
        
        /** @var \AppBundle\Services\PolicyHelper */
        $policyHelper = $this->get('policy_helper');
        $policyHelper->renderRegulation($regulation);
                
        /**
        * @var \Symfony\Bundle\FrameworkBundle\Translation\Translator
        */
        $translator = $this->get("translator");

        $response = new JsonResponse();
        $response->setData(array(
            "title" => $translator->trans('Регламент турнира типа "%s%"', array("%s%" => $format->getTitle()), null, $_locale),
            "body" => $regulation->getText()
        ));

        return $response;
    }

    /**
    * Распределение призовых
    *
    * @param string $_locale Текущая локаль
    * @param int $tournamentId ID турнира
    * @return JsonResponse
    */
    public function winDistributionAction($_locale, $tournamentId)
    {
        $format = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament')->getFormatByTournament($tournamentId);

        if(!$format)
            throw $this->createNotFoundException('Туринир не найден!');

        $format = ucfirst(strtolower($format));

        $result = $this->forward('AppBundle:Tournaments/'.$format.':windistribution', array(
            '_locale'  => $_locale,
            'tournament_id' => $tournamentId,
        ));

        /**
        * @var \Symfony\Bundle\FrameworkBundle\Translation\Translator
        */
        $translator = $this->get("translator");

        $response = new JsonResponse();
        $response->setData(array(
            "title" => $translator->trans('Распределение призовых', array(), null, $_locale),
            "body" => $result->getContent()
        ));

        return $response;
    }

    public function requestAction($tournament_id)
    {
        $format = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament')->getFormatByTournament($tournament_id);

        if(!$format)
            throw $this->createNotFoundException('Туринир не найден!');

        $format = ucfirst(strtolower($format));

        $tournament = $this->getDoctrine()->getRepository('AppBundle\Entity\Tournament'.$format)->findLazy($tournament_id);

        if($tournament->getTournament()->getIsStarted() ||
            $tournament->getTournament()->getIsOver() ||
            $tournament->getTournament()->getIsReject()
        )
            throw $this->createNotFoundException('Туринир недоступен для подачи заявки!');

        /**
        * @var \AppBundle\Entity\User $currentUser
        */
        $currentUser = $this->get('security.context')->getToken()->getUser();

        /**
        * @var \AppBundle\Services\TournamentsHelper $tournamentsHelper
        */
        $tournamentsHelper = $this->get('tournaments_helper');

        if(!$tournament->getTournament()->getWithConfirmation() && !$tournamentsHelper->isUserInTournament($tournament, $currentUser))
        {
            $tournament->getTournament()->addUser($currentUser);
        }
        elseif($tournamentsHelper->userCanSendRequest($tournament, $currentUser))
        {
            $tournament->getTournament()->addUserRequest($currentUser);
        }
        else
        {
            throw $this->createNotFoundException('Туринир недоступен для подачи заявки!');
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($tournament->getTournament());
        $em->flush();

        $fee = $tournament->getTournament()->getFee();
        if($fee > 0)
        {
            /**
            * @var \AppBundle\Services\BalanceHelper
            */
            $BalanceHelper = $this->get('balance_helper');
            $note = $BalanceHelper->getReservFeeNote($tournament->getTournament(), $fee);
            $bill = $BalanceHelper->createTournamentReserveFeeBill($currentUser, $fee, $note);
            $BalanceHelper->processBill($bill);
        }

        return $this->redirect($this->generateUrl('tournament/view', array('tournament_id' => $tournament->getTournament()->getId())));
    }
}
