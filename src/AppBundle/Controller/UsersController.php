<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UsersController extends Controller
{
    public function ratingListAction($maxCount = null)
    {
        $users = $this->getDoctrine()->getRepository('AppBundle\Entity\User')->getRatingList($maxCount);        

        return $this->render('AppBundle:frontend/users:rating_list.html.twig', array(
                'users' => $users
            )
        );
    }
    
    public function infoAction($_locale, $user_id)
    {
        $user = $this->getDoctrine()->getRepository('AppBundle\Entity\User')->findById($user_id);
        
        if(!$user)
            throw $this->createNotFoundException('Пользователь не найден!');
        
        return $this->render("AppBundle:common/user:info.html.twig", array(
            "user" => $user,
        ));
    }
}