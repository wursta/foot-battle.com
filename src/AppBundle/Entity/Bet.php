<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AppBundle\Entity\Bet
 *
 * @ORM\Table(name="tournament_user_bets")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\BetRepository")
 */
class Bet
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")     
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Tournament", inversedBy="bets")
     * @ORM\JoinColumn(name="tournament_id", referencedColumnName="id")
     */
    protected $tournament;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="bets")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
    
    /**
     * @ORM\ManyToOne(targetEntity="Match", inversedBy="bets")
     * @ORM\JoinColumn(name="match_id", referencedColumnName="id")
     */
    protected $match;      
    
    /**
     * @ORM\Column(type="integer")     
     */
    private $score_left;

    /**
     * @ORM\Column(type="integer")     
     */
    private $score_right;    
    
    /**
     * @ORM\Column(type="float")     
     */
    private $points;    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set score_left
     *
     * @param integer $scoreLeft
     * @return Bet
     */
    public function setScoreLeft($scoreLeft)
    {
        $this->score_left = $scoreLeft;
    
        return $this;
    }

    /**
     * Get score_left
     *
     * @return integer 
     */
    public function getScoreLeft()
    {
        return $this->score_left;
    }

    /**
     * Set score_right
     *
     * @param integer $scoreRight
     * @return Bet
     */
    public function setScoreRight($scoreRight)
    {
        $this->score_right = $scoreRight;
    
        return $this;
    }

    /**
     * Get score_right
     *
     * @return integer 
     */
    public function getScoreRight()
    {
        return $this->score_right;
    }

    /**
     * Set tournament
     *
     * @param \AppBundle\Entity\Tournament $tournament
     * @return Bet
     */
    public function setTournament(\AppBundle\Entity\Tournament $tournament = null)
    {
        $this->tournament = $tournament;
    
        return $this;
    }

    /**
     * Get tournament
     *
     * @return \AppBundle\Entity\Tournament 
     */
    public function getTournament()
    {
        return $this->tournament;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return Bet
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set match
     *
     * @param \AppBundle\Entity\Match $match
     * @return Bet
     */
    public function setMatch(\AppBundle\Entity\Match $match = null)
    {
        $this->match = $match;
    
        return $this;
    }

    /**
     * Get match
     *
     * @return \AppBundle\Entity\Match 
     */
    public function getMatch()
    {
        return $this->match;
    }

    /**
     * Set points
     *
     * @param float $points
     * @return Bet
     */
    public function setPoints($points)
    {
        $this->points = $points;
    
        return $this;
    }

    /**
     * Get points
     *
     * @return float 
     */
    public function getPoints()
    {
        return $this->points;
    }
}
