<?php
namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

class BetRepository extends EntityRepository
{
    /**
    * Поиск прогнозов пользователя в турнире
    *
    * @param int $tournament_id ID турнира.
    * @param int $user_id       ID пользователя.
    * 
    * @return Bet[]
    */
    public function findByTournamentAndUser($tournament_id, $user_id)
    {
        $q = $this->createQueryBuilder('b')
                  ->select('b, u, m')
                  ->leftJoin('b.tournament', 't')
                  ->leftJoin('b.user', 'u')
                  ->leftJoin('b.match', 'm')
                  ->where('t.id = :tournament_id')
                  ->setParameter('tournament_id', $tournament_id)
                  ->andWhere('u.id = :user_id')
                  ->setParameter('user_id', $user_id)
                  ->getQuery();

        try {
            return $q->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    public function isBetUnique($tournament_id, $user_id, $match_id)
    {
        $q = $this
                ->createQueryBuilder('b')
                ->select('COUNT(b.id) as cnt')
                ->where('b.tournament = :tournament_id')
                ->andWhere('b.user = :user_id')
                ->andWhere('b.match = :match_id')
                ->setParameter('tournament_id', $tournament_id)
                ->setParameter('user_id', $user_id)
                ->setParameter('match_id', $match_id)
                ->getQuery();

            try {
                    $result = $q->getSingleResult();
                    return (bool) !$result["cnt"];
                } catch (NoResultException $e) {

                    return false;
                }
    }

    public function findByTournamentMatchAndUser($tournament_id, $user_id, $match_id)
    {
        $q = $this
                ->createQueryBuilder('b')
                ->where('b.tournament = :tournament_id')
                ->andWhere('b.user = :user_id')
                ->andWhere('b.match = :match_id')
                ->setParameter('tournament_id', $tournament_id)
                ->setParameter('user_id', $user_id)
                ->setParameter('match_id', $match_id)
                ->getQuery();

            try {
                    return $q->getSingleResult();
                } catch (NoResultException $e) {

                    return null;
                }
    }

    public function findByTournamentAndMatch($tournament_id, $match_id)
    {
        $q = $this
                ->createQueryBuilder('b')
                ->where('b.tournament = :tournament_id')
                ->andWhere('b.match = :match_id')
                ->setParameter('tournament_id', $tournament_id)
                ->setParameter('match_id', $match_id)
                ->orderBy('b.points', 'DESC')
                ->getQuery();

            try {
                    return $q->getResult();
                } catch (NoResultException $e) {

                    return null;
                }
    }

    public function findByFinishedTournaments()
    {
        $q = $this
                ->createQueryBuilder('b')
                ->leftJoin('b.tournament', 't')
                ->andWhere('t.isReady = :isReady')
                ->andWhere('t.isStarted = :isStarted')
                ->andWhere('t.isOver = :isOver')
                ->andWhere('t.isResultsReady = :isResultsReady')
                ->setParameter('isReady', 1)
                ->setParameter('isStarted', 1)
                ->setParameter('isOver', 1)
                ->setParameter('isResultsReady', 0)
                ->getQuery();

            try {
                    return $q->getResult();
                } catch (NoResultException $e) {

                    return null;
                }
    }

    /**
    * Возвращает необработанные прогнозы на матчи, которые завершились или отменены.
    * 
    * @return \AppBundle\Entity\Bet[]
    */
    public function findByFinishedOrRejectedMatchesAndEmptyPoints()
    {
        $q = $this
                ->createQueryBuilder('b')
                ->select('b, m, t, ps')
                ->leftJoin('b.match', 'm')
                ->leftJoin('b.tournament', 't')
                ->leftJoin('t.pointsScheme', 'ps')
                ->andWhere('m.isStarted = 1')
                ->andWhere('(m.isOver = 1 OR m.isRejected = 1)')
                ->andWhere('b.points IS NULL')
                ->getQuery();

            try {
                return $q->getResult();
            } catch (NoResultException $e) {

                return null;
            }
    }

    public function findSumByTournamentGroupedByUsers($tournament_id)
    {
        $q = $this
                ->createQueryBuilder('b')
                ->addSelect('u, SUM(b.points) sum_point')
                ->leftJoin('b.user', 'u')
                ->where('b.tournament = :tournament_id')
                ->setParameter('tournament_id', $tournament_id)
                ->groupBy('b.user')
                ->orderBy('sum_point', 'DESC')
                ->getQuery();

        try {
                return $q->getResult();
            } catch (NoResultException $e) {

                return null;
            }
    }

    public function findByTournamentAndMatchesIds($tournament_id, $matchesIds)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT b, u, m
                                    FROM AppBundle\Entity\Bet b
                                    LEFT JOIN b.user u
                                    LEFT JOIN b.match m
                                    WHERE b.tournament = :tournament_id AND m.id IN (:matches_ids)")
                    ->setParameter("tournament_id", $tournament_id)
                    ->setParameter("matches_ids", $matchesIds);

        try {
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    public function removeBetsByUsersIdsAndMatchesIds($tournamentId, $usersIds, $matchesIds)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("DELETE FROM AppBundle\Entity\Bet b
                                        WHERE b.tournament = :tournament_id
                                        AND b.user IN (:users_ids)
                                        AND b.match IN (:matches_ids)")
                                        ->setParameter("tournament_id", $tournamentId)
                                        ->setParameter("users_ids", $usersIds)
                                        ->setParameter("matches_ids", $matchesIds);

        $query->execute();
        $em->flush();
    }
}