<?php
namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AppBundle\Entity\BillingOperation
 *
 * @ORM\Table(name="billing_operations")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\BillingOperationRepository")
 */
class BillingOperation
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $hash;

    /**
    * @ORM\Column(type="string")
    */
    private $paysystem_internal_id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="billing_operations")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="billing_operations_recipient")
     * @ORM\JoinColumn(name="user_recipient", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    protected $user_recipient;

    /**
     * @ORM\Column(type="string")
     * @Assert\Choice({"DEPOSIT", "WITHDRAW", "RESERVATION", "CASHOUT", "TRANSFER"})
     * @Assert\NotBlank()
     */
    private $action;

    /**
     * @ORM\Column(type="string")
     * @Assert\Choice({"QIWI", "CREDITCARD"})
     */
    private $type;

    /**
     * @ORM\Column(type="string")
     * @Assert\Choice({"DOL", "SYSTEM"})
     */
    private $paysystem;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank()
     */
    private $money_rate;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank()
     */
    private $amount;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank()
     */
    private $amount_with_commision;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank()
     */
    private $commission_percent;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $currency;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank()
     */
    private $operation_datetime;

    /**
     * @ORM\Column(type="datetime", name="operation_ok_datetime")
     */
    private $operationOkDatetime;

    /**
     * @ORM\Column(type="datetime", name="operation_cancel_datetime")
     */
    private $operationCancelDatetime;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     */
    private $note;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    private $status;

    /**
     * @ORM\Column(type="string")
     */
    private $user_ip;

    /**
     * @ORM\Column(type="string")
     */
    private $user_agent;

    /**
     * @ORM\Column(type="string", name="balance_info")
     */
    private $balanceInfo;

    /**
     * Set id
     *
     * @param string $id
     *
     * @return BillingOperation
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set action
     *
     * @param string $action
     *
     * @return BillingOperation
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return BillingOperation
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set paysystem
     *
     * @param string $paysystem
     *
     * @return BillingOperation
     */
    public function setPaysystem($paysystem)
    {
        $this->paysystem = $paysystem;

        return $this;
    }

    /**
     * Get paysystem
     *
     * @return string
     */
    public function getPaysystem()
    {
        return $this->paysystem;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return BillingOperation
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set amountWithCommision
     *
     * @param float $amountWithCommision
     *
     * @return BillingOperation
     */
    public function setAmountWithCommision($amountWithCommision)
    {
        $this->amount_with_commision = $amountWithCommision;

        return $this;
    }

    /**
     * Get amountWithCommision
     *
     * @return float
     */
    public function getAmountWithCommision()
    {
        return $this->amount_with_commision;
    }

    /**
     * Set commissionPercent
     *
     * @param float $commissionPercent
     *
     * @return BillingOperation
     */
    public function setCommissionPercent($commissionPercent)
    {
        $this->commission_percent = $commissionPercent;

        return $this;
    }

    /**
     * Get commissionPercent
     *
     * @return float
     */
    public function getCommissionPercent()
    {
        return $this->commission_percent;
    }

    /**
     * Set currency
     *
     * @param string $currency
     *
     * @return BillingOperation
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set operationDatetime
     *
     * @param \DateTime $operationDatetime
     *
     * @return BillingOperation
     */
    public function setOperationDatetime($operationDatetime)
    {
        $this->operation_datetime = $operationDatetime;

        return $this;
    }

    /**
     * Get operationDatetime
     *
     * @return \DateTime
     */
    public function getOperationDatetime()
    {
        return $this->operation_datetime;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return BillingOperation
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return BillingOperation
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return BillingOperation
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set userRecipient
     *
     * @param \AppBundle\Entity\User $userRecipient
     *
     * @return BillingOperation
     */
    public function setUserRecipient(\AppBundle\Entity\User $userRecipient = null)
    {
        $this->user_recipient = $userRecipient;

        return $this;
    }

    /**
     * Get userRecipient
     *
     * @return \AppBundle\Entity\User
     */
    public function getUserRecipient()
    {
        return $this->user_recipient;
    }

    /**
     * Set userIp
     *
     * @param string $userIp
     *
     * @return BillingOperation
     */
    public function setUserIp($userIp)
    {
        $this->user_ip = $userIp;

        return $this;
    }

    /**
     * Get userIp
     *
     * @return string
     */
    public function getUserIp()
    {
        return $this->user_ip;
    }

    /**
     * Set userAgent
     *
     * @param string $userAgent
     *
     * @return BillingOperation
     */
    public function setUserAgent($userAgent)
    {
        $this->user_agent = $userAgent;

        return $this;
    }

    /**
     * Get userAgent
     *
     * @return string
     */
    public function getUserAgent()
    {
        return $this->user_agent;
    }

    /**
     * Set moneyRate
     *
     * @param float $moneyRate
     *
     * @return BillingOperation
     */
    public function setMoneyRate($moneyRate)
    {
        $this->money_rate = $moneyRate;

        return $this;
    }

    /**
     * Get moneyRate
     *
     * @return float
     */
    public function getMoneyRate()
    {
        return $this->money_rate;
    }

    /**
     * Set hash
     *
     * @param string $hash
     *
     * @return BillingOperation
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Get hash
     *
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Set paysystemInternalId
     *
     * @param string $paysystemInternalId
     *
     * @return BillingOperation
     */
    public function setPaysystemInternalId($paysystemInternalId)
    {
        $this->paysystem_internal_id = $paysystemInternalId;

        return $this;
    }

    /**
     * Get paysystemInternalId
     *
     * @return string
     */
    public function getPaysystemInternalId()
    {
        return $this->paysystem_internal_id;
    }

    /**
     * Set balanceInfo
     *
     * @param string $balanceInfo
     *
     * @return BillingOperation
     */
    public function setBalanceInfo($balanceInfo)
    {
        $this->balanceInfo = serialize($balanceInfo);

        return $this;
    }

    /**
     * Get balanceInfo
     *
     * @return string
     */
    public function getBalanceInfo()
    {
        if(!empty($this->balanceInfo))
            return unserialize($this->balanceInfo);
        else
            return array();
    }

    /**
     * Set operationOkDatetime
     *
     * @param \DateTime $operationOkDatetime
     *
     * @return BillingOperation
     */
    public function setOperationOkDatetime($operationOkDatetime)
    {
        $this->operationOkDatetime = $operationOkDatetime;

        return $this;
    }

    /**
     * Get operationOkDatetime
     *
     * @return \DateTime
     */
    public function getOperationOkDatetime()
    {
        return $this->operationOkDatetime;
    }

    /**
     * Set operationCancelDatetime
     *
     * @param \DateTime $operationCancelDatetime
     *
     * @return BillingOperation
     */
    public function setOperationCancelDatetime($operationCancelDatetime)
    {
        $this->operationCancelDatetime = $operationCancelDatetime;

        return $this;
    }

    /**
     * Get operationCancelDatetime
     *
     * @return \DateTime
     */
    public function getOperationCancelDatetime()
    {
        return $this->operationCancelDatetime;
    }
}
