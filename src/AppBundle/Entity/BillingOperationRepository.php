<?php

namespace AppBundle\Entity;

use \Doctrine\ORM\Tools\Pagination\Paginator;

class BillingOperationRepository extends \Doctrine\ORM\EntityRepository
{
    /**
    * Поиск операции по внешнему идентификатору
    *
    * @param string $paymentId
    * @return \AppBundle\Entity\BillingOperation
    */
    public function findByPaymentId($paymentId)
    {
        $q = $this
                ->createQueryBuilder('bo')
                ->where('bo.paysystem_internal_id = :payment_id')
                ->setParameter('payment_id', $paymentId)
                ->getQuery();

        try {
                return $q->getSingleResult();
            } catch (NoResultException $e) {
                return null;
            }
    }

    /**
    * Получает массив операций за определённое временя определённого пользователя
    *
    * @param int $userId ID пользователя
    * @param \DateTime $start Дата/Время "От"
    * @param \DateTime $start Дата/Время "До"
    * @param int $page Номер страницы
    * @param int $rowsPerPage Кол-во элементов на странице
    * @return Doctrine\ORM\Tools\Pagination\Paginator
    */
    public function findPaginatorForIntervalByUser($userId, $start, $end, $page, $rowsPerPage = 10)
    {
        $offset = $rowsPerPage * ($page - 1);

        $q = $this
                ->createQueryBuilder('bo')
                ->select("bo, u, ur")
                ->leftJoin("bo.user", "u")
                ->leftJoin("bo.user_recipient", "ur")
                ->where('u.id = :user_id OR ur.id = :user_id')
                ->setParameter('user_id', $userId)
                ->andWhere("bo.operation_datetime >= :start AND bo.operation_datetime <= :end")
                ->setParameter('start', $start->format("Y-m-d H:i:s"))
                ->setParameter('end', $end->format("Y-m-d H:i:s"))
                ->orderBy('bo.id', 'DESC')
                ->addOrderBy("bo.operation_datetime", "DESC")
                ->setFirstResult($offset)
                ->setMaxResults($rowsPerPage);

        $paginator = new Paginator($q);

        return $paginator;
    }

    /**
    * Получает массив операций по фильтру
    *
    * @param array $arOrder Параметры сортировки
    * @param array $arFilter Фильтр
    * @param int $page Номер страницы
    * @param int $rowsPerPage Кол-во элементов на странице
    * @return Doctrine\ORM\Tools\Pagination\Paginator
    */
    public function findPaginatorByFilter($arOrder, $arFilter, $page, $rowsPerPage = 10)
    {
        $offset = $rowsPerPage * ($page - 1);

        $q = $this
                ->createQueryBuilder('bo')
                ->select("bo, u, ur")
                ->leftJoin("bo.user", "u")
                ->leftJoin("bo.user_recipient", "ur")
                ->setFirstResult($offset)
                ->setMaxResults($rowsPerPage);

        //Нужно исправить в контроллере на специальный ключ, чтобы было прозрачно
        if(!empty($arFilter["user_id"]))
        {
            $q->andWhere("u.id = :user_id")->setParameter("user_id", $arFilter["user_id"]);
            $q->orWhere("ur.id = :user_recipient_id")->setParameter("user_recipient_id", $arFilter["user_id"]);
        }

        if(!empty($arFilter["user_recipient_id"]))
        {
          $q->andWhere("ur.id = :user_recipient_id")->setParameter("user_recipient_id", $arFilter["user_recipient_id"]);
        }

        if(!empty($arFilter["action"]))
        {
          $q->andWhere("bo.action = :action")->setParameter("action", $arFilter["action"]);
        }

        if(!empty($arFilter["status"]))
        {
          $q->andWhere("bo.status = :status")->setParameter("status", $arFilter["status"]);
        }

        $q->orderBy('bo.id', 'DESC');

        if(!empty($arOrder))
        {
            foreach($arOrder as $sort => $order)
                $q->addOrderBy('bo.'.$sort, $order);
        }

        $paginator = new Paginator($q);

        return $paginator;
    }

    /**
    * Возвращает кол-во операций пополнения пользователю с $userId
    *
    * @param int $userId
    * @return int
    */
    public function getDepositCountByUser($userId)
    {
      $q = $this
                ->createQueryBuilder('bo')
                ->select("COUNT(bo.id)")
                ->where('bo.user = :user_id')
                ->setParameter('user_id', $userId)
                ->andWhere("bo.action = :action")
                ->setParameter('action', \AppBundle\Services\BalanceHelper::ACTION_DEPOSIT)
                ->andWhere("bo.status = :status")
                ->setParameter('status', \AppBundle\Services\BalanceHelper::STATUS_OK)
                ->getQuery();

      try {
        return $q->getSingleScalarResult();
      } catch (NoResultException $e) {
        return 0;
      }
    }

    /**
    * Возвращает кол-во операций вывода на конкретную дату
    *
    * @param int $userId ID пользователя
    * @param \DateTime $datetime Дата
    * @return int
    */
    public function getCashoutOperationsCount($userId, \DateTime $datetime)
    {
      $startDatetime = clone $datetime;
      $startDatetime->setTime(0,0,0);

      $q = $this
                ->createQueryBuilder('bo')
                ->select("COUNT(bo.id)")
                ->where('bo.user = :user_id')
                ->setParameter('user_id', $userId)
                ->andWhere("bo.action = :action")
                ->setParameter('action', \AppBundle\Services\BalanceHelper::ACTION_CASHOUT)
                ->andWhere("bo.operation_datetime >= :start_datetime AND bo.operation_datetime <= :end_datetime")
                ->setParameter("start_datetime", $startDatetime)
                ->setParameter("end_datetime", $datetime)
                ->getQuery();

      try {
        return $q->getSingleScalarResult();
      } catch (NoResultException $e) {
        return 0;
      }
    }

    /**
    * Возвращает операции зачисления старше переданной даты
    * 
    * @param \DateTime $datetime Дата начиная с которой возвращаются даные
    *
    * @return \AppBundle\Entity\BillingOperation[]
    */
    public function getDepositsInProgressOlderThan(\DateTime $datetime)
    {
        $datetime->setTime(0,0,0);

        $q = $this->createQueryBuilder('bo')
            ->where('bo.status = 0')
            ->andWhere('bo.operation_datetime <= :datetime')
            ->setParameter('datetime', $datetime)
            ->getQuery();

        try {
            return $q->getResult();
        } catch (NoResultException $e) {
            return array();
        }
    }
}
