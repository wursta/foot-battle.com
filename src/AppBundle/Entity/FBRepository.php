<?php
namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections\Expr\Expression;
use Doctrine\ORM\NoResultException;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Query\Expr\OrderBy;
use \Doctrine\ORM\Tools\Pagination\Paginator;

class FBRepository extends EntityRepository
{
    /**
    * Поиск
    *
    * @param array $arOrder     Массив сортировки вида [ [field => order], [field => order] ].
    * @param array $arFilter    Массив фильтра вида [ [=field => value], [>=field => value] ].
    * @param int   $page        Номер страницы
    * @param int   $rowsPerPage Кол-во записей на странице
    *
    * @return \Doctrine\ORM\Tools\Pagination\Paginator
    */
    public function findPaginator(array $arOrder, array $arFilter, $page, $rowsPerPage = 10)
    {
        $q = $this->createQueryBuilder('entity');

        $q = $q->setFirstResult($this->getOffset($page, $rowsPerPage))->setMaxResults($rowsPerPage);

        $q = $q->addCriteria($this->createCriteriaFromFilter($arFilter));
        $q = $q->addOrderBy($this->createOrderBy($arOrder, 'entity'));

        return $this->getPaginator($q);
    }

    /**
    * Создаёт объект Сriteria по фильтру
    *
    * @param array $arFilter Фильтр
    *
    * @return Criteria
    */
    protected function createCriteriaFromFilter(array $arFilter)
    {

        $criteria = new Criteria();

        foreach($arFilter as $propName => $propValue)
        {
            if(is_null($propValue)) {
                continue;
            }

            $tmp = explode(':', $propName);

            switch($tmp[0]) {
                case ">":
                    $expression = $criteria->expr()->gt($tmp[1], $propValue);
                    break;

                case ">=":
                    $expression = $criteria->expr()->gte($tmp[1], $propValue);
                    break;

                case "<":
                    $expression = $criteria->expr()->lt($tmp[1], $propValue);
                    break;

                case "<=":
                    $expression = $criteria->expr()->lte($tmp[1], $propValue);
                    break;

                default:
                    if(!is_array($propValue)) {
                        $expression = $criteria->expr()->eq($propName, $propValue);
                    } else {
                        $expression = $criteria->expr()->in($propName, $propValue);
                    }
                    break;
            }

            $criteria->andWhere($expression);
        }

        return $criteria;
    }

    /**
    * Возвращает offset (кол-во записей для пропуска)
    *
    * @param int $page        Номер страницы
    * @param int $rowsPerPage Кол-во записей на старнице
    *
    * @return int
    */
    protected function getOffset($page, $rowsPerPage)
    {
        return $rowsPerPage * ($page - 1);
    }

    /**
    * Создаёт объект OrderBy по массиву
    *
    * @param array  $arOrder      Сортировка
    * @param string $alias        Алиас для таблицы
    * @param array  $defaultOrder Сортировка по-умолчанию.
    *
    * @return OrderBy
    */
    protected function createOrderBy(array $arOrder, $alias, array $defaultOrder = array())
    {
        if(empty($arOrder) && !empty($defaultOrder)) {
            $arOrder = $defaultOrder;
        }
        
        $orderBy = new OrderBy();
        foreach($arOrder as $sort => $order) {
            $orderBy->add($alias . '.' . $sort, $order);
        }

        return $orderBy;
    }

    /**
    * Возвращает объект паджинатора из объекта запроса.
    *
    * @param \Doctrine\ORM\QueryBuilder $q
    *
    * @return Paginator
    */
    protected function getPaginator(\Doctrine\ORM\QueryBuilder &$q)
    {
        return new Paginator($q);
    }
}
