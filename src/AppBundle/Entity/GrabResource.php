<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AppBundle\Entity\GrabResource
 *
 * @ORM\Table(name="grab_resources") * 
 * @ORM\Entity(repositoryClass="AppBundle\Entity\GrabResourceRepository")
 */
class GrabResource
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=255)     
     * @Assert\Regex("/^[\w\d\s.-]*$/", message="Допускаются только цифры, точка, тире и латинские символы.")
     */
    private $internal_name;
    
    /**
     * @ORM\Column(type="string", length=255)     
     * @Assert\Length(min = 2, max = 255)
     */
    private $title;
    
    /**
     * @ORM\Column(type="string", length=255)     
     * @Assert\Length(min = 2, max = 255)
     */
    private $calendar_url;        

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;
    
    /**
     * @ORM\Column(name="current_round", type="integer")
     * @Assert\Range(min = 1)
     */
    private $currentRound;
    
    /**
     * @ORM\Column(name="by_period", type="boolean")
     */
    private $byPeriod;
    
    /**
     * @ORM\ManyToOne(targetEntity="League", inversedBy="resources")
     * @ORM\JoinColumn(name="league_id", referencedColumnName="id")     
     */
    protected $league;

    /**
     * @ORM\OneToMany(targetEntity="ResourceTeam", mappedBy="resource")
     */
    protected $resource_teams;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->league = new \Doctrine\Common\Collections\ArrayCollection();
        $this->resource_teams = new \Doctrine\Common\Collections\ArrayCollection();        
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set internalName
     *
     * @param string $internalName
     *
     * @return GrabResource
     */
    public function setInternalName($internalName)
    {
        $this->internal_name = $internalName;

        return $this;
    }

    /**
     * Get internalName
     *
     * @return string
     */
    public function getInternalName()
    {
        return $this->internal_name;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return GrabResource
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set calendarUrl
     *
     * @param string $calendarUrl
     *
     * @return GrabResource
     */
    public function setCalendarUrl($calendarUrl)
    {
        $this->calendar_url = $calendarUrl;

        return $this;
    }

    /**
     * Get calendarUrl
     *
     * @return string
     */
    public function getCalendarUrl()
    {
        return $this->calendar_url;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return GrabResource
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set currentRound
     *
     * @param integer $currentRound
     *
     * @return GrabResource
     */
    public function setCurrentRound($currentRound)
    {
        $this->currentRound = $currentRound;

        return $this;
    }

    /**
     * Get currentRound
     *
     * @return integer
     */
    public function getCurrentRound()
    {
        return $this->currentRound;
    }

    /**
     * Set byPeriod
     *
     * @param boolean $byPeriod
     *
     * @return GrabResource
     */
    public function setByPeriod($byPeriod)
    {
        $this->byPeriod = $byPeriod;

        return $this;
    }

    /**
     * Get byPeriod
     *
     * @return boolean
     */
    public function getByPeriod()
    {
        return $this->byPeriod;
    }

    /**
     * Set league
     *
     * @param \AppBundle\Entity\League $league
     *
     * @return GrabResource
     */
    public function setLeague(\AppBundle\Entity\League $league = null)
    {
        $this->league = $league;

        return $this;
    }

    /**
     * Get league
     *
     * @return \AppBundle\Entity\League
     */
    public function getLeague()
    {
        return $this->league;
    }

    /**
     * Add resourceTeam
     *
     * @param \AppBundle\Entity\ResourceTeam $resourceTeam
     *
     * @return GrabResource
     */
    public function addResourceTeam(\AppBundle\Entity\ResourceTeam $resourceTeam)
    {
        $this->resource_teams[] = $resourceTeam;

        return $this;
    }

    /**
     * Remove resourceTeam
     *
     * @param \AppBundle\Entity\ResourceTeam $resourceTeam
     */
    public function removeResourceTeam(\AppBundle\Entity\ResourceTeam $resourceTeam)
    {
        $this->resource_teams->removeElement($resourceTeam);
    }

    /**
     * Get resourceTeams
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResourceTeams()
    {
        return $this->resource_teams;
    }
}
