<?php
namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

class GrabResourceRepository extends EntityRepository
{
    public function findById($id)
    {
        $q = $this
                ->createQueryBuilder('r')                
                ->where('r.id = :id')                
                ->setParameter('id', $id)                
                ->getQuery();
        
        try {                    
            return $q->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        }
    }
    
    public function findByResourceTeam($resource_team_id)
    {
        $q = $this
                ->createQueryBuilder('r')                
                ->where('r.id = :id')                
                ->setParameter('id', $id)                
                ->getQuery();
        
        try {                    
            return $q->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        }
    }
}
?>
