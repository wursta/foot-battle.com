<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use AppBundle\Twig\CountryExtension;

/**
 * AppBundle\Entity\League
 *
 * @ORM\Table(name="leagues") * 
 * @ORM\Entity(repositoryClass="AppBundle\Entity\LeagueRepository")
 */
class League
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")     
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=2)          
     * @Assert\Country()
     */
    private $country;
    
    /**
     * @ORM\Column(type="string", length=255)     
     * @Assert\NotBlank()
     * @Assert\Length(min = 2, max = 255)
     */
    private $tournament;
    
    /**
     * @ORM\Column(type="string")        
     */
    private $icon;
    
    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;
    
    /**
     * @ORM\OneToMany(targetEntity="GrabResource", mappedBy="league")
     */
    protected $resources;
        
    /**
    * @ORM\ManyToMany(targetEntity="Team", inversedBy="leagues")
    * @ORM\JoinTable(name="team_league")
    */
    protected $teams;    
    
    /**
     * @ORM\OneToMany(targetEntity="Match", mappedBy="league")
     */
    protected $matches;        
    
    /**
     * @ORM\ManyToMany(targetEntity="Tournament", mappedBy="leagues")
     */
    private $tournaments;

    private $league_name;    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->resources = new \Doctrine\Common\Collections\ArrayCollection();
        $this->teams = new \Doctrine\Common\Collections\ArrayCollection();
        $this->matches = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tournaments = new \Doctrine\Common\Collections\ArrayCollection();
    }           
        
    public function __toString()
    {
        return $this->getLeagueName();
    }
        
    public function getLeagueName()
    {
        if(empty($this->country))
            return $this->tournament;
            
        $countryExt = new CountryExtension();
        return $countryExt->countryName($this->country)." (".$this->tournament.")";
    }        

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return League
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set tournament
     *
     * @param string $tournament
     * @return League
     */
    public function setTournament($tournament)
    {
        $this->tournament = $tournament;

        return $this;
    }

    /**
     * Get tournament
     *
     * @return string 
     */
    public function getTournament()
    {
        return $this->tournament;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return League
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Add resources
     *
     * @param \AppBundle\Entity\GrabResource $resources
     * @return League
     */
    public function addResource(\AppBundle\Entity\GrabResource $resources)
    {
        $this->resources[] = $resources;

        return $this;
    }

    /**
     * Remove resources
     *
     * @param \AppBundle\Entity\GrabResource $resources
     */
    public function removeResource(\AppBundle\Entity\GrabResource $resources)
    {
        $this->resources->removeElement($resources);
    }

    /**
     * Get resources
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getResources()
    {
        return $this->resources;
    }

    /**
     * Add teams
     *
     * @param \AppBundle\Entity\Team $teams
     * @return League
     */
    public function addTeam(\AppBundle\Entity\Team $teams)
    {
        $this->teams[] = $teams;

        return $this;
    }

    /**
     * Remove teams
     *
     * @param \AppBundle\Entity\Team $teams
     */
    public function removeTeam(\AppBundle\Entity\Team $teams)
    {
        $this->teams->removeElement($teams);
    }

    /**
     * Get teams
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTeams()
    {
        return $this->teams;
    }

    /**
     * Add matches
     *
     * @param \AppBundle\Entity\Match $matches
     * @return League
     */
    public function addMatch(\AppBundle\Entity\Match $matches)
    {
        $this->matches[] = $matches;

        return $this;
    }

    /**
     * Remove matches
     *
     * @param \AppBundle\Entity\Match $matches
     */
    public function removeMatch(\AppBundle\Entity\Match $matches)
    {
        $this->matches->removeElement($matches);
    }

    /**
     * Get matches
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMatches()
    {
        return $this->matches;
    }

    /**
     * Set icon
     *
     * @param string $icon
     * @return League
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
    
        return $this;
    }

    /**
     * Get icon
     *
     * @return string 
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Add tournament
     *
     * @param \AppBundle\Entity\Tournament $tournament
     *
     * @return League
     */
    public function addTournament(\AppBundle\Entity\Tournament $tournament)
    {
        $this->tournaments[] = $tournament;

        return $this;
    }

    /**
     * Remove tournament
     *
     * @param \AppBundle\Entity\Tournament $tournament
     */
    public function removeTournament(\AppBundle\Entity\Tournament $tournament)
    {
        $this->tournaments->removeElement($tournament);
    }

    /**
     * Get tournaments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTournaments()
    {
        return $this->tournaments;
    }
}
