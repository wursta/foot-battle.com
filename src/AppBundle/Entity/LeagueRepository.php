<?php
namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

class LeagueRepository extends EntityRepository
{
    /**
    * Возвращает все лиги
    * 
    * @param array $sort Параметры сортировки
    * @example [
    *   isActive => DESC,
    *   country => ASC
    * ]
    * @returns \AppBundle\Entity\League[]|null
    */
    public function findAll($sort = array())
    {
        $em = $this->getEntityManager();

        $query = $this->createQueryBuilder("l");
        
        if(!empty($sort))
        {
            foreach($sort as $sortKey => $sortDir)
            {
                $query->addOrderBy("l.".$sortKey, $sortDir);
            }
        }
                        
        $query = $query->getQuery();        

        try {
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }
    
    /**
    * Возвращает активные лиги
    * 
    * @return \AppBundle\Entity\League[]|null
    */
    public function findActive()
    {
        $query = $this->createQueryBuilder('l')
            ->where('l.isActive = 1')
            ->getQuery();

        try {            
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
    /**
    * Возвращает все активные лиги привязанные к турниру
    * 
    * @param int $tournamentId
    * @return \AppBundle\Entity\League[]
    */
    public function findActiveByTournament($tournamentId)
    {
      $query = $this->createQueryBuilder('l')                      
                      ->leftJoin("l.tournaments", "t")
                      ->where("t.id = :tournament_id")
                      ->setParameter("tournament_id", $tournamentId)                      
                      ->andWhere("l.isActive = :is_active")
                      ->setParameter("is_active", true)
                      ->orderBy("l.country", "ASC")
                      ->getQuery();
                  
        try {
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }
    
    public function findByIds($ids)
    {
        $query = $this->createQueryBuilder('l')
            ->where('l.isActive = 1')
            ->where('l.id IN (:ids)')
            ->setParameter("ids", $ids)
            ->getQuery();

        try {            
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
    public function findByGeography($geographyInternalName, $tournamentLeagues = array())
    {
        switch($geographyInternalName)
        {
            case "manual":                
                if(count($tournamentLeagues) == 0)
                {
                    return $this->findActive();
                }
                else
                {
                    $leaguesIds = array();
                    /**
                    * @var $tournamentLeagues \AppBundle\Entity\League[]
                    */
                    foreach($tournamentLeagues as $league)
                        $leaguesIds[] = $league->getId();
                    
                    return $this->findByIds($leaguesIds);
                }
            break;
        }        
    }
    
    public function findByTournament($tournament_id)
    {
        $query = $this->createQueryBuilder('l')                      
                      ->leftJoin("l.tournaments", "t")
                      ->where("t.id = :tournament_id")
                      ->setParameter("tournament_id", $tournament_id)                      
                      ->orderBy("l.country", "ASC")
                      ->getQuery();
                  
        try {
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }
}
?>
