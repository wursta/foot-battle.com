<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AppBundle\Entity\Log
 *
 * @ORM\Table(name="_log")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\LogRepository")
 */
class Log
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")     
     */
    private $id;
    
    /**
     * @ORM\Column(type="datetime")
     */
    private $datetime;
    
    /**
     * @ORM\Column(type="string")
     */
    private $microseconds;
    
    /**
     * @ORM\Column(type="string")     
     */
    private $type;        
    
    /**
     * @ORM\Column(type="string")
     */
    private $message;
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     * @return Log
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;
    
        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime 
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Log
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return Log
     */
    public function setMessage($message)
    {
        $this->message = $message;
    
        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set microseconds
     *
     * @param integer $microseconds
     * @return Log
     */
    public function setMicroseconds($microseconds)
    {
        $this->microseconds = $microseconds;
    
        return $this;
    }

    /**
     * Get microseconds
     *
     * @return integer 
     */
    public function getMicroseconds()
    {
        return $this->microseconds;
    }
}
