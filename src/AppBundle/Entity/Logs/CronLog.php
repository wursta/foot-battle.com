<?php
namespace AppBundle\Entity\Logs;

use Doctrine\ORM\Mapping as ORM;

/**
 * AppBundle\Entity\Logs\CronLog
 *
 * @ORM\Table(name="betbattle_logs.cron_logs")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Logs\CronLogRepository")
 */
class CronLog
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")     
     */
    private $id;
    
    /**
     * @ORM\Column(type="datetime")
     */
    private $datetime;
    
    /**
     * @ORM\Column(type="string")
     */
    private $microseconds;
    
    /**
     * @ORM\Column(type="string")     
     */
    private $command;
    
    /**
     * @ORM\Column(type="string")     
     */
    private $command_description;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $execution_time;
    
    /**
     * @ORM\Column(type="integer")
    */
    private $memory_peak_usage;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     *
     * @return CronLog
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set microseconds
     *
     * @param string $microseconds
     *
     * @return CronLog
     */
    public function setMicroseconds($microseconds)
    {
        $this->microseconds = $microseconds;

        return $this;
    }

    /**
     * Get microseconds
     *
     * @return string
     */
    public function getMicroseconds()
    {
        return $this->microseconds;
    }

    /**
     * Set command
     *
     * @param string $command
     *
     * @return CronLog
     */
    public function setCommand($command)
    {
        $this->command = $command;

        return $this;
    }

    /**
     * Get command
     *
     * @return string
     */
    public function getCommand()
    {
        return $this->command;
    }

    /**
     * Set executionTime
     *
     * @param integer $executionTime
     *
     * @return CronLog
     */
    public function setExecutionTime($executionTime)
    {
        $this->execution_time = $executionTime;

        return $this;
    }

    /**
     * Get executionTime
     *
     * @return integer
     */
    public function getExecutionTime()
    {
        return $this->execution_time;
    }

    /**
     * Set memoryPeakUsage
     *
     * @param integer $memoryPeakUsage
     *
     * @return CronLog
     */
    public function setMemoryPeakUsage($memoryPeakUsage)
    {
        $this->memory_peak_usage = $memoryPeakUsage;

        return $this;
    }

    /**
     * Get memoryPeakUsage
     *
     * @return integer
     */
    public function getMemoryPeakUsage()
    {
        return $this->memory_peak_usage;
    }

    /**
     * Set commandDescription
     *
     * @param string $commandDescription
     *
     * @return CronLog
     */
    public function setCommandDescription($commandDescription)
    {
        $this->command_description = $commandDescription;

        return $this;
    }

    /**
     * Get commandDescription
     *
     * @return string
     */
    public function getCommandDescription()
    {
        return $this->command_description;
    }
}
