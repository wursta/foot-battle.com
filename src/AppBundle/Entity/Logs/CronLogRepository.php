<?php

namespace AppBundle\Entity\Logs;

use Doctrine\ORM\NoResultException;

/**
 * CronLogRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CronLogRepository extends \Doctrine\ORM\EntityRepository
{
    public function findOneWeek(\DateTime $fromDatetime, \DateTime $toDatetime)
    {        
        $query = $this->createQueryBuilder('cl')
                      ->where("cl.datetime >= :fromDatetime")->setParameter("fromDatetime", $fromDatetime)
                      ->andWhere("cl.datetime <= :toDatetime")->setParameter("toDatetime", $toDatetime)
                      ->orderBy("cl.datetime", "ASC")
                      ->addOrderBy("cl.microseconds", "ASC")
                      ->getQuery();
                      
        try {
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }                
    }    
    
    public function getAverageMemoryUsage()
    {
        $query = $this->createQueryBuilder('cl')
                      ->select("cl.command, cl.command_description, AVG(cl.memory_peak_usage) as avg_memory_usage")
                      ->groupBy("cl.command")
                      ->getQuery();
                      
        try {
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }
    
    public function removeLogsFromDate(\DateTime $fromDate)
    {
        $query = $this->createQueryBuilder("cl")
                      ->delete()
                      ->where("cl.datetime <= :fromDatetime")->setParameter("fromDatetime", $fromDate)
                      ->getQuery();
                              
        return $query->execute();
    }
}
