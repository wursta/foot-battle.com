<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AppBundle\Entity\Match
 *
 * @ORM\Table(name="matches")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\MatchRepository")
 */
class Match
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")     
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="League", inversedBy="matches")
     * @ORM\JoinColumn(name="league_id", referencedColumnName="id")
     */
    protected $league;            
    
    /**
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="home_matches")
     * @ORM\JoinColumn(name="home_team_id", referencedColumnName="id")
     */
    protected $home_team;

    /**
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="guest_matches")
     * @ORM\JoinColumn(name="guest_team_id", referencedColumnName="id")
     */
    protected $guest_team;

    /**
     * @ORM\Column(type="date")
     * @Assert\Date()
     */
    protected $match_date;
    
    /**
     * @ORM\Column(type="time")     
     */
    protected $match_time;

    /**
     * @ORM\Column(type="integer")     
     * @Assert\Range(min = 0)
     */
    protected $score_left;

    /**
     * @ORM\Column(type="integer")     
     * @Assert\Range(min = 0)
     */
    protected $score_right;

    /**
     * @ORM\Column(type="integer")
     */
    protected $tour_number;
    
    /**
     * @ORM\Column(name="is_started", type="boolean")
     */
    protected $isStarted;    
    
    /**
     * @ORM\Column(name="is_over", type="boolean")
     */
    protected $isOver;        

    /**
     * @ORM\Column(name="is_rejected", type="boolean")
     */
    protected $isRejected;
    
    /**
     * @ORM\OneToMany(targetEntity="Bet", mappedBy="match")
     */    
    protected $bets;                

    /**
     * @ORM\OneToMany(targetEntity="TournamentChampionshipMatch", mappedBy="match")
     */    
    protected $tournament_championship_matches;                
    
    /**
     * @ORM\OneToMany(targetEntity="TournamentRoundMatch", mappedBy="match")
     */    
    protected $tournament_round_matches;                
    
    /**
     * @ORM\OneToMany(targetEntity="TournamentPlayoffMatch", mappedBy="match")
     */    
    protected $tournament_playoff_matches;

    /**
     * @ORM\OneToMany(targetEntity="TournamentChampionsleagueMatch", mappedBy="match")
     */    
    protected $tournament_championsleague_matches;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->isStarted = 0;
        $this->isOver = 0;
        $this->isRejected = 0;
        $this->tournament_championship_matches = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tournament_round_matches = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tournament_playoff_matches = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tournament_championsleague_matches = new \Doctrine\Common\Collections\ArrayCollection();
        $this->bets = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set match_date
     *
     * @param \DateTime $matchDate
     * @return Match
     */
    public function setMatchDate($matchDate)
    {
        $this->match_date = $matchDate;

        return $this;
    }

    /**
     * Get match_date
     *
     * @return \DateTime 
     */
    public function getMatchDate()
    {
        return $this->match_date;
    }

    /**
     * Set match_time
     *
     * @param \DateTime $matchTime
     * @return Match
     */
    public function setMatchTime($matchTime)
    {
        $this->match_time = $matchTime;

        return $this;
    }

    /**
     * Get match_time
     *
     * @return \DateTime 
     */
    public function getMatchTime()
    {
        return $this->match_time;
    }

    /**
     * Set score_left
     *
     * @param integer $scoreLeft
     * @return Match
     */
    public function setScoreLeft($scoreLeft)
    {
        $this->score_left = $scoreLeft;

        return $this;
    }

    /**
     * Get score_left
     *
     * @return integer 
     */
    public function getScoreLeft()
    {
        return $this->score_left;
    }

    /**
     * Set score_right
     *
     * @param integer $scoreRight
     * @return Match
     */
    public function setScoreRight($scoreRight)
    {
        $this->score_right = $scoreRight;

        return $this;
    }

    /**
     * Get score_right
     *
     * @return integer 
     */
    public function getScoreRight()
    {
        return $this->score_right;
    }

    /**
     * Set tour_number
     *
     * @param integer $tourNumber
     * @return Match
     */
    public function setTourNumber($tourNumber)
    {
        $this->tour_number = $tourNumber;

        return $this;
    }

    /**
     * Get tour_number
     *
     * @return integer 
     */
    public function getTourNumber()
    {
        return $this->tour_number;
    }

    /**
     * Set isStarted
     *
     * @param boolean $isStarted
     * @return Match
     */
    public function setIsStarted($isStarted)
    {
        $this->isStarted = $isStarted;

        return $this;
    }

    /**
     * Get isStarted
     *
     * @return boolean 
     */
    public function getIsStarted()
    {
        return $this->isStarted;
    }

    /**
     * Set isOver
     *
     * @param boolean $isOver
     * @return Match
     */
    public function setIsOver($isOver)
    {
        $this->isOver = $isOver;

        return $this;
    }

    /**
     * Get isOver
     *
     * @return boolean 
     */
    public function getIsOver()
    {
        return $this->isOver;
    }

    /**
     * Set isRejected
     *
     * @param boolean $isRejected
     * @return Match
     */
    public function setIsRejected($isRejected)
    {
        $this->isRejected = $isRejected;

        return $this;
    }

    /**
     * Get isRejected
     *
     * @return boolean 
     */
    public function getIsRejected()
    {
        return $this->isRejected;
    }

    /**
     * Set league
     *
     * @param \AppBundle\Entity\League $league
     * @return Match
     */
    public function setLeague(\AppBundle\Entity\League $league = null)
    {
        $this->league = $league;

        return $this;
    }

    /**
     * Get league
     *
     * @return \AppBundle\Entity\League 
     */
    public function getLeague()
    {
        return $this->league;
    }

    /**
     * Set home_team
     *
     * @param \AppBundle\Entity\Team $homeTeam
     * @return Match
     */
    public function setHomeTeam(\AppBundle\Entity\Team $homeTeam = null)
    {
        $this->home_team = $homeTeam;

        return $this;
    }

    /**
     * Get home_team
     *
     * @return \AppBundle\Entity\Team 
     */
    public function getHomeTeam()
    {
        return $this->home_team;
    }

    /**
     * Set guest_team
     *
     * @param \AppBundle\Entity\Team $guestTeam
     * @return Match
     */
    public function setGuestTeam(\AppBundle\Entity\Team $guestTeam = null)
    {
        $this->guest_team = $guestTeam;

        return $this;
    }

    /**
     * Get guest_team
     *
     * @return \AppBundle\Entity\Team 
     */
    public function getGuestTeam()
    {
        return $this->guest_team;
    }

    /**
     * Add bets
     *
     * @param \AppBundle\Entity\Bet $bets
     * @return Match
     */
    public function addBet(\AppBundle\Entity\Bet $bets)
    {
        $this->bets[] = $bets;

        return $this;
    }

    /**
     * Remove bets
     *
     * @param \AppBundle\Entity\Bet $bets
     */
    public function removeBet(\AppBundle\Entity\Bet $bets)
    {
        $this->bets->removeElement($bets);
    }

    /**
     * Get bets
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBets()
    {
        return $this->bets;
    }

    /**
     * Add tournament_championship_matches
     *
     * @param \AppBundle\Entity\TournamentChampionshipMatch $tournamentChampionshipMatches
     * @return Match
     */
    public function addTournamentChampionshipMatch(\AppBundle\Entity\TournamentChampionshipMatch $tournamentChampionshipMatches)
    {
        $this->tournament_championship_matches[] = $tournamentChampionshipMatches;

        return $this;
    }

    /**
     * Remove tournament_championship_matches
     *
     * @param \AppBundle\Entity\TournamentChampionshipMatch $tournamentChampionshipMatches
     */
    public function removeTournamentChampionshipMatch(\AppBundle\Entity\TournamentChampionshipMatch $tournamentChampionshipMatches)
    {
        $this->tournament_championship_matches->removeElement($tournamentChampionshipMatches);
    }

    /**
     * Get tournament_championship_matches
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTournamentChampionshipMatches()
    {
        return $this->tournament_championship_matches;
    }

    /**
     * Add tournament_round_matches
     *
     * @param \AppBundle\Entity\TournamentRoundMatch $tournamentRoundMatches
     * @return Match
     */
    public function addTournamentRoundMatch(\AppBundle\Entity\TournamentRoundMatch $tournamentRoundMatches)
    {
        $this->tournament_round_matches[] = $tournamentRoundMatches;
    
        return $this;
    }

    /**
     * Remove tournament_round_matches
     *
     * @param \AppBundle\Entity\TournamentRoundMatch $tournamentRoundMatches
     */
    public function removeTournamentRoundMatch(\AppBundle\Entity\TournamentRoundMatch $tournamentRoundMatches)
    {
        $this->tournament_round_matches->removeElement($tournamentRoundMatches);
    }

    /**
     * Get tournament_round_matches
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTournamentRoundMatches()
    {
        return $this->tournament_round_matches;
    }

    /**
     * Add tournament_playoff_matches
     *
     * @param \AppBundle\Entity\TournamentPlayoffMatch $tournamentPlayoffMatches
     * @return Match
     */
    public function addTournamentPlayoffMatch(\AppBundle\Entity\TournamentPlayoffMatch $tournamentPlayoffMatches)
    {
        $this->tournament_playoff_matches[] = $tournamentPlayoffMatches;
    
        return $this;
    }

    /**
     * Remove tournament_playoff_matches
     *
     * @param \AppBundle\Entity\TournamentPlayoffMatch $tournamentPlayoffMatches
     */
    public function removeTournamentPlayoffMatch(\AppBundle\Entity\TournamentPlayoffMatch $tournamentPlayoffMatches)
    {
        $this->tournament_playoff_matches->removeElement($tournamentPlayoffMatches);
    }

    /**
     * Get tournament_playoff_matches
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTournamentPlayoffMatches()
    {
        return $this->tournament_playoff_matches;
    }

    /**
     * Add tournament_championsleague_matches
     *
     * @param \AppBundle\Entity\TournamentPlayoffMatch $tournamentChampionsleagueMatches
     * @return Match
     */
    public function addTournamentChampionsleagueMatch(\AppBundle\Entity\TournamentPlayoffMatch $tournamentChampionsleagueMatches)
    {
        $this->tournament_championsleague_matches[] = $tournamentChampionsleagueMatches;

        return $this;
    }

    /**
     * Remove tournament_championsleague_matches
     *
     * @param \AppBundle\Entity\TournamentPlayoffMatch $tournamentChampionsleagueMatches
     */
    public function removeTournamentChampionsleagueMatch(\AppBundle\Entity\TournamentPlayoffMatch $tournamentChampionsleagueMatches)
    {
        $this->tournament_championsleague_matches->removeElement($tournamentChampionsleagueMatches);
    }

    /**
     * Get tournament_championsleague_matches
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTournamentChampionsleagueMatches()
    {
        return $this->tournament_championsleague_matches;
    }
}
