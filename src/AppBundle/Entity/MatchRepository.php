<?php
namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

class MatchRepository extends FBRepository
{
        /**
        * Поиск матча по ID
        * 
        * @param int $match_id ID матча
        * @return \AppBundle\Entity\Match
        */
        public function findById($match_id)
        {
            $q = $this
                ->createQueryBuilder('m')
                ->addSelect('ht, gt')
                ->leftJoin('m.home_team', 'ht')
                ->leftJoin('m.guest_team', 'gt')
                ->where('m.id = :match_id')                
                ->setParameter('match_id', $match_id)                
                ->getQuery();
                
            try {                    
                    return $q->getSingleResult();
                } catch (NoResultException $e) {                                        
                    
                    return null;
                }            
        }
        
        public function search($arFilter = array(), $arOrder = array())
        {
          $query = $this->createQueryBuilder('m')
                        ->select('m, ht, gt')
                        ->leftJoin('m.home_team', 'ht')
                        ->leftJoin('m.guest_team', 'gt');
          
          //Фильтрация          
          if(isset($arFilter["league_id"]))
          {            
            $query = $query->leftJoin('m.league', 'l')
                           ->andWhere("l.id = :league_id")
                           ->setParameter("league_id", $arFilter["league_id"]);
          }          
          
          if(isset($arFilter["isStarted"]))
          {
            $query = $query->andWhere("m.isStarted = :is_started")->setParameter("is_started", $arFilter["isStarted"]);
          }
          
          if(!empty($arFilter["exclude_matches"]))
          {
            $query = $query->andWhere("m.id NOT IN (:matched_ids)")->setParameter("matched_ids", $arFilter["exclude_matches"]);
          }
                    
                    
          //Сортировка
          if(isset($arOrder["match_date"]))
          {
            $query = $query->addOrderBy('m.match_date', 'ASC');
          }
          
          if(isset($arOrder["match_time"]))
          {
            $query = $query->addOrderBy('m.match_time', 'ASC');
          }
                    
          $query = $query->addOrderBy("m.id" , "ASC");
                    
          $query = $query->getQuery();
          
          try {
                return $query->getResult();
            } catch (NoResultException $e) {                
                return null;
            }
        }
        
        /**
        * Поиск матча по параметрам
        * 
        * @param mixed $matchData
        * @example [
        *   league_id
        *   match_datetime,
        *   home_team_id
        *   guest_team_id
        *   score        
        * ]
        * @return \AppBundle\Entity\Match|null
        */
        public function findMatchByData($matchData)
        {
            $match = null;
            
            $q = $this
                    ->createQueryBuilder('m')
                    ->where('m.league = :league')
                    ->setParameter('league', $matchData["league_id"])
                    ->andWhere('m.home_team = :home_team')
                    ->setParameter('home_team', $matchData["home_team_id"])
                    ->andWhere('m.guest_team = :guest_team')
                    ->setParameter('guest_team', $matchData["guest_team_id"])
                    ->andWhere('m.match_date = :match_date')
                    ->setParameter('match_date', $matchData["match_datetime"]->format("Y-m-d"))
                    ->andWhere('m.match_time = :match_time')
                    ->setParameter('match_time', $matchData["match_datetime"]->format("H:i:s"))
                    ->getQuery();
            
            try {
                $result = $q->getResult();                                
                if(!empty($result))                    
                    $match = array_shift($result);
            } catch (NoResultException $e) {                
                $match = null;
            }            
            
            if($match)
                return $match;
                
            $q = $this
                    ->createQueryBuilder('m')
                    ->where('m.league = :league')
                    ->setParameter('league', $matchData["league_id"])
                    ->andWhere('m.home_team = :home_team')
                    ->setParameter('home_team', $matchData["home_team_id"])
                    ->andWhere('m.guest_team = :guest_team')
                    ->setParameter('guest_team', $matchData["guest_team_id"])
                    ->andWhere('m.match_date = :match_date')
                    ->setParameter('match_date', $matchData["match_datetime"]->format("Y-m-d"))                    
                    ->getQuery();
                    
            try {                
                $result = $q->getResult();
                if(!empty($result))
                    return array_shift($result);
            } catch (NoResultException $e) {
                return null;
            }
        }
        
        /**
        * Возвращает матчи по турниру.
        * 
        * @param int $tournamentId Идентификатор турнира.
        * 
        * @return Match[]
        */
        public function findByTournament($tournamentId, array $filter = array())
        {
            $q = $this->createQueryBuilder('m')
                ->select('m, l, ht, gt')
                ->leftJoin('m.tournament_championship_matches', 'tChampionshipMatches')
                ->leftJoin('m.tournament_round_matches', 'tRoundMatches')
                ->leftJoin('m.tournament_playoff_matches', 'tPlayoffMatches')
                ->leftJoin('m.tournament_championsleague_matches', 'tChampionsleagueMatches')
                ->leftJoin('m.league', 'l')
                ->leftJoin('m.home_team', 'ht')
                ->leftJoin('m.guest_team', 'gt')
                ->where(
                    'tChampionshipMatches.tournament = :tournamentId OR ' .
                    'tRoundMatches.tournament = :tournamentId OR ' .
                    'tPlayoffMatches.tournament = :tournamentId OR ' .
                    'tChampionsleagueMatches.tournament = :tournamentId'
                )                
                ->setParameter('tournamentId', $tournamentId);            
            
            if (isset($filter["isProcessed"])) {
                $q = $q->where(
                    '(tChampionshipMatches.tournament = :tournamentId AND tChampionshipMatches.isProcessed = :isProcessed) OR ' .
                    '(tRoundMatches.tournament = :tournamentId AND tRoundMatches.isProcessed = :isProcessed) OR ' .
                    '(tPlayoffMatches.tournament = :tournamentId AND tPlayoffMatches.isProcessed = :isProcessed) OR ' .
                    '(tChampionsleagueMatches.tournament = :tournamentId AND tChampionsleagueMatches.isProcessed = :isProcessed)'
                    )
                    ->setParameter('isProcessed', $filter["isProcessed"]);
                
                unset($filter["isProcessed"]);
            }            
            
            $q = $q->addCriteria($this->createCriteriaFromFilter($filter));
            
            $q = $q->getQuery();            
            
            try {                
                return $q->getResult();
            } catch (NoResultException $e) {
                return array();
            }
        }
        
        /**
        * Возвращает номер тура/этапа неоконченного/неотменённого на данный момент матча у которого не проставлен счёт
        * 
        */
        public function getLastUnparsedMatchRound($league_id)
        {
            $query = $this->createQueryBuilder('m')
                            ->select("MIN(m.tour_number) as min_tour_number")
                            ->where("m.score_left IS NULL AND m.score_right IS NULL AND m.isRejected = :is_rejected AND m.league = :league_id")
                            ->setParameter("is_rejected", false)
                            ->setParameter("league_id", $league_id)
                            ->getQuery();
            
            try {                                
                $minRound = $query->getSingleScalarResult();                
                
                if($minRound)
                    return $minRound;                
            } catch (NoResultException $e) {
                return 1;
            }
            
            return 1;
        }
        
        /**
        *   Ищет нужный матч
        * @deprecated
        */
        public function loadMatchByData($league_id, $home_team_id, $guest_team_id, \DateTime $match_date, $tour_number)
        {            
            $q = $this
                ->createQueryBuilder('m')
                ->where('m.league = :league AND 
                         m.home_team = :home_team AND                          
                         m.guest_team = :guest_team AND
                         m.match_date = :match_date AND                         
                         m.match_time = :match_time AND                         
                         m.tour_number = :tour_number')
                ->setParameter('league', $league_id)
                ->setParameter('home_team', $home_team_id)
                ->setParameter('guest_team', $guest_team_id)
                ->setParameter('match_date', $match_date->format('Y-m-d'))                
                ->setParameter('match_time', $match_date->format('H:i:s'))                
                ->setParameter('tour_number', $tour_number)
                ->getQuery();
                                
                try {
                    $match = $q->getSingleResult();
                } catch (NoResultException $e) {                                        
                    
                    $match = null;
                }
                
                if($match != null)
                    return $match;
                    
                $q = $this
                    ->createQueryBuilder('m')
                    ->where('m.league = :league AND 
                             m.home_team = :home_team AND                          
                             m.guest_team = :guest_team AND
                             m.match_date = :match_date AND
                             m.tour_number = :tour_number')
                    ->setParameter('league', $league_id)
                    ->setParameter('home_team', $home_team_id)
                    ->setParameter('guest_team', $guest_team_id)
                    ->setParameter('match_date', $match_date->format('Y-m-d'))
                    ->setParameter('tour_number', $tour_number)
                    ->getQuery();
                    
                try {                    
                    $match = $q->getSingleResult();
                } catch (NoResultException $e) {                                        
                    
                    $match = null;
                }
                
                return $match;
        }
        
        /**
        *   Находит матчи по команде
        */
        public function findByTeam($team_id)
        {
            $q = $this
                ->createQueryBuilder('m')
                ->where('m.home_team = :team_id')
                ->orWhere('m.guest_team = :team_id')
                ->setParameter('team_id', $team_id)
                ->orderBy('m.match_date', 'ASC')
                ->addOrderBy('m.match_time', 'ASC')
                ->getQuery();
                
            try {                    
                    return $q->getResult();
                } catch (NoResultException $e) {                                        
                    
                    return null;
                }
        }
        
        /**
        *   Находит матчи по лиге
        */
        public function findByLeagueSortInverted($league_id)
        {
            $q = $this
                ->createQueryBuilder('m')                
                ->addSelect('ht, gt')
                ->leftJoin('m.home_team', 'ht')
                ->leftJoin('m.guest_team', 'gt')
                ->where('m.league = :league')
                ->setParameter('league', $league_id)
                ->orderBy('m.match_date', 'DESC')
                ->addOrderBy('m.match_time', 'DESC')
                ->getQuery();
                
            try {                    
                    return $q->getResult();
                } catch (NoResultException $e) {                                        
                    
                    return null;
                }
        }
        
        /**
        *   Находит матчи по лиге
        */
        public function findByLeague($league_id)
        {
            $q = $this
                ->createQueryBuilder('m')                
                ->addSelect('ht, gt')
                ->leftJoin('m.home_team', 'ht')
                ->leftJoin('m.guest_team', 'gt')
                ->where('m.league = :league')
                ->setParameter('league', $league_id)
                ->orderBy('m.match_date, m.match_time', 'ASC')
                ->getQuery();
                
            try {                    
                    return $q->getResult();
                } catch (NoResultException $e) {                                        
                    
                    return null;
                }
        }
        
        /**
        * Находит неначавшиеся матчи и отдаёт их в виде массива
        */
        public function findNotStartedAsArray($filter = array())
        {
            $q = $this->createQueryBuilder('m')                    
                    ->select('m.id, m.tour_number, DATE_FORMAT(m.match_date, \'%d.%m.%Y\') as match_date, DATE_FORMAT(m.match_time, \'%H:%i\') as match_time, ht.title as home_team, gt.title as guest_team')
                    ->leftJoin('m.home_team', 'ht')
                    ->leftJoin('m.guest_team', 'gt')
                    ->where('m.isStarted = 0')
                    ->andWhere('m.isOver = 0')
                    ->andWhere('m.isRejected = 0')
                    ->andWhere('m.match_time IS NOT NULL')
                    ->orderBy('m.match_date, m.match_time', 'ASC');
            
            if(!empty($filter["league"]))
            {
                $q = $q->leftJoin("m.league", "l")
                        ->andWhere("l.id = :league_id")
                        ->setParameter("league_id", $filter["league"]);
            }
            
            if(!empty($filter["exclude_matches"]))
            {
                $q->andWhere('m.id NOT IN (:excluded_ids)')->setParameter('excluded_ids', $filter["exclude_matches"]);
            }
                    
                    
            $q = $q->getQuery();                    
            try {                    
                    return $q->getResult();
                } catch (NoResultException $e) {                                        
                    
                    return null;
                }
        }
        
        /**
        *   Находит матчи которые необходимо стартовать
        */
        public function findThatNeedsToStart($currentDatetime)
        {            
            $q = $this
                ->createQueryBuilder('m')
                ->where("CONCAT(m.match_date, ' ', m.match_time) <= :now AND m.isStarted = 0 AND m.isOver = 0 AND m.isRejected = 0")
                ->setParameters(array('now' => $currentDatetime->format('Y-m-d H:i:s')))
                ->getQuery();                        
            
            try {                    
                    return $q->getResult();
                } catch (NoResultException $e) {
                    return null;
                }
        }
        
        /**
        *  Находит матчи которые не окончились и не отменились по переданной временной отметке
        * @param $matchDatetime \DateTime
        * @return \AppBundle\Entity\Match[]|null
        */
        public function findThatNeedsToReject($matchDatetime)
        {            
            $q = $this
                ->createQueryBuilder('m')
                ->where("CONCAT(m.match_date, ' ', m.match_time) <= :datetime")
                ->orWhere("m.match_date <= :date AND m.match_time IS NULL")
                ->andWhere("m.isStarted = 1 AND m.isOver = 0 AND m.isRejected = 0")
                ->setParameters(array('datetime' => $matchDatetime->format('Y-m-d H:i:s'),                                      
                                      'date' => $matchDatetime->format('Y-m-d') ))
                ->getQuery();                    
                        
            try {                    
                    return $q->getResult();
                } catch (NoResultException $e) {                                                            
                    return null;
                }
        }

        public function findLastMatches($teamId, $matchesCount)
        {
            $q = $this->createQueryBuilder('m')
                    ->addSelect('DATE_FORMAT(m.match_date, \'%d.%m.%Y\') as match_date, DATE_FORMAT(m.match_time, \'%H:%i\') as match_time')
                    ->leftJoin('m.home_team', 'ht')
                    ->leftJoin('m.guest_team', 'gt')
                    ->where('m.isStarted = 1')
                    ->andWhere('m.isOver = 1')
                    ->andWhere('m.isRejected = 0')
                    ->andWhere('ht.id = :teamId OR gt.id = :teamId')
                    ->setParameter('teamId', $teamId)
                    ->orderBy('m.match_date', 'DESC')
                    ->addOrderBy('m.match_time', 'DESC')
                    ->setFirstResult(0)
                    ->setMaxResults($matchesCount)
                    ->getQuery();
            
            try {                    
                    return $q->getResult();
                } catch (NoResultException $e) {                                        
                    
                    return null;
                }
        }

        public function findMatchesBetweenTeams($firstTeamsId, $secondTeamId, $matchesCount)
        {
            $q = $this->createQueryBuilder('m')
                    ->addSelect('DATE_FORMAT(m.match_date, \'%d.%m.%Y\') as match_date, DATE_FORMAT(m.match_time, \'%H:%i\') as match_time')
                    ->leftJoin('m.home_team', 'ht')
                    ->leftJoin('m.guest_team', 'gt')
                    ->where('m.isStarted = 1')
                    ->andWhere('m.isOver = 1')
                    ->andWhere('m.isRejected = 0')
                    ->andWhere('(ht.id = :firstTeamId AND gt.id = :secondTeamId) OR (ht.id = :secondTeamId AND gt.id = :firstTeamId)')
                    ->setParameter('firstTeamId', $firstTeamsId)
                    ->setParameter('secondTeamId', $secondTeamId)
                    ->orderBy('m.match_date', 'DESC')
                    ->addOrderBy('m.match_time', 'DESC')
                    ->setFirstResult(0)
                    ->setMaxResults($matchesCount)
                    ->getQuery();

            try {                    
                    return $q->getResult();
                } catch (NoResultException $e) {                                        
                    
                    return null;
                }
        }
}
?>
