<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * NewsRepository
 *
 */
class NewsRepository extends EntityRepository
{
    public function findAll()
    {
        $q = $this->createQueryBuilder('n')
                  ->orderBy('n.createDatetime', 'DESC')
                  ->getQuery();


        try {
            return $q->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public function findActive($maxCount)
    {
        $q = $this->createQueryBuilder('n')
                  ->where('n.isActive = 1')                  
                  ->orderBy('n.createDatetime', 'DESC')
                  ->setFirstResult(0)
                  ->setMaxResults($maxCount)
                  ->getQuery();


        try {
            return $q->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
}
