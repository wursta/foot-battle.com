<?php
namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AppBundle\Entity\PartnerCharge
 *
 * @ORM\Table(name="partner_charges")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\PartnerChargeRepository")
 */
class PartnerCharge
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")     
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="partner_charges")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $user;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="referral_charges")
     * @ORM\JoinColumn(name="referral_user_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $referral_user;
    
    /**
     * @ORM\Column(type="string")
     * @Assert\Choice({"FIRST_DEPOSIT"})
     * @Assert\NotBlank()
     */
    private $type;
    
    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank()
     */
    private $amount;
    
    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank()
     */
    private $operation_datetime;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return PartnerCharge
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return PartnerCharge
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set operationDatetime
     *
     * @param \DateTime $operationDatetime
     *
     * @return PartnerCharge
     */
    public function setOperationDatetime($operationDatetime)
    {
        $this->operation_datetime = $operationDatetime;

        return $this;
    }

    /**
     * Get operationDatetime
     *
     * @return \DateTime
     */
    public function getOperationDatetime()
    {
        return $this->operation_datetime;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return PartnerCharge
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set referralUser
     *
     * @param \AppBundle\Entity\User $referralUser
     *
     * @return PartnerCharge
     */
    public function setReferralUser(\AppBundle\Entity\User $referralUser = null)
    {
        $this->referral_user = $referralUser;

        return $this;
    }

    /**
     * Get referralUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getReferralUser()
    {
        return $this->referral_user;
    }
}
