<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AppBundle\Entity\PointsScheme
 *
 * @ORM\Table(name="points_schemas")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\PointsSchemeRepository")
 */
class PointsScheme
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="boolean", name="is_default")
     */
    private $isDefault;

    /**
     * @ORM\Column(type="float")
     */
    private $base;

    /**
     * @ORM\Column(type="float", name="add_diff_0")
     */
    private $addDiff0;

    /**
     * @ORM\Column(type="float", name="add_diff_1")
     */
    private $addDiff1;

    /**
     * @ORM\Column(type="float", name="add_diff_2")
     */
    private $addDiff2;

    /**
     * @ORM\Column(type="float", name="add_diff_3")
     */
    private $addDiff3;

    /**
     * @ORM\Column(type="float", name="add_diff_4")
     */
    private $addDiff4;

    /**
     * @ORM\Column(type="float", name="another_correct_score_points")
     */
    private $anotherCorrectScorePoints;

    /**
     * @ORM\OneToMany(targetEntity="PointsSchemeCorrectScore", mappedBy="pointsScheme", cascade={"remove", "persist"})
     */
    private $correctScores;

    /**
     * @ORM\OneToMany(targetEntity="Policy", mappedBy="pointsScheme")
     */
    private $policies;
    
    /**
     * @ORM\OneToMany(targetEntity="Tournament", mappedBy="pointsScheme")
     */
    protected $tournaments;

    private $title;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->isDefault = false;
        $this->correctScores = new \Doctrine\Common\Collections\ArrayCollection();
        $this->policies =  new \Doctrine\Common\Collections\ArrayCollection();
        $this->tournaments = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
    * Возвращает название схемы
    *
    */
    public function getTitle()
    {
        $title = 'Схема №' . $this->id . ' от ' . $this->getCreated()->format('d.m.Y');

        return $title;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return PointsScheme
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set base
     *
     * @param float $base
     *
     * @return PointsScheme
     */
    public function setBase($base)
    {
        $this->base = $base;

        return $this;
    }

    /**
     * Get base
     *
     * @return float
     */
    public function getBase()
    {
        return $this->base;
    }

    /**
     * Set addDiff0
     *
     * @param float $addDiff0
     *
     * @return PointsScheme
     */
    public function setAddDiff0($addDiff0)
    {
        $this->addDiff0 = $addDiff0;

        return $this;
    }

    /**
     * Get addDiff0
     *
     * @return float
     */
    public function getAddDiff0()
    {
        return $this->addDiff0;
    }

    /**
     * Set addDiff1
     *
     * @param float $addDiff1
     *
     * @return PointsScheme
     */
    public function setAddDiff1($addDiff1)
    {
        $this->addDiff1 = $addDiff1;

        return $this;
    }

    /**
     * Get addDiff1
     *
     * @return float
     */
    public function getAddDiff1()
    {
        return $this->addDiff1;
    }

    /**
     * Set addDiff2
     *
     * @param float $addDiff2
     *
     * @return PointsScheme
     */
    public function setAddDiff2($addDiff2)
    {
        $this->addDiff2 = $addDiff2;

        return $this;
    }

    /**
     * Get addDiff2
     *
     * @return float
     */
    public function getAddDiff2()
    {
        return $this->addDiff2;
    }

    /**
     * Set addDiff3
     *
     * @param float $addDiff3
     *
     * @return PointsScheme
     */
    public function setAddDiff3($addDiff3)
    {
        $this->addDiff3 = $addDiff3;

        return $this;
    }

    /**
     * Get addDiff3
     *
     * @return float
     */
    public function getAddDiff3()
    {
        return $this->addDiff3;
    }

    /**
     * Set addDiff4
     *
     * @param float $addDiff4
     *
     * @return PointsScheme
     */
    public function setAddDiff4($addDiff4)
    {
        $this->addDiff4 = $addDiff4;

        return $this;
    }

    /**
     * Get addDiff4
     *
     * @return float
     */
    public function getAddDiff4()
    {
        return $this->addDiff4;
    }

    /**
     * Add correctScore
     *
     * @param \AppBundle\Entity\PointsSchemeCorrectScore $correctScore
     *
     * @return PointsScheme
     */
    public function addCorrectScore(\AppBundle\Entity\PointsSchemeCorrectScore $correctScore)
    {
        $this->correctScores[] = $correctScore;

        return $this;
    }

    /**
     * Remove correctScore
     *
     * @param \AppBundle\Entity\PointsSchemeCorrectScore $correctScore
     */
    public function removeCorrectScore(\AppBundle\Entity\PointsSchemeCorrectScore $correctScore)
    {
        $this->correctScores->removeElement($correctScore);
    }

    /**
     * Get correctScores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCorrectScores()
    {
        return $this->correctScores;
    }

    /**
     * Set isDefault
     *
     * @param \boolean $isDefault
     *
     * @return PointsScheme
     */
    public function setIsDefault($isDefault)
    {
        $this->isDefault = $isDefault;

        return $this;
    }

    /**
     * Get isDefault
     *
     * @return \boolean
     */
    public function getIsDefault()
    {
        return $this->isDefault;
    }

    /**
     * Set anotherCorrectScorePoints
     *
     * @param float $anotherCorrectScorePoints
     *
     * @return PointsScheme
     */
    public function setAnotherCorrectScorePoints($anotherCorrectScorePoints)
    {
        $this->anotherCorrectScorePoints = $anotherCorrectScorePoints;

        return $this;
    }

    /**
     * Get anotherCorrectScorePoints
     *
     * @return float
     */
    public function getAnotherCorrectScorePoints()
    {
        return $this->anotherCorrectScorePoints;
    }

    /**
     * Add policy
     *
     * @param \AppBundle\Entity\Policy $policy
     *
     * @return PointsScheme
     */
    public function addPolicy(\AppBundle\Entity\Policy $policy)
    {
        $this->policies[] = $policy;

        return $this;
    }

    /**
     * Remove policy
     *
     * @param \AppBundle\Entity\Policy $policy
     */
    public function removePolicy(\AppBundle\Entity\Policy $policy)
    {
        $this->policies->removeElement($policy);
    }

    /**
     * Get policies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPolicies()
    {
        return $this->policies;
    }

    /**
     * Add tournament
     *
     * @param \AppBundle\Entity\Tournament $tournament
     *
     * @return PointsScheme
     */
    public function addTournament(\AppBundle\Entity\Tournament $tournament)
    {
        $this->tournaments[] = $tournament;

        return $this;
    }

    /**
     * Remove tournament
     *
     * @param \AppBundle\Entity\Tournament $tournament
     */
    public function removeTournament(\AppBundle\Entity\Tournament $tournament)
    {
        $this->tournaments->removeElement($tournament);
    }

    /**
     * Get tournaments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTournaments()
    {
        return $this->tournaments;
    }
}
