<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AppBundle\Entity\PointsScheme
 *
 * @ORM\Table(name="points_schemas_correct_scrores")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\PointsSchemeCorrectScoreRepository")
 */
class PointsSchemeCorrectScore
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="PointsScheme", inversedBy="correctScores")
     * @ORM\JoinColumn(name="points_scheme_id", referencedColumnName="id")
     */
    protected $pointsScheme;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $score;

    /**
     * @ORM\Column(type="float")
     */
    private $points;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set score
     *
     * @param string $score
     *
     * @return PointsSchemeCorrectScore
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return string
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set points
     *
     * @param float $points
     *
     * @return PointsSchemeCorrectScore
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points
     *
     * @return float
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * Set pointsScheme
     *
     * @param \AppBundle\Entity\PointsScheme $pointsScheme
     *
     * @return PointsSchemeCorrectScore
     */
    public function setPointsScheme(\AppBundle\Entity\PointsScheme $pointsScheme = null)
    {
        $this->pointsScheme = $pointsScheme;

        return $this;
    }

    /**
     * Get pointsScheme
     *
     * @return \AppBundle\Entity\PointsScheme
     */
    public function getPointsScheme()
    {
        return $this->pointsScheme;
    }
}
