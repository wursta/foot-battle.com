<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AppBundle\Entity\Policy
 *
 * @ORM\Table(name="policies")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\PolicyRepository")
 */
class Policy
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=2)
     * @Assert\Locale()
    */
    private $locale;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $number;

    /**
     * @ORM\ManyToOne(targetEntity="PointsScheme", inversedBy="policies")
     * @ORM\JoinColumn(name="points_scheme_id", referencedColumnName="id")
     */
    private $pointsScheme;

    /**
     * @ORM\Column(type="date", name="edition_date")
     */
    private $editionDate;

    /**
     * @ORM\Column(type="text")
     */
    private $terms;

    /**
     * @ORM\Column(type="text")
     */
    private $regulations;
    
    /**
     * @ORM\ManyToOne(targetEntity="Regulation", inversedBy="policiesWithChampionshipRegulation")
     * @ORM\JoinColumn(name="regulation_championship_id", referencedColumnName="id")
     */
    private $regulationChampionship;
    
    /**
     * @ORM\ManyToOne(targetEntity="Regulation", inversedBy="policiesWithRoundRegulation")
     * @ORM\JoinColumn(name="regulation_round_id", referencedColumnName="id")
     */
    private $regulationRound;
    
    /**
     * @ORM\ManyToOne(targetEntity="Regulation", inversedBy="policiesWithPlayoffRegulation")
     * @ORM\JoinColumn(name="regulation_playoff_id", referencedColumnName="id")
     */
    private $regulationPlayoff;
    
    /**
     * @ORM\ManyToOne(targetEntity="Regulation", inversedBy="policiesWithChampionsleagueRegulation")
     * @ORM\JoinColumn(name="regulation_championsleague_id", referencedColumnName="id")
     */
    private $regulationChampionsleague;

    /**
     * @ORM\Column(type="text")
     */
    private $privacy;

    /**
     * @ORM\Column(type="boolean", name="is_default")
     */
    private $isDefault;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->isDefault = false;
        $this->editionDate = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return Policy
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set editionDate
     *
     * @param \DateTime $editionDate
     *
     * @return Policy
     */
    public function setEditionDate($editionDate)
    {
        $this->editionDate = $editionDate;

        return $this;
    }

    /**
     * Get editionDate
     *
     * @return \DateTime
     */
    public function getEditionDate()
    {
        return $this->editionDate;
    }

    /**
     * Set terms
     *
     * @param string $terms
     *
     * @return Policy
     */
    public function setTerms($terms)
    {
        $this->terms = $terms;

        return $this;
    }

    /**
     * Get terms
     *
     * @return string
     */
    public function getTerms()
    {
        return $this->terms;
    }

    /**
     * Set regulations
     *
     * @param string $regulations
     *
     * @return Policy
     */
    public function setRegulations($regulations)
    {
        $this->regulations = $regulations;

        return $this;
    }

    /**
     * Get regulations
     *
     * @return string
     */
    public function getRegulations()
    {
        return $this->regulations;
    }

    /**
     * Set privacy
     *
     * @param string $privacy
     *
     * @return Policy
     */
    public function setPrivacy($privacy)
    {
        $this->privacy = $privacy;

        return $this;
    }

    /**
     * Get privacy
     *
     * @return string
     */
    public function getPrivacy()
    {
        return $this->privacy;
    }

    /**
     * Set isDefault
     *
     * @param boolean $isDefault
     *
     * @return Policy
     */
    public function setIsDefault($isDefault)
    {
        $this->isDefault = $isDefault;

        return $this;
    }

    /**
     * Get isDefault
     *
     * @return boolean
     */
    public function getIsDefault()
    {
        return $this->isDefault;
    }

    /**
     * Set pointsScheme
     *
     * @param \AppBundle\Entity\PointsScheme $pointsScheme
     *
     * @return Policy
     */
    public function setPointsScheme(\AppBundle\Entity\PointsScheme $pointsScheme = null)
    {        
        $this->pointsScheme = $pointsScheme;

        return $this;
    }

    /**
     * Get pointsScheme
     *
     * @return \AppBundle\Entity\PointsScheme
     */
    public function getPointsScheme()
    {
        return $this->pointsScheme;
    }

    /**
     * Set locale
     *
     * @param string $locale
     *
     * @return Policy
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get locale
     *
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }    

    /**
     * Set regulationChampionship
     *
     * @param \AppBundle\Entity\Regulation $regulationChampionship
     *
     * @return Policy
     */
    public function setRegulationChampionship(\AppBundle\Entity\Regulation $regulationChampionship = null)
    {
        $this->regulationChampionship = $regulationChampionship;

        return $this;
    }

    /**
     * Get regulationChampionship
     *
     * @return \AppBundle\Entity\Regulation
     */
    public function getRegulationChampionship()
    {
        return $this->regulationChampionship;
    }

    /**
     * Set regulationRound
     *
     * @param \AppBundle\Entity\Regulation $regulationRound
     *
     * @return Policy
     */
    public function setRegulationRound(\AppBundle\Entity\Regulation $regulationRound = null)
    {
        $this->regulationRound = $regulationRound;

        return $this;
    }

    /**
     * Get regulationRound
     *
     * @return \AppBundle\Entity\Regulation
     */
    public function getRegulationRound()
    {
        return $this->regulationRound;
    }

    /**
     * Set regulationPlayoff
     *
     * @param \AppBundle\Entity\Regulation $regulationPlayoff
     *
     * @return Policy
     */
    public function setRegulationPlayoff(\AppBundle\Entity\Regulation $regulationPlayoff = null)
    {
        $this->regulationPlayoff = $regulationPlayoff;

        return $this;
    }

    /**
     * Get regulationPlayoff
     *
     * @return \AppBundle\Entity\Regulation
     */
    public function getRegulationPlayoff()
    {
        return $this->regulationPlayoff;
    }

    /**
     * Set regulationChampionsleague
     *
     * @param \AppBundle\Entity\Regulation $regulationChampionsleague
     *
     * @return Policy
     */
    public function setRegulationChampionsleague(\AppBundle\Entity\Regulation $regulationChampionsleague = null)
    {
        $this->regulationChampionsleague = $regulationChampionsleague;

        return $this;
    }

    /**
     * Get regulationChampionsleague
     *
     * @return \AppBundle\Entity\Regulation
     */
    public function getRegulationChampionsleague()
    {
        return $this->regulationChampionsleague;
    }
}
