<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AppBundle\Entity\Regulation
 *
 * @ORM\Table(name="regulations")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\RegulationRepository")
 */
class Regulation
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=2)
     * @Assert\Locale()
    */
    private $locale;
    
    /**
     * @ORM\ManyToOne(targetEntity="TournamentFormat", inversedBy="regulations")
     * @ORM\JoinColumn(name="tournament_format_id", referencedColumnName="id")
     */
    private $tournamentFormat;
    
    /**
     * @ORM\Column(type="date", name="edition_date")
     */
    private $editionDate;
    
    /**
     * @ORM\Column(type="boolean", name="is_default")
     */
    private $isDefault;
    
    /**
     * @ORM\Column(type="text")
     */
    private $text;
    
    /**
     * @ORM\OneToMany(targetEntity="Policy", mappedBy="regulationChampionship")
     */
    private $policiesWithChampionshipRegulation;
    
    /**
     * @ORM\OneToMany(targetEntity="Policy", mappedBy="regulationRound")
     */
    private $policiesWithRoundRegulation;
    
    /**
     * @ORM\OneToMany(targetEntity="Policy", mappedBy="regulationPlayoff")
     */
    private $policiesWithPlayoffRegulation;
    
    /**
     * @ORM\OneToMany(targetEntity="Policy", mappedBy="regulationChampionsleague")
     */
    private $policiesWithChampionsleagueRegulation;
    
    /**
     * @ORM\OneToMany(targetEntity="Tournament", mappedBy="regulation")
     */
    protected $tournaments;

    public function __construct()
    {
        $this->editionDate = new \DateTime();
        $this->isDefault = false;
        $this->policiesWithChampionshipRegulation = new \Doctrine\Common\Collections\ArrayCollection();
        $this->policiesWithRoundRegulation = new \Doctrine\Common\Collections\ArrayCollection();
        $this->policiesWithPlayoffRegulation = new \Doctrine\Common\Collections\ArrayCollection();
        $this->policiesWithChampionsleagueRegulation = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tournaments = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    public function getTitle()
    {
        $title = 'Редакция от ' . $this->getEditionDate()->format('d.m.Y');

        return $title;
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set editionDate
     *
     * @param \DateTime $editionDate
     *
     * @return Regulation
     */
    public function setEditionDate($editionDate)
    {
        $this->editionDate = $editionDate;

        return $this;
    }

    /**
     * Get editionDate
     *
     * @return \DateTime
     */
    public function getEditionDate()
    {
        return $this->editionDate;
    }

    /**
     * Set isDefault
     *
     * @param boolean $isDefault
     *
     * @return Regulation
     */
    public function setIsDefault($isDefault)
    {
        $this->isDefault = $isDefault;

        return $this;
    }

    /**
     * Get isDefault
     *
     * @return boolean
     */
    public function getIsDefault()
    {
        return $this->isDefault;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Regulation
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set tournamentFormat
     *
     * @param \AppBundle\Entity\TournamentFormat $tournamentFormat
     *
     * @return Regulation
     */
    public function setTournamentFormat(\AppBundle\Entity\TournamentFormat $tournamentFormat = null)
    {
        $this->tournamentFormat = $tournamentFormat;

        return $this;
    }

    /**
     * Get tournamentFormat
     *
     * @return \AppBundle\Entity\TournamentFormat
     */
    public function getTournamentFormat()
    {
        return $this->tournamentFormat;
    }

    /**
     * Set locale
     *
     * @param string $locale
     *
     * @return Regulation
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get locale
     *
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Add policiesWithChampionshipRegulation
     *
     * @param \AppBundle\Entity\Policy $policiesWithChampionshipRegulation
     *
     * @return Regulation
     */
    public function addPoliciesWithChampionshipRegulation(\AppBundle\Entity\Policy $policiesWithChampionshipRegulation)
    {
        $this->policiesWithChampionshipRegulation[] = $policiesWithChampionshipRegulation;

        return $this;
    }

    /**
     * Remove policiesWithChampionshipRegulation
     *
     * @param \AppBundle\Entity\Policy $policiesWithChampionshipRegulation
     */
    public function removePoliciesWithChampionshipRegulation(\AppBundle\Entity\Policy $policiesWithChampionshipRegulation)
    {
        $this->policiesWithChampionshipRegulation->removeElement($policiesWithChampionshipRegulation);
    }

    /**
     * Get policiesWithChampionshipRegulation
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPoliciesWithChampionshipRegulation()
    {
        return $this->policiesWithChampionshipRegulation;
    }

    /**
     * Add policiesWithRoundRegulation
     *
     * @param \AppBundle\Entity\Policy $policiesWithRoundRegulation
     *
     * @return Regulation
     */
    public function addPoliciesWithRoundRegulation(\AppBundle\Entity\Policy $policiesWithRoundRegulation)
    {
        $this->policiesWithRoundRegulation[] = $policiesWithRoundRegulation;

        return $this;
    }

    /**
     * Remove policiesWithRoundRegulation
     *
     * @param \AppBundle\Entity\Policy $policiesWithRoundRegulation
     */
    public function removePoliciesWithRoundRegulation(\AppBundle\Entity\Policy $policiesWithRoundRegulation)
    {
        $this->policiesWithRoundRegulation->removeElement($policiesWithRoundRegulation);
    }

    /**
     * Get policiesWithRoundRegulation
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPoliciesWithRoundRegulation()
    {
        return $this->policiesWithRoundRegulation;
    }

    /**
     * Add policiesWithPlayoffRegulation
     *
     * @param \AppBundle\Entity\Policy $policiesWithPlayoffRegulation
     *
     * @return Regulation
     */
    public function addPoliciesWithPlayoffRegulation(\AppBundle\Entity\Policy $policiesWithPlayoffRegulation)
    {
        $this->policiesWithPlayoffRegulation[] = $policiesWithPlayoffRegulation;

        return $this;
    }

    /**
     * Remove policiesWithPlayoffRegulation
     *
     * @param \AppBundle\Entity\Policy $policiesWithPlayoffRegulation
     */
    public function removePoliciesWithPlayoffRegulation(\AppBundle\Entity\Policy $policiesWithPlayoffRegulation)
    {
        $this->policiesWithPlayoffRegulation->removeElement($policiesWithPlayoffRegulation);
    }

    /**
     * Get policiesWithPlayoffRegulation
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPoliciesWithPlayoffRegulation()
    {
        return $this->policiesWithPlayoffRegulation;
    }

    /**
     * Add policiesWithChampoionsleagueRegulation
     *
     * @param \AppBundle\Entity\Policy $policiesWithChampoionsleagueRegulation
     *
     * @return Regulation
     */
    public function addPoliciesWithChampoionsleagueRegulation(\AppBundle\Entity\Policy $policiesWithChampoionsleagueRegulation)
    {
        $this->policiesWithChampoionsleagueRegulation[] = $policiesWithChampoionsleagueRegulation;

        return $this;
    }    

    /**
     * Add policiesWithChampionsleagueRegulation
     *
     * @param \AppBundle\Entity\Policy $policiesWithChampionsleagueRegulation
     *
     * @return Regulation
     */
    public function addPoliciesWithChampionsleagueRegulation(\AppBundle\Entity\Policy $policiesWithChampionsleagueRegulation)
    {
        $this->policiesWithChampionsleagueRegulation[] = $policiesWithChampionsleagueRegulation;

        return $this;
    }

    /**
     * Remove policiesWithChampionsleagueRegulation
     *
     * @param \AppBundle\Entity\Policy $policiesWithChampionsleagueRegulation
     */
    public function removePoliciesWithChampionsleagueRegulation(\AppBundle\Entity\Policy $policiesWithChampionsleagueRegulation)
    {
        $this->policiesWithChampionsleagueRegulation->removeElement($policiesWithChampionsleagueRegulation);
    }

    /**
     * Get policiesWithChampionsleagueRegulation
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPoliciesWithChampionsleagueRegulation()
    {
        return $this->policiesWithChampionsleagueRegulation;
    }

    /**
     * Add tournament
     *
     * @param \AppBundle\Entity\Tournament $tournament
     *
     * @return Regulation
     */
    public function addTournament(\AppBundle\Entity\Tournament $tournament)
    {
        $this->tournaments[] = $tournament;

        return $this;
    }

    /**
     * Remove tournament
     *
     * @param \AppBundle\Entity\Tournament $tournament
     */
    public function removeTournament(\AppBundle\Entity\Tournament $tournament)
    {
        $this->tournaments->removeElement($tournament);
    }

    /**
     * Get tournaments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTournaments()
    {
        return $this->tournaments;
    }
}
