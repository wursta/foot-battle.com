<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AppBundle\Entity\ResourceTeam
 *
 * @ORM\Table(name="grab_resources_teams")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ResourceTeamRepository")
 */
class ResourceTeam
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=255)     
     * @Assert\Length(min = 2, max = 255)
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="resource_teams")
     * @ORM\JoinColumn(name="team_id", referencedColumnName="id")
     */
    protected $team;

    /**
     * @ORM\ManyToOne(targetEntity="GrabResource", inversedBy="resource_teams")
     * @ORM\JoinColumn(name="resource_id", referencedColumnName="id")
     */
    protected $resource;
    
    public function __toString()
    {
        $countryExt = new CountryExtension();
        
        if(!empty($this->city))
            return $this->title.' ('.$countryExt->countryName($this->team->getCountry()).', '.$this->team->getCity().')';
        else
            return $this->title.' ('.$countryExt->countryName($this->team->getCountry()).')';
    }
    
    /**
     * Set title
     *
     * @param string $title
     * @return ResourceTeam
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set resource
     *
     * @param \AppBundle\Entity\GrabResource $resource
     * @return ResourceTeam
     */
    public function setResource(\AppBundle\Entity\GrabResource $resource = null)
    {
        $this->resource = $resource;

        return $this;
    }

    /**
     * Get resource
     *
     * @return \AppBundle\Entity\GrabResource 
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set team
     *
     * @param \AppBundle\Entity\Team $team
     * @return ResourceTeam
     */
    public function setTeam(\AppBundle\Entity\Team $team = null)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return \AppBundle\Entity\Team 
     */
    public function getTeam()
    {
        return $this->team;
    }
}
