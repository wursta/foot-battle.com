<?php
namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ResourceTeamRepository extends EntityRepository
{
    public function findById($id)
    {
        $q = $this
                ->createQueryBuilder('rt')                
                ->where('rt.id = :id')                
                ->setParameter('id', $id)                
                ->getQuery();
        
        try {                    
            return $q->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        }
    }
    
    public function findByResource($resource_id)
    {
        $q = $this
                ->createQueryBuilder('rt')
                ->addSelect('r, t')
                ->leftJoin('rt.resource', 'r')
                ->leftJoin('rt.team', 't')
                ->where('r.id = :resource_id')                
                ->setParameter('resource_id', $resource_id)                
                ->orderBy('t.title', 'ASC')
                ->getQuery();
        
        try {                    
            return $q->getResult();
        } catch (NoResultException $e) {                                        
            
            return null;
        }
    }
}
?>
