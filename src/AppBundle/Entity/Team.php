<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Twig\CountryExtension;

/**
 * AppBundle\Entity\Team
 *
 * @ORM\Table(name="teams")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\TeamRepository")
 */
class Team
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")     
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=2)          
     * @Assert\Country()
     */
    private $country;        
    
    /**
     * @ORM\Column(type="string", length=255)     
     * @Assert\Length(min = 2, max = 255)
     */
    private $city;                

    /**
     * @ORM\Column(type="string", length=255)     
     * @Assert\Length(min = 2, max = 255)
     */
    private $title;                
    
    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;            

    /**
    * @ORM\ManyToMany(targetEntity="League", mappedBy="teams")    
    */
    protected $leagues;
    
    /**
     * @ORM\OneToMany(targetEntity="ResourceTeam", mappedBy="team")
     */
    protected $resource_teams;    

    /**
     * @ORM\OneToMany(targetEntity="Match", mappedBy="home_team")
     */
    protected $home_matches;

    /**
     * @ORM\OneToMany(targetEntity="Match", mappedBy="guest_team")
     */
    protected $guest_matches;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->resource_teams = new \Doctrine\Common\Collections\ArrayCollection();
        $this->leagues = new \Doctrine\Common\Collections\ArrayCollection();
        $this->home_matches = new \Doctrine\Common\Collections\ArrayCollection();
        $this->guest_matches = new \Doctrine\Common\Collections\ArrayCollection();
    }    
    
    public function __toString()
    {
        $countryExt = new CountryExtension();
        
        if(!empty($this->city))
            return $this->title.' ('.$countryExt->countryName($this->country).', '.$this->city.')';
        else
            return $this->title.' ('.$countryExt->countryName($this->country).')';
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Team
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return Team
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }    

    /**
     * Add resource_teams
     *
     * @param \AppBundle\Entity\ResourceTeam $resourceTeams
     * @return Team
     */
    public function addResourceTeam(\AppBundle\Entity\ResourceTeam $resourceTeams)
    {
        $this->resource_teams[] = $resourceTeams;

        return $this;
    }

    /**
     * Remove resource_teams
     *
     * @param \AppBundle\Entity\ResourceTeam $resourceTeams
     */
    public function removeResourceTeam(\AppBundle\Entity\ResourceTeam $resourceTeams)
    {
        $this->resource_teams->removeElement($resourceTeams);
    }

    /**
     * Get resource_teams
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getResourceTeams()
    {
        return $this->resource_teams;
    }

    /**
     * Add home_matches
     *
     * @param \AppBundle\Entity\Match $homeMatches
     * @return Team
     */
    public function addHomeMatch(\AppBundle\Entity\Match $homeMatches)
    {
        $this->home_matches[] = $homeMatches;

        return $this;
    }

    /**
     * Remove home_matches
     *
     * @param \AppBundle\Entity\Match $homeMatches
     */
    public function removeHomeMatch(\AppBundle\Entity\Match $homeMatches)
    {
        $this->home_matches->removeElement($homeMatches);
    }

    /**
     * Get home_matches
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHomeMatches()
    {
        return $this->home_matches;
    }

    /**
     * Add guest_matches
     *
     * @param \AppBundle\Entity\Match $guestMatches
     * @return Team
     */
    public function addGuestMatch(\AppBundle\Entity\Match $guestMatches)
    {
        $this->guest_matches[] = $guestMatches;

        return $this;
    }

    /**
     * Remove guest_matches
     *
     * @param \AppBundle\Entity\Match $guestMatches
     */
    public function removeGuestMatch(\AppBundle\Entity\Match $guestMatches)
    {
        $this->guest_matches->removeElement($guestMatches);
    }

    /**
     * Get guest_matches
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGuestMatches()
    {
        return $this->guest_matches;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Team
     */
    public function setCountry($country)
    {
        $this->country = $country;
    
        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Add leagues
     *
     * @param \AppBundle\Entity\League $leagues
     * @return Team
     */
    public function addLeague(\AppBundle\Entity\League $leagues)
    {
        $this->leagues[] = $leagues;
    
        return $this;
    }

    /**
     * Remove leagues
     *
     * @param \AppBundle\Entity\League $leagues
     */
    public function removeLeague(\AppBundle\Entity\League $leagues)
    {
        $this->leagues->removeElement($leagues);
    }

    /**
     * Get leagues
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLeagues()
    {
        return $this->leagues;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Team
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }
}
