<?php
namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

class TeamRepository extends EntityRepository
{
    public function findTeamsByLeague($league_id)
    {
        $q = $this
                ->createQueryBuilder('t')
                ->where('t.leagues = :leagues')
                ->setParameter('leagues', $league_id)
                ->orderBy('t.title', 'ASC')
                ->getQuery();
                
            try {                    
                    return $q->getResult();
                } catch (NoResultException $e) {                                        
                    
                    return null;
                }        
    }
    
    public function findTeamsByFilter($filter = array())
    {
        $q = $this->createQueryBuilder('t');        
        
        if(isset($filter['country']))
        {
            $q = $q->where('t.country = :country')->setParameter('country', $filter['country']);            
        }
        
        $q = $q->orderBy('t.country', 'ASC');
        $q = $q->orderBy('t.title', 'ASC');
        $q = $q->getQuery();
        
        try {                    
                return $q->getResult();
            } catch (NoResultException $e) {                                        
                
                return null;
            }
    }    
}
?>
