<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AppBundle\Entity\Tournament
 *
 * @ORM\Table(name="tournaments")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\TournamentRepository")
 */
class Tournament
{
    const USERS_COUNT_TYPE_LIMIT = 'LIMIT';
    const USERS_COUNT_TYPE_UNLIMIT = 'UNLIMIT';
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="tournaments")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     */
    protected $author;

    /**
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
    * @ORM\Column(type="float")
    */
    protected $fee;

    /**
    * @ORM\Column(name="init_budget", type="float")
    */
    protected $initBudget;

    /**
    * @ORM\Column(type="float")
    */
    protected $commission;

    /**
     * @ORM\ManyToOne(targetEntity="TournamentFormat", inversedBy="tournaments")
     * @ORM\JoinColumn(name="format_id", referencedColumnName="id")
     */
    protected $format;

    /**
     * @ORM\ManyToOne(targetEntity="TournamentGeography", inversedBy="tournaments")
     * @ORM\JoinColumn(name="geography_id", referencedColumnName="id")
     */
    protected $geography;

    /**
     * @ORM\ManyToMany(targetEntity="League", inversedBy="tournaments")
     * @ORM\JoinTable(name="tournaments_leagues")
     */
     protected $leagues;

    /**
     * @ORM\Column(type="string")
     * @Assert\Choice({"LIMIT", "UNLIMIT"})
     */
    protected $users_count_type;

    /**
     * @ORM\Column(name="max_users_count", type="integer")
     */
    protected $maxUsersCount;

    /**
     * @ORM\Column(name="current_round", type="integer")
     * @Assert\Range(min = 1)
     */
    protected $currentRound;

    /**
     * @ORM\OneToMany(targetEntity="TournamentChampionship", mappedBy="tournament")
     */
    protected $championships;

    /**
     * @ORM\OneToMany(targetEntity="TournamentRound", mappedBy="tournament")
     */
    protected $rounds;

    /**
     * @ORM\OneToMany(targetEntity="TournamentPlayoff", mappedBy="tournament")
     */
    protected $playoffs;

    /**
     * @ORM\OneToMany(targetEntity="TournamentChampionsleague", mappedBy="tournament")
     */
    protected $championsleagues;

    /**
     * @ORM\OneToMany(targetEntity="TournamentRoundGame", mappedBy="tournament")
     */
    protected $round_games;

    /**
     * @ORM\OneToMany(targetEntity="TournamentPlayoffGame", mappedBy="tournament")
     */
    protected $playoff_games;

    /**
     * @ORM\OneToMany(targetEntity="TournamentChampionsleagueGroupGame", mappedBy="tournament")
     */
    protected $championsleague_group_games;

    /**
     * @ORM\OneToMany(targetEntity="TournamentChampionsleaguePlayoffGame", mappedBy="tournament")
     */
    protected $championsleague_playoff_games;

    /**
     * @ORM\Column(name="start_datetime", type="datetime")
     */
    protected $startDatetime;

    /**
     * @ORM\Column(name="end_datetime", type="datetime")
     */
    protected $endDatetime;

    /**
     * @ORM\Column(type="integer")
     */
    protected $step;

    /**
     * @ORM\OneToMany(targetEntity="TournamentChampionshipMatch", mappedBy="tournament")
     */
    protected $championship_matches;

    /**
     * @ORM\OneToMany(targetEntity="TournamentRoundMatch", mappedBy="tournament")
     */
    protected $round_matches;

    /**
     * @ORM\OneToMany(targetEntity="TournamentPlayoffMatch", mappedBy="tournament")
     */
    protected $playoff_matches;

    /**
     * @ORM\OneToMany(targetEntity="TournamentChampionsleagueMatch", mappedBy="tournament")
     */
    protected $championsleague_matches;

    /**
     * @ORM\Column(name="is_open", type="boolean")
     */
    protected $isOpen;

    /**
     * @ORM\Column(name="with_confirmation", type="boolean")
     */
    protected $withConfirmation;

    /**
     * @ORM\Column(name="is_ready", type="boolean")
     */
    protected $isReady;

    /**
     * @ORM\Column(name="is_accessible", type="boolean")
     */
    protected $isAccessible;

    /**
     * @ORM\Column(name="is_started", type="boolean")
     */
    protected $isStarted;

    /**
     * @ORM\Column(name="is_over", type="boolean")
     */
    protected $isOver;

    /**
     * @ORM\Column(name="is_results_ready", type="boolean")
     */
    protected $isResultsReady;

    /**
     * @ORM\Column(name="is_winprize_distributed", type="boolean")
     */
    protected $isWinPrizeDistributed;

    /**
     * @ORM\Column(name="is_reject", type="boolean")
     */
    protected $isReject;

    /**
     * @ORM\Column(name="is_promo", type="boolean")
     */
    protected $isPromo;

    /**
     * @ORM\ManyToOne(targetEntity="Regulation", inversedBy="tournaments")
     * @ORM\JoinColumn(name="regulation_id", referencedColumnName="id")
     */
    protected $regulation;

    /**
     * @ORM\ManyToOne(targetEntity="PointsScheme", inversedBy="tournaments")
     * @ORM\JoinColumn(name="points_scheme_id", referencedColumnName="id")
     */
    protected $pointsScheme;

    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="tournament_users")
     * @ORM\JoinTable(name="tournament_users")
     */
    protected $users;

    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="tournaments_requests")
     * @ORM\JoinTable(name="tournament_users_requests")
     */
    protected $user_requests;

    /**
     * @ORM\OneToMany(targetEntity="Bet", mappedBy="tournament")
     */
    protected $bets;

    /**
     * @ORM\OneToMany(targetEntity="TournamentResult", mappedBy="tournament")
     */
    protected $results;

    /**
     * @ORM\OneToMany(targetEntity="TournamentChampionshipResult", mappedBy="tournament")
     */
    protected $championship_results;

    /**
     * @ORM\OneToMany(targetEntity="TournamentChampionshipRealTimeResult", mappedBy="tournament")
     */
    protected $championship_rt_results;

    /**
     * @ORM\OneToMany(targetEntity="TournamentRoundResult", mappedBy="tournament")
     */
    protected $round_results;

    /**
     * @ORM\OneToMany(targetEntity="TournamentPlayoffQualificationResult", mappedBy="tournament")
     */
    protected $playoff_qualification_results;

    /**
     * @ORM\OneToMany(targetEntity="TournamentPlayoffQualificationRealTimeResult", mappedBy="tournament")
     */
    protected $playoff_qualification_rt_results;

    /**
     * @ORM\OneToMany(targetEntity="TournamentChampionsleagueQualificationResult", mappedBy="tournament")
     */
    protected $championsleague_qualification_results;

    /**
     * @ORM\OneToMany(targetEntity="TournamentChampionsleagueQualificationRealTimeResult", mappedBy="tournament")
     */
    protected $championsleague_qualification_rt_results;

    /**
     * @ORM\OneToMany(targetEntity="TournamentChampionsleagueGroupResult", mappedBy="tournament")
     */
    protected $championsleague_group_results;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->currentRound = 1;
        $this->isOpen = true;
        $this->withConfirmation = false;
        $this->isReady = false;
        $this->isStarted = false;
        $this->isOver = false;
        $this->isReject = false;
        $this->isAccessible = false;
        $this->isResultsReady = false;
        $this->isWinPrizeDistributed = false;
        $this->maxUsersCount = 0;
        $this->currentRound = 1;
        $this->step = 1;        
        $this->leagues = new \Doctrine\Common\Collections\ArrayCollection();
        $this->championships = new \Doctrine\Common\Collections\ArrayCollection();
        $this->rounds = new \Doctrine\Common\Collections\ArrayCollection();
        $this->playoffs = new \Doctrine\Common\Collections\ArrayCollection();
        $this->championsleagues = new \Doctrine\Common\Collections\ArrayCollection();
        $this->round_games = new \Doctrine\Common\Collections\ArrayCollection();
        $this->playoff_games = new \Doctrine\Common\Collections\ArrayCollection();
        $this->championsleague_playoff_games = new \Doctrine\Common\Collections\ArrayCollection();
        $this->championship_matches = new \Doctrine\Common\Collections\ArrayCollection();
        $this->round_matches = new \Doctrine\Common\Collections\ArrayCollection();
        $this->playoff_matches = new \Doctrine\Common\Collections\ArrayCollection();
        $this->championsleague_matches = new \Doctrine\Common\Collections\ArrayCollection();
        $this->users_requests = new \Doctrine\Common\Collections\ArrayCollection();
        $this->bets = new \Doctrine\Common\Collections\ArrayCollection();
        $this->results = new \Doctrine\Common\Collections\ArrayCollection();
        $this->championship_results = new \Doctrine\Common\Collections\ArrayCollection();
        $this->championship_rt_results = new \Doctrine\Common\Collections\ArrayCollection();
        $this->round_results = new \Doctrine\Common\Collections\ArrayCollection();
        $this->playoff_qualification_results = new \Doctrine\Common\Collections\ArrayCollection();
        $this->playoff_qualification_rt_results = new \Doctrine\Common\Collections\ArrayCollection();
        $this->championsleague_qualification_results = new \Doctrine\Common\Collections\ArrayCollection();
        $this->championsleague_qualification_rt_results = new \Doctrine\Common\Collections\ArrayCollection();
        $this->championsleague_group_results = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set users_count_type
     *
     * @param string $usersCountType
     * @return Tournament
     */
    public function setUsersCountType($usersCountType)
    {
        $this->users_count_type = $usersCountType;

        return $this;
    }

    /**
     * Get users_count_type
     *
     * @return string
     */
    public function getUsersCountType()
    {
        return $this->users_count_type;
    }

    /**
     * Set step
     *
     * @param integer $step
     * @return Tournament
     */
    public function setStep($step)
    {
        $this->step = $step;

        return $this;
    }

    /**
     * Get step
     *
     * @return integer
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * Set isReady
     *
     * @param boolean $isReady
     * @return Tournament
     */
    public function setIsReady($isReady)
    {
        $this->isReady = $isReady;

        return $this;
    }

    /**
     * Get isReady
     *
     * @return boolean
     */
    public function getIsReady()
    {
        return $this->isReady;
    }

    /**
     * Set isStarted
     *
     * @param boolean $isStarted
     * @return Tournament
     */
    public function setIsStarted($isStarted)
    {
        $this->isStarted = $isStarted;

        return $this;
    }

    /**
     * Get isStarted
     *
     * @return boolean
     */
    public function getIsStarted()
    {
        return $this->isStarted;
    }

    /**
     * Set isOver
     *
     * @param boolean $isOver
     * @return Tournament
     */
    public function setIsOver($isOver)
    {
        $this->isOver = $isOver;

        return $this;
    }

    /**
     * Get isOver
     *
     * @return boolean
     */
    public function getIsOver()
    {
        return $this->isOver;
    }

    /**
     * Set isResultsReady
     *
     * @param boolean $isResultsReady
     * @return Tournament
     */
    public function setIsResultsReady($isResultsReady)
    {
        $this->isResultsReady = $isResultsReady;

        return $this;
    }

    /**
     * Get isResultsReady
     *
     * @return boolean
     */
    public function getIsResultsReady()
    {
        return $this->isResultsReady;
    }

    /**
     * Set author
     *
     * @param \AppBundle\Entity\User $author
     * @return Tournament
     */
    public function setAuthor(\AppBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \AppBundle\Entity\User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set format
     *
     * @param \AppBundle\Entity\TournamentFormat $format
     * @return Tournament
     */
    public function setFormat(\AppBundle\Entity\TournamentFormat $format = null)
    {
        $this->format = $format;

        return $this;
    }

    /**
     * Get format
     *
     * @return \AppBundle\Entity\TournamentFormat
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Add championships
     *
     * @param \AppBundle\Entity\TournamentChampionship $championships
     * @return Tournament
     */
    public function addChampionship(\AppBundle\Entity\TournamentChampionship $championships)
    {
        $this->championships[] = $championships;

        return $this;
    }

    /**
     * Remove championships
     *
     * @param \AppBundle\Entity\TournamentChampionship $championships
     */
    public function removeChampionship(\AppBundle\Entity\TournamentChampionship $championships)
    {
        $this->championships->removeElement($championships);
    }

    /**
     * Get championships
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChampionships()
    {
        return $this->championships;
    }

    /**
     * Add rounds
     *
     * @param \AppBundle\Entity\TournamentRound $rounds
     * @return Tournament
     */
    public function addRound(\AppBundle\Entity\TournamentRound $rounds)
    {
        $this->rounds[] = $rounds;

        return $this;
    }

    /**
     * Remove rounds
     *
     * @param \AppBundle\Entity\TournamentRound $rounds
     */
    public function removeRound(\AppBundle\Entity\TournamentRound $rounds)
    {
        $this->rounds->removeElement($rounds);
    }

    /**
     * Get rounds
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRounds()
    {
        return $this->rounds;
    }

    /**
     * Add championship_matches
     *
     * @param \AppBundle\Entity\TournamentChampionshipMatch $championshipMatches
     * @return Tournament
     */
    public function addChampionshipMatch(\AppBundle\Entity\TournamentChampionshipMatch $championshipMatches)
    {
        $this->championship_matches[] = $championshipMatches;

        return $this;
    }

    /**
     * Remove championship_matches
     *
     * @param \AppBundle\Entity\TournamentChampionshipMatch $championshipMatches
     */
    public function removeChampionshipMatch(\AppBundle\Entity\TournamentChampionshipMatch $championshipMatches)
    {
        $this->championship_matches->removeElement($championshipMatches);
    }

    /**
     * Get championship_matches
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChampionshipMatches()
    {
        return $this->championship_matches;
    }

    /**
     * Add users
     *
     * @param \AppBundle\Entity\User $users
     * @return Tournament
     */
    public function addUser(\AppBundle\Entity\User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \AppBundle\Entity\User $users
     */
    public function removeUser(\AppBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add user_requests
     *
     * @param \AppBundle\Entity\User $userRequests
     * @return Tournament
     */
    public function addUserRequest(\AppBundle\Entity\User $userRequests)
    {
        $this->user_requests[] = $userRequests;

        return $this;
    }

    /**
     * Remove user_requests
     *
     * @param \AppBundle\Entity\User $userRequests
     */
    public function removeUserRequest(\AppBundle\Entity\User $userRequests)
    {
        $this->user_requests->removeElement($userRequests);
    }

    /**
     * Get user_requests
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserRequests()
    {
        return $this->user_requests;
    }

    /**
     * Add bets
     *
     * @param \AppBundle\Entity\Bet $bets
     * @return Tournament
     */
    public function addBet(\AppBundle\Entity\Bet $bets)
    {
        $this->bets[] = $bets;

        return $this;
    }

    /**
     * Remove bets
     *
     * @param \AppBundle\Entity\Bet $bets
     */
    public function removeBet(\AppBundle\Entity\Bet $bets)
    {
        $this->bets->removeElement($bets);
    }

    /**
     * Get bets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBets()
    {
        return $this->bets;
    }

    /**
     * Add results
     *
     * @param \AppBundle\Entity\TournamentResult $results
     * @return Tournament
     */
    public function addResult(\AppBundle\Entity\TournamentResult $results)
    {
        $this->results[] = $results;

        return $this;
    }

    /**
     * Remove results
     *
     * @param \AppBundle\Entity\TournamentResult $results
     */
    public function removeResult(\AppBundle\Entity\TournamentResult $results)
    {
        $this->results->removeElement($results);
    }

    /**
     * Get results
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResults()
    {
        return $this->results;
    }

    /**
     * Set startDatetime
     *
     * @param \DateTime $startDatetime
     * @return Tournament
     */
    public function setStartDatetime($startDatetime)
    {
        $this->startDatetime = $startDatetime;

        return $this;
    }

    /**
     * Get startDatetime
     *
     * @return \DateTime
     */
    public function getStartDatetime()
    {
        return $this->startDatetime;
    }

    /**
     * Add round_matches
     *
     * @param \AppBundle\Entity\TournamentRoundMatch $roundMatches
     * @return Tournament
     */
    public function addRoundMatch(\AppBundle\Entity\TournamentRoundMatch $roundMatches)
    {
        $this->round_matches[] = $roundMatches;

        return $this;
    }

    /**
     * Remove round_matches
     *
     * @param \AppBundle\Entity\TournamentRoundMatch $roundMatches
     */
    public function removeRoundMatch(\AppBundle\Entity\TournamentRoundMatch $roundMatches)
    {
        $this->round_matches->removeElement($roundMatches);
    }

    /**
     * Get round_matches
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRoundMatches()
    {
        return $this->round_matches;
    }

    /**
     * Add round_games
     *
     * @param \AppBundle\Entity\TournamentRoundGame $roundGames
     * @return Tournament
     */
    public function addRoundGame(\AppBundle\Entity\TournamentRoundGame $roundGames)
    {
        $this->round_games[] = $roundGames;

        return $this;
    }

    /**
     * Remove round_games
     *
     * @param \AppBundle\Entity\TournamentRoundGame $roundGames
     */
    public function removeRoundGame(\AppBundle\Entity\TournamentRoundGame $roundGames)
    {
        $this->round_games->removeElement($roundGames);
    }

    /**
     * Get round_games
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRoundGames()
    {
        return $this->round_games;
    }

    /**
     * Add championship_results
     *
     * @param \AppBundle\Entity\TournamentChampionshipResult $championshipResults
     * @return Tournament
     */
    public function addChampionshipResult(\AppBundle\Entity\TournamentChampionshipResult $championshipResults)
    {
        $this->championship_results[] = $championshipResults;

        return $this;
    }

    /**
     * Remove championship_results
     *
     * @param \AppBundle\Entity\TournamentChampionshipResult $championshipResults
     */
    public function removeChampionshipResult(\AppBundle\Entity\TournamentChampionshipResult $championshipResults)
    {
        $this->championship_results->removeElement($championshipResults);
    }

    /**
     * Get championship_results
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChampionshipResults()
    {
        return $this->championship_results;
    }

    /**
     * Add round_results
     *
     * @param \AppBundle\Entity\TournamentRoundResult $roundResults
     * @return Tournament
     */
    public function addRoundResult(\AppBundle\Entity\TournamentRoundResult $roundResults)
    {
        $this->round_results[] = $roundResults;

        return $this;
    }

    /**
     * Remove round_results
     *
     * @param \AppBundle\Entity\TournamentRoundResult $roundResults
     */
    public function removeRoundResult(\AppBundle\Entity\TournamentRoundResult $roundResults)
    {
        $this->round_results->removeElement($roundResults);
    }

    /**
     * Get round_results
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRoundResults()
    {
        return $this->round_results;
    }

    /**
     * Set isReject
     *
     * @param boolean $isReject
     * @return Tournament
     */
    public function setIsReject($isReject)
    {
        $this->isReject = $isReject;

        return $this;
    }

    /**
     * Get isReject
     *
     * @return boolean
     */
    public function getIsReject()
    {
        return $this->isReject;
    }

    /**
     * Add playoffs
     *
     * @param \AppBundle\Entity\TournamentPlayoff $playoffs
     * @return Tournament
     */
    public function addPlayoff(\AppBundle\Entity\TournamentPlayoff $playoffs)
    {
        $this->playoffs[] = $playoffs;

        return $this;
    }

    /**
     * Remove playoffs
     *
     * @param \AppBundle\Entity\TournamentPlayoff $playoffs
     */
    public function removePlayoff(\AppBundle\Entity\TournamentPlayoff $playoffs)
    {
        $this->playoffs->removeElement($playoffs);
    }

    /**
     * Get playoffs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayoffs()
    {
        return $this->playoffs;
    }

    /**
     * Add playoff_matches
     *
     * @param \AppBundle\Entity\TournamentPlayoffMatch $playoffMatches
     * @return Tournament
     */
    public function addPlayoffMatch(\AppBundle\Entity\TournamentPlayoffMatch $playoffMatches)
    {
        $this->playoff_matches[] = $playoffMatches;

        return $this;
    }

    /**
     * Remove playoff_matches
     *
     * @param \AppBundle\Entity\TournamentPlayoffMatch $playoffMatches
     */
    public function removePlayoffMatch(\AppBundle\Entity\TournamentPlayoffMatch $playoffMatches)
    {
        $this->playoff_matches->removeElement($playoffMatches);
    }

    /**
     * Get playoff_matches
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayoffMatches()
    {
        return $this->playoff_matches;
    }

    /**
     * Add playoff_qualification_results
     *
     * @param \AppBundle\Entity\TournamentPlayoffQualificationResult $playoffQualificationResults
     * @return Tournament
     */
    public function addPlayoffQualificationResult(\AppBundle\Entity\TournamentPlayoffQualificationResult $playoffQualificationResults)
    {
        $this->playoff_qualification_results[] = $playoffQualificationResults;

        return $this;
    }

    /**
     * Remove playoff_qualification_results
     *
     * @param \AppBundle\Entity\TournamentPlayoffQualificationResult $playoffQualificationResults
     */
    public function removePlayoffQualificationResult(\AppBundle\Entity\TournamentPlayoffQualificationResult $playoffQualificationResults)
    {
        $this->playoff_qualification_results->removeElement($playoffQualificationResults);
    }

    /**
     * Get playoff_qualification_results
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayoffQualificationResults()
    {
        return $this->playoff_qualification_results;
    }

    /**
     * Add playoff_games
     *
     * @param \AppBundle\Entity\TournamentPlayoffGame $playoffGames
     * @return Tournament
     */
    public function addPlayoffGame(\AppBundle\Entity\TournamentPlayoffGame $playoffGames)
    {
        $this->playoff_games[] = $playoffGames;

        return $this;
    }

    /**
     * Remove playoff_games
     *
     * @param \AppBundle\Entity\TournamentPlayoffGame $playoffGames
     */
    public function removePlayoffGame(\AppBundle\Entity\TournamentPlayoffGame $playoffGames)
    {
        $this->playoff_games->removeElement($playoffGames);
    }

    /**
     * Get playoff_games
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayoffGames()
    {
        return $this->playoff_games;
    }

    /**
     * Set endDatetime
     *
     * @param \DateTime $endDatetime
     * @return Tournament
     */
    public function setEndDatetime($endDatetime)
    {
        $this->endDatetime = $endDatetime;

        return $this;
    }

    /**
     * Get endDatetime
     *
     * @return \DateTime
     */
    public function getEndDatetime()
    {
        return $this->endDatetime;
    }

    /**
     * Add champion_leagues
     *
     * @param \AppBundle\Entity\TournamentChampionsleague $championLeagues
     * @return Tournament
     */
    public function addChampionLeague(\AppBundle\Entity\TournamentChampionsleague $championLeagues)
    {
        $this->champion_leagues[] = $championLeagues;

        return $this;
    }

    /**
     * Remove champion_leagues
     *
     * @param \AppBundle\Entity\TournamentChampionsleague $championLeagues
     */
    public function removeChampionLeague(\AppBundle\Entity\TournamentChampionsleague $championLeagues)
    {
        $this->champion_leagues->removeElement($championLeagues);
    }

    /**
     * Get champion_leagues
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChampionLeagues()
    {
        return $this->champion_leagues;
    }

    /**
     * Add championsleagues
     *
     * @param \AppBundle\Entity\TournamentChampionsleague $championsleagues
     * @return Tournament
     */
    public function addChampionsleague(\AppBundle\Entity\TournamentChampionsleague $championsleagues)
    {
        $this->championsleagues[] = $championsleagues;

        return $this;
    }

    /**
     * Remove championsleagues
     *
     * @param \AppBundle\Entity\TournamentChampionsleague $championsleagues
     */
    public function removeChampionsleague(\AppBundle\Entity\TournamentChampionsleague $championsleagues)
    {
        $this->championsleagues->removeElement($championsleagues);
    }

    /**
     * Get championsleagues
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChampionsleagues()
    {
        return $this->championsleagues;
    }

    /**
     * Add championsleague_matches
     *
     * @param \AppBundle\Entity\TournamentChampionsleagueMatch $championsleagueMatches
     * @return Tournament
     */
    public function addChampionsleagueMatch(\AppBundle\Entity\TournamentChampionsleagueMatch $championsleagueMatches)
    {
        $this->championsleague_matches[] = $championsleagueMatches;

        return $this;
    }

    /**
     * Remove championsleague_matches
     *
     * @param \AppBundle\Entity\TournamentChampionsleagueMatch $championsleagueMatches
     */
    public function removeChampionsleagueMatch(\AppBundle\Entity\TournamentChampionsleagueMatch $championsleagueMatches)
    {
        $this->championsleague_matches->removeElement($championsleagueMatches);
    }

    /**
     * Get championsleague_matches
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChampionsleagueMatches()
    {
        return $this->championsleague_matches;
    }

    /**
     * Add championsleague_qualification_results
     *
     * @param \AppBundle\Entity\TournamentChampionsleagueQualificationResult $championsleagueQualificationResults
     * @return Tournament
     */
    public function addChampionsleagueQualificationResult(\AppBundle\Entity\TournamentChampionsleagueQualificationResult $championsleagueQualificationResults)
    {
        $this->championsleague_qualification_results[] = $championsleagueQualificationResults;

        return $this;
    }

    /**
     * Remove championsleague_qualification_results
     *
     * @param \AppBundle\Entity\TournamentChampionsleagueQualificationResult $championsleagueQualificationResults
     */
    public function removeChampionsleagueQualificationResult(\AppBundle\Entity\TournamentChampionsleagueQualificationResult $championsleagueQualificationResults)
    {
        $this->championsleague_qualification_results->removeElement($championsleagueQualificationResults);
    }

    /**
     * Get championsleague_qualification_results
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChampionsleagueQualificationResults()
    {
        return $this->championsleague_qualification_results;
    }

    /**
     * Add championsleague_group_games
     *
     * @param \AppBundle\Entity\TournamentChampionsleagueGroupGame $championsleagueGroupGames
     * @return Tournament
     */
    public function addChampionsleagueGroupGame(\AppBundle\Entity\TournamentChampionsleagueGroupGame $championsleagueGroupGames)
    {
        $this->championsleague_group_games[] = $championsleagueGroupGames;

        return $this;
    }

    /**
     * Remove championsleague_group_games
     *
     * @param \AppBundle\Entity\TournamentChampionsleagueGroupGame $championsleagueGroupGames
     */
    public function removeChampionsleagueGroupGame(\AppBundle\Entity\TournamentChampionsleagueGroupGame $championsleagueGroupGames)
    {
        $this->championsleague_group_games->removeElement($championsleagueGroupGames);
    }

    /**
     * Get championsleague_group_games
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChampionsleagueGroupGames()
    {
        return $this->championsleague_group_games;
    }

    /**
     * Add championsleague_group_results
     *
     * @param \AppBundle\Entity\TournamentChampionsleagueGroupResult $championsleagueGroupResults
     * @return Tournament
     */
    public function addChampionsleagueGroupResult(\AppBundle\Entity\TournamentChampionsleagueGroupResult $championsleagueGroupResults)
    {
        $this->championsleague_group_results[] = $championsleagueGroupResults;

        return $this;
    }

    /**
     * Remove championsleague_group_results
     *
     * @param \AppBundle\Entity\TournamentChampionsleagueGroupResult $championsleagueGroupResults
     */
    public function removeChampionsleagueGroupResult(\AppBundle\Entity\TournamentChampionsleagueGroupResult $championsleagueGroupResults)
    {
        $this->championsleague_group_results->removeElement($championsleagueGroupResults);
    }

    /**
     * Get championsleague_group_results
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChampionsleagueGroupResults()
    {
        return $this->championsleague_group_results;
    }

    /**
     * Add championsleague_playoff_games
     *
     * @param \AppBundle\Entity\TournamentChampionsleaguePlayoffGame $championsleaguePlayoffGames
     * @return Tournament
     */
    public function addChampionsleaguePlayoffGame(\AppBundle\Entity\TournamentChampionsleaguePlayoffGame $championsleaguePlayoffGames)
    {
        $this->championsleague_playoff_games[] = $championsleaguePlayoffGames;

        return $this;
    }

    /**
     * Remove championsleague_playoff_games
     *
     * @param \AppBundle\Entity\TournamentChampionsleaguePlayoffGame $championsleaguePlayoffGames
     */
    public function removeChampionsleaguePlayoffGame(\AppBundle\Entity\TournamentChampionsleaguePlayoffGame $championsleaguePlayoffGames)
    {
        $this->championsleague_playoff_games->removeElement($championsleaguePlayoffGames);
    }

    /**
     * Get championsleague_playoff_games
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChampionsleaguePlayoffGames()
    {
        return $this->championsleague_playoff_games;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Tournament
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set fee
     *
     * @param float $fee
     * @return Tournament
     */
    public function setFee($fee)
    {
        $this->fee = $fee;

        return $this;
    }

    /**
     * Get fee
     *
     * @return float
     */
    public function getFee()
    {
        return $this->fee;
    }

    /**
     * Set commission
     *
     * @param float $commission
     * @return Tournament
     */
    public function setCommission($commission)
    {
        $this->commission = $commission;

        return $this;
    }

    /**
     * Get commission
     *
     * @return float
     */
    public function getCommission()
    {
        return $this->commission;
    }

    /**
     * Set isWinPrizeDistributed
     *
     * @param boolean $isWinPrizeDistributed
     * @return Tournament
     */
    public function setIsWinPrizeDistributed($isWinPrizeDistributed)
    {
        $this->isWinPrizeDistributed = $isWinPrizeDistributed;

        return $this;
    }

    /**
     * Get isWinPrizeDistributed
     *
     * @return boolean
     */
    public function getIsWinPrizeDistributed()
    {
        return $this->isWinPrizeDistributed;
    }

    /**
     * Set isAccessible
     *
     * @param boolean $isAccessible
     * @return Tournament
     */
    public function setIsAccessible($isAccessible)
    {
        $this->isAccessible = $isAccessible;

        return $this;
    }

    /**
     * Get isAccessible
     *
     * @return boolean
     */
    public function getIsAccessible()
    {
        return $this->isAccessible;
    }

    /**
     * Add championship_rt_results
     *
     * @param \AppBundle\Entity\TournamentChampionshipRealTimeResult $championshipRtResults
     * @return Tournament
     */
    public function addChampionshipRtResult(\AppBundle\Entity\TournamentChampionshipRealTimeResult $championshipRtResults)
    {
        $this->championship_rt_results[] = $championshipRtResults;

        return $this;
    }

    /**
     * Remove championship_rt_results
     *
     * @param \AppBundle\Entity\TournamentChampionshipRealTimeResult $championshipRtResults
     */
    public function removeChampionshipRtResult(\AppBundle\Entity\TournamentChampionshipRealTimeResult $championshipRtResults)
    {
        $this->championship_rt_results->removeElement($championshipRtResults);
    }

    /**
     * Get championship_rt_results
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChampionshipRtResults()
    {
        return $this->championship_rt_results;
    }

    /**
     * Add championsleague_qualification_rt_results
     *
     * @param \AppBundle\Entity\TournamentChampionsleagueQualificationRealTimeResult $championsleagueQualificationRtResults
     * @return Tournament
     */
    public function addChampionsleagueQualificationRtResult(\AppBundle\Entity\TournamentChampionsleagueQualificationRealTimeResult $championsleagueQualificationRtResults)
    {
        $this->championsleague_qualification_rt_results[] = $championsleagueQualificationRtResults;

        return $this;
    }

    /**
     * Remove championsleague_qualification_rt_results
     *
     * @param \AppBundle\Entity\TournamentChampionsleagueQualificationRealTimeResult $championsleagueQualificationRtResults
     */
    public function removeChampionsleagueQualificationRtResult(\AppBundle\Entity\TournamentChampionsleagueQualificationRealTimeResult $championsleagueQualificationRtResults)
    {
        $this->championsleague_qualification_rt_results->removeElement($championsleagueQualificationRtResults);
    }

    /**
     * Get championsleague_qualification_rt_results
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChampionsleagueQualificationRtResults()
    {
        return $this->championsleague_qualification_rt_results;
    }

    /**
     * Set isOpen
     *
     * @param boolean $isOpen
     *
     * @return Tournament
     */
    public function setIsOpen($isOpen)
    {
        $this->isOpen = $isOpen;

        return $this;
    }

    /**
     * Get isOpen
     *
     * @return boolean
     */
    public function getIsOpen()
    {
        return $this->isOpen;
    }

    /**
     * Set withConfirmation
     *
     * @param boolean $withConfirmation
     *
     * @return Tournament
     */
    public function setWithConfirmation($withConfirmation)
    {
        $this->withConfirmation = $withConfirmation;

        return $this;
    }

    /**
     * Get withConfirmation
     *
     * @return boolean
     */
    public function getWithConfirmation()
    {
        return $this->withConfirmation;
    }

    /**
     * Add playoffQualificationRtResult
     *
     * @param \AppBundle\Entity\TournamentPlayoffQualificationRealTimeResult $playoffQualificationRtResult
     *
     * @return Tournament
     */
    public function addPlayoffQualificationRtResult(\AppBundle\Entity\TournamentPlayoffQualificationRealTimeResult $playoffQualificationRtResult)
    {
        $this->playoff_qualification_rt_results[] = $playoffQualificationRtResult;

        return $this;
    }

    /**
     * Remove playoffQualificationRtResult
     *
     * @param \AppBundle\Entity\TournamentPlayoffQualificationRealTimeResult $playoffQualificationRtResult
     */
    public function removePlayoffQualificationRtResult(\AppBundle\Entity\TournamentPlayoffQualificationRealTimeResult $playoffQualificationRtResult)
    {
        $this->playoff_qualification_rt_results->removeElement($playoffQualificationRtResult);
    }

    /**
     * Get playoffQualificationRtResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlayoffQualificationRtResults()
    {
        return $this->playoff_qualification_rt_results;
    }

    /**
     * Add league
     *
     * @param \AppBundle\Entity\League $league
     *
     * @return Tournament
     */
    public function addLeague(\AppBundle\Entity\League $league)
    {
        $this->leagues[] = $league;

        return $this;
    }

    /**
     * Remove league
     *
     * @param \AppBundle\Entity\League $league
     */
    public function removeLeague(\AppBundle\Entity\League $league)
    {
        $this->leagues->removeElement($league);
    }

    /**
     * Get leagues
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLeagues()
    {
        return $this->leagues;
    }

    /**
     * Set geography
     *
     * @param \AppBundle\Entity\TournamentGeography $geography
     *
     * @return Tournament
     */
    public function setGeography(\AppBundle\Entity\TournamentGeography $geography = null)
    {
        $this->geography = $geography;

        return $this;
    }

    /**
     * Get geography
     *
     * @return \AppBundle\Entity\TournamentGeography
     */
    public function getGeography()
    {
        return $this->geography;
    }

    /**
     * Set initBudget
     *
     * @param float $initBudget
     *
     * @return Tournament
     */
    public function setInitBudget($initBudget)
    {
        $this->initBudget = $initBudget;

        return $this;
    }

    /**
     * Get initBudget
     *
     * @return float
     */
    public function getInitBudget()
    {
        return $this->initBudget;
    }

    /**
     * Set isPromo
     *
     * @param boolean $isPromo
     *
     * @return Tournament
     */
    public function setIsPromo($isPromo)
    {
        $this->isPromo = $isPromo;

        return $this;
    }

    /**
     * Get isPromo
     *
     * @return boolean
     */
    public function getIsPromo()
    {
        return $this->isPromo;
    }

    /**
     * Set regulation
     *
     * @param \AppBundle\Entity\Regulation $regulation
     *
     * @return Tournament
     */
    public function setRegulation(\AppBundle\Entity\Regulation $regulation = null)
    {
        $this->regulation = $regulation;

        return $this;
    }

    /**
     * Get regulation
     *
     * @return \AppBundle\Entity\Regulation
     */
    public function getRegulation()
    {
        return $this->regulation;
    }

    /**
     * Set pointsScheme
     *
     * @param \AppBundle\Entity\PointsScheme $pointsScheme
     *
     * @return Tournament
     */
    public function setPointsScheme(\AppBundle\Entity\PointsScheme $pointsScheme = null)
    {
        $this->pointsScheme = $pointsScheme;

        return $this;
    }

    /**
     * Get pointsScheme
     *
     * @return \AppBundle\Entity\PointsScheme
     */
    public function getPointsScheme()
    {
        return $this->pointsScheme;
    }

    /**
     * Set maxUsersCount
     *
     * @param integer $maxUsersCount
     *
     * @return Tournament
     */
    public function setMaxUsersCount($maxUsersCount)
    {
        $this->maxUsersCount = $maxUsersCount;

        return $this;
    }

    /**
     * Get maxUsersCount
     *
     * @return integer
     */
    public function getMaxUsersCount()
    {
        return $this->maxUsersCount;
    }

    /**
     * Set currentRound
     *
     * @param integer $currentRound
     *
     * @return Tournament
     */
    public function setCurrentRound($currentRound)
    {
        $this->currentRound = $currentRound;

        return $this;
    }

    /**
     * Get currentRound
     *
     * @return integer
     */
    public function getCurrentRound()
    {
        return $this->currentRound;
    }
}
