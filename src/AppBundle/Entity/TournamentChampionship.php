<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AppBundle\Entity\TournamentChampionship
 *
 * @ORM\Table(name="tournaments_championship")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\TournamentChampionshipRepository")
 */
class TournamentChampionship
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")     
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Tournament", inversedBy="championships")
     * @ORM\JoinColumn(name="tournament_id", referencedColumnName="id")
     */
    protected $tournament;
    
    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min = 1)
     */
    protected $rounds_count;
    
    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min = 1)
     */
    protected $matches_in_round;    

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min = 2)
     */
    protected $max_users_count;
    
    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min = 1)
     */
    protected $current_round;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->current_round = 1;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rounds_count
     *
     * @param integer $roundsCount
     * @return TournamentChampionship
     */
    public function setRoundsCount($roundsCount)
    {
        $this->rounds_count = $roundsCount;

        return $this;
    }

    /**
     * Get rounds_count
     *
     * @return integer 
     */
    public function getRoundsCount()
    {
        return $this->rounds_count;
    }

    /**
     * Set matches_in_round
     *
     * @param integer $matchesInRound
     * @return TournamentChampionship
     */
    public function setMatchesInRound($matchesInRound)
    {
        $this->matches_in_round = $matchesInRound;

        return $this;
    }

    /**
     * Get matches_in_round
     *
     * @return integer 
     */
    public function getMatchesInRound()
    {
        return $this->matches_in_round;
    }

    /**
     * Set max_users_count
     *
     * @param integer $maxUsersCount
     * @return TournamentChampionship
     */
    public function setMaxUsersCount($maxUsersCount)
    {
        $this->max_users_count = $maxUsersCount;

        return $this;
    }

    /**
     * Get max_users_count
     *
     * @return integer 
     */
    public function getMaxUsersCount()
    {
        return $this->max_users_count;
    }

    /**
     * Set tournament
     *
     * @param \AppBundle\Entity\Tournament $tournament
     * @return TournamentChampionship
     */
    public function setTournament(\AppBundle\Entity\Tournament $tournament = null)
    {
        $this->tournament = $tournament;

        return $this;
    }

    /**
     * Get tournament
     *
     * @return \AppBundle\Entity\Tournament 
     */
    public function getTournament()
    {
        return $this->tournament;
    }

    /**
     * Set current_round
     *
     * @param integer $currentRound
     * @return TournamentChampionship
     */
    public function setCurrentRound($currentRound)
    {
        $this->current_round = $currentRound;

        return $this;
    }

    /**
     * Get current_round
     *
     * @return integer 
     */
    public function getCurrentRound()
    {
        return $this->current_round;
    }
}
