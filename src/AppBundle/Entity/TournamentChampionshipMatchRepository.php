<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

class TournamentChampionshipMatchRepository extends EntityRepository
{
    /**
    * Поиск матчей турнира по ID турнира со всеми связанными объектами
    * 
    * @param int $tournamentId ID турнира
    * @return \AppBundle\Entity\TournamentChampionshipMatch[]
    */
    public function findByTournament($tournamentId)
    {
        $query = $this->createQueryBuilder('tm')
                      ->select('tm, m, l, ht, gt')
                      ->leftJoin('tm.tournament', 't')
                      ->leftJoin('tm.match', 'm', Join::WITH, 'm.id = tm.match')
                      ->leftJoin('m.league', 'l')
                      ->leftJoin('m.home_team', 'ht')
                      ->leftJoin('m.guest_team', 'gt')
                      ->where('t.id = :tournament_id')
                      ->setParameter('tournament_id', $tournamentId)
                      ->groupBy('tm.id')
                      ->orderBy('tm.round', 'ASC')
                      ->addOrderBy('m.match_date', 'ASC')
                      ->addOrderBy('m.match_time', 'ASC')
                      ->addOrderBy('m.id', 'ASC')
                      ->getQuery();
                
        try {
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }
    
    /**
    * "Ленивый"(без связаных объектов) поиск матчей по ID турнира
    * 
    * @param int $tournamentId ID турнира
    * @return \AppBundle\Entity\TournamentChampionshipMatch[]
    */
    public function findLazyByTournament($tournamentId)
    {
      $query = $this->createQueryBuilder('tm')
                    ->leftJoin('tm.tournament', 't')
                    ->where('t.id = :tournament_id')
                    ->setParameter('tournament_id', $tournamentId)
                    ->getQuery();
                
        try {
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }
    
    /**
    * Возвращает дату и время первого матча в турнире
    * 
    * @param $id ID турнира
    * @return string Дата/Время первого матча в турнире в формате Y-m-d H:i:s
    */
    public function getMinStartDatetime($id)
    {        
        $q = $this->createQueryBuilder('tcm')
                  ->select('DATE_FORMAT(MIN(CONCAT(m.match_date, \' \',  m.match_time)), \'%Y-%m-%d %H:%i:%s\')')
                  ->leftJoin('tcm.tournament', 't')
                  ->leftJoin('tcm.match', 'm')
                  ->where('t.id = :id')
                  ->setParameter('id', $id)
                  ->getQuery();
        
        try {            
            return $q->getSingleScalarResult();
          } catch (NoResultException $e) {
              return null;
          }
    }        
    
    /**
    * Возвращает количество матчей в турнире
    * 
    * @param int $tournament_id ID турнира
    * @return int
    */
    public function findMathesCountByTournament($tournament_id)
    {
        $q = $this->createQueryBuilder('tm')
                ->select("COUNT(tm.id)")
                ->where('tm.tournament = :tournament_id')
                ->setParameter('tournament_id', $tournament_id)
                ->getQuery();

        try {                    
            return $result = $q->getSingleScalarResult();
        } catch (NoResultException $e) {
            return 0;
        }
    }
    
    /**
    * Поиск уже начавшихся матчей в турнире
    * 
    * @param int $id ID турнира
    * @return \AppBundle\Entity\TournamentChampionshipMatch[]
    */
    public function findStartedByTournament($id)
    {
        $q = $this->createQueryBuilder('tcm')
                    ->leftJoin('tcm.match', 'm')
                    ->where('m.isStarted = 1')
                    ->andWhere('tcm.tournament = :id')                    
                    ->setParameter('id', $id)
                    ->getQuery();

        try {                    
            return $q->getResult();
        } catch (NoResultException $e) {                                        
            
            return null;
        }
    }
    
    /**
    * Поиск матчей турнира по ID турнира и туру
    * 
    * @param int $tournament_id
    * @param int $round
    * @todo Переделать
    * @return \AppBundle\Entity\TournamentChampionshipMatch[]
    */
    public function findByTournamentAndRound($tournament_id, $round)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT tm, m
                                    FROM AppBundle\Entity\TournamentChampionshipMatch tm
                                        LEFT JOIN tm.match m WITH m.id = tm.match
                                    WHERE tm.tournament = :tournament_id AND tm.round = :round")
                    ->setParameter("tournament_id", $tournament_id)
                    ->setParameter("round", $round);
                
        try {
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }
    
    /**
    * Проверят находится ли переданный матч в турнире или нет
    * 
    * @param int $tournament_id ID турнира
    * @param int $match_id ID матча
    * @return bool
    */
    public function isMatchInTournament($tournament_id, $match_id)
    {
        $q = $this->createQueryBuilder('tcm')
                    ->select('COUNT(tcm.id)')
                    ->leftJoin('tcm.match', 'm', 'WITH', 'm.id = tcm.match')                    
                    ->where('tcm.tournament = :tournament_id')
                    ->setParameter('tournament_id', $tournament_id)
                    ->andWhere('tcm.match = :match_id')
                    ->setParameter('match_id', $match_id)
                    ->getQuery();
        
        try {
            return (bool) $q->getSingleScalarResult();
        } catch (NoResultException $e) {
            return false;
        }
    }
    
    public function findByIds($tournament_id, $matches_ids)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT tm, m
                                    FROM AppBundle\Entity\TournamentChampionshipMatch tm
                                        LEFT JOIN tm.match m WITH m.id = tm.match
                                    WHERE tm.tournament = :tournament_id AND m.id IN (:matches_ids)")
                    ->setParameter("tournament_id", $tournament_id)
                    ->setParameter("matches_ids", $matches_ids);        
        try {            
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }
        
    /**
    * Находи матч турнира по id турнира и матча
    */
    public function findByTournamentAndMatch($tournament_id, $match_id)
    {
        $q = $this->createQueryBuilder('tcm')
                ->where('tcm.tournament = :tournament_id')
                ->andWhere('tcm.match = :match_id')
                ->setParameter('tournament_id', $tournament_id)
                ->setParameter('match_id', $match_id)
                ->getQuery();
                
            try {                    
                    return $q->getSingleResult();
                } catch (NoResultException $e) {                                        
                    
                    return null;
                }
    }

    public function findNonProcessedMatchesFinished($tournament_id, $round)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT m.id
                                    FROM AppBundle\Entity\TournamentChampionshipMatch tcm
                                    LEFT JOIN tcm.tournament t
                                    LEFT JOIN t.championships tc                                    
                                    LEFT JOIN tcm.match m
                                    WHERE t.id = :tournament_id 
                                        AND tcm.round = :round 
                                        AND m.isStarted = 1 
                                        AND (m.isOver = 1 OR m.isRejected = 1) 
                                        AND tcm.isProcessed = 0")
                    ->setParameter("tournament_id", $tournament_id)
                    ->setParameter("round", $round);
                                                            
        try {            
            $result = $query->getResult();
                        
            if(count($result) != 0)
                return $result;
            else
                return null;                        
        } catch (NoResultException $e) {
            return null;
        }
    }
    
    public function allBetsCalculated($tournament_id, $matches_ids)
    {        
        $em = $this->getEntityManager();        
        
        $query = $em->createQuery("SELECT COUNT(b.id) as total_bets
                                    FROM AppBundle\Entity\Bet b
                                    WHERE b.tournament = :tournament_id AND b.match IN (:matches_ids)
                                    ")
                    ->setParameter("tournament_id", $tournament_id)                    
                    ->setParameter("matches_ids", $matches_ids);
                    
        try {            
            $result = $query->getSingleResult();            
            $totalBets = $result["total_bets"];                        
        } catch (NoResultException $e) {
            return false;
        }
        
        if($totalBets == 0)
            return true;                
        
        $query = $em->createQuery("SELECT COUNT(b.id) as calculated_bets
                                    FROM AppBundle\Entity\Bet b
                                    WHERE b.tournament = :tournament_id AND b.points IS NOT NULL AND b.match IN (:matches_ids)
                                    ")
                    ->setParameter("tournament_id", $tournament_id)                    
                    ->setParameter("matches_ids", $matches_ids);
                    
        try {            
            $result = $query->getSingleResult();            
            $calculatedBets = $result["calculated_bets"];            
        } catch (NoResultException $e) {
            return false;
        }                
        
        if($totalBets == $calculatedBets)
            return true;
        else
            return false;
    }
    
    public function allRoundMatchesFinished($tournament_id, $round)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT COUNT(m.id) as matches_finished, tc.matches_in_round
                                    FROM AppBundle\Entity\TournamentChampionshipMatch tcm
                                    LEFT JOIN tcm.tournament t
                                    LEFT JOIN t.championships tc                                    
                                    LEFT JOIN tcm.match m
                                    WHERE t.id = :tournament_id 
                                        AND tcm.round = :round
                                        AND m.isStarted = 1 
                                        AND (m.isOver = 1 OR m.isRejected = 1)")
                    ->setParameter("tournament_id", $tournament_id)
                    ->setParameter("round", $round);
           
        try {
            $result = $query->getSingleResult();            
            return (intval($result["matches_finished"]) === intval($result["matches_in_round"]));
        } catch (NoResultException $e) {
            return false;
        }
    }
    
    public function matchesPointsCalculated($tournament_id, $round)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT COUNT(b.id) as bets_without_points
                                    FROM AppBundle\Entity\Bet b
                                    WHERE b.points IS NULL AND b.match IN (
                                        SELECT m.id
                                            FROM AppBundle\Entity\TournamentChampionshipMatch tcm
                                            LEFT JOIN tcm.match m
                                            WHERE tcm.tournament = :tournament_id AND tcm.round = :round
                                    )
                                    ")
                    ->setParameter("tournament_id", $tournament_id)
                    ->setParameter("round", $round);
           
        try {            
            $result = $query->getSingleResult();
            return (intval($result["bets_without_points"]) === 0);
        } catch (NoResultException $e) {
            return false;
        }
    }    
}
