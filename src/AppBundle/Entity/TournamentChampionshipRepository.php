<?php
namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

class TournamentChampionshipRepository extends EntityRepository
{
    /**
    * Поиск турнира со всеми связанными объектами
    * 
    * @param int $id ID турнира
    * @return \AppBundle\Entity\TournamentChampionship|null
    */
    public function find($id, $user = false)
    {
      $q = $this->createQueryBuilder('tc')
                ->addSelect('t, g, f, tm, m, u, ht, gt, l')                
                ->leftJoin('tc.tournament', 't')
                ->leftJoin('t.geography', 'g')
                ->leftJoin('t.format', 'f')
                ->leftJoin('t.users', 'u')
                ->leftJoin('t.championship_matches', 'tm')
                ->leftJoin('tm.match', 'm', 'WITH', 'm.id = tm.match')                
                ->leftJoin('m.league', 'l')
                ->leftJoin('m.home_team', 'ht')
                ->leftJoin('m.guest_team', 'gt')
                ->where('t.id = :id')
                ->setParameter('id', $id)                
                ->groupBy('tm.id')
                ->addGroupBy('u.id')
                ->orderBy('tm.round', 'ASC')
                ->addOrderBy('m.match_date', 'ASC')                
                ->addOrderBy('m.match_time', 'ASC')                
                ->addOrderBy('u.id', 'ASC');
                
      if(is_a($user, '\AppBundle\Entity\User'))
      {
            $q->addSelect('b')
              ->leftJoin('t.bets', 'b', 'WITH', 'b.match = m.id AND b.user = :user_id')
              ->setParameter('user_id', $user->getId())
              ->addGroupBy('b.id');
      }      
      
      $q = $q->getQuery();
      
      try {            
          if(count($q->getResult()) == 0)  
            return null;
            
          return $q->getSingleResult();
      } catch (NoResultException $e) {                                        
          
          return null;
      }
    }

    /**
    * Поиск турнира без связанных объектов
    * 
    * @param int $tournamentId ID турнира
    * @return \AppBundle\Entity\TournamentChampionship|null
    */
    public function findLazy($tournamentId)
    {
      $q = $this->createQueryBuilder('tc')
                ->addSelect('t, u')                
                ->leftJoin('tc.tournament', 't')                
                ->leftJoin('t.users', 'u')
                ->where('t.id = :id')
                ->setParameter('id', $tournamentId)                
                ->getQuery();
      
      try {                    
          return $q->getSingleResult();
      } catch (NoResultException $e) {
          return null;
      }
    }

    /**
    * Получает масси id'шников матчей турнира
    */
    public function getTournamentMatchesIds($id)
    {
        $q = $this->createQueryBuilder('tc')
                  ->select('m.id')
                  ->leftJoin('tc.tournament', 't')
                  ->leftJoin('t.championship_matches', 'tm', 'WITH', 't.id = tm.tournament')
                  ->leftJoin('tm.match', 'm', 'WITH', 'm.id = tm.match')
                  ->where('t.id = :id')
                  ->groupBy('m.id')
                  ->setParameter('id', $id)
                  ->getQuery();

        try {                    
          $matches = $q->getResult();
          $matchesIds = array();
          foreach($matches as $match)
            if($match["id"])
              $matchesIds[] = $match["id"];

          return $matchesIds;

      } catch (NoResultException $e) {                                        
          
          return array();
      }
    }

    /*
    * Проверяет на уникальность матч в турнире
    */
    public function matchIsUnique($tournament_id, $match_id)
    {
        $q = $this->createQueryBuilder('tc')
                ->select('COUNT(tcm.id) as cnt')
                ->leftJoin('tc.tournament', 't')
                ->leftJoin('t.championship_matches', 'tcm')
                ->where('t.id = :tournament_id')
                ->andWhere('tcm.match = :match_id')
                ->setParameter('tournament_id', $tournament_id)                
                ->setParameter('match_id', $match_id)                
                ->getQuery();

        try {                    
            $result = $q->getSingleResult();
            return !((bool) $result["cnt"]);
        } catch (NoResultException $e) {                                        
            
            return false;
        }        
    }
    
    /**
    * Проверяет есть ли данный матч в турнире
    * 
    * @param int $tournament_id
    * @param int $match_id
    * @return bool
    */
    public function isMatchInTournament($tournament_id, $match_id)
    {
        $q = $this->createQueryBuilder('tc')
                  ->select('COUNT(tcm.id) as cnt')
                  ->leftJoin('tc.tournament', 't')
                  ->leftJoin('t.championship_matches', 'tcm')
                  ->where('t.id = :tournament_id')
                  ->andWhere('tcm.match = :match_id')
                  ->setParameter('tournament_id', $tournament_id)                
                  ->setParameter('match_id', $match_id)                
                  ->getQuery();

        try {
            $result = $q->getSingleResult();
            return ((bool) $result["cnt"]);
        } catch (NoResultException $e) {                                        
            
            return false;
        }
    }
    
    public function findForMatchesInTournamentRound($tournament_id, $round)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT b, u, m
                                    FROM AppBundle\Entity\Bet b
                                    LEFT JOIN b.user u
                                    LEFT JOIN b.match m
                                    WHERE m.id IN (
                                        SELECT m2.id
                                            FROM AppBundle\Entity\TournamentChampionshipMatch tcm
                                            LEFT JOIN tcm.match m2
                                            WHERE tcm.tournament = :tournament_id AND tcm.round = :round
                                    )")
                    ->setParameter("tournament_id", $tournament_id)
                    ->setParameter("round", $round);
                
        try {
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }        
    
    public function findForNonProcessedMatchesInTournamentRound($tournament_id, $round)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT b, u, m
                                    FROM AppBundle\Entity\Bet b
                                    LEFT JOIN b.user u
                                    LEFT JOIN b.match m
                                    WHERE m.id IN (
                                        SELECT m2.id
                                            FROM AppBundle\Entity\TournamentChampionshipMatch tcm
                                            LEFT JOIN tcm.match m2
                                            WHERE tcm.tournament = :tournament_id AND tcm.round = :round AND tcm.isProcessed = 0
                                    )")
                    ->setParameter("tournament_id", $tournament_id)
                    ->setParameter("round", $round);
                
        try {
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }    
}
