<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AppBundle\Entity\TournamentChampionshipResult
 *
 * @ORM\Table(name="tournaments_championship_results")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\TournamentChampionshipResultRepository")
 */
class TournamentChampionshipResult
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")     
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Tournament", inversedBy="championship_results")
     * @ORM\JoinColumn(name="tournament_id", referencedColumnName="id")
     */
    protected $tournament;   
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="tournaments_championship_results")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
    
    /**
     * @ORM\Column(type="integer")     
     */
    private $round;
    
    /**
     * @ORM\Column(type="integer")     
     */
    private $place;
    
    /**
     * @ORM\Column(type="float")     
     */
    private $total_points;
    
    /**
     * @ORM\Column(type="float")     
     */
    private $points_in_round;
    
    /**
     * @ORM\Column(type="integer")     
     */
    private $total_guessed_results;
    
    /**
     * @ORM\Column(type="integer")     
     */
    private $total_guessed_outcomes;
    
    /**
     * @ORM\Column(type="integer")     
     */
    private $total_guessed_differences;
    
    /**
     * @ORM\Column(type="float")
     */
    private $user_rating;

    public function __construct()
    {
        $this->place = 0;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set round
     *
     * @param integer $round
     * @return TournamentChampionshipResult
     */
    public function setRound($round)
    {
        $this->round = $round;
    
        return $this;
    }

    /**
     * Get round
     *
     * @return integer 
     */
    public function getRound()
    {
        return $this->round;
    }

    /**
     * Set total_points
     *
     * @param float $totalPoints
     * @return TournamentChampionshipResult
     */
    public function setTotalPoints($totalPoints)
    {
        $this->total_points = $totalPoints;
    
        return $this;
    }

    /**
     * Get total_points
     *
     * @return float 
     */
    public function getTotalPoints()
    {
        return $this->total_points;
    }

    /**
     * Set points_in_round
     *
     * @param float $pointsInRound
     * @return TournamentChampionshipResult
     */
    public function setPointsInRound($pointsInRound)
    {
        $this->points_in_round = $pointsInRound;
    
        return $this;
    }

    /**
     * Get points_in_round
     *
     * @return float 
     */
    public function getPointsInRound()
    {
        return $this->points_in_round;
    }

    /**
     * Set total_guessed_results
     *
     * @param integer $totalGuessedResults
     * @return TournamentChampionshipResult
     */
    public function setTotalGuessedResults($totalGuessedResults)
    {
        $this->total_guessed_results = $totalGuessedResults;
    
        return $this;
    }

    /**
     * Get total_guessed_results
     *
     * @return integer 
     */
    public function getTotalGuessedResults()
    {
        return $this->total_guessed_results;
    }

    /**
     * Set total_guessed_outcomes
     *
     * @param integer $totalGuessedOutcomes
     * @return TournamentChampionshipResult
     */
    public function setTotalGuessedOutcomes($totalGuessedOutcomes)
    {
        $this->total_guessed_outcomes = $totalGuessedOutcomes;
    
        return $this;
    }

    /**
     * Get total_guessed_outcomes
     *
     * @return integer 
     */
    public function getTotalGuessedOutcomes()
    {
        return $this->total_guessed_outcomes;
    }

    /**
     * Set total_guessed_differences
     *
     * @param integer $totalGuessedDifferences
     * @return TournamentChampionshipResult
     */
    public function setTotalGuessedDifferences($totalGuessedDifferences)
    {
        $this->total_guessed_differences = $totalGuessedDifferences;
    
        return $this;
    }

    /**
     * Get total_guessed_differences
     *
     * @return integer 
     */
    public function getTotalGuessedDifferences()
    {
        return $this->total_guessed_differences;
    }

    /**
     * Set tournament
     *
     * @param \AppBundle\Entity\Tournament $tournament
     * @return TournamentChampionshipResult
     */
    public function setTournament(\AppBundle\Entity\Tournament $tournament = null)
    {
        $this->tournament = $tournament;
    
        return $this;
    }

    /**
     * Get tournament
     *
     * @return \AppBundle\Entity\Tournament 
     */
    public function getTournament()
    {
        return $this->tournament;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return TournamentChampionshipResult
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user_rating
     *
     * @param float $userRating
     * @return TournamentChampionshipResult
     */
    public function setUserRating($userRating)
    {
        $this->user_rating = $userRating;

        return $this;
    }

    /**
     * Get user_rating
     *
     * @return float 
     */
    public function getUserRating()
    {
        return $this->user_rating;
    }

    /**
     * Set place
     *
     * @param integer $place
     * @return TournamentChampionshipResult
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return integer 
     */
    public function getPlace()
    {
        return $this->place;
    }
}
