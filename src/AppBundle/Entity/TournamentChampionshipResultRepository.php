<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

class TournamentChampionshipResultRepository extends EntityRepository
{
    /**
    * Ищет результаты по турниру и раунду
    * 
    * @param int $tournament_id
    * @param int $round
    * @return array
    */
    public function findByTournamentAndRound($tournament_id, $round)
    {
      $q = $this->createQueryBuilder('tcr')
                  ->addSelect('t, u')
                  ->leftJoin('tcr.tournament', 't')
                  ->leftJoin('tcr.user', 'u')
                  ->where('tcr.tournament = :tournament_id')
                  ->setParameter('tournament_id', $tournament_id)
                  ->andWhere('tcr.round = :round')
                  ->setParameter('round', $round)                
                  ->orderBy('tcr.place', 'ASC')
                  ->getQuery();
                          
        try {            
            return $q->getResult();
          } catch (NoResultException $e) {                                                      
              return null;
          }
    }

    /**
    * Получает последний раунд по которому сгенерированы результаты
    */
    public function getMaxResultRoundByTournament($tournament_id)
    {
        $q = $this->createQueryBuilder('tcr')                                        
                    ->select('MAX(tcr.round) as max_round')
                    ->where('tcr.tournament = :tournament_id')
                    ->setParameter('tournament_id', $tournament_id)                    
                    ->getQuery();

        try {            
            $result = $q->getSingleResult();
            return $result["max_round"];
          } catch (NoResultException $e) {                                                      
              return null;
          }
    }
}
