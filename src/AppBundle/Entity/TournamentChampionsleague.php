<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AppBundle\Entity\TournamentChampionsleague
 *
 * @ORM\Table(name="tournaments_championsleague")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\TournamentChampionsleagueRepository")
 */
class TournamentChampionsleague
{
    const STAGE_QUALIFICATION = 'QUALIFICATION';
    const STAGE_GROUP = 'GROUP';
    const STAGE_PLAYOFF = 'PLAYOFF';

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")     
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Tournament", inversedBy="championsleagues")
     * @ORM\JoinColumn(name="tournament_id", referencedColumnName="id")
     */
    protected $tournament;

    /**
     * @ORM\Column(name="qualification_rounds_count", type="integer")     
     */
    private $qualificationRoundsCount;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min = 1)
     */
    private $matches_in_qualification;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min = 1)
     */
    private $matches_in_game;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min = 1)
     */
    private $matches_in_playoff;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min = 4)
     */
    private $max_users_count;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min = 1)
     */
    private $max_groups_count;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min = 1)
     */
    private $users_out_of_group;

    /**
     * @ORM\Column(type="string")
     * @Assert\Choice({"QUALIFICATION", "GROUP", "PLAYOFF"})
     */
    private $stage;    

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min = 1)
     */
    private $real_users_out_of_qualification;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min = 4)
     */
    private $real_users_count;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min = 1)
     */
    private $real_groups_count;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min = 1)
     */
    private $real_users_in_group;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min = 1)
     */
    private $real_users_out_of_group;

    /**
     * @ORM\Column(type="integer")     
     */
    private $real_group_rounds_count;

    /**
     * @ORM\Column(type="integer")     
     */
    private $real_playoff_rounds_count;

    /**
     * @ORM\Column(type="integer")     
     */
    private $current_round;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->current_round = 1;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set qualificationRoundsCount
     *
     * @param integer $qualificationRoundsCount
     * @return TournamentChampionsleague
     */
    public function setQualificationRoundsCount($qualificationRoundsCount)
    {
        $this->qualificationRoundsCount = $qualificationRoundsCount;

        return $this;
    }

    /**
     * Get qualificationRoundsCount
     *
     * @return integer 
     */
    public function getQualificationRoundsCount()
    {
        return $this->qualificationRoundsCount;
    }

    /**
     * Set matches_in_qualification
     *
     * @param integer $matchesInQualification
     * @return TournamentChampionsleague
     */
    public function setMatchesInQualification($matchesInQualification)
    {
        $this->matches_in_qualification = $matchesInQualification;

        return $this;
    }

    /**
     * Get matches_in_qualification
     *
     * @return integer 
     */
    public function getMatchesInQualification()
    {
        return $this->matches_in_qualification;
    }

    /**
     * Set matches_in_game
     *
     * @param integer $matchesInGame
     * @return TournamentChampionsleague
     */
    public function setMatchesInGame($matchesInGame)
    {
        $this->matches_in_game = $matchesInGame;

        return $this;
    }

    /**
     * Get matches_in_game
     *
     * @return integer 
     */
    public function getMatchesInGame()
    {
        return $this->matches_in_game;
    }

    /**
     * Set matches_in_playoff
     *
     * @param integer $matchesInPlayoff
     * @return TournamentChampionsleague
     */
    public function setMatchesInPlayoff($matchesInPlayoff)
    {
        $this->matches_in_playoff = $matchesInPlayoff;

        return $this;
    }

    /**
     * Get matches_in_playoff
     *
     * @return integer 
     */
    public function getMatchesInPlayoff()
    {
        return $this->matches_in_playoff;
    }

    /**
     * Set max_users_count
     *
     * @param integer $maxUsersCount
     * @return TournamentChampionsleague
     */
    public function setMaxUsersCount($maxUsersCount)
    {
        $this->max_users_count = $maxUsersCount;

        return $this;
    }

    /**
     * Get max_users_count
     *
     * @return integer 
     */
    public function getMaxUsersCount()
    {
        return $this->max_users_count;
    }

    /**
     * Set stage
     *
     * @param string $stage
     * @return TournamentChampionsleague
     */
    public function setStage($stage)
    {
        $this->stage = $stage;

        return $this;
    }

    /**
     * Get stage
     *
     * @return string 
     */
    public function getStage()
    {
        return $this->stage;
    }

    /**
     * Set tournament
     *
     * @param \AppBundle\Entity\Tournament $tournament
     * @return TournamentChampionsleague
     */
    public function setTournament(\AppBundle\Entity\Tournament $tournament = null)
    {
        $this->tournament = $tournament;

        return $this;
    }

    /**
     * Get tournament
     *
     * @return \AppBundle\Entity\Tournament 
     */
    public function getTournament()
    {
        return $this->tournament;
    }

    /**
     * Set max_groups_count
     *
     * @param integer $maxGroupsCount
     * @return TournamentChampionsleague
     */
    public function setMaxGroupsCount($maxGroupsCount)
    {
        $this->max_groups_count = $maxGroupsCount;

        return $this;
    }

    /**
     * Get max_groups_count
     *
     * @return integer 
     */
    public function getMaxGroupsCount()
    {
        return $this->max_groups_count;
    }

    /**
     * Set users_out_of_group
     *
     * @param integer $usersOutOfGroup
     * @return TournamentChampionsleague
     */
    public function setUsersOutOfGroup($usersOutOfGroup)
    {
        $this->users_out_of_group = $usersOutOfGroup;

        return $this;
    }

    /**
     * Get users_out_of_group
     *
     * @return integer 
     */
    public function getUsersOutOfGroup()
    {
        return $this->users_out_of_group;
    }

    /**
     * Set real_users_out_of_qualification
     *
     * @param integer $realUsersOutOfQualification
     * @return TournamentChampionsleague
     */
    public function setRealUsersOutOfQualification($realUsersOutOfQualification)
    {
        $this->real_users_out_of_qualification = $realUsersOutOfQualification;

        return $this;
    }

    /**
     * Get real_users_out_of_qualification
     *
     * @return integer 
     */
    public function getRealUsersOutOfQualification()
    {
        return $this->real_users_out_of_qualification;
    }

    /**
     * Set real_users_count
     *
     * @param integer $realUsersCount
     * @return TournamentChampionsleague
     */
    public function setRealUsersCount($realUsersCount)
    {
        $this->real_users_count = $realUsersCount;

        return $this;
    }

    /**
     * Get real_users_count
     *
     * @return integer 
     */
    public function getRealUsersCount()
    {
        return $this->real_users_count;
    }

    /**
     * Set real_groups_count
     *
     * @param integer $realGroupsCount
     * @return TournamentChampionsleague
     */
    public function setRealGroupsCount($realGroupsCount)
    {
        $this->real_groups_count = $realGroupsCount;

        return $this;
    }

    /**
     * Get real_groups_count
     *
     * @return integer 
     */
    public function getRealGroupsCount()
    {
        return $this->real_groups_count;
    }

    /**
     * Set real_users_in_group
     *
     * @param integer $realUsersInGroup
     * @return TournamentChampionsleague
     */
    public function setRealUsersInGroup($realUsersInGroup)
    {
        $this->real_users_in_group = $realUsersInGroup;

        return $this;
    }

    /**
     * Get real_users_in_group
     *
     * @return integer 
     */
    public function getRealUsersInGroup()
    {
        return $this->real_users_in_group;
    }

    /**
     * Set real_users_out_of_group
     *
     * @param integer $realUsersOutOfGroup
     * @return TournamentChampionsleague
     */
    public function setRealUsersOutOfGroup($realUsersOutOfGroup)
    {
        $this->real_users_out_of_group = $realUsersOutOfGroup;

        return $this;
    }

    /**
     * Get real_users_out_of_group
     *
     * @return integer 
     */
    public function getRealUsersOutOfGroup()
    {
        return $this->real_users_out_of_group;
    }

    /**
     * Set real_group_rounds_count
     *
     * @param integer $realGroupRoundsCount
     * @return TournamentChampionsleague
     */
    public function setRealGroupRoundsCount($realGroupRoundsCount)
    {
        $this->real_group_rounds_count = $realGroupRoundsCount;

        return $this;
    }

    /**
     * Get real_group_rounds_count
     *
     * @return integer 
     */
    public function getRealGroupRoundsCount()
    {
        return $this->real_group_rounds_count;
    }

    /**
     * Set real_playoff_rounds_count
     *
     * @param integer $realPlayoffRoundsCount
     * @return TournamentChampionsleague
     */
    public function setRealPlayoffRoundsCount($realPlayoffRoundsCount)
    {
        $this->real_playoff_rounds_count = $realPlayoffRoundsCount;

        return $this;
    }

    /**
     * Get real_playoff_rounds_count
     *
     * @return integer 
     */
    public function getRealPlayoffRoundsCount()
    {
        return $this->real_playoff_rounds_count;
    }

    /**
     * Set current_round
     *
     * @param integer $currentRound
     * @return TournamentChampionsleague
     */
    public function setCurrentRound($currentRound)
    {
        $this->current_round = $currentRound;

        return $this;
    }

    /**
     * Get current_round
     *
     * @return integer 
     */
    public function getCurrentRound()
    {
        return $this->current_round;
    }
}
