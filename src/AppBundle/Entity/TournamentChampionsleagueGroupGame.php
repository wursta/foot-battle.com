<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AppBundle\Entity\TournamentChampionsleagueGroupGame
 *
 * @ORM\Table(name="tournaments_championsleague_group_games")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\TournamentChampionsleagueGroupGameRepository")
 */
class TournamentChampionsleagueGroupGame
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")     
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Tournament", inversedBy="championsleague_group_games")
     * @ORM\JoinColumn(name="tournament_id", referencedColumnName="id")
     */
    protected $tournament;

    /**
     * @ORM\Column(type="integer")     
     */
    private $round;

    /**
     * @ORM\Column(name="group_num", type="integer")     
     */
    private $groupNum;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="tournaments_championsleague_group_games_at_left")
     * @ORM\JoinColumn(name="user_left", referencedColumnName="id")
     */
    protected $user_left;   
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="tournaments_championsleague_group_games_at_right")
     * @ORM\JoinColumn(name="user_right", referencedColumnName="id")
     */
    protected $user_right;   
    
    /**
     * @ORM\Column(type="float")     
     * @Assert\Range(min = 0)
     */
    protected $score_left;

    /**
     * @ORM\Column(type="float")     
     * @Assert\Range(min = 0)
     */
    protected $score_right;
    
    /**
     * @ORM\Column(type="float")     
     * @Assert\Range(min = 0)
     */
    protected $points_left;

    /**
     * @ORM\Column(type="float")     
     * @Assert\Range(min = 0)
     */
    protected $points_right;    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set round
     *
     * @param integer $round
     * @return TournamentChampionsleagueGroupGame
     */
    public function setRound($round)
    {
        $this->round = $round;

        return $this;
    }

    /**
     * Get round
     *
     * @return integer 
     */
    public function getRound()
    {
        return $this->round;
    }

    /**
     * Set score_left
     *
     * @param float $scoreLeft
     * @return TournamentChampionsleagueGroupGame
     */
    public function setScoreLeft($scoreLeft)
    {
        $this->score_left = $scoreLeft;

        return $this;
    }

    /**
     * Get score_left
     *
     * @return float 
     */
    public function getScoreLeft()
    {
        return $this->score_left;
    }

    /**
     * Set score_right
     *
     * @param float $scoreRight
     * @return TournamentChampionsleagueGroupGame
     */
    public function setScoreRight($scoreRight)
    {
        $this->score_right = $scoreRight;

        return $this;
    }

    /**
     * Get score_right
     *
     * @return float 
     */
    public function getScoreRight()
    {
        return $this->score_right;
    }

    /**
     * Set points_left
     *
     * @param float $pointsLeft
     * @return TournamentChampionsleagueGroupGame
     */
    public function setPointsLeft($pointsLeft)
    {
        $this->points_left = $pointsLeft;

        return $this;
    }

    /**
     * Get points_left
     *
     * @return float 
     */
    public function getPointsLeft()
    {
        return $this->points_left;
    }

    /**
     * Set points_right
     *
     * @param float $pointsRight
     * @return TournamentChampionsleagueGroupGame
     */
    public function setPointsRight($pointsRight)
    {
        $this->points_right = $pointsRight;

        return $this;
    }

    /**
     * Get points_right
     *
     * @return float 
     */
    public function getPointsRight()
    {
        return $this->points_right;
    }

    /**
     * Set tournament
     *
     * @param \AppBundle\Entity\Tournament $tournament
     * @return TournamentChampionsleagueGroupGame
     */
    public function setTournament(\AppBundle\Entity\Tournament $tournament = null)
    {
        $this->tournament = $tournament;

        return $this;
    }

    /**
     * Get tournament
     *
     * @return \AppBundle\Entity\Tournament 
     */
    public function getTournament()
    {
        return $this->tournament;
    }

    /**
     * Set user_left
     *
     * @param \AppBundle\Entity\User $userLeft
     * @return TournamentChampionsleagueGroupGame
     */
    public function setUserLeft(\AppBundle\Entity\User $userLeft = null)
    {
        $this->user_left = $userLeft;

        return $this;
    }

    /**
     * Get user_left
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUserLeft()
    {
        return $this->user_left;
    }

    /**
     * Set user_right
     *
     * @param \AppBundle\Entity\User $userRight
     * @return TournamentChampionsleagueGroupGame
     */
    public function setUserRight(\AppBundle\Entity\User $userRight = null)
    {
        $this->user_right = $userRight;

        return $this;
    }

    /**
     * Get user_right
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUserRight()
    {
        return $this->user_right;
    }

    /**
     * Set groupNum
     *
     * @param integer $groupNum
     * @return TournamentChampionsleagueGroupGame
     */
    public function setGroupNum($groupNum)
    {
        $this->groupNum = $groupNum;

        return $this;
    }

    /**
     * Get groupNum
     *
     * @return integer 
     */
    public function getGroupNum()
    {
        return $this->groupNum;
    }
}
