<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * TournamentChampionsleagueGroupGameRepository
 *
 */
class TournamentChampionsleagueGroupGameRepository extends EntityRepository
{
    public function findByTournamentAndRound($tournament_id, $round)
    {
        $q = $this->createQueryBuilder('tg')
                    ->leftJoin('tg.tournament', 't')
                    ->where('t.id = :tournament_id')
                    ->setParameter('tournament_id', $tournament_id)
                    ->andWhere('tg.round = :round')
                    ->setParameter('round', $round)                    
                    ->getQuery();

        try {                    
            return $q->getResult();
        } catch (NoResultException $e) {                                        
            
            return null;
        }            
    }
    
    public function findByTournamentAndGroupAndRound($tournament_id, $round, $group)
    {
        $q = $this->createQueryBuilder('tg')
                    ->leftJoin('tg.tournament', 't')
                    ->where('t.id = :tournament_id')
                    ->setParameter('tournament_id', $tournament_id)
                    ->andWhere('tg.round = :round')
                    ->setParameter('round', $round)
                    ->andWhere('tg.groupNum = :group')
                    ->setParameter('group', $group)                    
                    ->getQuery();

        try {                    
            return $q->getResult();
        } catch (NoResultException $e) {                                        
            
            return null;
        }            
    }
    
    public function findByTournamentAndGroup($tournament_id, $group)
    {
        $q = $this->createQueryBuilder('tg')
                    ->leftJoin('tg.tournament', 't')
                    ->where('t.id = :tournament_id')
                    ->setParameter('tournament_id', $tournament_id)                    
                    ->andWhere('tg.groupNum = :group')
                    ->setParameter('group', $group)                    
                    ->orderBy('tg.round', 'ASC')
                    ->getQuery();

        try {                    
            return $q->getResult();
        } catch (NoResultException $e) {                                        
            
            return null;
        }            
    }
    
    public function findUserGroup($tournament_id, $user_id)
    {
        $q = $this->createQueryBuilder('tg')                    
                    ->select('tg.groupNum')
                    ->leftJoin('tg.tournament', 't')
                    ->where('t.id = :tournament_id')
                    ->setParameter('tournament_id', $tournament_id)
                    ->andWhere('tg.user_left = :user_id OR tg.user_right = :user_id')
                    ->setParameter('user_id', $user_id)
                    ->distinct(true)
                    ->getQuery();
                    
        try {                    
            $result = $q->getResult();            
            if(!empty($result[0]["groupNum"]))
             return $result[0]["groupNum"];
        } catch (NoResultException $e) {                                        
            
            return 1;
        } 
    }
}
