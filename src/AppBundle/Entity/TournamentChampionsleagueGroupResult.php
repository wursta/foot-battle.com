<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AppBundle\Entity\TournamentChampionsleagueGroupResult
 *
 * @ORM\Table(name="tournaments_championsleague_group_results")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\TournamentChampionsleagueGroupResultRepository")
 */
class TournamentChampionsleagueGroupResult
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")     
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Tournament", inversedBy="championsleague_group_results")
     * @ORM\JoinColumn(name="tournament_id", referencedColumnName="id")
     */
    protected $tournament;   
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="tournaments_championsleague_group_results")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
    
    /**
     * @ORM\Column(type="integer")     
     */
    private $group_num;

    /**
     * @ORM\Column(type="integer")     
     */
    private $round;
    
    /**
     * @ORM\Column(type="integer")     
     */
    private $place;

    /**
     * @ORM\Column(type="integer")     
     */
    private $total_games;

    /**
     * @ORM\Column(type="integer")     
     */
    private $total_wins;

    /**
     * @ORM\Column(type="integer")     
     */
    private $total_draws;

    /**
     * @ORM\Column(type="integer")     
     */
    private $total_loss;

    /**
     * @ORM\Column(type="integer")     
     */
    private $total_goals_against;
    
    /**
     * @ORM\Column(type="integer")     
     */
    private $total_goals_scored;

    /**
     * @ORM\Column(type="integer")     
     */
    private $total_goals_difference;

    /**
     * @ORM\Column(type="float")     
     */
    private $total_points;

    /**
     * @ORM\Column(type="float")     
     */
    private $user_rating;

    public function __construct()
    {
        $this->total_games = 0;
        $this->total_points = 0;
        
        $this->total_draws = 0;                
        $this->total_loss = 0;        
        $this->total_wins = 0;
        
        $this->total_goals_against = 0;
        $this->total_goals_difference = 0;
        $this->total_goals_scored = 0;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set group_num
     *
     * @param integer $groupNum
     * @return TournamentChampionsleagueGroupResult
     */
    public function setGroupNum($groupNum)
    {
        $this->group_num = $groupNum;

        return $this;
    }

    /**
     * Get group_num
     *
     * @return integer 
     */
    public function getGroupNum()
    {
        return $this->group_num;
    }

    /**
     * Set round
     *
     * @param integer $round
     * @return TournamentChampionsleagueGroupResult
     */
    public function setRound($round)
    {
        $this->round = $round;

        return $this;
    }

    /**
     * Get round
     *
     * @return integer 
     */
    public function getRound()
    {
        return $this->round;
    }

    /**
     * Set total_games
     *
     * @param integer $totalGames
     * @return TournamentChampionsleagueGroupResult
     */
    public function setTotalGames($totalGames)
    {
        $this->total_games = $totalGames;

        return $this;
    }

    /**
     * Get total_games
     *
     * @return integer 
     */
    public function getTotalGames()
    {
        return $this->total_games;
    }

    /**
     * Set total_wins
     *
     * @param integer $totalWins
     * @return TournamentChampionsleagueGroupResult
     */
    public function setTotalWins($totalWins)
    {
        $this->total_wins = $totalWins;

        return $this;
    }

    /**
     * Get total_wins
     *
     * @return integer 
     */
    public function getTotalWins()
    {
        return $this->total_wins;
    }

    /**
     * Set total_draws
     *
     * @param integer $totalDraws
     * @return TournamentChampionsleagueGroupResult
     */
    public function setTotalDraws($totalDraws)
    {
        $this->total_draws = $totalDraws;

        return $this;
    }

    /**
     * Get total_draws
     *
     * @return integer 
     */
    public function getTotalDraws()
    {
        return $this->total_draws;
    }

    /**
     * Set total_loss
     *
     * @param integer $totalLoss
     * @return TournamentChampionsleagueGroupResult
     */
    public function setTotalLoss($totalLoss)
    {
        $this->total_loss = $totalLoss;

        return $this;
    }

    /**
     * Get total_loss
     *
     * @return integer 
     */
    public function getTotalLoss()
    {
        return $this->total_loss;
    }

    /**
     * Set total_goals_against
     *
     * @param integer $totalGoalsAgainst
     * @return TournamentChampionsleagueGroupResult
     */
    public function setTotalGoalsAgainst($totalGoalsAgainst)
    {
        $this->total_goals_against = $totalGoalsAgainst;

        return $this;
    }

    /**
     * Get total_goals_against
     *
     * @return integer 
     */
    public function getTotalGoalsAgainst()
    {
        return $this->total_goals_against;
    }

    /**
     * Set total_goals_scored
     *
     * @param integer $totalGoalsScored
     * @return TournamentChampionsleagueGroupResult
     */
    public function setTotalGoalsScored($totalGoalsScored)
    {
        $this->total_goals_scored = $totalGoalsScored;

        return $this;
    }

    /**
     * Get total_goals_scored
     *
     * @return integer 
     */
    public function getTotalGoalsScored()
    {
        return $this->total_goals_scored;
    }

    /**
     * Set total_goals_difference
     *
     * @param integer $totalGoalsDifference
     * @return TournamentChampionsleagueGroupResult
     */
    public function setTotalGoalsDifference($totalGoalsDifference)
    {
        $this->total_goals_difference = $totalGoalsDifference;

        return $this;
    }

    /**
     * Get total_goals_difference
     *
     * @return integer 
     */
    public function getTotalGoalsDifference()
    {
        return $this->total_goals_difference;
    }

    /**
     * Set total_points
     *
     * @param float $totalPoints
     * @return TournamentChampionsleagueGroupResult
     */
    public function setTotalPoints($totalPoints)
    {
        $this->total_points = $totalPoints;

        return $this;
    }

    /**
     * Get total_points
     *
     * @return float 
     */
    public function getTotalPoints()
    {
        return $this->total_points;
    }

    /**
     * Set tournament
     *
     * @param \AppBundle\Entity\Tournament $tournament
     * @return TournamentChampionsleagueGroupResult
     */
    public function setTournament(\AppBundle\Entity\Tournament $tournament = null)
    {
        $this->tournament = $tournament;

        return $this;
    }

    /**
     * Get tournament
     *
     * @return \AppBundle\Entity\Tournament 
     */
    public function getTournament()
    {
        return $this->tournament;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return TournamentChampionsleagueGroupResult
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user_rating
     *
     * @param float $userRating
     * @return TournamentChampionsleagueGroupResult
     */
    public function setUserRating($userRating)
    {
        $this->user_rating = $userRating;

        return $this;
    }

    /**
     * Get user_rating
     *
     * @return float 
     */
    public function getUserRating()
    {
        return $this->user_rating;
    }

    /**
     * Set place
     *
     * @param integer $place
     * @return TournamentChampionsleagueGroupResult
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return integer 
     */
    public function getPlace()
    {
        return $this->place;
    }
}
