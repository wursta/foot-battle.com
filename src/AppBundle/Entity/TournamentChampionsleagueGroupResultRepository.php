<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * TournamentChampionsleagueGroupResultRepository
 *
 */
class TournamentChampionsleagueGroupResultRepository extends EntityRepository
{
    public function findByTournamentAndRound($tournament_id, $round)
    {
        $q = $this->createQueryBuilder('tgr')                    
                    ->leftJoin('tgr.tournament', 't')
                    ->leftJoin('t.users', 'u')
                    ->where('t.id = :tournament_id')
                    ->setParameter('tournament_id', $tournament_id)
                    ->andWhere('tgr.round = :round')
                    ->setParameter('round', $round)                    
                    ->orderBy('tgr.place', 'ASC')                    
                    ->getQuery();

        try {                    
            return $q->getResult();
        } catch (NoResultException $e) {                                        
            
            return null;
        }            
    }
    
    public function findByTournamentAndGroupAndRound($tournament_id, $round, $group)
    {
        $q = $this->createQueryBuilder('tgr')                    
                    ->addSelect("t, u")
                    ->leftJoin('tgr.tournament', 't')
                    ->leftJoin('t.users', 'u')
                    ->where('t.id = :tournament_id')
                    ->setParameter('tournament_id', $tournament_id)
                    ->andWhere('tgr.round = :round')
                    ->setParameter('round', $round)                    
                    ->andWhere("tgr.group_num = :group")
                    ->setParameter('group', $group)
                    ->orderBy('tgr.place', 'ASC')                    
                    ->getQuery();

        try {                    
            return $q->getResult();
        } catch (NoResultException $e) {
            return null;
        }            
    }
    
    public function findSortedByTournamentAndGroupAndRound($tournament_id, $round, $group)
    {
        $q = $this->createQueryBuilder('tgr')                    
                    ->leftJoin('tgr.tournament', 't')
                    ->leftJoin('t.users', 'u')
                    ->where('t.id = :tournament_id')
                    ->setParameter('tournament_id', $tournament_id)
                    ->andWhere('tgr.round = :round')
                    ->setParameter('round', $round)                    
                    ->andWhere("tgr.group_num = :group")
                    ->setParameter('group', $group)
                    ->orderBy('tgr.total_points', 'DESC')
                    ->addOrderBy('tgr.total_goals_scored', 'DESC')
                    ->addOrderBy('tgr.total_wins', 'DESC')
                    ->addOrderBy('tgr.total_goals_difference', 'DESC')
                    ->addOrderBy('tgr.user_rating', 'DESC')
                    ->getQuery();

        try {                    
            return $q->getResult();
        } catch (NoResultException $e) {                                        
            
            return null;
        }            
    }
    
    public function findByTournamentAndRoundForPlayoff($tournament_id, $round, $maxPlace)
    {
        $q = $this->createQueryBuilder('tgr')                    
                    ->addSelect("u")                    
                    ->leftJoin('tgr.user', 'u')
                    ->where('tgr.tournament = :tournament_id')
                    ->setParameter('tournament_id', $tournament_id)
                    ->andWhere('tgr.round = :round')
                    ->setParameter('round', $round)
                    ->andWhere("tgr.place <= :max_place")
                    ->setParameter("max_place", $maxPlace)                    
                    ->getQuery();

        try {                    
            return $q->getResult();
        } catch (NoResultException $e) {                                        
            
            return null;
        }            
    }
    
    public function removeUsersBetsThatNotInPlayoff($tournament_id, $lastRound, $maxPlace)
    {
        $em = $this->getEntityManager();
        
        $query = $em->createQuery("DELETE FROM \AppBundle\Entity\Bet b WHERE b.tournament = :tournament_id AND b.user IN (
            SELECT DISTINCT u.id FROM \AppBundle\Entity\TournamentChampionsleagueGroupResult tgr                
                LEFT JOIN tgr.user u                
                WHERE tgr.tournament = :tournament_id AND tgr.round = :last_round AND tgr.place > :maxPlace
        ) AND b.match IN (
            SELECT DISTINCT m.id FROM \AppBundle\Entity\TournamentChampionsleagueMatch tcm
                LEFT JOIN tcm.match m
                WHERE tcm.tournament = :tournament_id AND tcm.isQualificationMatch = 0 AND tcm.isGroupMatch = 0
        )")
        ->setParameter("tournament_id", $tournament_id)        
        ->setParameter("last_round", $lastRound)        
        ->setParameter("maxPlace", $maxPlace);        
        
        $query->execute();
        $em->flush();
    }
    
    public function isUserInGroupStage($tournament_id, $user_id)
    {
        $q = $this->createQueryBuilder('tgr')                    
                    ->select("COUNT(tgr.id) AS CNT")
                    ->leftJoin('tgr.tournament', 't')                    
                    ->where('t.id = :tournament_id')
                    ->setParameter('tournament_id', $tournament_id)
                    ->andWhere('tgr.user = :user_id')
                    ->setParameter('user_id', $user_id)                                        
                    ->getQuery();

        try {                    
            $result = $q->getSingleResult();
            return (bool) $result["CNT"];
        } catch (NoResultException $e) {                                        
            
            return null;
        }            
    }    
}
