<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

/**
 * TournamentChampionsleagueMatchRepository
 *
 */
class TournamentChampionsleagueMatchRepository extends EntityRepository
{
    /**
    * Возвращает матчи квалификации по ID турнира
    * 
    * @param int $tournamentId
    * @return \AppBundle\Entity\TournamentChampionsleagueMatch[]
    */
    public function findQualificationByTournament($tournamentId)
    {
        $query = $this->createQueryBuilder('tm')
                      ->select('tm, m, l, ht, gt')
                      ->leftJoin('tm.tournament', 't')
                      ->leftJoin('tm.match', 'm', Join::WITH, 'm.id = tm.match')
                      ->leftJoin('m.league', 'l')
                      ->leftJoin('m.home_team', 'ht')
                      ->leftJoin('m.guest_team', 'gt')
                      ->where('t.id = :tournament_id')
                      ->setParameter('tournament_id', $tournamentId)
                      ->andWhere('tm.isQualificationMatch = :isQualificationMatch')
                      ->setParameter('isQualificationMatch', true)
                      ->groupBy('tm.id')
                      ->orderBy('tm.round', 'ASC')
                      ->addOrderBy('m.match_date', 'ASC')
                      ->addOrderBy('m.match_time', 'ASC')
                      ->addOrderBy('m.id', 'ASC')
                      ->getQuery();  
      
        try {
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }
    
    /**
    * Возвращает матчи группового этапа по ID турнира
    * 
    * @param int $tournamentId
    * @return \AppBundle\Entity\TournamentChampionsleagueMatch[]
    */
    public function findGroupByTournament($tournamentId)
    {
      $query = $this->createQueryBuilder('tm')
                      ->select('tm, m, l, ht, gt')
                      ->leftJoin('tm.tournament', 't')
                      ->leftJoin('tm.match', 'm', Join::WITH, 'm.id = tm.match')
                      ->leftJoin('m.league', 'l')
                      ->leftJoin('m.home_team', 'ht')
                      ->leftJoin('m.guest_team', 'gt')
                      ->where('t.id = :tournament_id')
                      ->setParameter('tournament_id', $tournamentId)
                      ->andWhere('tm.isQualificationMatch = :isQualificationMatch')
                      ->setParameter('isQualificationMatch', false)
                      ->andWhere('tm.isGroupMatch = :isGroupMatch')
                      ->setParameter('isGroupMatch', true)
                      ->groupBy('tm.id')
                      ->orderBy('tm.round', 'ASC')
                      ->addOrderBy('m.match_date', 'ASC')
                      ->addOrderBy('m.match_time', 'ASC')
                      ->addOrderBy('m.id', 'ASC')
                      ->getQuery();  
        
        try {
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }
    
    /**
    * Возвращает матчи этапа плей-офф по ID турнира
    * 
    * @param int $tournamentId
    * @return \AppBundle\Entity\TournamentChampionsleagueMatch[]
    */
    public function findPlayoffByTournament($tournamentId)
    {
      $query = $this->createQueryBuilder('tm')
                      ->select('tm, m, l, ht, gt')
                      ->leftJoin('tm.tournament', 't')
                      ->leftJoin('tm.match', 'm', Join::WITH, 'm.id = tm.match')
                      ->leftJoin('m.league', 'l')
                      ->leftJoin('m.home_team', 'ht')
                      ->leftJoin('m.guest_team', 'gt')
                      ->where('t.id = :tournament_id')
                      ->setParameter('tournament_id', $tournamentId)
                      ->andWhere('tm.isQualificationMatch = :isQualificationMatch')
                      ->setParameter('isQualificationMatch', false)
                      ->andWhere('tm.isGroupMatch = :isGroupMatch')
                      ->setParameter('isGroupMatch', false)
                      ->groupBy('tm.id')
                      ->orderBy('tm.round', 'ASC')
                      ->addOrderBy('m.match_date', 'ASC')
                      ->addOrderBy('m.match_time', 'ASC')
                      ->addOrderBy('m.id', 'ASC')
                      ->getQuery();
                              
        try {
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }
    
    /**
    * Поиск уже начавшихся матчей в турнире
    * 
    * @param int $id ID турнира
    * @return \AppBundle\Entity\TournamentPlayoffMatch[]
    */
    public function findStartedByTournament($id)
    {
        $q = $this->createQueryBuilder('tm')
                    ->leftJoin('tm.match', 'm')
                    ->where('m.isStarted = 1')
                    ->andWhere('tm.tournament = :id')                    
                    ->setParameter('id', $id)
                    ->getQuery();

        try {                    
            return $q->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }
    
    /**
    * Возвращает дату и время первого матча в турнире
    * 
    * @param $tournamentId ID турнира
    * @return string Дата/Время первого матча в турнире в формате Y-m-d H:i:s
    */
    public function getMinStartDatetime($tournamentId)
    {        
        $q = $this->createQueryBuilder('tm')
                  ->select('DATE_FORMAT(MIN(CONCAT(m.match_date, \' \',  m.match_time)), \'%Y-%m-%d %H:%i:%s\')')
                  ->leftJoin('tm.tournament', 't')
                  ->leftJoin('tm.match', 'm')
                  ->where('t.id = :id')
                  ->setParameter('id', $tournamentId)
                  ->getQuery();
        
        try {
            return $q->getSingleScalarResult();
        } catch (NoResultException $e) {              
            return null;
        }
    }
    
    /**
    * Возвращает количество матчей в турнире
    * 
    * @param int $tournamentId ID турнира
    * @return int
    */
    public function findMathesCountByTournament($tournamentId)
    {
        $q = $this->createQueryBuilder('tm')
                ->select("COUNT(tm.id)")
                ->where('tm.tournament = :tournament_id')
                ->setParameter('tournament_id', $tournamentId)
                ->getQuery();

        try {                    
            return $result = $q->getSingleScalarResult();
        } catch (NoResultException $e) {
            return 0;
        }
    }
    
    /**
    * Поиск плейофф матчей турнира по ID турнира
    * 
    * @param int $tournament_id
    * @return array
    */
    public function findIdsPlayoffNextRoundByTournament($tournament_id, $round)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT m.id
                                    FROM AppBundle\Entity\TournamentChampionsleagueMatch tm
                                    LEFT JOIN tm.match m
                                    WHERE tm.tournament = :tournament_id 
                                    AND tm.round >= :round
                                    AND tm.isQualificationMatch = 0 
                                    AND tm.isGroupMatch = 0")
                    ->setParameter("tournament_id", $tournament_id)
                    ->setParameter("round", $round);
                
        try {
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }
    
    /**
    * Находит матч турнира по id турнира и матча
    */
    public function findByTournamentAndMatch($tournament_id, $match_id)
    {
        $q = $this->createQueryBuilder('tm')
                ->where('tm.tournament = :tournament_id')
                ->andWhere('tm.match = :match_id')
                ->setParameter('tournament_id', $tournament_id)
                ->setParameter('match_id', $match_id)
                ->getQuery();
                
            try {                    
                    return $q->getSingleResult();
                } catch (NoResultException $e) {                                        
                    
                    return null;
                }
    }

    /**
    * Проверяет больше или равна переданная дата чем дата и время последнего матча в предыдущем туре
    * 
    * @param int $tournament_id
    * @param string $match_date
    * @param string $match_time
    * @param int $round
    */
    public function isMatchDatetimeMoreThanInPreviousRounds($tournament_id, $match_date, $match_time, $round, $qualificationMatch, $groupMatch)
    {
        if($qualificationMatch)
            $leftJoinCondition = 'tm.round < :round AND tm.isQualificationMatch = 1';
        elseif(!$qualificationMatch && $groupMatch)
            $leftJoinCondition = '(tm.round < :round AND tm.isQualificationMatch = 0) OR tm.isQualificationMatch = 1';
        elseif(!$qualificationMatch && !$groupMatch)
            $leftJoinCondition = '(tm.round < :round AND tm.isQualificationMatch = 0) OR tm.isQualificationMatch = 1 OR tm.isGroupMatch = 1';

        $q = $this->createQueryBuilder('tm')
                  ->select('DATE_FORMAT(MAX(CONCAT(m.match_date, \' \',  m.match_time)), \'%Y-%m-%d %H:%i:%s\') as max_start_datetime')
                  ->leftJoin('tm.tournament', 't')
                  ->leftJoin('tm.match', 'm', 'WITH', $leftJoinCondition)
                  ->setParameter('round', $round)
                  ->where('t.id = :tournament_id')
                  ->setParameter('tournament_id', $tournament_id)
                  ->getQuery();

        try {            
            $result = $q->getSingleResult();

            if(!$result["max_start_datetime"])
                return true;

            $maxStartDatetimeInPreviousRounds = new \DateTime($result["max_start_datetime"]);
            $matchDatetime = new \DateTime($match_date->format("Y-m-d")." ".$match_time->format("H:i:s"));    

            if($maxStartDatetimeInPreviousRounds->format('U') <= $matchDatetime->format('U'))
                return true;            
        } catch (NoResultException $e) {                            
              return true;
        }
    
        return false;
    }

    /**
    * Проверяет меньше или равна переданная дата чем дата и время последнего матча в следующем туре
    * 
    * @param int $tournament_id
    * @param string $match_date
    * @param string $match_time
    * @param int $round
    */
    public function isMatchDatetimeLessThanInNextRounds($tournament_id, $match_date, $match_time, $round, $qualificationMatch, $groupMatch)
    {
        if($qualificationMatch == 1)
            $leftJoinCondition = '(tm.round > :round AND tm.isQualificationMatch = 1) OR tm.isQualificationMatch = 0';
        elseif(!$qualificationMatch && $groupMatch)
            $leftJoinCondition = '(tm.round > :round AND tm.isGroupMatch = 1 AND tm.isQualificationMatch = 0) OR (tm.isGroupMatch = 0 AND tm.isQualificationMatch = 0)';
        elseif(!$qualificationMatch && !$groupMatch)
            $leftJoinCondition = 'tm.round > :round AND tm.isQualificationMatch = 0 AND tm.isGroupMatch = 0';

        $q = $this->createQueryBuilder('tm')
                  ->select('DATE_FORMAT(MIN(CONCAT(m.match_date, \' \',  m.match_time)), \'%Y-%m-%d %H:%i:%s\') as min_start_datetime')
                  ->leftJoin('tm.tournament', 't')
                  ->leftJoin('tm.match', 'm', 'WITH', $leftJoinCondition)
                  ->setParameter('round', $round)
                  ->where('t.id = :tournament_id')
                  ->setParameter('tournament_id', $tournament_id)
                  ->getQuery();

        try {            
            $result = $q->getSingleResult();

            if(!$result["min_start_datetime"])
                return true;
            
            $minStartDatetimeInNextRounds = new \DateTime($result["min_start_datetime"]);
            $matchDatetime = new \DateTime($match_date->format("Y-m-d")." ".$match_time->format("H:i:s"));
            
            if($minStartDatetimeInNextRounds->format('U') >= $matchDatetime->format('U'))
                return true;

        } catch (NoResultException $e) {                            
              return true;
        }
    
        return false;
    }
   /*
    public function removeUnneededMatchesAndBets($tournament_id, $groupRoundNum, $playoffRoundNum)
    {        
        $em = $this->getEntityManager();

        
        //Выбираем матчи группового этапа где раунд > $groupRoundNum и превращаем их в матчи плейофф        
        $q = $this->createQueryBuilder('tm')
                  ->select('tm')                  
                  ->where('tm.tournament = :tournament_id')
                  ->setParameter('tournament_id', $tournament_id)
                  ->andWhere('tm.round > :groupRoundNum AND tm.isQualificationMatch = 0 AND tm.isGroupMatch = 1')
                  ->setParameter('groupRoundNum', $groupRoundNum)
                  ->getQuery();
                                    
        try {            
            $matches = $q->getResult();
        }
         catch (NoResultException $e) {                            
              $matches = null;
        }

        if(empty($matches))
            return null;
        
        $matchesBetToRemoveIds = array();
        $lastPlayoffRound = 0;
        foreach($matches as $match)
        {
            $playoffRound = $match->getRound() - $groupRoundNum;
            
            if($playoffRound > $playoffRoundNum)
            {
                $matchesBetToRemoveIds[] = $match->getMatch()->getId();
                $em->remove($match);
                continue;
            }
            
            $match->setRound($playoffRound);
            $match->setIsGroupMatch(false);
            $em->persist($match);
            
            $lastPlayoffRound = $playoffRound;
        }
        
        //$em->flush();        
        var_dump($lastPlayoffRound);
        exit;
        
        //Выбираем все матчи плейофф        
        $q = $this->createQueryBuilder('tm')
                  ->select('tm')                  
                  ->where('tm.tournament = :tournament_id')
                  ->setParameter('tournament_id', $tournament_id)
                  ->andWhere('tm.isQualificationMatch = 0 AND tm.isGroupMatch = 0')                  
                  ->getQuery();
                                    
        try {            
            $matches = $q->getResult();
        }
         catch (NoResultException $e) {                            
              $matches = null;
        }
        
        if(!empty($matches))
        {
            foreach($matches as $match)
            {
                $playoffRound = $match->getRound() + $lastPlayoffRound;
                
                if($playoffRound > $playoffRoundNum)
                {
                    $matchesBetToRemoveIds[] = $match->getMatch()->getId();
                    $em->remove($match);
                    continue;                                        
                }
                
                $match->setRound($playoffRound);                    
                $em->persist($match);
                
                $em->flush();
            }
        }        

        $dql = "DELETE FROM AppBundle\Entity\Bet b
                    WHERE b.tournament = $tournament_id
                    AND b.match IN (".implode(', ', $matchesBetToRemoveIds).")";

        
        $query = $em->createQuery($dql);        
        $query->execute();        
        $em->flush();
    }
    */
    
    public function removeUnneededMatchesAndBets($tournament_id, $groupRoundNum, $playoffRoundNum)
    {        
        $em = $this->getEntityManager();

        $q = $this->createQueryBuilder('tm')
                  ->select('tm, t')
                  ->leftJoin('tm.tournament', 't')
                  ->where('t.id = :tournament_id')
                  ->setParameter('tournament_id', $tournament_id)
                  ->andWhere('(tm.round > :groupRoundNum AND tm.isQualificationMatch = 0 AND tm.isGroupMatch = 1) OR (tm.round > :playoffRoundNum AND tm.isQualificationMatch = 0 AND tm.isGroupMatch = 0)')
                  ->setParameter('groupRoundNum', $groupRoundNum)
                  ->setParameter('playoffRoundNum', $playoffRoundNum)
                  ->groupBy('tm.match')
                  ->getQuery();
                    
        try {            
            $result = $q->getResult();
        }
         catch (NoResultException $e) {                            
              return null;
        }

        if(empty($result))
            return null;

        $matchesIds = array();
        foreach($result as $match)
        {
            $matchesIds[] = $match->getMatch()->getId();
            $em->remove($match);            
        }        
        $em->flush();

        $dql = "DELETE FROM AppBundle\Entity\Bet b
                    WHERE b.tournament = $tournament_id
                    AND b.match IN (".implode(', ', $matchesIds).")";

        
        $query = $em->createQuery($dql);        
        $query->execute();        
        $em->flush();
    }
        
    public function findByTournamentAndRound($tournament_id, $round, $isQualificationMatch, $isGroupMatch)
    {
        $q = $this->createQueryBuilder('tm')
                ->where('tm.tournament = :tournament_id')                
                ->setParameter('tournament_id', $tournament_id)                
                ->andWhere('tm.round = :round')
                ->setParameter('round', $round)
                ->andWhere('tm.isQualificationMatch = :isQualificationMatch AND tm.isGroupMatch = :isGroupMatch')
                ->setParameter('isQualificationMatch', $isQualificationMatch)->setParameter('isGroupMatch', $isGroupMatch)
                ->getQuery();
                
        try {                    
                return $q->getResult();
            } catch (NoResultException $e) {                                        
                
                return null;
            }            
    }

    public function findTotalPointsForUsersByRound($tournament_id, $round, $isQualificationMatch, $isGroupMatch)
    {
        $q = $this->createQueryBuilder('tm')
                  ->select('u.id as user_id, tm.round, SUM(b.points) as total_points')
                  ->leftJoin('tm.tournament', 't')
                  ->leftJoin('t.users', 'u')                  
                  ->leftJoin('t.bets', 'b', 'WITH', 'tm.match = b.match AND u.id = b.user')
                  ->groupBy('u.id')
                  ->addGroupBy('tm.round')
                  ->where('t.id = :tournament_id')
                  ->setParameter('tournament_id', $tournament_id)                  
                  ->andWhere('tm.round = :round')
                  ->setParameter('round', $round)
                  ->andWhere('tm.isQualificationMatch = :isQualificationMatch AND tm.isGroupMatch = :isGroupMatch')
                  ->setParameter('isQualificationMatch', $isQualificationMatch)->setParameter('isGroupMatch', $isGroupMatch)                  
                  ->andWhere('b.points IS NOT NULL')
                  ->orderBy('total_points', 'DESC')
                  ->getQuery();
        
        try {                    
            return $q->getResult();
        } catch (NoResultException $e) {                                        
            
            return null;
        }
    }
    
    public function isMatchInTournamentAndRound($tournament_id, $match_id, $round_num, $qualificationMatch, $groupMatch)
    {
        $query = $this->createQueryBuilder('tm')
                ->select("COUNT(tm.id) AS CNT")
                ->where('tm.tournament = :tournament_id')                
                ->setParameter('tournament_id', $tournament_id)                
                ->andWhere('tm.match = :match_id')
                ->setParameter('match_id', $match_id)
                ->andWhere('tm.round = :round_num')
                ->setParameter('round_num', $round_num)                
                ->andWhere("tm.isQualificationMatch = :isQualificationMatch")
                ->setParameter("isQualificationMatch", $qualificationMatch)
                ->andWhere("tm.isGroupMatch = :isGroupMatch")
                ->setParameter("isGroupMatch", $groupMatch)                
                ->getQuery();
        
        try {
            $result = $query->getSingleResult();            
            return (bool) $result["CNT"];        
        } catch (NoResultException $e) {
            return false;
        }
    }
    
    public function findNonProcessedMatchesFinished($tournament_id, $round, $isQualificationMatch, $isGroupMatch)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT m.id
                                    FROM AppBundle\Entity\TournamentChampionsleagueMatch tcm
                                    LEFT JOIN tcm.tournament t
                                    LEFT JOIN t.championsleagues tc                                    
                                    LEFT JOIN tcm.match m
                                    WHERE t.id = :tournament_id 
                                        AND tcm.round = :round 
                                        AND tcm.isQualificationMatch = :isQualificationMatch
                                        AND tcm.isGroupMatch = :isGroupMatch
                                        AND m.isStarted = 1 
                                        AND (m.isOver = 1 OR m.isRejected = 1) 
                                        AND tcm.isProcessed = 0")
                    ->setParameter("tournament_id", $tournament_id)
                    ->setParameter("round", $round)                    
                    ->setParameter("isQualificationMatch", $isQualificationMatch)
                    ->setParameter("isGroupMatch", $isGroupMatch);
                                                            
        try {            
            $result = $query->getResult();
                        
            if(count($result) != 0)
                return $result;
            else
                return null;                        
        } catch (NoResultException $e) {
            return null;
        }
    }
    
    public function allBetsCalculated($tournament_id, $matches_ids)
    {        
        $em = $this->getEntityManager();        
        
        $query = $em->createQuery("SELECT COUNT(b.id) as total_bets
                                    FROM AppBundle\Entity\Bet b
                                    WHERE b.tournament = :tournament_id AND b.match IN (:matches_ids)
                                    ")
                    ->setParameter("tournament_id", $tournament_id)                    
                    ->setParameter("matches_ids", $matches_ids);
                    
        try {            
            $result = $query->getSingleResult();            
            $totalBets = $result["total_bets"];                        
        } catch (NoResultException $e) {
            return false;
        }
        
        if($totalBets == 0)
            return true;                
        
        $query = $em->createQuery("SELECT COUNT(b.id) as calculated_bets
                                    FROM AppBundle\Entity\Bet b
                                    WHERE b.tournament = :tournament_id AND b.points IS NOT NULL AND b.match IN (:matches_ids)
                                    ")
                    ->setParameter("tournament_id", $tournament_id)                    
                    ->setParameter("matches_ids", $matches_ids);
                    
        try {            
            $result = $query->getSingleResult();            
            $calculatedBets = $result["calculated_bets"];            
        } catch (NoResultException $e) {
            return false;
        }                
        
        if($totalBets == $calculatedBets)
            return true;
        else
            return false;
    }
    
    public function findByIds($tournament_id, $matches_ids)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT tm, m
                                    FROM AppBundle\Entity\TournamentChampionsleagueMatch tm
                                        LEFT JOIN tm.match m WITH m.id = tm.match
                                    WHERE tm.tournament = :tournament_id AND m.id IN (:matches_ids)")
                    ->setParameter("tournament_id", $tournament_id)
                    ->setParameter("matches_ids", $matches_ids);        
        try {            
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }
    
    public function allRoundMatchesFinished($tournament_id, $round, $isQualificationMatch, $isGroupMatch)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT COUNT(m.id) as matches_finished, tc.matches_in_qualification, tc.matches_in_game, tc.matches_in_playoff
                                    FROM AppBundle\Entity\TournamentChampionsleagueMatch tcm
                                    LEFT JOIN tcm.tournament t
                                    LEFT JOIN t.championsleagues tc                                    
                                    LEFT JOIN tcm.match m
                                    WHERE t.id = :tournament_id 
                                        AND tcm.round = :round
                                        AND tcm.isQualificationMatch = :isQualificationMatch
                                        AND tcm.isGroupMatch = :isGroupMatch
                                        AND m.isStarted = 1 
                                        AND (m.isOver = 1 OR m.isRejected = 1)")
                    ->setParameter("tournament_id", $tournament_id)
                    ->setParameter("round", $round)
                    ->setParameter("isQualificationMatch", $isQualificationMatch)
                    ->setParameter("isGroupMatch", $isGroupMatch);
           
        try {
            $result = $query->getSingleResult();            
            if($isQualificationMatch)            
                return (intval($result["matches_finished"]) === intval($result["matches_in_qualification"]));
            elseif(!$isQualificationMatch && $isGroupMatch)
                return (intval($result["matches_finished"]) === intval($result["matches_in_game"]));
            elseif(!$isQualificationMatch && !$isGroupMatch)
                return (intval($result["matches_finished"]) === intval($result["matches_in_playoff"]));
        } catch (NoResultException $e) {
            return false;
        }
    }
    
    public function matchesPointsCalculated($tournament_id, $round, $isQualificationMatch, $isGroupMatch)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT COUNT(b.id) as bets_without_points
                                    FROM AppBundle\Entity\Bet b
                                    WHERE b.points IS NULL AND b.match IN (
                                        SELECT m.id
                                            FROM AppBundle\Entity\TournamentChampionsleagueMatch tcm
                                            LEFT JOIN tcm.match m
                                            WHERE tcm.tournament = :tournament_id 
                                                AND tcm.round = :round 
                                                AND tcm.isQualificationMatch = :isQualificationMatch 
                                                AND tcm.isGroupMatch = :isGroupMatch
                                    )
                                    ")
                    ->setParameter("tournament_id", $tournament_id)
                    ->setParameter("round", $round)
                    ->setParameter("isQualificationMatch", $isQualificationMatch)
                    ->setParameter("isGroupMatch", $isGroupMatch);
           
        try {            
            $result = $query->getSingleResult();
            return (intval($result["bets_without_points"]) === 0);
        } catch (NoResultException $e) {
            return false;
        }
    }
    
    public function findGroupMatchesAndBetsByTournament($tournament_id, $group)
    {
        $em = $this->getEntityManager();    
        
        $query = $em->createQuery("SELECT tcm, m, ht, gt, b
                                    FROM AppBundle\Entity\TournamentChampionsleagueMatch tcm                                    
                                    LEFT JOIN tcm.match m
                                    LEFT JOIN m.home_team ht
                                    LEFT JOIN m.guest_team gt
                                    LEFT JOIN m.bets b
                                    WHERE tcm.tournament = :tournament_id
                                    AND tcm.isQualificationMatch = 0
                                    AND tcm.isGroupMatch = 1
                                    AND tcm.isProcessed = 1
                                    AND b.points IS NOT NULL                                    
                                    AND b.user IN (
                                        SELECT u.id
                                            FROM AppBundle\Entity\TournamentChampionsleagueGroupResult tgr
                                            LEFT JOIN tgr.user u
                                            WHERE tgr.tournament = :tournament_id
                                            AND tgr.group_num = :group
                                    )
                                    ")
                    ->setParameter("tournament_id", $tournament_id)                                        
                    ->setParameter("group", $group);
        
        try {            
            return  $query->getResult();            
        } catch (NoResultException $e) {
            return null;
        }
    }
    
    public function findPlayoffMatchesAndBetsByTournamentAndRound($tournament_id, $round)
    {        
        /*
        $subquery = $this->getEntityManager()->createQueryBuilder()
                         ->select("u.id")                         
                         ->from('AppBundle\Entity\TournamentChampionsleaguePlayoffGame', 'tgr')                         
                         ->leftJoin('tgr.user_left', 'u', \Doctrine\ORM\Query\Expr\Join::ON, 'tgr.user_left = u.id OR tgr.user_right = u.id')
                         ->where("tgr.tournament = :tournament_id");
        $subquery = $subquery->getQuery();
        echo $subquery->getSQL();
        exit;
        $query = $this->createQueryBuilder("tcm");
        $query = $query->select("tcm, m, ht, gt, b")
                      ->leftJoin("tcm.match", "m")
                      ->leftJoin("m.home_team", "ht")
                      ->leftJoin("m.guest_team", "gt")
                      ->leftJoin("m.bets", "b")
                      ->where("tcm.tournament = :tournament_id
                                    AND tcm.round = :round
                                    AND tcm.isQualificationMatch = 0
                                    AND tcm.isGroupMatch = 0                                    
                                    AND tcm.isProcessed = 1")
                      ->andWhere($query->expr()->in("b.user", $subquery->getDQL()))
                      ->setParameter("tournament_id", $tournament_id)                                        
                      ->setParameter("round", $round)
                      ->getQuery();
        */
        $em = $this->getEntityManager();
        
        $query = $em->createQuery("SELECT tcm, m, ht, gt, b
                                    FROM AppBundle\Entity\TournamentChampionsleagueMatch tcm                                    
																		LEFT JOIN tcm.tournament t
                                    LEFT JOIN tcm.match m
                                    LEFT JOIN m.home_team ht
                                    LEFT JOIN m.guest_team gt
                                    LEFT JOIN m.bets b WITH t.id = b.tournament
                                    WHERE tcm.tournament = :tournament_id
                                    AND tcm.round = :round
                                    AND tcm.isQualificationMatch = 0
                                    AND tcm.isGroupMatch = 0                                    
                                    AND tcm.isProcessed = 1
                                    AND b.points IS NOT NULL                                    
                                    AND 
                                    (b.user IN (
                                        SELECT u1.id
                                            FROM AppBundle\Entity\TournamentChampionsleaguePlayoffGame tgr1
                                            LEFT JOIN tgr1.user_left u1
                                            WHERE tgr1.tournament = :tournament_id
                                    )
                                    OR
                                    b.user IN (
                                        SELECT u2.id
                                            FROM AppBundle\Entity\TournamentChampionsleaguePlayoffGame tgr2
                                            LEFT JOIN tgr2.user_right u2
                                            WHERE tgr2.tournament = :tournament_id
                                    )
                                    )
                                    ")
                    ->setParameter("tournament_id", $tournament_id)                                        
                    ->setParameter("round", $round);        
        try {            
            return  $query->getResult();            
        } catch (NoResultException $e) {
            return null;
        }
    }
}
