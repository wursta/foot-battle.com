<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AppBundle\Entity\TournamentChampionsleaguePlayoffGame
 *
 * @ORM\Table(name="tournaments_championsleague_playoff_games")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\TournamentChampionsleaguePlayoffGameRepository")
 */
class TournamentChampionsleaguePlayoffGame
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")     
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Tournament", inversedBy="championsleague_playoff_games")
     * @ORM\JoinColumn(name="tournament_id", referencedColumnName="id")
     */
    protected $tournament;

    /**
     * @ORM\Column(type="integer")     
     */
    private $round;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="tournaments_championsleague_playoff_games_at_left")
     * @ORM\JoinColumn(name="user_left", referencedColumnName="id")
     */
    protected $user_left;   
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="tournaments_championsleague_playoff_games_at_right")
     * @ORM\JoinColumn(name="user_right", referencedColumnName="id")
     */
    protected $user_right;        
    
    /**
     * @ORM\Column(type="float")     
     * @Assert\Range(min = 0)
     */
    protected $points_left;

    /**
     * @ORM\Column(type="float")     
     * @Assert\Range(min = 0)
     */
    protected $points_right;

    /**
     * @ORM\Column(type="integer")     
     */
    private $total_guessed_results_left;
    
    /**
     * @ORM\Column(type="integer")     
     */
    private $total_guessed_outcomes_left;
    
    /**
     * @ORM\Column(type="integer")     
     */
    private $total_guessed_differences_left;

    /**
     * @ORM\Column(type="integer")     
     */
    private $total_guessed_results_right;
    
    /**
     * @ORM\Column(type="integer")     
     */
    private $total_guessed_outcomes_right;
    
    /**
     * @ORM\Column(type="integer")     
     */
    private $total_guessed_differences_right;

    /**
     * @ORM\Column(type="float")     
     */
    private $user_rating_left;
    
    /**
     * @ORM\Column(type="float")     
     */
    private $user_rating_right;
    
    /**
     * @ORM\Column(type="boolean")     
     */
    private $user_left_is_winner;
    
    /**
     * @ORM\Column(type="boolean")     
     */
    private $user_right_is_winner;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set round
     *
     * @param integer $round
     * @return TournamentChampionsleaguePlayoffGame
     */
    public function setRound($round)
    {
        $this->round = $round;

        return $this;
    }

    /**
     * Get round
     *
     * @return integer 
     */
    public function getRound()
    {
        return $this->round;
    }

    /**
     * Set points_left
     *
     * @param float $pointsLeft
     * @return TournamentChampionsleaguePlayoffGame
     */
    public function setPointsLeft($pointsLeft)
    {
        $this->points_left = $pointsLeft;

        return $this;
    }

    /**
     * Get points_left
     *
     * @return float 
     */
    public function getPointsLeft()
    {
        return $this->points_left;
    }

    /**
     * Set points_right
     *
     * @param float $pointsRight
     * @return TournamentChampionsleaguePlayoffGame
     */
    public function setPointsRight($pointsRight)
    {
        $this->points_right = $pointsRight;

        return $this;
    }

    /**
     * Get points_right
     *
     * @return float 
     */
    public function getPointsRight()
    {
        return $this->points_right;
    }

    /**
     * Set total_guessed_results_left
     *
     * @param integer $totalGuessedResultsLeft
     * @return TournamentChampionsleaguePlayoffGame
     */
    public function setTotalGuessedResultsLeft($totalGuessedResultsLeft)
    {
        $this->total_guessed_results_left = $totalGuessedResultsLeft;

        return $this;
    }

    /**
     * Get total_guessed_results_left
     *
     * @return integer 
     */
    public function getTotalGuessedResultsLeft()
    {
        return $this->total_guessed_results_left;
    }

    /**
     * Set total_guessed_outcomes_left
     *
     * @param integer $totalGuessedOutcomesLeft
     * @return TournamentChampionsleaguePlayoffGame
     */
    public function setTotalGuessedOutcomesLeft($totalGuessedOutcomesLeft)
    {
        $this->total_guessed_outcomes_left = $totalGuessedOutcomesLeft;

        return $this;
    }

    /**
     * Get total_guessed_outcomes_left
     *
     * @return integer 
     */
    public function getTotalGuessedOutcomesLeft()
    {
        return $this->total_guessed_outcomes_left;
    }

    /**
     * Set total_guessed_differences_left
     *
     * @param integer $totalGuessedDifferencesLeft
     * @return TournamentChampionsleaguePlayoffGame
     */
    public function setTotalGuessedDifferencesLeft($totalGuessedDifferencesLeft)
    {
        $this->total_guessed_differences_left = $totalGuessedDifferencesLeft;

        return $this;
    }

    /**
     * Get total_guessed_differences_left
     *
     * @return integer 
     */
    public function getTotalGuessedDifferencesLeft()
    {
        return $this->total_guessed_differences_left;
    }

    /**
     * Set total_guessed_results_right
     *
     * @param integer $totalGuessedResultsRight
     * @return TournamentChampionsleaguePlayoffGame
     */
    public function setTotalGuessedResultsRight($totalGuessedResultsRight)
    {
        $this->total_guessed_results_right = $totalGuessedResultsRight;

        return $this;
    }

    /**
     * Get total_guessed_results_right
     *
     * @return integer 
     */
    public function getTotalGuessedResultsRight()
    {
        return $this->total_guessed_results_right;
    }

    /**
     * Set total_guessed_outcomes_right
     *
     * @param integer $totalGuessedOutcomesRight
     * @return TournamentChampionsleaguePlayoffGame
     */
    public function setTotalGuessedOutcomesRight($totalGuessedOutcomesRight)
    {
        $this->total_guessed_outcomes_right = $totalGuessedOutcomesRight;

        return $this;
    }

    /**
     * Get total_guessed_outcomes_right
     *
     * @return integer 
     */
    public function getTotalGuessedOutcomesRight()
    {
        return $this->total_guessed_outcomes_right;
    }

    /**
     * Set total_guessed_differences_right
     *
     * @param integer $totalGuessedDifferencesRight
     * @return TournamentChampionsleaguePlayoffGame
     */
    public function setTotalGuessedDifferencesRight($totalGuessedDifferencesRight)
    {
        $this->total_guessed_differences_right = $totalGuessedDifferencesRight;

        return $this;
    }

    /**
     * Get total_guessed_differences_right
     *
     * @return integer 
     */
    public function getTotalGuessedDifferencesRight()
    {
        return $this->total_guessed_differences_right;
    }

    /**
     * Set tournament
     *
     * @param \AppBundle\Entity\Tournament $tournament
     * @return TournamentChampionsleaguePlayoffGame
     */
    public function setTournament(\AppBundle\Entity\Tournament $tournament = null)
    {
        $this->tournament = $tournament;

        return $this;
    }

    /**
     * Get tournament
     *
     * @return \AppBundle\Entity\Tournament 
     */
    public function getTournament()
    {
        return $this->tournament;
    }

    /**
     * Set user_left
     *
     * @param \AppBundle\Entity\User $userLeft
     * @return TournamentChampionsleaguePlayoffGame
     */
    public function setUserLeft(\AppBundle\Entity\User $userLeft = null)
    {
        $this->user_left = $userLeft;

        return $this;
    }

    /**
     * Get user_left
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUserLeft()
    {
        return $this->user_left;
    }

    /**
     * Set user_right
     *
     * @param \AppBundle\Entity\User $userRight
     * @return TournamentChampionsleaguePlayoffGame
     */
    public function setUserRight(\AppBundle\Entity\User $userRight = null)
    {
        $this->user_right = $userRight;

        return $this;
    }

    /**
     * Get user_right
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUserRight()
    {
        return $this->user_right;
    }

    /**
     * Set user_rating_left
     *
     * @param float $userRatingLeft
     * @return TournamentChampionsleaguePlayoffGame
     */
    public function setUserRatingLeft($userRatingLeft)
    {
        $this->user_rating_left = $userRatingLeft;

        return $this;
    }

    /**
     * Get user_rating_left
     *
     * @return float 
     */
    public function getUserRatingLeft()
    {
        return $this->user_rating_left;
    }

    /**
     * Set user_rating_right
     *
     * @param float $userRatingRight
     * @return TournamentChampionsleaguePlayoffGame
     */
    public function setUserRatingRight($userRatingRight)
    {
        $this->user_rating_right = $userRatingRight;

        return $this;
    }

    /**
     * Get user_rating_right
     *
     * @return float 
     */
    public function getUserRatingRight()
    {
        return $this->user_rating_right;
    }

    /**
     * Set user_left_is_winner
     *
     * @param boolean $userLeftIsWinner
     * @return TournamentChampionsleaguePlayoffGame
     */
    public function setUserLeftIsWinner($userLeftIsWinner)
    {
        $this->user_left_is_winner = $userLeftIsWinner;

        return $this;
    }

    /**
     * Get user_left_is_winner
     *
     * @return boolean 
     */
    public function getUserLeftIsWinner()
    {
        return $this->user_left_is_winner;
    }

    /**
     * Set user_right_is_winner
     *
     * @param boolean $userRightIsWinner
     * @return TournamentChampionsleaguePlayoffGame
     */
    public function setUserRightIsWinner($userRightIsWinner)
    {
        $this->user_right_is_winner = $userRightIsWinner;

        return $this;
    }

    /**
     * Get user_right_is_winner
     *
     * @return boolean 
     */
    public function getUserRightIsWinner()
    {
        return $this->user_right_is_winner;
    }
}
