<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * TournamentChampionsleaguePlayoffGameRepository
 *
 */
class TournamentChampionsleaguePlayoffGameRepository extends EntityRepository
{
    public function isUserInPlayoffRound($tournament_id, $user_id, $round)
    {        
        $q = $this->createQueryBuilder('tpg')
                  ->select('COUNT(tpg.id) as cnt')                  
                  ->leftJoin('tpg.tournament', 't')
                  ->leftJoin('t.championsleagues', 'tc')
                  ->where('tpg.tournament = :tournament_id')
                  ->setParameter('tournament_id', $tournament_id)
                  ->andWhere('tpg.user_left = :user_id OR tpg.user_right = :user_id')
                  ->setParameter('user_id', $user_id)                  
                  ->andWhere('tpg.round = :round')
                  ->setParameter('round', $round)
                  ->getQuery();
                  
        try {            
            $result = $q->getSingleResult();

            return (bool) $result["cnt"];

          } catch (NoResultException $e) {                                                      
              return false;
          }
    }

    public function findByTournamentAndRound($tournament_id, $round)
    {
        $q = $this->createQueryBuilder('tpg')
                  ->addSelect('t, ul, ur')
                  ->leftJoin('tpg.tournament', 't')
                  ->leftJoin('tpg.user_left', 'ul')
                  ->leftJoin('tpg.user_right', 'ur')
                  ->where('tpg.tournament = :tournament_id')
                  ->setParameter('tournament_id', $tournament_id)
                  ->andWhere('tpg.round = :round')
                  ->setParameter('round', $round)
                  ->orderBy('tpg.id', 'ASC')
                  ->getQuery();
                  
        try {            
            return $q->getResult();
          } catch (NoResultException $e) {                                                      
              return null;
          }
    }
}
