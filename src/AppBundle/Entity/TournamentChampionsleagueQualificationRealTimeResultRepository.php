<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * TournamentChampionsleagueQualificationRealTimeResultRepository
 *
 */
class TournamentChampionsleagueQualificationRealTimeResultRepository extends EntityRepository
{
    public function findByTournament($tournament_id)
    {
        $q = $this->createQueryBuilder('tcr')
                  ->addSelect('t, u')
                  ->leftJoin('tcr.tournament', 't')
                  ->leftJoin('tcr.user', 'u')
                  ->where('tcr.tournament = :tournament_id')
                  ->setParameter('tournament_id', $tournament_id)                  
                  ->orderBy('tcr.place', 'ASC')
                  ->getQuery();
                          
        try {            
            return $q->getResult();
          } catch (NoResultException $e) {                                                      
              return null;
          }
    }
    
    /**
    * Сортируем результаты по турниру и туры
    */
    public function findSorted($tournament_id)
    {
        $q = $this->createQueryBuilder('tcr')
                  ->leftJoin("tcr.user", "u")
                  ->where('tcr.tournament = :tournament_id')
                  ->setParameter('tournament_id', $tournament_id)
                  ->addOrderBy('tcr.total_points', 'DESC')
                  ->addOrderBy('tcr.total_guessed_results', 'DESC')
                  ->addOrderBy('tcr.total_guessed_outcomes', 'DESC')
                  ->addOrderBy('tcr.total_guessed_differences', 'DESC')
                  ->addOrderBy('u.rating', 'DESC')
                  ->getQuery();
                  
        try {            
            return $q->getResult();
          } catch (NoResultException $e) {                                                      
              return null;
          }
    }
    
    public function removeByTournament($tournament_id)
    {
        $q = $this->createQueryBuilder('tcr')
                    ->delete()
                    ->where('tcr.tournament = :tournament_id')
                    ->setParameter('tournament_id', $tournament_id)
                    ->getQuery();
                    
        $q->execute();
    }
}
