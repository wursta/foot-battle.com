<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * TournamentChampionsleagueQualificationResultRepository
 *
 */
class TournamentChampionsleagueQualificationResultRepository extends EntityRepository
{
    /**
    * Ищет результаты квалификации по турниру и раунду
    * 
    * @param int $tournament_id
    * @param int $round
    * @return array
    */
    public function findByTournamentAndRound($tournament_id, $round)
    {
      $q = $this->createQueryBuilder('tcr')
                  ->addSelect('t, u')
                  ->leftJoin('tcr.tournament', 't')
                  ->leftJoin('tcr.user', 'u')
                  ->where('tcr.tournament = :tournament_id')
                  ->setParameter('tournament_id', $tournament_id)
                  ->andWhere('tcr.round = :round')
                  ->setParameter('round', $round)                
                  ->orderBy('tcr.place', 'ASC')
                  ->getQuery();
                          
        try {            
            return $q->getResult();
          } catch (NoResultException $e) {                                                      
              return null;
          }
    }
    
    /**
    * Ищет пользователей проходящих в групповой этап из квалификации по турниру и раунду
    * 
    * @param int $tournament_id
    * @param int $round
    * @return array
    */
    public function findUsersForGroupsByTournamentAndRound($tournament_id, $round, $maxPlace)
    {        
        $em = $this->getEntityManager();
        
        $q = $this->createQueryBuilder('tcr')
                  ->addSelect('u')
                  ->leftJoin('tcr.tournament', 't')
                  ->leftJoin('tcr.user', 'u')
                  ->where('tcr.tournament = :tournament_id')
                  ->setParameter('tournament_id', $tournament_id)
                  ->andWhere('tcr.round = :round')
                  ->setParameter('round', $round)                
                  ->andWhere('tcr.place <= :maxPlace')
                  ->setParameter('maxPlace', $maxPlace)
                  ->orderBy('tcr.place', 'ASC')
                  ->getQuery();                
        
        try {            
            return $q->getResult();
          } catch (NoResultException $e) {                                                      
              return null;
          }
    }
    
    /**
    * Ищет пользователей не проходящих в групповой этап и плейофф из квалификации по турниру и раунду
    * 
    * @param int $tournament_id
    * @param int $round
    * @return array
    */
    public function findByTournamentUsersIdsNotInGroupsAndPlayoff($tournament_id, $lastRound, $maxPlace)
    {        
        $em = $this->getEntityManager();
        
        $q = $this->createQueryBuilder('tcr')
                  ->addSelect('u')
                  ->leftJoin('tcr.tournament', 't')
                  ->leftJoin('tcr.user', 'u')
                  ->where('tcr.tournament = :tournament_id')
                  ->setParameter('tournament_id', $tournament_id)
                  ->andWhere('tcr.round = :round')
                  ->setParameter('round', $lastRound)                
                  ->andWhere('tcr.place > :maxPlace')
                  ->setParameter('maxPlace', $maxPlace)
                  ->orderBy('tcr.place', 'ASC')
                  ->getQuery();                
        
        try {            
            $results = $q->getResult();
            $users = array();
            foreach($results as $result)
                $users[] = $result->getUser()->getId();
            
            return $users;
          } catch (NoResultException $e) {                                                      
              return null;
          }                    
    }
}
