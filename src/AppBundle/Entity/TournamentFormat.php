<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AppBundle\Entity\TournamentFormat
 *
 * @ORM\Table(name="tournament_formats")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\TournamentFormatRepository")
 */
class TournamentFormat
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")     
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=50)     
     * @Assert\NotBlank()
     * @Assert\Length(min = 1, max = 50)     
     */
    protected $internal_name;
    
    /**
     * @ORM\Column(type="string", length=255)     
     * @Assert\NotBlank()
     * @Assert\Length(min = 2, max = 255)     
     */
    protected $title;
    
    /**
     * @ORM\OneToMany(targetEntity="Tournament", mappedBy="format")
     */
    protected $tournaments;

    /**
     * @ORM\OneToMany(targetEntity="WinRulesGroup", mappedBy="format")
     */
    protected $win_rules_groups;
    
    /**
     * @ORM\OneToMany(targetEntity="Regulation", mappedBy="tournamentFormat")
     */
    protected $regulations;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tournaments = new \Doctrine\Common\Collections\ArrayCollection();        
        $this->win_rules_groups = new \Doctrine\Common\Collections\ArrayCollection();
        $this->regulations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set internal_name
     *
     * @param string $internalName
     * @return TournamentFormat
     */
    public function setInternalName($internalName)
    {
        $this->internal_name = $internalName;
    
        return $this;
    }

    /**
     * Get internal_name
     *
     * @return string 
     */
    public function getInternalName()
    {
        return $this->internal_name;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return TournamentFormat
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Add tournaments
     *
     * @param \AppBundle\Entity\Tournament $tournaments
     * @return TournamentFormat
     */
    public function addTournament(\AppBundle\Entity\Tournament $tournaments)
    {
        $this->tournaments[] = $tournaments;
    
        return $this;
    }

    /**
     * Remove tournaments
     *
     * @param \AppBundle\Entity\Tournament $tournaments
     */
    public function removeTournament(\AppBundle\Entity\Tournament $tournaments)
    {
        $this->tournaments->removeElement($tournaments);
    }

    /**
     * Get tournaments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTournaments()
    {
        return $this->tournaments;
    }

    /**
     * Add win_rules_groups
     *
     * @param \AppBundle\Entity\WinRulesGroup $winRulesGroups
     * @return TournamentFormat
     */
    public function addWinRulesGroup(\AppBundle\Entity\WinRulesGroup $winRulesGroups)
    {
        $this->win_rules_groups[] = $winRulesGroups;

        return $this;
    }

    /**
     * Remove win_rules_groups
     *
     * @param \AppBundle\Entity\WinRulesGroup $winRulesGroups
     */
    public function removeWinRulesGroup(\AppBundle\Entity\WinRulesGroup $winRulesGroups)
    {
        $this->win_rules_groups->removeElement($winRulesGroups);
    }

    /**
     * Get win_rules_groups
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWinRulesGroups()
    {
        return $this->win_rules_groups;
    }

    /**
     * Add regulation
     *
     * @param \AppBundle\Entity\Regulation $regulation
     *
     * @return TournamentFormat
     */
    public function addRegulation(\AppBundle\Entity\Regulation $regulation)
    {
        $this->regulations[] = $regulation;

        return $this;
    }

    /**
     * Remove regulation
     *
     * @param \AppBundle\Entity\Regulation $regulation
     */
    public function removeRegulation(\AppBundle\Entity\Regulation $regulation)
    {
        $this->regulations->removeElement($regulation);
    }

    /**
     * Get regulations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRegulations()
    {
        return $this->regulations;
    }
}
