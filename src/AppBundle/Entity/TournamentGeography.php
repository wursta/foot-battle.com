<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AppBundle\Entity\TournamentGeography
 *
 * @ORM\Table(name="tournament_geography")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\TournamentGeographyRepository")
 */
class TournamentGeography
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")     
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=50)     
     * @Assert\NotBlank()
     * @Assert\Length(min = 1, max = 50)     
     */
    protected $internal_name;
    
    /**
     * @ORM\Column(type="string", length=255)     
     * @Assert\NotBlank()
     * @Assert\Length(min = 2, max = 255)     
     */
    protected $title;
    
    /**
     * @ORM\OneToMany(targetEntity="Tournament", mappedBy="geography")
     */
    protected $tournaments;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tournaments = new \Doctrine\Common\Collections\ArrayCollection();        
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set internal_name
     *
     * @param string $internalName
     * @return TournamentGeography
     */
    public function setInternalName($internalName)
    {
        $this->internal_name = $internalName;
    
        return $this;
    }

    /**
     * Get internal_name
     *
     * @return string 
     */
    public function getInternalName()
    {
        return $this->internal_name;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return TournamentGeography
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Add tournaments
     *
     * @param \AppBundle\Entity\Tournament $tournaments
     * @return TournamentGeography
     */
    public function addTournament(\AppBundle\Entity\Tournament $tournaments)
    {
        $this->tournaments[] = $tournaments;
    
        return $this;
    }

    /**
     * Remove tournaments
     *
     * @param \AppBundle\Entity\Tournament $tournaments
     */
    public function removeTournament(\AppBundle\Entity\Tournament $tournaments)
    {
        $this->tournaments->removeElement($tournaments);
    }

    /**
     * Get tournaments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTournaments()
    {
        return $this->tournaments;
    }
}
