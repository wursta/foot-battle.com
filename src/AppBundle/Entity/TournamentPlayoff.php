<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AppBundle\Entity\TournamentPlayoff
 *
 * @ORM\Table(name="tournaments_playoff")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\TournamentPlayoffRepository")
 */
class TournamentPlayoff
{
    const STAGE_QUALIFICATION = 'QUALIFICATION';
    const STAGE_PLAYOFF = 'PLAYOFF';

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")     
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Tournament", inversedBy="playoffs")
     * @ORM\JoinColumn(name="tournament_id", referencedColumnName="id")
     */
    protected $tournament;
    
    /**
     * @ORM\Column(name="qualification_rounds_count", type="integer")     
     */
    private $qualificationRoundsCount;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min = 1)
     */
    private $matches_in_qualification;
    
    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min = 1)
     */
    private $matches_in_game;
    
    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min = 2)
     */
    private $max_users_count;            

    /**
     * @ORM\Column(type="string")
     * @Assert\Choice({"QUALIFICATION", "PLAYOFF"})
     */
    private $stage;
    
    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min = 2)
     */
    private $real_users_out_of_qualification;        

    /**
     * @ORM\Column(name="playoff_rounds_count", type="integer")     
     */
    private $playoffRoundsCount;
    
    /**
     * @ORM\Column(name="current_round", type="integer")     
     */
    private $currentRound;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->currentRound = 1;
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set max_users_count
     *
     * @param integer $maxUsersCount
     * @return TournamentPlayoff
     */
    public function setMaxUsersCount($maxUsersCount)
    {
        $this->max_users_count = $maxUsersCount;
    
        return $this;
    }

    /**
     * Get max_users_count
     *
     * @return integer 
     */
    public function getMaxUsersCount()
    {
        return $this->max_users_count;
    }

    /**
     * Set tournament
     *
     * @param \AppBundle\Entity\Tournament $tournament
     * @return TournamentPlayoff
     */
    public function setTournament(\AppBundle\Entity\Tournament $tournament = null)
    {
        $this->tournament = $tournament;
    
        return $this;
    }

    /**
     * Get tournament
     *
     * @return \AppBundle\Entity\Tournament 
     */
    public function getTournament()
    {
        return $this->tournament;
    }

    /**
     * Set matches_in_game
     *
     * @param integer $matchesInGame
     * @return TournamentPlayoff
     */
    public function setMatchesInGame($matchesInGame)
    {
        $this->matches_in_game = $matchesInGame;
    
        return $this;
    }

    /**
     * Get matches_in_game
     *
     * @return integer 
     */
    public function getMatchesInGame()
    {
        return $this->matches_in_game;
    }

    /**
     * Set matches_in_qualification
     *
     * @param integer $matchesInQualification
     * @return TournamentPlayoff
     */
    public function setMatchesInQualification($matchesInQualification)
    {
        $this->matches_in_qualification = $matchesInQualification;
    
        return $this;
    }

    /**
     * Get matches_in_qualification
     *
     * @return integer 
     */
    public function getMatchesInQualification()
    {
        return $this->matches_in_qualification;
    }

    /**
     * Set qualificationRoundsCount
     *
     * @param integer $qualificationRoundsCount
     * @return TournamentPlayoff
     */
    public function setQualificationRoundsCount($qualificationRoundsCount)
    {
        $this->qualificationRoundsCount = $qualificationRoundsCount;

        return $this;
    }

    /**
     * Get qualificationRoundsCount
     *
     * @return integer 
     */
    public function getQualificationRoundsCount()
    {
        return $this->qualificationRoundsCount;
    }

    /**
     * Set stage
     *
     * @param string $stage
     * @return TournamentPlayoff
     */
    public function setStage($stage)
    {
        $this->stage = $stage;

        return $this;
    }

    /**
     * Get stage
     *
     * @return string 
     */
    public function getStage()
    {
        return $this->stage;
    }

    /**
     * Set playoffRoundsCount
     *
     * @param integer $playoffRoundsCount
     * @return TournamentPlayoff
     */
    public function setPlayoffRoundsCount($playoffRoundsCount)
    {
        $this->playoffRoundsCount = $playoffRoundsCount;

        return $this;
    }

    /**
     * Get playoffRoundsCount
     *
     * @return integer 
     */
    public function getPlayoffRoundsCount()
    {
        return $this->playoffRoundsCount;
    }

    /**
     * Set currentPlayoffRound
     *
     * @param integer $currentPlayoffRound
     * @return TournamentPlayoff
     */
    public function setCurrentPlayoffRound($currentPlayoffRound)
    {
        $this->currentPlayoffRound = $currentPlayoffRound;
    
        return $this;
    }

    /**
     * Get currentPlayoffRound
     *
     * @return integer 
     */
    public function getCurrentPlayoffRound()
    {
        return $this->currentPlayoffRound;
    }

    /**
     * Set currentRound
     *
     * @param integer $currentRound
     *
     * @return TournamentPlayoff
     */
    public function setCurrentRound($currentRound)
    {
        $this->currentRound = $currentRound;

        return $this;
    }

    /**
     * Get currentRound
     *
     * @return integer
     */
    public function getCurrentRound()
    {
        return $this->currentRound;
    }

    /**
     * Set realUsersOutOfQualification
     *
     * @param integer $realUsersOutOfQualification
     *
     * @return TournamentPlayoff
     */
    public function setRealUsersOutOfQualification($realUsersOutOfQualification)
    {
        $this->real_users_out_of_qualification = $realUsersOutOfQualification;

        return $this;
    }

    /**
     * Get realUsersOutOfQualification
     *
     * @return integer
     */
    public function getRealUsersOutOfQualification()
    {
        return $this->real_users_out_of_qualification;
    }
}
