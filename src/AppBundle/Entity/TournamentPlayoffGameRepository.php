<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * TournamentPlayoffGameRepository
 *
 */
class TournamentPlayoffGameRepository extends EntityRepository
{
    /**
    * Возвращает результат проверки: играет ли пользователь в туре плей-офф?
    *
    * @param int $tournament_id
    * @param int $user_id
    * @param int $roundNum
    *
    * @return bool
    */
    public function isUserInPlayoffRound($tournamentId, $userId, $roundNum) {

        $q = $this->createQueryBuilder('tpg')
            ->select('COUNT(tpg.id) as cnt')
            ->leftJoin('tpg.tournament', 't')
            ->leftJoin('t.playoffs', 'tc')
            ->where('tpg.tournament = :tournament_id')
            ->setParameter('tournament_id', $tournamentId)
            ->andWhere('tpg.user_left = :user_id OR tpg.user_right = :user_id')
            ->setParameter('user_id', $userId)
            ->andWhere('tpg.round = :round')
            ->setParameter('round', $roundNum)
            ->getQuery();

        try {
            return (bool) $q->getSingleScalarResult();
        } catch (NoResultException $e) {
            return false;
        }
    }

    /**
    * Ищет результаты по турниру
    */
    public function findByTournament($tournament_id)
    {
        $q = $this->createQueryBuilder('tpg')
                  ->addSelect('t, ul, ur')
                  ->leftJoin('tpg.tournament', 't')
                  ->leftJoin('tpg.user_left', 'ul')
                  ->leftJoin('tpg.user_right', 'ur')
                  ->where('tpg.tournament = :tournament_id')
                  ->setParameter('tournament_id', $tournament_id)
                  ->orderBy('tpg.round', 'ASC')
                  ->getQuery();

        try {
            return $q->getResult();
          } catch (NoResultException $e) {
              return null;
          }
    }

    public function isUserInPlayoff($tournament_id, $user_id)
    {
        $q = $this->createQueryBuilder('tpg')
                  ->select('COUNT(tpg.id) as cnt')
                  ->where('tpg.tournament = :tournament_id')
                  ->setParameter('tournament_id', $tournament_id)
                  ->andWhere('tpg.user_left = :user_id OR tpg.user_right = :user_id')
                  ->setParameter('user_id', $user_id)
                  ->getQuery();

        try {
            $result = $q->getSingleResult();

            return (bool) $result["cnt"];

          } catch (NoResultException $e) {
              return false;
          }
    }

    public function findByTournamentAndRound($tournament_id, $round)
    {
        $q = $this->createQueryBuilder('tpg')
                  ->addSelect('t, ul, ur')
                  ->leftJoin('tpg.tournament', 't')
                  ->leftJoin('tpg.user_left', 'ul')
                  ->leftJoin('tpg.user_right', 'ur')
                  ->where('tpg.tournament = :tournament_id')
                  ->setParameter('tournament_id', $tournament_id)
                  ->andWhere('tpg.round = :round')
                  ->setParameter('round', $round)
                  ->orderBy("tpg.id", "ASC")
                  ->getQuery();

        try {
            return $q->getResult();
          } catch (NoResultException $e) {
              return null;
          }
    }
}
