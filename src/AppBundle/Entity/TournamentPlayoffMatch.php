<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AppBundle\Entity\TournamentPlayoffMatch
 *
 * @ORM\Table(name="tournaments_playoff_matches")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\TournamentPlayoffMatchRepository")
 */
class TournamentPlayoffMatch
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")     
     */
    private $id;

    /**
     * @ORM\Column(type="integer")     
     */
    private $round;

    /**
     * @ORM\ManyToOne(targetEntity="Tournament", inversedBy="playoff_matches")
     * @ORM\JoinColumn(name="tournament_id", referencedColumnName="id")
     */
    protected $tournament;

    /**
     * @ORM\ManyToOne(targetEntity="Match", inversedBy="tournament_playoff_matches")
     * @ORM\JoinColumn(name="match_id", referencedColumnName="id")
     */
    protected $match;

    /**
     * @ORM\Column(name="is_qualification_match", type="boolean")     
     */
    private $isQualificationMatch;
    
    /**
     * @ORM\Column(name="is_processed", type="boolean")
     */
    private $isProcessed;

    public function __construct()
    {
        $this->isProcessed = 0;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set round
     *
     * @param integer $round
     * @return TournamentPlayoffMatch
     */
    public function setRound($round)
    {
        $this->round = $round;
    
        return $this;
    }

    /**
     * Get round
     *
     * @return integer 
     */
    public function getRound()
    {
        return $this->round;
    }

    /**
     * Set tournament
     *
     * @param \AppBundle\Entity\Tournament $tournament
     * @return TournamentPlayoffMatch
     */
    public function setTournament(\AppBundle\Entity\Tournament $tournament = null)
    {
        $this->tournament = $tournament;
    
        return $this;
    }

    /**
     * Get tournament
     *
     * @return \AppBundle\Entity\Tournament 
     */
    public function getTournament()
    {
        return $this->tournament;
    }

    /**
     * Set match
     *
     * @param \AppBundle\Entity\Match $match
     * @return TournamentPlayoffMatch
     */
    public function setMatch(\AppBundle\Entity\Match $match = null)
    {
        $this->match = $match;
    
        return $this;
    }

    /**
     * Get match
     *
     * @return \AppBundle\Entity\Match 
     */
    public function getMatch()
    {
        return $this->match;
    }

    /**
     * Set isQualificationMatch
     *
     * @param boolean $isQualificationMatch
     * @return TournamentPlayoffMatch
     */
    public function setIsQualificationMatch($isQualificationMatch)
    {
        $this->isQualificationMatch = $isQualificationMatch;

        return $this;
    }

    /**
     * Get isQualificationMatch
     *
     * @return boolean 
     */
    public function getIsQualificationMatch()
    {
        return $this->isQualificationMatch;
    }

    /**
     * Set isProcessed
     *
     * @param boolean $isProcessed
     *
     * @return TournamentPlayoffMatch
     */
    public function setIsProcessed($isProcessed)
    {
        $this->isProcessed = $isProcessed;

        return $this;
    }

    /**
     * Get isProcessed
     *
     * @return boolean
     */
    public function getIsProcessed()
    {
        return $this->isProcessed;
    }
}
