<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

/**
 * TournamentPlayoffMatchRepository
 *
 */
class TournamentPlayoffMatchRepository extends EntityRepository
{
    /**
    * Возвращает матчи квалификации по ID турнира
    *
    * @param int $tournament_id
    * @return array
    */
    public function findQualificationByTournament($tournamentId)
    {

        $query = $this->createQueryBuilder('tm')
                      ->select('tm, m, l, ht, gt')
                      ->leftJoin('tm.tournament', 't')
                      ->leftJoin('tm.match', 'm', Join::WITH, 'm.id = tm.match')
                      ->leftJoin('m.league', 'l')
                      ->leftJoin('m.home_team', 'ht')
                      ->leftJoin('m.guest_team', 'gt')
                      ->where('t.id = :tournament_id')
                      ->setParameter('tournament_id', $tournamentId)
                      ->andWhere('tm.isQualificationMatch = :isQualificationMatch')
                      ->setParameter('isQualificationMatch', true)
                      ->groupBy('tm.id')
                      ->orderBy('tm.round', 'ASC')
                      ->addOrderBy('m.match_date', 'ASC')
                      ->addOrderBy('m.match_time', 'ASC')
                      ->addOrderBy('m.id', 'ASC')
                      ->getQuery();

        try {
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    /**
    * Возвращает матчи плей-офф по ID турнира
    *
    * @param int $tournament_id
    * @return array
    */
    public function findPlayoffByTournament($tournamentId)
    {
      $query = $this->createQueryBuilder('tm')
                      ->select('tm, m, l, ht, gt')
                      ->leftJoin('tm.tournament', 't')
                      ->leftJoin('tm.match', 'm', Join::WITH, 'm.id = tm.match')
                      ->leftJoin('m.league', 'l')
                      ->leftJoin('m.home_team', 'ht')
                      ->leftJoin('m.guest_team', 'gt')
                      ->where('t.id = :tournament_id')
                      ->setParameter('tournament_id', $tournamentId)
                      ->andWhere('tm.isQualificationMatch = :isQualificationMatch')
                      ->setParameter('isQualificationMatch', false)
                      ->groupBy('tm.id')
                      ->orderBy('tm.round', 'ASC')
                      ->addOrderBy('m.match_date', 'ASC')
                      ->addOrderBy('m.match_time', 'ASC')
                      ->addOrderBy('m.id', 'ASC')
                      ->getQuery();

        try {
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    /**
    * Возвращает матчи плей-офф по ID турнира и номеру тура
    *
    * @param int $tournamentId ID турнира
    * @param int $roundNum     Номер тура
    *
    * @return TournamentPlayoffMatch[]
    */
    public function findPlayoffByTournamentAndRound($tournamentId, $roundNum)
    {
        $query = $this->createQueryBuilder('tm')
            ->select('tm, m, l, ht, gt')
            ->leftJoin('tm.tournament', 't')
            ->leftJoin('tm.match', 'm', Join::WITH, 'm.id = tm.match')
            ->leftJoin('m.league', 'l')
            ->leftJoin('m.home_team', 'ht')
            ->leftJoin('m.guest_team', 'gt')
            ->where('t.id = :tournament_id')
            ->setParameter('tournament_id', $tournamentId)
            ->andWhere('tm.round = :roundNum')
            ->setParameter('roundNum', $roundNum)
            ->andWhere('tm.isQualificationMatch = :isQualificationMatch')
            ->setParameter('isQualificationMatch', false)
            ->groupBy('tm.id')
            ->addOrderBy('m.match_date', 'ASC')
            ->addOrderBy('m.match_time', 'ASC')
            ->addOrderBy('m.id', 'ASC')
            ->getQuery();

        try {
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    /**
    * Возвращает дату и время первого матча в турнире
    *
    * @param $id ID турнира
    * @return string Дата/Время первого матча в турнире в формате Y-m-d H:i:s
    */
    public function getMinStartDatetime($id)
    {
        $q = $this->createQueryBuilder('tm')
                  ->select('DATE_FORMAT(MIN(CONCAT(m.match_date, \' \',  m.match_time)), \'%Y-%m-%d %H:%i:%s\')')
                  ->leftJoin('tm.tournament', 't')
                  ->leftJoin('tm.match', 'm')
                  ->where('t.id = :id')
                  ->setParameter('id', $id)
                  ->getQuery();

        try {
            return $q->getSingleScalarResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    /**
    * Возвращает количество матчей в турнире
    *
    * @param int $tournament_id ID турнира
    * @return int
    */
    public function findMathesCountByTournament($tournament_id)
    {
        $q = $this->createQueryBuilder('tm')
                ->select("COUNT(tm.id)")
                ->where('tm.tournament = :tournament_id')
                ->setParameter('tournament_id', $tournament_id)
                ->getQuery();

        try {
            return $result = $q->getSingleScalarResult();
        } catch (NoResultException $e) {
            return 0;
        }
    }

    /**
    * Поиск уже начавшихся матчей в турнире
    *
    * @param int $id ID турнира
    * @return \AppBundle\Entity\TournamentPlayoffMatch[]
    */
    public function findStartedByTournament($id)
    {
        $q = $this->createQueryBuilder('tm')
                    ->leftJoin('tm.match', 'm')
                    ->where('m.isStarted = 1')
                    ->andWhere('tm.tournament = :id')
                    ->setParameter('id', $id)
                    ->getQuery();

        try {
            return $q->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    /**
    * Проверяет есть ли матч в турнире
    *
    * @param int $tournamentId ID турнира
    * @param int $matchId      ID матча
    *
    * @return bool
    */
    public function isMatchInTournament($tournamentId, $matchId)
    {
        $query = $this->createQueryBuilder('tm')
            ->select("COUNT(tm.id) AS CNT")
            ->where('tm.tournament = :tournament_id')
            ->setParameter('tournament_id', $tournamentId)
            ->andWhere('tm.match = :match_id')
            ->setParameter('match_id', $matchId)
            ->getQuery();

        try {
            return (bool) $query->getSingleScalarResult();
        } catch (NoResultException $e) {
            return false;
        }
    }

    public function isMatchInTournamentAndRound($tournament_id, $match_id, $round_num, $qualificationMatch)
    {
        $query = $this->createQueryBuilder('tm')
                ->select("COUNT(tm.id) AS CNT")
                ->where('tm.tournament = :tournament_id')
                ->setParameter('tournament_id', $tournament_id)
                ->andWhere('tm.match = :match_id')
                ->setParameter('match_id', $match_id)
                ->andWhere('tm.round = :round_num')
                ->setParameter('round_num', $round_num)
                ->andWhere("tm.isQualificationMatch = :isQualificationMatch")
                ->setParameter("isQualificationMatch", $qualificationMatch)
                ->getQuery();

        try {
            $result = $query->getSingleResult();
            return (bool) $result["CNT"];
        } catch (NoResultException $e) {
            return false;
        }
    }

    /**
    * Находит матч турнира по id турнира и матча
    */
    public function findByTournamentAndMatch($tournament_id, $match_id)
    {
        $q = $this->createQueryBuilder('tm')
                ->where('tm.tournament = :tournament_id')
                ->andWhere('tm.match = :match_id')
                ->setParameter('tournament_id', $tournament_id)
                ->setParameter('match_id', $match_id)
                ->getQuery();

            try {
                    return $q->getSingleResult();
                } catch (NoResultException $e) {

                    return null;
                }
    }

    /**
    * Проверяет больше или равна переданная дата чем дата и время последнего матча в предыдущем туре
    *
    * @param int $tournament_id
    * @param string $match_date
    * @param string $match_time
    * @param int $round
    */
    public function isMatchDatetimeMoreThanInPreviousRounds($tournament_id, $match_date, $match_time, $round, $qualificationMatch)
    {
        if($qualificationMatch == 1)
            $leftJoinCondition = 'tm.round < :round AND tm.isQualificationMatch = 1';
        else
            $leftJoinCondition = '(tm.round < :round AND tm.isQualificationMatch = 0) OR tm.isQualificationMatch = 1';

        $q = $this->createQueryBuilder('tm')
                  ->select('DATE_FORMAT(MAX(CONCAT(m.match_date, \' \',  m.match_time)), \'%Y-%m-%d %H:%i:%s\') as max_start_datetime')
                  ->leftJoin('tm.tournament', 't')
                  ->leftJoin('tm.match', 'm', 'WITH', $leftJoinCondition)
                  ->setParameter('round', $round)
                  ->where('t.id = :tournament_id')
                  ->setParameter('tournament_id', $tournament_id)
                  ->getQuery();

        try {
            $result = $q->getSingleResult();

            if(!$result["max_start_datetime"])
                return true;

            $maxStartDatetimeInPreviousRounds = new \DateTime($result["max_start_datetime"]);
            $matchDatetime = new \DateTime($match_date->format("Y-m-d")." ".$match_time->format("H:i:s"));

            if($maxStartDatetimeInPreviousRounds->format('U') <= $matchDatetime->format('U'))
                return true;
        } catch (NoResultException $e) {
              return true;
        }

        return false;
    }

    /**
    * Проверяет меньше или равна переднная дата чем дата и время последнего матча в следующем туре
    *
    * @param int $tournament_id
    * @param string $match_date
    * @param string $match_time
    * @param int $round
    */
    public function isMatchDatetimeLessThanInNextRounds($tournament_id, $match_date, $match_time, $round, $qualificationMatch)
    {
        if($qualificationMatch == 1)
            $leftJoinCondition = 'tm.round > :round AND tm.isQualificationMatch = 1';
        else
            $leftJoinCondition = 'tm.round > :round AND tm.isQualificationMatch = 0';

        $q = $this->createQueryBuilder('tm')
                  ->select('DATE_FORMAT(MIN(CONCAT(m.match_date, \' \',  m.match_time)), \'%Y-%m-%d %H:%i:%s\') as min_start_datetime')
                  ->leftJoin('tm.tournament', 't')
                  ->leftJoin('tm.match', 'm', 'WITH', $leftJoinCondition)
                  ->setParameter('round', $round)
                  ->where('t.id = :tournament_id')
                  ->setParameter('tournament_id', $tournament_id)
                  ->getQuery();

        try {
            $result = $q->getSingleResult();

            if(!$result["min_start_datetime"])
                return true;

            $minStartDatetimeInNextRounds = new \DateTime($result["min_start_datetime"]);
            $matchDatetime = new \DateTime($match_date->format("Y-m-d")." ".$match_time->format("H:i:s"));

            if($minStartDatetimeInNextRounds->format('U') >= $matchDatetime->format('U'))
                return true;

        } catch (NoResultException $e) {
              return true;
        }

        return false;
    }

    public function findByTournamentAndRound($tournament_id, $round, $isQualificationMatch)
    {
        $q = $this->createQueryBuilder('tm')
                ->where('tm.tournament = :tournament_id')
                ->andWhere('tm.round = :round')
                ->andWhere('tm.isQualificationMatch = :isQualificationMatch')
                ->setParameter('tournament_id', $tournament_id)
                ->setParameter('round', $round)
                ->setParameter('isQualificationMatch', $isQualificationMatch)
                ->getQuery();

            try {
                    return $q->getResult();
                } catch (NoResultException $e) {

                    return null;
                }
    }

    public function findPlayoffMatches($tournament_id)
    {
        $q = $this->createQueryBuilder('tm')
                ->where('tm.tournament = :tournament_id')
                ->andWhere('tm.round >= 1')
                ->andWhere('tm.isQualificationMatch = 0')
                ->setParameter('tournament_id', $tournament_id)
                ->getQuery();

            try {
                    return $q->getResult();
                } catch (NoResultException $e) {

                    return null;
                }
    }

    public function removeUnneededMatchesFromPlayoffByTournament($tournament_id, $deleteFromRoundNum)
    {
        $dql = "DELETE FROM AppBundle\Entity\TournamentPlayoffMatch tm
                    WHERE tm.tournament = $tournament_id
                      AND tm.round > $deleteFromRoundNum
                      AND tm.isQualificationMatch = 0";

        $em = $this->getEntityManager();
        $query = $em->createQuery($dql);
        $query->execute();
    }

    public function findPlayoffMatchesAndBetsByTournamentAndRound($tournament_id, $round)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT tcm, m, ht, gt, b
                                    FROM AppBundle\Entity\TournamentPlayoffMatch tcm
                                    LEFT JOIN tcm.tournament t
                                    LEFT JOIN tcm.match m
                                    LEFT JOIN m.home_team ht
                                    LEFT JOIN m.guest_team gt
                                    LEFT JOIN m.bets b WITH t.id = b.tournament
                                    WHERE tcm.tournament = :tournament_id
                                    AND tcm.round = :round
                                    AND tcm.isQualificationMatch = 0
                                    AND tcm.isProcessed = 1
                                    AND b.points IS NOT NULL
                                    AND
                                    (b.user IN (
                                        SELECT u1.id
                                            FROM AppBundle\Entity\TournamentPlayoffGame tgr1
                                            LEFT JOIN tgr1.user_left u1
                                            WHERE tgr1.tournament = :tournament_id
                                    )
                                    OR
                                    b.user IN (
                                        SELECT u2.id
                                            FROM AppBundle\Entity\TournamentPlayoffGame tgr2
                                            LEFT JOIN tgr2.user_right u2
                                            WHERE tgr2.tournament = :tournament_id
                                    )
                                    )
                                    ")
                    ->setParameter("tournament_id", $tournament_id)
                    ->setParameter("round", $round);
        try {
            return  $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    public function removeUnneededMatchesAndBets($tournament_id, $playoffRoundNum)
    {
        $em = $this->getEntityManager();

        $q = $this->createQueryBuilder('tm')
                  ->select('tm, t')
                  ->leftJoin('tm.tournament', 't')
                  ->where('t.id = :tournament_id')
                  ->setParameter('tournament_id', $tournament_id)
                  ->andWhere('(tm.round > :playoffRoundNum AND tm.isQualificationMatch = 0)')
                  ->setParameter('playoffRoundNum', $playoffRoundNum)
                  ->groupBy('tm.match')
                  ->getQuery();

        try {
            $result = $q->getResult();
        }
         catch (NoResultException $e) {
              return null;
        }

        if(empty($result))
            return null;

        $matchesIds = array();
        foreach($result as $match)
        {
            $matchesIds[] = $match->getMatch()->getId();
            $em->remove($match);
        }
        $em->flush();

        $dql = "DELETE FROM AppBundle\Entity\Bet b
                    WHERE b.tournament = $tournament_id
                    AND b.match IN (".implode(', ', $matchesIds).")";


        $query = $em->createQuery($dql);
        $query->execute();
        $em->flush();
    }

    public function findNonProcessedMatchesFinished($tournament_id, $round, $isQualificationMatch)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT m.id
                                    FROM AppBundle\Entity\TournamentPlayoffMatch tcm
                                    LEFT JOIN tcm.tournament t
                                    LEFT JOIN t.playoffs tc
                                    LEFT JOIN tcm.match m
                                    WHERE t.id = :tournament_id
                                        AND tcm.round = :round
                                        AND tcm.isQualificationMatch = :isQualificationMatch
                                        AND m.isStarted = 1
                                        AND (m.isOver = 1 OR m.isRejected = 1)
                                        AND tcm.isProcessed = 0")
                    ->setParameter("tournament_id", $tournament_id)
                    ->setParameter("round", $round)
                    ->setParameter("isQualificationMatch", $isQualificationMatch);

        try {
            $result = $query->getResult();

            if(count($result) != 0)
                return $result;
            else
                return null;
        } catch (NoResultException $e) {
            return null;
        }
    }

    public function allBetsCalculated($tournament_id, $matches_ids)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT COUNT(b.id) as total_bets
                                    FROM AppBundle\Entity\Bet b
                                    WHERE b.tournament = :tournament_id AND b.match IN (:matches_ids)
                                    ")
                    ->setParameter("tournament_id", $tournament_id)
                    ->setParameter("matches_ids", $matches_ids);

        try {
            $result = $query->getSingleResult();
            $totalBets = $result["total_bets"];
        } catch (NoResultException $e) {
            return false;
        }

        if($totalBets == 0)
            return true;

        $query = $em->createQuery("SELECT COUNT(b.id) as calculated_bets
                                    FROM AppBundle\Entity\Bet b
                                    WHERE b.tournament = :tournament_id AND b.points IS NOT NULL AND b.match IN (:matches_ids)
                                    ")
                    ->setParameter("tournament_id", $tournament_id)
                    ->setParameter("matches_ids", $matches_ids);

        try {
            $result = $query->getSingleResult();
            $calculatedBets = $result["calculated_bets"];
        } catch (NoResultException $e) {
            return false;
        }

        if($totalBets == $calculatedBets)
            return true;
        else
            return false;
    }

    public function findByIds($tournament_id, $matches_ids)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT tm, m
                                    FROM AppBundle\Entity\TournamentPlayoffMatch tm
                                        LEFT JOIN tm.match m WITH m.id = tm.match
                                    WHERE tm.tournament = :tournament_id AND m.id IN (:matches_ids)")
                    ->setParameter("tournament_id", $tournament_id)
                    ->setParameter("matches_ids", $matches_ids);
        try {
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    public function allRoundMatchesFinished($tournament_id, $round, $isQualificationMatch)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT COUNT(m.id) as matches_finished, tc.matches_in_qualification, tc.matches_in_game
                                    FROM AppBundle\Entity\TournamentPlayoffMatch tcm
                                    LEFT JOIN tcm.tournament t
                                    LEFT JOIN t.playoffs tc
                                    LEFT JOIN tcm.match m
                                    WHERE t.id = :tournament_id
                                        AND tcm.round = :round
                                        AND tcm.isQualificationMatch = :isQualificationMatch
                                        AND m.isStarted = 1
                                        AND (m.isOver = 1 OR m.isRejected = 1)")
                    ->setParameter("tournament_id", $tournament_id)
                    ->setParameter("round", $round)
                    ->setParameter("isQualificationMatch", $isQualificationMatch);

        try {
            $result = $query->getSingleResult();
            if($isQualificationMatch)
                return (intval($result["matches_finished"]) === intval($result["matches_in_qualification"]));
            else
                return (intval($result["matches_finished"]) === intval($result["matches_in_game"]));
        } catch (NoResultException $e) {
            return false;
        }
    }

    public function matchesPointsCalculated($tournament_id, $round, $isQualificationMatch)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT COUNT(b.id) as bets_without_points
                                    FROM AppBundle\Entity\Bet b
                                    WHERE b.points IS NULL AND b.match IN (
                                        SELECT m.id
                                            FROM AppBundle\Entity\TournamentPlayoffMatch tcm
                                            LEFT JOIN tcm.match m
                                            WHERE tcm.tournament = :tournament_id
                                                AND tcm.round = :round
                                                AND tcm.isQualificationMatch = :isQualificationMatch
                                    )
                                    ")
                    ->setParameter("tournament_id", $tournament_id)
                    ->setParameter("round", $round)
                    ->setParameter("isQualificationMatch", $isQualificationMatch);

        try {
            $result = $query->getSingleResult();
            return (intval($result["bets_without_points"]) === 0);
        } catch (NoResultException $e) {
            return false;
        }
    }

    /**
    * Поиск плейофф матчей турнира по ID турнира
    *
    * @param int $tournament_id
    * @return array
    */
    public function findIdsPlayoffNextRoundByTournament($tournament_id, $round)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT m.id
                                    FROM AppBundle\Entity\TournamentPlayoffMatch tm
                                    LEFT JOIN tm.match m
                                    WHERE tm.tournament = :tournament_id
                                    AND tm.round >= :round
                                    AND tm.isQualificationMatch = 0")
                    ->setParameter("tournament_id", $tournament_id)
                    ->setParameter("round", $round);

        try {
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }
}
