<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AppBundle\Entity\TournamentPlayoffQualificationRealTimeResult
 *
 * @ORM\Table(name="tournaments_playoff_qualification_realtime_results")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\TournamentPlayoffQualificationRealTimeResultRepository")
 */
class TournamentPlayoffQualificationRealTimeResult
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")     
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Tournament", inversedBy="playoff_qualification_rt_results")
     * @ORM\JoinColumn(name="tournament_id", referencedColumnName="id")
     */
    protected $tournament;   
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="tournaments_playoff_qualification_rt_results")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;        
    
    /**
     * @ORM\Column(type="integer")     
     */
    private $place;
    
    /**
     * @ORM\Column(type="float")     
     */
    private $total_points;    
    
    /**
     * @ORM\Column(type="integer")     
     */
    private $total_guessed_results;
    
    /**
     * @ORM\Column(type="integer")     
     */
    private $total_guessed_outcomes;
    
    /**
     * @ORM\Column(type="integer")     
     */
    private $total_guessed_differences;

    /**
     * @ORM\Column(type="float")
     */
    private $user_rating;

    public function __construct()
    {
        $this->total_points = 0;
        $this->total_guessed_results = 0;
        $this->total_guessed_differences = 0;
        $this->total_guessed_outcomes = 0;
    }        

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set place
     *
     * @param integer $place
     *
     * @return TournamentPlayoffQualificationRealTimeResult
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return integer
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set totalPoints
     *
     * @param float $totalPoints
     *
     * @return TournamentPlayoffQualificationRealTimeResult
     */
    public function setTotalPoints($totalPoints)
    {
        $this->total_points = $totalPoints;

        return $this;
    }

    /**
     * Get totalPoints
     *
     * @return float
     */
    public function getTotalPoints()
    {
        return $this->total_points;
    }

    /**
     * Set totalGuessedResults
     *
     * @param integer $totalGuessedResults
     *
     * @return TournamentPlayoffQualificationRealTimeResult
     */
    public function setTotalGuessedResults($totalGuessedResults)
    {
        $this->total_guessed_results = $totalGuessedResults;

        return $this;
    }

    /**
     * Get totalGuessedResults
     *
     * @return integer
     */
    public function getTotalGuessedResults()
    {
        return $this->total_guessed_results;
    }

    /**
     * Set totalGuessedOutcomes
     *
     * @param integer $totalGuessedOutcomes
     *
     * @return TournamentPlayoffQualificationRealTimeResult
     */
    public function setTotalGuessedOutcomes($totalGuessedOutcomes)
    {
        $this->total_guessed_outcomes = $totalGuessedOutcomes;

        return $this;
    }

    /**
     * Get totalGuessedOutcomes
     *
     * @return integer
     */
    public function getTotalGuessedOutcomes()
    {
        return $this->total_guessed_outcomes;
    }

    /**
     * Set totalGuessedDifferences
     *
     * @param integer $totalGuessedDifferences
     *
     * @return TournamentPlayoffQualificationRealTimeResult
     */
    public function setTotalGuessedDifferences($totalGuessedDifferences)
    {
        $this->total_guessed_differences = $totalGuessedDifferences;

        return $this;
    }

    /**
     * Get totalGuessedDifferences
     *
     * @return integer
     */
    public function getTotalGuessedDifferences()
    {
        return $this->total_guessed_differences;
    }

    /**
     * Set userRating
     *
     * @param float $userRating
     *
     * @return TournamentPlayoffQualificationRealTimeResult
     */
    public function setUserRating($userRating)
    {
        $this->user_rating = $userRating;

        return $this;
    }

    /**
     * Get userRating
     *
     * @return float
     */
    public function getUserRating()
    {
        return $this->user_rating;
    }

    /**
     * Set tournament
     *
     * @param \AppBundle\Entity\Tournament $tournament
     *
     * @return TournamentPlayoffQualificationRealTimeResult
     */
    public function setTournament(\AppBundle\Entity\Tournament $tournament = null)
    {
        $this->tournament = $tournament;

        return $this;
    }

    /**
     * Get tournament
     *
     * @return \AppBundle\Entity\Tournament
     */
    public function getTournament()
    {
        return $this->tournament;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return TournamentPlayoffQualificationRealTimeResult
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
