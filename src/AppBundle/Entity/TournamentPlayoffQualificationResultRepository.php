<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

class TournamentPlayoffQualificationResultRepository extends EntityRepository
{
    /**
    * Ищет результаты по турниру
    */
    public function findByTournament($tournament_id)
    {
        $q = $this->createQueryBuilder('tcr')
                  ->addSelect('t, u')
                  ->leftJoin('tcr.tournament', 't')
                  ->leftJoin('tcr.user', 'u')
                  ->where('tcr.tournament = :tournament_id')
                  ->setParameter('tournament_id', $tournament_id)
                  ->orderBy('tcr.round', 'ASC')
                  ->addOrderBy('tcr.total_points', 'DESC')
                  ->addOrderBy('tcr.total_guessed_results', 'DESC')
                  ->addOrderBy('tcr.total_guessed_outcomes', 'DESC')
                  ->addOrderBy('tcr.total_guessed_differences', 'DESC')
                  //необходимо сортировать по рейтингу
                  ->addOrderBy('u.id', 'ASC')
                  ->getQuery();
                  
        try {            
            return $q->getResult();
          } catch (NoResultException $e) {                                                      
              return null;
          }
    }   
    
    /**
    * Ищет результаты квалификации по турниру и раунду
    * 
    * @param int $tournament_id
    * @param int $round
    * @return array
    */
    public function findByTournamentAndRound($tournament_id, $round, $maxPlace = false)
    {
      $q = $this->createQueryBuilder('tcr')
                  ->addSelect('t, u')
                  ->leftJoin('tcr.tournament', 't')
                  ->leftJoin('tcr.user', 'u')
                  ->where('tcr.tournament = :tournament_id')
                  ->setParameter('tournament_id', $tournament_id)
                  ->andWhere('tcr.round = :round')
                  ->setParameter('round', $round)                
                  ->orderBy('tcr.place', 'ASC');
      
      if($maxPlace)
      {
        $q = $q->andWhere("tcr.place <= :maxPlace")->setParameter("maxPlace", $maxPlace);
      }
            
      $q = $q->getQuery();      
                          
        try {            
            return $q->getResult();
          } catch (NoResultException $e) {                                                      
              return null;
          }
    }
    
    /**
    * Ищет пользователей не проходящих в плейофф из квалификации по турниру и раунду
    * 
    * @param int $tournament_id
    * @param int $round
    * @return array
    */
    public function findByTournamentUsersIdsNotInPlayoff($tournament_id, $lastRound, $maxPlace)
    {        
        $em = $this->getEntityManager();
        
        $q = $this->createQueryBuilder('tcr')
                  ->addSelect('u')
                  ->leftJoin('tcr.tournament', 't')
                  ->leftJoin('tcr.user', 'u')
                  ->where('tcr.tournament = :tournament_id')
                  ->setParameter('tournament_id', $tournament_id)
                  ->andWhere('tcr.round = :round')
                  ->setParameter('round', $lastRound)                
                  ->andWhere('tcr.place > :maxPlace')
                  ->setParameter('maxPlace', $maxPlace)
                  ->orderBy('tcr.place', 'ASC')
                  ->getQuery();                
        
        try {            
            $results = $q->getResult();
            $users = array();
            foreach($results as $result)
                $users[] = $result->getUser()->getId();
            
            return $users;
          } catch (NoResultException $e) {                                                      
              return null;
          }                    
    }
}
