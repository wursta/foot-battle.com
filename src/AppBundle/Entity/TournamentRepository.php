<?php
namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

use AppBundle\Services\TournamentsHelper;

class TournamentRepository extends FBRepository
{
    /**
    * Поиск турниров
    *
    * @param array $arOrder     Массив с сортировкой.
    * @param array $arFilter    Массив с фильтрами.
    * Может принимать дополнительно следующие ключи помимо ключей сущности Tournament:
    *   format - Формат турнира (internal name)
    *   priceFrom - Минимальная цена входа
    *   priceTo - Максимальная цена входа
    *   onlyPromo - Только промо-турниры
    *
    * @param int   $page        Номер страницы паджинации.
    * @param int   $rowsPerPage Размер страницы паджинации.
    *
    * @return Paginator Возвращает массив из турниров и дополнительной ифнормации.
    * Пример:
    * [ [ 0 => Tournament, usersCount => value ], [ 0 => Tournament, usersCount => value ], ... ]
    */
    public function findPaginator(array $arOrder, array $arFilter, $page, $rowsPerPage = 10)
    {
        $q = $this
            ->createQueryBuilder('t')
            ->select("t")                
            ->groupBy("t.id")
            ->orderBy('t.isStarted', 'ASC')
            ->addOrderBy("t.startDatetime", "DESC")
            ->addOrderBy('t.id', 'DESC')
            ->setFirstResult($this->getOffset($page, $rowsPerPage))
            ->setMaxResults($rowsPerPage);

        //Кастомная фильтрация
        if (isset($arFilter["format"])) {
            if (!empty($arFilter["format"])) {
                $q = $q->leftJoin("t.format", "f")
                    ->addSelect('f')
                    ->andWhere("f.internal_name = :format")->setParameter("format", $arFilter["format"]);
            }
            unset($arFilter["format"]);
        }

        if (!empty($arFilter["priceFrom"])) {
            $q = $q->andWhere("t.fee >= :priceFrom")->setParameter("priceFrom", $arFilter["priceFrom"]);
            unset($arFilter["priceFrom"]);
        }

        if (!empty($arFilter["priceTo"])) {          
            $q = $q->andWhere("t.fee <= :priceTo")->setParameter("priceTo", $arFilter["priceTo"]);
            unset($arFilter["priceTo"]);
        }

        if (!empty($arFilter["onlyPromo"])) {
            $q = $q->andWhere("t.isPromo = 1");
            unset($arFilter["onlyPromo"]);
        }

        if (isset($arFilter["status"])) {
            switch ($arFilter["status"]) {
                case TournamentsHelper::STATUS_NOT_STARTED:
                    $q = $q->andWhere('t.isStarted = 0');
                    break;

                case TournamentsHelper::STATUS_GOES:
                    $q = $q->andWhere('t.isStarted = 1 AND t.isOver = 0');
                    break;

                case TournamentsHelper::STATUS_OVER:
                    $q = $q->andWhere('t.isOver = 1');
                    break;
            }
            unset($arFilter["status"]);
        }

        if (!empty($arFilter["user_id"])) {
            $q = $q->leftJoin("t.users", "u")->addSelect('u');
            $q = $q->andWhere("u.id in (:user_id)")->setParameter("user_id", $arFilter["user_id"]);
            unset($arFilter["user_id"]);
        }

        $q = $q->addCriteria($this->createCriteriaFromFilter($arFilter));

        $paginator = $this->getPaginator($q);

        return $paginator;
    }

    /**
    * Поиск турниров по фильтру
    *
    */
    public function search($filter, $returnCount = false)
    {
        $query = $this->createQueryBuilder('t');

        if(!$returnCount)
        {
            $query = $query->select("t, g, f, COUNT(u.id) as users_count")
                            ->leftJoin("t.geography", "g")
                            ->leftJoin("t.format", "f")
                            ->leftJoin("t.users", "u")
                            ->where("(t.isReady = 1 AND (t.isReject = 0 OR t.isReject IS NULL))")
                            ->groupBy("t.id")
                            ->orderBy("t.startDatetime", "DESC")
                            ->addOrderBy('t.id', 'DESC');
        }
        else
        {
            $query = $query->select("COUNT(t.id) AS cnt")
                           ->leftJoin("t.geography", "g")
                           ->leftJoin("t.format", "f")
                           ->where("t.isReady = 1 AND (t.isReject = 0 OR t.isReject IS NULL)");
        }

        if(!empty($filter["id"]))
            $query = $query->andWhere("t.id = :tournament_id")->setParameter("tournament_id", $filter["id"]);

        if(!empty($filter["format"]))
            $query = $query->andWhere("f.internal_name = :format")->setParameter("format", $filter["format"]);

        if(!empty($filter["status"]))
        {
            if($filter["status"] == "not_started")
                $query = $query->andWhere("t.isStarted = 0");
            elseif($filter["status"] == "is_started_not_over")
                $query = $query->andWhere("t.isStarted = 1 AND t.isOver = 0");
            elseif($filter["status"] == "is_over")
                $query = $query->andWhere("t.isOver = 1");
        }

        if(!empty($filter["is_accessible"]))
        {
            $query = $query->andWhere("t.isAccessible = 1");
        }

        if(isset($filter["first_result"]))
            $query->setFirstResult(intval($filter["first_result"]));

        if(isset($filter["max_results"]))
            $query->setMaxResults(intval($filter["max_results"]));

        $query = $query->getQuery();

        try {
            if($returnCount)
            {
                $result = $query->getSingleResult();
                return $result["cnt"];
            }
            else
            {
                return $query->getResult();
            }
        } catch (NoResultException $e) {
            return null;
        }
    }

    /**
    * Находит неопубликованные турниры
    */
    public function findDraft()
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT t, g, f
                                      FROM AppBundle\Entity\Tournament t
                                        LEFT JOIN t.geography g
                                        LEFT JOIN t.format f
                                      WHERE t.isReady = 0");

        try {
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    /**
    * Находит полностью оконченные турниры
    */
    public function findFinished()
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT t, g, f, u
                                      FROM AppBundle\Entity\Tournament t
                                        LEFT JOIN t.geography g
                                        LEFT JOIN t.format f
                                        LEFT JOIN t.users u
                                      WHERE t.isReady = 1
                                          AND t.isStarted = 1
                                          AND t.isOver = 1
                                          AND t.isWinPrizeDistributed = 1
                                          AND t.isReject = 0
                                      GROUP BY t.id, u.id
                                      ORDER BY t.startDatetime DESC
                                  ");

        try {
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    /**
    * Находит доступные пользователям турниры
    *
    * @param int $start
    * @param int $count
    * @return array
    */
    public function findAccessible($start, $count)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT t, g, f, COUNT(u.id) as users_count
                                      FROM AppBundle\Entity\Tournament t
                                        LEFT JOIN t.geography g
                                        LEFT JOIN t.format f
                                        LEFT JOIN t.users u
                                      WHERE t.isAccessible = 1
                                      GROUP BY t.id
                                      ORDER BY t.startDatetime ASC
                                  ");

        $query->setFirstResult($start);
        $query->setMaxResults($count);

        try {
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    /**
    * Возвращает кол-во доступных пользователям турниров
    *
    */
    public function findAccessibleCount()
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT COUNT(t.id) as cnt
                                      FROM AppBundle\Entity\Tournament t
                                      WHERE t.isAccessible = 1
                                  ");

        try {
            $result = $query->getSingleResult();
            return $result["cnt"];
        } catch (NoResultException $e) {
            return 0;
        }
    }

    public function findCurrentByUser($user_id)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT t, g, f, COUNT(u.id) as users_count
                                      FROM AppBundle\Entity\Tournament t
                                        LEFT JOIN t.geography g
                                        LEFT JOIN t.format f
                                        LEFT JOIN t.users u
                                      WHERE t.isReady = 1 AND t.isOver = 0 AND (t.isReject = 0 OR t.isReject IS NULL) AND u.id = :user_id
                                      GROUP BY t.id
                                      ORDER BY t.startDatetime ASC
                                  ")
                                  ->setParameter("user_id", $user_id);
        try {
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    public function findFinishedByUser($user_id, $start = false, $count = false, $onlyCount = false)
    {
        $query = $this->createQueryBuilder("t")
                        ->select("t, g, f, r")
                        ->leftJoin("t.geography", "g")
                        ->leftJoin("t.format", "f")
                        ->leftJoin("t.users", "u")
                        ->leftJoin("t.results", "r", "WITH", "u.id = r.user")
                        ->where("t.isReady = 1 AND t.isOver = 1 AND t.isResultsReady = 1 AND t.isWinPrizeDistributed = 1 AND t.isReject = 0 AND u.id = :user_id")
                        ->setParameter("user_id", $user_id);

        if($start !== false && $count !== false)
        {
            $query->groupBy("t.id")->orderBy("t.endDatetime", "DESC");
            $query->setFirstResult($start);
            $query->setMaxResults($count);
        }

        if($onlyCount !== false)
        {
            $query->select("COUNT(t.id) as cnt");
        }

        $query = $query->getQuery();

        try {
            if($onlyCount !== false)
            {
                $result = $query->getSingleResult();
                return $result["cnt"];
            }
            else
            {
                return $query->getResult();
            }
        } catch (NoResultException $e) {
            return null;
        }
    }

    /**
    *   Получает внутреннее имя формата турнира
    */
    public function getFormatByTournament($id)
    {
        $q = $this->createQueryBuilder('t')
                    ->select('f.internal_name')
                    ->leftJoin('t.format', 'f')
                    ->where('t.id = :id')
                    ->setParameter('id', $id)
                    ->getQuery();

        try {
            $result = $q->getSingleResult();
            return $result["internal_name"];
        } catch (NoResultException $e) {

            return null;
        }
    }

    /**
    *   Проверяет являетя ли пользователь участником турнира
    */
    public function isUserInTournament($tournament_id, $user_id)
    {
        $q = $this
                ->createQueryBuilder('t')
                ->select('COUNT(t.id) as cnt')
                ->leftJoin('t.users', 'u')
                ->where('t.id = :tournament_id')
                ->andWhere('u.id = :user_id')
                ->setParameter('tournament_id', $tournament_id)
                ->setParameter('user_id', $user_id)
                ->getQuery();

            try {
                    $result = $q->getSingleResult();
                    return (bool) $result["cnt"];
                } catch (NoResultException $e) {

                    return false;
                }
    }

    /**
    *   Проверяет не посылал ли уже пользователь заявку на турнир
    */

    public function requestWasSent($tournament_id, $user_id)
    {
        $q = $this->createQueryBuilder('t')
                ->select('COUNT(t.id) as cnt')
                ->leftJoin('t.user_requests', 'u')
                ->where('t.id = :tournament_id')
                ->andWhere('u.id = :user_id')
                ->setParameter('tournament_id', $tournament_id)
                ->setParameter('user_id', $user_id)
                ->getQuery();

        try {
            $result = $q->getSingleResult();
            return (bool) $result["cnt"];
        } catch (NoResultException $e) {

            return false;
        }
    }


    /**
    *   Ищет турниры по флагу "isReady"
    */
    public function findByIsReady($isReady)
    {
        $q = $this->createQueryBuilder('t')
                  ->addSelect('g, f, u')
                  ->leftJoin('t.geography', 'g')
                  ->leftJoin('t.format', 'f')
                  ->leftJoin('t.users', 'u')
                  ->where("t.isReady = :isReady")
                  ->setParameter("isReady", $isReady)
                  ->groupBy('t.id')
                  ->addGroupBy('u.id')
                  ->orderBy('t.startDatetime', 'ASC')
                  ->getQuery();

        try {
            return $q->getResult();
        } catch (NoResultException $e) {

            return null;
        }
    }

    /**
    *   Ищет турниры по флагу "isReject"
    */
    public function findByisReject($isReady)
    {
        $q = $this->createQueryBuilder('t')
                  ->addSelect('g, f, u')
                  ->leftJoin('t.geography', 'g')
                  ->leftJoin('t.format', 'f')
                  ->leftJoin('t.users', 'u')
                  ->where("t.isReject = :isReject")
                  ->setParameter("isReject", $isReady)
                  ->groupBy('t.id')
                  ->addGroupBy('u.id')
                  ->orderBy('t.startDatetime', 'ASC')
                  ->getQuery();

        try {
            return $q->getResult();
        } catch (NoResultException $e) {

            return null;
        }
    }

    public function findByIsReadyAndIsOver($isReady, $isOver)
    {
        $q = $this->createQueryBuilder('t')
                  ->addSelect('g, f, u')
                  ->leftJoin('t.geography', 'g')
                  ->leftJoin('t.format', 'f')
                  ->leftJoin('t.users', 'u')
                  ->where("t.isReady = :isReady")
                  ->setParameter("isReady", $isReady)
                  ->andWhere("t.isOver = :isOver")
                  ->setParameter("isOver", $isOver)
                  ->andWhere("t.isReject = 0 OR t.isReject IS NULL")
                  ->groupBy('t.id')
                  ->addGroupBy('u.id')
                  ->orderBy('t.startDatetime', 'ASC')
                  ->getQuery();

        try {
            return $q->getResult();
        } catch (NoResultException $e) {

            return null;
        }
    }

    /**
    *   Ищет турниры по флагу "IsStarted" и IsOver
    */
    public function findByIsStartedAndIsOver($isStarted, $isOver)
    {
        $q = $this->createQueryBuilder('t')
                  ->addSelect('g, f, u')
                  ->leftJoin('t.geography', 'g')
                  ->leftJoin('t.format', 'f')
                  ->leftJoin('t.users', 'u')
                  ->where("t.isStarted = :isStarted")
                  ->setParameter("isStarted", $isStarted)
                  ->andWhere("t.isOver = :isOver")
                  ->setParameter("isOver", $isOver)
                  ->andWhere("t.isReject = 0 OR t.isReject IS NULL")
                  ->groupBy('t.id')
                  ->addGroupBy('u.id')
                  ->orderBy('t.startDatetime', 'ASC')
                  ->addOrderBy('t.id', 'ASC')
                  ->getQuery();

        try {
            return $q->getResult();
        } catch (NoResultException $e) {

            return null;
        }
    }

    /**
    *   Ищет турниры по флагу "isOver"
    */
    public function findByIsOver($isOver)
    {
        $q = $this->createQueryBuilder('t')
                  ->addSelect('g, f, u')
                  ->leftJoin('t.geography', 'g')
                  ->leftJoin('t.format', 'f')
                  ->leftJoin('t.users', 'u')
                  ->where("t.isOver = :isOver")
                  ->setParameter("isOver", $isOver)
                  ->groupBy('t.id')
                  ->addGroupBy('u.id')
                  ->orderBy('t.startDatetime', 'ASC')
                  ->addOrderBy('t.id', 'ASC')
                  ->getQuery();

        try {
            return $q->getResult();
        } catch (NoResultException $e) {

            return null;
        }
    }

    /**
    * Поиск турниров в которых пользователь принимал участие с фильтрацией по окончанию
    *
    * @param int $user_id
    * @param bool $isOver
    * @return array
    */
    public function findTournamentsByUserAndIsOver($user_id, $isOver)
    {
        $q = $this->createQueryBuilder('t')
                ->leftJoin('t.users', 'u')
                ->where('u.id = :user_id')
                ->setParameter('user_id', $user_id)
                ->andWhere('t.isOver = :isOver')
                ->setParameter('isOver', $isOver)
                ->andWhere("t.isReject = 0 OR t.isReject IS NULL")
                ->orderBy("t.startDatetime", "DESC")
                ->groupBy('t.id')
                ->getQuery();

        try {
                return $q->getResult();
            } catch (NoResultException $e) {
                return null;
            }
    }

    /**
    *   Находит турниры которые нужно стартовать
    */
    public function findNeedsToStart($startDatetime)
    {
        $q = $this->createQueryBuilder('t')
                    ->addSelect('f')
                    ->leftJoin('t.format', 'f')
                    ->where('t.isReady = 1 AND t.isStarted = 0 AND t.isOver = 0 AND t.isReject = 0 OR t.isReject IS NULL')
                    ->andWhere('t.startDatetime <= :startDatetime AND t.startDatetime IS NOT NULL')
                    ->setParameter("startDatetime", $startDatetime)
                    ->groupBy('t.id')
                    ->getQuery();

        try {
                return $q->getResult();
            } catch (NoResultException $e) {

                return null;
            }
    }

    /**
    * Находит турниры оконченные турниры без результата
    *
    */
    public function findFinishedWithoutResults()
    {
        $q = $this->createQueryBuilder('t')
                ->where('t.isReady = 1 AND t.isStarted = 1 AND t.isOver = 1 AND t.isResultsReady = 0')
                ->getQuery();

        try {
                return $q->getResult();
            } catch (NoResultException $e) {

                return null;
            }
    }

    public function getSummaryPointForUserByTournament($id, $user_id)
    {
        $q = $this->createQueryBuilder('t')
                ->select('SUM(bets.points) as sum_point')
                ->leftJoin('t.users', 'users')
                ->leftJoin('t.bets', 'bets', 'WITH', 'users.id = bets.user')
                ->where('t.id = :id')
                ->andWhere('users.id = :user_id')
                ->setParameter('user_id', $user_id)
                ->setParameter('id', $id)
                ->getQuery();
        try {
                $result = $q->getSingleResult();
                if(!empty($result["sum_point"]))
                    return (float) $result["sum_point"];
                else
                    return 0;
            } catch (NoResultException $e) {

                return 0;
            }
    }

    public function findNotStarted()
    {
         $q = $this->createQueryBuilder('t')
                ->addSelect('DATE_FORMAT(MIN(CONCAT(m.match_date, \' \',  m.match_time)), \'%d.%m.%Y %H:%i\') as start_datetime')
                ->leftJoin('t.matches', 'tm')
                ->leftJoin('tm.match', 'm')
                ->where('t.isReady = :isReady AND t.isStarted = :isStarted AND t.isOver = :isOver')
                ->setParameter('isReady', 1)
                ->setParameter('isStarted', 0)
                ->setParameter('isOver', 0)
                ->groupBy('t.id')
                ->orderBy('start_datetime', 'DESC')
                ->getQuery();

        try {
                return $q->getResult();
            } catch (NoResultException $e) {

                return null;
            }
    }

    public function findUserRequests()
    {
      $q = $this->createQueryBuilder('t')
                ->addSelect('ur')
                ->innerJoin('t.user_requests', 'ur')
                ->where('t.isReady = 1 AND t.isStarted = 0 AND t.isOver = 0 AND (t.isReject = 0 OR t.isReject IS NULL)')
                ->orderBy('t.startDatetime', 'DESC')
                ->getQuery();

      try {
          return $q->getResult();
      } catch (NoResultException $e) {

          return null;
      }
    }

    public function findReadyForWinDistribution()
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT t, tr FROM AppBundle\Entity\Tournament t
                                        LEFT JOIN t.results tr
                                        WHERE
                                              t.isReady = 1
                                          AND t.isStarted = 1
                                          AND t.isOver = 1
                                          AND t.isResultsReady = 1
                                          AND t.isWinPrizeDistributed = 0
                                          AND t.isReject = 0
                                  ");

        try {
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }
}
?>
