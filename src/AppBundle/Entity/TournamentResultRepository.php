<?php
namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

class TournamentResultRepository extends EntityRepository
{    
    public function findByTournament($tournament_id)
    {
        $q = $this->createQueryBuilder('tr')
                ->addSelect('u')
                ->leftJoin('tr.user', 'u')
                ->where('tr.tournament = :tournament_id')
                ->setParameter('tournament_id', $tournament_id)
                ->orderBy('tr.place')
                ->getQuery();
                
        try {                    
                return $q->getResult();
            } catch (NoResultException $e) {                                        
                
                return null;
            }
    }

    public function findDistributed($tournament_id)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT tr, t, u
                                      FROM AppBundle\Entity\TournamentResult tr                                        
                                        LEFT JOIN tr.tournament t
                                        LEFT JOIN tr.user u
                                      WHERE t.isWinPrizeDistributed = 1
                                        AND t.id = :tournament_id
                                      ORDER BY tr.place ASC")
                    ->setParameter('tournament_id', $tournament_id);
                   
        try {                    
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }
}