<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AppBundle\Entity\TournamentRound
 *
 * @ORM\Table(name="tournaments_round")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\TournamentRoundRepository")
 */
class TournamentRound
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")     
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Tournament", inversedBy="rounds")
     * @ORM\JoinColumn(name="tournament_id", referencedColumnName="id")
     */
    protected $tournament;    
    
    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min = 2)
     */
    protected $max_users_count;
    
    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min = 1)
     */
    protected $matches_in_game;               

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min = 1)
     */
    protected $real_rounds_count;               
    
    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min = 1)
     */
    protected $current_round;
    
    public function __construct()
    {
        $this->current_round = 1;        
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set max_users_count
     *
     * @param integer $maxUsersCount
     * @return TournamentRound
     */
    public function setMaxUsersCount($maxUsersCount)
    {
        $this->max_users_count = $maxUsersCount;
    
        return $this;
    }

    /**
     * Get max_users_count
     *
     * @return integer 
     */
    public function getMaxUsersCount()
    {
        return $this->max_users_count;
    }

    /**
     * Set matches_in_game
     *
     * @param integer $matchesInGame
     * @return TournamentRound
     */
    public function setMatchesInGame($matchesInGame)
    {
        $this->matches_in_game = $matchesInGame;
    
        return $this;
    }

    /**
     * Get matches_in_game
     *
     * @return integer 
     */
    public function getMatchesInGame()
    {
        return $this->matches_in_game;
    }

    /**
     * Set tournament
     *
     * @param \AppBundle\Entity\Tournament $tournament
     * @return TournamentRound
     */
    public function setTournament(\AppBundle\Entity\Tournament $tournament = null)
    {
        $this->tournament = $tournament;
    
        return $this;
    }

    /**
     * Get tournament
     *
     * @return \AppBundle\Entity\Tournament 
     */
    public function getTournament()
    {
        return $this->tournament;
    }

    /**
     * Set real_rounds_count
     *
     * @param integer $realRoundsCount
     * @return TournamentRound
     */
    public function setRealRoundsCount($realRoundsCount)
    {
        $this->real_rounds_count = $realRoundsCount;

        return $this;
    }

    /**
     * Get real_rounds_count
     *
     * @return integer 
     */
    public function getRealRoundsCount()
    {
        return $this->real_rounds_count;
    }

    /**
     * Set current_round
     *
     * @param integer $currentRound
     * @return TournamentRound
     */
    public function setCurrentRound($currentRound)
    {
        $this->current_round = $currentRound;

        return $this;
    }

    /**
     * Get current_round
     *
     * @return integer 
     */
    public function getCurrentRound()
    {
        return $this->current_round;
    }
}
