<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AppBundle\Entity\TournamentRoundGame
 *
 * @ORM\Table(name="tournaments_round_games")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\TournamentRoundGameRepository")
 */
class TournamentRoundGame
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")     
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Tournament", inversedBy="round_games")
     * @ORM\JoinColumn(name="tournament_id", referencedColumnName="id")
     */
    protected $tournament;

    /**
     * @ORM\Column(type="integer")     
     */
    private $round;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="tournaments_round_games_at_left")
     * @ORM\JoinColumn(name="user_left", referencedColumnName="id")
     */
    protected $user_left;   
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="tournaments_round_games_at_right")
     * @ORM\JoinColumn(name="user_right", referencedColumnName="id")
     */
    protected $user_right;   
    
    /**
     * @ORM\Column(type="float")     
     * @Assert\Range(min = 0)
     */
    protected $score_left;

    /**
     * @ORM\Column(type="float")     
     * @Assert\Range(min = 0)
     */
    protected $score_right;
    
    /**
     * @ORM\Column(type="float")     
     * @Assert\Range(min = 0)
     */
    protected $points_left;

    /**
     * @ORM\Column(type="float")     
     * @Assert\Range(min = 0)
     */
    protected $points_right;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tournament
     *
     * @param \AppBundle\Entity\Tournament $tournament
     * @return TournamentRoundGame
     */
    public function setTournament(\AppBundle\Entity\Tournament $tournament = null)
    {
        $this->tournament = $tournament;
    
        return $this;
    }

    /**
     * Get tournament
     *
     * @return \AppBundle\Entity\Tournament 
     */
    public function getTournament()
    {
        return $this->tournament;
    }

    /**
     * Set user_left
     *
     * @param \AppBundle\Entity\User $userLeft
     * @return TournamentRoundGame
     */
    public function setUserLeft(\AppBundle\Entity\User $userLeft = null)
    {
        $this->user_left = $userLeft;
    
        return $this;
    }

    /**
     * Get user_left
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUserLeft()
    {
        return $this->user_left;
    }

    /**
     * Set user_right
     *
     * @param \AppBundle\Entity\User $userRight
     * @return TournamentRoundGame
     */
    public function setUserRight(\AppBundle\Entity\User $userRight = null)
    {
        $this->user_right = $userRight;
    
        return $this;
    }

    /**
     * Get user_right
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUserRight()
    {
        return $this->user_right;
    }

    /**
     * Set round
     *
     * @param integer $round
     * @return TournamentRoundGame
     */
    public function setRound($round)
    {
        $this->round = $round;

        return $this;
    }

    /**
     * Get round
     *
     * @return integer 
     */
    public function getRound()
    {
        return $this->round;
    }

    /**
     * Set score_left
     *
     * @param integer $scoreLeft
     * @return TournamentRoundGame
     */
    public function setScoreLeft($scoreLeft)
    {
        $this->score_left = $scoreLeft;
    
        return $this;
    }

    /**
     * Get score_left
     *
     * @return integer 
     */
    public function getScoreLeft()
    {
        return $this->score_left;
    }

    /**
     * Set score_right
     *
     * @param integer $scoreRight
     * @return TournamentRoundGame
     */
    public function setScoreRight($scoreRight)
    {
        $this->score_right = $scoreRight;
    
        return $this;
    }

    /**
     * Get score_right
     *
     * @return integer 
     */
    public function getScoreRight()
    {
        return $this->score_right;
    }

    /**
     * Set points_left
     *
     * @param float $pointsLeft
     * @return TournamentRoundGame
     */
    public function setPointsLeft($pointsLeft)
    {
        $this->points_left = $pointsLeft;
    
        return $this;
    }

    /**
     * Get points_left
     *
     * @return float 
     */
    public function getPointsLeft()
    {
        return $this->points_left;
    }

    /**
     * Set points_right
     *
     * @param float $pointsRight
     * @return TournamentRoundGame
     */
    public function setPointsRight($pointsRight)
    {
        $this->points_right = $pointsRight;
    
        return $this;
    }

    /**
     * Get points_right
     *
     * @return float 
     */
    public function getPointsRight()
    {
        return $this->points_right;
    }
}
