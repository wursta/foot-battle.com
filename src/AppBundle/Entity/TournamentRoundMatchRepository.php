<?php
namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

class TournamentRoundMatchRepository extends EntityRepository
{
    /**
    * Поиск матчей турнира по ID турнира со всеми связанными объектами
    * 
    * @param int $tournamentId ID турнира
    * @return \AppBundle\Entity\TournamentRoundMatch[]
    */
    public function findByTournament($tournamentId)
    {
        $query = $this->createQueryBuilder('tm')
        ->select('tm, m, l, ht, gt')
        ->leftJoin('tm.tournament', 't')
        ->leftJoin('tm.match', 'm', Join::WITH, 'm.id = tm.match')
        ->leftJoin('m.league', 'l')
        ->leftJoin('m.home_team', 'ht')
        ->leftJoin('m.guest_team', 'gt')
        ->where('t.id = :tournament_id')
        ->setParameter('tournament_id', $tournamentId)
        ->groupBy('tm.id')
        ->orderBy('tm.round', 'ASC')
        ->addOrderBy('m.match_date', 'ASC')
        ->addOrderBy('m.match_time', 'ASC')
        ->addOrderBy('m.id', 'ASC')
        ->getQuery();

        try {
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }
    
    /**
    * Поиск матчей по ID турнира и номеру тура со всеми связанными объектами
    * 
    * @param int $tournamentId ID турнира
    * @param int $roundNum Тур
    * @return \AppBundle\Entity\TournamentRoundMatch[]
    */
    public function findByTournamentAndRound($tournamentId, $roundNum)
    {        
        $query = $this->createQueryBuilder('tm')
            ->select('tm, m, l, ht, gt')
            ->leftJoin('tm.tournament', 't')
            ->leftJoin('tm.match', 'm', Join::WITH, 'm.id = tm.match')
            ->leftJoin('m.league', 'l')
            ->leftJoin('m.home_team', 'ht')
            ->leftJoin('m.guest_team', 'gt')
            ->where('t.id = :tournament_id')            
            ->setParameter('tournament_id', $tournamentId)
            ->andWhere('tm.round = :roundNum')
            ->setParameter('roundNum', $roundNum)
            ->groupBy('tm.id')
            ->addOrderBy('m.match_date', 'ASC')
            ->addOrderBy('m.match_time', 'ASC')
            ->addOrderBy('m.id', 'ASC')
            ->getQuery();

        try {
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    /**
    * Возвращает дату и время первого матча в турнире
    * 
    * @param $id ID турнира
    * @return string Дата/Время первого матча в турнире в формате Y-m-d H:i:s
    */
    public function getMinStartDatetime($id)
    {        
        $q = $this->createQueryBuilder('tcm')
        ->select('DATE_FORMAT(MIN(CONCAT(m.match_date, \' \',  m.match_time)), \'%Y-%m-%d %H:%i:%s\')')
        ->leftJoin('tcm.tournament', 't')
        ->leftJoin('tcm.match', 'm')
        ->where('t.id = :id')
        ->setParameter('id', $id)
        ->getQuery();

        try {            
            return $q->getSingleScalarResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    /**
    * Возвращает количество матчей в турнире
    * 
    * @param int $tournament_id ID турнира
    * @return int
    */
    public function findMathesCountByTournament($tournament_id)
    {
        $q = $this->createQueryBuilder('tm')
        ->select("COUNT(tm.id)")
        ->where('tm.tournament = :tournament_id')
        ->setParameter('tournament_id', $tournament_id)
        ->getQuery();

        try {                    
            return $result = $q->getSingleScalarResult();
        } catch (NoResultException $e) {
            return 0;
        }
    }

    /**
    * Поиск уже начавшихся матчей в турнире
    * 
    * @param int $id ID турнира
    * @return \AppBundle\Entity\TournamentRoundMatch[]
    */
    public function findStartedByTournament($id)
    {
        $q = $this->createQueryBuilder('tm')
        ->leftJoin('tm.match', 'm')
        ->where('m.isStarted = 1')
        ->andWhere('tm.tournament = :id')                    
        ->setParameter('id', $id)
        ->getQuery();

        try {                    
            return $q->getResult();
        } catch (NoResultException $e) {                                        

            return null;
        }
    }

    /**
    * Проверяет есть ли матч в турнире
    * 
    * @param int $tournamentId ID турнира
    * @param int $matchId ID матча
    * @return bool
    */
    public function isMatchInTournament($tournamentId, $matchId)
    {
        $q = $this->createQueryBuilder('tm')
                  ->select("COUNT(tm.id)")
                  ->where("tm.tournament = :tournamentId")
                  ->andWhere("tm.match = :matchId")
                  ->setParameters(array("tournamentId" => $tournamentId, "matchId" => $matchId))
                  ->getQuery();
        
        try {                    
            return (bool) $q->getSingleScalarResult();
        } catch (NoResultException $e) {                                        
            return false;
        }
        
    }
    
    /**
    * Находим матчи и ставки пользователей в данном туре
    * 
    * @param mixed $tournament_id
    * @param mixed $group
    * @return array
    */
    
    public function findMatchesAndBetsByTournamentAndRound($tournament_id, $round)
    {
        $em = $this->getEntityManager();    

        $query = $em->createQuery("SELECT tcm, m, ht, gt, b
        FROM AppBundle\Entity\TournamentRoundMatch tcm
        LEFT JOIN tcm.match m
        LEFT JOIN m.home_team ht
        LEFT JOIN m.guest_team gt
        LEFT JOIN m.bets b
        WHERE tcm.tournament = :tournament_id
        AND tcm.round = :round                                    
        AND tcm.isProcessed = 1

        AND b.user IN (
        SELECT u.id
        FROM AppBundle\Entity\Tournament t
        LEFT JOIN t.users u
        WHERE t.id = :tournament_id                                            
        )
        ")
        ->setParameter("tournament_id", $tournament_id)                                        
        ->setParameter("round", $round);

    try {            
        return  $query->getResult();            
    } catch (NoResultException $e) {
        return null;
    }
    }
    

    //public function 

    public function isMatchInTournamentAndRound($tournament_id, $match_id, $round_num)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT COUNT(tm.id) as CNT
            FROM AppBundle\Entity\TournamentRoundMatch tm                                        
        WHERE tm.tournament = :tournament_id AND tm.match = :match_id AND tm.round = :round_num")
        ->setParameter("tournament_id", $tournament_id)
        ->setParameter("match_id", $match_id)
        ->setParameter("round_num", $round_num);

        try {
            $result = $query->getSingleResult();            
            return (bool) $result["CNT"];        
        } catch (NoResultException $e) {
            return false;
        }
    }

    /**
    * Находит матч турнира по id турнира и матча
    */
    public function findByTournamentAndMatch($tournament_id, $match_id)
    {
        $q = $this->createQueryBuilder('tm')
        ->where('tm.tournament = :tournament_id')
        ->andWhere('tm.match = :match_id')
        ->setParameter('tournament_id', $tournament_id)
        ->setParameter('match_id', $match_id)
        ->getQuery();

        try {                    
            return $q->getSingleResult();
        } catch (NoResultException $e) {                                        

            return null;
        }
    }

    public function findTotalPointsForUsersByRound($tournament_id)
    {
        $q = $this->createQueryBuilder('tm')
        ->select('u.id as user_id, tm.round, SUM(b.points) as total_points')
        ->leftJoin('tm.tournament', 't')
        ->leftJoin('t.users', 'u')                  
        ->leftJoin('t.bets', 'b', 'WITH', 'tm.match = b.match AND u.id = b.user')
        ->groupBy('u.id')
        ->addGroupBy('tm.round')
        ->where('t.id = :tournament_id')
        ->andWhere('b.points IS NOT NULL')
        ->setParameter('tournament_id', $tournament_id)
        ->orderBy('total_points', 'DESC')
        ->getQuery();

        try {                    
            return $q->getResult();
        } catch (NoResultException $e) {                                        

            return null;
        }
    }

    /**
    * Проверяет больше или равна переданная дата чем дата и время последнего матча в предыдущем туре
    * 
    * @param int $tournament_id
    * @param string $match_date
    * @param string $match_time
    * @param int $round
    */
    public function isMatchDatetimeMoreThanInPreviousRounds($tournament_id, $match_date, $match_time, $round)
    {
        $q = $this->createQueryBuilder('tm')
        ->select('DATE_FORMAT(MAX(CONCAT(m.match_date, \' \',  m.match_time)), \'%Y-%m-%d %H:%i:%s\') as max_start_datetime')
        ->leftJoin('tm.tournament', 't')
        ->leftJoin('tm.match', 'm', 'WITH', 'tm.round < :round')
        ->setParameter('round', $round)
        ->where('t.id = :tournament_id')
        ->setParameter('tournament_id', $tournament_id)
        ->getQuery();

        try {            
            $result = $q->getSingleResult();

            if(!$result["max_start_datetime"])
                return true;

            $maxStartDatetimeInPreviousRounds = new \DateTime($result["max_start_datetime"]);
            $matchDatetime = new \DateTime($match_date->format("Y-m-d")." ".$match_time->format("H:i:s"));    

            if($maxStartDatetimeInPreviousRounds->format('U') <= $matchDatetime->format('U'))
            return true;            
        } catch (NoResultException $e) {                            
            return true;
        }

        return false;
    }

    /**
    * Проверяет меньше или равна переднная дата чем дата и время последнего матча в следующем туре
    * 
    * @param int $tournament_id
    * @param string $match_date
    * @param string $match_time
    * @param int $round
    */
    public function isMatchDatetimeLessThanInNextRounds($tournament_id, $match_date, $match_time, $round)
    {
        $q = $this->createQueryBuilder('tm')
        ->select('DATE_FORMAT(MIN(CONCAT(m.match_date, \' \',  m.match_time)), \'%Y-%m-%d %H:%i:%s\') as min_start_datetime')
        ->leftJoin('tm.tournament', 't')
        ->leftJoin('tm.match', 'm', 'WITH', 'tm.round > :round')
        ->setParameter('round', $round)
        ->where('t.id = :tournament_id')
        ->setParameter('tournament_id', $tournament_id)
        ->getQuery();

        try {            
            $result = $q->getSingleResult();

            if(!$result["min_start_datetime"])
                return true;

            $minStartDatetimeInNextRounds = new \DateTime($result["min_start_datetime"]);
            $matchDatetime = new \DateTime($match_date->format("Y-m-d")." ".$match_time->format("H:i:s"));

            if($minStartDatetimeInNextRounds->format('U') >= $matchDatetime->format('U'))
            return true;

        } catch (NoResultException $e) {                            
            return true;
        }

        return false;
    }

    /**
    * Удаляет матчи и ставки пользователей начиная с заданного тура
    * 
    * @param mixed $tournament_id
    * @param mixed $round_num
    */
    public function removeMatchesAndBetsFromRound($tournament_id, $round_num)
    {

        $query = $this->createQueryBuilder('tm')
        ->select('tm, m')
        ->leftJoin('tm.match', 'm')
        ->where('tm.tournament = :tournament_id')
        ->setParameter("tournament_id", $tournament_id)
        ->andWhere("tm.round >= :round")
        ->setParameter("round", $round_num)
        ->getQuery();        

        try{
            $matches = $query->getResult();
        } catch (NoResultException $e) {                            
            return true;
        }

        $matchesIds = array();

        $em = $this->getEntityManager();

        $em->beginTransaction();
        foreach($matches as $match)
        {
            $matchesIds[] = $match->getMatch()->getId();

            $em->remove($match);
        }

        $em->flush();

        if(empty($matchesIds))
        {
            $em->commit();
            return true;
        }

        $dql = "DELETE FROM AppBundle\Entity\Bet b WHERE b.tournament = :tournament_id AND b.match IN (:matches_ids)";

        $query = $this->getEntityManager()
        ->createQuery($dql)
        ->setParameter("tournament_id", $tournament_id)
        ->setParameter("matches_ids", $matchesIds);                

        $query->execute();

        $em->commit();
        return true;        
    }

    public function findNonProcessedMatchesFinished($tournament_id, $round)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT m.id
            FROM AppBundle\Entity\TournamentRoundMatch trm
            LEFT JOIN trm.tournament t
            LEFT JOIN t.rounds tr
            LEFT JOIN trm.match m
            WHERE t.id = :tournament_id 
            AND trm.round = :round                                         
            AND m.isStarted = 1 
            AND (m.isOver = 1 OR m.isRejected = 1) 
        AND trm.isProcessed = 0")
        ->setParameter("tournament_id", $tournament_id)
        ->setParameter("round", $round);

        try {            
            $result = $query->getResult();

            if(count($result) != 0)
                return $result;
            else
            return null;                        
        } catch (NoResultException $e) {
            return null;
        }
    }

    public function allBetsCalculated($tournament_id, $matches_ids)
    {        
        $em = $this->getEntityManager();        

        $query = $em->createQuery("SELECT COUNT(b.id) as total_bets
            FROM AppBundle\Entity\Bet b
            WHERE b.tournament = :tournament_id AND b.match IN (:matches_ids)
        ")
        ->setParameter("tournament_id", $tournament_id)                    
        ->setParameter("matches_ids", $matches_ids);

        try {            
            $result = $query->getSingleResult();            
            $totalBets = $result["total_bets"];                                    
        } catch (NoResultException $e) {
            return false;
        }

        if($totalBets == 0)
            return true;                

        $query = $em->createQuery("SELECT COUNT(b.id) as calculated_bets
            FROM AppBundle\Entity\Bet b
            WHERE b.tournament = :tournament_id AND b.points IS NOT NULL AND b.match IN (:matches_ids)
        ")
        ->setParameter("tournament_id", $tournament_id)                    
        ->setParameter("matches_ids", $matches_ids);

        try {            
            $result = $query->getSingleResult();            
            $calculatedBets = $result["calculated_bets"];            
        } catch (NoResultException $e) {
            return false;
        }                

        if($totalBets == $calculatedBets)
            return true;
        else
            return false;
    }

    public function findByIds($tournament_id, $matches_ids)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT tm, m
            FROM AppBundle\Entity\TournamentRoundMatch tm
            LEFT JOIN tm.match m WITH m.id = tm.match
        WHERE tm.tournament = :tournament_id AND m.id IN (:matches_ids)")
        ->setParameter("tournament_id", $tournament_id)
        ->setParameter("matches_ids", $matches_ids);        
        try {            
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    public function allRoundMatchesFinished($tournament_id, $round)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT COUNT(m.id) as matches_finished, tr.matches_in_game
            FROM AppBundle\Entity\TournamentRoundMatch trm
            LEFT JOIN trm.tournament t
            LEFT JOIN t.rounds tr
            LEFT JOIN trm.match m
            WHERE t.id = :tournament_id 
            AND trm.round = :round                                        
            AND m.isStarted = 1 
        AND (m.isOver = 1 OR m.isRejected = 1)")
        ->setParameter("tournament_id", $tournament_id)
        ->setParameter("round", $round);

        try {
            $result = $query->getSingleResult();                                    
            return (intval($result["matches_finished"]) === intval($result["matches_in_game"]));
        } catch (NoResultException $e) {
            return false;
        }
    }

    public function matchesPointsCalculated($tournament_id, $round)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT COUNT(b.id) as bets_without_points
            FROM AppBundle\Entity\Bet b
            WHERE b.points IS NULL AND b.match IN (
            SELECT m.id
            FROM AppBundle\Entity\TournamentRoundMatch trm
            LEFT JOIN trm.match m
            WHERE trm.tournament = :tournament_id 
            AND trm.round = :round                                                
            )
        ")
        ->setParameter("tournament_id", $tournament_id)
        ->setParameter("round", $round);

        try {            
            $result = $query->getSingleResult();
            return (intval($result["bets_without_points"]) === 0);
        } catch (NoResultException $e) {
            return false;
        }
    }
}
