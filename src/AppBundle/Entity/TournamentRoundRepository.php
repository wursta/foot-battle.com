<?php
namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

class TournamentRoundRepository extends EntityRepository
{
    /**
    * Поиск турнира со всеми связанными объектами
    */
    public function find($id, $user = false)
    {
      $q = $this->createQueryBuilder('tc')
                ->addSelect('t, g, f, tm, m, u, ht, gt, l')                
                ->leftJoin('tc.tournament', 't')
                ->leftJoin('t.geography', 'g')
                ->leftJoin('t.format', 'f')
                ->leftJoin('t.users', 'u')                
                ->leftJoin('t.round_matches', 'tm')
                ->leftJoin('tm.match', 'm', 'WITH', 'm.id = tm.match')                
                ->leftJoin('m.league', 'l')
                ->leftJoin('m.home_team', 'ht')
                ->leftJoin('m.guest_team', 'gt')
                ->where('t.id = :id')
                ->setParameter('id', $id)                
                ->groupBy('tm.id')
                ->addGroupBy('u.id')                                
                ->orderBy('tm.round', 'ASC')
                ->addOrderBy('m.match_date', 'ASC')                
                ->addOrderBy('m.match_time', 'ASC')                
                ->addOrderBy('u.id', 'ASC');
                
      if(is_a($user, '\AppBundle\Entity\User'))
      {
            $q->addSelect('b')
              ->leftJoin('t.bets', 'b', 'WITH', 'b.match = m.id AND b.user = :user_id')
              ->setParameter('user_id', $user->getId())
              ->addGroupBy('b.id');
      }
      
      $q = $q->getQuery();
      
      try {            
          if(count($q->getResult()) == 0)  
            return null;
          
          return $q->getSingleResult();
      } catch (NoResultException $e) {                                        
          
          return null;
      }
    }

    /*
    * Поиск турнира без связанных объектов
    */
    public function findLazy($id)
    {
      $q = $this->createQueryBuilder('tc')
                ->addSelect('t, u')                
                ->leftJoin('tc.tournament', 't')                
                ->leftJoin('t.users', 'u')
                ->where('t.id = :id')
                ->setParameter('id', $id)                
                ->getQuery();
      
      try {                    
          return $q->getSingleResult();
      } catch (NoResultException $e) {                                        
          
          return null;
      }
    }

    /**
    * Получает массив id'шников матчей турнира
    */
    public function getTournamentMatchesIds($id)
    {
        $q = $this->createQueryBuilder('tc')
                  ->select('m.id')
                  ->leftJoin('tc.tournament', 't')
                  ->leftJoin('t.round_matches', 'tm', 'WITH', 't.id = tm.tournament')
                  ->leftJoin('tm.match', 'm', 'WITH', 'm.id = tm.match')
                  ->where('t.id = :id')
                  ->groupBy('m.id')
                  ->setParameter('id', $id)
                  ->getQuery();

        try {                    
          $matches = $q->getResult();
          $matchesIds = array();
          foreach($matches as $match)
            if($match["id"])
              $matchesIds[] = $match["id"];

          return $matchesIds;

      } catch (NoResultException $e) {                                        
          
          return array();
      }
    }

    /*
    * Проверяет на уникальность матч в турнире
    */
    public function matchIsUnique($tournament_id, $match_id)
    {
        $q = $this->createQueryBuilder('tc')
                ->select('COUNT(tcm.id) as cnt')
                ->leftJoin('tc.tournament', 't')
                ->leftJoin('t.round_matches', 'tcm')
                ->where('t.id = :tournament_id')
                ->andWhere('tcm.match = :match_id')
                ->setParameter('tournament_id', $tournament_id)                
                ->setParameter('match_id', $match_id)                
                ->getQuery();

        try {                    
            $result = $q->getSingleResult();
            return !((bool) $result["cnt"]);
        } catch (NoResultException $e) {                                        
            
            return false;
        }        
    }
    
    /**
    * Проверяет есть ли данный матч в турнире
    * 
    * @param int $tournament_id
    * @param int $match_id
    * @return bool
    */
    public function isMatchInTournament($tournament_id, $match_id)
    {
        $q = $this->createQueryBuilder('tc')
                  ->select('COUNT(tcm.id) as cnt')
                  ->leftJoin('tc.tournament', 't')
                  ->leftJoin('t.round_matches', 'tcm')
                  ->where('t.id = :tournament_id')
                  ->andWhere('tcm.match = :match_id')
                  ->setParameter('tournament_id', $tournament_id)                
                  ->setParameter('match_id', $match_id)                
                  ->getQuery();

        try {
            $result = $q->getSingleResult();
            return ((bool) $result["cnt"]);
        } catch (NoResultException $e) {                                        
            
            return false;
        }
    }

    /**
    * Проверят законены ли все матчи в турнире
    */
    public function isAllTournamentMatchesOver($tournament_id)
    {
        $q = $this->createQueryBuilder('tc')
                  ->select("tc.real_rounds_count*tc.matches_in_game  AS total_count, COUNT(m.id) as finished_matches")
                  ->leftJoin('tc.tournament', 't')
                  ->leftJoin('t.round_matches', 'tcm')
                  ->leftJoin('tcm.match', 'm', 'WITH', 'm.isOver = 1 OR m.isRejected = 1')
                  ->where('t.id = :tournament_id')
                  ->setParameter('tournament_id', $tournament_id)
                  ->getQuery();

        try {
          $result = $q->getSingleResult();
          if($result["total_count"] == $result["finished_matches"])
            return true;

          return false;
        }
        catch (NoResultException $e) {                                        
            
            return false;
        }

    }
}
