<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AppBundle\Entity\TransactionLog
 *
 * @ORM\Table(name="transactions_log")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\TransactionLogRepository")
 */
class TransactionLog
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")     
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="transaction_logs")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="transaction_logs_from")
     * @ORM\JoinColumn(name="user_from_id", referencedColumnName="id")
     */
    protected $user_from;
    
    /**
     * @ORM\Column(type="string")
     * @Assert\Choice({"DEPOSIT", "WITHDRAW"})
     */
    protected $action;
    
    /**
     * @ORM\Column(type="float")
     */
    protected $sum;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $from_reserv;
    
    /**
     * @ORM\Column(type="datetime")
     */
    protected $datetime;
    
    /**
     * @ORM\Column(type="string")     
     * @Assert\NotBlank()
     */
    private $note;

    /**
     * Constructor
     */
    public function __construct()
    {
        
    }        

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set action
     *
     * @param string $action
     * @return TransactionLog
     */
    public function setAction($action)
    {
        $this->action = $action;
    
        return $this;
    }

    /**
     * Get action
     *
     * @return string 
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set sum
     *
     * @param float $sum
     * @return TransactionLog
     */
    public function setSum($sum)
    {
        $this->sum = $sum;
    
        return $this;
    }

    /**
     * Get sum
     *
     * @return float 
     */
    public function getSum()
    {
        return $this->sum;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     * @return TransactionLog
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;
    
        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime 
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return TransactionLog
     */
    public function setNote($note)
    {
        $this->note = $note;
    
        return $this;
    }

    /**
     * Get note
     *
     * @return string 
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return TransactionLog
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user_from
     *
     * @param \AppBundle\Entity\User $userFrom
     * @return TransactionLog
     */
    public function setUserFrom(\AppBundle\Entity\User $userFrom = null)
    {
        $this->user_from = $userFrom;
    
        return $this;
    }

    /**
     * Get user_from
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUserFrom()
    {
        return $this->user_from;
    }

    /**
     * Set from_reserv
     *
     * @param \boolean $fromReserv
     * @return TransactionLog
     */
    public function setFromReserv($fromReserv)
    {
        $this->from_reserv = $fromReserv;

        return $this;
    }

    /**
     * Get from_reserv
     *
     * @return \boolean 
     */
    public function getFromReserv()
    {
        return $this->from_reserv;
    }
}
