<?php

    namespace AppBundle\Entity;

    use Doctrine\ORM\EntityRepository;

    /**
    * TransactionLogRepository
    *
    */
    class TransactionLogRepository extends EntityRepository
    {
        public function findByUser($user_id, $offset = 0, $rowsPerPage = 10)
        {
            $q = $this->createQueryBuilder('log')              
            ->select("COUNT(log.id) as total_count")
            ->where('(log.user = :user_id OR log.user_from = :user_id)')
            ->setParameter('user_id', $user_id)
            ->getQuery();
            
            $totalCnt = $q->getSingleResult();
            $totalCnt = $totalCnt["total_count"];
            
            $q = $this->createQueryBuilder('log')                          
            ->where('(log.user = :user_id OR log.user_from = :user_id)')
            ->setParameter('user_id', $user_id)            
            ->orderBy('log.datetime', 'DESC')
            ->addOrderBy('log.id', 'DESC')
            ->setFirstResult($offset)
            ->setMaxResults($rowsPerPage)
            ->getQuery();

            try {                 
                return array("total" => $totalCnt, "data" => $q->getResult());
            } catch (NoResultException $e) {
                return null;
            }
        }
        
        public function findForIntervalByUser($user_id, $start, $end, $offset = 0, $rowsPerPage = 10)
        {
            $q = $this->createQueryBuilder('log')              
            ->select("COUNT(log.id) as total_count")
            ->where('(log.user = :user_id OR log.user_from = :user_id)')            
            ->setParameter('user_id', $user_id)
            ->andWhere("log.datetime >= :start AND log.datetime <= :end")
            ->setParameter('start', $start->format("Y-m-d H:i:s"))
            ->setParameter('end', $end->format("Y-m-d H:i:s"))
            ->getQuery();
            
            $totalCnt = $q->getSingleResult();
            $totalCnt = $totalCnt["total_count"];
            
            $q = $this->createQueryBuilder('log')                          
            ->where('(log.user = :user_id OR log.user_from = :user_id)')
            ->setParameter('user_id', $user_id)            
            ->andWhere("log.datetime >= :start AND log.datetime <= :end")
            ->setParameter('start', $start->format("Y-m-d H:i:s"))
            ->setParameter('end', $end->format("Y-m-d H:i:s"))
            ->orderBy('log.datetime', 'DESC')
            ->addOrderBy('log.id', 'ASC')
            ->setFirstResult($offset)
            ->setMaxResults($rowsPerPage)
            ->getQuery();

            try {                 
                return array("total" => $totalCnt, "data" => $q->getResult());
            } catch (NoResultException $e) {
                return null;
            }
        }
    }
