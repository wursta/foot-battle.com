<?php
namespace AppBundle\Entity;

use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * AppBundle\Entity\User
 *
 * @ORM\Table(name="users") *
 * @ORM\Entity(repositoryClass="AppBundle\Entity\UserRepository")
 * @UniqueEntity("email")
 * @ORM\HasLifecycleCallbacks
 */
class User implements AdvancedUserInterface, \Serializable
{
    const ROLE_BOT = 'ROLE_BOT';
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=64)
     * @Assert\Length(min = 3, max = 64)
     */
    private $password;

    /**
     * @ORM\Column(name="email", type="string", length=60, unique=true)
     * @Assert\NotBlank()
     * @Assert\Email()
     * @Assert\Length(min = 1, max = 60)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $role;

    /**
     * @ORM\Column(type="string", length=2)
     * @Assert\Locale()
     */
    private $locale;

    /**
     * @ORM\Column(type="string", length=25)
     * @Assert\NotBlank()
     * @Assert\Length(min = 2, max = 64)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\Length(min = 2, max = 50)
     */
    private $first_name;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\Length(min = 2, max = 50)
     */
    private $last_name;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\Length(min = 2, max = 50)
     */
    private $second_name;

    /**
     * @ORM\Column(type="date", length=50)
     * @Assert\Date()
     */
    private $birthdate;

    /**
     * @ORM\Column(type="string", length=2)
     * @Assert\Country()
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min = 2, max = 255)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $avatar;

    /**
     * @Assert\File(maxSize="6000000", mimeTypes = { "image/gif", "image/jpeg", "image/png" })
     */
    private $avatar_file;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(name="is_confirmed", type="boolean")
     */
    private $isConfirmed;

    /**
     * @ORM\Column(name="is_agreed", type="boolean")
     */
    private $isAgreed;

    /**
     * @ORM\Column(type="float")
     */
    private $rating;

    /**
     * @ORM\Column(type="float")
     */
    private $total_points;

    /**
     * @ORM\Column(type="integer")
     */
    private $total_bets;

    /**
     * @ORM\Column(type="float")
     */
    private $balance;

    /**
     * @ORM\Column(type="float")
     */
    private $free_funds;

    /**
     * @ORM\Column(type="float")
     */
    private $reserved;

    /**
     * @ORM\Column(type="datetime", length=50)
     * @Assert\Date()
     */
    private $registration_date;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $referral_code;

    /**
     * @ORM\OneToOne(targetEntity="User")
     * @ORM\JoinColumn(name="referral_user_id", referencedColumnName="id")
     */
    protected $referral_user;

    /**
    * @ORM\Column(type="integer")
    */
    private $partner_level;

    /**
     * @ORM\OneToMany(targetEntity="BillingOperation", mappedBy="user")
     */
    protected $billing_operations;

    /**
     * @ORM\OneToMany(targetEntity="PartnerCharge", mappedBy="user")
     */
    protected $partner_charges;

    /**
     * @ORM\OneToMany(targetEntity="PartnerCharge", mappedBy="referral_user")
     */
    protected $referral_charges;

    /**
     * @ORM\OneToMany(targetEntity="BillingOperation", mappedBy="user_recipient")
     */
    protected $billing_operations_recipient;

    /**
     * @ORM\OneToMany(targetEntity="Tournament", mappedBy="author")
     */
    protected $tournaments;

    /**
     * @ORM\ManyToMany(targetEntity="Tournament", mappedBy="users")
     */
    private $tournament_users;

    /**
     * @ORM\ManyToMany(targetEntity="Tournament", mappedBy="user_requests")
     */
    private $tournaments_requests;

    /**
     * @ORM\OneToMany(targetEntity="Bet", mappedBy="user")
     */
    protected $bets;

    /**
     * @ORM\OneToMany(targetEntity="TournamentResult", mappedBy="user")
     */
    protected $tournaments_results;

    /**
     * @ORM\OneToMany(targetEntity="TournamentChampionshipResult", mappedBy="user")
     */
    protected $tournaments_championship_results;

    /**
     * @ORM\OneToMany(targetEntity="TournamentChampionshipRealTimeResult", mappedBy="user")
     */
    protected $tournaments_championship_rt_results;

    /**
     * @ORM\OneToMany(targetEntity="TournamentRoundResult", mappedBy="user")
     */
    protected $tournaments_round_results;

    /**
     * @ORM\OneToMany(targetEntity="TournamentPlayoffQualificationResult", mappedBy="user")
     */
    protected $tournaments_playoff_qualification_results;

    /**
     * @ORM\OneToMany(targetEntity="TournamentPlayoffQualificationRealTimeResult", mappedBy="user")
     */
    protected $tournaments_playoff_qualification_rt_results;

    /**
     * @ORM\OneToMany(targetEntity="TournamentChampionsleagueQualificationRealTimeResult", mappedBy="user")
     */
    protected $tournaments_championsleague_qualification_rt_results;

    /**
     * @ORM\OneToMany(targetEntity="TournamentChampionsleagueGroupResult", mappedBy="user")
     */
    protected $tournaments_championsleague_group_results;

    /**
     * @ORM\OneToMany(targetEntity="TournamentRoundGame", mappedBy="user_left")
     */
    protected $tournaments_round_games_at_left;

    /**
     * @ORM\OneToMany(targetEntity="TournamentRoundGame", mappedBy="user_right")
     */
    protected $tournaments_round_games_at_right;

    /**
     * @ORM\OneToMany(targetEntity="TournamentPlayoffGame", mappedBy="user_left")
     */
    protected $tournaments_playoff_games_at_left;

    /**
     * @ORM\OneToMany(targetEntity="TournamentPlayoffGame", mappedBy="user_right")
     */
    protected $tournaments_playoff_games_at_right;

    /**
     * @ORM\OneToMany(targetEntity="TournamentChampionsleagueQualificationResult", mappedBy="user")
     */
    protected $tournaments_championsleague_qualification_results;

    /**
     * @ORM\OneToMany(targetEntity="TournamentChampionsleagueGroupGame", mappedBy="user_left")
     */
    protected $tournaments_championsleague_group_games_at_left;

    /**
     * @ORM\OneToMany(targetEntity="TournamentChampionsleagueGroupGame", mappedBy="user_right")
     */
    protected $tournaments_championsleague_group_games_at_right;

    /**
     * @ORM\OneToMany(targetEntity="TournamentChampionsleaguePlayoffGame", mappedBy="user_left")
     */
    protected $tournaments_championsleague_playoff_games_at_left;

    /**
     * @ORM\OneToMany(targetEntity="TournamentChampionsleaguePlayoffGame", mappedBy="user_right")
     */
    protected $tournaments_championsleague_playoff_games_at_right;

    /**
     * @ORM\OneToMany(targetEntity="TransactionLog", mappedBy="user")
     */
    protected $transaction_logs;

    /**
     * @ORM\OneToMany(targetEntity="TransactionLog", mappedBy="user_from")
     */
    protected $transaction_logs_from;

    private $tempAvatarFile;

    public function __construct()
    {
        $this->isAgreed = true;
        $this->isConfirmed = false;
        $this->isActive = false;
        $this->role = 'ROLE_USER';
        $this->rating = 0;
        $this->total_points = 0;
        $this->total_bets = 0;
        $this->balance = 0;
        $this->free_funds = 0;
        $this->reserved = 0;
        $this->partner_level = 1;
        $this->registration_date = new \DateTime();
        $this->billing_operations = new \Doctrine\Common\Collections\ArrayCollection();
        $this->partner_charges = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tournaments = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tournaments_requests = new \Doctrine\Common\Collections\ArrayCollection();
        $this->bets = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tournaments_results = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tournaments_championship_results = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tournaments_championship_rt_results = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tournaments_round_results = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tournaments_playoff_qualification_results = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tournaments_playoff_qualification_rt_results = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tournaments_championsleague_group_results = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tournaments_round_games_at_left = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tournaments_round_games_at_right = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tournaments_championsleague_qualification_results = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tournaments_championsleague_qualification_rt_results = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tournaments_championsleague_group_games_at_left = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tournaments_championsleague_group_games_at_right = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tournaments_championsleague_playoff_games_at_left = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tournaments_championsleague_playoff_games_at_right = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
     /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->email,
            $this->password,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->email,
            $this->password,
        ) = unserialize($serialized);
    }        

    protected function getUploadRootDir()
    {
        return __DIR__.'/../Resources/public/data/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'user/avatars';
    }

    public function getAbsolutePath()
    {
        return null === $this->avatar
            ? null
            : $this->getUploadRootDir().'/'.$this->avatar;
    }

    public function getWebPath()
    {
        return null === $this->avatar
            ? null
            : $this->getUploadDir().'/'.$this->avatar;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->getAvatarFile()) {
            $this->avatar = $this->id.'.'.$this->getAvatarFile()->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function uploadAvatar()
    {
        if (null === $this->getAvatarFile()) {
            return;
        }

        // check if we have an old image
        if (isset($this->tempAvatarFile)) {
            // delete the old image
            unlink($this->tempAvatarFile);
            // clear the temp image path
            $this->tempAvatarFile = null;
        }

        $filename = $this->getId().'.'.$this->getAvatarFile()->guessExtension();

        switch($this->getAvatarFile()->getMimeType())
        {
            case "image/gif":
                $srcImage = imagecreatefromgif($this->getAvatarFile()->getPathname());
            break;
            case "image/jpeg":
                $srcImage = imagecreatefromjpeg($this->getAvatarFile()->getPathname());
            break;
            case "image/png":
                $srcImage = imagecreatefrompng($this->getAvatarFile()->getPathname());
            break;
        }

        $thumbW = 100;
        $srcImageW = imageSX($srcImage);
        $srcImageH = imageSY($srcImage);

        $dstImage=ImageCreateTrueColor($thumbW,$thumbW);
        ImageAlphaBlending($dstImage, false);
        imageSaveAlpha($dstImage, true);
        imagefill($dstImage, 0, 0, IMG_COLOR_TRANSPARENT);

        // Горизонтальное изображение
        if ($srcImageW > $srcImageH && $srcImageW > $thumbW)
        {
            imagecopyresampled($dstImage, $srcImage, 0, 0, (($srcImageW-$srcImageH)/2), 0, $thumbW, $thumbW, $srcImageH, $srcImageH);
        }
        // Вертикальное изображение
        elseif ($srcImageH > $srcImageW && $srcImageH > $thumbW)
        {
            imagecopyresampled($dstImage, $srcImage, 0, 0, 0, (($srcImageH-$srcImageW)/2), $thumbW, $thumbW, $srcImageW, $srcImageW);
        }
        // Если квадрат
        elseif ($srcImageH == $srcImageW && $srcImageH > $thumbW)
        {
            imagecopyresampled($dstImage, $srcImage, 0, 0, 0, 0, $thumbW, $thumbW, $srcImageW, $srcImageW);
        }

        imagejpeg($dstImage,$this->getUploadRootDir().'/'.$filename);
        imagedestroy($dstImage);
        imagedestroy($srcImage);

        $this->avatar = $filename;

        $this->avatar_file = null;
    }

    /**
     * @ORM\PreRemove()
     */
    public function storeFilenameForRemove()
    {
        $this->tempAvatarFile = $this->getAbsolutePath();
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if (isset($this->tempAvatarFile)) {
            unlink($this->tempAvatarFile);
        }
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return ($this->isActive && $this->isConfirmed && $this->isAgreed);
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function getUsername()
    {
        return $this->email;
    }

    public function getNick()
    {
        return $this->username;
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    /**
     * @inheritDoc
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        return array($this->role);
        //return array('ROLE_USER');
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
    }
    
    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Set nick
     *
     * @param string $nick
     * @return User
     */
    public function setNick($nick)
    {
        $this->username = $nick;

        return $this;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set role
     *
     * @param string $role
     * @return User
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set isConfirmed
     *
     * @param boolean $isConfirmed
     * @return User
     */
    public function setIsConfirmed($isConfirmed)
    {
        $this->isConfirmed = $isConfirmed;

        return $this;
    }

    /**
     * Get isConfirmed
     *
     * @return boolean
     */
    public function getIsConfirmed()
    {
        return $this->isConfirmed;
    }

    /**
     * Set isAgreed
     *
     * @param boolean $isAgreed
     * @return User
     */
    public function setIsAgreed($isAgreed)
    {
        $this->isAgreed = $isAgreed;

        return $this;
    }

    /**
     * Get isAgreed
     *
     * @return boolean
     */
    public function getIsAgreed()
    {
        return $this->isAgreed;
    }

    /**
     * Set locale
     *
     * @param string $locale
     * @return User
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get locale
     *
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set first_name
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;

        return $this;
    }

    /**
     * Get first_name
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Set last_name
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;

        return $this;
    }

    /**
     * Get last_name
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Set second_name
     *
     * @param string $secondName
     * @return User
     */
    public function setSecondName($secondName)
    {
        $this->second_name = $secondName;

        return $this;
    }

    /**
     * Get second_name
     *
     * @return string
     */
    public function getSecondName()
    {
        return $this->second_name;
    }

    /**
     * Set birthdate
     *
     * @param string $birthdate
     * @return User
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * Get birthdate
     *
     * @return string
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return User
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    public function getFullName()
    {
        if(empty($this->first_name) && empty($this->last_name))
            return $this->username;

        if(!empty($this->first_name) && empty($this->last_name))
            return $this->first_name." (".$this->username.")";

        if(!empty($this->first_name) && !empty($this->last_name) && empty($this->second_name))
            return $this->first_name." ".$this->last_name;

        if(!empty($this->first_name) && !empty($this->last_name) && !empty($this->second_name))
            return $this->first_name." ".$this->second_name." ".$this->last_name;
    }

    /**
     * Add tournaments
     *
     * @param \AppBundle\Entity\Tournament $tournaments
     * @return User
     */
    public function addTournament(\AppBundle\Entity\Tournament $tournaments)
    {
        $this->tournaments[] = $tournaments;

        return $this;
    }

    /**
     * Remove tournaments
     *
     * @param \AppBundle\Entity\Tournament $tournaments
     */
    public function removeTournament(\AppBundle\Entity\Tournament $tournaments)
    {
        $this->tournaments->removeElement($tournaments);
    }

    /**
     * Get tournaments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTournaments()
    {
        return $this->tournaments;
    }

    /**
     * Get tournament
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTournament()
    {
        return $this->tournament;
    }

    /**
     * Add tournaments_requests
     *
     * @param \AppBundle\Entity\Tournament $tournamentsRequests
     * @return User
     */
    public function addTournamentsRequest(\AppBundle\Entity\Tournament $tournamentsRequests)
    {
        $this->tournaments_requests[] = $tournamentsRequests;

        return $this;
    }

    /**
     * Remove tournaments_requests
     *
     * @param \AppBundle\Entity\Tournament $tournamentsRequests
     */
    public function removeTournamentsRequest(\AppBundle\Entity\Tournament $tournamentsRequests)
    {
        $this->tournaments_requests->removeElement($tournamentsRequests);
    }

    /**
     * Get tournaments_requests
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTournamentsRequests()
    {
        return $this->tournaments_requests;
    }

    /**
     * Add tournament_users
     *
     * @param \AppBundle\Entity\Tournament $tournamentUsers
     * @return User
     */
    public function addTournamentUser(\AppBundle\Entity\Tournament $tournamentUsers)
    {
        $this->tournament_users[] = $tournamentUsers;

        return $this;
    }

    /**
     * Remove tournament_users
     *
     * @param \AppBundle\Entity\Tournament $tournamentUsers
     */
    public function removeTournamentUser(\AppBundle\Entity\Tournament $tournamentUsers)
    {
        $this->tournament_users->removeElement($tournamentUsers);
    }

    /**
     * Get tournament_users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTournamentUsers()
    {
        return $this->tournament_users;
    }

    /**
     * Add bets
     *
     * @param \AppBundle\Entity\Bet $bets
     * @return User
     */
    public function addBet(\AppBundle\Entity\Bet $bets)
    {
        $this->bets[] = $bets;

        return $this;
    }

    /**
     * Remove bets
     *
     * @param \AppBundle\Entity\Bet $bets
     */
    public function removeBet(\AppBundle\Entity\Bet $bets)
    {
        $this->bets->removeElement($bets);
    }

    /**
     * Get bets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBets()
    {
        return $this->bets;
    }

    /**
     * Set total_points
     *
     * @param float $totalPoints
     * @return User
     */
    public function setTotalPoints($totalPoints)
    {
        $this->total_points = $totalPoints;

        return $this;
    }

    /**
     * Get total_points
     *
     * @return float
     */
    public function getTotalPoints()
    {
        return $this->total_points;
    }

    /**
     * Add tournaments_results
     *
     * @param \AppBundle\Entity\TournamentResult $tournamentsResults
     * @return User
     */
    public function addTournamentsResult(\AppBundle\Entity\TournamentResult $tournamentsResults)
    {
        $this->tournaments_results[] = $tournamentsResults;

        return $this;
    }

    /**
     * Remove tournaments_results
     *
     * @param \AppBundle\Entity\TournamentResult $tournamentsResults
     */
    public function removeTournamentsResult(\AppBundle\Entity\TournamentResult $tournamentsResults)
    {
        $this->tournaments_results->removeElement($tournamentsResults);
    }

    /**
     * Get tournaments_results
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTournamentsResults()
    {
        return $this->tournaments_results;
    }

    /**
     * Add tournaments_round_games_at_left
     *
     * @param \AppBundle\Entity\TournamentRoundGame $tournamentsRoundGamesAtLeft
     * @return User
     */
    public function addTournamentsRoundGamesAtLeft(\AppBundle\Entity\TournamentRoundGame $tournamentsRoundGamesAtLeft)
    {
        $this->tournaments_round_games_at_left[] = $tournamentsRoundGamesAtLeft;

        return $this;
    }

    /**
     * Remove tournaments_round_games_at_left
     *
     * @param \AppBundle\Entity\TournamentRoundGame $tournamentsRoundGamesAtLeft
     */
    public function removeTournamentsRoundGamesAtLeft(\AppBundle\Entity\TournamentRoundGame $tournamentsRoundGamesAtLeft)
    {
        $this->tournaments_round_games_at_left->removeElement($tournamentsRoundGamesAtLeft);
    }

    /**
     * Get tournaments_round_games_at_left
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTournamentsRoundGamesAtLeft()
    {
        return $this->tournaments_round_games_at_left;
    }

    /**
     * Add tournaments_round_games_at_right
     *
     * @param \AppBundle\Entity\TournamentRoundGame $tournamentsRoundGamesAtRight
     * @return User
     */
    public function addTournamentsRoundGamesAtRight(\AppBundle\Entity\TournamentRoundGame $tournamentsRoundGamesAtRight)
    {
        $this->tournaments_round_games_at_right[] = $tournamentsRoundGamesAtRight;

        return $this;
    }

    /**
     * Remove tournaments_round_games_at_right
     *
     * @param \AppBundle\Entity\TournamentRoundGame $tournamentsRoundGamesAtRight
     */
    public function removeTournamentsRoundGamesAtRight(\AppBundle\Entity\TournamentRoundGame $tournamentsRoundGamesAtRight)
    {
        $this->tournaments_round_games_at_right->removeElement($tournamentsRoundGamesAtRight);
    }

    /**
     * Get tournaments_round_games_at_right
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTournamentsRoundGamesAtRight()
    {
        return $this->tournaments_round_games_at_right;
    }

    /**
     * Add tournaments_championship_results
     *
     * @param \AppBundle\Entity\TournamentChampionshipResult $tournamentsChampionshipResults
     * @return User
     */
    public function addTournamentsChampionshipResult(\AppBundle\Entity\TournamentChampionshipResult $tournamentsChampionshipResults)
    {
        $this->tournaments_championship_results[] = $tournamentsChampionshipResults;

        return $this;
    }

    /**
     * Remove tournaments_championship_results
     *
     * @param \AppBundle\Entity\TournamentChampionshipResult $tournamentsChampionshipResults
     */
    public function removeTournamentsChampionshipResult(\AppBundle\Entity\TournamentChampionshipResult $tournamentsChampionshipResults)
    {
        $this->tournaments_championship_results->removeElement($tournamentsChampionshipResults);
    }

    /**
     * Get tournaments_championship_results
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTournamentsChampionshipResults()
    {
        return $this->tournaments_championship_results;
    }

    /**
     * Add tournaments_round_results
     *
     * @param \AppBundle\Entity\TournamentRoundResult $tournamentsRoundResults
     * @return User
     */
    public function addTournamentsRoundResult(\AppBundle\Entity\TournamentRoundResult $tournamentsRoundResults)
    {
        $this->tournaments_round_results[] = $tournamentsRoundResults;

        return $this;
    }

    /**
     * Remove tournaments_round_results
     *
     * @param \AppBundle\Entity\TournamentRoundResult $tournamentsRoundResults
     */
    public function removeTournamentsRoundResult(\AppBundle\Entity\TournamentRoundResult $tournamentsRoundResults)
    {
        $this->tournaments_round_results->removeElement($tournamentsRoundResults);
    }

    /**
     * Get tournaments_round_results
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTournamentsRoundResults()
    {
        return $this->tournaments_round_results;
    }

    /**
     * Add tournaments_playoff_qualification_results
     *
     * @param \AppBundle\Entity\TournamentPlayoffQualificationResult $tournamentsPlayoffQualificationResults
     * @return User
     */
    public function addTournamentsPlayoffQualificationResult(\AppBundle\Entity\TournamentPlayoffQualificationResult $tournamentsPlayoffQualificationResults)
    {
        $this->tournaments_playoff_qualification_results[] = $tournamentsPlayoffQualificationResults;

        return $this;
    }

    /**
     * Remove tournaments_playoff_qualification_results
     *
     * @param \AppBundle\Entity\TournamentPlayoffQualificationResult $tournamentsPlayoffQualificationResults
     */
    public function removeTournamentsPlayoffQualificationResult(\AppBundle\Entity\TournamentPlayoffQualificationResult $tournamentsPlayoffQualificationResults)
    {
        $this->tournaments_playoff_qualification_results->removeElement($tournamentsPlayoffQualificationResults);
    }

    /**
     * Get tournaments_playoff_qualification_results
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTournamentsPlayoffQualificationResults()
    {
        return $this->tournaments_playoff_qualification_results;
    }

    /**
     * Add tournaments_playoff_games_at_left
     *
     * @param \AppBundle\Entity\TournamentPlayoffGame $tournamentsPlayoffGamesAtLeft
     * @return User
     */
    public function addTournamentsPlayoffGamesAtLeft(\AppBundle\Entity\TournamentPlayoffGame $tournamentsPlayoffGamesAtLeft)
    {
        $this->tournaments_playoff_games_at_left[] = $tournamentsPlayoffGamesAtLeft;

        return $this;
    }

    /**
     * Remove tournaments_playoff_games_at_left
     *
     * @param \AppBundle\Entity\TournamentPlayoffGame $tournamentsPlayoffGamesAtLeft
     */
    public function removeTournamentsPlayoffGamesAtLeft(\AppBundle\Entity\TournamentPlayoffGame $tournamentsPlayoffGamesAtLeft)
    {
        $this->tournaments_playoff_games_at_left->removeElement($tournamentsPlayoffGamesAtLeft);
    }

    /**
     * Get tournaments_playoff_games_at_left
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTournamentsPlayoffGamesAtLeft()
    {
        return $this->tournaments_playoff_games_at_left;
    }

    /**
     * Add tournaments_playoff_games_at_right
     *
     * @param \AppBundle\Entity\TournamentPlayoffGame $tournamentsPlayoffGamesAtRight
     * @return User
     */
    public function addTournamentsPlayoffGamesAtRight(\AppBundle\Entity\TournamentPlayoffGame $tournamentsPlayoffGamesAtRight)
    {
        $this->tournaments_playoff_games_at_right[] = $tournamentsPlayoffGamesAtRight;

        return $this;
    }

    /**
     * Remove tournaments_playoff_games_at_right
     *
     * @param \AppBundle\Entity\TournamentPlayoffGame $tournamentsPlayoffGamesAtRight
     */
    public function removeTournamentsPlayoffGamesAtRight(\AppBundle\Entity\TournamentPlayoffGame $tournamentsPlayoffGamesAtRight)
    {
        $this->tournaments_playoff_games_at_right->removeElement($tournamentsPlayoffGamesAtRight);
    }

    /**
     * Get tournaments_playoff_games_at_right
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTournamentsPlayoffGamesAtRight()
    {
        return $this->tournaments_playoff_games_at_right;
    }

    /**
     * Set rating
     *
     * @param float $rating
     * @return User
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return float
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set total_bets
     *
     * @param integer $totalBets
     * @return User
     */
    public function setTotalBets($totalBets)
    {
        $this->total_bets = $totalBets;

        return $this;
    }

    /**
     * Get total_bets
     *
     * @return integer
     */
    public function getTotalBets()
    {
        return $this->total_bets;
    }

    /**
     * Add tournaments_championsleague_qualification_results
     *
     * @param \AppBundle\Entity\TournamentChampionsleagueQualificationResult $tournamentsChampionsleagueQualificationResults
     * @return User
     */
    public function addTournamentsChampionsleagueQualificationResult(\AppBundle\Entity\TournamentChampionsleagueQualificationResult $tournamentsChampionsleagueQualificationResults)
    {
        $this->tournaments_championsleague_qualification_results[] = $tournamentsChampionsleagueQualificationResults;

        return $this;
    }

    /**
     * Remove tournaments_championsleague_qualification_results
     *
     * @param \AppBundle\Entity\TournamentChampionsleagueQualificationResult $tournamentsChampionsleagueQualificationResults
     */
    public function removeTournamentsChampionsleagueQualificationResult(\AppBundle\Entity\TournamentChampionsleagueQualificationResult $tournamentsChampionsleagueQualificationResults)
    {
        $this->tournaments_championsleague_qualification_results->removeElement($tournamentsChampionsleagueQualificationResults);
    }

    /**
     * Get tournaments_championsleague_qualification_results
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTournamentsChampionsleagueQualificationResults()
    {
        return $this->tournaments_championsleague_qualification_results;
    }

    /**
     * Add tournaments_championsleague_group_games_at_left
     *
     * @param \AppBundle\Entity\TournamentChampionsleagueGroupGame $tournamentsChampionsleagueGroupGamesAtLeft
     * @return User
     */
    public function addTournamentsChampionsleagueGroupGamesAtLeft(\AppBundle\Entity\TournamentChampionsleagueGroupGame $tournamentsChampionsleagueGroupGamesAtLeft)
    {
        $this->tournaments_championsleague_group_games_at_left[] = $tournamentsChampionsleagueGroupGamesAtLeft;

        return $this;
    }

    /**
     * Remove tournaments_championsleague_group_games_at_left
     *
     * @param \AppBundle\Entity\TournamentChampionsleagueGroupGame $tournamentsChampionsleagueGroupGamesAtLeft
     */
    public function removeTournamentsChampionsleagueGroupGamesAtLeft(\AppBundle\Entity\TournamentChampionsleagueGroupGame $tournamentsChampionsleagueGroupGamesAtLeft)
    {
        $this->tournaments_championsleague_group_games_at_left->removeElement($tournamentsChampionsleagueGroupGamesAtLeft);
    }

    /**
     * Get tournaments_championsleague_group_games_at_left
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTournamentsChampionsleagueGroupGamesAtLeft()
    {
        return $this->tournaments_championsleague_group_games_at_left;
    }

    /**
     * Add tournaments_championsleague_group_games_at_right
     *
     * @param \AppBundle\Entity\TournamentChampionsleagueGroupGame $tournamentsChampionsleagueGroupGamesAtRight
     * @return User
     */
    public function addTournamentsChampionsleagueGroupGamesAtRight(\AppBundle\Entity\TournamentChampionsleagueGroupGame $tournamentsChampionsleagueGroupGamesAtRight)
    {
        $this->tournaments_championsleague_group_games_at_right[] = $tournamentsChampionsleagueGroupGamesAtRight;

        return $this;
    }

    /**
     * Remove tournaments_championsleague_group_games_at_right
     *
     * @param \AppBundle\Entity\TournamentChampionsleagueGroupGame $tournamentsChampionsleagueGroupGamesAtRight
     */
    public function removeTournamentsChampionsleagueGroupGamesAtRight(\AppBundle\Entity\TournamentChampionsleagueGroupGame $tournamentsChampionsleagueGroupGamesAtRight)
    {
        $this->tournaments_championsleague_group_games_at_right->removeElement($tournamentsChampionsleagueGroupGamesAtRight);
    }

    /**
     * Get tournaments_championsleague_group_games_at_right
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTournamentsChampionsleagueGroupGamesAtRight()
    {
        return $this->tournaments_championsleague_group_games_at_right;
    }

    /**
     * Add tournaments_championsleague_group_results
     *
     * @param \AppBundle\Entity\TournamentChampionsleagueGroupResult $tournamentsChampionsleagueGroupResults
     * @return User
     */
    public function addTournamentsChampionsleagueGroupResult(\AppBundle\Entity\TournamentChampionsleagueGroupResult $tournamentsChampionsleagueGroupResults)
    {
        $this->tournaments_championsleague_group_results[] = $tournamentsChampionsleagueGroupResults;

        return $this;
    }

    /**
     * Remove tournaments_championsleague_group_results
     *
     * @param \AppBundle\Entity\TournamentChampionsleagueGroupResult $tournamentsChampionsleagueGroupResults
     */
    public function removeTournamentsChampionsleagueGroupResult(\AppBundle\Entity\TournamentChampionsleagueGroupResult $tournamentsChampionsleagueGroupResults)
    {
        $this->tournaments_championsleague_group_results->removeElement($tournamentsChampionsleagueGroupResults);
    }

    /**
     * Get tournaments_championsleague_group_results
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTournamentsChampionsleagueGroupResults()
    {
        return $this->tournaments_championsleague_group_results;
    }

    /**
     * Add tournaments_championsleague_playoff_games_at_left
     *
     * @param \AppBundle\Entity\TournamentChampionsleaguePlayoffGame $tournamentsChampionsleaguePlayoffGamesAtLeft
     * @return User
     */
    public function addTournamentsChampionsleaguePlayoffGamesAtLeft(\AppBundle\Entity\TournamentChampionsleaguePlayoffGame $tournamentsChampionsleaguePlayoffGamesAtLeft)
    {
        $this->tournaments_championsleague_playoff_games_at_left[] = $tournamentsChampionsleaguePlayoffGamesAtLeft;

        return $this;
    }

    /**
     * Remove tournaments_championsleague_playoff_games_at_left
     *
     * @param \AppBundle\Entity\TournamentChampionsleaguePlayoffGame $tournamentsChampionsleaguePlayoffGamesAtLeft
     */
    public function removeTournamentsChampionsleaguePlayoffGamesAtLeft(\AppBundle\Entity\TournamentChampionsleaguePlayoffGame $tournamentsChampionsleaguePlayoffGamesAtLeft)
    {
        $this->tournaments_championsleague_playoff_games_at_left->removeElement($tournamentsChampionsleaguePlayoffGamesAtLeft);
    }

    /**
     * Get tournaments_championsleague_playoff_games_at_left
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTournamentsChampionsleaguePlayoffGamesAtLeft()
    {
        return $this->tournaments_championsleague_playoff_games_at_left;
    }

    /**
     * Add tournaments_championsleague_playoff_games_at_right
     *
     * @param \AppBundle\Entity\TournamentChampionsleaguePlayoffGame $tournamentsChampionsleaguePlayoffGamesAtRight
     * @return User
     */
    public function addTournamentsChampionsleaguePlayoffGamesAtRight(\AppBundle\Entity\TournamentChampionsleaguePlayoffGame $tournamentsChampionsleaguePlayoffGamesAtRight)
    {
        $this->tournaments_championsleague_playoff_games_at_right[] = $tournamentsChampionsleaguePlayoffGamesAtRight;

        return $this;
    }

    /**
     * Remove tournaments_championsleague_playoff_games_at_right
     *
     * @param \AppBundle\Entity\TournamentChampionsleaguePlayoffGame $tournamentsChampionsleaguePlayoffGamesAtRight
     */
    public function removeTournamentsChampionsleaguePlayoffGamesAtRight(\AppBundle\Entity\TournamentChampionsleaguePlayoffGame $tournamentsChampionsleaguePlayoffGamesAtRight)
    {
        $this->tournaments_championsleague_playoff_games_at_right->removeElement($tournamentsChampionsleaguePlayoffGamesAtRight);
    }

    /**
     * Get tournaments_championsleague_playoff_games_at_right
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTournamentsChampionsleaguePlayoffGamesAtRight()
    {
        return $this->tournaments_championsleague_playoff_games_at_right;
    }

    /**
     * Set balance
     *
     * @param float $balance
     * @return User
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * Get balance
     *
     * @return float
     */
    public function getBalance()
    {
        return round($this->balance, 2);
    }

    /**
     * Set reserved
     *
     * @param float $reserved
     * @return User
     */
    public function setReserved($reserved)
    {
        $this->reserved = $reserved;

        return $this;
    }

    /**
     * Get reserved
     *
     * @return float
     */
    public function getReserved()
    {
        return round($this->reserved, 2);
    }

    /**
     * Add transaction_logs
     *
     * @param \AppBundle\Entity\TransactionLog $transactionLogs
     * @return User
     */
    public function addTransactionLog(\AppBundle\Entity\TransactionLog $transactionLogs)
    {
        $this->transaction_logs[] = $transactionLogs;

        return $this;
    }

    /**
     * Remove transaction_logs
     *
     * @param \AppBundle\Entity\TransactionLog $transactionLogs
     */
    public function removeTransactionLog(\AppBundle\Entity\TransactionLog $transactionLogs)
    {
        $this->transaction_logs->removeElement($transactionLogs);
    }

    /**
     * Get transaction_logs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTransactionLogs()
    {
        return $this->transaction_logs;
    }

    /**
     * Add transaction_logs_from
     *
     * @param \AppBundle\Entity\TransactionLog $transactionLogsFrom
     * @return User
     */
    public function addTransactionLogsFrom(\AppBundle\Entity\TransactionLog $transactionLogsFrom)
    {
        $this->transaction_logs_from[] = $transactionLogsFrom;

        return $this;
    }

    /**
     * Remove transaction_logs_from
     *
     * @param \AppBundle\Entity\TransactionLog $transactionLogsFrom
     */
    public function removeTransactionLogsFrom(\AppBundle\Entity\TransactionLog $transactionLogsFrom)
    {
        $this->transaction_logs_from->removeElement($transactionLogsFrom);
    }

    /**
     * Get transaction_logs_from
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTransactionLogsFrom()
    {
        return $this->transaction_logs_from;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     * @return User
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Sets avatar_file.
     *
     * @param UploadedFile $avatar_file
     */
    public function setAvatarFile(UploadedFile $avatar_file = null)
    {
        $this->avatar_file = $avatar_file;

        // check if we have an old image path
        if(is_file($this->getAbsolutePath())) {
            // store the old name to delete after the update
            $this->tempAvatarFile = $this->getAbsolutePath();
        } else {
            $this->avatar = 'initial';
        }
    }

    /**
     * Get avatar_file.
     *
     * @return UploadedFile
     */
    public function getAvatarFile()
    {
        return $this->avatar_file;
    }

    /**
     * Set registration_date
     *
     * @param \DateTime $registrationDate
     * @return User
     */
    public function setRegistrationDate($registrationDate)
    {
        $this->registration_date = $registrationDate;

        return $this;
    }

    /**
     * Get registration_date
     *
     * @return \DateTime
     */
    public function getRegistrationDate()
    {
        return $this->registration_date;
    }

    /**
     * Add tournaments_championship_rt_results
     *
     * @param \AppBundle\Entity\TournamentChampionshipRealTimeResult $tournamentsChampionshipRtResults
     * @return User
     */
    public function addTournamentsChampionshipRtResult(\AppBundle\Entity\TournamentChampionshipRealTimeResult $tournamentsChampionshipRtResults)
    {
        $this->tournaments_championship_rt_results[] = $tournamentsChampionshipRtResults;

        return $this;
    }

    /**
     * Remove tournaments_championship_rt_results
     *
     * @param \AppBundle\Entity\TournamentChampionshipRealTimeResult $tournamentsChampionshipRtResults
     */
    public function removeTournamentsChampionshipRtResult(\AppBundle\Entity\TournamentChampionshipRealTimeResult $tournamentsChampionshipRtResults)
    {
        $this->tournaments_championship_rt_results->removeElement($tournamentsChampionshipRtResults);
    }

    /**
     * Get tournaments_championship_rt_results
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTournamentsChampionshipRtResults()
    {
        return $this->tournaments_championship_rt_results;
    }

    /**
     * Add tournaments_championsleague_qualification_rt_results
     *
     * @param \AppBundle\Entity\TournamentChampionsleagueQualificationRealTimeResult $tournamentsChampionsleagueQualificationRtResults
     * @return User
     */
    public function addTournamentsChampionsleagueQualificationRtResult(\AppBundle\Entity\TournamentChampionsleagueQualificationRealTimeResult $tournamentsChampionsleagueQualificationRtResults)
    {
        $this->tournaments_championsleague_qualification_rt_results[] = $tournamentsChampionsleagueQualificationRtResults;

        return $this;
    }

    /**
     * Remove tournaments_championsleague_qualification_rt_results
     *
     * @param \AppBundle\Entity\TournamentChampionsleagueQualificationRealTimeResult $tournamentsChampionsleagueQualificationRtResults
     */
    public function removeTournamentsChampionsleagueQualificationRtResult(\AppBundle\Entity\TournamentChampionsleagueQualificationRealTimeResult $tournamentsChampionsleagueQualificationRtResults)
    {
        $this->tournaments_championsleague_qualification_rt_results->removeElement($tournamentsChampionsleagueQualificationRtResults);
    }

    /**
     * Get tournaments_championsleague_qualification_rt_results
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTournamentsChampionsleagueQualificationRtResults()
    {
        return $this->tournaments_championsleague_qualification_rt_results;
    }

    /**
     * Add tournamentsPlayoffQualificationRtResult
     *
     * @param \AppBundle\Entity\TournamentPlayoffQualificationRealTimeResult $tournamentsPlayoffQualificationRtResult
     *
     * @return User
     */
    public function addTournamentsPlayoffQualificationRtResult(\AppBundle\Entity\TournamentPlayoffQualificationRealTimeResult $tournamentsPlayoffQualificationRtResult)
    {
        $this->tournaments_playoff_qualification_rt_results[] = $tournamentsPlayoffQualificationRtResult;

        return $this;
    }

    /**
     * Remove tournamentsPlayoffQualificationRtResult
     *
     * @param \AppBundle\Entity\TournamentPlayoffQualificationRealTimeResult $tournamentsPlayoffQualificationRtResult
     */
    public function removeTournamentsPlayoffQualificationRtResult(\AppBundle\Entity\TournamentPlayoffQualificationRealTimeResult $tournamentsPlayoffQualificationRtResult)
    {
        $this->tournaments_playoff_qualification_rt_results->removeElement($tournamentsPlayoffQualificationRtResult);
    }

    /**
     * Get tournamentsPlayoffQualificationRtResults
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTournamentsPlayoffQualificationRtResults()
    {
        return $this->tournaments_playoff_qualification_rt_results;
    }

    /**
     * Set freeFunds
     *
     * @param float $freeFunds
     *
     * @return User
     */
    public function setFreeFunds($freeFunds)
    {
        $this->free_funds = $freeFunds;

        return $this;
    }

    /**
     * Get freeFunds
     *
     * @return float
     */
    public function getFreeFunds()
    {
        return round($this->free_funds, 2);
    }

    /**
     * Add billingOperation
     *
     * @param \AppBundle\Entity\BillingOperation $billingOperation
     *
     * @return User
     */
    public function addBillingOperation(\AppBundle\Entity\BillingOperation $billingOperation)
    {
        $this->billing_operations[] = $billingOperation;

        return $this;
    }

    /**
     * Remove billingOperation
     *
     * @param \AppBundle\Entity\BillingOperation $billingOperation
     */
    public function removeBillingOperation(\AppBundle\Entity\BillingOperation $billingOperation)
    {
        $this->billing_operations->removeElement($billingOperation);
    }

    /**
     * Get billingOperations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBillingOperations()
    {
        return $this->billing_operations;
    }

    /**
     * Add billingOperationsRecipient
     *
     * @param \AppBundle\Entity\BillingOperation $billingOperationsRecipient
     *
     * @return User
     */
    public function addBillingOperationsRecipient(\AppBundle\Entity\BillingOperation $billingOperationsRecipient)
    {
        $this->billing_operations_recipient[] = $billingOperationsRecipient;

        return $this;
    }

    /**
     * Remove billingOperationsRecipient
     *
     * @param \AppBundle\Entity\BillingOperation $billingOperationsRecipient
     */
    public function removeBillingOperationsRecipient(\AppBundle\Entity\BillingOperation $billingOperationsRecipient)
    {
        $this->billing_operations_recipient->removeElement($billingOperationsRecipient);
    }

    /**
     * Get billingOperationsRecipient
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBillingOperationsRecipient()
    {
        return $this->billing_operations_recipient;
    }

    /**
     * Set referralCode
     *
     * @param string $referralCode
     *
     * @return User
     */
    public function setReferralCode($referralCode)
    {
        $this->referral_code = $referralCode;

        return $this;
    }

    /**
     * Get referralCode
     *
     * @return string
     */
    public function getReferralCode()
    {
        return $this->referral_code;
    }

    /**
     * Set referralUser
     *
     * @param \AppBundle\Entity\User $referralUser
     *
     * @return User
     */
    public function setReferralUser(\AppBundle\Entity\User $referralUser = null)
    {
        $this->referral_user = $referralUser;

        return $this;
    }

    /**
     * Get referralUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getReferralUser()
    {
        return $this->referral_user;
    }

    /**
     * Set partnerLevel
     *
     * @param integer $partnerLevel
     *
     * @return User
     */
    public function setPartnerLevel($partnerLevel)
    {
        $this->partner_level = $partnerLevel;

        return $this;
    }

    /**
     * Get partnerLevel
     *
     * @return integer
     */
    public function getPartnerLevel()
    {
        return $this->partner_level;
    }

    /**
     * Add partnerCharge
     *
     * @param \AppBundle\Entity\PartnerCharge $partnerCharge
     *
     * @return User
     */
    public function addPartnerCharge(\AppBundle\Entity\PartnerCharge $partnerCharge)
    {
        $this->partner_charges[] = $partnerCharge;

        return $this;
    }

    /**
     * Remove partnerCharge
     *
     * @param \AppBundle\Entity\PartnerCharge $partnerCharge
     */
    public function removePartnerCharge(\AppBundle\Entity\PartnerCharge $partnerCharge)
    {
        $this->partner_charges->removeElement($partnerCharge);
    }

    /**
     * Get partnerCharges
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPartnerCharges()
    {
        return $this->partner_charges;
    }

    /**
     * Add referralCharge
     *
     * @param \AppBundle\Entity\PartnerCharge $referralCharge
     *
     * @return User
     */
    public function addReferralCharge(\AppBundle\Entity\PartnerCharge $referralCharge)
    {
        $this->referral_charges[] = $referralCharge;

        return $this;
    }

    /**
     * Remove referralCharge
     *
     * @param \AppBundle\Entity\PartnerCharge $referralCharge
     */
    public function removeReferralCharge(\AppBundle\Entity\PartnerCharge $referralCharge)
    {
        $this->referral_charges->removeElement($referralCharge);
    }

    /**
     * Get referralCharges
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReferralCharges()
    {
        return $this->referral_charges;
    }
}
