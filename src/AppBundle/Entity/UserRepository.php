<?php
namespace AppBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Config\Definition\Exception\Exception;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use \Doctrine\ORM\Tools\Pagination\Paginator;

class UserRepository extends FBRepository implements UserProviderInterface
{

    /**
    * Получает массив пользователей по фильтру
    *
    * @param array $arOrder Параметры сортировки
    * @param array $arFilter Фильтр
    * @param int $page Номер страницы
    * @param int $rowsPerPage Кол-во элементов на странице
    *
    * @return Doctrine\ORM\Tools\Pagination\Paginator
    */
    public function findPaginatorByFilter($arOrder, $arFilter, $page, $rowsPerPage = 10)
    {
        $offset = $rowsPerPage * ($page - 1);

        $q = $this
                ->createQueryBuilder('u')
                ->select('u')
                ->setFirstResult($offset)
                ->setMaxResults($rowsPerPage);

        if(!empty($arFilter["query"])) {
            $q = $q->andWhere('
                u.username LIKE :query OR
                u.first_name LIKE :query OR
                u.last_name LIKE :query OR
                u.email LIKE :query')
                ->setParameter('query', '%' . $arFilter["query"] . '%');
        }
        unset($arFilter["query"]);

        $q = $q->addCriteria($this->createCriteriaFromFilter($arFilter));

        if(!empty($arOrder))
        {
            foreach($arOrder as $sort => $order)
                $q->addOrderBy('u.'.$sort, $order);
        }

        $paginator = new Paginator($q);

        return $paginator;
    }

    /**
    * Ищет пользователя по ID
    *
    * @param int $id ID пользователя
    * @return \AppBundle\Entity\User
    */
    public function findById($id)
    {
        $q = $this
            ->createQueryBuilder('u')
            ->where('u.id = :id')
            ->setParameter('id', $id)
            ->getQuery();

        try {
            // The Query::getSingleResult() method throws an exception
            // if there is no record matching the criteria.
            $result = $q->getResult();
            if(isset($result[0]))
                return $result[0];
            else
                return null;
        } catch (NoResultException $e) {
            return null;
        }
    }

    /**
    * Ищет пользователя по email'у
    *
    * @param string $email E-mail пользователя
    *
    * @return \AppBundle\Entity\User
    */
    public function loadUserByUsername($email)
    {
        $q = $this
            ->createQueryBuilder('u')
            ->where('u.email = :email')
            ->setParameter('email', $email)
            ->getQuery();

        try {
            // The Query::getSingleResult() method throws an exception
            // if there is no record matching the criteria.
            $user = $q->getSingleResult();
        } catch (NoResultException $e) {
            $message = sprintf(
                'Unable to find an active admin AppBundle:User object identified by "%s".',
                $email
            );
            throw new UsernameNotFoundException($message, 0, $e);
        }

        return $user;
    }

    /**
    * Поиск пользователя по email
    *
    * @param string $email
    *
    * @return \AppBundle\Entity\User
    */
    public function findUserByEmail($email)
    {
        $q = $this
            ->createQueryBuilder('u')
            ->where('u.email = :email')
            ->setParameter('email', $email)
            ->getQuery();

        try {
            // The Query::getSingleResult() method throws an exception
            // if there is no record matching the criteria.
            $result = $q->getResult();
            if(isset($result[0]))
                return $result[0];
            else
                return null;
        } catch (NoResultException $e) {
            return null;
        }
    }

    /**
    * Поиск пользователя по никнейму
    *
    * @param string $username Никнейм
    * @param int $exceptUserId Не включать в поиск пользователя с данным ID
    * @return \AppBundle\Entity\User
    */
    public function findUserByUsername($username, $exceptUserId = false)
    {
        $q = $this
            ->createQueryBuilder('u')
            ->where('u.username = :username')
            ->setParameter('username', $username);

        if($exceptUserId)
        {
          $q = $q->andWhere('u.id != :user_id')->setParameter('user_id', $exceptUserId);
        }

        $q = $q->getQuery();

        try {
            // The Query::getSingleResult() method throws an exception
            // if there is no record matching the criteria.
            return $q->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        }
    }
    
    /**
    * Возвращает пользователей турнира.
    * 
    * @param int   $tournamentId Идентификатор турнира.
    * @param array $filter       Массив с фильтром.
    * @param array $order        Массив с сортировкой.
    * 
    * @return User[]
    */
    public function findByTournament($tournamentId, array $filter = array(), array $order = array())
    {
        $q = $this->createQueryBuilder('u')
            ->leftJoin('u.tournament_users', 't')
            ->where('t.id = :tournamentId')
            ->setParameter('tournamentId', $tournamentId);           
        
        $q->addCriteria($this->createCriteriaFromFilter($filter));        
                
        $q->addOrderBy($this->createOrderBy($order, 'u', array('id' => 'ASC')));
            
        $q = $q->getQuery();
        
        try {
            return $q->getResult();
        } catch (NoResultException $e) {
            return array();
        }
    }

    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(
                sprintf(
                    'Instances of "%s" are not supported.',
                    $class
                )
            );
        }

        return $this->find($user->getId());
    }

    public function supportsClass($class)
    {
        return $this->getEntityName() === $class
            || is_subclass_of($class, $this->getEntityName());
    }

    /**
    * Проверяет пользователя на уникальность
    *
    * @param UserInterface $user
    *
    * @return bool
    */
    public function userUniqueCheck(UserInterface $user)
    {
        $em = $this->getEntityManager();

        //check email
        $findedUser = $em->getRepository('AppBundle\Entity\User')->findOneBy(array('email' => $user->getEmail()));
        if(!empty($findedUser))
        {
            throw new Exception("email");
            return false;
        }

        return true;
    }

    /**
    * Ищет пользователя по хэшу
    *
    * @param string $hash MD5 от email'а
    *
    * @return \AppBundle\Entity\User
    */
    public function loadUserByHash($hash)
    {
        $q = $this
            ->createQueryBuilder('u')
            ->where('md5(u.email) = :hash')
            ->setParameter('hash', $hash)
            ->getQuery();

        try {
            // The Query::getSingleResult() method throws an exception
            // if there is no record matching the criteria.
            $user = $q->getSingleResult();
        } catch (NoResultException $e) {
            $message = sprintf(
                'Unable to find an active admin AppBundle:User object identified by "%s".',
                $hash
            );
            throw new UsernameNotFoundException($message, 0, $e);
        }

        return $user;
    }

    public function isEmailUnique($email)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT COUNT(u.id) cnt FROM AppBundle\Entity\User u
                                    WHERE u.email = :email")
                    ->setParameter("email", $email);

        try {
            $result = $query->getSingleResult();
            return (bool) !$result["cnt"];
        } catch (NoResultException $e) {
            return false;
        }
    }

    /**
    * Ищет пользователя по полю referral_code
    * Пользователь должен быть активен
    *
    * @param string $referralCode Реферальный код
    * @return null|\AppBundle\Entity\User
    */
    public function findUserByReferralCode($referralCode)
    {
      $q = $this->createQueryBuilder("u")
                ->where('u.referral_code = :referralCode')
                ->setParameter("referralCode", $referralCode)
                ->andWhere('u.isActive = :isActive')
                ->setParameter("isActive", true)
                ->getQuery();

      try {
            return $q->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    /**
    * Возвращает неактивных пользователей зарегистрированных ранее даты $registrationDate
    * и которые не подтвердили свой аккаунт по email.
    *
    * @param \DateTime $registrationDate Дата.
    *
    * @return \AppBundle\Entity\User[]
    */
    public function findForRemoveByDate(\DateTime $registrationDate)
    {
        $registrationDate->setTime(0, 0, 0);

        $q = $this->createQueryBuilder("u")
                ->where('u.isActive = 0 AND u.isConfirmed = 0')
                ->andWhere('u.registration_date <= :regDate')
                ->setParameter('regDate', $registrationDate)
                ->getQuery();

      try {
            return $q->getResult();
        } catch (NoResultException $e) {
            return array();
        }
    }

    /**
    * Находит пользователей "ботов", которых нет в турнире.
    *
    * @param int $tournamentId Идентификатор турнира
    *
    * @return \AppBundle\Entity\User[]
    */
    public function findBotsNotInTournament($tournamentId)
    {
        $subQ = $this->createQueryBuilder("u")
            ->select('u.id')
            ->leftJoin('u.tournament_users', 't')
            ->where('u.role = :botRole')            
            ->andWhere('u.isActive = 1')
            ->andWhere('t.id = :tournamentId')
            ->setParameter('tournamentId', $tournamentId);
            
        $q = $this->createQueryBuilder('u2');
        $q = $q->where($q->expr()->notIn('u2.id', $subQ->getDQL()))->andWhere('u2.role = :botRole');
        $q = $q->setParameter('botRole', User::ROLE_BOT)->setParameter('tournamentId', $tournamentId);
        $q = $q->getQuery();
                
        try {
            return $q->getResult();
        } catch (NoResultException $e) {
            return array();
        }
    }

    public function loadUsersList($filter = false)
    {
        $q = $this->createQueryBuilder('u');

        $q = $q->getQuery();

        $result = $user = $q->getResult();
        return $result;
    }

    public function loadUserById($id)
    {
        $q = $this
            ->createQueryBuilder('u')
            ->where('u.id = :id')
            ->setParameter('id', $id)
            ->getQuery();

        try {
            $user = $q->getSingleResult();
        } catch (NoResultException $e) {
            $message = sprintf(
                'Unable to find an active admin AppBundle:User object identified by "%s".',
                $id
            );
            throw new UsernameNotFoundException($message, 0, $e);
        }

        return $user;
    }

    public function search($query)
    {
        $q = $this
            ->createQueryBuilder("u")
            ->where("u.username LIKE :query OR u.first_name LIKE :query OR u.last_name LIKE :query OR u.email LIKE :query")
            ->andWhere("u.isActive = 1")
            ->setParameter('query', $query.'%')
            ->getQuery();

        try {
            return $q->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    public function findTournamentUsersOrderedByRating($tournament_id)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT u
                                      FROM AppBundle\Entity\User u
                                      LEFT JOIN u.tournament_users t
                                      WHERE t.id = :tournament_id
                                      ORDER BY u.rating DESC
                                  ")
                                  ->setParameter("tournament_id", $tournament_id);

        try {
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    public function getRatingList($maxCount)
    {
        $q = $this->createQueryBuilder('u')
                  ->where('u.isActive = :is_active')
                  ->setParameter('is_active', true)
                  ->orderBy('u.rating', 'DESC')
                  ->addOrderBy('u.id', 'ASC');

        $q->setFirstResult(0);
        $q->setMaxResults($maxCount);

        $q = $q->getQuery();

        try {
            return $q->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    public function findChampionshipUsers($tournament_id)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT u
                                        FROM AppBundle\Entity\User u
                                        LEFT JOIN u.tournament_users t
                                        WHERE t.id = :tournament_id
                                        GROUP BY u.id
                                        ORDER BY u.rating DESC")
                                        ->setParameter("tournament_id", $tournament_id);

        try {
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    public function findChampionshipUsersCount($tournament_id)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT COUNT(u.id) as cnt
                                        FROM AppBundle\Entity\User u
                                        LEFT JOIN u.tournament_users t
                                        WHERE t.id = :tournament_id")
                                        ->setParameter("tournament_id", $tournament_id);

        try {
            $result = $query->getSingleResult();
            return $result["cnt"];
        } catch (NoResultException $e) {
            return null;
        }
    }

    public function getAvailableFunds()
    {
        $query = $this->createQueryBuilder("u")
                ->select("u.balance as funds")
                ->where("u.id = 1")
                ->getQuery();

       try {
            $result = $query->getSingleResult();
            return $result["funds"];
        } catch (NoResultException $e) {
            return null;
        }
    }

    public function getOnUserAccountsFunds()
    {
        $query = $this->createQueryBuilder("u")
                ->select("SUM(u.balance) as funds")
                ->getQuery();

       try {
            $result = $query->getSingleResult();
            return $result["funds"];
        } catch (NoResultException $e) {
            return null;
        }
    }

    public function getBlockedFunds()
    {
        $query = $this->createQueryBuilder("u")
                ->select("SUM(u.reserved) as funds")
                ->getQuery();

       try {
            $result = $query->getSingleResult();
            return $result["funds"];
        } catch (NoResultException $e) {
            return null;
        }
    }

    public function getRegistredUsersCountByPeriod(\DateTime $startDateTime, \DateTime $endDateTime)
    {
        $query = $this->createQueryBuilder("u")
                ->select("COUNT(u.id) as cnt")
                ->where("u.registration_date >= :startDateTime AND u.registration_date <= :endDateTime")
                ->setParameters(array("startDateTime" => $startDateTime, "endDateTime" => $endDateTime))
                ->getQuery();

       try {
            $result = $query->getSingleResult();
            return $result["cnt"];
        } catch (NoResultException $e) {
            return null;
        }
    }
}