<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AppBundle\Entity\WinRule
 *
 * @ORM\Table(name="win_rules")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\WinRuleRepository")
 */
class WinRule
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")     
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="WinRulesGroup", inversedBy="rules", cascade={"persist"})
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
    protected $group;

    /**
     * @ORM\Column(type="integer")
     */
    private $place;

    /**
     * @ORM\Column(type="float")
     */
    private $percent;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set place
     *
     * @param integer $place
     * @return WinRule
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return integer 
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set percent
     *
     * @param float $percent
     * @return WinRule
     */
    public function setPercent($percent)
    {
        $this->percent = $percent;

        return $this;
    }

    /**
     * Get percent
     *
     * @return float 
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * Set group
     *
     * @param \AppBundle\Entity\WinRulesGroup $group
     * @return WinRule
     */
    public function setGroup(\AppBundle\Entity\WinRulesGroup $group = null)
    {        
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \AppBundle\Entity\WinRulesGroup 
     */
    public function getGroup()
    {
        return $this->group;
    }
}
