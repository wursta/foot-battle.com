<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AppBundle\Entity\WinRulesGroup
 *
 * @ORM\Table(name="win_rules_groups")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\WinRulesGroupRepository")
 */
class WinRulesGroup
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")     
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="TournamentFormat", inversedBy="win_rules_groups")
     * @ORM\JoinColumn(name="format_id", referencedColumnName="id")
     */
    protected $format;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min = 1)
     */
    private $min_users;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min = 2)
     */
    private $max_users;

    /**
     * @ORM\OneToMany(targetEntity="WinRule", mappedBy="group", cascade={"persist"})
     */
    protected $rules;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->rules = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set min_users
     *
     * @param integer $minUsers
     * @return WinRulesGroup
     */
    public function setMinUsers($minUsers)
    {
        $this->min_users = $minUsers;

        return $this;
    }

    /**
     * Get min_users
     *
     * @return integer 
     */
    public function getMinUsers()
    {
        return $this->min_users;
    }

    /**
     * Set max_users
     *
     * @param integer $maxUsers
     * @return WinRulesGroup
     */
    public function setMaxUsers($maxUsers)
    {
        $this->max_users = $maxUsers;

        return $this;
    }

    /**
     * Get max_users
     *
     * @return integer 
     */
    public function getMaxUsers()
    {
        return $this->max_users;
    }

    /**
     * Set format
     *
     * @param \AppBundle\Entity\TournamentFormat $format
     * @return WinRulesGroup
     */
    public function setFormat(\AppBundle\Entity\TournamentFormat $format = null)
    {
        $this->format = $format;

        return $this;
    }

    /**
     * Get format
     *
     * @return \AppBundle\Entity\TournamentFormat 
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Add rules
     *
     * @param \AppBundle\Entity\WinRule $rules
     * @return WinRulesGroup
     */
    public function addRule(\AppBundle\Entity\WinRule $rules)
    {
        $this->rules[] = $rules;

        return $this;
    }

    /**
     * Remove rules
     *
     * @param \AppBundle\Entity\WinRule $rules
     */
    public function removeRule(\AppBundle\Entity\WinRule $rules)
    {
        $this->rules->removeElement($rules);
    }

    /**
     * Get rules
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRules()
    {        
        return $this->rules;
    }
}
