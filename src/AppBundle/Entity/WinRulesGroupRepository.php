<?php
namespace AppBundle\Entity;

/**
 * WinRulesGroupRepository
 *
 */
class WinRulesGroupRepository extends FBRepository
{
    /**
    * Возвращает распределения призовых по формату
    * 
    * @param int $formatId ID формата
    * 
    * @return \AppBundle\Entity\WinRulesGroup[]
    */
    public function findByFormat($formatId)
    {
        $q = $this->createQueryBuilder('wrg')
            ->where('wrg.format = :formatId')
            ->setParameter("formatId", $formatId)
            ->orderBy('wrg.min_users', 'ASC')
            ->getQuery();

        try {
            return $q->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    public function findById($id)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT wrg FROM AppBundle\Entity\WinRulesGroup wrg
                                    WHERE wrg.id = :id")
                    ->setParameter("id", $id);

        try {
            return $query->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        }
    }       
    
    public function findWithFormats()
    {
        $query = $this->createQueryBuilder("wrg")
                     ->addSelect("f, wr")
                     ->leftJoin("wrg.format", "f")
                     ->leftJoin("wrg.rules", "wr")
                     ->getQuery();
                      
        try {
            return $query->getResult();
        } catch (NoResultException $e) {
            return null;
        }
    }
}
