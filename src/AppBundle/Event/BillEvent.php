<?php
namespace AppBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class BillEvent extends Event
{
    /**    
    * @var \AppBundle\Entity\BillingOperation
    */
    protected $bill;    

    public function __construct($bill)
    {
        $this->bill = $bill;        
    }

    public function getBill()
    {
        return $this->bill;
    }    
}