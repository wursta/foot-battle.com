<?php
namespace AppBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class TournamentEvent extends Event
{
    protected $tournament;    

    public function __construct($tournament)
    {
        $this->tournament = $tournament;        
    }
    
    public function getTournament()
    {
        return $this->tournament;
    }    
}