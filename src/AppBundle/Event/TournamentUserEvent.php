<?php
namespace AppBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class TournamentUserEvent extends Event
{
    protected $user;    

    public function __construct($user, $tournament)
    {
        $this->user = $user;        
        $this->tournament = $tournament;
    }

    public function getUser()
    {
        return $this->user;
    }    
    
    public function getTournament()
    {
      return $this->tournament;
    }
}