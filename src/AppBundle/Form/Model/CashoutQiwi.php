<?php
namespace AppBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

class CashoutQiwi
{
    protected $qiwi_phone;
    
    protected $amount;
    
    public function getQiwiPhone()
    {
        return $this->qiwi_phone;
    }        
    
    public function setQiwiPhone($qiwi_phone)
    {
        $this->qiwi_phone = $qiwi_phone;
    }
    
    public function getAmount()
    {
        return $this->amount;
    }
    
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }
}
?>
