<?php
namespace AppBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

class CashoutRequest
{    
    protected $card_number;    
    protected $bank;        
    protected $amount;
    protected $fio;
    protected $birthdate;
    
    public function getCardNumber()
    {
        return $this->card_number;
    }
    
    public function setCardNumber($card_number)
    {
        $this->card_number = $card_number;
        
        return $this;
    }
    
    public function getBank()
    {
      return $this->bank;
    }
    
    public function setBank($bank)
    {
      $this->bank = $bank;
      
      return $this;
    }    
    
    public function getAmount()
    {
        return $this->amount;
    }
    
    public function setAmount($amount)
    {
        $this->amount = $amount;
        
        return $this;
    }
    
    public function getFio()
    {
      return $this->fio;
    }
    
    public function setFio($fio)
    {
      $this->fio = $fio;
      
      return $this;
    }
    
    public function getBirthdate()
    {
      return $this->birthdate;
    }
    
    public function setBirthdate($birthdate)
    {
      $this->birthdate = $birthdate;
      
      return $this;
    }
}
?>
