<?php
namespace AppBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

class Feedback
{
    protected $created;
    
    protected $ip_address;
            
    /**
     * @Assert\NotBlank()    
     */
    protected $name;
    
    /**
     * @Assert\NotBlank()    
     */
    protected $email;
    
    /**
     * @Assert\NotBlank()    
     */
    protected $subject;
    
    /**
     * @Assert\NotBlank()    
     */
    protected $message;
    
    public function getCreated()
    {
        return $this->created;
    }
    
    public function setCreated($created)
    {
        $this->created = $created;
    }
    
    public function getIpAddress()
    {
        return $this->ip_address;
    }
    
    public function setIpAddress($ip_address)
    {
        $this->ip_address = $ip_address;
    }    
        
    public function getName()
    {
        return $this->name;
    }
    
    public function setName($name)
    {
        $this->name = $name;
    }
    
    public function getEmail()
    {
        return $this->email;
    }
    
    public function setEmail($email)
    {
        $this->email = $email;
    }
    
    public function getSubject()
    {
        return $this->subject;
    }
    
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }
    
    public function getMessage()
    {
        return $this->message;
    }
    
    public function setMessage($message)
    {
        $this->message = $message;
    }
}
?>
