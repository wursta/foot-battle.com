<?php
namespace AppBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

class RefillQiwi
{
    /**
     * @Assert\NotBlank
     */
    protected $qiwi_phone;

    /**
     * @Assert\NotBlank
     */
    protected $amount;

    public function getQiwiPhone()
    {
        return $this->qiwi_phone;
    }

    public function setQiwiPhone($qiwi_phone)
    {
        $this->qiwi_phone = $qiwi_phone;
    }

    /**
    * Округляет сумму до 2х знаков после запятой
    * @return float
    */
    public function getAmount()
    {
        return round((float) $this->amount, 2);
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;
    }
}
?>
