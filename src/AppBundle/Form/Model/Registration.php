<?php
namespace AppBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\User;

class Registration
{
    /**
     * @var \AppBundle\Entity\User
     *
     * @Assert\Type(type="AppBundle\Entity\User")
     * @Assert\Valid()
     */
    protected $user;

    /**
     * @Assert\NotBlank()
     * @Assert\True()
     */
    protected $termsAccepted;

    protected $referralCode;

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getTermsAccepted()
    {
        return $this->termsAccepted;
    }

    public function setTermsAccepted($termsAccepted)
    {
        $this->termsAccepted = (boolean) $termsAccepted;
    }

    public function getReferralCode()
    {
        return $this->referralCode;
    }

    public function setReferralCode($referralCode)
    {
        $this->referralCode = $referralCode;
    }
}