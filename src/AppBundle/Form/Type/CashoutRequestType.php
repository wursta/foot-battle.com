<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CashoutRequestType extends AbstractType
{    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('card_number', 'text');
        $builder->add('bank', 'text');
        $builder->add('amount', 'number');
        $builder->add('fio', 'text');
        $builder->add('birthdate', 'date', array(
          'years' => range(date("Y") - 70, date("Y") - 18)
        ));
        $builder->add('submit', 'submit');        
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(            
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
            // a unique key to help generate the secret token
            'intention'       => 'cashout_request',
        ));
    }
    
    public function getName()
    {
        return 'cashout_request';
    }
}