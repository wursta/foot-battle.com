<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Doctrine\ORM\EntityRepository;

class MatchSearchType extends AbstractType
{
    private $tournamentId = null;
    
    public function __construct($tournamentId)
    {      
      $this->tournamentId = $tournamentId;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      $tournamentId = $this->tournamentId;      
      $builder->add('league', 'entity', array(
            "class" => 'AppBundle\Entity\League',            
            'query_builder' => function(EntityRepository $er) use ($tournamentId) {
              return $er->createQueryBuilder('l')
                            ->select('l')
                            ->leftJoin('l.tournaments', 't')
                            ->where("l.isActive = :is_active")
                            ->setParameter("is_active", true)
                            ->andWhere("t.id = :tournament_id")
                            ->setParameter("tournament_id", $tournamentId)
                            ->orderBy('l.country', 'ASC');
            }
        ));
        
        $builder->add('search', 'submit');
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(            
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
            // a unique key to help generate the secret token
            'intention'       => 'match_search',
        ));
    }
    
    public function getName()
    {
        return 'match_search';
    }
}