<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PasswordRecoveryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {                
        $builder->add('email', 'email');

        $builder->add('submit', 'submit');
    }    

    public function getName()
    {
        return 'password_recovery';
    }
}