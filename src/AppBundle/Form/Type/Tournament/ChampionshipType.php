<?php
namespace AppBundle\Form\Type\Tournament;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use AppBundle\Services\Tournaments\Championship;

class ChampionshipType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {    
    $builder->add('rounds_count', 'integer', array(      
      'attr' => array(
          'min' => Championship::MIN_ROUNDS_COUNT
        )
      )
    );

    $builder->add('matches_in_round', 'integer', array(      
      'attr' => array(
          'min' => Championship::MIN_MATCHES_IN_ROUND
        )
      )
    );

    $builder->add('max_users_count', 'integer', array(      
      'attr' => array(
          'min' => Championship::MIN_USERS_COUNT, 
          'max' => Championship::MAX_USERS_COUNT
        )
      )
    );
    
    $builder->add('save', 'submit');
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $resolver->setDefaults(array(            
      'csrf_protection' => true,
      'csrf_field_name' => '_token',
      // a unique key to help generate the secret token
      'intention'       => 'tournament_championship_edit',
    ));
  }

  public function getName()
  {
    return 'tournament_championship_edit';
  }
}
