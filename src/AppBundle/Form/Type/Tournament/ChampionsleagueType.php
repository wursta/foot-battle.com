<?php
namespace AppBundle\Form\Type\Tournament;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use AppBundle\Services\Tournaments\Championsleague;

class ChampionsleagueType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder->add('qualification_rounds_count', 'integer', array(
      'attr' => array(
          'min' => Championsleague::MIN_QUALIFICATION_ROUNDS_COUNT
          )
      )
    );

    $builder->add('matches_in_qualification', 'integer', array(        
      'attr' => array(
        'min' => Championsleague::MIN_MATCHES_IN_QUALIFICATION
        )
      )
    );
    
    $builder->add('matches_in_game', 'integer', array(        
      'attr' => array(
        'min' => Championsleague::MIN_MATCHES_IN_GROUP_GAME
        )
      )
    );

    $builder->add('matches_in_playoff', 'integer', array(        
      'attr' => array(
        'min' => Championsleague::MIN_MATCHES_IN_PLAYOFF_GAME
        )
      )
    );
    
    $usersCountChoices = Championsleague::getPossibleUsersCounts();
        
    $builder->add('max_users_count', 'choice', array(
      'choices' => $usersCountChoices,        
      )
    );
    
    $groupsCountChoices = Championsleague::getPossibleGroupsCounts();
    $builder->add('max_groups_count', 'choice', array(
        'choices' => $groupsCountChoices,        
        )
    );

    
    $outOfGroupChoices = Championsleague::getPossibleOutOfGroupUsersCounts();    
    $builder->add('users_out_of_group', 'choice', array(
      'choices' => $outOfGroupChoices,        
      )
    );
    
    $builder->add('save', 'submit');
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $resolver->setDefaults(array(            
      'csrf_protection' => true,
      'csrf_field_name' => '_token',
      // a unique key to help generate the secret token
      'intention'       => 'tournament_championsleague_edit',
    ));
  }

  public function getName()
  {
    return 'tournament_championsleague_edit';
  }
}
