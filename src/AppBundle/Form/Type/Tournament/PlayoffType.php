<?php
namespace AppBundle\Form\Type\Tournament;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use AppBundle\Services\Tournaments\Playoff;

class PlayoffType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder->add('qualification_rounds_count', 'integer', array(
      'attr' => array(
        'min' => Playoff::MIN_QUALIFICATION_ROUNDS_COUNT
        )
      )
    );

    $builder->add('matches_in_qualification', 'integer', array(        
      'attr' => array(
        'min' => Playoff::MIN_MATCHES_IN_QUALIFICATION
        )
      )
    );
    
    $builder->add('matches_in_game', 'integer', array(        
      'attr' => array(
        'min' => Playoff::MIN_MATCHES_IN_GAME
        )
      )
    );
    
    
    $usersCountChoices = Playoff::getPossibleUsersCounts();
        
    $builder->add('max_users_count', 'choice', array(
      'choices' => $usersCountChoices
      )
    );
    
    $builder->add('save', 'submit');
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $resolver->setDefaults(array(            
      'csrf_protection' => true,
      'csrf_field_name' => '_token',
      // a unique key to help generate the secret token
      'intention'       => 'tournament_playoff_edit',
    ));
  }

  public function getName()
  {
    return 'tournament_playoff_edit';
  }
}
