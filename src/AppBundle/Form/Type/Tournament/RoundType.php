<?php
namespace AppBundle\Form\Type\Tournament;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use AppBundle\Services\Tournaments\Round;

class RoundType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder->add('matches_in_game', 'integer', array(        
      'attr' => array(
        'min' => Round::MIN_MATCHES_IN_GAME
        )
      )
    );
    
    $builder->add('max_users_count', 'integer', array(        
      'attr' => array(
        'min' => Round::MIN_USERS_COUNT,
        'max' => Round::MAX_USERS_COUNT
        )
      )
    );
    
    $builder->add('save', 'submit');
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $resolver->setDefaults(array(            
      'csrf_protection' => true,
      'csrf_field_name' => '_token',
      // a unique key to help generate the secret token
      'intention'       => 'tournament_round_edit',
    ));
  }

  public function getName()
  {
    return 'tournament_round_edit';
  }
}
