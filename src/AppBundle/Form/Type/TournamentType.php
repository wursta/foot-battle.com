<?php
namespace AppBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use AppBundle\Admin\Form\Type\LeagueType;

class TournamentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {        
        $builder->add('format', 'entity', array(
            'class' => 'AppBundle\Entity\TournamentFormat',
            'property' => 'title'
        ));
        
        $builder->add('title', 'text', array("required" => false));            
        $builder->add('fee', 'number');
        $builder->add('initBudget', 'number', array(
          "data" => 0
        ));
        $builder->add('commission', 'number', array(
            "data" => 10
        ));
        
        $builder->add('geography', 'entity', array(
            'class' => 'AppBundle\Entity\TournamentGeography',
            'property' => 'title'
        ));
        
        $builder->add('leagues', 'entity', array(
            "class" => 'AppBundle\Entity\League',
            'query_builder' => function(EntityRepository $er) {
                $query = $er->createQueryBuilder('l')
                            ->where("l.isActive = :is_active")
                            ->setParameter("is_active", true)
                            ->orderBy('l.country', 'ASC');                    
                
                return $query;
            },
            "multiple" => true,
            'expanded' => true
        ));
        
        $builder->add('isPromo', 'checkbox', array("required" => false));
        
        $builder->add('is_open', 'checkbox', array("required" => false));
        $builder->add('with_confirmation', 'checkbox', array("required" => false));            
        
        /*$builder->add('users_count_type', 'choice', array(
            'choices' => array('LIMIT' => 'Ограниченно', 'UNLIMIT' => 'Неограниченно')
        ));*/
        
        $builder->add('save', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            // a unique key to help generate the secret token
            'intention'       => 'tournament',

        ));
    }
    
    public function getName()
    {
        return 'tournament';
    }    
}