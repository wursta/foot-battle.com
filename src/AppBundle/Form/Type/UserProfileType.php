<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserProfileType extends AbstractType
{
    private $user;
    
    public function __construct(\AppBundle\Entity\User $user)
    {
      $this->user = $user;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {                
        $builder->add('nick', 'text', array("data" => $this->user->getNick()));
        $builder->add('first_name', 'text', array("required" => false, "data" => $this->user->getFirstName()));
        $builder->add('last_name', 'text', array("required" => false, "data" => $this->user->getLastName()));
        $builder->add('second_name', 'text', array("required" => false, "data" => $this->user->getSecondName()));
        $builder->add('birthdate', 'birthday', array("required" => false, "data" => $this->user->getBirthdate()));
        $builder->add('country', 'country', array("required" => false, "data" => $this->user->getCountry()));
        $builder->add('city', 'text', array("required" => false, "data" => $this->user->getCity()));

        $builder->add('avatar_file', 'file', array("required" => false));

        $builder->add('avatar_delete', 'checkbox', array("required" => false, "mapped" => false));

        $builder->add('save', 'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
            // a unique key to help generate the secret token
            'intention'       => 'user_profile',
        ));
    }

    public function getName()
    {
        return 'user_profile';
    }
}