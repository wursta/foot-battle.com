fbAdminApp.controller('AdminBillingLogCtrl', ['$scope', 'AdminBilling', 'Utils', function($scope, AdminBilling, Utils) {

    $scope.billingLog = {
        currentPage: 1,
        pageSize: 20,
        totalItems: 0,
        totalPages: 0,
        logItems: [],
        filter: {

        },
        order: {
            operation_datetime: "DESC"
        }
    };

    /**
    * Загружает страницу операций
    *
    * @param {Integer} page Номер страницы
    */
    $scope.billingLog.loadPage = function(page) {
        AdminBilling.loadOperationsLog(page, $scope.billingLog.filter, $scope.billingLog.order).then(function(response){
            $scope.billingLog.totalItems = response.total;
            $scope.billingLog.logItems = response.items;
        });
    }

}]);