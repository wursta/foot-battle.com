fbAdminApp.controller('AdminPoliciesPointsSchemasAddCtrl', ['$scope', function($scope) {

    $scope.pointsScheme = {
        base: 0,
        addDiff0: 0,
        addDiff1: 0,
        addDiff2: 0,
        addDiff3: 0,
        addDiff4: 0,
        correctScores: []
    };

    $scope.pointsScheme.addCorrectScore = function(data) {
        if(data == undefined) {
            data = {
                score: null,
                points: null
            };
        }

        $scope.pointsScheme.correctScores.push(data);
    }

}]);