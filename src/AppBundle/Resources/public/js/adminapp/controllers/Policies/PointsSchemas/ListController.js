fbAdminApp.controller('AdminPoliciesPointsSchemasListCtrl', ['$scope', 'AdminPoliciesPointsSchemas', 'Utils', function($scope, AdminPoliciesPointsSchemas, Utils) {

    $scope.pointsSchemas = {
        currentPage: 1,
        pageSize: 20,
        total: 0,
        totalPages: 0,
        items: [],
        filter: {},
        order: {
            created: "ASC"
        }
    };

    /**
    * Загружает страницу схем начисления очков
    *
    * @param {Integer} page Номер страницы
    */
    $scope.pointsSchemas.loadPage = function(page) {
        AdminPoliciesPointsSchemas.loadPage(page, $scope.pointsSchemas.filter, $scope.pointsSchemas.order)
            .then(function(response) {
                $scope.pointsSchemas.total = response.total;
                $scope.pointsSchemas.items = response.items;
            });
    }

    $scope.pointsSchemas.view = function(pointsSchemeId) {
        AdminPoliciesPointsSchemas.view(pointsSchemeId);
    }

}]);