fbAdminApp.controller('AdminPoliciesRegulationsAddCtrl',
    [
        '$scope',
        'AdminPoliciesRegulations',
        'AdminUtils',
        function(
            $scope,
            AdminPoliciesRegulations,            
            AdminUtils) {

    $scope.regulations = {
        tinymceOptions: AdminUtils.getTinymceOptions(),        
        localeDisabled: false,
        tournamentFormatDisabled: false,        
        fields: {
            locale: null,
            tournamentFormat: null,
            text: null,            
        }
    };

    $scope.$watch('regulations.fields.tournamentFormat', function(newVal, oldVal) {                
        $scope.regulations.loadDefaultByType(newVal);
    });
    
    /**
    * Подгружает поля из реглмента турнира заданного типа по-умолчанию
    * 
    * @param {Integer} tournamentFormatId ID формата турнира
    */
    $scope.regulations.loadDefaultByType = function(tournamentFormatId)
    {
        if($scope.regulations.fields.text.length == 0 || confirm('Вы уверены, что хотите сменить формат турнира? Заполненный текст регламента будет заменён текстом по-умолчанию.')){
            AdminPoliciesRegulations.getDefault(tournamentFormatId).then(function(data) {
                $scope.regulations.fields.text = data.text;                
            });
        }
    }    

}]);