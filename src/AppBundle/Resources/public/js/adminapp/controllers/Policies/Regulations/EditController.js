fbAdminApp.controller('AdminPoliciesRegulationsEditCtrl',
    [
        '$scope',
        'AdminPoliciesRegulations',
        'AdminUtils',
        function(
            $scope,
            AdminPoliciesRegulations,            
            AdminUtils) {

    $scope.regulations = {
        tinymceOptions: AdminUtils.getTinymceOptions(),        
        localeDisabled: true,
        tournamentFormatDisabled: true,
        fields: {
            locale: null,
            tournamentFormat: null,
            text: null,            
        }
    };
}]);