fbAdminApp.controller('AdminPoliciesRegulationsListCtrl', ['$scope', 'AdminPoliciesRegulations', function($scope, AdminPoliciesRegulations) {

    $scope.regulations = {
        currentPage: 1,
        pageSize: 20,
        total: 0,
        totalPages: 0,
        editions: [],
        filter: {},
        order: {            
            id: "ASC"
        }
    };

    /**
    * Загружает страницу с редакциями регламентов турниров
    *
    * @param {Integer} page Номер страницы
    */
    $scope.regulations.loadPage = function(page) {
        AdminPoliciesRegulations.loadPage(page, $scope.regulations.filter, $scope.regulations.order)
            .then(function(response) {
                $scope.regulations.total = response.total;
                $scope.regulations.editions = response.items;
            });
    }

    $scope.regulations.view = function(editionId) {
        AdminPoliciesRegulations.view(editionId);
    }

}]);