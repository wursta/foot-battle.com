fbAdminApp.controller('AdminPoliciesTermsAddCtrl',
    [
        '$scope',
        'AdminPoliciesTerms',
        'AdminPoliciesPointsSchemas',
        'AdminPoliciesRegulations',
        'AdminUtils',
        function(
            $scope,
            AdminPoliciesTerms,
            AdminPoliciesPointsSchemas,
            AdminPoliciesRegulations,
            AdminUtils) {

    $scope.terms = {
        tinymceOptions: AdminUtils.getTinymceOptions(),
        pointsSchemeDisabled: false,
        localeDisabled: false,
        regulationsDisabled: false,
        fields: {
            locale: null,
            terms: null,
            regulations: null,
            privacy: null,
            pointsSchemeId: null,
            regulationChampionshipId: null,
            regulationRoundId: null,
            regulationPlayoffId: null,
            regulationChampionsleagueId: null
        }
    };

    /**
    * Подгружает поля из пользовательского соглашения по-умолчанию
    */
    $scope.terms.loadDefault = function()
    {
        AdminPoliciesTerms.getDefault().then(function(data) {            
            $scope.terms.fields.pointsSchemeId = data.pointsSchemeId.toString();
            $scope.terms.fields.regulationChampionshipId = data.regulationChampionshipId.toString();
            $scope.terms.fields.regulationRoundId = data.regulationRoundId.toString();
            $scope.terms.fields.regulationPlayoffId = data.regulationPlayoffId.toString();
            $scope.terms.fields.regulationChampionsleagueId = data.regulationChampionsleagueId.toString();
            $scope.terms.fields.terms = data.terms;
            $scope.terms.fields.regulations = data.regulations;
            $scope.terms.fields.privacy = data.privacy;
        });
    }

    /**
    * Открывает модальное окно с выбраной схемой начисления очков
    */
    $scope.terms.viewSelectedPointsScheme = function()
    {
        AdminPoliciesPointsSchemas.view($scope.terms.fields.pointsSchemeId);
    }    

    /**
    * Открывает модальное окно с выбраным регламентом турнира
    */
    $scope.terms.viewSelectedRegulation = function(id)
    {
        AdminPoliciesRegulations.view(id);
    }
    
}]);