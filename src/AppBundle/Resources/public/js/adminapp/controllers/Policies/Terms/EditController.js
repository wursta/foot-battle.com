fbAdminApp.controller('AdminPoliciesTermsEditCtrl',
    [
        '$scope',
        'AdminPoliciesTerms',
        'AdminPoliciesPointsSchemas',
        'AdminPoliciesRegulations',
        'AdminUtils',
        function(
            $scope,
            AdminPoliciesTerms,
            AdminPoliciesPointsSchemas,
            AdminPoliciesRegulations,
            AdminUtils) {

    $scope.terms = {
        tinymceOptions: AdminUtils.getTinymceOptions(),
        pointsSchemeDisabled: true,
        localeDisabled: true,
        regulationsDisabled: true,
        fields: {
            locale: null,
            terms: null,
            regulations: null,
            privacy: null,
            pointsSchemeId: null,
            regulationChampionshipId: null,
            regulationRoundId: null,
            regulationPlayoffId: null,
            regulationChampionsleagueId: null
        }
    };

    /**
    * Открывает модальное окно с выбраной схемой начисления очков
    */
    $scope.terms.viewSelectedPointsScheme = function()
    {
        AdminPoliciesPointsSchemas.view($scope.terms.fields.pointsSchemeId);
    }
    
    /**
    * Открывает модальное окно с выбраным регламентом турнира
    */
    $scope.terms.viewSelectedRegulation = function(id)
    {
        AdminPoliciesRegulations.view(id);
    }        
}]);