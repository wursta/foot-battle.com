fbAdminApp.controller('AdminPoliciesTermsListCtrl', ['$scope', 'AdminPoliciesTerms', function($scope, AdminPoliciesTerms) {

    $scope.terms = {
        currentPage: 1,
        pageSize: 20,
        total: 0,
        totalPages: 0,
        editions: [],
        filter: {},
        order: {
            id: "ASC"
        }
    };

    /**
    * Загружает страницу с редакциями пользовательских соглашений
    *
    * @param {Integer} page Номер страницы
    */
    $scope.terms.loadPage = function(page) {
        AdminPoliciesTerms.loadPage(page, $scope.terms.filter, $scope.terms.order)
            .then(function(response) {
                $scope.terms.total = response.total;
                $scope.terms.editions = response.items;
            });
    }

    $scope.terms.view = function(editionId) {
        AdminPoliciesTerms.view(editionId);
    }

}]);