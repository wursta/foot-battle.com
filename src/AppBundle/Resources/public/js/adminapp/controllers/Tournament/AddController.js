fbAdminApp.controller('AdminTournamentAddCtrl', [
    '$scope', 
    'AdminPoliciesRegulations',
    function($scope, AdminPoliciesRegulations) {
        $scope.tournament = {
            commissionReadonly: false,
            openType: null,
            fields: {
                format: null,
                commission: null,                
                isPromo: null,
                isOpen: null,
                withConfirmation: null
            }
        };
        
        /**
        * Открывает модальное окно с выбраным регламентом турнира
        */
        $scope.tournament.viewSelectedFormatRegulation = function()
        {            
            AdminPoliciesRegulations.viewByFormat($scope.tournament.fields.format);
        }
        
        $scope.$watch('tournament.openType', function(newVal, oldVal) {            
            
            switch (newVal) {
                case 'OPEN_WITHOUT_CONFIRMATION':
                    $scope.tournament.fields.isOpen = true;
                    $scope.tournament.fields.withConfirmation = false;
                    break;
                
                case 'OPEN_WITH_CONFIRMATION':
                    $scope.tournament.fields.isOpen = true;
                    $scope.tournament.fields.withConfirmation = true;
                    break;
                    
                case 'PRIVATE_WITHOUT_CONFIRMATION':
                    $scope.tournament.fields.isOpen = false;
                    $scope.tournament.fields.withConfirmation = false;
                    break;
            }            
        });
        
        $scope.$watch('tournament.fields.isPromo', function(newVal, oldVal) {            
            if(newVal) {
                $scope.tournament.fields.commission = 0;
                $scope.tournament.commissionReadonly = true;
                $scope.tournament.openType = 'OPEN_WITHOUT_CONFIRMATION';                
            } else {
                $scope.tournament.fields.commission = 10;
                $scope.tournament.commissionReadonly = false;
            }
        });
    }
]);