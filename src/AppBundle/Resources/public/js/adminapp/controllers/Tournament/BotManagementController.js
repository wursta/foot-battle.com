fbAdminApp.controller('AdminTournamentBotManagementCtrl', [
    '$scope',
    'AdminUsers',
    'AdminTournament',
    'AdminUtils',
    '$q',
    function($scope, AdminUsers, AdminTournament, AdminUtils, $q) {
        $scope.tournament = {
            id: null,
            usersCount: 0,
            maxUsersCount: 0
        };
        $scope.roles = {
            users: [AdminUsers.ROLES.ADMIN, AdminUsers.ROLES.USER],
            bots: [AdminUsers.ROLES.BOT]
        },        
        $scope.filter = {
            role: $scope.roles.bots
        };
        $scope.users = [];
        $scope.currentUserSelected = null;
        $scope.selectedRole = 'bots';
        
        $scope.matches = [];
        $scope.userBets = [];        

        /**
        * Добавление бота в турнир.
        */
        $scope.addBot = function() {
            AdminTournament.openBotSelectModal(
                $scope.tournament.id,
                $scope.tournament.usersCount,
                $scope.tournament.maxUsersCount
            ).then(function(selectedUsersCount) {                
                $scope.tournament.usersCount = parseInt($scope.tournament.usersCount) + parseInt(selectedUsersCount);
                console.log($scope.tournament.usersCount);
                if($scope.tournament.usersCount >= $scope.tournament.maxUsersCount) {
                    $scope.tournament.usersCount = $scope.tournament.maxUsersCount;
                }
                $scope.loadUsersList();
            });
        };
        
        /**
        * Загружает список пользователей в турнире.        
        */
        $scope.loadUsersList = function() {                        
            AdminTournament.findUsers($scope.tournament.id, $scope.filter).then(function(result) {
                $scope.users = result.users;
            });
        };
        
        /**
        * Показывает матчи турнира для простановки прогнозов.
        * 
        * @param {Integer} userId Идентификато пользователя.
        */
        $scope.showMatchesBets = function(userId) {
            $scope.currentUserSelected = userId;
                        
            var matchesFilter = {
                isProcessed: false,
                isStarted: false,
                isOver: false,
                isRejected: false
            };
            
            $scope.matches = [];
            $scope.userBets = [];
            
            $q.all([
                AdminTournament.findMatches($scope.tournament.id, matchesFilter),
                AdminTournament.findUserBets($scope.tournament.id, $scope.currentUserSelected),
            ]).then(function(values) {
                var matchesResponse = values[0];
                var betsResponse = values[1];
                
                $scope.matches = matchesResponse.matches;
                $scope.userBets = betsResponse.bets;
            });                                    
        };
        
        $scope.saveBet = function(matchId) {            
            var bet = $scope.userBets[matchId];
            
            if(isNaN(bet.scoreLeft) || isNaN(bet.scoreRight))
              return;
            
            bet.isSaving = true;
            AdminTournament.saveUserBet(
                $scope.tournament.id,
                $scope.currentUserSelected,
                matchId,
                bet
            ).then(function(result) {
                bet.isSaving = false;
            });
        };
    }
]);