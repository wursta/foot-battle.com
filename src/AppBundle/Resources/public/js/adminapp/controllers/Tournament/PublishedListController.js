fbAdminApp.controller('AdminTournamentPublishedListCtrl', [
    '$scope',
    'AdminTournament',
    function($scope, AdminTournament) {
        $scope.tournaments = {
            items: [],
            pageSize: 25,
            currentPage: 1,
            totalItems: 0,
            filter: {
                isReady: true,
                isOver: false,
                isReject: false
            },
            order: {}
        };

        /**
        * Загружает страницу со списком турниров
        *
        * @param {Integer} page Номер страницы
        */
        $scope.tournaments.loadPage = function(page) {
            AdminTournament.load(page, $scope.tournaments.filter, $scope.tournaments.order).then(function(response){
                $scope.tournaments.totalItems = response.total;
                $scope.tournaments.items = response.items;
            });
        }

        /**
        * Проверяет необходимо ли показать кнопку "Приглашения пользователя в турнир"
        *
        * @param {Object} tournament Объект с данными турнира.
        *
        * @returns {Boolean}
        */
        $scope.tournaments.checkUserInviteBtnVisibility = function(tournament) {
            if(!tournament.isOpen && tournament.isAccessible) {
                return true;
            }

            return false;
        }

        /**
        * Проверяет необходимо ли показать кнопку "Заявки пользователей в турнир"
        *
        * @param {Object} tournament Объект с данными турнира.
        *
        * @returns {Boolean}
        */
        $scope.tournaments.checkUserRequestsBtnVisibility = function(tournament) {
            if(tournament.userRequestsCount > 0) {
                return true;
            }

            return false;
        }
    }
]);