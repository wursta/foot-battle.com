fbAdminApp.controller('AdminUsersCtrl', ['$scope', 'AdminUsers', function($scope, AdminUsers) {

    $scope.usersList = {
        currentPage: 1,
        pageSize: 20,
        totalItems: 0,
        totalPages: 0,
        items: [],
        filter: {
            query: null,
            referral_user: null,
            isActive: null,
            registrationDate: {
                startDate: null,
                endDate: null
            }
        },
        order: {
            id: "ASC"
        }
    };

    /**
    * Применяет фильтр
    *
    */
    $scope.usersList.applyFilter = function() {
        $scope.usersList.currentPage = 1;
        $scope.usersList.loadPage($scope.usersList.currentPage);
    },

    /**
    * Сбрасывает фильтр
    *
    */
    $scope.usersList.cancelFilter = function() {
        $scope.usersList.currentPage = 1;
        $scope.usersList.filter = {
            query: null,
            referral_user: null,
            isActive: null,
            registrationDate: {
                startDate: null,
                endDate: null
            }
        };

        $scope.usersList.loadPage($scope.usersList.currentPage);
    },

    /**
    * Загружает страницу со списком пользователей
    *
    * @param {Integer} page Номер страницы
    */
    $scope.usersList.loadPage = function(page) {
        AdminUsers.load(page, $scope.usersList.filter, $scope.usersList.order).then(function(response){
            $scope.usersList.totalItems = response.total;
            $scope.usersList.items = response.items;
        });
    }

}]);