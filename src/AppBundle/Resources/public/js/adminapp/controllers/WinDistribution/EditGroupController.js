fbAdminApp.controller('AdminWinDistributionEditGroupCtrl', ['$scope', function($scope) {

    $scope.group = {
        rules: []
    };

    $scope.group.addRule = function(data)
    {
        if(data == undefined) {
            data = {
                place: null,
                percent: null
            };
        }

        $scope.group.rules.push(data);
    }

    $scope.group.removeRule = function(index)
    {
        $scope.group.rules.splice(index, 1);
    }

}]);