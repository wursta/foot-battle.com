angular.module('AdminBooleanSelectDirective', [])
  .directive('booleanselect', function() {
  return {
    restrict: 'E',
    scope: {
      model: '='
    },
    template: '<select class="form-control" ng-model="model"><option value=""></option><option value="0">Нет</option><option value="1">Да</option></select>'
  };
});