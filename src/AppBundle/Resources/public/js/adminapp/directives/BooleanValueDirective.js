angular.module('AdminBooleanValueDirective', [])
  .directive('booleanval', function() {
  return {
    restrict: 'E',
    scope: {
      value: '=',
      text: '@',
      cls: '@',
    },
    link: function($scope, element, attrs) {
        if ($scope.value) {
            $scope.text = 'Да';
            $scope.cls = 'glyphicon-ok text-success';
        } else {
            $scope.text = 'Нет';
            $scope.cls = 'glyphicon-remove text-danger';
        }
    },
    template: '<i class="glyphicon [[ cls ]]" title="[[ text ]]"></i>'
  };
});