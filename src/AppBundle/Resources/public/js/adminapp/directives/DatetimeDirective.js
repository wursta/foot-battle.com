angular.module('AdminDatetimeDirective', [])
  .directive('datetime', function() {
  return {
    restrict: 'E',
    scope: {
      timestamp: '=',
    },
    template: '[[ timestamp | date: "dd.MM.yy HH:mm" ]]'
  };
});