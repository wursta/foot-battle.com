angular.module('AdminFBDateRangePickerDirective', [])
  .directive('fbdaterangepicker', function() {
  return {
    restrict: 'E',
    scope: {
      model: '='
    },
    template: function() { return '<input '+
        'date-range-picker class="form-control date-picker" type="text" '+
        'ng-model="model" '+
        'options="{'+
            'locale: {'+
                'firstDay: 1,'+
                'format: \'DD.MM.YYYY\','+
                'applyLabel: \'Применить\','+
                'cancelLabel: \'Отмена\','+
                'fromLabel: \'C\','+
                'toLabel: \'По\','+
                'customRangeLabel: \'Выбрать\','+
                'weekLabel: \'Неделя\','+
                'daysOfWeek: [\'Вс\', \'Пн\', \'Вт\', \'Ср\', \'Чт\', \'Пт\', \'Сб\'],'+
                'monthNames: [\'Январь\', \'Февраль\', \'Март\', \'Апрель\', \'Май\', \'Июнь\', \'Июль\', \'Август\', \'Сентябрь\', \'Октябрь\', \'Ноябрь\'],'+
            '}, '+
        '}"'+
        '/>'
    }
  };
});