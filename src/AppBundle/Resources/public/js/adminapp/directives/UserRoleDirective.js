angular.module('AdminUserRoleDirective', [])
  .directive('userrole', function() {
  return {
    restrict: 'E',
    scope: {
      role: '='
    },
    link: function($scope, element, attrs) {

        switch($scope.role) {
            case 'ROLE_ADMIN':
                $scope.role = 'Администратор';
                break;
            case 'ROLE_USER':
                $scope.role = 'Пользователь';
                break;
            case 'ROLE_BOT':
                $scope.role = 'Бот';
                break;
        }

    },
    template: '[[ role ]]'
  };
});