angular.module('ngReplaceName', [])
.directive('ngReplaceName', function() {
    return {
        restrict: 'A',
        scope: {
          replace: '@'
        },
        require: ['?ngModel', '^?form'],
        link: function postLink(scope, elem, attrs, ctrls) {
            var newName = attrs.name.replace('__name__', scope.replace);
            attrs.$set('name', newName);

            var modelCtrl = ctrls[0];
            var formCtrl  = ctrls[1];

            if (modelCtrl && formCtrl) {
                modelCtrl.$name = attrs.name;
                formCtrl.$addControl(modelCtrl);
                scope.$on('$destroy', function () {
                    formCtrl.$removeControl(modelCtrl);
                });
            }
        }
    };
});