angular.module('AdminBillingService', [])
.factory('AdminBilling', ['$http', '$q', function($http, $q) {
    return {

        /**
        * Загружает постранично операции по фильтру с сортировкой
        *
        * @param page
        * @param filter
        * @param order
        *
        * @return {Promise}
        */
        loadOperationsLog: function(page, filter, order) {
            var deferred = $q.defer();

            var data = {
              filter: filter,
              order: order
            }

            $http({
                method: "POST",
                url: '/admin/billing/' + page,
                data: data
            } )
            .success(function(data, status, headers, config){
                deferred.resolve(data);
            })
            .error(function(response, status, headers, config){
                deferred.reject(status);
            });

            return deferred.promise;
        }

    }
}]);