angular.module('AdminPoliciesPointsSchemasService', [])
.factory('AdminPoliciesPointsSchemas', ['$http', '$q', '$uibModal', function($http, $q, $uibModal) {
    return {

        /**
        * Загружает постранично схемы по фильтру с сортировкой
        *
        * @param page
        * @param filter
        * @param order
        *
        * @return {Promise}
        */
        loadPage: function(page, filter, order) {
            var deferred = $q.defer();

            var data = {
              filter: filter,
              order: order
            }

            $http({
                method: "POST",
                url: '/admin/policies/pointsSchemas/search.json/' + page,
                data: data
            })
            .success(function(data, status, headers, config){
                deferred.resolve(data);
            })
            .error(function(response, status, headers, config){
                deferred.reject(status);
            });

            return deferred.promise;
        },

        /**
        * Получает данные о схеме по ID
        *
        * @param {Integer} pointsSchemeId ID Схемы.
        *
        * @return {Promise}
        */
        getById: function(pointsSchemeId)
        {
            var deferred = $q.defer();

            $http({
                method: "POST",
                url: '/admin/policies/pointsSchemas/view.json/' + pointsSchemeId
            })
            .success(function(data, status, headers, config){
                deferred.resolve(data);
            })
            .error(function(response, status, headers, config){
                deferred.reject(status);
            });

            return deferred.promise;
        },

        /**
        * Открывает модальное окно со схемой начисления очков
        *
        * @param int pointsSchemeId
        */
        view: function (pointsSchemeId)
        {
            var self = this;

            self.getById(pointsSchemeId).then(function(data){

                var modalInstance = $uibModal.open({
                    templateUrl: 'pointsSchemeModalTmpl.html',
                    controller: 'PointsSchemeModalCtrl',
                    resolve: {
                        pointsScheme: function() {
                            return data;
                        }
                    }
                });
            });
        }

    }
}]);

fbAdminApp.controller('PointsSchemeModalCtrl', ['$scope', '$uibModalInstance', 'pointsScheme', function($scope, $uibModalInstance, pointsScheme) {
    $scope.pointsScheme = pointsScheme;
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);