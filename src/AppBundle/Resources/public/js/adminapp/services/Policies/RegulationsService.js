angular.module('AdminPoliciesRegulationsService', [])
.factory('AdminPoliciesRegulations', ['$http', '$q', '$uibModal', function($http, $q, $uibModal) {
    return {

        /**
        * Загружает постранично редакции регламентов турниров
        *
        * @param page
        * @param filter
        * @param order
        *
        * @return {Promise}
        */
        loadPage: function(page, filter, order) {
            var deferred = $q.defer();

            var data = {
              filter: filter,
              order: order
            }

            $http({
                method: "POST",
                url: '/admin/policies/regulations/search.json/' + page,
                data: data
            })
            .success(function(data, status, headers, config){
                deferred.resolve(data);
            })
            .error(function(response, status, headers, config){
                deferred.reject(status);
            });

            return deferred.promise;
        },

        /**
        * Получает данные о регламенте по-умолчанию
        *        
        * @param {Integer} tournamentFormatID ID формата турнира
        *
        * @return {Promise}
        */
        getDefault: function(tournamentFormatID)
        {
            var deferred = $q.defer();

            $http({
                method: "POST",
                url: '/admin/policies/regulations/get/default.json/' + tournamentFormatID
            })
            .success(function(data, status, headers, config){
                deferred.resolve(data);
            })
            .error(function(response, status, headers, config){
                deferred.reject(status);
            });

            return deferred.promise;
        },

        /**
        * Получает редакцию регламента турнира по ID
        *
        * @param {Integer} regulationId Идентификатор редакции.
        *
        * @return {Promise}
        */
        getById: function(regulationId)
        {
            var deferred = $q.defer();

            $http({
                method: "POST",
                url: '/admin/policies/regulations/view.json/' + regulationId
            })
            .success(function(data, status, headers, config){
                deferred.resolve(data);
            })
            .error(function(response, status, headers, config){
                deferred.reject(status);
            });

            return deferred.promise;
        },

        /**
        * Открывает модальное окно с редакцией регламента
        *
        * @param int regulationId Идентификатор регламента
        */
        view: function (regulationId)
        {
            var self = this;

            self.getById(regulationId).then(function(data){

                var modalInstance = $uibModal.open({
                    templateUrl: 'regulationModalTmpl.html',
                    controller: 'RegulationModalCtrl',
                    size: 'lg',
                    resolve: {
                        regulation: function() {
                            return data;
                        }
                    }
                });
            });
        }        
    }
}]);

fbAdminApp.controller('RegulationModalCtrl', ['$scope', '$sce', '$uibModalInstance', 'regulation', function($scope, $sce, $uibModalInstance, regulation) {
    $scope.regulation = {
        title: regulation.title,
        tournamentFormatTitle: regulation.tournamentFormatTitle,
        text: $sce.trustAsHtml(regulation.text),        
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);