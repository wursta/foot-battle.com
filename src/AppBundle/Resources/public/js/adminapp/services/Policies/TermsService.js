angular.module('AdminPoliciesTermsService', [])
.factory('AdminPoliciesTerms', ['$http', '$q', '$uibModal', function($http, $q, $uibModal) {
    return {

        /**
        * Загружает постранично редакции пользовательских соглашений
        *
        * @param page
        * @param filter
        * @param order
        *
        * @return {Promise}
        */
        loadPage: function(page, filter, order) {
            var deferred = $q.defer();

            var data = {
              filter: filter,
              order: order
            }

            $http({
                method: "POST",
                url: '/admin/policies/terms/search.json/' + page,
                data: data
            })
            .success(function(data, status, headers, config){
                deferred.resolve(data);
            })
            .error(function(response, status, headers, config){
                deferred.reject(status);
            });

            return deferred.promise;
        },

        /**
        * Получает данные о дефолтной схеме
        *        
        * @return {Promise}
        */
        getDefault: function()
        {
            var deferred = $q.defer();

            $http({
                method: "POST",
                url: '/admin/policies/terms/get/default.json'
            })
            .success(function(data, status, headers, config){
                deferred.resolve(data);
            })
            .error(function(response, status, headers, config){
                deferred.reject(status);
            });

            return deferred.promise;
        },

        /**
        * Получает редакцию пользовательского соглашения по ID
        *
        * @param {Integer} editionId Идентификатор редакции.
        *
        * @return {Promise}
        */
        getById: function(editionId)
        {
            var deferred = $q.defer();

            $http({
                method: "POST",
                url: '/admin/policies/terms/view.json/' + editionId
            })
            .success(function(data, status, headers, config){
                deferred.resolve(data);
            })
            .error(function(response, status, headers, config){
                deferred.reject(status);
            });

            return deferred.promise;
        },

        /**
        * Открывает модальное окно с редакцией пользовательского соглашения
        *
        * @param int editionId Идентификатор редакции
        */
        view: function (editionId)
        {
            var self = this;

            self.getById(editionId).then(function(data){

                var modalInstance = $uibModal.open({
                    templateUrl: 'termsModalTmpl.html',
                    controller: 'TermsModalCtrl',
                    size: 'lg',
                    resolve: {
                        terms: function() {
                            return data;
                        }
                    }
                });
            });
        }
    }
}]);

fbAdminApp.controller('TermsModalCtrl', ['$scope', '$sce', '$uibModalInstance', 'terms', function($scope, $sce, $uibModalInstance, terms) {
    $scope.terms = {
        number: terms.number,
        editionDate: terms.editionDate,
        terms: $sce.trustAsHtml(terms.terms),
        regulations: $sce.trustAsHtml(terms.regulations),
        privacy: $sce.trustAsHtml(terms.privacy)
    }
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);