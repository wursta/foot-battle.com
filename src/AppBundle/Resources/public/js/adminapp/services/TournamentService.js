angular.module('AdminTournamentService', [])
.factory('AdminTournament', [
    '$http',
    '$q',
    '$uibModal',
    'AdminUsers',
    'AdminUtils',
    function($http, $q, $uibModal, AdminUsers, AdminUtils) {
        return {
            /**
            * Загружает постранично список турниров по фильтру с сортировкой
            *
            * @param {Integer} page   Номер страницы.
            * @param {Object}  filter Объект с фильтром.
            * @param {Object}  order  Объект с сортировкой.
            *
            * @returns {Promise}
            */
            load: function(page, filter, order) {
                var deferred = $q.defer();

                var data = {
                    filter: filter,
                    order: order
                }

                $http({
                    method: "POST",
                    url: '/admin/tournaments/search.json/page:' + page,
                    data: data
                } )
                .success(function(data, status, headers, config){
                    deferred.resolve(data);
                })
                .error(function(response, status, headers, config){
                    deferred.reject(status);
                });

                return deferred.promise;
            },
            
            /**
            * Загружает пользователей в турнире.
            * 
            * @param {Object} filter Параметры фильтрации.
            * @param {Object} order Параметры сортировки.
            */
            findUsers: function(tournamentId, filter, order) {
                var params = {
                    filter: filter,
                    order: order
                };
                
                return AdminUtils.performPostCall('/admin/tournament/' + tournamentId + '/users/list.json', params);
            },

            /**
            * Открывает модальное окно для выбора ботов.
            *
            * @param {Integer} tournamentId Идентификатор турнира.
            */
            openBotSelectModal: function(tournamentId, currentUsersCount, maxUsersCount) {
                var self = this;
                
                var deferred = $q.defer();
                
                AdminUsers.findBotsNotInTournament(tournamentId).then(function(result) {
                    
                    var bots = result.bots
                    
                    var modalInstance = $uibModal.open({
                        size: 'lg',
                        templateUrl: 'botSelectModalTmpl.html',
                        controller: 'BotSelectModalCtrl',
                        resolve: {
                            tournamentId: function() {
                                return tournamentId;
                            },
                            availableUsersCount: function() {
                                return maxUsersCount - currentUsersCount;
                            },
                            users: function() {
                                return bots;
                            }
                        }
                    });

                    modalInstance.result.then(function (selectedUsersIds) {
                        self.addUsersToTournament(tournamentId, selectedUsersIds).then(function(response) {
                            deferred.resolve(selectedUsersIds.length);
                        });
                    });
                });
                
                return deferred.promise;
            },

            /**
            * Добавляет пользователей в турнир.
            *
            * @param {Array} usersIds Массив с идентификаторами пользователей.
            */
            addUsersToTournament: function(tournamentId, usersIds) {                
                var params = {
                    tournamentId: tournamentId,
                    users: usersIds
                };
                
                return AdminUtils.performPostCall('/admin/tournament/users/addByIds.json', params);                               
            },
            
            /**
            * Загружает матчи турнира.
            * 
            * @param {Integer} tournamentId Идентификатор турнира.            
            * @param {Object}  filter       Параметры фильтрации.
            * @param {Object}  order        Параметры сортировки.
            *
            * @return {Promise}
            */
            findMatches: function(tournamentId, filter, order) {
                var params = {
                    filter: filter,
                    order: order
                };
                
                return AdminUtils.performPostCall('/admin/tournament/' + tournamentId + '/matches/list.json', params);
            },
            
            /**
            * Загружает прогнозы пользователя.
            * 
            * @param {Integer} tournamentId Идентификатор турнира.
            * @param {Integer} userId       Идентификатор пользователя.
            * @param {Object}  filter       Параметры фильтрации.
            * @param {Object}  order        Параметры сортировки.
            *
            * @return {Promise}
            */
            findUserBets: function(tournamentId, userId, filter, order) {
                var params = {
                    filter: filter,
                    order: order
                };
                
                return AdminUtils.performPostCall(
                    '/admin/tournament/' + tournamentId + '/userbets/' + userId + '/list.json',
                    params
                );
            },
            
            /**
            * Сохранение прогноза за пользователя.
            * 
            * @param {Integer} tournamentId Идентификатор турнира.
            * @param {Integer} userId       Идентификатор пользователя.
            * @param {Integer} matchId      Идентификатор матча.
            * @param {Object}  betObject    Объект прогноза.
            */
            saveUserBet: function(tournamentId, userId, matchId, betObject) {
                return AdminUtils.performPostCall(
                    '/admin/tournament/' + tournamentId + '/userbets/' + userId + '/match/' + matchId + '/save.json',
                    betObject
                );
            }
        }
}]);

fbAdminApp.controller('BotSelectModalCtrl', [
    '$scope',
    '$uibModalInstance',
    'Utils',
    'tournamentId',
    'availableUsersCount',
    'users',
    function(
        $scope,
        $uibModalInstance,
        Utils,
        tournamentId,
        availableUsersCount,
        users
    ) {
        $scope.users = users;
        $scope.tournamentId = tournamentId;
        $scope.availableUsersCount = availableUsersCount;
        $scope.selectedUsersIds = [];

        /**
        * Проверяет, выбран ли пользователь.
        *
        * @param {Integer} userId Идентификатор пользователя.
        */
        $scope.userIsSelected = function(userId) {
            return Utils.inArray(userId, $scope.selectedUsersIds);
        };

        /**
        * Отмечает пользователя как выбранного.
        *
        * @param {Integer} userId Идентификатор пользователя.
        */
        $scope.selectUser = function(userId) {            
            $scope.selectedUsersIds.push(userId);
            $scope.availableUsersCount--;
        };

        /**
        * Удаляет отметку о выборе пользователя.
        *
        * @param {Integer} userId Идентификатор пользователя.
        */
        $scope.deselectUser = function(userId) {
            var index = Utils.arraySearch(userId, $scope.selectedUsersIds);
            $scope.selectedUsersIds.splice(index, 1);
            $scope.availableUsersCount++;
        }

        /**
        * Закрывает модальное окно и передаёт идентификаторы выбранных ботов.
        */
        $scope.apply = function() {
            $uibModalInstance.close($scope.selectedUsersIds);
        };

        /**
        * Закрывает модальное окно.
        *
        */
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
}]);