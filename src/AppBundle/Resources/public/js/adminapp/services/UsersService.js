angular.module('AdminUsersService', [])
.factory('AdminUsers', ['$http', '$q', 'AdminUtils', function($http, $q, AdminUtils) {
    return {
        ROLES: {
            ADMIN: 'ROLE_ADMIN',
            USER: 'ROLE_USER',
            BOT: 'ROLE_BOT'
        },        
        /**
        * Загружает постранично список пользователей по фильтру с сортировкой
        *
        * @param page
        * @param filter
        * @param order
        *
        * @return {Promise}
        */
        load: function(page, filter, order) {
            var deferred = $q.defer();

            var data = {
              filter: filter,
              order: order
            }

            $http({
                method: "POST",
                url: '/admin/users/list.json/page:' + page,
                data: data
            } )
            .success(function(data, status, headers, config){
                deferred.resolve(data);
            })
            .error(function(response, status, headers, config){
                deferred.reject(status);
            });

            return deferred.promise;
        },

        /**
        * Поиск ботов которых нет в турнире.
        *
        * @param {Integer} tournamentId Идентификатор турнира.        
        *
        * @return {Promise}
        */
        findBotsNotInTournament: function(tournamentId) {            
            return AdminUtils.performPostCall('/admin/users/bots/not_in_tournament.json/' + tournamentId);
        },
    }
}]);