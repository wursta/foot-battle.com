angular.module('AdminUtilsService', [])
.factory('AdminUtils', [
    '$http',
    '$q',
    '$sce',
    '$uibModal',
    function(
        $http,
        $q,
        $sce,
        $uibModal
    ) {
    return {

        /**
        * Возвращает настройки tinyMce
        *
        * @return {Object}
        */
        getTinymceOptions: function() {
            return {
                baseURL: '/admin/tinymce',
                language: 'ru',
                min_height: 300,
                plugins: "paste code charmap fullscreen fbplaceholders",
                toolbar: 'select_fbplaceholder, undo redo | styleselect | bold italic | alignleft aligncenter alignright | numlist bullist',
            }
        },
        
        /**
        * Выводит сообщение об ошибке.
        * 
        * @param {Object} response Объект ответа.        
        */
        handleResponseError: function(response) {
            if(!response.success) {
                if(response.error != undefined && response.error != '') {
                    var errorText = response.error;                    
                } else {
                    var errorText = "Произошла неизвестная ошибка";
                }
                
                var modalInstance = $uibModal.open({
                    size: 'lg',
                    templateUrl: 'applicationErrorModalTmpl.html',
                    controller: 'ApplicationErrorModalCtrl',
                    resolve: {
                        errorText: function() {
                            return $sce.trustAsHtml(errorText);
                        }
                    }
                });                
            }
        },
        
        /**
        * Ассинхронно отправляет POST запрос.
        * 
        * @param {String} uri    Адрес.
        * @param {Object} params Параметры запроса.
        * 
        * @return {Promise}
        */
        performPostCall: function(uri, params) {
            var self = this;
            
            var deferred = $q.defer();
            
            $http({
                method: "POST",
                url: uri,
                data: params
            } )
            .success(function(data, status, headers, config){
                if (!data.success) {
                    self.handleResponseError({ success: false, error: data});
                }
                
                deferred.resolve(data);
            })
            .error(function(response, status, headers, config){
                self.handleResponseError({ success: false, error: response});
                deferred.reject(status);
            });

            return deferred.promise;
        }
    }
}]);

fbAdminApp.controller('ApplicationErrorModalCtrl', ['$scope', '$uibModalInstance', 'errorText', function(
        $scope,
        $uibModalInstance,        
        errorText
    ) {
        $scope.errorText = errorText;
        
        /**
        * Закрывает модальное окно.        
        */
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
}]);