fbApp.controller('BillingLogCtrl', ['$scope', '$http', function($scope, $http) {

    $scope.billing = {
        log: {
            currentPage: 1,
            pageSize: 5,
            totalItems: 0,
            totalPages: 0,
            logItems: []
        }
    };

    $scope.billing.log.loadPage = function(page) {

        $http.post( Routing.generate('personal/balance/billingLog.json', { page: page }) ).then(function(response){
            $scope.billing.log.totalItems = response.data.total;
            $scope.billing.log.logItems = response.data.items;
        });
    }

    $scope.billing.log.loadPage($scope.billing.log.currentPage);
}]);