fbApp.controller('BillingRefillQiwiCtrl', ['$scope', '$sce', 'Billing', function($scope, $sce, Billing) {

    $scope.billing = {
        refill : {
            qiwi: {
                fields: {
                    qiwiPhone: '',
                    amount: ''
                },
                steps: {
                    step_1: {
                        modalTitle: '',
                        isLoading: false
                    },
                    step_2: {
                        modalTitle: '',
                        isLoading: false,
                        refillInfo: {
                            amount: 0,
                            amountWithCommission: 0,
                            commissionPercent: 0,
                            commissionReal: 0,
                            moneyAmount: 0,
                            qiwiPhone: '',
                            currencySign: ''
                        }
                    },
                    step_3: {
                        iframeSrc: ''
                    }
                },
                modalTitle: '',
                currentStep: 'step_1',
                amountIsOk: false,
                amountError: '',
                confirmationDisabled: true,
                submitDisabled: false
            }
        }
    };

    /**
    * Проверка поля "Сумма".
    * В случае ошибки устанавливает параметр amountIsOk в false.
    */
    $scope.billing.refill.qiwi.checkAmount = function()
    {
        if(
            $scope.billing.refill.qiwi.fields.amount != '' &&
            $scope.billing.refill.qiwi.fields.amount != undefined &&
            !isNaN(parseFloat($scope.billing.refill.qiwi.fields.amount)) &&
            parseFloat($scope.billing.refill.qiwi.fields.amount) > 0
        ) {
            $scope.billing.refill.qiwi.amountIsOk = true;
        } else {
            $scope.billing.refill.qiwi.amountIsOk = false;
        }

        $scope.billing.refill.qiwi.checkConfirmDisabled();
    };

    /**
    * Проверка возможности подтверждения формы и перехода к шагу 2
    */
    $scope.billing.refill.qiwi.checkConfirmDisabled = function()
    {
        if(
            $scope.billing.refill.qiwi.fields.qiwiPhone != '' &&
            $scope.billing.refill.qiwi.fields.qiwiPhone != undefined &&
            $scope.billing.refill.qiwi.amountIsOk == true
        ) {
            $scope.billing.refill.qiwi.confirmationDisabled = false;
        } else {
            $scope.billing.refill.qiwi.confirmationDisabled = true;
        }
    };

    /**
    * Подтверждение формы покупки.
    * Отправка запроса на валидацию данных
    *
    * @return {void}
    */
    $scope.billing.refill.qiwi.confirm = function()
    {
        $scope.billing.refill.qiwi.steps.step_1.isLoading = true;
        $scope.billing.refill.qiwi.confirmationDisabled = true;

        Billing.confirmQiwiRefill(
                $scope.billing.refill.qiwi.fields.qiwiPhone,
                $scope.billing.refill.qiwi.fields.amount
        ).then(function(response) {
            if (response.success) {
                $scope.billing.refill.qiwi.steps.step_2.refillInfo = response.data;
                $scope.billing.refill.qiwi.modalTitle = $scope.billing.refill.qiwi.steps.step_2.modalTitle;
                $scope.billing.refill.qiwi.amountError = '';

                $scope.billing.refill.qiwi.fields.qiwiPhone = response.data.qiwiPhone;
                $scope.billing.refill.qiwi.fields.amount = response.data.amount;

                $scope.billing.refill.qiwi.currentStep = 'step_2';
            } else {
                $scope.billing.refill.qiwi.amountError = $sce.trustAsHtml(response.error);
            }

            $scope.billing.refill.qiwi.steps.step_1.isLoading = false;
            $scope.billing.refill.qiwi.confirmationDisabled = false;
        });
    };

    $scope.billing.refill.qiwi.submit = function(e)
    {
        e.preventDefault();

        $scope.billing.refill.qiwi.steps.step_2.isLoading = true;
        $scope.billing.refill.qiwi.submitDisabled = true;

        Billing.refillByQiwi(
            $scope.billing.refill.qiwi.fields.qiwiPhone,
            $scope.billing.refill.qiwi.fields.amount
        ).then(function(response) {
            if(response.success) {
                $scope.billing.refill.qiwi.modalTitle = $scope.billing.refill.qiwi.steps.step_1.modalTitle;
                $scope.billing.refill.qiwi.steps.step_3.iframeSrc = $sce.trustAsResourceUrl(response.data.iframeUrl);
                $scope.billing.refill.qiwi.currentStep = 'step_3';
            } else {
                $scope.billing.refill.qiwi.currentStep = 'step_1';
                $scope.billing.refill.qiwi.amountError = $sce.trustAsHtml(response.error);
            }

            $scope.billing.refill.qiwi.steps.step_2.isLoading = false;
            $scope.billing.refill.qiwi.submitDisabled = false;
        });
    }

}]);