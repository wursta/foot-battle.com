fbApp.controller('LoginFormCtrl', ['$scope', 'Security', function($scope, Security) {

    $scope.loginModal = {
        title: ''
    };

    $scope.loginForm = {
        username: '',
        password: '',
        rememberMe: true,
        errorMsg: '',
        isLoading: false,
        isHidden: false,
        modalTitle: ''
    };

    $scope.passwordRecoveryForm = {
        fields: {
            email: ''
        },
        isHidden: true,
        modalTitle: '',
        isLoading: false,
        errorMsg: '',
        successMsg: ''
    };

    /**
    * Отправить запрос на авторизацию
    *
    * @return {void}
    */
    $scope.loginForm.submit = function()
    {
        $scope.loginForm.isLoading = true;

        Security.authorize(
            $scope.loginForm.username,
            $scope.loginForm.password,
            $scope.loginForm.rememberMe
        ).then(function(response){
            if (response.success) {
                $scope.loginForm.errorMsg = '';
                window.location.reload();
            } else {
                $scope.loginForm.errorMsg = response.error;
                $scope.passwordRecoveryForm.successMsg = '';
            }
            $scope.loginForm.isLoading = false;
        });
    };

    /**
    * Скрывает форму восстановления пароля и показыват форму авторизации
    *
    * @return {void}
    */
    $scope.loginForm.show = function()
    {
        $scope.loginForm.isHidden = false;
        $scope.passwordRecoveryForm.isHidden = true;
        $scope.loginModal.title = $scope.loginForm.modalTitle;
    };

    /**
    * Скрывает форму авторизации и показыват форму восстановления пароля
    *
    * @return {void}
    */
    $scope.passwordRecoveryForm.show = function()
    {
        $scope.loginForm.isHidden = true;
        $scope.passwordRecoveryForm.isHidden = false;
        $scope.loginModal.title = $scope.passwordRecoveryForm.modalTitle;
    };

    /**
    * Отправить запрос на восстановление пароля
    *
    * @return {void}
    */
    $scope.passwordRecoveryForm.submit = function()
    {
        $scope.passwordRecoveryForm.isLoading = true;

        Security.passwordRecovery(
            $scope.passwordRecoveryForm.fields.email
        ).then(function(response){
            if (response.success) {
                $scope.passwordRecoveryForm.errorMsg = '';
                $scope.passwordRecoveryForm.successMsg = response.successMsg;
                $scope.loginForm.show();

            } else {
                $scope.passwordRecoveryForm.errorMsg = response.error;
                $scope.passwordRecoveryForm.successMsg = '';
            }
            $scope.passwordRecoveryForm.isLoading = false;
        });
    };

}]);
