fbApp.controller('NavigationCtrl', ['$scope', '$uibModal', '$http', 'Utils', 'Security', function($scope, $uibModal, $http, Utils, Security) {

    $scope.mainNav = {
        isOpen: false
    };

    /**
    * Сохраняет куку, если есть реферальный код.
    *
    * @return {void}
    */
    $scope.saveReferralCodeCookie = function() {
        var code = Security.parseReferralCodeFromLocationHash();
        if (code) {
            Security.saveReferralCodeCookie(code);
        }
    }

    /**
    * Показывает модальное окно формы авторизации
    *
    * @return {void}
    */
    $scope.showLoginModal = function(e)
    {
        e.preventDefault();
        Security.openLoginModal();
    };

    /**
    * Показывает модальное окно формы регистрации
    *
    * @return {void}
    */
    $scope.showRegistrationModal = function(e)
    {
        e.preventDefault();
        Security.openRegistrationModal();
    };
}]);

fbApp.controller('ModalCtrl', ['$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}]);