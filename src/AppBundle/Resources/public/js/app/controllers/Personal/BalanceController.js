fbApp.controller('PersonalBalanceCtrl', ['$scope', 'Billing', function($scope, Billing) {

    $scope.balance = {};

    /**
    * Открытие окна для выбора возможностей пополнения
    *
    * @return {void}
    */
    $scope.balance.openRefillModal = function()
    {
        Billing.openRefillChoicesModal();
    }

}]);