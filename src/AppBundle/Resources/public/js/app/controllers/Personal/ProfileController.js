fbApp.controller('PersonalProfileCtrl', ['$scope', 'Security', function($scope, Security) {

    $scope.profileForm = {
        fields: {
            username: ''
        },

        usernameError: null,
        usernameSuccess: false,
    };

    /**
    * Проверка Никнейма
    *
    * return {void}
    */
    $scope.profileForm.checkUsername = function(currentUserId)
    {
        if($scope.profileForm.fields.username == '')
            return false;

        Security.checkUsername($scope.profileForm.fields.username, { excludeUserId: currentUserId }).then(function(response){
            if (response.success) {
                $scope.profileForm.usernameError = null;
                $scope.profileForm.usernameSuccess = true;
            } else {
                $scope.profileForm.usernameError = response.errorMsg;
                $scope.profileForm.usernameSuccess = false;

            }
        });
    };

}]);