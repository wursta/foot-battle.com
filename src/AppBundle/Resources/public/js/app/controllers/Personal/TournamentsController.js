fbApp.controller('PersonalTournamentsCtrl', ['$scope', 'UserTournaments', function($scope, UserTournaments) {

    $scope.curTournaments = {
        currentPage: 1,
        pageSize: 10,
        totalItems: 0,
        totalPages: 0,
        filter: {},
        tournaments: []
    };

    /**
    * Загружает турниры по фильтру с паджинацией
    *
    * @param {Integer} page Текущая страница.
    *
    * @return {void}
    */
    $scope.curTournaments.loadPage = function(page) {
        UserTournaments.load(
            page,
            $scope.curTournaments.filter,
            { pageSize: $scope.curTournaments.pageSize }
        ).then(function(data, responseHeaders){
            $scope.curTournaments.totalItems = data.total;
            $scope.curTournaments.tournaments = data.items;
        });
    };

}]);