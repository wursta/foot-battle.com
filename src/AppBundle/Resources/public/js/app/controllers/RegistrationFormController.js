fbApp.controller('RegistrationFormCtrl', ['$scope', 'Utils', 'Security', function($scope, Utils, Security) {

    $scope.registrationForm = {
        fields: {
            email: '',
            username: '',
            password: '',
            passwordConfirm: '',
            termsAccepted: false,
            referralCode: ''
        },
        emailError: null,
        emailSuccess: false,

        usernameError: null,
        usernameSuccess: false,

        passwordError: null,
        passwordSuccess: false,

        submitDisabled: true,
        isLoading: false,

        submitErrors: [],
        confirmEmailSent: false,

        referralUser: false
    };

    /**
    * Проверка заполненности всех полей и выставление флага submitDisabled
    * если не все поля заполнены.
    *
    * return {void}
    */
    $scope.registrationForm.checkSubmitDisabled = function()
    {
        if(
            $scope.registrationForm.emailSuccess &&
            $scope.registrationForm.usernameSuccess &&
            $scope.registrationForm.passwordSuccess &&
            $scope.registrationForm.fields.termsAccepted
        ) {
            $scope.registrationForm.submitDisabled = false;
        } else {
            $scope.registrationForm.submitDisabled = true
        }
    };

    /**
    * Проверка E-mail
    *
    * return {void}
    */
    $scope.registrationForm.checkEmail = function()
    {
        if($scope.registrationForm.fields.email == '')
            return false;

        Security.checkEmail($scope.registrationForm.fields.email).then(function(response){
            if (response.success) {
                $scope.registrationForm.emailError = null;
                $scope.registrationForm.emailSuccess = true;
            } else {
                $scope.registrationForm.emailError = response.errorMsg;
                $scope.registrationForm.emailSuccess = false;

            }

            $scope.registrationForm.checkSubmitDisabled();
        });
    };

    /**
    * Проверка совпадения паролей
    *
    * return {void}
    */
    $scope.registrationForm.checkPasswords = function()
    {
        if($scope.registrationForm.fields.password == '' || $scope.registrationForm.fields.passwordConfirm == '') {
            return;
        }

        if($scope.registrationForm.fields.password == $scope.registrationForm.fields.passwordConfirm)
        {
            $scope.registrationForm.passwordError = null;
            $scope.registrationForm.passwordSuccess = true;
        }
        else
        {
            $scope.registrationForm.passwordError = true;
            $scope.registrationForm.passwordSuccess = false;
        }

        $scope.registrationForm.checkSubmitDisabled();
    }

    /**
    * Проверка Никнейма
    *
    * return {void}
    */
    $scope.registrationForm.checkUsername = function()
    {
        if($scope.registrationForm.fields.username == '')
            return false;

        Security.checkUsername($scope.registrationForm.fields.username).then(function(response){
            if (response.success) {
                $scope.registrationForm.usernameError = null;
                $scope.registrationForm.usernameSuccess = true;
            } else {
                $scope.registrationForm.usernameError = response.errorMsg;
                $scope.registrationForm.usernameSuccess = false;

            }

            $scope.registrationForm.checkSubmitDisabled();
        });
    };

    /**
    * Срабатывает в момент сабмита формы регистрации
    *
    * @return {void}
    */
    $scope.registrationForm.submit = function(e) {
        $scope.registrationForm.isLoading = true;
        $scope.registrationForm.submitDisabled = true;
    }

    /**
    * Обработчик загузки iframe'а, куда отправляется форма регистрации.
    * Вызывает обработчик сабмита в скоупе контроллера.
    *
    * return {void}
    */
    window.registrationFormIFrameOnLoadHandler = function(iframe)
    {
        var response = Utils.getIFrameJsonContent(iframe);

        if(response !== false)
        {
            var scope = angular.element(document.getElementById('registrationFormModal')).scope();
            scope.$apply($scope.registrationForm.onSubmitHandler(response));
        }
    };

    /**
    * Обработчик ответа формы после сабмита
    *
    * return {void}
    */
    $scope.registrationForm.onSubmitHandler = function(response)
    {
        if (response.success) {
            $scope.registrationForm.confirmEmailSent = true;
            $scope.registrationForm.submitErrors = [];
        } else {
            $scope.registrationForm.submitErrors = response.errors;
        }

        $scope.registrationForm.isLoading = false;
        $scope.registrationForm.checkSubmitDisabled();
    };

    /**
    * Проверяет реферальный хэш в строке запроса
    * Если он есть, то отправляет запрос на получение данных о пригласившем пользователе
    * и заполняет соответсвующие переменные скоупа
    *
    * @return {void}
    */
    $scope.registrationForm.checkReferral = function()
    {
        var code = Security.parseReferralCodeFromLocationHash();

        if(!code)
            code = Security.getReferralCodeFromCookie();

        if (code) {
            $scope.registrationForm.fields.referralCode = code;
            Security.getUserByReferralCode($scope.registrationForm.fields.referralCode).then(function(userData){
                $scope.registrationForm.referralUser = userData;
            });
        }
    };
}]);
