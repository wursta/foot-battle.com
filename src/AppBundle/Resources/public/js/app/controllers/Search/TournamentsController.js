fbApp.controller('SearchTournamentsCtrl', ['$scope', 'SearchTournaments', function($scope, SearchTournaments){

    $scope.searchTournaments = {
        currentPage: 1,
        pageSize: 20,
        totalItems: null,
        totalPages: null,
        filter: {
            isReject: false,
            format: null,
            status: null,
            priceFrom: null,
            priceTo: null,
            onlyPromo: null,
        },
        tournaments: []
    };

    /**
    * Применяет фильтр при изменении
    *
    */
    $scope.searchTournaments.applyFilter = function() {
      $scope.currentPage = 1;
      $scope.searchTournaments.loadPage($scope.currentPage);
    },

    /**
    * Загружает турниры по фильтру с паджинацией
    *
    * @param {Integer} page Текущая страница.
    *
    * @return {void}
    */
    $scope.searchTournaments.loadPage = function(page) {
        SearchTournaments.load(
            page,
            $scope.searchTournaments.filter,
            { pageSize: $scope.searchTournaments.pageSize }
        ).then(function(data, responseHeaders){
            $scope.searchTournaments.totalItems = data.total;
            $scope.searchTournaments.tournaments = data.items;
        });
    };

}]);