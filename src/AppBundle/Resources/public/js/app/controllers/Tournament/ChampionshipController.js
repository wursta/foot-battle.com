fbApp.controller('TournamentChampionshipCtrl', ['$scope', '$http', 'Tournament', function($scope, $http, Tournament) {
  
  $scope.activePill = (Tournament.isOver($scope.tournament.status)) ? 1 : 0;        
  $scope.results = [];  
  $scope.winners = [];  
    
  /**
  * Загрузка турнира
  * @param int round Номер тура
  */
  $scope.loadTournament = function(round) {      
      $scope.tournament.currentRound = round;      
      $scope.loadMatches();
  },
  
  /**
  * Загрузка матчей
  */
  $scope.loadMatches = function() {
      var promiseObj = Tournament.loadMatches($scope.tournament.id);
      promiseObj.then(function(matchesByRounds) {
          $scope.tournament.matchesByRounds = matchesByRounds;
      });      
  },
  
  /**
  * Загрузка результатов тура  
  */
  $scope.loadResults = function() {
      if(!$scope.tournament.currentRound)
        return;
      
      var params = { 
          tournamentId: $scope.tournament.id, 
          roundNum: $scope.tournament.currentRound
      }
      
      $http.post( Routing.generate('tournament/championship/results.json', params) ).then(function(response){
        $scope.results = response.data;
      });
  },
  
  /**
  * Загруза победителей турнира  
  */
  $scope.loadWinners = function() {
    $http.post( Routing.generate('tournament/winners.json', { tournamentId: $scope.tournament.id }) ).then(function(response){
      $scope.winners = response.data;
    });
  }
  
  /**
  * При смене тура необходимо перезагрузить результаты
  */
  $scope.$watch('tournament.currentRound', function(newval, oldval){
      $scope.loadResults();
  });
  
}]);