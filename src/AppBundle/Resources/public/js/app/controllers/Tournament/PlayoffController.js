fbApp.controller('TournamentPlayoffCtrl', ['$scope', '$http', 'Playoff', function($scope, $http, Playoff) {

    $scope.activePill = (Playoff.isOver($scope.tournament.status)) ? 1 : 0;
    $scope.gamesPairs = [];
    $scope.winners = [];
    $scope.qualificationResults = [];
    $scope.currentStage = TOURNAMENT_STAGE;
    $scope.stage = $scope.currentStage;
    $scope.activePlayoffPill = (Playoff.isQualificationStage($scope.currentStage)) ? 0 : 1

    /**
    * Загрузка турнира стадии "Квалификация"
    * @param int round Номер тура
    */
    $scope.loadQualification = function(stage, round) {
        $scope.stage = 'QUALIFICATION';

        if(Playoff.isQualificationStage($scope.currentStage))
            $scope.tournament.currentRound = round;
        else
            $scope.tournament.currentRound = 1;

        $scope.loadQualificationMatches();
    },

    /**
    * Загрузка турнира стадии "Плей-офф"
    * @param int round Номер тура
    */
    $scope.loadPlayoff = function(round) {
        $scope.stage = 'PLAYOFF';

        $scope.tournament.currentRound = round;
        $scope.loadPlayoffMatches();
    },

    /**
    * Загрузка матчей плей-офф
    */
    $scope.loadQualificationMatches = function() {
        var promiseObj = Playoff.loadMatches($scope.tournament.id, 'QUALIFICATION');
        promiseObj.then(function(matchesByRounds) {
            $scope.tournament.matchesByRounds = matchesByRounds;
        });
    },

    /**
    * Загрузка матчей плей-офф
    */
    $scope.loadPlayoffMatches = function() {
        var promiseObj = Playoff.loadMatches($scope.tournament.id, 'PLAYOFF');
        promiseObj.then(function(matchesByRounds) {
            $scope.tournament.matchesByRounds = matchesByRounds;
        });
    },

    /**
    * Загрузка результатов тура квалификации
    */
    $scope.loadQualificationResults = function() {
      if(!$scope.tournament.currentRound)
        return;

      var promiseObj = Playoff.loadQualificationResults($scope.tournament.id, $scope.tournament.currentRound);
        promiseObj.then(function(results) {
            $scope.qualificationResults = results;
        });
    },

    /**
    * Загрузка игр тура плей-офф
    */
    $scope.loadGames = function() {
      if(!$scope.tournament.currentRound)
        return;

      var promiseObj = Playoff.loadGames($scope.tournament.id, $scope.tournament.currentRound);
        promiseObj.then(function(gamesPairs) {
            $scope.gamesPairs = gamesPairs;
        });
    },

    /**
    * Загруза победителей турнира
    */
    $scope.loadWinners = function() {
        $http.post( Routing.generate('tournament/winners.json', { tournamentId: $scope.tournament.id }) ).then(function(response){
            $scope.winners = response.data;
        });
    }

    /**
    * При смене тура необходимо перезагрузить игры
    */
    $scope.$watch('tournament.currentRound', function(newval, oldval){
        if(Playoff.isQualificationStage($scope.stage))
            $scope.loadQualificationResults();
        else
            $scope.loadGames();
    });
}]);