fbApp.controller('TournamentRoundCtrl', ['$scope', '$http', 'Tournament', function($scope, $http, Tournament) {

    $scope.activePill = (Tournament.isOver($scope.tournament.status)) ? 1 : 0;        
    $scope.results = [];  
    $scope.winners = [];  
    $scope.games = [];    
    $scope.gamesInfo = [];
    $scope.gamesAccordionConfig = {
        accordionGroupOpen:[]
    }

    /**
    * Загрузка турнира
    * @param int round Номер тура
    */
    $scope.loadTournament = function(round) {      
        $scope.tournament.currentRound = round;      
        $scope.loadMatches();
    },

    /**
    * Загрузка матчей
    */
    $scope.loadMatches = function() {
        var promiseObj = Tournament.loadMatches($scope.tournament.id);
        promiseObj.then(function(matchesByRounds) {
            $scope.tournament.matchesByRounds = matchesByRounds;
        });      
    },

    /**
    * Загрузка результатов тура  
    */
    $scope.loadResults = function() {
        if(!$scope.tournament.currentRound)
            return;

        var params = { 
            tournamentId: $scope.tournament.id, 
            roundNum: $scope.tournament.currentRound
        }

        $http.post( Routing.generate('tournament/round/results.json', params) ).then(function(response){
            $scope.results = response.data;
        });
    },

    /**
    * Загрузка игр тура  
    */
    $scope.loadGames = function() {      
        if(!$scope.tournament.currentRound)
            return;

        var params = {
            tournamentId: $scope.tournament.id, 
            roundNum: $scope.tournament.currentRound
        }

        $http.post( Routing.generate('tournament/round/games.json', params) ).then(function(response){
            $scope.games = response.data;
        });
    },

    /**
    * Загруза победителей турнира  
    */
    $scope.loadWinners = function() {
        $http.post( Routing.generate('tournament/winners.json', { tournamentId: $scope.tournament.id }) ).then(function(response){
            $scope.winners = response.data;
        });
    }

    $scope.showGameInfo = function(gameId) {        
        $http.post( Routing.generate('tournament/round/game/info.json', { tournamentId: $scope.tournament.id, gameId: gameId }) ).then(function(response){            
            $scope.gamesInfo[gameId] = response.data;            
        });
    }

    /**
    * При смене тура необходимо перезагрузить результаты
    */
    $scope.$watch('tournament.currentRound', function(newval, oldval){
        $scope.loadResults();
        $scope.loadGames();
    });

    $scope.$watchCollection('games', function(newval, oldval){
        $scope.gamesAccordionConfig.accordionGroupOpen = [];
        for(var i in $scope.games)
        {            
            $scope.gamesAccordionConfig.accordionGroupOpen[$scope.games[i].id] = false;
        }            
    });

    $scope.$watchCollection('gamesAccordionConfig.accordionGroupOpen', function(newval, oldval) {
        //При создании массива
        if(newval.length == 0)
              return;
        
        for(var i in newval)
        {
            if(newval[i] == true && oldval[i] !== true)
            {                
                $scope.showGameInfo(i);
            }
        }        
    });
    
}]);