fbApp.controller('TournamentCtrl', [
    '$scope',
    '$http',
    '$uibModal',
    'Tournament',
    'Security',
    'Billing',
    function(
        $scope,
        $http,
        $uibModal,
        Tournament,
        Security,
        Billing
    ) {

    $scope.tournament = {
        id: TOURNAMENT_ID,
        status: TOURNAMENT_STATUS,
        currentRound: (Tournament.isOver(TOURNAMENT_STATUS)) ? null : TOURNAMENT_CURRENT_ROUND,
        matchesByRounds: []
    };

    /**
    * Показывает модальное окно формы авторизации
    *
    * @return {void}
    */
    $scope.tournament.showLoginModal = function(e)
    {
        e.preventDefault();
        Security.openLoginModal();
    };

    /**
    * Показывает модальное окно формы регистрации
    *
    * @return {void}
    */
    $scope.tournament.showRegistrationModal = function(e)
    {
        e.preventDefault();
        Security.openRegistrationModal();
    };

    /**
    * Показывает модальное окно формы авторизации
    *
    * @return {void}
    */
    $scope.tournament.showRefillModal = function(e)
    {
        e.preventDefault();
        Billing.openRefillChoicesModal();
    };

    $scope.tournament.showRegulationsModal = function(tournamentId) {

        $http.post( Routing.generate('tournament/regulations.json', { tournamentId: tournamentId }) ).then(function(response){
            var modalInstance = $uibModal.open({
                template: '<div class="modal-header"><h3 class="modal-title">{{ modalTitle }}</h3></div><div class="modal-body" ng-bind-html="modalBody"></div><div class="modal-footer"><button class="btn btn-warning" type="button" ng-click="cancel()">' + TRANS.CLOSE + '</button></div>',
                controller: 'TournamentModalCtrl',
                size: 'lg',
                resolve: {
                    modalTitle: function() {
                        return response.data.title;
                    },
                    modalBody: function () {
                        return response.data.body;
                    }
                }
            });
        });
    };

    $scope.tournament.showWinDistributionModal = function(tournamentId) {
        $http.post( Routing.generate('tournament/windistribution.json', { tournamentId: tournamentId }) ).then(function(response){
            var modalInstance = $uibModal.open({
                template: '<div class="modal-header"><h3 class="modal-title">{{ modalTitle }}</h3></div><div class="modal-body" ng-bind-html="modalBody"></div><div class="modal-footer"><button class="btn btn-warning" type="button" ng-click="cancel()">' + TRANS.CLOSE + '</button></div>',
                controller: 'TournamentModalCtrl',
                size: 'sm',
                resolve: {
                    modalTitle: function() {
                        return response.data.title;
                    },
                    modalBody: function () {
                        return response.data.body;
                    }
                }
            });
        });
    };
}]);

fbApp.controller('TournamentModalCtrl', function($scope, $sce, $uibModalInstance, modalTitle, modalBody) {
    $scope.modalTitle =  modalTitle;
    $scope.modalBody =  $sce.trustAsHtml(modalBody);
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

});