angular.module('BillingLogActionDirective', [])
.directive('billinglogaction', function() {
    return {
        restrict: 'E',
        scope: {
            action: '=',
            textDeposit: '@',
            textWithdraw: '@',
            textTransfer: '@',
            textReservation: '@',
            textReservationReturn: '@',
            textReservationOff: '@',
            textChargeWin: '@',
            textChargePromoWin: '@',
            textCashout: '@',
            textReferralCharge: '@',

            title: '@',
            textClass: '@'
        },
        link: function($scope, element, attrs) {
            switch($scope.action)
            {
                case 'DEPOSIT':
                    $scope.title = $scope.textDeposit;
                    $scope.textClass = "text-success";
                    break;

                case 'WITHDRAW':
                    $scope.title = $scope.textWithdraw;
                    $scope.textClass = "text-danger";
                    break;

                case 'TRANSFER':
                    $scope.title = $scope.textTransfer;
                    $scope.textClass = "text-info";
                    break;

                case 'RESERVATION':
                    $scope.title = $scope.textReservation;
                    $scope.textClass = "text-info";
                    break;

                case 'RESERVATION_RETURN':
                    $scope.title = $scope.textReservationReturn;
                    $scope.textClass = "text-info";
                    break;

                case 'RESERVATION_OFF':
                    $scope.title = $scope.textReservationOff;
                    $scope.textClass = "text-warning";
                    break;

                case 'CHARGE_WIN':
                    $scope.title = $scope.textChargeWin;
                    $scope.textClass = "text-success";
                    break;

                case 'CHARGE_PROMO_WIN':
                    $scope.title = $scope.textChargePromoWin;
                    $scope.textClass = "text-success";
                    break;

                case 'CASHOUT':
                    $scope.title = $scope.textCashout;
                    $scope.textClass = "text-danger";
                    break;

                case 'REFERRAL_CHARGE':
                    $scope.title = $scope.textReferralCharge;
                    $scope.textClass = "text-success";
                    break;
            }
        },
        template: '<span class="{{textClass}}">{{title}}</span>'
    };
});