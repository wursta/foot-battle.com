angular.module('BillingLogStatusDirective', [])
.directive('billinglogstatus', function() {
    return {
        restrict: 'E',
        scope: {
            status: '=',
            textStatusOk: '@',
            textStatusInProgress: '@',
            textStatusCanceled: '@',

            title: '@',
            textClass: '@'
        },
        link: function($scope, element, attrs) {
            switch($scope.status)
            {
                case 0:
                    $scope.title = $scope.textStatusInProgress;
                    $scope.textClass = "text-info";
                    break;

                case 1:
                    $scope.title = $scope.textStatusOk;
                    $scope.textClass = "text-success";
                    break;

                case -1:
                    $scope.title = $scope.textStatusCanceled;
                    $scope.textClass = "text-danger";
                    break;
            }
        },
        template: '<span class="{{textClass}}">{{title}}</span>'
    };
});