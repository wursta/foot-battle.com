angular.module('DatetimeDirective', [])
  .directive('datetime', function() {
  return {
    restrict: 'E',
    scope: {
      datetime: '=',
    },
    template: '{{ datetime | date: "dd.MM.yy HH:mm" }}'
  };
});