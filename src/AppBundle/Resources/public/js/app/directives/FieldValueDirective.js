angular.module('FieldValueDirective', [])
.directive("initFieldValue", function ($parse) {
    return {
        link: function (scope, element, attrs) {
            var attr = attrs.initFromForm || attrs.ngModel || element.attrs('name'),
            val = attrs.value || element.val();
            $parse(attr).assign(scope, val)
        }
    };
});