angular.module('LeagueIconDirective', [])  
  .directive('leagueicon', function() {    
  return {
    restrict: 'E',
    scope: {
      title: '=',
      icon: '=',
      tooltipPlacement: "@"
    },
    template: '<img uib-tooltip="{{ title }}" tooltip-placement="{{ tooltipPlacement }}" alt="{{ title }}" ng-src="/img/leagues/icons/sm/{{ icon }}" />'
  };
});