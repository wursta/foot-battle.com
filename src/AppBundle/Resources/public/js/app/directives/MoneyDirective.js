angular.module('MoneyDirective', [])
.directive('money', function() {
    return {
        restrict: 'E',
        scope: {
            qty: '=',
            zeroText: '@'
        },
        template: '<span class="money" ng-if="qty != 0">{{qty}} <img src="/img/currency.png" /></span><span ng-if="qty == 0">{{zeroText}}</span>'
    }
});
