angular.module('TournamentMatchesDirective', [])  
  .directive('tournamentmatches', function() {
  return {
    restrict: 'E',
    controller: ['$scope', '$timeout', 'Tournament', function($scope, $timeout, Tournament) {
        
        $scope.matchesByRounds = [];        
        $scope.accordionGroupOpen = [];
        $scope.usersBets = [];
        $scope.usersBetsPopoverIsOpen = [];
        $scope.usersBetsPopoverTmpl = 'usersBetsPopoverTmpl.html';
        
        $scope.matchesAccordionConfig = {
            showOneMatchesRoundAtTime: true,
            accordionGroupOpen: []
        }
        
        $scope.$watchCollection('matchesByRounds', function(newval, oldval){            
            if(newval == oldval)
                return;
                        
            $scope.matchesAccordionConfig.accordionGroupOpen = [];
            for(var i in $scope.matchesByRounds)
            {
                var isOpen = false;
                if($scope.currentRound == i)
                    isOpen = true;
                    
                $scope.matchesAccordionConfig.accordionGroupOpen[i] = isOpen;
            }            
        });
        
        $scope.$watchCollection('matchesAccordionConfig.accordionGroupOpen', function(newval, oldval){            
            //При создании массива
            if(newval.length == 0)
              return;
            
            if(!newval[$scope.currentRound])
            {
              var round = newval.indexOf(true);
              
              if(round > 0)
                $scope.currentRound = round;
            }
        });
        
        $scope.showUsersBets = function(matchId) {
            
            $scope.usersBetsPopoverIsOpen = [];
            Tournament.loadUsersBets($scope.tournamentId, matchId).then(function(usersBets){
                $scope.usersBets[matchId] = usersBets;
                $scope.usersBetsPopoverIsOpen[matchId] = true;
            });
        }
        
        /**
        * Сохранение прогноза
        * 
        */
        $scope.saveBet = function(matchId, bet) {
            if(isNaN(bet.scoreLeft) || isNaN(bet.scoreRight))
              return;

            bet.saving = true;            
            
            Tournament.saveBet($scope.tournamentId, matchId, bet.scoreLeft, bet.scoreRight).then(function(response){                
                bet.savedSuccess = true;

                $timeout(function(){
                    bet.saving = false;
                    bet.savedSuccess = false;
                }, 500);
                
                if(!response.success)
                    alert(response.msg);
            }, function(response) {
              bet.saving = false;
              
              alert("Произошла непредвиденная ошибка при сохранении прогноза.");
            });
        }
    }],    
    scope: {
        tournamentId: "=",
        matchesByRounds: "=",
        currentRound: "="
    },
    templateUrl: 'tournamentMatchesAccordionTmpl.html'
  };
});