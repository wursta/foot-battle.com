angular.module('TournamentPlayoffPairsDirective', [])
.controller('TournamentPlayoffGameModalInstanceCtrl', ['$scope', '$uibModalInstance', 'gameInfo', function($scope, $uibModalInstance, gameInfo){
    $scope.gameInfo = gameInfo;

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}])
.directive('playoffpairs', function() {
    return {
        restrict: 'E',
        controller: ['$scope', '$uibModal', 'Playoff', function($scope, $uibModal, Playoff) {

            $scope.showGameInfo = function(tournamentId, gameId) {
                Playoff.loadGameInfo(tournamentId, gameId).then(function(gameInfo){

                    var modalInstance = $uibModal.open({
                        templateUrl: 'tournamentPlayoffGameInfoTmpl.html',
                        controller: 'TournamentPlayoffGameModalInstanceCtrl',
                        resolve: {
                            gameInfo: function() {
                                return gameInfo;
                            }
                        }
                    });

                });
            }
        }],
        scope: {
            tournamentId: "=",
            gamesPairs: "=",
        },
        templateUrl: 'tournamentPlayoffGamePairTmpl.html'
    }
});