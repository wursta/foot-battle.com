angular.module('TournamentStatusDirective', [])
  .directive('tournamentstatus', function() {
  return {
    restrict: 'E',
    scope: {
      code: '=',
      textStatusIsNotStarted: '@',
      textStatusGoes: '@',
      textStatusOver: '@',
      title: '@',
      spanClass: '@',
    },
    link: function($scope, element, attrs) {

        if($scope.textStatusIsNotStarted == undefined) {
            $scope.textStatusIsNotStarted = 'Идет регистрация';
        }

        if($scope.textStatusGoes == undefined) {
            $scope.textStatusGoes = 'Турнир идёт';
        }

        if($scope.textStatusOver == undefined) {
            $scope.textStatusOver = 'Турнир окончен';
        }

        switch($scope.code)
        {
            case 'STATUS_NOT_STARTED':
                $scope.title = $scope.textStatusIsNotStarted;
                $scope.spanClass = "text-success";
            break;

            case 'STATUS_GOES':
                $scope.title = $scope.textStatusGoes;
                $scope.spanClass = "text-info";
            break;

            case 'STATUS_OVER':
                $scope.title = $scope.textStatusOver;
                $scope.spanClass = "text-default";
            break;
        }
    },
    template: '<span class="tournament_status {{spanClass}}">{{title}}</span>'
  };
});