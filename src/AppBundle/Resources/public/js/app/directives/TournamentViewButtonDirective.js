angular.module('TournamentViewButtonDirective', [])
.directive('tournamentviewbutton', function() {
    return {
      restrict: 'E',
      scope: {
        code: '=',
        href: "=",
        textStatusIsNotStarted: '@',
        textStatusGoes: '@',
        textStatusOver: '@',
        title: '@',
        btnClass: '@',
      },
      link: function($scope, element, attrs) {
        switch($scope.code)
        {
          case 'STATUS_NOT_STARTED':
            $scope.title = $scope.textStatusIsNotStarted;
            $scope.btnClass = "btn-success";
          break;

          case 'STATUS_GOES':
            $scope.title = $scope.textStatusGoes;
            $scope.btnClass = "btn-primary";
          break;

          case 'STATUS_OVER':
            $scope.title = $scope.textStatusOver;
            $scope.btnClass = "btn-default";
          break;
        }
      },
      template: '<a href="{{href}}" class="btn btn-sm btn-block tournament-enter-btn {{btnClass}}"><span class="hidden-xs hidden-sm">{{title}}</span><i class="visible-xs visible-sm glyphicon glyphicon-chevron-right"></i></a>'
    };
  });