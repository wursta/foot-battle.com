angular.module('UserRatingDirective', [])  
  .directive('userrating', function() {    
  return {
    restrict: 'E',
    scope: {
      val: '='
    },
    template: '<abbr class="initialism" uib-tooltip="{{val | dropDigits: 4}}">{{val | dropDigits: 2}}</abbr>'
  };
})
  .filter('dropDigits', function() {
    return function(floatNum, precision) {
        return String(floatNum)
            .split('.')
            .map(function (d, i) { return i ? d.substr(0, precision) : d; })
            .join('.');
    };
});