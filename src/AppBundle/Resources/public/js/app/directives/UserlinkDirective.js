angular.module('UserlinkDirective', [])
  .directive('userlink', function() {
  return {
    restrict: 'E',
    scope: {
      userId: '=',
      userAvatar: '=',
      noAvatar: '=',
      userAvatarSrc: '@',
      userNick: '=',
    },
    link: function($scope, element, attrs) {
      if(!$scope.noAvatar)
      {
          if(!$scope.userAvatar)
            $scope.userAvatarSrc = '/img/user_no_avatar.png';
          else
            $scope.userAvatarSrc = '/data/user/avatars/' + $scope.userAvatar;
      }
    },
    template: '<span class="userlink"><span class="userimage-wrapper hidden-xs" ng-hide="noAvatar"><img class="img-circle" ng-src="{{ userAvatarSrc }}"></span><span class="usernick-wrapper">{{ userNick }}</span></span>'
  };
});