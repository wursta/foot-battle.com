angular.module('BillingService', [])
.factory('Billing', ['$uibModal', '$http', '$q', 'Utils', function($uibModal, $http, $q, Utils) {
    return {

        /**
        * Открывает и возвращает модальное окно с выбором возможностей пополнения баланса
        *
        * @return {void}
        */
        openRefillChoicesModal: function()
        {
            return this.openRefillQiwiModal();
        },

        /**
        * Открывает и возвращает модальное окно с пополнением через Qiwi-кошелёк
        *
        * @return {void}
        */
        openRefillQiwiModal: function()
        {
            return $uibModal.open({
                templateUrl: 'refillQiwiModalTmpl.html',
                controller: 'BillingModalCtrl',
                backdrop: 'static',
                size: 'lg',
            });
        },

        /**
        * Отправляет запрос на валидацию данных пополнения через Qiwi-кошелёк.
        * В случае успеха возвращает данные о пополнении.
        *
        * @param {String} qiwiPhone Счёт в Qiwi-кошельке
        * @param {Float} amount Сумма пополнения
        *
        * @return {Promise}
        */
        confirmQiwiRefill: function(qiwiPhone, amount)
        {
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: Routing.generate('billing/refill/qiwi/validate.json'),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                },
                transformRequest: Utils.transformJsonToString,
                data: {
                    qiwiPhone: qiwiPhone,
                    amount: amount
                }
            })
            .success(function(data, status, headers, config){
                deferred.resolve(data);
            })
            .error(function(response, status, headers, config){
                deferred.reject(status);
            });

            return deferred.promise;
        },

        /**
        * Отправляет запрос на создание операции пополнения и в Qiwi для редиректа
        *
        * @param {String} qiwiPhone Счёт в Qiwi-кошельке
        * @param {Float} amount Сумма пополнения
        *
        * @return {Promise}
        */
        refillByQiwi: function(qiwiPhone, amount)
        {
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: Routing.generate('billing/refill/qiwi/submit.json'),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                },
                transformRequest: Utils.transformJsonToString,
                data: {
                    qiwiPhone: qiwiPhone,
                    amount: amount
                }
            })
            .success(function(data, status, headers, config){
                deferred.resolve(data);
            })
            .error(function(response, status, headers, config){
                deferred.reject(status);
            });

            return deferred.promise;
        }

    }
}])
.controller('BillingModalCtrl', ['$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

}]);