angular.module('SearchTournamentsService', [])
.factory('SearchTournaments', ['$http', '$q', 'Utils', function($http, $q, Utils) {
    return {

        /**
        * Отправляет запрос на получение турниров пользователя по фильтру с паджинацией
        *
        * @param {Integer} page    Номер страницы.
        * @param {Object}  filter  Объект с фильтрами.
        * @param {Object}  options Объект с дополнительными опциями.
        *
        * @return {Promise}
        */
        load: function(page, filter, options) {

            var deferred = $q.defer();

            var data = {
              page: page,
              filter: filter,
              options: options
            }

            $http({
                method: "POST",
                url: Routing.generate('search/tournaments.json'),
                data: data
            } )
            .success(function(data, status, headers, config){
                deferred.resolve(data);
            })
            .error(function(response, status, headers, config){
                deferred.reject(status);
            });

            return deferred.promise;

        }

    }
}]);