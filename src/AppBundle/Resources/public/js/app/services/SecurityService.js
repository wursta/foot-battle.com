angular.module('SecurityService', ['ngCookies'])
.factory('Security', ['$http', '$q', '$uibModal', '$cookies', 'Utils', function($http, $q, $uibModal, $cookies, Utils) {
    return {

        /**
        * Открывает и возвращает модальное окно авторизации
        *
        * @return {Object}
        */
        openLoginModal: function() {
            return $uibModal.open({
                templateUrl: 'loginModalTmpl.html',
                controller: 'ModalCtrl',
                size: 'sm',
            });
        },

        /**
        * Открывает и возвращает модальное окно регистрации
        *
        * @return {Object}
        */
        openRegistrationModal: function() {
            return $uibModal.open({
                templateUrl: 'registrationModalTmpl.html',
                controller: 'ModalCtrl',
                size: 'lg',
            });
        },

        /**
        * Авторизация пользователя по логину и паролю
        *
        * @param username {String}  Юзернейм.
        * @param password {String}  Пароль
        * @param remember {Boolean} Флаг "Запомнить меня"
        *
        * @return {Pormise}
        */
        authorize: function(username, password, remember) {

            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: Routing.generate('login_check'),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                },
                transformRequest: Utils.transformJsonToString,
                data: {
                    login: true,
                    _username: username,
                    _password: password,
                    _remember_me: remember ? 'on' : undefined
                }
            })
            .success(function(data, status, headers, config){
                deferred.resolve(data);
            })
            .error(function(response, status, headers, config){
                deferred.reject(status);
            });

            return deferred.promise;
        },

        /**
        * Проверка email'а.
        *
        * @param {String} email
        *
        * @return {Promise}
        */
        checkEmail: function(email) {

            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: Routing.generate('email_check.json'),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                },
                transformRequest: Utils.transformJsonToString,
                data: {
                    email: email
                }
            })
            .success(function(data, status, headers, config){
                deferred.resolve(data);
            })
            .error(function(response, status, headers, config){
                deferred.reject(status);
            });

            return deferred.promise;
        },

        /**
        * Проверка username'а.
        *
        * @param {String} username
        * @param {Object} options
        *
        * @return {Promise}
        */
        checkUsername: function(username, options) {

            var deferred = $q.defer();

            var postParams = {
                username: username
            };

            if(options == undefined)
                options = {};

            if(options.excludeUserId != undefined)
                postParams.excludeUserId = options.excludeUserId;

            $http({
                method: 'POST',
                url: Routing.generate('username_check.json'),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                },
                transformRequest: Utils.transformJsonToString,
                data: postParams
            })
            .success(function(data, status, headers, config){
                deferred.resolve(data);
            })
            .error(function(response, status, headers, config){
                deferred.reject(status);
            });

            return deferred.promise;
        },

        /**
        * Парсит хэш из строки запроса.
        *
        * @returns {String|Boolean} Возвращает код реферала или false, если его нет.
        */
        parseReferralCodeFromLocationHash: function() {
            var code = window.location.hash.replace('#', '');
            var isReferralHash = code.indexOf('referral:');

            if (code != '' && isReferralHash != -1) {
                return code.replace('referral:', '');
            }

            return false;
        },

        /**
        * Получает данные о пользователе по реферральному коду
        *
        * @param {string} code Реферальный код

        * @return {Promise}
        */
        getUserByReferralCode: function(code) {
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: Routing.generate('get_user_by_referral_code.json', { referralCode: code })
            })
            .success(function(data, status, headers, config){
                deferred.resolve(data);
            })
            .error(function(response, status, headers, config){
                deferred.reject(status);
            });

            return deferred.promise;
        },

        /**
        * Сохраняет код реферала в куку на 2 недели
        *
        * @param {String} code Код реферрала
        *
        * @return {void}
        */
        saveReferralCodeCookie: function(code) {
            var expires = new Date();
            expires.setDate( expires.getDate() + 14 )
            $cookies.put(
                'referralCode',
                code,
                {
                    path: '/',
                    expires: expires
                }
            );
        },

        /**
        * Возвращает код реферала из куки
        *
        * @param {String} code Код реферрала
        *
        * @return {String|Boolean} Код реферала, если кука есть или false, если её нет.
        */
        getReferralCodeFromCookie: function() {
            var code = $cookies.get('referralCode');

            if (code != undefined) {
                return code;
            }
            return false;
        },

        /**
        * Отправляет запрос на восстановление пароля
        *
        * @param {String} email E-mail пользователя
        *
        * @return {Promise}
        */
        passwordRecovery: function(email) {
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: Routing.generate('password_recovery.json'),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                },
                transformRequest: Utils.transformJsonToString,
                data: {
                    email: email
                }
            })
            .success(function(data, status, headers, config){
                deferred.resolve(data);
            })
            .error(function(response, status, headers, config){
                deferred.reject(status);
            });

            return deferred.promise;
        }

    }
}]);