angular.module('PlayoffService', [])
.factory('Playoff', ['Tournament', '$http', '$q', function(Tournament, $http, $q) {

    Tournament.isQualificationStage = function(stage) {
        return (stage == 'QUALIFICATION');
    }

    Tournament.loadMatches = function(tournamentId, stage) {

        var url = 'tournament/playoff/playoff_matches.json';
        if(stage == 'QUALIFICATION')
            var url = 'tournament/playoff/qualification_matches.json';

        var deferred = $q.defer();
        $http({method: "POST", url: Routing.generate(url, { tournamentId: tournamentId }) } )
        .success(function(data, status, headers, config){
            deferred.resolve(data);
        })
        .error(function(response, status, headers, config){
            deferred.reject(status);
        });

        return deferred.promise;
    }

    Tournament.loadQualificationResults = function(tournamentId, roundNum) {

        var deferred = $q.defer();

        var params = {
          tournamentId: tournamentId,
          roundNum: roundNum
        }

        $http({method: "POST", url: Routing.generate('tournament/playoff/qualification_results.json', params) } )
        .success(function(data, status, headers, config){
            deferred.resolve(data);
        })
        .error(function(response, status, headers, config){
            deferred.reject(status);
        });

        return deferred.promise;
    }

    Tournament.loadGames = function(tournamentId, roundNum) {

        var deferred = $q.defer();

        var params = {
          tournamentId: tournamentId,
          roundNum: roundNum
        }

        $http({method: "POST", url: Routing.generate('tournament/playoff/games.json', params) } )
        .success(function(data, status, headers, config){
            deferred.resolve(data);
        })
        .error(function(response, status, headers, config){
            deferred.reject(status);
        });

        return deferred.promise;
    }

    Tournament.loadGameInfo = function(tournamentId, gameId) {
        var deferred = $q.defer();

        var params = {
            tournamentId: tournamentId,
            gameId: gameId
        }

        $http({method: "POST", url: Routing.generate('tournament/playoff/game/info.json', params) } )
        .success(function(data, status, headers, config){
            deferred.resolve(data);
        })
        .error(function(response, status, headers, config){
            deferred.reject(status);
        });

        return deferred.promise;
    }

    return Tournament;
}]);