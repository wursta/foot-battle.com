angular.module('TournamentService', [])
.factory('Tournament', ['$http', '$q', function($http, $q) {
  return {
      isOver: function(status) {
          return status == 'STATUS_OVER';
      },

      loadMatches: function(tournamentId) {
          var deferred = $q.defer();
          $http({method: "POST", url: Routing.generate('tournament/matches.json', { tournamentId: tournamentId }) } )
            .success(function(data, status, headers, config){
              deferred.resolve(data);
            })
            .error(function(response, status, headers, config){
              deferred.reject(status);
            });

          return deferred.promise;
      },

      loadUsersBets: function(tournamentId, matchId) {
          var deferred = $q.defer();
          $http({method: "POST", url: Routing.generate('tournament/bet/list.json', { tournamentId: tournamentId, matchId: matchId }) } )
            .success(function(data, status, headers, config){
              deferred.resolve(data);
            })
            .error(function(response, status, headers, config){
              deferred.reject(status);
            });

          return deferred.promise;
      },

      saveBet: function(tournamentId, matchId, scoreLeft, scoreRight) {

          var params = {
            scoreLeft: scoreLeft,
            scoreRight: scoreRight,
          }

          var deferred = $q.defer();
          $http.post(Routing.generate('tournament/bet/save.json', { tournamentId: tournamentId, matchId: matchId }), params)
            .success(function(data, status, headers, config){
              deferred.resolve(data);
            })
            .error(function(response, status, headers, config){
              deferred.reject(status);
            });

          return deferred.promise;
      }
  }
}]);