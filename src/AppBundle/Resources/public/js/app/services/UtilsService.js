angular.module('UtilsService', [])
.factory('Utils', [function() {
    return {

        /**
        * Превращает объект JSON в строку вида foo=bar&bar=foo
        *
        * @param {Object} obj
        *
        * @return {String}
        */
        transformJsonToString: function(obj) {
            var str = [];
            for (var p in obj) {
                if(obj[p] == undefined)
                    continue;

                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }

            return str.join('&');
        },

        /**
        * Получает содержимое объекта iFrame и возвращает JSON объект
        *
        * @param {Element} iframeDOMObject Объект iFrame
        *
        * @return {JSON}
        */
        getIFrameJsonContent: function(iframeDOMObject) {
            var iframeDocument = iframeDOMObject.contentDocument || iframeDOMObject.contentWindow.document;
            var content = iframeDocument.getElementsByTagName('body')[0].innerText;

            if (content == '') {
                return false;
            }

            var jsonContent = JSON.parse(content);
            return jsonContent;
        },

        /**
        * Переключает булево значение на противоположное.
        *
        * @param {Boolean} boolValue Значение.
        *
        * @return {Boolean}
        */
        toggleBoolean: function(boolValue) {
            return (boolValue) ? false : true;
        },

        /**
        * Проверяет, присутствует ли в массиве значение
        *
        * @param {*} needle    Искомое значение.
        * @param {*} haystack  Массив.
        * @param {*} argStrict Если третий параметр strict установлен в TRUE тогда функция,
        * также проверит соответствие типов параметра needle и соответствующего значения массива haystack.
        *
        * @returns {Boolean}
        */
        inArray: function(needle, haystack, argStrict) {
            //  discuss at: http://locutus.io/php/in_array/
            // original by: Kevin van Zonneveld (http://kvz.io)
            // improved by: vlado houba
            // improved by: Jonas Sciangula Street (Joni2Back)
            //    input by: Billy
            // bugfixed by: Brett Zamir (http://brett-zamir.me)
            //   example 1: in_array('van', ['Kevin', 'van', 'Zonneveld'])
            //   returns 1: true
            //   example 2: in_array('vlado', {0: 'Kevin', vlado: 'van', 1: 'Zonneveld'})
            //   returns 2: false
            //   example 3: in_array(1, ['1', '2', '3'])
            //   example 3: in_array(1, ['1', '2', '3'], false)
            //   returns 3: true
            //   returns 3: true
            //   example 4: in_array(1, ['1', '2', '3'], true)
            //   returns 4: false

            var key = ''
            var strict = !!argStrict

            // we prevent the double check (strict && arr[key] === ndl) || (!strict && arr[key] === ndl)
            // in just one for, in order to improve the performance
            // deciding wich type of comparation will do before walk array
            if (strict) {
                for (key in haystack) {
                    if (haystack[key] === needle) {
                        return true
                    }
                }
            } else {
                for (key in haystack) {
                    if (haystack[key] == needle) { // eslint-disable-line eqeqeq
                        return true
                    }
                }
            }

            return false
        },

        /**
        * Осуществляет поиск данного значения в массиве и возвращает соответствующий ключ в случае удачи.
        *
        * @param {*} needle    Искомое значение.
        * @param {*} haystack  Массив.
        * @param {*} argStrict Если третий параметр strict установлен в TRUE, то функция
        * будет искать идентичные элементы в haystack. Это означает, что также будут проверяться типы
        * needle в haystack, а объекты должны быть одни и тем же экземпляром.
        *
        * @returns {Object}
        */
        arraySearch: function(needle, haystack, argStrict) {
            //  discuss at: http://locutus.io/php/array_search/
            // original by: Kevin van Zonneveld (http://kvz.io)
            //    input by: Brett Zamir (http://brett-zamir.me)
            // bugfixed by: Kevin van Zonneveld (http://kvz.io)
            //        test: skip-all
            //   example 1: array_search('zonneveld', {firstname: 'kevin', middle: 'van', surname: 'zonneveld'})
            //   returns 1: 'surname'

            var strict = !!argStrict
            var key = ''

            if (typeof needle === 'object' && needle.exec) {
                // Duck-type for RegExp
                if (!strict) {
                    // Let's consider case sensitive searches as strict
                    var flags = 'i' + (needle.global ? 'g' : '') +
                    (needle.multiline ? 'm' : '') +
                    // sticky is FF only
                    (needle.sticky ? 'y' : '')
                    needle = new RegExp(needle.source, flags)
                }
                for (key in haystack) {
                    if (haystack.hasOwnProperty(key)) {
                        if (needle.test(haystack[key])) {
                            return key
                        }
                    }
                }
                return false
            }

            for (key in haystack) {
                if (haystack.hasOwnProperty(key)) {
                    if ((strict && haystack[key] === needle) || (!strict && haystack[key] === needle)) {
                        return key
                    }
                }
            }

            return false
        }
    }
}]);