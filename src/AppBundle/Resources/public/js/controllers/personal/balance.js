Balance.getContainer('cashoutBtn').click(function(e){
    e.preventDefault();    
    var btn = $(this);
    
    var url = btn.attr("href");
    
    if(btn.data("loading") == true)
        return false;
        
    Utils.showLoading();
    
    Balance.showCashoutModal({
        url: btn.attr('href'),        
        target: btn,
        onOpen: function() {
            btn.data("loading", false);            
                                    
            Utils.hideLoading();
        },
        onLoadFailure: function() {
            Utils.hideLoading();
        }
    });
    
});