CashoutQiwiModel.getContainer('form').on('submit', function(e){    
    var form = $(this);
    
    if(form.data('pragmaticallysubmit') == true)
        return true;
    
    e.preventDefault();
        
    var confirmUrl = form.data("confirmurl");
    
    if(form.data("loading") == true)
        return false;
    
    var amount = CashoutQiwiModel.getContainer('amountField').val();
    
    if(isNaN(amount))
        return false;    
    
    form.data("loading", true);
    
    Utils.showLoading();
    
    CashoutQiwiModel.load({
        url: confirmUrl,
        postData: form.serialize(),
        success: function(content) {
            form.data("loading", false);
                        
            CashoutQiwiModel.show({
                title: false,                
                html: content,
                buttons: {
                    confirm: {                        
                        handler: function(btn){
                            form.data('pragmaticallysubmit', true);
                            form.submit();
                        }
                    }                    
                }
            });
            
            Utils.hideLoading();
        },
        failure: function() {
            Utils.hideLoading();
        }
    });    
});