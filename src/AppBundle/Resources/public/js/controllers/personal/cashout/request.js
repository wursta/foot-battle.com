$('#cashout_request_card_number').validateCreditCard(function(result){
    var cardName = 'unknown';    
    if(result.card_type != null)
    {
        cardName = result.card_type.name;
    }
    
    $(this).prev().find('img').attr('src', '/img/cardicons/'+cardName+'.png');
});

CashoutRequestModel.getContainer('form').on('submit', function(e){
    var form = $(this);
    
    if(form.data('pragmaticallysubmit') == true)
        return true;
    
    e.preventDefault();
        
    var confirmUrl = form.data("confirmurl");
    
    if(form.data("loading") == true)
        return false;
        
    form.data("loading", true);
    
    Utils.showLoading();
    
    CashoutRequestModel.load({
        url: confirmUrl,
        postData: form.serialize(),
        success: function(content) {
            form.data("loading", false);
                        
            CashoutRequestModel.show({
                title: false,
                html: content,
                buttons: {
                    confirm: {
                        handler: function(btn){
                            form.data('pragmaticallysubmit', true);
                            form.submit();
                        }
                    }                    
                }
            });
            
            Utils.hideLoading();
        },
        failure: function() {
            Utils.hideLoading();
        }
    });    
});