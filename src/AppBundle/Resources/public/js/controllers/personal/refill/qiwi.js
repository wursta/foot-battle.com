RefillQiwiModel.getContainer('form').on('submit', function(e){
    var form = $(this);
    
    if(form.data('pragmaticallysubmit') == true)
        return true;
    
    e.preventDefault();
        
    var confirmUrl = form.data("confirmurl");
    
    if(form.data("loading") == true)
        return false;
    
    var qiwiPhone = RefillQiwiModel.getContainer('qiwiPhoneField').val();
    var amount = RefillQiwiModel.getContainer('qiwiAmountField').val();
    
    if(isNaN(amount))
        return false;
    
    form.data("loading", true);
    
    Utils.showLoading();
    
    RefillQiwiModel.load({
        url: confirmUrl,
        postData: form.serialize(),
        success: function(content) {
            form.data("loading", false);
                        
            RefillQiwiModel.show({
                title: false,                
                html: content,
                buttons: {
                    confirm: {                        
                        handler: function(btn){
                            form.data('pragmaticallysubmit', true);
                            form.submit();
                        }
                    }                    
                }
            });
            
            Utils.hideLoading();
        },
        failure: function() {
            Utils.hideLoading();
        }
    });    
});