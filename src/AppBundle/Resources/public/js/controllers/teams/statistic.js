StatisticModel.getContainer('matches').on('click', '.matchStatisticBtn', function(e){
    e.preventDefault();
        
    var btn = $(this);
    
    if(btn.data("loading") == true)
        return false;
        
    if(btn.data("rendered") == true)    
        return false;
        
    btn.data("loading", true);
    btn.removeClass('glyphicon-stats').addClass('glyphicon-refresh glyphicon-refresh-animate');
    
    StatisticModel.load({
        url: btn.attr('href'),        
        success: function(content) {
            
            btn.data("loading", false);
            btn.data("rendered", true);
            
            btn.addClass('glyphicon-stats').removeClass('glyphicon-refresh glyphicon-refresh-animate');
            
            StatisticModel.show({
                target: btn, 
                html: content
            });                        
        },
        failure: function() {
            btn.addClass('glyphicon-stats').removeClass('glyphicon-refresh glyphicon-refresh-animate');            
        }
    });
});

StatisticModel.getContainer('matches').on('afterclose', '.matchStatisticBtn', function(e){
    var btn = $(this);    
    btn.data("rendered", false);
});