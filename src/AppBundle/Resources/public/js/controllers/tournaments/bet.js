BetModel.getContainer('matches').find('.bet-control').on('focus', function(e){
    var input = $(this);
    
    input.parents(".matchContainer").find('.savingBet').addClass("hidden");
    input.parents(".matchContainer").find('.savingBetFail').addClass("hidden");
    
    input.parents(".matchContainer").find(".saveBetBtn").parent().removeClass("hidden");
    input.parents(".matchContainer").find(".saveBetBtn").removeClass("hidden");    
});

BetModel.getContainer('matches').on('click', '.saveBetBtn', function(e){
    var btn = $(this);
    
    var inputOne = btn.parents('.matchContainer').find('.bet-control-one');
    var inputTwo = btn.parents('.matchContainer').find('.bet-control-two');
    
    var inputOneVal = inputOne.val();
    var inputTwoVal = inputTwo.val();
    
    if(inputOneVal == '' || inputTwoVal == '')
        return;        

    if(isNaN(inputOneVal) || inputOneVal < 0)
        inputOneVal = 0;
        
    if(isNaN(inputTwoVal) || inputTwoVal < 0)
        inputTwoVal = 0;        
    
    inputOneVal = parseInt(inputOneVal);
    inputTwoVal = parseInt(inputTwoVal);
    
    inputOne.val(inputOneVal);
    inputTwo.val(inputTwoVal);
    
    btn.addClass("hidden");
    btn.parent().removeClass("hidden");
    btn.parent().find(".savingBet").removeClass("hidden");
    
    data = {
        _token: btn.data('csrftoken')
    }
    
    if(btn.data("qualificationmatch") != undefined)
        data.qualificationMatch = btn.data("qualificationmatch");
    
    if(btn.data("groupmatch") != undefined)
        data.groupMatch = btn.data("groupmatch");
    
    inputOne.attr("disabled", true);
    inputTwo.attr("disabled", true);
    
    BetModel.save(inputOneVal, inputTwoVal, {        
        url: btn.data('url'),
        params: data,
        success: function() {
            inputOne.attr("disabled", false).parent().removeClass('has-error');
            inputTwo.attr("disabled", false).parent().removeClass('has-error');
            
            btn.addClass("hidden");
            btn.parent().find('.savingBet').addClass("hidden");
            btn.parent().find('.savingBetFail').addClass("hidden");
            btn.parent().addClass("hidden");
        },
        failure: function() {
            inputOne.val('');
            inputTwo.val('');
            inputOne.attr("disabled", false);
            inputTwo.attr("disabled", false);
            btn.parent().find('.savingBet').addClass("hidden");
            btn.parent().find('.savingBetFail').removeClass("hidden");
        }
    });
});