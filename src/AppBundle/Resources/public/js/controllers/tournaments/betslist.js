BetsListModel.getContainer('matches').on('click', '.betsListBtn', function(e){
    e.preventDefault();
    
    var btn = $(this);
    
    if(btn.data("loading") == true)
        return false;
        
    if(btn.data("rendered") == true)    
        return false;
        
    btn.data("loading", true);
    btn.removeClass('glyphicon-list-alt').addClass('glyphicon-refresh glyphicon-refresh-animate');

    data = {};
    
    if(btn.data("qualificationmatch") != undefined)
        data.qualificationMatch = btn.data("qualificationmatch");
        
    if(btn.data("groupmatch") != undefined)
        data.groupMatch = btn.data("groupmatch");
        
    BetsListModel.load({
        url: btn.attr('href'),
        params: data,
        success: function(content) {
            
            btn.data("loading", false);
            btn.data("rendered", true);
            
            btn.addClass('glyphicon-list-alt').removeClass('glyphicon-refresh glyphicon-refresh-animate');
            
            BetsListModel.show({
                target: btn, 
                html: content
            });                        
        },
        failure: function() {
            btn.addClass('glyphicon-list-alt').removeClass('glyphicon-refresh glyphicon-refresh-animate');            
        }
    });
});

BetsListModel.getContainer('matches').on('afterclose', '.betsListBtn', function(e){
    var btn = $(this);    
    btn.data("rendered", false);
});