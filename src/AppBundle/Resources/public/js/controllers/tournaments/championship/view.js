$('#matchesAccordion').on('show.bs.collapse', function (e) {
    if($('#'+$(e.target).attr('aria-labelledby')).data('winners_tab') != 1)
    {
        var results_url = $('#'+$(e.target).attr('aria-labelledby')).data('results_url');        
        
        if(!results_url)
            return;
        
        TournamentChampionship.loadResultsByRound(results_url);
    }
    else
    {
        var winners_url = $('#'+$(e.target).attr('aria-labelledby')).data('winners_url');
        TournamentChampionship.loadWinners(winners_url);
    }
});