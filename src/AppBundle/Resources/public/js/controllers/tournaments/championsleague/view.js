TournamentChampionsleagueModel.getContainer('sidebar').on('click', '#groupsLabels .groupLabel', function(e){
    e.preventDefault();
    
    var results_url = $(e.target).data('results');
    var games_url = $(e.target).data('games');
    var groupNum = $(e.target).data('groupnum');
    console.dir(results_url, games_url, groupNum);
    if(!results_url || !games_url || !groupNum)
        return;
    
    TournamentChampionsleagueModel.loadGroupGamesAndResults(results_url, games_url);
    TournamentChampionsleagueModel.setCurrentGroup(groupNum);    
});

$('#matchesAccordion').on('show.bs.collapse', function (e) {
    if($('#'+$(e.target).attr('aria-labelledby')).data('winners_tab') != 1)
    {
        var stage = $('#'+$(e.target).attr('aria-labelledby')).data('stage');
        
        switch(stage)
        {
            case "QUALIFICATION":
                var results_url = $('#'+$(e.target).attr('aria-labelledby')).data('results');
                if(!results_url)
                    return;
                    
                TournamentChampionsleagueModel.loadQualificationResults(results_url);
            break;
            
            case "GROUP":
                var results_url = $('#'+$(e.target).attr('aria-labelledby')).data('results');
                var games_url = $('#'+$(e.target).attr('aria-labelledby')).data('games');
                
                var results_tpl = $('#'+$(e.target).attr('aria-labelledby')).data('resultstpl');
                var games_tpl = $('#'+$(e.target).attr('aria-labelledby')).data('gamestpl');
                
                if(!results_url || !games_url || !results_tpl || !games_tpl)
                    return;
                                
                if(TournamentChampionsleagueModel.getCurrentGroup() != null)
                {                    
                    results_url = results_tpl.replace('0', TournamentChampionsleagueModel.getCurrentGroup());
                    games_url = games_tpl.replace('0', TournamentChampionsleagueModel.getCurrentGroup());                    
                }
                
                TournamentChampionsleagueModel.loadGroupGamesAndResults(results_url, games_url);
            break;
            
            case "PLAYOFF":
                var games_url = $('#'+$(e.target).attr('aria-labelledby')).data('games');
                if(!games_url)
                    return;
                    
                TournamentChampionsleagueModel.loadPlayoffResults(games_url);
            break;
        }                
    }
    else
    {
        var winners_url = $('#'+$(e.target).attr('aria-labelledby')).data('winners_url');
        TournamentChampionsleagueModel.loadWinners(winners_url);
    }
});