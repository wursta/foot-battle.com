$('#matchesAccordion').on('show.bs.collapse', function (e) {
    if($('#'+$(e.target).attr('aria-labelledby')).data('winners_tab') != 1)
    {
        var results_url = $('#'+$(e.target).attr('aria-labelledby')).data('results_url');
        var games_url = $('#'+$(e.target).attr('aria-labelledby')).data('games_url');
        
        if(!results_url || !games_url)
            return;
        
        TournamentRound.loadGamesAndResultsByRound(results_url, games_url);
    }
    else
    {
        var winners_url = $('#'+$(e.target).attr('aria-labelledby')).data('winners_url');
        TournamentRound.loadWinners(winners_url);
    }
});