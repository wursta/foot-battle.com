WinPrizesModel.getContainer('button').on('click', function(e){
    e.preventDefault();
    
    var btn = $(this);
    
    url = btn.attr("href");
    
    if(btn.data("loading") == true)
        return false;
    
    Utils.showLoading();
    WinPrizesModel.load({
        url: btn.attr('href'),        
        success: function(content) {            
            btn.data("loading", false);            
            
            WinPrizesModel.show({
                target: btn, 
                html: content
            });
            
            Utils.hideLoading();
        },
        failure: function() {
            Utils.hideLoading();
        }
    });
});