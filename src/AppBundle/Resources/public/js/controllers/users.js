UsersModel.getContainer('site_container').on('click', '.userPopoverBtn', function(e){
    e.preventDefault();
        
    var btn = $(this);
    
    if(btn.data("loading") == true)
        return false;
        
    if(btn.data("rendered") == true)    
        return false;
        
    btn.data("loading", true);
    
    var placement = 'right';
    if(btn.data("popoverplacement") != undefined)
        placement = btn.data("popoverplacement");
    
    var title = '';
    if(btn.data('popovertitle') != undefined)
        title = btn.data('popovertitle');
    
    UsersModel.load({
        url: btn.attr('href'),        
        success: function(content) {
            
            btn.data("loading", false);
            btn.data("rendered", true);

            UsersModel.show({
                target: btn,
                placement: placement,
                title: title,                
                html: content
            });            
        },
        failure: function() {
            
        }
    });
});