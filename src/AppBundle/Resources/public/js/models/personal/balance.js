var Balance = {
    elementsIds : {
        cashoutBtn: '#cashoutBtn'        
    },    
    elements : {},
    getContainer : function(name) {
        var self = Balance;
        
        if(!self.elements[name])
            self.elements[name] = $(self.elementsIds[name]);
        
        return self.elements[name];
    },
    
    showCashoutModal: function(config) {
        var self = Balance;
        
        var btn = config.target;
        
        $.ajax({
            url: config.url,
            dataType: "html",
            success: function(data, textStatus, jqXHR) {
              
              var modal = Utils.createModal('cashoutModal', {
                  title: btn.data("modaltitle"),
                  body: data,
                  large: false,
                  buttons: [
                      {
                          text: TRANS.CLOSE                    
                      }
                  ]
              });
              
              $('#cashoutModal').on('hidden.bs.modal', function(e){
                  self.closeCashoutModal('#cashoutModal');
              });
              
              modal.modal('show'); 
              
              config.onOpen();
              
            },
            error: config.onLoadFailure
        });        
    },
    
    closeCashoutModal: function(btnId) {
        var self = Balance;
        $(btnId).remove();
    },
}