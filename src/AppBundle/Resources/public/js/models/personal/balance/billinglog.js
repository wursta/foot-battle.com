var BillingLog = {
    elementsIds : {
        container: '#billingLog'        
    },    
    elements : {},
    getContainer : function(name) {
        var self = BillingLog;
        
        if(!self.elements[name])
            self.elements[name] = $(self.elementsIds[name]);
        
        return self.elements[name];
    },
    
    load: function(url) {
        var self = BillingLog;
        
        Utils.showLoading();
        
        $.ajax({
            url: url,
            dataType: "html",
            success: function(data, textStatus, jqXHR) {
                self.getContainer('container').html(data);
                Utils.hideLoading();
            }
        });
    }
}