var CashoutQiwiModel = {
    elementsIds : {
        form: 'form[name="cashout_qiwi"]',
        qiwiNumberField: '#cashout_qiwi_qiwi_phone',
        amountField: '#cashout_qiwi_amount',
        cardImage: '#cardImage'
    },    
    elements : {},
    getContainer : function(name) {
        var self = CashoutQiwiModel;
        
        if(!self.elements[name])
            self.elements[name] = $(self.elementsIds[name]);
        
        return self.elements[name];
    },
    
    load: function(config){
        var self = CashoutQiwiModel;
        
        var url, successCallback, failureCallback;
        
        if(config.url == undefined)
            return null;
        
        url = config.url;
        
        if(config.success != undefined)
            successCallback = config.success;
        
        if(config.failure != undefined)
            failureCallback = config.failure;

        $.ajax({
            url: url,
            method: 'POST',
            dataType: "html",            
            data : config.postData,
            success: successCallback,
            error: failureCallback
        });
        
        return null;
    },
    
    show: function(config) {
        var self = CashoutQiwiModel;
                        
        var content = config.html;

        var modal = Utils.createModal('cashoutQiwiModal', {
            title: config.title,
            body: content,
            large: false,
            closeIcon: false,
            buttons: [
                {
                    text: TRANS.CONFIRM,
                    dismiss: false,
                    cls: 'btn-success',
                    handler: config.buttons.confirm.handler
                },
                {
                    text: TRANS.CANCEL,
                    cls: 'btn-danger',
                }
            ]
        });
        
        $('#cashoutQiwiModal').on('hidden.bs.modal', function(e){
            self.close('#cashoutQiwiModal');
        });
        
        modal.modal({
            backdrop: 'static'
        });
    },
    
    close: function(btnId) {
        var self = CashoutQiwiModel;
        $(btnId).remove();
    },
}