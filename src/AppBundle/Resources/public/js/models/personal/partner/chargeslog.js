var PartnerChargesLog = {
    elementsIds : {
        container: '#partnerChargesLog'        
    },    
    elements : {},
    getContainer : function(name) {
        var self = PartnerChargesLog;
        
        if(!self.elements[name])
            self.elements[name] = $(self.elementsIds[name]);
        
        return self.elements[name];
    },
    
    load: function(url) {
        var self = PartnerChargesLog;
        
        Utils.showLoading();
        
        $.ajax({
            url: url,
            dataType: "html",
            success: function(data, textStatus, jqXHR) {
                self.getContainer('container').html(data);
                Utils.hideLoading();
            }
        });
    }
}