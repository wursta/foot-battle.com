var RefillQiwiModel = {
    elementsIds : {
        form: 'form[name="refill_qiwi"]',
        qiwiPhoneField: '#refill_qiwi_qiwi_phone',
        qiwiAmountField: '#refill_qiwi_amount',        
    },    
    elements : {},
    getContainer : function(name) {
        var self = RefillQiwiModel;
        
        if(!self.elements[name])
            self.elements[name] = $(self.elementsIds[name]);
        
        return self.elements[name];
    },
    
    load: function(config){
        var self = RefillQiwiModel;
        
        var url, successCallback, failureCallback;
        
        if(config.url == undefined)
            return null;
        
        url = config.url;
        
        if(config.success != undefined)
            successCallback = config.success;
        
        if(config.failure != undefined)
            failureCallback = config.failure;

        $.ajax({
            url: url,
            method: 'POST',
            dataType: "html",            
            data : config.postData,
            success: successCallback,
            error: failureCallback
        });
        
        return null;
    },
    
    show: function(config) {
        var self = RefillQiwiModel;
                        
        var content = config.html;

        var modal = Utils.createModal('refillQiwiModal', {
            title: config.title,
            body: content,
            large: false,
            closeIcon: false,
            buttons: [
                {
                    text: TRANS.CONFIRM,
                    dismiss: false,
                    cls: 'btn-success',
                    handler: config.buttons.confirm.handler
                },
                {
                    text: TRANS.CANCEL,
                    cls: 'btn-danger',
                }
            ]
        });
        
        $('#refillQiwiModal').on('hidden.bs.modal', function(e){
            self.close('#refillQiwiModal');
        });
        
        modal.modal({
            backdrop: 'static'
        });
    },
    
    close: function(btnId) {
        var self = RefillQiwiModel;
        $(btnId).remove();
    },
}