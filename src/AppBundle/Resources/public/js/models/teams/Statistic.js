var StatisticModel = {
    elementsIds : {
        matches: '#tournamentMatches'
    },    
    elements : {},
    getContainer : function(name) {
        var self = StatisticModel;
        
        if(!self.elements[name])
            self.elements[name] = $(self.elementsIds[name]);
        
        return self.elements[name];
    },
    
    load: function(config) {
        var self = StatisticModel;
        
        var url, successCallback, failureCallback;
        
        if(config.url == undefined)
            return null;
        
        url = config.url;
        
        if(config.success != undefined)
            successCallback = config.success;
        
        if(config.failure != undefined)
            failureCallback = config.failure;
        
        $.ajax({
            url: url,
            dataType: "html",
            success: successCallback,
            error: failureCallback
        });
        
        return null;
    },
    
    show: function(config) {
        var self = StatisticModel;
        
        var btn = config.target;
        var content = config.html;

        btn.popover({
            trigger: 'click',
            placement: 'left',
            container: 'body',
            animation: false,
            html: true,
            template: '<div class="popover statistic-popover" role="tooltip"><h3 class="popover-title"></h3><div class="popover-content"></div><div class="popover-footer"><button type="button" class="btn btn-sm btn-default" onclick="return StatisticModel.close(\''+btn.attr("id")+'\')">'+TRANS.CLOSE+'</button></div></div>',
            content: content,
        });
        
        btn.popover('show');        
    },
    
    close: function(btnId) {
        var self = StatisticModel;
                
        $('#'+btnId).popover('destroy');
        $('#'+btnId).trigger('afterclose');
    },
}