var BetModel = {
    elementsIds : {
        matches: '#tournamentMatches'
    },    
    elements : {},
    getContainer : function(name) {
        var self = BetModel;
        
        if(!self.elements[name])
            self.elements[name] = $(self.elementsIds[name]);
        
        return self.elements[name];
    },
    
    save : function(homeTeamScore, guestTeamScore, config) {        
        var url, successCallback, failureCallback, data;
        
        if(config.url == undefined)
            return null;
            
        url = config.url;
        
        if(config.success != undefined)
            successCallback = config.success;
        
        if(config.failure != undefined)
            failureCallback = config.failure;
        
        data = {};
        if(config.params != undefined)        
            data = config.params;
        
        data.score_left = homeTeamScore;
        data.score_right = guestTeamScore;
        
        $.ajax({
            url: url,
            method: "POST",
            dataType: "json",
            data: data,
            success: successCallback,
            error: failureCallback
        });
        
        return null;
    }
}