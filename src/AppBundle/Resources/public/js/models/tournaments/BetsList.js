var BetsListModel = {
    elementsIds : {
        matches: '#tournamentMatches'
    },    
    elements : {},
    getContainer : function(name) {
        var self = BetsListModel;
        
        if(!self.elements[name])
            self.elements[name] = $(self.elementsIds[name]);
        
        return self.elements[name];
    },
    
     load: function(config) {
        var self = BetsListModel;
        
        var url, successCallback, failureCallback, data;
        
        if(config.url == undefined)
            return null;
        
        url = config.url;
        
        if(config.success != undefined)
            successCallback = config.success;
        
        if(config.failure != undefined)
            failureCallback = config.failure;
        
        data = {};
        if(config.params != undefined)        
            data = config.params;        
            
        $.ajax({
            url: url,
            dataType: "html",
            method: "POST",
            data: data,
            success: successCallback,
            error: failureCallback
        });
        
        return null;
    },
    
    show: function(config) {
        var self = BetsListModel;
        
        var btn = config.target;
        var content = config.html;

        btn.popover({
            trigger: 'click',
            placement: 'left',
            container: 'body',
            animation: false,
            html: true,
            template: '<div class="popover" role="tooltip"><h3 class="popover-title"></h3><div class="popover-content"></div><div class="popover-footer"><button type="button" class="btn btn-sm btn-default" onclick="return BetsListModel.close(\''+btn.attr("id")+'\')">'+TRANS.CLOSE+'</button></div></div>',
            content: content,
        });
        
        btn.popover('show');        
    },
    
    close: function(btnId) {
        var self = BetsListModel;
                
        $('#'+btnId).popover('destroy');
        $('#'+btnId).trigger('afterclose');
    },
}