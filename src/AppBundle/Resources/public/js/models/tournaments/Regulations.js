var RegulationsModel = {
    elementsIds : {
        button: '#regulationsBtn'
    },    
    elements : {},
    getContainer : function(name) {
        var self = RegulationsModel;
        
        if(!self.elements[name])
            self.elements[name] = $(self.elementsIds[name]);
        
        return self.elements[name];
    },
    
    load: function(config){
        var self = RegulationsModel;
        
        var url, successCallback, failureCallback;
        
        if(config.url == undefined)
            return null;
        
        url = config.url;
        
        if(config.success != undefined)
            successCallback = config.success;
        
        if(config.failure != undefined)
            failureCallback = config.failure;
        
        $.ajax({
            url: url,
            dataType: "html",
            success: successCallback,
            error: failureCallback
        });
        
        return null;
    },
    
    show: function(config) {
        var self = RegulationsModel;
        
        var btn = config.target;
        var content = config.html;

        var modal = Utils.createModal('regulationModal', {
            title: btn.data("modaltitle"),
            body: content,
            large: true,
            buttons: [
                {
                    text: TRANS.CLOSE                    
                }
            ]
        });
        
        $('#regulationModal').on('hidden.bs.modal', function(e){
            self.close('#regulationModal');
        });
        
        modal.modal('show');
    },
    
    close: function(btnId) {
        var self = RegulationsModel;
        $(btnId).remove();
    },
}