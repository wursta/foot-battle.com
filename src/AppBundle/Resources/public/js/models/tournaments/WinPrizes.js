var WinPrizesModel = {
    elementsIds : {
        button: '#winprizesBtn'
    },    
    elements : {},
    getContainer : function(name) {
        var self = WinPrizesModel;
        
        if(!self.elements[name])
            self.elements[name] = $(self.elementsIds[name]);
        
        return self.elements[name];
    },
    
    load: function(config){
        var self = WinPrizesModel;
        
        var url, successCallback, failureCallback;
        
        if(config.url == undefined)
            return null;
        
        url = config.url;
        
        if(config.success != undefined)
            successCallback = config.success;
        
        if(config.failure != undefined)
            failureCallback = config.failure;
        
        $.ajax({
            url: url,
            dataType: "html",
            success: successCallback,
            error: failureCallback
        });
        
        return null;
    },
    
    show: function(config) {
        var self = WinPrizesModel;
        
        var btn = config.target;
        var content = config.html;

        var modal = Utils.createModal('winprizesModal', {
            title: btn.data("modaltitle"),
            body: content,            
            buttons: [
                {
                    text: TRANS.CLOSE                    
                }
            ]
        });
        
        $('#winprizesModal').on('hidden.bs.modal', function(e){
            self.close('#winprizesModal');
        });
        
        modal.modal('show');
    },
    
    close: function(btnId) {
        var self = WinPrizesModel;
        $(btnId).remove();
    },
}