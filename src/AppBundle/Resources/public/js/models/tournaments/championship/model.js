var TournamentChampionship = {
    elementsIds : {
        main: '#tournamentMain',        
    },    
    elements : {},
    getContainer : function(name) {
        var self = TournamentChampionship;
        
        if(!self.elements[name])
            self.elements[name] = $(self.elementsIds[name]);
        
        return self.elements[name];
    },
    
    loadResultsByRound : function(results_url) {
        var self = TournamentChampionship;
        
        Utils.showLoading();
        
        $.ajax({
            url: results_url,
            dataType: "html",
            success: function(data, textStatus, jqXHR) {
                self.getContainer('main').html(data);                
            },
            complete: function(jqXHR, textStatus) {                
                Utils.hideLoading();
            }
        });
    },
    
    loadWinners : function(winners_url) {
        var self = TournamentChampionship;
        
        Utils.showLoading();
        
        $.ajax({
            url: winners_url,
            dataType: "html",
            success: function(data, textStatus, jqXHR) {                
                self.getContainer('main').html(data);                
            },
            complete: function(jqXHR, textStatus) {                
                Utils.hideLoading();
            }
        });
        
    }
}