var TournamentChampionsleagueModel = {
    elementsIds : {
        main: '#tournamentMain',
        sidebar: '#tournamentSidebar'        
    },
    elements : {},
    getContainer : function(name) {
        var self = TournamentChampionsleagueModel;
        
        if(!self.elements[name])
            self.elements[name] = $(self.elementsIds[name]);
        
        return self.elements[name];
    },
    
    currentGroup: null,
    setCurrentGroup : function(groupNum) {
        var self = TournamentChampionsleagueModel;
        var group = parseInt(groupNum);
        if(group == 0)
            group = 1;
        
        self.currentGroup = group;
    },
    
    getCurrentGroup : function() {
        var self = TournamentChampionsleagueModel;                
        
        return self.currentGroup;
    },
    
    loadQualificationResults : function(results_url) {
        var self = TournamentChampionsleagueModel;
        
        Utils.showLoading();
        
        $.ajax({
            url: results_url,
            dataType: "html",
            success: function(data, textStatus, jqXHR) {
                self.getContainer('main').html(data);
                self.getContainer('sidebar').empty();
            },            
            complete: function(jqXHR, textStatus) {                
                Utils.hideLoading();
            }
        });
    },
    
    loadPlayoffResults : function(games_url) {
        var self = TournamentChampionsleagueModel;
        
        Utils.showLoading();
        
        $.ajax({
            url: games_url,
            dataType: "html",
            success: function(data, textStatus, jqXHR) {
                self.getContainer('main').html(data);
                self.getContainer('sidebar').empty();
            },            
            complete: function(jqXHR, textStatus) {                
                Utils.hideLoading();
            }
        });
    },
    
    loadGroupGamesAndResults : function(results_url, games_url) {
        var self = TournamentChampionsleagueModel;
        
        Utils.showLoading();
        
        $.ajax({
            url: results_url,
            dataType: "html",
            success: function(data, textStatus, jqXHR) {
                self.getContainer('sidebar').html(data);                
            },            
            complete: function(jqXHR, textStatus) {                
                Utils.hideLoading();
            }
        });
        
        $.ajax({
            url: games_url,
            dataType: "html",
            success: function(data, textStatus, jqXHR) {
                self.getContainer('main').html(data);             
            },
            complete: function(jqXHR, textStatus) {                
                Utils.hideLoading();
            }
        });
    },
    
    loadWinners : function(winners_url) {
        var self = TournamentChampionsleagueModel;
        
        Utils.showLoading();
        
        $.ajax({
            url: winners_url,
            dataType: "html",
            success: function(data, textStatus, jqXHR) {                
                self.getContainer('main').html(data);
                self.getContainer('sidebar').empty();
            },
            complete: function(jqXHR, textStatus) {                
                Utils.hideLoading();
            }
        });
        
    }
}