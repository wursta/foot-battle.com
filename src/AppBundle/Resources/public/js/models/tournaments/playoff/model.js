var TournamentPlayoff = {
    elementsIds : {
        main: '#tournamentMain',        
    },
    elements : {},
    getContainer : function(name) {
        var self = TournamentPlayoff;
        
        if(!self.elements[name])
            self.elements[name] = $(self.elementsIds[name]);
        
        return self.elements[name];
    },
    
    loadQualificationResults : function(results_url) {
        var self = TournamentPlayoff;
        
        Utils.showLoading();
        
        $.ajax({
            url: results_url,
            dataType: "html",
            success: function(data, textStatus, jqXHR) {
                self.getContainer('main').html(data);                
            },            
            complete: function(jqXHR, textStatus) {                
                Utils.hideLoading();
            }
        });
    },
    
    loadPlayoffResults : function(games_url) {
        var self = TournamentPlayoff;
        
        Utils.showLoading();
        
        $.ajax({
            url: games_url,
            dataType: "html",
            success: function(data, textStatus, jqXHR) {
                self.getContainer('main').html(data);                
            },            
            complete: function(jqXHR, textStatus) {                
                Utils.hideLoading();
            }
        });
    },    
    
    loadWinners : function(winners_url) {
        var self = TournamentPlayoff;
        
        Utils.showLoading();
        
        $.ajax({
            url: winners_url,
            dataType: "html",
            success: function(data, textStatus, jqXHR) {                
                self.getContainer('main').html(data);                
            },
            complete: function(jqXHR, textStatus) {                
                Utils.hideLoading();
            }
        });
        
    }
}