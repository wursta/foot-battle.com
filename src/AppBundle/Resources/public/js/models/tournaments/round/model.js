var TournamentRound = {
    elementsIds : {
        main: '#tournamentMain',
        sidebar: '#tournamentSidebar'        
    },    
    elements : {},
    getContainer : function(name) {
        var self = TournamentRound;
        
        if(!self.elements[name])
            self.elements[name] = $(self.elementsIds[name]);
        
        return self.elements[name];
    },
    
    loadGamesAndResultsByRound : function(results_url, games_url) {
        var self = TournamentRound;
        
        Utils.showLoading();
        
        $.ajax({
            url: results_url,
            dataType: "html",
            success: function(data, textStatus, jqXHR) {
                self.getContainer('sidebar').html(data);              
            },            
            complete: function(jqXHR, textStatus) {                
                Utils.hideLoading();
            }
        });                
        
        $.ajax({
            url: games_url,
            dataType: "html",
            success: function(data, textStatus, jqXHR) {
                self.getContainer('main').html(data);             
            },
            complete: function(jqXHR, textStatus) {                
                Utils.hideLoading();
            }
        });
    },
    
    loadWinners : function(winners_url) {
        var self = TournamentRound;
        
        Utils.showLoading();
        
        $.ajax({
            url: winners_url,
            dataType: "html",
            success: function(data, textStatus, jqXHR) {
                self.getContainer('sidebar').empty();
                self.getContainer('main').html(data);                
            },
            complete: function(jqXHR, textStatus) {                
                Utils.hideLoading();
            }
        });
        
    }
}