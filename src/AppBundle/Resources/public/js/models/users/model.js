var UsersModel = {
    elementsIds : {
        site_container: '#siteContainer'
    },    
    elements : {},
    getContainer : function(name) {
        var self = UsersModel;
        
        if(!self.elements[name])
            self.elements[name] = $(self.elementsIds[name]);
        
        return self.elements[name];
    },
    
    load: function(config) {
        var self = UsersModel;
        
        var url, successCallback, failureCallback;
        
        if(config.url == undefined)
            return null;
        
        url = config.url;
        
        if(config.success != undefined)
            successCallback = config.success;
        
        if(config.failure != undefined)
            failureCallback = config.failure;
        
        $.ajax({
            url: url,
            dataType: "html",
            success: successCallback,
            error: failureCallback
        });
        
        return null;
    },
    
    show: function(config) {
        var self = UsersModel;
        
        var btn = config.target;
        var title = config.title;
        var placement = config.placement;
        var content = config.html;

        btn.popover({
            trigger: 'click',
            placement: placement,
            container: 'body',
            animation: false,
            html: true,
            trigger: 'focus',            
            template: '<div class="popover userInfoPopover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>',
            title: title,
            content: content,
        });
        
        btn.popover('show');        
    },
    
    close: function(btnId) {
        var self = UsersModel;        
        var btn = $('#'+btnId);
                
        btn.popover('destroy');
        btn.trigger('afterclose');
    },
}