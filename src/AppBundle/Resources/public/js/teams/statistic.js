$('#tournamentMatches .matchStatistic').click(function(){
    return showStatistic(this);
});

function showStatistic(aElement)
{
    if($(aElement).data("loading") == true)
        return false;
        
    if($(aElement).data("rendered") == true)    
        return false;
        
    
    $(aElement).data("loading", true);
    $(aElement).removeClass('glyphicon-stats').addClass('glyphicon-refresh glyphicon-refresh-animate');
    $.ajax({
        url: $(aElement).attr("href"),
        dataType: "html",                
        success: function(data, textStatus, jqXHR) {            
            
            $(aElement).popover({
                trigger: 'click',
                placement: 'left',
                container: 'body',
                animation: false,
                html: true,
                template: '<div class="popover statistic-popover" role="tooltip"><h3 class="popover-title"></h3><div class="popover-content"></div><div class="popover-footer"><button type="button" class="btn btn-sm btn-default" onclick="return hideStatistic(\'#'+$(aElement).attr("id")+'\')">'+TRANS.CLOSE+'</button></div></div>',
                content: data,
            });                        
            
            $(aElement).popover('show');
            
            $(aElement).data("loading", false);
            $(aElement).data("rendered", true);
            $(aElement).addClass('glyphicon-stats').removeClass('glyphicon-refresh glyphicon-refresh-animate');            
        }
    });        
    
    return false;
}

function hideStatistic(aElementId)
{
    $(aElementId).popover('destroy');
    $(aElementId).data("rendered", false);
    return false;
}