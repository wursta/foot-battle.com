$('#matchesAccordion').on('show.bs.collapse', function (e) {
    if($('#'+$(e.target).attr('aria-labelledby')).data('winners_tab') != 1)
    {
        var stage = $('#'+$(e.target).attr('aria-labelledby')).data('stage');
        
        switch(stage)
        {
            case "QUALIFICATION":
                var results_url = $('#'+$(e.target).attr('aria-labelledby')).data('results');
                if(!results_url)
                    return;
                    
                TournamentPlayoff.loadQualificationResults(results_url);
            break;            
            
            case "PLAYOFF":
                var games_url = $('#'+$(e.target).attr('aria-labelledby')).data('games');
                if(!games_url)
                    return;
                    
                TournamentPlayoff.loadPlayoffResults(games_url);
            break;
        }                
    }
    else
    {
        var winners_url = $('#'+$(e.target).attr('aria-labelledby')).data('winners_url');
        TournamentPlayoff.loadWinners(winners_url);
    }
});