function showAdditionalInfo(aElement)
{
    if($(aElement).data("loading") == true)
        return false;
        
    if($(aElement).data("rendered") == true)    
        return false;
        
    
    $(aElement).data("loading", true);
    $(aElement).find('span').removeClass('glyphicon-info-sign').addClass('glyphicon-refresh glyphicon-refresh-animate');    
    $.ajax({
        url: $(aElement).attr("href"),
        dataType: "html",                
        success: function(data, textStatus, jqXHR) {            
            
            $(aElement).popover({
                trigger: 'click',
                placement: 'left',
                container: 'body',
                animation: false,
                html: true,
                template: '<div class="popover" role="tooltip"><h3 class="popover-title"></h3><div class="popover-content"></div><div class="popover-footer"><button type="button" class="btn btn-sm btn-default" onclick="return hideAdditionalInfo(\'#'+$(aElement).attr("id")+'\')">'+TRANS.CLOSE+'</button></div></div>',
                content: data,
            });                        
            
            $(aElement).popover('show');
            
            $(aElement).data("loading", false);
            $(aElement).data("rendered", true);
            $(aElement).find('span').addClass('glyphicon-info-sign').removeClass('glyphicon-refresh glyphicon-refresh-animate');            
        }
    });        
    
    return false;
}

function hideAdditionalInfo(aElementId)
{
    $(aElementId).popover('destroy');
    $(aElementId).data("rendered", false);
    return false;
}