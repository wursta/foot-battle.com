function showWinPrizePopover(aElement)
{
    if($(aElement).data("loading") == true)
        return false;
        
    if($(aElement).data("rendered") == true)    
        return false;
        
    
    $(aElement).data("loading", true);
    
    $.ajax({
        url: $(aElement).attr("href"),
        dataType: "html",
        success: function(data, textStatus, jqXHR) {            
            
            $(aElement).popover({
                trigger: 'click',
                placement: 'bottom',
                container: 'body',
                animation: false,
                html: true,
                template: '<div class="popover" role="tooltip"><h3 class="popover-title"></h3><div class="popover-content"></div><div class="popover-footer"><button type="button" class="btn btn-sm btn-default" onclick="return hideWinPrizePopover(\'#'+$(aElement).attr("id")+'\')">'+TRANS.CLOSE+'</button></div></div>',
                content: data,
            });                        
            
            $(aElement).popover('show');
            
            $(aElement).data("loading", false);
            $(aElement).data("rendered", true);            
        }
    });        
    
    return false;
}

function hideWinPrizePopover(aElementId)
{
    $(aElementId).popover('destroy');
    $(aElementId).data("rendered", false);
    return false;
}