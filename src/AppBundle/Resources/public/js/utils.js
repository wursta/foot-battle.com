var Utils = {
    elementsIds : {
        siteContainer: '#siteContainer'
    },    
    elements : {},
    getContainer : function(name) {
        var self = Utils;
        
        if(!self.elements[name])
            self.elements[name] = $(self.elementsIds[name]);
        
        return self.elements[name];
    },    
    showLoading: function(){
        var self = Utils;
        if($('#small_loading_container').length == 0)
            $('<div id="small_loading_container" class="label label-default"><img src="/img/ajax-loader.gif" /></div>').appendTo(self.elementsIds.siteContainer);
    },
    
    hideLoading: function(){        
        if($('#small_loading_container').length > 0)
            $('#small_loading_container').remove();
    },
    
    createModal: function(id, config){
        var self = Utils;
        
        var modalHtml = '';
        
        modalHtml += '<div id="'+id+'" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="'+id+'Label">';
            modalHtml += '<div class="modal-dialog'+ ((config.large != undefined) ? ' modal-lg' : '')+'">';
                modalHtml += '<div class="modal-content">';
                    if(config.title !== false)
                    {
                        modalHtml += '<div class="modal-header">';
                            if(config.closeIcon == undefined || config.closeIcon == true)
                            {
                                modalHtml += '<button type="button" class="close" data-dismiss="modal" aria-label="'+TRANS.CLOSE+'"><span aria-hidden="true">&times;</span></button>';
                            }
                            modalHtml += '<h4 class="modal-title" id="'+id+'Label">'+config.title+'</h4>';
                        modalHtml += '</div>';
                    }
                    
                    modalHtml += '<div class="modal-body">';
                        modalHtml += config.body;
                    modalHtml += '</div>';
                    modalHtml += '<div class="modal-footer">';
                        $(config.buttons).each(function(index, btnConfig){
                            var dismiss = ' data-dismiss="modal"';
                            if(btnConfig.dismiss == false)
                                dismiss = '';
                            
                            var cls = ' btn-default';
                            if(btnConfig.cls != undefined)
                                cls = ' '+btnConfig.cls;
                            
                            modalHtml += '<button type="button" class="btn'+cls+'"'+dismiss+'>'+btnConfig.text+'</button>';
                        });
                    modalHtml += '</div>';
                modalHtml += '</div>';
            modalHtml += '</div>';
        modalHtml += '</div>';
        
        var modal = $(modalHtml).appendTo(self.getContainer('siteContainer'));
        
        var modalFooter = $(modal).find('.modal-footer');
        $(config.buttons).each(function(index, btnConfig){
            if(btnConfig.handler == undefined)
                return true;
            
            modalFooter.find('button').eq(index).click(function(){
                return btnConfig.handler(this);
            });
        });
        
        return modal;
    }    
}