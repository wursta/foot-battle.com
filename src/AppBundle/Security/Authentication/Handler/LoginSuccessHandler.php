<?php

namespace AppBundle\Security\Authentication\Handler;

use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Router;

class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface
{

    protected $router;
    protected $security;

    public function __construct(Router $router, SecurityContext $security)
    {
        $this->router = $router;
        $this->security = $security;
    }

    /**
    * Обработчик события успешной авторизации
    *
    * @param Request $request
    * @param TokenInterface $token
    *
    * @return JsonResponse
    */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $locale = $token->getUser()->getLocale();

        $response = new JsonResponse();

        $response->setData(array(
            "success" => true
        ));

        return $response;
    }

}