<?php
namespace AppBundle\Services;

use AppBundle\Entity\BillingOperation;
use AppBundle\Entity\TransactionLog;
use AppBundle\Event\BillEvent;
use AppBundle\BillEvents;

class BalanceHelper
{
    private $em;

    /**
    * @var \AppBundle\Entity\UserRepository
    */
    private $userRep;

    /**
    * @var \AppBundle\Entity\BillingOperationRepository
    */
    private $billingOperationRep;

    /**
    * @var \AppBundle\Services\PartnerProgramHelper
    */
    private $partnerProgramHelper;

    /**
    * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
    */
    private $eventDispatcher;

    private $currentUser;

    /**
    * ID системного пользователя
    */
    const SYSTEM_USER_ID = 1;

    /**
    * Обменный курс золотых монет
    */
    const MONEY_RATE = 1;

    /**
    * Действие "Зачисление"
    */
    const ACTION_DEPOSIT = 'DEPOSIT';

    /**
    * Действие "Снятие"
    */
    const ACTION_WITHDRAW = 'WITHDRAW';

    /**
    * Действие "Перевод"
    */
    const ACTION_TRANSFER = 'TRANSFER';

    /**
    * Действие "Резервация"
    */
    const ACTION_RESERVATION = 'RESERVATION';

    /**
    * Действие "Снятие из резерва"
    */
    const ACTION_RESERVATION_OFF = 'RESERVATION_OFF';

    /**
    * Действие "Возврат зарезервированных средств"
    */
    const ACTION_RESERVATION_RETURN = 'RESERVATION_RETURN';

    /**
    * Действие "Зачисление выигрыша"
    */
    const ACTION_CHARGE_WIN = 'CHARGE_WIN';

    /**
    * Действие "Зачисление выигрыша в промо-турнире"
    */
    const ACTION_CHARGE_PROMO_WIN = 'CHARGE_PROMO_WIN';

    /**
    * Действие "Реферальное вознаграждение"
    */
    const ACTION_REFERRAL_CHARGE = 'REFERRAL_CHARGE';

    /**
    * Действие "Вывод"
    */
    const ACTION_CASHOUT = 'CASHOUT';

    /**
    * Тип счёта - Заявка
    */
    const TYPE_REQUEST = 'REQUEST';

    /**
    * Тип счёта - QIWI-кошелёк
    */
    const TYPE_QIWI = 'QIWI';

    const PAYSYSTEM_INTERNAL = 'INTERNAL';
    const PAYSYSTEM_DOL = 'DOL';

    const CURRENCY_RUB = 'RUB';
    const CURRENT_EUR = 'EUR';

    /**
    * Статус "Отменён"
    */
    const STATUS_CANCELED = -1;

    /**
    * Статус "В процессе"
    */
    const STATUS_IN_PROGRESS = 0;

    /**
    * Статус "Выполнен"
    */
    const STATUS_OK = 1;

    /**
    * Коммисия за перевод внутри системы, %
    */
    const COMMISSION_TRANSFER = 0;

    /**
    * Комиссия за ручной вывод средств, %
    */
    const COMMISSION_CASHOUT_MANUAL_REQUEST = 3;

    /**
    * Минимальное количество золотых монет для обмена по заявке
    */
    const MIN_CASHOUT_MANUAL_REQUEST = 15;

    /**
    * Минимальное количество монет при котором будет зачислено реферальное вознаграждение
    */
    const MIN_DEPOSIT_FOR_PARTNER_CHARGE = 2;

    /**
    * Реферальное вознаграждение за первый депозит - кол-во золотых монет
    */
    const REFERRAL_CHARGE_FIRST_DEPOSIT = 1;

    /**
    * Шаблон комментария для счёта зачисления/вывода через киви
    */
    const BILLING_OPERATION_NOTE_REFILL_QIWI = 'Покупка золотых монет ({MONEY_AMOUNT}) через QIWI-кошелёк ({QIWI_PHONE})';
    const BILLING_OPERATION_NOTE_CASHOUT_QIWI = 'Обмен золотых монет ({MONEY_AMOUNT}) и вывод на QIWI-кошелёк ({QIWI_PHONE})';

    /**
    * Шаблон комментария для счёта вывода по заявке на банковскую карту
    */
    const BILLING_OPERATION_NOTE_CASHOUT_REQUEST = 'Обмен золотых монет ({MONEY_AMOUNT}) и вывод на банковскую карту по заявке';

    /**
    * Шаблон комментария для счёта резервирования платы для участия в турнире
    */
    const BILLING_OPERATION_NOTE_TOURNAMENT_FEE_RESERV = 'Резервирование золотых монет ({MONEY_AMOUNT}) за участие в турнире {TOURNAMENT_TITLE}[{TOURNAMENT_ID}]';

    const BILLING_OPERATION_NOTE_TOURNAMENT_FEE_RESERV_RETURN = 'Возврат зарезервированных средств ({MONEY_AMOUNT}) в связи с отменой турнира {TOURNAMENT_TITLE}[{TOURNAMENT_ID}]';

    /**
    * Шаблон комментария для счёта снятия из резерва за вход в турнир для пользователя
    */
    const BILLING_OPERATION_NOTE_TOURNAMENT_FEE_OFF = 'Снятие золотых монет ({MONEY_AMOUNT}) из резерва за участие в турнире {TOURNAMENT_TITLE}[{TOURNAMENT_ID}]';

    /**
    * Шаблон комментария для счёта зачисления выигрыша в турнире
    */
    const BILLING_OPERATION_NOTE_TOURNAMENT_WIN = 'Зачисление выигрыша ({MONEY_AMOUNT}) за призовое место в турнире {TOURNAMENT_TITLE}[{TOURNAMENT_ID}]';

    /**
    * Шаблон комментария для счёта зачисления выигрыша в промо-турнире
    */
    const BILLING_OPERATION_NOTE_PROMO_TOURNAMENT_WIN = 'Зачисление выигрыша ({MONEY_AMOUNT}) за призовое место в промо-турнире {TOURNAMENT_TITLE}[{TOURNAMENT_ID}] на игровой счёт';

    /**
    * Шаблон комментария для счёта зачисления реферального вознаграждения
    */
    const BILLING_OPERATION_NOTE_REFERRAL_CHARGE = 'Зачисление реферального вознаграждения ({MONEY_AMOUNT}) за первый депозит более 2 ЗМ от приглашённого пользователя';

    const BILLING_OPERATION_NOTE_CASHOUT_CREDIT_CARD = 'Выплата на банковскую карту.';

    //const TOURNAMENT_FEE_USER_MSG = 'Снятие из резерва за вход в турнир {TOURNAMENT_TITLE}[{TOURNAMENT_ID}]';
    //const TOURNAMENT_FEE_SYS_MSG = 'Плата за вход в турнир {TOURNAMENT_TITLE}[{TOURNAMENT_ID}] от пользователя {USER_NICK}[{USER_ID}]';
    const TOURNAMENT_FEE_RESERV_USER_MSG = 'Резервирование суммы взноса за участие в турнире {TOURNAMENT_TITLE}[{TOURNAMENT_ID}]';
    const TOURNAMENT_CHARGE_WIN_SYS_MSG = 'Списание призовых на счёт пользователя {USER_NICK}[{USER_ID}] в турнире {TOURNAMENT_TITLE}[{TOURNAMENT_ID}]';
    const TOURNAMENT_CHARGE_WIN_USER_MSG = 'Зачисление призовых в турнире {TOURNAMENT_TITLE}[{TOURNAMENT_ID}]';
    const TOURNAMENT_COMMISSION_SYS_MSG = 'Коммиссия системы за турнир {TOURNAMENT_TITLE}[{TOURNAMENT_ID}]';
    const TOURNAMENT_RETURN_FEE_USER_MSG = 'Возврат взноса за участие в отменённом турнире {TOURNAMENT_TITLE}[{TOURNAMENT_ID}]';

    public function __construct(\Doctrine\ORM\EntityManager $em, \AppBundle\Services\PartnerProgramHelper $partnerProgramHelper, \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher)
    {
        $this->em = $em;
        $this->partnerProgramHelper = $partnerProgramHelper;
        $this->eventDispatcher = $eventDispatcher;
        $this->userRep = $this->em->getRepository('AppBundle\Entity\User');
        $this->billingOperationRep = $this->em->getRepository('AppBundle\Entity\BillingOperation');
    }

    /**
    * Возвращает пользователя системы
    *
    * @return \AppBundle\Entity\User
    */
    public function getSystemUser()
    {
        $systemUser = $this->userRep->find(self::SYSTEM_USER_ID);

        if(!$systemUser)
            return null;

        return $systemUser;
    }

    public function isSystem(\AppBundle\Entity\User $user)
    {
        if($user->getId() == self::SYSTEM_USER_ID)
            return true;

        return false;
    }

    /**
    * Получает баланс системы
    *
    * @return float
    */
    public function getSystemBalance()
    {
        $systemUser = $this->getSystemUser();

        if(!$systemUser)
            return 0;

        return $systemUser->getBalance();
    }

    /**
    * Возвращает текущую валюту в которой совершаются операции со средствами
    * @return string
    */
    public function getCurrent()
    {
        return self::CURRENT_EUR;
    }

    /**
    * Возвращает баланс пользователя
    *
    * @param \AppBundle\Entity\User $user
    * @return float
    */
    public function getUserBalance(\AppBundle\Entity\User $user)
    {
        return $user->getBalance();
    }

    /**
    * Генерирует уникальный "хэш" операции
    *
    * @param int $user_id
    * @return string
    */
    public function generateOperationId($user_id)
    {
        return md5(uniqid().$user_id.time().microtime());
    }

    /**
    * Создаёт счёт для зачисления средств на счёт системы
    *
    * @param \AppBundle\Entity\User $userFrom Пользователь "От"
    * @param float $money Количество золотых монет
    * @param string|null $note Комментарий
    * @param array $options Дополнительные поля (IP с которого происходит действие, строка User-Agent)
    * @example $options [
    *   user_ip,
    *   user_agent
    * ]
    * @return \AppBundle\Entity\BillingOperation
    */
    public function createSystemDepositBill(\AppBundle\Entity\User $userFrom, $money, $note = null, $options = array())
    {
        $amount = $money/self::MONEY_RATE;
        $realCommission = $this->getCommissionForAmount($amount, 0);
        $amountWithCommission = $amount - $realCommission;

        $BillingOperation = new BillingOperation();
        $BillingOperation->setHash($this->generateOperationId($userFrom->getId()));
        $BillingOperation->setAction(self::ACTION_DEPOSIT);
        $BillingOperation->setAmount($amount);
        $BillingOperation->setAmountWithCommision($amountWithCommission);
        $BillingOperation->setCommissionPercent(0);
        $BillingOperation->setCurrency($this->getCurrent());
        $BillingOperation->setMoneyRate(self::MONEY_RATE);
        $BillingOperation->setNote($note);
        $BillingOperation->setOperationDatetime(new \DateTime());
        $BillingOperation->setPaysystem(self::PAYSYSTEM_INTERNAL);
        $BillingOperation->setStatus(self::STATUS_IN_PROGRESS);
        $BillingOperation->setUser($userFrom);
        $BillingOperation->setBalanceInfo($this->getBalanceInfo($userFrom));

        if(!empty($options["user_ip"]))
            $BillingOperation->setUserIp($options["user_ip"]);

        if(!empty($options["user_agent"]))
            $BillingOperation->setUserAgent($options["user_agent"]);

        $this->em->persist($BillingOperation);
        $this->em->flush($BillingOperation);

        return $BillingOperation;
    }

    /**
    * Создаёт счёт для первода средств от одного пользователя - другому
    *
    * @param \AppBundle\Entity\User $userFrom Пользователь "От"
    * @param \AppBundle\Entity\User $userRecipient Пользователь "Кому"
    * @param float $money Количество золотых монет
    * @param string|null $note Комментарий
    * @param array $options Дополнительные поля (IP с которого происходит действие, строка User-Agent)
    * @example $options [
    *   user_ip,
    *   user_agent
    * ]
    * @return \AppBundle\Entity\BillingOperation
    */
    public function createTransferBill(\AppBundle\Entity\User $userFrom, \AppBundle\Entity\User $userRecipient, $money, $note = null, $options = array())
    {
        $amount = $money/self::MONEY_RATE;
        $realCommission = $this->getCommissionForAmount($amount, self::COMMISSION_TRANSFER);
        $amountWithCommission = $amount - $realCommission;

        $BillingOperation = new BillingOperation();
        $BillingOperation->setHash($this->generateOperationId($userFrom->getId()));
        $BillingOperation->setAction(self::ACTION_TRANSFER);
        $BillingOperation->setAmount($amount);
        $BillingOperation->setAmountWithCommision($amountWithCommission);
        $BillingOperation->setCommissionPercent(self::COMMISSION_TRANSFER);
        $BillingOperation->setCurrency($this->getCurrent());
        $BillingOperation->setMoneyRate(self::MONEY_RATE);
        $BillingOperation->setNote($note);
        $BillingOperation->setOperationDatetime(new \DateTime());
        $BillingOperation->setPaysystem(self::PAYSYSTEM_INTERNAL);
        $BillingOperation->setStatus(self::STATUS_IN_PROGRESS);
        $BillingOperation->setUser($userFrom);
        $BillingOperation->setUserRecipient($userRecipient);
        $BillingOperation->setBalanceInfo($this->getBalanceInfo($userFrom, $userRecipient));


        if(!empty($options["user_ip"]))
            $BillingOperation->setUserIp($options["user_ip"]);

        if(!empty($options["user_agent"]))
            $BillingOperation->setUserAgent($options["user_agent"]);

        $this->em->persist($BillingOperation);
        $this->em->flush($BillingOperation);

        return $BillingOperation;
    }

    /**
    * Создаёт счёт для вывода средств из системы
    *
    * @param \AppBundle\Entity\User $userFrom Пользователь совершающий вывод средств
    * @param float $money Количество золотых монет
    * @param float $commissionPercent Процент коммиссии за операцию
    * @param string|null $note Комментарий
    * @param array $options Дополнительные поля (IP с которого происходит действие, строка User-Agent)
    * @example $options [
    *   operation_id
    *   type,
    *   paysystem
    *   user_ip,
    *   user_agent,
    * ]
    * @return \AppBundle\Entity\BillingOperation
    */
    public function createCashoutBill(\AppBundle\Entity\User $userFrom, $money, $commissionPercent, $note = null, $options = array())
    {
        $amount = $money/self::MONEY_RATE;
        $realCommission = $this->getCommissionForAmount($amount, $commissionPercent);
        $amountWithCommission = $amount - $realCommission;

        $BillingOperation = new BillingOperation();
        if(!empty($options["operation_id"]))
            $BillingOperation->setHash($options["operation_id"]);
        else
            $BillingOperation->setHash($this->generateOperationId($userFrom->getId()));
        $BillingOperation->setAction(self::ACTION_CASHOUT);
        $BillingOperation->setAmount($amount);
        $BillingOperation->setAmountWithCommision($amountWithCommission);
        $BillingOperation->setCommissionPercent($commissionPercent);
        $BillingOperation->setCurrency($this->getCurrent());
        $BillingOperation->setMoneyRate(self::MONEY_RATE);
        $BillingOperation->setNote($note);
        $BillingOperation->setOperationDatetime(new \DateTime());
        $BillingOperation->setStatus(self::STATUS_IN_PROGRESS);
        $BillingOperation->setUser($userFrom);
        $BillingOperation->setBalanceInfo($this->getBalanceInfo($userFrom));

        if(!empty($options["paysystem"]))
            $BillingOperation->setPaysystem($options["paysystem"]);
        else
            $BillingOperation->setPaysystem(self::PAYSYSTEM_INTERNAL);

        if(!empty($options["type"]))
            $BillingOperation->setType($options["type"]);

        if(!empty($options["user_ip"]))
            $BillingOperation->setUserIp($options["user_ip"]);

        if(!empty($options["user_agent"]))
            $BillingOperation->setUserAgent($options["user_agent"]);

        $this->em->persist($BillingOperation);
        $this->em->flush($BillingOperation);

        return $BillingOperation;
    }

    /**
    * Создаёт счёт для резервирования суммы за участие в турнире
    *
    * @param \AppBundle\Entity\User $userFrom
    * @param float $money Кол-во золотых монет
    * @param string $note Комментарий к счёту
    * @return \AppBundle\Entity\BillingOperation
    */
    public function createTournamentReserveFeeBill(\AppBundle\Entity\User $userFrom, $money, $note = null)
    {
        $amount = $money/self::MONEY_RATE;

        $BillingOperation = new BillingOperation();
        $BillingOperation->setHash($this->generateOperationId($userFrom->getId()));
        $BillingOperation->setAction(self::ACTION_RESERVATION);
        $BillingOperation->setAmount($amount);
        $BillingOperation->setAmountWithCommision($amount);
        $BillingOperation->setCommissionPercent(0);
        $BillingOperation->setCurrency($this->getCurrent());
        $BillingOperation->setMoneyRate(self::MONEY_RATE);
        $BillingOperation->setNote($note);
        $BillingOperation->setOperationDatetime(new \DateTime());
        $BillingOperation->setPaysystem(self::PAYSYSTEM_INTERNAL);
        $BillingOperation->setStatus(self::STATUS_IN_PROGRESS);
        $BillingOperation->setUser($userFrom);
        $BillingOperation->setBalanceInfo($this->getBalanceInfo($userFrom));

        $this->em->persist($BillingOperation);
        $this->em->flush($BillingOperation);

        return $BillingOperation;
    }

    /**
    * Создаёт счёт для снятия коммиссии за вход с участника турнира из резерва
    *
    * @param \AppBundle\Entity\User $userFrom
    * @param float $money Кол-во золотых монет
    * @param string $note Комментарий к счёту
    * @return \AppBundle\Entity\BillingOperation
    */
    public function createTournamentFeeOffBill(\AppBundle\Entity\User $userFrom, $money, $note = null)
    {
        $amount = $money/self::MONEY_RATE;
        $realCommission = $this->getCommissionForAmount($amount, 0);
        $amountWithCommission = $amount - $realCommission;

        $BillingOperation = new BillingOperation();
        $BillingOperation->setHash($this->generateOperationId($userFrom->getId()));
        $BillingOperation->setAction(self::ACTION_RESERVATION_OFF);
        $BillingOperation->setAmount($amount);
        $BillingOperation->setAmountWithCommision($amountWithCommission);
        $BillingOperation->setCommissionPercent(0);
        $BillingOperation->setCurrency($this->getCurrent());
        $BillingOperation->setMoneyRate(self::MONEY_RATE);
        $BillingOperation->setNote($note);
        $BillingOperation->setOperationDatetime(new \DateTime());
        $BillingOperation->setPaysystem(self::PAYSYSTEM_INTERNAL);
        $BillingOperation->setStatus(self::STATUS_IN_PROGRESS);
        $BillingOperation->setUser($userFrom);
        $BillingOperation->setUserRecipient($this->getSystemUser());
        $BillingOperation->setBalanceInfo($this->getBalanceInfo($userFrom, $this->getSystemUser()));

        $this->em->persist($BillingOperation);
        $this->em->flush($BillingOperation);

        return $BillingOperation;
    }

    public function createTournamentFeeReturnBill(\AppBundle\Entity\User $userFrom, $money, $note = null)
    {
        $amount = $money/self::MONEY_RATE;
        $realCommission = $this->getCommissionForAmount($amount, 0);
        $amountWithCommission = $amount - $realCommission;

        $BillingOperation = new BillingOperation();
        $BillingOperation->setHash($this->generateOperationId($userFrom->getId()));
        $BillingOperation->setAction(self::ACTION_RESERVATION_RETURN);
        $BillingOperation->setAmount($amount);
        $BillingOperation->setAmountWithCommision($amountWithCommission);
        $BillingOperation->setCommissionPercent(0);
        $BillingOperation->setCurrency($this->getCurrent());
        $BillingOperation->setMoneyRate(self::MONEY_RATE);
        $BillingOperation->setNote($note);
        $BillingOperation->setOperationDatetime(new \DateTime());
        $BillingOperation->setPaysystem(self::PAYSYSTEM_INTERNAL);
        $BillingOperation->setStatus(self::STATUS_IN_PROGRESS);
        $BillingOperation->setUser($userFrom);
        $BillingOperation->setBalanceInfo($this->getBalanceInfo($userFrom));

        $this->em->persist($BillingOperation);
        $this->em->flush($BillingOperation);

        return $BillingOperation;
    }

    /**
    * Создаёт счёт для зачисление выигрыша на счёт пользователя
    *
    * @param \AppBundle\Entity\User $userRecipient
    * @param float $money Кол-во золотых монет
    * @param string $note Комментарий к счёту
    * @return \AppBundle\Entity\BillingOperation
    */
    public function createTournamentWinBill(\AppBundle\Entity\User $userRecipient, $money, $note = null)
    {
        $amount = $money/self::MONEY_RATE;
        $realCommission = $this->getCommissionForAmount($amount, 0);
        $amountWithCommission = $amount - $realCommission;

        $BillingOperation = new BillingOperation();
        $BillingOperation->setHash($this->generateOperationId($this->getSystemUser()->getId()));
        $BillingOperation->setAction(self::ACTION_CHARGE_WIN);
        $BillingOperation->setAmount($amount);
        $BillingOperation->setAmountWithCommision($amountWithCommission);
        $BillingOperation->setCommissionPercent(0);
        $BillingOperation->setCurrency($this->getCurrent());
        $BillingOperation->setMoneyRate(self::MONEY_RATE);
        $BillingOperation->setNote($note);
        $BillingOperation->setOperationDatetime(new \DateTime());
        $BillingOperation->setPaysystem(self::PAYSYSTEM_INTERNAL);
        $BillingOperation->setStatus(self::STATUS_IN_PROGRESS);
        $BillingOperation->setUser($this->getSystemUser());
        $BillingOperation->setUserRecipient($userRecipient);
        $BillingOperation->setBalanceInfo($this->getBalanceInfo($this->getSystemUser(), $userRecipient));

        $this->em->persist($BillingOperation);
        $this->em->flush($BillingOperation);

        return $BillingOperation;
    }

    /**
    * Создаёт счёт для зачисление выигрыша на игровой счёт пользователя
    *
    * @param \AppBundle\Entity\User $userRecipient
    * @param float $money Кол-во золотых монет
    * @param string $note Комментарий к счёту
    * @return \AppBundle\Entity\BillingOperation
    */
    public function createPromoTournamentWinBill(\AppBundle\Entity\User $userRecipient, $money, $note = null)
    {
        $amount = $money/self::MONEY_RATE;
        $realCommission = $this->getCommissionForAmount($amount, 0);
        $amountWithCommission = $amount - $realCommission;

        $BillingOperation = new BillingOperation();
        $BillingOperation->setHash($this->generateOperationId($this->getSystemUser()->getId()));
        $BillingOperation->setAction(self::ACTION_CHARGE_PROMO_WIN);
        $BillingOperation->setAmount($amount);
        $BillingOperation->setAmountWithCommision($amountWithCommission);
        $BillingOperation->setCommissionPercent(0);
        $BillingOperation->setCurrency($this->getCurrent());
        $BillingOperation->setMoneyRate(self::MONEY_RATE);
        $BillingOperation->setNote($note);
        $BillingOperation->setOperationDatetime(new \DateTime());
        $BillingOperation->setPaysystem(self::PAYSYSTEM_INTERNAL);
        $BillingOperation->setStatus(self::STATUS_IN_PROGRESS);
        $BillingOperation->setUser($this->getSystemUser());
        $BillingOperation->setUserRecipient($userRecipient);
        $BillingOperation->setBalanceInfo($this->getBalanceInfo($this->getSystemUser(), $userRecipient));

        $this->em->persist($BillingOperation);
        $this->em->flush($BillingOperation);

        return $BillingOperation;
    }

    /**
    * Создаёт счёт для зачисления реферального вознаграждения пригласившему пользователю
    *
    * @param \AppBundle\Entity\User $userRecipient Получатель реферального вознаграждения
    * @param float $money Кол-во золотых монет
    * @param string $note Комментарий к счёту
    * @return \AppBundle\Entity\BillingOperation
    */
    public function createReferralChargeBill(\AppBundle\Entity\User $userRecipient, $money, $note = null)
    {
        $amount = $money/self::MONEY_RATE;
        $realCommission = $this->getCommissionForAmount($amount, 0);
        $amountWithCommission = $amount - $realCommission;

        $BillingOperation = new BillingOperation();
        $BillingOperation->setHash($this->generateOperationId($this->getSystemUser()->getId()));
        $BillingOperation->setAction(self::ACTION_REFERRAL_CHARGE);
        $BillingOperation->setAmount($amount);
        $BillingOperation->setAmountWithCommision($amountWithCommission);
        $BillingOperation->setCommissionPercent(0);
        $BillingOperation->setCurrency($this->getCurrent());
        $BillingOperation->setMoneyRate(self::MONEY_RATE);
        $BillingOperation->setNote($note);
        $BillingOperation->setOperationDatetime(new \DateTime());
        $BillingOperation->setPaysystem(self::PAYSYSTEM_INTERNAL);
        $BillingOperation->setStatus(self::STATUS_IN_PROGRESS);
        $BillingOperation->setUser($this->getSystemUser());
        $BillingOperation->setUserRecipient($userRecipient);
        $BillingOperation->setBalanceInfo($this->getBalanceInfo($this->getSystemUser(), $userRecipient));

        $this->em->persist($BillingOperation);
        $this->em->flush($BillingOperation);

        return $BillingOperation;
    }

    /**
    * Производит действия связанные с балансом пользователя на основе переданного счёта
    * Зачисляет средства на баланс пользователья, Снимает средства с баланса,
    * Начисляет выигрыш в турнирах, Резервирует средства при входе в турнир
    *
    * @param \AppBundle\Entity\BillingOperation $bill
    * @return bool
    */
    public function processBill(\AppBundle\Entity\BillingOperation $bill)
    {
        $money = $this->getMoneyFromAmount($bill->getAmountWithCommision(), $bill->getMoneyRate());

        switch($bill->getAction())
        {
            case self::ACTION_DEPOSIT:
            {
                $bill->getUser()->setBalance($bill->getUser()->getBalance() + $money);
                $this->em->persist($bill->getUser());
            }
            break;

            case self::ACTION_TRANSFER:
            {
                $bill->getUser()->setBalance($bill->getUser()->getBalance() - $money);

                if($bill->getUser()->getFreeFunds() > $bill->getUser()->getBalance())
                {
                    $bill->getUser()->setFreeFunds($bill->getUser()->getBalance());
                }

                $bill->getUserRecipient()->setBalance($bill->getUserRecipient()->getBalance() + $money);

                $this->em->persist($bill->getUser());
                $this->em->persist($bill->getUserRecipient());
            }
            break;

            case self::ACTION_CASHOUT:
            {
                //Снимать со счёта нужно сумму без комиссии
                $money = $this->getMoneyFromAmount($bill->getAmount(), $bill->getMoneyRate());

                $bill->getUser()->setReserved($bill->getUser()->getReserved() - $money);
                $this->em->persist($bill->getUser());
            }
            break;

            case self::ACTION_RESERVATION:
            {
                $bill->getUser()->setBalance($bill->getUser()->getBalance() - $money);

                if($bill->getUser()->getFreeFunds() > $bill->getUser()->getBalance())
                {
                    $bill->getUser()->setFreeFunds($bill->getUser()->getBalance());
                }

                $bill->getUser()->setReserved($bill->getUser()->getReserved() + $money);
                $this->em->persist($bill->getUser());
            }
            break;

            case self::ACTION_RESERVATION_OFF:
            {
                $bill->getUser()->setReserved($bill->getUser()->getReserved() - $money);
                $this->em->persist($bill->getUser());

                $bill->getUserRecipient()->setBalance($bill->getUserRecipient()->getBalance() + $money);
                $this->em->persist($bill->getUserRecipient());
            }
            break;

            case self::ACTION_RESERVATION_RETURN:
            {
                $bill->getUser()->setReserved($bill->getUser()->getReserved() - $money);
                $bill->getUser()->setBalance($bill->getUser()->getBalance() + $money);
                $this->em->persist($bill->getUser());
            }
            break;

            case self::ACTION_CHARGE_WIN:
            {
                $bill->getUser()->setBalance($bill->getUser()->getBalance() - $money);
                $this->em->persist($bill->getUser());

                $bill->getUserRecipient()->setBalance($bill->getUserRecipient()->getBalance() + $money);
                $bill->getUserRecipient()->setFreeFunds($bill->getUserRecipient()->getFreeFunds() + $money);
                $this->em->persist($bill->getUserRecipient());
            }
            break;

            case self::ACTION_CHARGE_PROMO_WIN:
            {
                $bill->getUser()->setBalance($bill->getUser()->getBalance() - $money);
                $this->em->persist($bill->getUser());

                $bill->getUserRecipient()->setBalance($bill->getUserRecipient()->getBalance() + $money);
                $this->em->persist($bill->getUserRecipient());
            }
            break;

            case self::ACTION_REFERRAL_CHARGE:
            {
                $bill->getUser()->setBalance($bill->getUser()->getBalance() - $money);
                $this->em->persist($bill->getUser());

                $bill->getUserRecipient()->setBalance($bill->getUserRecipient()->getBalance() + $money);

                if($bill->getUserRecipient()->getPartnerLevel() > \AppBundle\Services\UserHelper::PARTNER_LEVEL_1)
                $bill->getUserRecipient()->setFreeFunds($bill->getUserRecipient()->getFreeFunds() + $money);

                $this->em->persist($bill->getUserRecipient());
            }
            break;
        }

        $bill->setStatus(self::STATUS_OK);
        $bill->setOperationOkDatetime(new \DateTime());
        $this->em->persist($bill);
        $this->em->flush();

        //Инициируем события
        switch($bill->getAction())
        {
            case self::ACTION_DEPOSIT:
            {
                $event = new BillEvent($bill);
                $this->eventDispatcher->dispatch(BillEvents::DEPOSIT, $event);
            }
            break;
        }

        return true;
    }

    /**
    * Отменяет счёт и проивзодит соответсвующие манипуляции со счётом пользователя
    *
    * @param \AppBundle\Entity\BillingOperation $bill
    * @return bool
    */
    public function cancelBill(\AppBundle\Entity\BillingOperation $bill)
    {
        $money = $this->getMoneyFromAmount($bill->getAmount(), $bill->getMoneyRate());

        switch($bill->getAction())
        {
            case self::ACTION_CASHOUT:
            {
                $user = $bill->getUser();
                $user->setReserved($bill->getUser()->getReserved() - $money);
                $user->setBalance($bill->getUser()->getBalance() + $money);
                $user->setFreeFunds($bill->getUser()->getFreeFunds() + $money);

                $this->em->persist($user);
            }
            break;
        }

        $bill->setStatus(self::STATUS_CANCELED);
        $bill->setOperationCancelDatetime(new \DateTime());
        $this->em->persist($bill);
        $this->em->flush();
    }

    /**
    * Резервирует золотые монеты пользователя перед выводом средств до момента подтверждения операции биллинга
    *
    * @param \AppBundle\Entity\User $user
    * @param float $amount
    */
    public function reservMoneyForCashout(\AppBundle\Entity\User $user, $amount)
    {
        //Если это система, то резервировать средства на счёте для вывода не нужно.
        //Так как вывод с системы происходит только после перевода средств от друого пользователя, то
        //при переводе средства на счёт системы зачисляются только на основной баланс и не зачисляются на счёт для вывода
        //Поэтому снимать со счёта для вывода не нужно.
        if(!$this->isSystem($user)) {
            $user->setFreeFunds($user->getFreeFunds() - $amount);
        }
        $user->setBalance($user->getBalance() - $amount);
        $user->setReserved($user->getReserved() + $amount);

        $this->em->persist($user);
        $this->em->flush($user);
    }

    /**
    * Проверяет возможность зачисления реферального вознаграждения за депозит
    * Вознаграждение выплачивается только в случае, если депозит составляет более 2 ЗМ и он является первым депозитом игрока
    *
    * @param \AppBundle\Entity\BillingOperation $bill
    * @return bool
    */
    public function needReferralRewardForDeposit($bill)
    {
        $referralUser = $bill->getUser()->getReferralUser();
        if(!($referralUser instanceof \AppBundle\Entity\User))
            return false;

        $money = $this->getMoneyFromAmount($bill->getAmount(), $bill->getMoneyRate());

        if($money >= self::MIN_DEPOSIT_FOR_PARTNER_CHARGE && $this->billingOperationRep->getDepositCountByUser($bill->getUser()) == 1)
            return true;

        return false;
    }

    /**
    * Зачисляет реферальное вознаграждение пользователю за первый депозит
    *
    * @param \AppBundle\Entity\User $user
    * @param \AppBundle\Entity\User $referralUser
    *
    */
    public function chargeFirstDepositReferralReward($user, $referralUser)
    {
        $amount = self::REFERRAL_CHARGE_FIRST_DEPOSIT;

        $bill = $this->createReferralChargeBill($user, $amount, $this->getReferralChargeNote($amount));
        $this->processBill($bill);

        $this->partnerProgramHelper->addCharge($user, $referralUser, \AppBundle\Services\PartnerProgramHelper::TYPE_FIRST_DEPOSIT, $amount);
    }

    /**
    * Возвращает округлённую сумму.
    *
    * @param float $amount Сумма.
    *
    * @return float
    */
    public function getAmount($amount)
    {
        return round(floatval($amount), 2);
    }

    /**
    * Рассчитывает реальную сумму коммиссии
    * Округялет её до 2х знаков после запятой
    *
    * @param float $amount Сумма
    * @param float $commissionPercent Процент коммиссии
    * @return float
    */
    public function getCommissionForAmount($amount, $commissionPercent)
    {
        return round(($amount * $commissionPercent)/100, 2);
    }

    /**
    * Возвращает количество денег во внутренней валюте по реальным деньгам
    *
    * @param float $amount Сумма в у.е.
    * @param float $moneyRate Курс внутренней валюты. Если не передан, то берётся текущий курс
    */
    public function getMoneyFromAmount($amount, $moneyRate = null)
    {
        if(!$moneyRate)
            $moneyRate = self::MONEY_RATE;

        return round(floatval($amount)/floatval($moneyRate), 2);
    }

    /**
    * Проверка, что вывод средств не осуществлялся в текущие сутки
    * Вывод доступен раз в сутки.
    *
    * @param \AppBundle\Entity\User $user
    * @param \DateTime $datetime
    */
    public function firstCashoutAtDay(\AppBundle\Entity\User $user, \DateTime $datetime)
    {
        return (bool) !$this->billingOperationRep->getCashoutOperationsCount($user->getId(), $datetime);
    }

    /**
    * Проверка возможнсти обмена переданного количества монет на деньги переданным пользователем
    *
    * @param \AppBundle\Entity\User $user
    * @param float $amount Кол-во монет для обмена
    * @param float $moneyRate Курс одной монеты
    */
    public function cashoutIsAvailable(\AppBundle\Entity\User $user, $amount, $moneyRate)
    {
        if(($user->getFreeFunds()*$moneyRate - $amount) < 0)
            return false;

        return true;
    }

    /**
    * Формирование комментария к счёту выставленному на снятие суммы из резерва за участие в турнире
    *
    * @param \AppBundle\Entity\Tournament $tournament
    * @param float $moneyAmount
    */
    public function getFeeOffNote($tournament, $moneyAmount)
    {
        $data = array("{TOURNAMENT_TITLE}" => $tournament->getTitle(),
            "{TOURNAMENT_ID}" => $tournament->getId(),
            "{MONEY_AMOUNT}" => $moneyAmount,
        );

        return str_replace(array_keys($data), array_values($data), self::BILLING_OPERATION_NOTE_TOURNAMENT_FEE_OFF);
    }

    /**
    * Формирование комментария к счёту выставленному на зачисление выигрыша участнику турнира
    *
    * @param \AppBundle\Entity\Tournament $tournament
    * @param float $moneyAmount
    */
    public function getWinNote($tournament, $moneyAmount)
    {
        $data = array("{TOURNAMENT_TITLE}" => $tournament->getTitle(),
            "{TOURNAMENT_ID}" => $tournament->getId(),
            "{MONEY_AMOUNT}" => $moneyAmount,
        );

        return str_replace(array_keys($data), array_values($data), self::BILLING_OPERATION_NOTE_TOURNAMENT_WIN);
    }

    /**
    * Формирование комментария к счёту выставленному на зачисление выигрыша участнику промо-турнира
    *
    * @param \AppBundle\Entity\Tournament $tournament
    * @param float $moneyAmount
    */
    public function getPromoWinNote($tournament, $moneyAmount)
    {
        $data = array("{TOURNAMENT_TITLE}" => $tournament->getTitle(),
            "{TOURNAMENT_ID}" => $tournament->getId(),
            "{MONEY_AMOUNT}" => $moneyAmount,
        );

        return str_replace(array_keys($data), array_values($data), self::BILLING_OPERATION_NOTE_PROMO_TOURNAMENT_WIN);
    }

    /**
    * Формирование комментария к счёту выставленному на резервирование
    *
    * @param \AppBundle\Entity\Tournament $tournament
    * @param float $moneyAmount
    */
    public function getReservFeeNote($tournament, $moneyAmount)
    {
        $data = array("{TOURNAMENT_TITLE}" => $tournament->getTitle(),
            "{TOURNAMENT_ID}" => $tournament->getId(),
            "{MONEY_AMOUNT}" => $moneyAmount,
        );

        return str_replace(array_keys($data), array_values($data), self::BILLING_OPERATION_NOTE_TOURNAMENT_FEE_RESERV);
    }

    /**
    * Формирование комментария к счёту выставленному на возврат зарезервированных средств
    *
    * @param \AppBundle\Entity\Tournament $tournament
    * @param float $moneyAmount
    */
    public function getReservFeeReturnNote($tournament, $moneyAmount)
    {
        $data = array("{TOURNAMENT_TITLE}" => $tournament->getTitle(),
            "{TOURNAMENT_ID}" => $tournament->getId(),
            "{MONEY_AMOUNT}" => $moneyAmount,
        );

        return str_replace(array_keys($data), array_values($data), self::BILLING_OPERATION_NOTE_TOURNAMENT_FEE_RESERV_RETURN);
    }

    /**
    * Формирование комментария к счёту выставленному на пополнение QIWI кошелька
    *
    * @param string $qiwiPhone Номер QIWI-кошелька
    * @param float $moneyAmount Количество золотых монет
    */
    public function getQIWIOperationNote($qiwiPhone, $moneyAmount)
    {
        $data = array("{QIWI_PHONE}" => substr($qiwiPhone, 0, strlen($qiwiPhone) - 4).str_repeat("#", 4),
            "{MONEY_AMOUNT}" => $moneyAmount,
        );

        return str_replace(array_keys($data), array_values($data), self::BILLING_OPERATION_NOTE_REFILL_QIWI);
    }

    /**
    * Формирование комментария к счёту
    *
    * @param string $qiwiPhone Номер QIWI-кошелька
    * @param float $moneyAmount Количество золотых монет
    */
    public function getQIWICashoutNote($qiwiPhone, $moneyAmount)
    {
        $data = array("{QIWI_PHONE}" => substr($qiwiPhone, 0, strlen($qiwiPhone) - 4).str_repeat("#", 4),
            "{MONEY_AMOUNT}" => $moneyAmount,
        );

        return str_replace(array_keys($data), array_values($data), self::BILLING_OPERATION_NOTE_CASHOUT_QIWI);
    }

    public function getReferralChargeNote($moneyAmount)
    {
        $data = array("{MONEY_AMOUNT}" => $moneyAmount);

        return str_replace(array_keys($data), array_values($data), self::BILLING_OPERATION_NOTE_REFERRAL_CHARGE);
    }

    public function getRequestCashoutNote($moneyAmount)
    {
        $data = array("{MONEY_AMOUNT}" => $moneyAmount);

        return str_replace(array_keys($data), array_values($data), self::BILLING_OPERATION_NOTE_CASHOUT_REQUEST);
    }

    /**
    * Возвращает информацию о балансах на счетах пользователей (отправителя и получателя) в сериализованном виде.
    *
    * @param \AppBundle\Entity\User $userFrom      Пользователь инициировавший операцию.
    * @param \AppBundle\Entity\User $userRecipient Пользоватль получатель перевода.
    *
    * @return string Сериализованная строка
    */
    private function getBalanceInfo(\AppBundle\Entity\User $userFrom, \AppBundle\Entity\User $userRecipient = null)
    {
        $info = array(
            "userFrom" => array(
                "balance" => $userFrom->getBalance(),
                "reserved" => $userFrom->getReserved(),
                "free_funds" => $userFrom->getFreeFunds()
            )
        );

        if($userRecipient)
        {
            $info["userRecipient"] = array(
                "balance" => $userRecipient->getBalance(),
                "reserved" => $userFrom->getReserved(),
                "free_funds" => $userRecipient->getFreeFunds()
            );
        }

        return $info;
    }

    # Handlers

    /**
    * Обработчк события зачисления средств
    *
    * @param \AppBundle\Event\BillEvent $event
    */
    public function onBillDeposit(\AppBundle\Event\BillEvent $event)
    {
        $bill = $event->getBill();

        if($this->needReferralRewardForDeposit($bill))
        {
            $this->chargeFirstDepositReferralReward($bill->getUser()->getReferralUser(), $bill->getUser());
        }
    }
}