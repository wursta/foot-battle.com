<?php
namespace AppBundle\Services;

use Doctrine\ORM\EntityManager;

class BetsHelper
{
    /**    
    * @var \AppBundle\Entity\BetRepository
    */
    private $repository;

    public function __construct(EntityManager $em)
    {
        $this->repository = $em->getRepository('AppBundle\Entity\Bet');
    }
    
    /**
    * Перестраивает массив из одномерного (из базы) в многомерный по матчам
    * Вид:
    * array(
    *   match_id => array(
    *     user_id => AppBundle\Entity\Bet
    *   )
    * )
    * 
    * @param \AppBundle\Entity\Bet[] $bets
    * @return array
    */
    public function rebuildByMatches($bets)
    {
      $rebuildedBets = array();
      
      foreach($bets as $bet)
      {
        $rebuildedBets[$bet->getMatch()->getId()][$bet->getUser()->getId()] = $bet;        
      }
      
      return $rebuildedBets;
    }
}