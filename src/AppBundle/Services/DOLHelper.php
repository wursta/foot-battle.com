<?php
namespace AppBundle\Services;

use \Httpful\Request;
class DOLHelper
{
    private $translator;
    private $refillUrl;
    private $gsgApiUrl;

    /**
    * ID валюты "Рубля"
    */
    const CURRENCY_RUB = 'RUB';

    /**
    * ID валюты "Евро"
    */
    const CURRENCY_EUR = 'EUR';

    /**
    * ID и secure key проекта для зачисления средств
    */
    const PROJECT_ID = 9643;
    const SECURE_KEY = 'wxCGtSLD7vn7OhUroWu2O3_tTnCTfPde';

    /**
    * ID и secure key проекта для вывода средств
    */
    const PROJECT_GSG_ID = 10693;
    const SECURE_GSG_KEY = '5XtBhi5o1eakOeUoqHIN2NDcCcpg_k8C';

    const MODE_TYPE_QIWI = 664;

    /**
    *  ID системы-получателя выплат в системе GSG
    */
    const GSG_PAYSYSTEM_QIWI_ID = 2;

    /**
    * Процент коммиссии взимаемый при вводе средств через QIWI
    */
    const COMMISSION_QIWI_PERCENT = 0;

    /**
    * Минимальная сумма доступная для зачисления через QIWI
    */
    const MIN_DEPOSIT_QIWI = 0;

    /**
    * Процент коммиссии взимаемый при выводе средств на карту
    */
    const GSG_COMMISSION_QIWI_PERCENT = 15;

    /**
    * Минимальная сумма доступная для вывода на карту
    */
    const GSG_QIWI_MIN_CASHOUT = 0.15;

    /**
    * Максимальная сумма доступная для вывода на карту
    */
    const GSG_QIWI_MAX_CASHOUT = 200;

    const STATUS_UNDEFINED = -1;
    const STATUS_NEW = 0;
    const STATUS_OK = 1;
    const STATUS_IN_PROGRESS = 2;
    const STATUS_POSTPONED = 3;
    const STATUS_MANUAL_VERIFICATION = 4;
    const STATUS_PAYMENT_ERROR = 6;
    const STATUS_BAD_REQUEST = 12;
    const STATUS_NO_PROJECT = 14;
    const STATUS_NOT_ALLOWED = 15;
    const STATUS_NOT_ENOUGH_MONEY = 16;
    const STATUS_BAD_ACTION = 17;
    const STATUS_BAD_PAYSYSTEM = 18;
    const STATUS_BAD_ACCOUNT = 19;
    const STATUS_BAD_PARAM = 20;
    const STATUS_BAD_CURRENCY = 21;
    const STATUS_BAD_INVOICE = 22;
    const STATUS_PS_ERROR = 23;
    const STATUS_DUPLICATE_PAYMENT = 24;
    const STATUS_DUPLICATE_TXN = 25;
    const STATUS_BAD_AMOUNT = 26;
    const STATUS_AMOUNT_TOO_SMALL = 27;
    const STATUS_AMOUNT_TOO_BIG = 28;
    const STATUS_BAD_TXN_ID = 29;
    const STATUS_EMPTY_SIGNATURE = 30;
    const STATUS_WRONG_SIGNATURE = 31;
    const STATUS_EMPTY_REQUEST = 32;
    const STATUS_BAD_EXT_AUTH = 34;
    const STATUS_DISABLE_BALANCE_TRANSFER = 35;
    const STATUS_CURRENCY_NOT_ALLOWED = 36;
    const STATUS_WRONG_EXPIRATION_DATE = 97;
    const STATUS_WRONG_CARDHOLDER_NAME = 98;
    const STATUS_CANCELED = 99;
    const STATUS_PS_CHECK_FAILED = 100;
    const STATUS_BAD_NUMBER_RANGE = 101;
    const STATUS_BAD_CARD_NUMBER = 102;
    const STATUS_BAD_LIMITS = 103;
    const STATUS_WM_WALLET_NOT_FOUND = 104;
    const STATUS_INVALID_EMAIL = 108;
    const STATUS_INVALID_PHONE = 109;
    const STATUS_PS_PAY_FAILED = 200;
    const STATUS_PAYMENT_NOT_ALLOWED = 201;
    const STATUS_ACCOUNT_BLOCKED = 202;
    const STATUS_LIMITS_EXCEEDED = 203;
    const STATUS_PS_NOT_CONFIGURED = 204;
    const STATUS_UNAVAILABLE_FOR_REGION = 205;
    const STATUS_INCOMPLETE_PERSONAL_INFO = 991;
    const STATUS_INVOICE_LIFETIME_EXPIRED = 993;
    const STATUS_SECRET_KEY_EXPIRED = 994;
    const STATUS_IP_NOT_WHITELISTED = 995;
    const STATUS_DEPRECATED_ACTION = 996;
    const STATUS_PS_UNAVAILABLE = 997;
    const STATUS_KEY_NOT_FOUND = 998;
    const STATUS_FORBIDDEN = 999;
    const STATUS_INTERNAL_ERROR = 1000;


    public function __construct(\Doctrine\ORM\EntityManager $em, $translator)
    {
        $this->translator = $translator;
        $this->refillUrl = "https://www.onlinedengi.ru/wmpaycheck.php";
        $this->gsgApiUrl = "https://gsg.dengionline.com/api/".self::PROJECT_GSG_ID."/";
    }

    public function getDefaultCurrency()
    {
        return self::CURRENCY_EUR;
    }

    public function getRefillUrl()
    {
        return $this->refillUrl;
    }

    /**
    * Совершает запрос к ДОЛ и возвращает текущий баланс по договору GSG
    * @return float
    */
    public function getGSGBalance()
    {
        $payload = $this->getGSGPayloadXML(array("action" => "balance", "params" => array("currency" => self::getDefaultCurrency())));
        $request = Request::post($this->gsgApiUrl, $payload)->contentType("xml")->expectsType("xml");

        $response = $request->send();

        $status = (string) $response->body->status;

        if($status != self::STATUS_OK)
        {
            throw new \Exception($this->getStatusMessage($status), $status);
            return false;
        }

        if(!isset($response->body->balance))
        {
            throw new \Exception($this->getStatusMessage(self::STATUS_UNDEFINED), self::STATUS_UNDEFINED);
            return false;
        }

        $balance = (float) $response->body->balance;

        return $balance;
    }

    /**
    * Совершает запрос в ДОЛ для проверки возможности проведения платежа
    *
    * @param string $account ID аккаунта в системе получателя платежа (сервис-провайдера), например, номер карты
    * @param float $amount сумма выплаты
    * @param string $operationId ID транзакции во внешней системе (системе проекта)
    */
    public function gsgCheck($account, $amount, $operationId)
    {
        $params = array(
            "currency" => self::getDefaultCurrency(),
            "paysystem" => self::GSG_PAYSYSTEM_QIWI_ID,
            "account" => $account,
            "amount" => $amount,
            "txn_id" => $operationId
        );
        $payload = $this->getGSGPayloadXML(array("action" => "check", "params" => $params));

        $request = Request::post($this->gsgApiUrl, $payload)->contentType("xml")->expectsType("xml");
        $response = $request->send();

        $status = (string) $response->body->status;

        if($status != self::STATUS_OK)
        {
            throw new \Exception($this->getStatusMessage($status), $status);
            return false;
        }

        $result = array(
            "reference" => (string) $response->body->reference,
            "timestamp" => (string) $response->body->timestamp,
            "invoice" => (string) $response->body->invoice,
            "income" => (string) $response->body->income,
            "amount" => (string) $response->body->amount,
            "outcome" => (string) $response->body->outcome,
            "rate" => array(
                "income" => (string) $response->body->rate["income"],
                "outcome" => (string) $response->body->rate["outcome"],
                "total" => (string) $response->body->rate["total"]
            )
        );

        return $result;
    }

    public function gsgPay($invoiceId, $operationId, $amount, $note = "")
    {
        $params = array(
            "invoice" => $invoiceId,
            "txn_id" => $operationId,
            "amount" => $amount,
            "currency" => self::getDefaultCurrency(),
            //"comment" => $note
        );
        $payload = $this->getGSGPayloadXML(array("action" => "pay", "params" => $params));
        $request = Request::post($this->gsgApiUrl, $payload)->contentType("xml")->expectsType("xml");
        $response = $request->send();

        $status = (string) $response->body->status;

        if(!in_array($status, array(self::STATUS_OK,
                                    self::STATUS_IN_PROGRESS,
                                    self::STATUS_POSTPONED,
                                    self::STATUS_MANUAL_VERIFICATION
                                   )
                     )
           )
        {
            throw new \Exception($this->getStatusMessage($status), $status);
            return false;
        }

        $result = array(
            "status" => (string) $response->body->status,
            "reference" => (string) $response->body->reference,
            "timestamp" => (string) $response->body->timestamp,
            "invoice" => (string) $response->body->invoice,
            "income" => (string) $response->body->income,
            "rate" => (string) $response->body->rate,
            "amount" => (string) $response->body->amount,
            "outcome" => (string) $response->body->outcome,
            "fee" => (string) $response->body->fee
        );

        return $result;
    }

    /**
    * Выставление счёта в ДОЛ на пополнение через QIWI-кошелёк
    *
    * @param \AppBundle\Entity\BillingOperation $billingOperation
    * @return array|false
    */
    public function createBillForQIWI(\AppBundle\Entity\BillingOperation $billingOperation, $qiwiPhone)
    {
        $params["project"] = self::PROJECT_ID;
        $params["mode_type"] = 'mobilePayment';
        $params["order_id"] = $billingOperation->getHash();
        $params["amount"] = $billingOperation->getAmountWithCommision();
        $params["nickname"] = $billingOperation->getUser()->getId();
        $params["comment"] = $billingOperation->getNote();
        $params["nick_extra"] = $billingOperation->getUser()->getEmail();
        $params["paymentCurrency"] = self::CURRENCY_EUR;
        $params["sendQIWIPayment"] = 1;
        $params["qiwi_phone"] = $qiwiPhone;
        $params["md5"] = $this->getQIWISignStr($params["nickname"], $params["amount"], $qiwiPhone);

        $queryParams = http_build_query($params);

        $request = Request::post($this->refillUrl."?".$queryParams)
                                ->addOnCurlOption(CURLOPT_ENCODING, "windows-1251")
                                ->expectsType("xml");

        $response = $request->send();

        if(!isset($response->body->status) || $response->body->status != 0)
        {
            if(!empty($response->body->comment))
                throw new \Exception($response->body->comment->__toString());

            return false;
        }

        if(empty($response->body->ODpaymentID)) {
            throw new \Exception('Произошла внутренняя ошибка.');
            return false;
        }

        $ODpaymentID = $response->body->ODpaymentID->__toString();

        if(empty($response->body->iframeSRC))
        {
            throw new \Exception('Произошла внутренняя ошибка.');
            return false;
        }

        $iframeUrl = base64_decode($response->body->iframeSRC);
        if ($iframeUrl === false) {
            throw new \Exception('Произошла внутренняя ошибка.');
            return false;
        }

        return array(
            "ODpaymentID" => $ODpaymentID,
            "iframeUrl" => $iframeUrl
        );
    }

    public function getQIWISignStr($nickname, $amount, $qiwiPhone)
    {
        return md5(strtolower(self::SECURE_KEY.self::PROJECT_ID.$nickname.floatval($amount).$qiwiPhone.self::SECURE_KEY));
    }

    /**
    * Проверка контрольной подписи запроса
    * Приходит от ДОЛ в проект при оповещении о зачислении платежа
    *
    * @param string $key
    * @param \AppBundle\Entity\BillingOperation $BillingOperation
    * @return bool
    */
    public function checkCallbackKey($key, $BillingOperation)
    {
        /**
        * @todo На данный момент проверка отключена так как сумма приходит в рублях, а в базе она хранится в евро
        */
        return true;

        return $key == md5($BillingOperation->getAmount().$BillingOperation->getUser()->getId().$BillingOperation->getPaysystemInternalId().self::SECURE_KEY);
    }

    /**
    * Проверка контрольной подписи запроса
    * Призодит от ДОЛ в проект при идентификации пользователя или заказа
    *
    * @param string $key
    * @param int $userId
    */
    public function checkCheckKey($key, $userId)
    {
        return $key == md5("0".$userId."0".self::SECURE_KEY);
    }

    /**
    * Проверка текущего счёта системы
    * Возвращает баланс системы, если 0 , то возвращает false, если больше, то возвращает сумму на счету
    *
    * @return float|bool
    */
    public function checkCashoutAbility()
    {
        $systemBalance = $this->getGSGBalance();

        if($systemBalance <= 0)
            return false;

        return (float) $systemBalance;
    }

    /**
    * Валидирует счёт Qiwi-кошелька
    *
    * @param string $qiwiPhone Счёт Qiwi-кошелька
    *
    * @return bool
    */
    public function isQiwiPhoneValid($qiwiPhone)
    {
        if(empty($qiwiPhone))
            return false;

        return true;
    }

    /**
    * Проверяет сумму депозита.
    *
    * @param float $amount Сумма
    *
    * @return bool
    */
    public function greaterThanQiwiMinDeposit($amount)
    {
        return ($amount >= self::MIN_DEPOSIT_QIWI);
    }

    /**
    * Формирует XML для запроса баланса
    *
    * @param array $params Параметры запроса
    * @return string
    */
    private function getGSGPayloadXML($params)
    {
        $paramsForSign = array();

        $xml = new \DOMDocument("1.0", "UTF-8");
        $xml->formatOutput = true;
        $xml->preserveWhiteSpace = true;

        $requestNode = $xml->createElement("request");
        $xml->appendChild($requestNode);

        $projectNode = $xml->createElement("project", self::PROJECT_GSG_ID);
        $requestNode->appendChild($projectNode);
        $paramsForSign["project"] = self::PROJECT_GSG_ID;

        $now = time();
        $timestampNode = $xml->createElement("timestamp", $now);
        $requestNode->appendChild($timestampNode);
        $paramsForSign["timestamp"] = $now;

        foreach($params as $key => $value)
        {
            if($key != "params")
            {
                $paramsForSign[$key] = $value;
                $node = $xml->createElement($key, $value);
                $requestNode->appendChild($node);
            }
            else
            {
                $paramsNode = $xml->createElement("params");
                $requestNode->appendChild($paramsNode);
                foreach($value as $paramKey => $paramValue)
                {
                    $paramsForSign[$paramKey] = $paramValue;
                    $node = $xml->createElement($paramKey, $paramValue);
                    $paramsNode->appendChild($node);
                }
            }
        }

        $sign = $this->getGSGSignStr($paramsForSign);
        $signNode = $xml->createElement("sign", $sign);
        $requestNode->appendChild($signNode);

        return $xml->saveXML();
    }

    /**
    * Формирует строку "подписи" запроса к GSG API
    *
    * @param array $params
    * @return string
    */
    private function getGSGSignStr($params)
    {
        ksort($params);

        $query = "secret=".self::SECURE_GSG_KEY."&".http_build_query($params);
        $query = str_replace(" ", "+", $query);
        return sha1($query);
    }

    private function getStatusMessage($statusId)
    {
        switch($statusId)
        {
            case self::STATUS_BAD_ACCOUNT:
                return $this->translator->trans("Неверный номер карты/счёта");
            break;

        }
        $class = new \ReflectionClass(get_class($this));
        $constants = $class->getConstants();
        foreach($constants as $key => $value)
        {
            if(strpos($key, "STATUS") !== 0)
            {
                unset($constants[$key]);
            }
        }

        $constants = array_flip($constants);

        $message = $this->translator->trans($constants[$statusId], array("%status_id%" => $statusId), "dol");

        return $message;
    }
}
