<?php
namespace AppBundle\Services\Interfaces;

interface ResourceGrabberInterface
{
    public static function getInstance(\Doctrine\ORM\EntityManager $em, \AppBundle\Services\ResourceGrabber $resourceGrabber);
    
    /**
    * Возвращает массив матчей предварительно спаршенных с ресурса
    * 
    * @param \AppBundle\Entity\GrabResource $resource
    * @param int Крайний тур дальше которого нельзя идти, так как есть неспаршенные матчи в лиге
    * @return array
    */
    public function getMatches(\AppBundle\Entity\GrabResource $resource, $maxRound = 1);
    
    /**
    * Возвращает ошибки произошедшие во время парсинга
    * @return array
    */
    public function getErrors();
}
?>
