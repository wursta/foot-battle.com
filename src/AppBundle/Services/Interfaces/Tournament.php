<?php
namespace AppBundle\Services\Interfaces;

interface Tournament
{
    /*
    *   Get current tournament repository
    */
    public function getRepository();

    /**
    *   Get current tournament rules
    */
    public function getRules();

    /**
    *   Get rule by $ruleName
    */
    public function getRule($ruleName);

    /**
    *   Create tournament instance and returns it
    */
    public function createTournament(\AppBundle\Entity\Tournament $initTournament);

    /**
    *   Validate step 2 in tournament creation
    */
    public function validateStep2($tournament, $formData);

    /**
    *   Validate step 3 in tournament creation
    *   Checks that tournament can be published
    */
    public function validateStep3($tournament);

    /**
    *   Return formatted array with matches, bets for tournament view
    */
    public function distributeMatches($tournament);

    /**
    *   Validate and add match to tournament
    */
    public function addMatch($tournament, \AppBundle\Entity\Match $match, $request);
}