<?php
namespace AppBundle\Services;

class LocaleHelper
{
    protected $translator;

    public function __construct($translator)
    {
        $this->translator = $translator;
    }

    public function getLocales()
    {
        return array("ru" => $this->translator->trans("Русский"));
        //return array("ru" => $this->translator->trans("Русский"), "en" => $this->translator->trans("Английский")." (dev)");
    }
}