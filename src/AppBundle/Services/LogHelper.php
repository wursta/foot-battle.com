<?php
namespace AppBundle\Services;

use \AppBundle\Entity\Log;

class LogHelper
{
    private $em;
    
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        $this->em = $em;
    }
    
    public function add($type, $message)
    {    
        $log = new Log();
        
        $now = new \DateTime();
        $log->setDatetime($now);
        $log->setMicroseconds(microtime());
        $log->setType($type);
        $log->setMessage($message);        
        
        $this->em->persist($log);        
    }
}