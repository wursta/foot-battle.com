<?php
namespace AppBundle\Services\Logs;

use \AppBundle\Entity\Logs\CronLog;

class CronLogHelper
{
    private $em;
    private $startDatetime;
    private $startMicroseconds;
    
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        $this->em = $em;
    }        
    
    public function start()
    {
        $this->startDatetime = new \DateTime();        
        $this->startMicroseconds = microtime();
    }        
    
    public function save($command, $command_description = null)
    {
        $log = new CronLog();

        $log->setDatetime($this->startDatetime);
        $log->setMicroseconds($this->startMicroseconds);
        $log->setCommand($command);
        
        $executionTime = time() - $this->startDatetime->format("U");
        $log->setExecutionTime($executionTime);
                
        $log->setMemoryPeakUsage(memory_get_peak_usage(true));
        
        if(!empty($command_description))
            $log->setCommandDescription($command_description);
        
        $this->em->persist($log);
        $this->em->flush($log);
    }
}