<?php
namespace AppBundle\Services;

class MailerHelper
{
    protected $mailer;
    protected $translator;
    protected $templating;

    private $from_email;
    private $from_name;
    private $system_email;
    private $system_name;

    public function __construct($mailer, $translator, $templating, $mailer_from, $mailer_from_name, $mailer_system_email, $mailer_system_name)
    {
        $this->mailer = $mailer;
        $this->translator = $translator;
        $this->templating = $templating;

        $this->from_email = $mailer_from;
        $this->from_name = $mailer_from_name;
        $this->system_email = $mailer_system_email;
        $this->system_name = $mailer_system_name;
    }

    public function sendConfirmationEmail($email, $user, $confirmationUrl)
    {
        $message = \Swift_Message::newInstance()
                    ->setSubject($this->translator->trans("Завершите процесс регистрации на сайте www.FootBattle.com", array(), 'mails'))
                    ->setFrom($this->from_email, $this->from_name)
                    ->setTo($email, $user->getUsername())
                    ->setBody(
                        $this->templating->render(
                            'AppBundle:mails/registration:confirm.html.twig',
                            array("user" => $user,
                                  "confirmationUrl" => $confirmationUrl,
                            )
                        ),
                        'text/html'
                    );

        $this->mailer->send($message);
    }

    /**
    * Отправка письма с новым паролем пользователю
    *
    * @param \AppBundle\Entity\User $user        Пользователь
    * @param string                 $newPassword Новый пароль
    *
    * @return void
    */
    public function sendPasswordRecoveryEmail(\AppBundle\Entity\User $user, $newPassword)
    {
        $message = \Swift_Message::newInstance()
                    ->setSubject($this->translator->trans("Восстановление пароля", array(), 'mails'))
                    ->setFrom($this->from_email, $this->from_name)
                    ->setTo($user->getEmail(), $user->getUsername())
                    ->setBody(
                        $this->templating->render(
                            'AppBundle:mails:password_recovery.html.twig',
                            array("user" => $user,
                                  "newPassword" => $newPassword
                            )
                        ),
                        'text/html'
                    );

        $this->mailer->send($message);
    }

    public function sendTournamentConfirmationRequestEmail($email, $tournamentUrl, $user, $tournament)
    {
        $tournamentTitle = (!$tournament->getTitle()) ? $tournament->getId() : $tournament->getTitle();

        $message = \Swift_Message::newInstance()
                    ->setSubject($this->translator->trans("Ваша заявка на турнир \"%s%\" одобрена", array("%s%" => $tournamentTitle), 'mails'))
                    ->setFrom($this->from_email, $this->from_name)
                    ->setTo($email, $user->getUsername())
                    ->setBody(
                        $this->templating->render(
                            'AppBundle:mails/tournament:request_confirmed.html.twig',
                            array("tournamentUrl" => $tournamentUrl,
                                  "user" => $user,
                                  "tournament" => $tournament
                            )
                        ),
                        'text/html'
                    );

        $this->mailer->send($message);
    }

    public function sendFeedbackMail(\AppBundle\Form\Model\Feedback $feedback)
    {
        $message = \Swift_Message::newInstance()
                    ->setSubject("Сообщение из контактной формы")
                    ->setFrom($this->from_email, $this->from_name)
                    ->setTo($this->system_email, $this->system_name)
                    ->setBody(
                        $this->templating->render(
                            'AppBundle:mails:feedback.html.twig',
                            array("feedback" => $feedback)
                        ),
                        'text/html'
                    );

        $this->mailer->send($message);
    }

    /**
    * Отправка уведомления о том, что матчи скоро отменятся.
    *
    * @param array \AppBundle\Entity\Match[]
    * @param float Часов до отмены матчей переданных в $matches
    */
    public function sendMatchesRejectEmail($matches, $hoursToReject)
    {
        return true;
    }

    /**
    * Отправка заявки на вывод от пользователя
    *
    * @param \AppBundle\Entity\BillingOperation $bill
    * @param \AppBundle\Form\Model\CashoutRequest $cashoutRequest
    */
    public function sendCashoutRequestMail(\AppBundle\Entity\BillingOperation $bill, \AppBundle\Form\Model\CashoutRequest $cashoutRequest)
    {
        $message = \Swift_Message::newInstance()
                        ->setSubject("Заявка на вывод от пользователя " . $bill->getUser()->getNick() . " [" . $bill->getUser()->getId() . "]")
                        ->setFrom($this->from_email, $this->from_name)
                        ->setTo($this->system_email, $this->system_name)
                        ->setPriority(1)
                        //->setReturnPath($bill->getUser()->getEmail())
                        ->setBody(
                            $this->templating->render(
                                'AppBundle:mails:cashout_request.html.twig',
                                array(
                                  "bill" => $bill,
                                  "cashoutRequest" => $cashoutRequest
                                )
                            ),
                            'text/html'
                        );

        $this->mailer->send($message);
    }

    /**
    * Отправка уведомления об ошибке
    *
    * @param string $message
    */
    public function sendErrorMail($message, $file = null, $line = null, $trace = null)
    {        
        if($file) {
            $message .= '<br />File:' . $file;
        }
        
        if($line) {
            $message .= '; Line:' . $line;
        }
        
        if($trace) {
            $message .= '<br />Trace:<br/ >' . $trace;
        }
        
        $message = \Swift_Message::newInstance()
                        ->setSubject("Ошибка во время выполнения")
                        ->setFrom($this->from_email, $this->from_name)
                        ->setTo($this->system_email, $this->system_name)
                        ->setBody(
                            $this->templating->render(
                                'AppBundle:mails:error.html.twig',
                                array("message" => $message)
                            ),
                            'text/html'
                        );

        $this->mailer->send($message);
    }

    /**
    * Отправка тестового мыла
    *
    */
    public function sendTestMail()
    {
        $message = \Swift_Message::newInstance()
                        ->setSubject("Mail check")
                        ->setFrom($this->from_email, $this->from_name)
                        ->setTo($this->system_email, $this->system_name)
                        ->setBody(
                            $this->templating->render(
                                'AppBundle:mails:mailcheck.html.twig'
                            ),
                            'text/html'
                        );

        return $this->mailer->send($message);
    }
}