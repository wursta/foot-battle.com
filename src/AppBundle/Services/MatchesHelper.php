<?php
namespace AppBundle\Services;

use Doctrine\ORM\EntityManager;

use AppBundle\Entity\Match;

class MatchesHelper
{        
    /**    
    * @var \Doctrine\ORM\EntityManager
    */
    private $em;    
    
    /**
    * @var \AppBundle\Entity\MatchRepository
    */
    private $matchesRep;    
    
    /**
    * @var \AppBundle\Entity\LeagueRepository
    */
    private $leagueRep;    
    
    /**
    * @var \AppBundle\Entity\TeamRepository
    */
    private $teamRep;
    
    /**
    * Временная зона в которой хранятся матчи
    * 
    * @var \DateTimeZone
    */
    private $matchesTimezone;
    
    /**
    * Количество часов которое должно пройти с момента начала матча до его отмены
    */
    const REJECT_HOURS = 5;
    
    /**
    * Количество часов которое должно пройти с момента начала матча до отправки уведомления об отмене
    */
    const REJECT_NOTIFY_HOURS = 3;
    
    /**
    * Временная зона в которой приходит время начала матчей из БД
    */
    const TIMEZONE = 'Europe/Moscow';

    public function __construct(EntityManager $em)
    {
        $this->em = $em;        
        $this->matchesRep = $this->em->getRepository("AppBundle\Entity\Match");
        $this->leagueRep = $this->em->getRepository("AppBundle\Entity\League");
        $this->teamRep = $this->em->getRepository("AppBundle\Entity\Team");
        $this->matchesTimezone = new \DateTimeZone(self::TIMEZONE);
    }

    public function resultGuessed(\AppBundle\Entity\Match $match, $score_left, $score_right)
    {        
        return ($match->getScoreLeft() == $score_left && $match->getScoreRight() == $score_right);
    }
    
    /**
    * Проверяет является ли результат матча ничейным
    * 
    * @param \AppBundle\Entity\Match $match
    * 
    * @return boolean
    */
    public function isDraw(\AppBundle\Entity\Match $match)
    {
        return ($match->getScoreLeft() == $match->getScoreRight());
    }
    
    /**
    * Проверяет является ли результат победой домашней команды
    * 
    * @param \AppBundle\Entity\Match $match
    * 
    * @return bool
    */
    public function homeWin(\AppBundle\Entity\Match $match)
    {
        return ($match->getScoreLeft() > $match->getScoreRight());
    }
    
    /**
    * Проверяет является ли результат победой гостевой команды
    * 
    * @param \AppBundle\Entity\Match $match
    * 
    * @return bool
    */
    public function guestWin(\AppBundle\Entity\Match $match)
    {
        return ($match->getScoreLeft() < $match->getScoreRight());
    }
    
    /**
    * Возвращает абсолютную разницу мячей 
    * 
    * @param \AppBundle\Entity\Match $match
    * 
    * @return int
    */
    public function getGoalDifference(\AppBundle\Entity\Match $match)
    {
        return abs($match->getScoreLeft() - $match->getScoreRight());
    }
    
    /**
    * Возвращает дату и время начала матча.
    * 
    * @param \DateTime $matchDate Дата начала матча.
    * @param \DateTime $matchTime Время начала матча.
    * 
    * @return \DateTime
    */
    public function getMatchStartDatetime(\DateTime $matchDate, \DateTime $matchTime)
    {
        return $matchDate->setTime($matchTime->format("H"), $matchTime->format("i"), 0);
    }
    
    /**
    * Сохраняет матчи из массива переданного в параметре в БД
    * 
    * @param array $matches
    * @example [league_id][
    *   match_datetime,
    *   home_team_id
    *   guest_team_id
    *   score => [
    *           home,
    *           guest
    *       ]
    *   round
    * ]
    * @return void
    */
    public function fillMatches($matchesPull)
    {
        foreach($matchesPull as $leagueId => $matches)
        {
            $league = $this->leagueRep->find($leagueId);
            
            foreach($matches as $matchData)
            {                
                $matchData["league_id"] = $league->getId();
                //Переводим время матча во временную зону в которой хранятся матчи в БД
                $matchData["match_datetime"] = $matchData["match_datetime"]->setTimezone($this->matchesTimezone);
                
                $match = $this->matchesRep->findMatchByData($matchData);
                
                if(!$match)
                {
                    $match = new Match();                    
                    $match->setLeague($league);
                    
                    $homeTeam = $this->teamRep->find($matchData["home_team_id"]);
                    $match->setHomeTeam($homeTeam);                    
                    
                    $guestTeam = $this->teamRep->find($matchData["guest_team_id"]);
                    $match->setGuestTeam($guestTeam);                                        
                }
                
                if($match->getIsOver() || $match->getIsRejected())
                    continue;

                //Обновляем время матча если вдруг оно изменилось
                $match->setMatchDate($matchData["match_datetime"]);
                $match->setMatchTime($matchData["match_datetime"]);
                
                $match->setTourNumber($matchData["round"]);
                
                if($matchData["score"])
                {
                    $match->setScoreLeft($matchData["score"]["home"]);
                    $match->setScoreRight($matchData["score"]["guest"]);
                    $match->setIsOver(true);
                    $match->setIsStarted(true);
                    $match->setIsRejected(false);
                }
                
                $this->em->persist($match);
                $this->em->flush($match);
            }
        }        
    }
    
    public function notifyAboutReject(\AppBundle\Services\MailerHelper $mailerHelper)
    {
        $matchDatetime = \DateTime::createFromFormat("U", time() - self::REJECT_NOTIFY_HOURS*3600)->setTimezone(new \DateTimeZone(self::TIMEZONE));
        
        $matches = $this->matchesRep->findThatNeedsToReject($matchDatetime);
        
        $mailerHelper->sendMatchesRejectEmail($matches, self::REJECT_HOURS - self::REJECT_NOTIFY_HOURS);
    }
    
    /**
    * Отменяет матчи, которые не закончились в течении x часов    
    */
    public function rejectMatches()
    {
        $matchDatetime = \DateTime::createFromFormat("U", time() - self::REJECT_HOURS*3600)->setTimezone(new \DateTimeZone(self::TIMEZONE));
        
        $matches = $this->matchesRep->findThatNeedsToReject($matchDatetime);
        
        foreach($matches as $match)
        {            
            $match->setIsOver(false);
            $match->setIsStarted(true);
            $match->setIsRejected(true);

            $this->em->persist($match);
        }
        $this->em->flush();
    }
}