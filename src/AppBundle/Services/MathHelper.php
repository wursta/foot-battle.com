<?php
namespace AppBundle\Services;

class MathHelper
{
    public function factorial($n)
    {
        return $n?$n*$this->factorial($n-1):1;
    }   
}