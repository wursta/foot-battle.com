<?php
namespace AppBundle\Services;

use AppBundle\Entity\PartnerCharge;

class PartnerProgramHelper
{
    private $em;
    
    const TYPE_FIRST_DEPOSIT = 'FIRST_DEPOSIT';
    
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        $this->em = $em;
    }
    
    /**
    * Создаёт запись о платеже
    * 
    * @param \AppBundle\Entity\User $user Пользователь, кому начисляется вознаграждение
    * @param \AppBundle\Entity\User $referralUser Пользователь, в результате действий которого начисляется вознаграждение
    * @param string $type Тип вознаграждения
    * @param float $amount Сумма вознаграждения
    * 
    * @return \AppBundle\Entity\PartnerCharge
    */
    public function addCharge(\AppBundle\Entity\User $user, \AppBundle\Entity\User $referralUser, $type, $amount)
    {
      $charge = new PartnerCharge();
      $charge->setUser($user);
      $charge->setReferralUser($referralUser);
      $charge->setType($type);
      $charge->setAmount($amount);
      $charge->setOperationDatetime(new \DateTime());
      
      $this->em->persist($charge);
      $this->em->flush($charge);
      
      return $charge;
    }
}