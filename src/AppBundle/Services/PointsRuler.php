<?php
namespace AppBundle\Services;

class PointsRuler
{
    const CORRECT_SCORE_DELIMITER = ':';
    /**    
    * 
    * @var \AppBundle\Services\MatchesHelper
    */
    private $matchesHelper;
    
    public function __construct(\AppBundle\Services\MatchesHelper $matchesHelper)
    {        
        $this->matchesHelper = $matchesHelper;
    }
    
    /**
    * Подсчитывает количество очков за прогноз
    * 
    * @param \AppBundle\Entity\Bet $bet
    * @return float
    */
    public function calculatePoints(\AppBundle\Entity\Bet $bet)
    {
        $pointsScheme = $bet->getTournament()->getPointsScheme();
        
        $match = $bet->getMatch();
        
        $points = 0;
        
        //если матч отменён
        if($match->getIsRejected())
            return 0;
        

        //если не угадан исход
        if(!$this->matchOutcomeGuessed($bet, $match))
            return 0;
        
        //за угаданный исход
        $points = $pointsScheme->getBase();
        
        //если не угадана разница
        if(!$this->scoreDifferenceGuessed($bet, $match))
            return $points;
        
        //за угаданную разницу
        $points = $pointsScheme->getBase() + $this->getExtraDifferencePoints($pointsScheme, $match);
        
        //eсли не угадан счёт
        if(!$this->scoreGuessed($bet, $match))
            return $points;
        
        //за угаданный счёт
        $points = $this->getPointsForCorrectScore(
            $pointsScheme, 
            $match->getScoreLeft(), 
            $match->getScoreRight()
        );

        return $points;
    }
    
    /**
    * Проверка угаданности исхода
    * 
    * @param \AppBundle\Entity\Bet $bet
    * @param \AppBundle\Entity\Match $match
    * @return boolean
    */
    public function matchOutcomeGuessed(\AppBundle\Entity\Bet $bet, \AppBundle\Entity\Match $match)
    {
        if($this->matchesHelper->isDraw($match) && $bet->getScoreLeft() == $bet->getScoreRight())        
            return true;
        elseif($this->matchesHelper->homeWin($match) && $bet->getScoreLeft() > $bet->getScoreRight())        
            return true;
        elseif($this->matchesHelper->guestWin($match) && $bet->getScoreLeft() < $bet->getScoreRight())        
            return true;
        
        return false;
    }
    
    /**
    * Проверка угаданности разницы мячей
    * 
    * @param \AppBundle\Entity\Bet $bet
    * @param \AppBundle\Entity\Match $match
    * @return boolean
    */
    public function scoreDifferenceGuessed(\AppBundle\Entity\Bet $bet, \AppBundle\Entity\Match $match)
    {
        $goalsDifference = $this->matchesHelper->getGoalDifference($match);
        $betDifference = abs($bet->getScoreLeft() - $bet->getScoreRight());
        
        if($goalsDifference == $betDifference)
            return true;
        
        return false;
    }
    
    /**
    * Возвращает количество дополнительных очков за разницу мячей
    * 
    * @param \AppBundle\Entity\PointsScheme $pointsScheme
    * @param \AppBundle\Entity\Bet $bet    
    * 
    * @return float
    */
    public function getExtraDifferencePoints(
        \AppBundle\Entity\PointsScheme $pointsScheme, 
        \AppBundle\Entity\Match $match
    )
    {
        if($this->matchesHelper->isDraw($match))
            return $pointsScheme->getAddDiff0();
        
        if($this->matchesHelper->getGoalDifference($match) == 1)
            return $pointsScheme->getAddDiff1();
        
        if($this->matchesHelper->getGoalDifference($match) == 2)
            return $pointsScheme->getAddDiff2();
        
        if($this->matchesHelper->getGoalDifference($match) == 3)
            return $pointsScheme->getAddDiff3();
        
        if($this->matchesHelper->getGoalDifference($match) >= 4)
            return $pointsScheme->getAddDiff3();
    }
    
    /**
    * Проверка угаданности точного счёта
    * 
    * @param \AppBundle\Entity\Bet $bet
    * @param \AppBundle\Entity\Match $match
    * @return boolean
    */
    public function scoreGuessed(\AppBundle\Entity\Bet $bet, \AppBundle\Entity\Match $match)
    {
        if($match->getScoreLeft() == $bet->getScoreLeft() && $match->getScoreRight() == $bet->getScoreRight())
            return true;
            
        return false;
    }
    
    /**
    * Воззвращает количество очков за конкретный угаданный счёт
    * 
    * @param \AppBundle\Entity\PointsScheme $pointsScheme
    * @param int $scoreLeft
    * @param int $scoreRight
    * 
    * @return float
    */
    public function getPointsForCorrectScore(\AppBundle\Entity\PointsScheme $pointsScheme, $scoreLeft, $scoreRight)
    {
        $score = $scoreLeft . self::CORRECT_SCORE_DELIMITER . $scoreRight;
        
        $points = 0;
        
        $scoresPoints = $this->getCorrectScoresPointsList($pointsScheme->getCorrectScores());
        
        if(isset($scoresPoints[$score])) {
            $points = $scoresPoints[$score];
        } else {
            $points = $pointsScheme->getAnotherCorrectScorePoints();
        }
        
        return $points;
    }
    
    /**
    * Возвращает массив со всеми возможными счетами и очками за них.
    * 
    * @param \AppBundle\Entity\PointsSchemeCorrectScore[] $correctScores
    * 
    * @return array Массив вида [ [2:1] => 6, [1:2] => 6, [1:1] => 7, [3:1] => 8, [1:3] => 8, ... ]
    */
    private function getCorrectScoresPointsList($correctScores)
    {
        $scoresPoints = array();        
        foreach ($correctScores as $correctScore) {
            $score = split(self::CORRECT_SCORE_DELIMITER, $correctScore->getScore());

            $scoreLeft = $score[0];
            $scoreRight = $score[1];
            
            $scoresPoints[$scoreLeft . self::CORRECT_SCORE_DELIMITER . $scoreRight] = $correctScore->getPoints();
            $scoresPoints[$scoreRight . self::CORRECT_SCORE_DELIMITER . $scoreLeft] = $correctScore->getPoints();
        }
        
        return $scoresPoints;
    }
}