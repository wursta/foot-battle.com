<?php
namespace AppBundle\Services;

use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Policy;
use AppBundle\Entity\PointsScheme;
use AppBundle\Entity\Regulation;
use AppBundle\Entity\WinRulesGroup;

class PolicyHelper
{
    /**
    * @var \AppBundle\Entity\TournamentFormatRepository
    */
    private $tournamentFormatRep;
    
    /**
    * @var \AppBundle\Entity\PolicyRepository
    */
    private $policyRep;
    
    /**
    * @var \AppBundle\Entity\PointsSchemeRepository
    */
    private $pointsSchemeRep;
    
    /**
    * @var \AppBundle\Entity\RegulationRepository
    */
    private $regulationRep;
    
    /**
    * @var \AppBundle\Entity\WinRulesGroupRepository
    */
    private $winRulesGroupRep;

    /**
    * @var \Twig_Environment
    */
    private $twig;

    /**
    * Конструктор
    *
    * @param EntityManager $em
    *
    * @return PolicyHelper
    */
    public function __construct(EntityManager $em, \Twig_Environment $twig)
    {
        $this->tournamentFormatRep = $em->getRepository('AppBundle\Entity\TournamentFormat');
        $this->policyRep = $em->getRepository('AppBundle\Entity\Policy');
        $this->pointsSchemeRep = $em->getRepository('AppBundle\Entity\PointsScheme');
        $this->regulationRep = $em->getRepository('AppBundle\Entity\Regulation');        
        $this->winRulesGroupRep = $em->getRepository('AppBundle\Entity\WinRulesGroup');
        $this->twig = $twig;
    }

    /**
    * Возвращает пользовательское соглашение установленное по умолчанию
    *
    * @param string $locale Язык редакции.
    *
    * @return \AppBundle\Entity\Policy|bool
    */
    public function getDefault($locale)
    {
        $policy = $this->policyRep->findOneBy(array(
            'locale' => $locale,
            'isDefault' => true
        ));

        if (!$policy) {
            return false;
        }

        return $policy;
    }
    
    public function getDefaultPointsScheme()
    {
        $pointsScheme = $this->pointsSchemeRep->findOneBy(array(
            "isDefault" => true
        ));
        
        if (!$pointsScheme) {
            return false;
        }
        
        return $pointsScheme;
    }
    
    /**
    * Возвращает регламент турнира заданного формата по-умолчанию
    * 
    * @param string $locale Язык редакции регламента.
    * @param int $tournamentFormatId ID формата турнира
    * 
    * @return \AppBundle\Entity\Regulation|bool
    */
    public function getDefaultFormatRegulation($locale, $tournamentFormatId)
    {
        $regulation = $this->regulationRep->findOneBy(array(
            "locale" => $locale,
            "tournamentFormat" => $tournamentFormatId,
            "isDefault" => true
        ));
        
        if (!$regulation) {
            return false;
        }

        return $regulation;
    }
    
    /**
    * Возвращает группы распределения призовых установленные по-умолчанию по заданному формату турнира
    * 
    * @param int $tournamentFormatId ID формата турнира
    * 
    * @return \AppBundle\Entity\WinRulesGroup[]
    */
    public function getDefaultWinRulesGroups($tournamentFormatId)
    {
        $winRulesGroups = $this->winRulesGroupRep->findByFormat($tournamentFormatId);
        
        if (!$winRulesGroups) {
            return false;
        }
        
        return $winRulesGroups;
    }

    /**
    * Рендерит пользовательское соглашения прогоняя тексты через twig.
    *
    * @param Policy $policy  Редакиця пользовательского соглашения
    * @param string $context Контекст рендеринга.
    * Определят откуда будут браться шаблоны для замены или frontend, admin или common.
    *
    * @return void
    */
    public function render(Policy $policy, $context = 'frontend')
    {
        $params = array();

        $pointsScheme = $policy->getPointsScheme();

        $params['POINTS_SCHEME'] = $this->renderPointsScheme($pointsScheme, $policy->getLocale());               
        
        $params['TOURNAMENT_CHAMPIONSHIP_REGULATION'] = $this->renderRegulation(
            $policy->getRegulationChampionship()
        );
        $params['TOURNAMENT_ROUND_REGULATION'] = $this->renderRegulation(
            $policy->getRegulationRound()
        );
        $params['TOURNAMENT_PLAYOFF_REGULATION'] = $this->renderRegulation(
            $policy->getRegulationPlayoff()
        );
        $params['TOURNAMENT_CHAMPIONSLEAGUE_REGULATION'] = $this->renderRegulation(
            $policy->getRegulationChampionsleague()
        );
        
        $formats = $this->tournamentFormatRep->findAll();
        $formatsIds = array();
        foreach ($formats as $format) {
            $formatsIds[$format->getInternalName()] = $format->getId();
        }
        
        $params['CHAMPIONSHIP_WIN_DISTRIBUTION'] = $this->renderWinDistribution(
            $this->getDefaultWinRulesGroups($formatsIds["championship"]),
            $policy->getLocale()
        );
        
        $params['ROUND_WIN_DISTRIBUTION'] = $this->renderWinDistribution(
            $this->getDefaultWinRulesGroups($formatsIds["round"]),
            $policy->getLocale()
        );
        
        $params['PLAYOFF_WIN_DISTRIBUTION'] = $this->renderWinDistribution(
            $this->getDefaultWinRulesGroups($formatsIds["playoff"]),
            $policy->getLocale()
        );
        
        $params['CHAMPIONSLEAGUE_WIN_DISTRIBUTION'] = $this->renderWinDistribution(
            $this->getDefaultWinRulesGroups($formatsIds["championsleague"]),
            $policy->getLocale()
        );

        $template = $this->twig->createTemplate($policy->getTerms());
        $policy->setTerms($template->render($params));

        $template = $this->twig->createTemplate($policy->getRegulations());
        $policy->setRegulations($template->render($params));    

        $template = $this->twig->createTemplate($policy->getPrivacy());
        $policy->setPrivacy($template->render($params));
    }
    
    /**
    * Рендерит распределение призовых
    * 
    * @param array  $winRulesGroups Массив с объектами \AppBundle\Entity\WinRulesGroup
    * @param string $locale         Локаль.
    * 
    * @return string
    */
    public function renderWinDistribution(array $winRulesGroups, $locale) 
    {
        return $this->twig->render(
            'AppBundle:frontend/policies/' . $locale . ':win_distribution.html.twig',
            array(
                "winRulesGroups" => $winRulesGroups
            )
        );
    }
    
    /**
    * Рендерит схему начисления очков
    * 
    * @param PointsScheme $pointsScheme Схема начисления очков.
    * @param string       $locale       Локаль.
    * 
    * @return string
    */
    public function renderPointsScheme(PointsScheme $pointsScheme, $locale)
    {
        return $this->twig->render(
            'AppBundle:frontend/policies/' . $locale . ':points_scheme.html.twig',
            array(
                "pointsScheme" => $pointsScheme
            )
        );
    }
    
    /**
    * Рендерит регламент турнира прогоняя текст через twig
    * 
    * @param Regulation $regulation Регламент.
    * 
    * @return string
    */
    public function renderRegulation(Regulation $regulation) 
    {        
        $params = array();
        $pointsScheme = $this->getDefaultPointsScheme();
        
        $params['POINTS_SCHEME'] = $this->renderPointsScheme($pointsScheme, $regulation->getLocale());        
        $template = $this->twig->createTemplate($regulation->getText());
        $regulation->setText($template->render($params));
        
        return $regulation->getText();
    }
}
