<?php
namespace AppBundle\Services;

class RatingHelper
{    
    public function getFormatted($rating)
    {
      return number_format($rating, 4, '.', ',');
    }
}