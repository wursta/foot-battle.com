<?php
namespace AppBundle\Services\ResourceGrabber;

use Symfony\Component\CssSelector\CssSelector;
use Symfony\Component\DomCrawler\Crawler;

use AppBundle\Services\Interfaces\ResourceGrabberInterface;
use AppBundle\Exception\ResourceParserException;

class XScores implements ResourceGrabberInterface
{
    /**
    * Временная зона в которой приходит время матча
    */
    const TIMEZONE = 'Europe/Athens';
    
    /**
    * Сегодня + Количество дней на которое парсятся матчи
    */
    const MATCH_DAYS_FORWARD = 5;
    
    /**
    * Сегодня - Количество дней на которое парсятся матчи
    */
    const MATCH_DAYS_BACKWARD = 2;
    
    /**
    * Формат даты на ресурсе
    */
    const DATE_FORMAT = "Y-m-d";
    
    /**
    * Статус матча - Не начался
    */
    const STATUS_MATCH_NOT_STARTED = 0;
    
    /**
    * Статус матча - Начался
    */
    const STATUS_MATCH_STARTED = 1;
    
    /**
    * Статус матча - Окончен
    */
    const STATUS_MATCH_FINISHED = 2;
    
    /**
    * Статус матча - Отменён
    */
    const STATUS_MATCH_REJECTED = 3;
    
    private static $_instance = null;
    
    /**
    * @var \Doctrine\ORM\EntityManager
    */
    private $em;
    
    /**    
    * @var \AppBundle\Services\ResourceGrabber
    */
    private $resourceGrabber;
    
    private $resourcesTeamsTitles = array();
    
    /**
    * Минимальная дата матча пападающего под парсинг
    * 
    * @var \DateTime
    */
    private $minParseDatetime;
    
    /**
    * Максимальная дата матча пападающего под парсинг
    * 
    * @var \DateTime
    */
    private $maxParseDatetime;
    
    /**
    * Временная зона в которой приходят матчи
    * 
    * @var \DateTimeZone
    */
    private $matchesTimezone;
    
    private $teamsTitlesErrors = array();
    
    private $matchRowMap = array(
        "MATCH_TIME" => 0,
        "MATCH_STATUS" => 1,    
        "HOME_TEAM_NAME" => 4,
        "GUEST_TEAM_NAME" => 7,
        "MATCH_RESULT_FULL_TIME" => 11,
        "MATCH_RESULT_EXTRA_TIME" => 12,
        "MATCH_RESULT_PENALTIES" => 13
    );        
    
    private $matchStatusMap = array(
        "Sched" => self::STATUS_MATCH_NOT_STARTED,
        "1 HF" => self::STATUS_MATCH_STARTED,
        "H/T" => self::STATUS_MATCH_STARTED,
        "2 HF" => self::STATUS_MATCH_STARTED,        
        "E/T" => self::STATUS_MATCH_STARTED,        
        "Pen" => self::STATUS_MATCH_STARTED,
        "PEN." => self::STATUS_MATCH_STARTED,
        "Int" => self::STATUS_MATCH_REJECTED,
        "Abd" => self::STATUS_MATCH_REJECTED,
        "Post" => self::STATUS_MATCH_REJECTED,
        "Canc" => self::STATUS_MATCH_REJECTED,
        "Fin" => self::STATUS_MATCH_FINISHED,
        "FTR" => self::STATUS_MATCH_FINISHED,
    );
    
    /**
    * @return XScores
    */
    public static function getInstance(\Doctrine\ORM\EntityManager $em, \AppBundle\Services\ResourceGrabber $resourceGrabber)
    {
        if (null === self::$_instance)
        {            
            self::$_instance = new self($em, $resourceGrabber);
        }

        return self::$_instance;
    }
        
    private function __construct(\Doctrine\ORM\EntityManager $em, \AppBundle\Services\ResourceGrabber $resourceGrabber) {
        $this->em = $em;
        $this->resourceGrabber = $resourceGrabber;
        $this->matchesTimezone = new \DateTimeZone(self::TIMEZONE);
        $this->minParseDatetime = \DateTime::createFromFormat("U", time() - self::MATCH_DAYS_BACKWARD*24*60*60)->setTimezone($this->matchesTimezone)->setTime(0,0,0);
        $this->maxParseDatetime = \DateTime::createFromFormat("U", time() + self::MATCH_DAYS_FORWARD*24*60*60)->setTimezone($this->matchesTimezone)->setTime(23,59,59);        
    }
    
    private function __clone() {}        
    
    public function getMatches(\AppBundle\Entity\GrabResource $resource, $maxRound = 1)
    {
        $this->initTeams($resource);
        
        $matches = array();

        $settings = unserialize($resource->getCalendarUrl());
        $roundsUrls = $settings["rounds_urls"];
        $roundChanged = false;
        $matchesWOScoreInCurRound = false;
        
        foreach($roundsUrls as $roundNum => $url)
        {
            if($roundNum < $resource->getCurrentRound())
                continue;

            $content = $this->resourceGrabber->getContentFromUrl($url, 'http://www.xscores.com/soccer/');

            $crawler = new Crawler();
            $crawler->addHtmlContent($content);
            unset($content);
            
            $scoreTableRows = $crawler->filter('#scoretable')->children();
            
            //Если нету ни одной строки(точнее только заголовок), значит матчей ещё нет либо в этом туре/месяце их нет
            //Пропускаем этот тур/месяц и переходим к следующему
            if(count($scoreTableRows) == 1)
                continue;
            
            $matchDateTime = null;
            
            //Итерируем по строкам таблицы с матчами
            for($rowIndex = 1; $rowIndex < count($scoreTableRows); $rowIndex++)
            {
                $row = $scoreTableRows->eq($rowIndex);

                //Если строка является строкой с датой матчей, то парсим дату матча
                if($this->isDateRow($row))
                {
                    $matchDateTime = $this->getDateFromDateRow($row);
                    
                    if(!$matchDateTime || !($matchDateTime instanceof \DateTime))
                        throw new ResourceParserException("Не удалось спарсить дату матчей лиги с ID:".$resource->getLeague()->getId()." по следующему адресу: ".$url." на ".$rowIndex." строчке.");
                                        
                    $matchDateTime->setTime(0,0,0);
                    
                    //Если дата матча меньше, чем Сегодня - x дней, то пропускаем все матчи этой даты.                    
                    //Для этого устанавливаем переменную $matchDateTime в NULL, так как без даты матчи парсится не будут.                    
                    if($matchDateTime->format("U") < $this->minParseDatetime->format("U")// ||
                       //$matchDateTime->format("U") > $this->maxParseDatetime->format("U")
                      ) {
                        $matchDateTime = null;
                        continue;
                    }
                    
                    //Переходим к следующей строке
                    continue;
                }
                
                //Если дата матча не установлена, то это значит, что данный матч парсить не требуется
                //Пропускаем эту строку с матчем
                if(!$matchDateTime || !($matchDateTime instanceof \DateTime))
                    continue;
                
                $parsedMatchData = $this->parseMatchFromRow($row);                                
                                
                //Если не удалось спарсить матчи, то пропускаем
                if(!$parsedMatchData)                
                    continue;                
                                
                //Устанавливаем время матча
                $matchDateTime->setTime($parsedMatchData["time"]["hour"], $parsedMatchData["time"]["minute"], 0);
                
                //Если в "Текущем туре" есть хотя бы один матч у которых ещё нету счёта, то нужно проставить данный флаг
                if(!$parsedMatchData["score"] && $roundNum == $resource->getCurrentRound())
                {
                    $matchesWOScoreInCurRound = true;
                }
                
                //Если дата матча больше, чем Сегодня + x дней, то пропускаем этот матч                
                if($matchDateTime->format("U") > $this->maxParseDatetime->format("U"))
                {
                    //Но записываем тур,если этот матч ещё не состоялся(Это может быть, если матч был отложен, но он стоит всё ещё в "старом" туре)
                    //Например, если результаты есть уже в 26 туре, а данный матч стоит ещё в 20 туре.
                    if(!$matchesWOScoreInCurRound && !$parsedMatchData["score"] && !$roundChanged && $maxRound >= $roundNum)
                    {
                        $roundChanged = true;
                        $resource->setCurrentRound($roundNum);
                        $this->em->persist($resource);
                        $this->em->flush($resource);                        
                    }
                    
                    continue;
                }
                                
                $homeTeamId = array_search($parsedMatchData["home_team"], $this->resourcesTeamsTitles[$resource->getId()]);
                $guestTeamId = array_search($parsedMatchData["guest_team"], $this->resourcesTeamsTitles[$resource->getId()]);
                
                //Если команда с таким названием не найдена на портале, то нужно добавить её в лог ошибок
                if($homeTeamId === false)
                {
                    $this->teamsTitlesErrors[$resource->getLeague()->getId()][$parsedMatchData["home_team"]] = $parsedMatchData["home_team"];
                    continue;
                }
                
                //Если команда с таким названием не найдена на портале, то нужно добавить её в лог ошибок
                if($guestTeamId === false)
                {
                    $this->teamsTitlesErrors[$resource->getLeague()->getId()][$parsedMatchData["guest_team"]] = $parsedMatchData["guest_team"];
                    continue;
                }
                
                $matches[] = array(
                    "match_datetime" => clone $matchDateTime,
                    "home_team_id" => $homeTeamId,
                    "guest_team_id" => $guestTeamId,
                    "score" => $parsedMatchData["score"],
                    "round" => $roundNum
                );
            }
        }
        
        return $matches;
    }
    
    /**
    * Получает данные о матчах с ресурса
    * 
    * @param \AppBundle\Entity\GrabResource $resource
    * @return array
    */
    public function getMatchesByPeriod(\AppBundle\Entity\GrabResource $resource)
    {
      $this->initTeams($resource);
      
      $matches = array();

      $curDate = new \DateTime();      
      $urls = array();            
      //если минимальная дата парсинга находится в предыдущем месяце, то нужно спарсить и его
      //если максимальная дата парсинга находится в слудующем месяце, то нужно спарсить и его
      if($this->maxParseDatetime->format('n') < $curDate->format('n'))
      {
        $urls[] = $this->generateCalendarUrl($resource->getCalendarUrl(), $this->minParseDatetime);
      }
      
      $urls[] = $this->generateCalendarUrl($resource->getCalendarUrl(), $curDate);
      
      //если максимальная дата парсинга находится в слудующем месяце, то нужно спарсить и его
      if($this->maxParseDatetime->format('n') > $curDate->format('n'))
      {
        $urls[] = $this->generateCalendarUrl($resource->getCalendarUrl(), $this->maxParseDatetime);
      }
      
      foreach($urls as $monthNum => $url)
      {
        $content = $this->resourceGrabber->getContentFromUrl($url, 'http://www.xscores.com/soccer/');

        $crawler = new Crawler();
        $crawler->addHtmlContent($content);
        unset($content);
        
        $scoreTableRows = $crawler->filter('#scoretable')->children();
        
        //Если нету ни одной строки(точнее только заголовок), значит матчей ещё нет, либо в этом месяце их нет
        //Пропускаем этот месяц и переходим к следующему, если он есть
        if(count($scoreTableRows) == 1)
            continue;
        
        $matchDateTime = null;
        
        //Итерируем по строкам таблицы с матчами
        for($rowIndex = 1; $rowIndex < count($scoreTableRows); $rowIndex++)
        {
            $row = $scoreTableRows->eq($rowIndex);

            //Если строка является строкой с датой матчей, то парсим дату матча
            if($this->isDateRow($row))
            {
                $matchDateTime = $this->getDateFromDateRow($row);
                
                if(!$matchDateTime || !($matchDateTime instanceof \DateTime))
                    throw new ResourceParserException("Не удалось спарсить дату матчей лиги с ID:".$resource->getLeague()->getId()." по следующему адресу: ".$url." на ".$rowIndex." строчке.");
                                    
                $matchDateTime->setTime(0,0,0);
                
                //Если дата матча меньше, чем Сегодня - x дней, то пропускаем все матчи этой даты.                    
                //Для этого устанавливаем переменную $matchDateTime в NULL, так как без даты матчи парсится не будут.                    
                if($matchDateTime->format("U") < $this->minParseDatetime->format("U")) {
                    $matchDateTime = null;
                    continue;
                }
                
                //Переходим к следующей строке
                continue;
            }
            
            //Если дата матча не установлена, то это значит, что данный матч парсить не требуется
            //Пропускаем эту строку с матчем
            if(!$matchDateTime || !($matchDateTime instanceof \DateTime))
                continue;
            
            $parsedMatchData = $this->parseMatchFromRow($row);
            
            //Если не удалось спарсить матчи, то пропускаем
            if(!$parsedMatchData)                
                continue;                
                            
            //Устанавливаем время матча
            $matchDateTime->setTime($parsedMatchData["time"]["hour"], $parsedMatchData["time"]["minute"], 0);
            
            //Если дата матча больше, чем Сегодня + x дней, то пропускаем этот матч
            if($matchDateTime->format("U") > $this->maxParseDatetime->format("U"))
                continue;
            
            $homeTeamId = array_search($parsedMatchData["home_team"], $this->resourcesTeamsTitles[$resource->getId()]);
            $guestTeamId = array_search($parsedMatchData["guest_team"], $this->resourcesTeamsTitles[$resource->getId()]);
            
            //Если команда с таким названием не найдена на портале, то нужно добавить её в лог ошибок
            if($homeTeamId === false)
            {
                $this->teamsTitlesErrors[$resource->getLeague()->getId()][$parsedMatchData["home_team"]] = $parsedMatchData["home_team"];
                continue;
            }
            
            //Если команда с таким названием не найдена на портале, то нужно добавить её в лог ошибок
            if($guestTeamId === false)
            {
                $this->teamsTitlesErrors[$resource->getLeague()->getId()][$parsedMatchData["guest_team"]] = $parsedMatchData["guest_team"];
                continue;
            }
            
            $matches[] = array(
                "match_datetime" => clone $matchDateTime,
                "home_team_id" => $homeTeamId,
                "guest_team_id" => $guestTeamId,
                "score" => $parsedMatchData["score"],
                "round" => 0
            );
        }
      }
      
      return $matches;
    }
    
    public function getErrors()
    {
        return array(
            "teams_errors" => $this->teamsTitlesErrors
        );
    }
    
    /**
    * Генерирует ссылку для парсера подставляя в неё года и номер месяца
    * 
    * @param string $urlMask URL c плейсхолдерами для замены, вида http://www.xscores.com/soccer/leagueresults/england/premier_league/{prev_year}-{cur_year}/p/{month_num}
    * @param \DateTime $date
    * 
    * @return string
    */
    private function generateCalendarUrl($urlMask, \DateTime $date)
    {      
      $replacement = array(
        "{prev_year}" => $date->format('Y') - 1,
        "{cur_year}" => $date->format('Y'),
        "{month_num}" => $date->format('n'),
      );
      
      return str_replace(array_keys($replacement), array_values($replacement), $urlMask);
    }
    
    private function parseMatchFromRow(Crawler $row)
    {
        $cells = $row->children();
                
        $timeCell = $cells->eq($this->matchRowMap["MATCH_TIME"]);
        $statusCell = $cells->eq($this->matchRowMap["MATCH_STATUS"]);
        $homeTeamCell = $cells->eq($this->matchRowMap["HOME_TEAM_NAME"]);
        $guestTeamCell = $cells->eq($this->matchRowMap["GUEST_TEAM_NAME"]);
        $scoreCell = $cells->eq($this->matchRowMap["MATCH_RESULT_FULL_TIME"]);
        
        if(!($timeCell instanceof Crawler) || 
           !($statusCell instanceof Crawler) || 
           !($homeTeamCell instanceof Crawler) ||
           !($guestTeamCell instanceof Crawler) ||
           !($scoreCell instanceof Crawler)
           )
        {
            return null;
        }    
        
        $time = trim($timeCell->text());

        if(empty($time))
            return null;
        
        $time = explode(":", $time);
        
        if(!isset($time[0]) || !isset($time[1]))
            return null;
        
        $time = array("hour" => $time[0], "minute" => $time[1]);
        
        $status = self::STATUS_MATCH_NOT_STARTED;
        $statusStr = trim($statusCell->text());
        if(!empty($this->matchStatusMap[$statusStr]))
            $status = $this->matchStatusMap[$statusStr];
            
        $homeTeam = trim($homeTeamCell->text());
        if(empty($homeTeam))
            return null;
            
        $guestTeam = trim($guestTeamCell->text());
        if(empty($guestTeam))
            return null;
            
        $score = explode("-", trim($scoreCell->text()));
        
        if(isset($score[0]) && is_numeric($score[0]) &&
           isset($score[1]) && is_numeric($score[1])
          ) {
              $score = array("home" => (int) $score[0], "guest" => (int) $score[1]);
        }
        else
        {
            $score = null;
        }                
        
        if(!empty($score) && $status != self::STATUS_MATCH_FINISHED)
            $score = null;
        
        return array(
            "time" => $time,            
            "home_team" => $homeTeam,
            "guest_team" => $guestTeam,
            "score" => $score
        );        
    }
    
    private function isDateRow(Crawler $row)
    {
        $dateCell = $row->filter('td.countryheader');
        
        return count($dateCell);
    }
    
    private function getDateFromDateRow(Crawler $row)
    {        
        $dateStr = $row->filter('td.countryheader')->text();        
        $matchDate = \DateTime::createFromFormat(self::DATE_FORMAT, $dateStr, $this->matchesTimezone);
        
        if(!$matchDate)
            return null;
                
        return $matchDate;
    }
    
    private function initTeams(\AppBundle\Entity\GrabResource $resource)
    {
        if(!empty($this->resourcesTeamsTitles[$resource->getId()]))
            return $this->resourcesTeamsTitles[$resource->getId()];                
            
        $this->resourcesTeamsTitles[$resource->getId()] = array();
        /**        
        * @var \AppBundle\Entity\ResourceTeam[]
        */
        $resourceTeams = $resource->getResourceTeams();
        foreach($resourceTeams as $resourceTeam)
        {            
            $this->resourcesTeamsTitles[$resource->getId()][$resourceTeam->getTeam()->getId()] = $resourceTeam->getTitle();
        }
    }
}
