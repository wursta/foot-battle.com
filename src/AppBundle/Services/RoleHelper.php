<?php
namespace AppBundle\Services;

class RoleHelper
{
    protected $translator;
    private $roles;

    public function __construct($translator)
    {
        $this->translator = $translator;
        $this->roles = $this->getRoles();
    }

    public function getRoles()
    {
        return array("ROLE_ADMIN" => "Администратор",
                     "ROLE_USER" => "Пользователь",
                     "ROLE_BOT" => "Бот"
                    );
    }

    public function getTitle($roleId)
    {
        if(isset($this->roles[$roleId]))
            return $this->roles[$roleId];
        else
            return null;
    }
}