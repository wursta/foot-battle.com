<?php
namespace AppBundle\Services;

class TimezoneHelper
{
    /**
    * Системная временная зона
    *
    * @var \DateTimeZone
    */
    private $defaultTimezone;

    /**
    * Конструктор
    *
    * @param string $defaultTimezone Системная временная зона.
    *
    * @return TimezoneHelper
    */
    public function __construct($defaultTimezone) {
        $this->defaultTimezone = new \DateTimeZone($defaultTimezone);
    }

    /**
    * Возвращает объект системной временной зоны
    *
    * @return \DateTimeZone
    */
    public function getDefaultTimezone()
    {
        return $this->defaultTimezone;
    }
}