<?php
namespace AppBundle\Services;

use Doctrine\ORM\EntityManager;

use AppBundle\Entity\TournamentChampionship;
use AppBundle\Entity\TournamentChampionshipMatch;
use AppBundle\Entity\TournamentChampionshipResult;
use AppBundle\Entity\TournamentResult;


use AppBundle\Event\TournamentEvent;

use AppBundle\Services\PointsRuler;

class TournamentChampionshipHelper
{
    private $em;
    private $repository;    
    private $matchesRepository;
    private $rules;
    private $pointsRuler;

    public function __construct(EntityManager $em, PointsRuler $pointsRuler, $partnerProgramHelper, $eventDispatcher)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository('AppBundle\Entity\TournamentChampionship');
        $this->matchesRepository = $this->em->getRepository('AppBundle\Entity\TournamentChampionshipMatch');
        $this->resultRepository = $this->em->getRepository('AppBundle\Entity\TournamentChampionshipResult');
        $this->pointsRuler = $pointsRuler;
        $this->rules = array(
            "min_rounds_count" => 1,
            "min_matches_in_round" => 1,
            "min_max_users_count" => 2,
            "max_max_users_count" => 10000
        );        
    }

    public function getRepository()
    {
        return $this->repository;
    }

    public function getRules()
    {
        return $this->rules;
    }

    public function getRule($ruleName)
    {
        return $this->rules[$ruleName];
    }
    /*
    public function distributeMatches(\AppBundle\Entity\TournamentChampionship $tournament)
    {
        $roundsCount = $tournament->getRoundsCount();
        $matchesInRound = $tournament->getMatchesInRound();

        $matches = $tournament->getTournament()->getChampionshipMatches();        

        $distributedMatches = array();
        foreach(range(1, $roundsCount) as $round)
        {
            $distributedMatches["matches"][$round] = array();

            foreach(range(1, $matchesInRound) as $matchIndex)
            {
                $distributedMatches["matches"][$round][$matchIndex] = null;
            
                foreach($matches as $key => $match)
                {                    
                    if($match->getRound() == $round)
                    {
                        $distributedMatches["matches"][$round][$matchIndex] = $match;
                        unset($matches[$key]);
                        break;
                    }
                }
            }
        }

        $usersBets = $tournament->getTournament()->getBets();
        $distributedMatches["bets"] = array();
        foreach($usersBets as $bet)
        {
            $distributedMatches["bets"][$bet->getMatch()->getId()] = $bet;
        }

        return $distributedMatches;
    }        
    
    public function processRoundResults(\AppBundle\Entity\TournamentChampionship $tournament)
    {        
        $championshipResults = $tournament->getTournament()->getChampionshipResults();
        $users = $tournament->getTournament()->getUsers();
        $usersBets = $tournament->getTournament()->getBets();
        
        $bets = array();
        foreach($usersBets as $bet)        
            $bets[$bet->getMatch()->getId()][$bet->getUser()->getId()] = $bet;
            
        $roundResults = array_fill(1, $tournament->getRoundsCount(), array());
        foreach($championshipResults as $result)        
            $roundResults[$result->getRound()][$result->getUser()->getId()] = $result;
        
        $matches = $this->initTournamentMatchesArray($tournament->getRoundsCount(), $tournament->getMatchesInRound(), $tournament->getTournament()->getChampionshipMatches());                

        foreach($matches as $round => $matchesInRound)
        {                        
            if(!empty($roundResults[$round]))
                continue;                            
            
            if(!$this->isAllMatchesInRoundFinishedAndPointsCalculated($matchesInRound, $bets))
                continue;
                        
            foreach($users as $user)
            {
                if(isset($roundResults[$round - 1][$user->getId()]))
                    $userResInPrevRound = $roundResults[$round - 1][$user->getId()];
                else
                    $userResInPrevRound = null;
                
                $resultsForUserByRound = $this->generateResultForUserByRound($user->getId(), $matchesInRound, $bets, $userResInPrevRound);                
                
                $TournamentChampionshipResult = new TournamentChampionshipResult();
                $TournamentChampionshipResult->setTournament($tournament->getTournament());
                $TournamentChampionshipResult->setUser($user);
                $TournamentChampionshipResult->setRound($round);
                $TournamentChampionshipResult->setTotalPoints($resultsForUserByRound["total_points"]);
                $TournamentChampionshipResult->setPointsInRound($resultsForUserByRound["points_in_round"]);
                $TournamentChampionshipResult->setTotalGuessedResults($resultsForUserByRound["total_guessed_results"]);
                $TournamentChampionshipResult->setTotalGuessedOutcomes($resultsForUserByRound["total_guessed_outcomes"]);
                $TournamentChampionshipResult->setTotalGuessedDifferences($resultsForUserByRound["total_guessed_differences"]);
                
                $this->em->persist($TournamentChampionshipResult);
                $this->em->flush();
            }

            //считаем очки только для одного тура.
            break;
        }
    }

    public function processResults(\AppBundle\Entity\TournamentChampionship $tournament)
    {
        $lastRound = $tournament->getRoundsCount();
        $resultsInLastRound = $this->resultRepository->findByTournamentAndRound($tournament->getTournament()->getId(), $lastRound);

        $place = 1;
        foreach($resultsInLastRound as $result)
        {
            $TournamentResult = new TournamentResult();
            $TournamentResult->setTournament($tournament->getTournament());
            $TournamentResult->setUser($result->getUser());
            $TournamentResult->setPlace($place);
            $TournamentResult->setPoints($result->getTotalPoints());            

            $this->em->persist($TournamentResult);
            $this->em->flush();

            $place++;
        }        
    }    
    
    public function createTournament(\AppBundle\Entity\Tournament $initTournament)
    {
        $tournament = new TournamentChampionship();
        $tournament->setTournament($initTournament);

        return $tournament;
    }   
    
    public function validateStep2(\AppBundle\Entity\TournamentChampionship $tournament, $formData)
    {        
        
        if($formData["max_users_count"] < $this->getRule("min_max_users_count"))
            throw new \Exception("Максимальное кол-во участников не может быть меньше ".$this->getRule("min_max_users_count")."!");

        $tournament->setMaxUsersCount($formData["max_users_count"]);

        if($formData["rounds_count"] < $this->getRule("min_rounds_count"))
            throw new \Exception("Количество туров не может быть меньше ".$this->getRule("min_rounds_count")."!");

        if($formData["rounds_count"] < $tournament->getRoundsCount())
            throw new \Exception("Количество туров не может быть меньше уже сохранённого значения (".$tournament->getRoundsCount().")!");

        $tournament->setRoundsCount($formData["rounds_count"]);

        if($formData["matches_in_round"] < $this->getRule("min_matches_in_round"))
            throw new \Exception("Количество матчей в туре не может быть меньше ".$this->getRule("min_matches_in_round")."!");

        if($formData["matches_in_round"] < $tournament->getMatchesInRound())
            throw new \Exception("Количество матчей в туре не может быть меньше уже сохранённого значения (".$tournament->getMatchesInRound().")!");

        $tournament->setMatchesInRound($formData["matches_in_round"]);
    }

    public function validateStep3(\AppBundle\Entity\TournamentChampionship $tournament)
    {
        if(count($tournament->getTournament()->getChampionshipMatches()) < 1)
            throw new \Exception("Нельзя опубликовать турнир пока не заполнен хотя бы один матч!");
        
        if(count($this->matchesRepository->findStartedByTournament($tournament->getTournament()->getId())) > 0)        
            throw new \Exception("В турнире имеются уже начавишеся, оконченные или отменённые матчи!");
    }
    
    public function addMatch(\AppBundle\Entity\TournamentChampionship $tournament, \AppBundle\Entity\Match $match, $request)
    {        
        if(!$this->repository->matchIsUnique($tournament->getTournament()->getId(), $match->getId()))
            throw new Exception("Нельзя добавить два одинаковых матча в один турнир!");

        $matchToChangeId = $request->get('match_to_change');
        
        $tournamentMatch = new TournamentChampionshipMatch();
        if(!empty($matchToChangeId))
        {            
            $tm = $this->matchesRepository->findByTournamentAndMatch($tournament->getTournament()->getId(), $matchToChangeId);            
            if($tm)
                $tournamentMatch = $tm;
        }
                
        $round = $request->get('round');
        if(!$round)
            throw new Exception("Не указан тур!");
        
        $tournamentMatch->setRound($round);        
        $tournamentMatch->setTournament($tournament->getTournament());
        $tournamentMatch->setMatch($match);
        
        $this->em->persist($tournamentMatch);
        $this->em->flush();

        $tournament->getTournament()->addChampionshipMatch($tournamentMatch);

        
        
        $this->em->persist($tournament->getTournament());
        $this->em->flush();        
    }
    */
    
    public function isMaxUsersReached(\AppBundle\Entity\TournamentChampionship $tournament)
    {
        return (count($tournament->getTournament()->getUsers()) >= $tournament->getMaxUsersCount());
    }
    
    public function isTournamentCanStart(\AppBundle\Entity\TournamentChampionship $tournament)
    {
        return (count($tournament->getTournament()->getUsers()) >= $this->getRule('min_max_users_count'));
    }

    public function isTournamentOver(\AppBundle\Entity\TournamentChampionship $tournament)
    {
        return ($tournament->getRoundsCount() == $this->resultRepository->getMaxResultRoundByTournament($tournament->getTournament()->getId()));
    }

    public function initTournamentMatchesArray($roundsCount, $matchesInRound, $tournamentMatches)
    {
        $matches = array();
        foreach(range(1, $roundsCount) as $round)
        {
            foreach(range(1, $matchesInRound) as $matchIndex)
            {
                $matches[$round][$matchIndex] = null;
                foreach($tournamentMatches as $key => $match)
                {
                    if($match->getRound() == $round)
                    {
                        $matches[$round][$matchIndex] = $match;
                        unset($tournamentMatches[$key]);
                        break;
                    }
                }
            }
        }
        
        return $matches;
    }

    public function onTournamentMatchSaved(\AppBundle\Event\TournamentEvent $event)
    {                
        $tournament = $event->getTournament();
        
        $this->_updateTournamentStartDatetime($tournament);        
    }
    
    public function onTournamentUserAdded(\AppBundle\Event\TournamentEvent $event)
    {
        
    }

    public function onTournamentStarted(\AppBundle\Event\TournamentEvent $event)
    {
        
    }
    
    private function _updateTournamentStartDatetime(\AppBundle\Entity\TournamentChampionship $tournament)
    {        
        $minStartDatetime = $this->matchesRepository->getMinStartDatetime($tournament->getTournament()->getId());
                
        if($minStartDatetime)
        {
            $datetime = new \DateTime($minStartDatetime);
            $tournament->getTournament()->setStartDatetime($datetime);
            $this->em->persist($tournament->getTournament());
            $this->em->flush();
        }
    }

    public function generateResultForUserByRound($user_id, $matchesInRound, $bets, $userResInPrevRound)
    {
        $totalPoints = 0;
        $pointsInRound = 0;
        $totalGuessedResults = 0;
        $totalGuessedOutcomes = 0;
        $totalGuessedDifferences = 0;        
        
        if($userResInPrevRound)
        {
            $totalPoints = $userResInPrevRound->getTotalPoints();
            $totalGuessedResults = $userResInPrevRound->getTotalGuessedResults();
            $totalGuessedOutcomes = $userResInPrevRound->getTotalGuessedOutcomes();
            $totalGuessedDifferences = $userResInPrevRound->getTotalGuessedDifferences();
        }
        
        foreach($matchesInRound as $tournamentMatch)
        {
            $match = $tournamentMatch->getMatch();
            
            if(!isset($bets[$match->getId()][$user_id]))
                continue;
            
            $bet = $bets[$match->getId()][$user_id];            
            
            $pointsInRound += $bet->getPoints();
                        
            if($this->pointsRuler->scoreGuessed($bet, $match))
            {
                $totalGuessedResults++;
                $totalGuessedDifferences++;
                $totalGuessedOutcomes++;

            }
            elseif($this->pointsRuler->scoreDifferenceGuessed($bet, $match))            
            {
                $totalGuessedDifferences++;
                $totalGuessedOutcomes++;
            }
            elseif($this->pointsRuler->matchOutcomeGuessed($bet, $match))            
            {
                $totalGuessedOutcomes++;
            }
            
        }
        
        $totalPoints += $pointsInRound;
        
        return array("total_points" => $totalPoints,
                     "points_in_round" => $pointsInRound,
                     "total_guessed_results" => $totalGuessedResults,
                     "total_guessed_outcomes" => $totalGuessedOutcomes,
                     "total_guessed_differences" => $totalGuessedDifferences);
    }        
    
    private function isAllMatchesInRoundFinishedAndPointsCalculated($matchesInRound, $bets)
    {        
        foreach($matchesInRound as $match)
        {
            if(!$match)
                return false;
            
            if(!$match->getMatch()->getIsStarted())
                return false;
                
            if(!$match->getMatch()->getIsOver() && !$match->getMatch()->getIsRejected())
                return false;
                
            //если ниодин пользователь не сделал ставку на данный матч, то считаем что все ставки сделаны.
            if(!isset($bets[$match->getMatch()->getId()]))
                return true;
            
            //если ставки всё таки есть, то нужно проверить выставились ли за них очки
            foreach($bets[$match->getMatch()->getId()] as $bet)
            {
                if(!is_numeric($bet->getPoints()))
                    return false;
            }
        }
        
        return true;
    }
}
