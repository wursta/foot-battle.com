<?php
namespace AppBundle\Services;

use Doctrine\ORM\EntityManager;

use AppBundle\Entity\TournamentChampionsleague;
use AppBundle\Entity\TournamentChampionsleagueMatch;
use AppBundle\Entity\TournamentChampionsleagueQualificationResult;
use AppBundle\Entity\TournamentChampionsleagueGroupGame;
use AppBundle\Entity\TournamentChampionsleagueGroupResult;
use AppBundle\Entity\TournamentChampionsleaguePlayoffGame;
use AppBundle\Entity\TournamentResult;

use AppBundle\Event\TournamentEvent;

use AppBundle\Services\BalanceHelper;
use AppBundle\Services\PointsRuler;
use AppBundle\Services\TournamentsHelper;
use AppBundle\Services\TournamentRoundHelper;
use AppBundle\Services\TournamentChampionsipHelper;

class TournamentChampionsleagueHelper implements Interfaces\Tournament
{
    private $em;
    private $repository;    
    private $matchesRepository;
    private $groupGamesRepository;
    private $playoffGamesRepository;
    private $qualificationResultsRepository;
    private $groupResultsRepository;    
    private $pointsRuler;
    private $tournamentRoundHelper;
    private $tournamentChampionshipHelper;
    private $rules;

    const POINTS_FOR_WIN = 3;
    const POINTS_FOR_LOSS = 0;
    const POINTS_FOR_DRAW = 1;

    public function __construct(EntityManager $em, PointsRuler $pointsRuler, $partnerProgramHelper, $eventDispatcher)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository('AppBundle\Entity\TournamentChampionsleague');        
        $this->matchesRepository = $this->em->getRepository('AppBundle\Entity\TournamentChampionsleagueMatch');                
        $this->qualificationResultsRepository = $this->em->getRepository('AppBundle\Entity\TournamentChampionsleagueQualificationResult');        
        $this->groupGamesRepository = $this->em->getRepository('AppBundle\Entity\TournamentChampionsleagueGroupGame');        
        $this->groupResultsRepository = $this->em->getRepository('AppBundle\Entity\TournamentChampionsleagueGroupResult');        
        $this->playoffGamesRepository = $this->em->getRepository('AppBundle\Entity\TournamentChampionsleaguePlayoffGame');
        $this->pointsRuler = $pointsRuler;
        $balanceHelper = new BalanceHelper($this->em, $partnerProgramHelper, $eventDispatcher);
        $this->tournamentsHelper = new TournamentsHelper($this->em, $balanceHelper);
        $this->tournamentRoundHelper = new TournamentRoundHelper($this->em, $this->pointsRuler, $partnerProgramHelper, $eventDispatcher);
        $this->tournamentChampionshipHelper = new TournamentChampionshipHelper($this->em, $this->pointsRuler, $partnerProgramHelper, $eventDispatcher);
        $this->rules = array(
            "min_qualification_rounds_count" => 1,
            "min_matches_in_qualification" => 1,
            "min_matches_in_game" => 1,
            "min_matches_in_playoff" => 1,
            "min_max_users_count" => 4,
            "max_max_users_count" => 256,
            "min_max_groups_count" => 1,
            "max_max_groups_count" => 64,
            "min_users_out_of_group" => 2
        );        
    }

    public function getRepository()
    {
        return $this->repository;
    }

    public function getRules()
    {
        return $this->rules;
    }

    public function getRule($ruleName)
    {
        return $this->rules[$ruleName];
    }
    
    public function createTournament(\AppBundle\Entity\Tournament $initTournament)
    {
        $tournament = new TournamentChampionsleague();
        $tournament->setTournament($initTournament);
        $tournament->setStage(TournamentChampionsleague::STAGE_QUALIFICATION);

        return $tournament;
    }

    public function validateStep2($tournament, $formData)
    {
        if($formData["max_users_count"] < $this->getRule("min_max_users_count"))
            throw new \Exception("Максимальное кол-во участников не может быть меньше ".$this->getRule("min_max_users_count")."!");

        if($formData["max_users_count"] % 2 == 1)
            throw new \Exception("Максимальное кол-во участников в данном типе турнира не может быть нечётным числом!");

        
        if(count($tournament->getTournament()->getChampionsleagueMatches()) > 0)
        {
            if($formData["max_users_count"] < $tournament->getMaxUsersCount())
                throw new \Exception("Максимальное кол-во участников не может быть меньше ".$tournament->getMaxUsersCount()."!");
        }        

        $tournament->setMaxUsersCount($formData["max_users_count"]);

        if($formData["qualification_rounds_count"] < $this->getRule("min_qualification_rounds_count"))
            throw new \Exception("Количество туров в квалификации не может быть меньше ".$this->getRule("min_qualification_rounds_count")."!");

        if($formData["qualification_rounds_count"] < $tournament->getQualificationRoundsCount())
            throw new \Exception("Количество туров в квалификации не может быть меньше ".$tournament->getQualificationRoundsCount()."!");

        $tournament->setQualificationRoundsCount($formData["qualification_rounds_count"]);

        if($formData["matches_in_qualification"] < $tournament->getMatchesInQualification())
            throw new \Exception("Количество матчей в квалификации не может быть меньше ".$tournament->getMatchesInQualification()."!");
            
        $tournament->setMatchesInQualification($formData["matches_in_qualification"]);

        if($formData["max_groups_count"] < $this->getRule("min_max_groups_count"))
            throw new \Exception("Количество групп не может быть меньше ".$this->getRule("min_max_groups_count")."!");

        if($formData["max_groups_count"] < $tournament->getMaxGroupsCount())
            throw new \Exception("Количество групп не может быть меньше ".$tournament->getMaxGroupsCount()."!");

        if($formData["max_groups_count"] > $this->getRule("max_max_users_count"))
            throw new \Exception("Количество групп не может быть больше, чем кол-во участников!");

        if(count($tournament->getTournament()->getChampionsleagueMatches()) > 0)
        {
            if($formData["max_groups_count"] < $tournament->getMaxUsersCount())
                throw new \Exception("Количество групп не может быть больше, чем кол-во участников!");
        }

        $tournament->setMaxGroupsCount($formData["max_groups_count"]);

        if($formData["users_out_of_group"] < $this->getRule("min_users_out_of_group"))
            throw new \Exception("Количество вышедших из группы не может быть меньше ".$this->getRule("min_users_out_of_group")."!");

        if($formData["users_out_of_group"] < $tournament->getUsersOutOfGroup())
            throw new \Exception("Количество вышедших из группы не может быть меньше ".$tournament->getUsersOutOfGroup()."!");

        $playoffRoundsCount = log($tournament->getMaxGroupsCount()*$formData["users_out_of_group"], 2);        
        if(!ctype_digit(strval($playoffRoundsCount)))
            throw new \Exception("Недопустимое количество вышедших из группы!");

        $usersInGroup = $tournament->getMaxUsersCount()/$tournament->getMaxGroupsCount();
        if($formData["users_out_of_group"] > $usersInGroup)
            throw new \Exception("Количество вышедших из группы не может быть больше кол-ва участников в одной группе (".$usersInGroup.")!");

        $tournament->setUsersOutOfGroup($formData["users_out_of_group"]);

        if($formData["matches_in_game"] < $this->getRule("min_matches_in_game"))
            throw new \Exception("Количество матчей в групповом туре не может быть меньше ".$this->getRule("min_matches_in_game")."!");
            
        if($formData["matches_in_game"] < $tournament->getMatchesInGame())
            throw new \Exception("Количество матчей в групповом туре не может быть меньше ".$tournament->getMatchesInGame()."!");
            
        $tournament->setMatchesInGame($formData["matches_in_game"]);                

        if($formData["matches_in_playoff"] < $this->getRule("min_matches_in_playoff"))
            throw new \Exception("Количество матчей в туре плей-офф не может быть меньше ".$this->getRule("min_matches_in_playoff")."!");
            
        if($formData["matches_in_playoff"] < $tournament->getMatchesInPlayoff())
            throw new \Exception("Количество матчей в туре плей-офф не может быть меньше ".$tournament->getMatchesInPlayoff()."!");
            
        $tournament->setMatchesInPlayoff($formData["matches_in_playoff"]);
    }

    public function validateStep3($tournament)
    {        
        if(count($tournament->getTournament()->getChampionsleagueMatches()) < 1)
            throw new \Exception("Нельзя опубликовать турнир пока не заполнен хотя бы один матч!");
        
        if(count($this->matchesRepository->findStartedByTournament($tournament->getTournament()->getId())) > 0)
            throw new \Exception("В турнире имеются уже начавишеся, оконченные или отменённые матчи!");        
    }        

    public function distributeMatches($tournament)
    {
        $qualificationRoundsCount = $tournament->getQualificationRoundsCount();
        $matchesInQualification = $tournament->getMatchesInQualification();

        $distributedMatches = array();

        $distributedMatches["qualification_matches"] = array_fill(1, $qualificationRoundsCount, array_fill(1, $matchesInQualification, null));

        $tournamentMatches = $tournament->getTournament()->getChampionsleagueMatches();        

        foreach($distributedMatches["qualification_matches"] as $roundNum => &$roundMatches)
        {
            foreach($roundMatches as &$roundMatch)
            {
                foreach($tournamentMatches as $key => &$match)
                {
                    if(!$match->getIsQualificationMatch())
                        continue;

                    if($match->getRound() == $roundNum)
                    {
                        $roundMatch = $match;
                        unset($tournamentMatches[$key]);
                        break;
                    }
                }
            }
        }

        $usersInGroupCount = $tournament->getMaxUsersCount()/$tournament->getMaxGroupsCount();
        
        if($tournament->getTournament()->getIsStarted())
            $usersInGroupCount = $tournament->getRealUsersInGroup();
        
        $groupGames = $this->tournamentRoundHelper->generateRoundRobinPairings($usersInGroupCount, "user_left", "user_right");
        $mathesInGroupGame = $tournament->getMatchesInGame();
        $distributedMatches["group_matches"] = array_fill(1, count($groupGames), array_fill(1, $mathesInGroupGame, null));
        
        foreach($distributedMatches["group_matches"] as $roundNum => &$roundMatches)
        {
            foreach($roundMatches as &$roundMatch)
            {
                foreach($tournamentMatches as $key => &$match)
                {
                    if(!$match->getIsGroupMatch())
                        continue;

                    if($match->getRound() == $roundNum)
                    {
                        $roundMatch = $match;
                        unset($tournamentMatches[$key]);
                        break;
                    }
                }
            }
        }
        
        $usersInPlayoffCount = $tournament->getMaxGroupsCount()*$tournament->getUsersOutOfGroup();        

        $playoffRoundsCount = round(log($usersInPlayoffCount, 2));

         if($tournament->getTournament()->getIsStarted())
            $playoffRoundsCount = $tournament->getRealPlayoffRoundsCount();

        $matchesInPlayoffGame = $tournament->getMatchesInPlayoff();
        $playoffMatchesInRound = array_fill(1, $matchesInPlayoffGame, null);
        
        $distributedMatches["playoff_matches"] = array_fill(1, $playoffRoundsCount, $playoffMatchesInRound);

        foreach($distributedMatches["playoff_matches"] as $roundNum => &$roundMatches)
        {
            foreach($roundMatches as &$roundMatch)
            {
                foreach($tournamentMatches as $key => &$match)
                {
                    if($match->getIsQualificationMatch() || $match->getIsGroupMatch())
                        continue;

                    if($match->getRound() == $roundNum)
                    {
                        $roundMatch = $match;
                        unset($tournamentMatches[$key]);
                        break;
                    }
                }
            }
        }

        $usersBets = $tournament->getTournament()->getBets();
        $distributedMatches["bets"] = array();
        foreach($usersBets as $bet)
        {
            $distributedMatches["bets"][$bet->getMatch()->getId()] = $bet;
        }

        return $distributedMatches;
    }

    public function addMatch($tournament, \AppBundle\Entity\Match $match, $request)
    {
        if(!$this->repository->matchIsUnique($tournament->getTournament()->getId(), $match->getId()))
            throw new \Exception("Нельзя добавить два одинаковых матча в один турнир!");        

        $matchToChangeId = $request->get('match_to_change');
        
        $tournamentMatch = new TournamentChampionsleagueMatch();
        if(!empty($matchToChangeId))
        {
            $tm = $this->matchesRepository->findByTournamentAndMatch($tournament->getTournament()->getId(), $matchToChangeId);
            if($tm)
                $tournamentMatch = $tm;
        }

        $round = $request->get('round');
        if(!$round)
            throw new \Exception("Не указан тур!");

        $qualificationMatch = (bool) $request->get('qualification_match');        
        $groupMatch = (bool) $request->get('group_match');        
        
        if(!$this->matchesRepository->isMatchDatetimeMoreThanInPreviousRounds($tournament->getTournament()->getId(), $match->getMatchDate(), $match->getMatchTime(), $round, $qualificationMatch, $groupMatch))
            throw new \Exception("Данный матч начинается раньше, чем последние матчи в предыдущих турах!");
        
        if(!$this->matchesRepository->isMatchDatetimeLessThanInNextRounds($tournament->getTournament()->getId(), $match->getMatchDate(), $match->getMatchTime(), $round, $qualificationMatch, $groupMatch))
            throw new \Exception("Данный матч начинается позже, чем первые матчи в последующих турах!");
        
        $tournamentMatch->setRound($round);        
        $tournamentMatch->setTournament($tournament->getTournament());
        $tournamentMatch->setMatch($match);
        $tournamentMatch->setIsQualificationMatch($qualificationMatch);
        $tournamentMatch->setIsGroupMatch($groupMatch);
        
        $this->em->persist($tournamentMatch);
        $this->em->flush();
        
        $tournament->getTournament()->addChampionsleagueMatch($tournamentMatch);

        $this->em->persist($tournament->getTournament());
        $this->em->flush();        
    }

    public function processRoundResults(\AppBundle\Entity\TournamentChampionsleague $tournament)
    {
        if($tournament->getStage() == TournamentChampionsleague::STAGE_QUALIFICATION)
            $this->processQualificationRound($tournament);
        elseif($tournament->getStage() == TournamentChampionsleague::STAGE_GROUP)
            $this->processGroupRound($tournament);
        elseif($tournament->getStage() == TournamentChampionsleague::STAGE_PLAYOFF)
            $this->processPlayoffRound($tournament);
    }

    public function processResults(\AppBundle\Entity\TournamentChampionsleague $tournament)
    {
        return true;
    }

    public function isMaxUsersReached(\AppBundle\Entity\TournamentChampionsleague $tournament)
    {
        return (count($tournament->getTournament()->getUsers()) >= $tournament->getMaxUsersCount());
    }

    public function isTournamentCanStart(\AppBundle\Entity\TournamentChampionsleague $tournament)
    {
        return (count($tournament->getTournament()->getUsers()) >= $this->getRule('min_max_users_count'));
    }

    public function isTournamentOver(\AppBundle\Entity\TournamentChampionsleague $tournament)
    {
        if($tournament->getStage() == TournamentChampionsleague::STAGE_QUALIFICATION || $tournament->getStage() == TournamentChampionsleague::STAGE_GROUP)
            return false;
        
        if(!$tournament->getCurrentRound())
            return true;
            
        return false;        
    }

    public function onTournamentMatchSaved(\AppBundle\Event\TournamentEvent $event)
    {        
        $tournament = $event->getTournament();
        
        $this->_updateTournamentStartDatetime($tournament);
    }

    public function onTournamentUserAdded(\AppBundle\Event\TournamentEvent $event)
    {
        
    }

    public function onTournamentStarted(\AppBundle\Event\TournamentEvent $event)
    {
        $tournament = $event->getTournament();

        $realUsersCount = count($tournament->getTournament()->getUsers());

        $tournament->setCurrentRound(1);

        if($realUsersCount == $tournament->getMaxUsersCount())
        {            
            $tournament->setRealUsersOutOfQualification($tournament->getMaxUsersCount());
            $tournament->setRealUsersCount($tournament->getMaxUsersCount());
            $tournament->setRealGroupsCount($tournament->getMaxGroupsCount());
            $tournament->setRealUsersInGroup($tournament->getMaxUsersCount()/$tournament->getMaxGroupsCount());
            $tournament->setRealUsersOutOfGroup($tournament->getUsersOutOfGroup());            
        }
        else
        {
            $tournament->setRealUsersCount($realUsersCount);

            $maxUsersCountChoices = array();
            for($exp = 2; pow(2, $exp) <= $this->getRule("max_max_users_count"); $exp++)
                $maxUsersCountChoices[pow(2, $exp)] = pow(2, $exp);

            foreach($maxUsersCountChoices as $choice)
            {                                
                if($choice > $realUsersCount)
                    break;

                $realUsersOutOfQualification = $choice;
            }

            $tournament->setRealUsersOutOfQualification($realUsersOutOfQualification);
            $tournament->setRealUsersInGroup(4);
            
            $tournament->setRealGroupsCount($tournament->getRealUsersOutOfQualification()/$tournament->getRealUsersInGroup());
            $tournament->setRealUsersOutOfGroup(2);
        }

        $groupGames = $this->tournamentRoundHelper->generateRoundRobinPairings($tournament->getRealUsersInGroup(), "user_left", "user_right");
        $tournament->setRealGroupRoundsCount(count($groupGames));

        $realUsersCountInPlayoff = $tournament->getRealUsersOutOfGroup()*$tournament->getRealGroupsCount();
        $playoffRoundsCount = round(log($realUsersCountInPlayoff, 2));
        $tournament->setRealPlayoffRoundsCount($playoffRoundsCount);

        $this->matchesRepository->removeUnneededMatchesAndBets($tournament->getTournament()->getId(), count($groupGames), $playoffRoundsCount);

        $this->em->persist($tournament);
        $this->em->flush();

    }

    private function _updateTournamentStartDatetime(\AppBundle\Entity\TournamentChampionsleague $tournament)
    {        
        $minStartDatetime = $this->matchesRepository->getMinStartDatetime($tournament->getTournament()->getId());
        
        if($minStartDatetime)
        {
            $datetime = new \DateTime($minStartDatetime);
            $tournament->getTournament()->setStartDatetime($datetime);
            $this->em->persist($tournament->getTournament());
            $this->em->flush();
        }
    }

    private function processQualificationRound(\AppBundle\Entity\TournamentChampionsleague $tournament)
    {
        $usersBets = $tournament->getTournament()->getBets();
        $bets = array();
        foreach($usersBets as $bet)
            $bets[$bet->getMatch()->getId()][$bet->getUser()->getId()] = $bet;

        $currentRound = $tournament->getCurrentRound();
        $totalRounds = $tournament->getQualificationRoundsCount();

        $roundMatches = $this->matchesRepository->findByTournamentAndRound($tournament->getTournament()->getId(), $currentRound, true, false);

        if(!$this->tournamentsHelper->isMatchesFinishedAndPointsCalculated($roundMatches, $bets))
            return false;                

        $resultsInPreviousRound = $this->qualificationResultsRepository->findByTournamentAndRound($tournament->getTournament()->getId(), ($currentRound - 1));

        $users = $tournament->getTournament()->getUsers();

        foreach($users as $user)
        {
            if(isset($resultsInPreviousRound[$user->getId()]))
                $userResInPrevRound = $resultsInPreviousRound[$user->getId()];
            else
                $userResInPrevRound = null;
            
            $resultsForUserByRound = $this->tournamentChampionshipHelper->generateResultForUserByRound($user->getId(), $roundMatches, $bets, $userResInPrevRound);

            $TournamentQualificationResult = new TournamentChampionsleagueQualificationResult();                    
            $TournamentQualificationResult->setTournament($tournament->getTournament());
            $TournamentQualificationResult->setUser($user);
            $TournamentQualificationResult->setRound($currentRound);
            $TournamentQualificationResult->setTotalPoints($resultsForUserByRound["total_points"]);
            $TournamentQualificationResult->setPointsInRound($resultsForUserByRound["points_in_round"]);
            $TournamentQualificationResult->setTotalGuessedResults($resultsForUserByRound["total_guessed_results"]);
            $TournamentQualificationResult->setTotalGuessedOutcomes($resultsForUserByRound["total_guessed_outcomes"]);
            $TournamentQualificationResult->setTotalGuessedDifferences($resultsForUserByRound["total_guessed_differences"]);            
            $TournamentQualificationResult->setUserRating($user->getRating());

            $this->em->persist($TournamentQualificationResult);
            $this->em->flush();
        }

        //если тур не послдений, то увеличиваем счётчик туров
        if($currentRound != $totalRounds)
        {
            $tournament->setCurrentRound($currentRound + 1);

            $this->em->persist($tournament);
            $this->em->flush();

            return null;
        }


        //если посчитали последний тур квалификации, то генерируем пары для группового этапа        
        $this->generatePairsForGroups($tournament);

        $tournament->setStage(TournamentChampionsleague::STAGE_GROUP);
        $tournament->setCurrentRound(1);

        $this->em->persist($tournament);
        $this->em->flush();
    }

    private function generatePairsForGroups(\AppBundle\Entity\TournamentChampionsleague $tournament)
    {
        $totalRounds = $tournament->getQualificationRoundsCount();
        $resultsInLastRound = $this->qualificationResultsRepository->findByTournamentAndRound($tournament->getTournament()->getId(), $totalRounds);
        $resultsInLastRoundByGroups = array_chunk($resultsInLastRound, $tournament->getRealUsersInGroup());

        $groupsCount = $tournament->getRealGroupsCount();            

        for($i = 0; $i < $groupsCount; $i++)
        {
            $qresults = $resultsInLastRoundByGroups[$i];
                        
            $groupGames = $this->tournamentRoundHelper->generateRoundRobinPairings(count($qresults), "user_left", "user_right");
            
            foreach($groupGames as $roundNum => $roundGames)
            {
                foreach($roundGames as $game)
                {
                    $TournamentGroupGame = new TournamentChampionsleagueGroupGame();
                    $TournamentGroupGame->setTournament($tournament->getTournament());
                    $TournamentGroupGame->setGroupNum($i + 1);
                    $TournamentGroupGame->setRound($roundNum);

                    $userLeft = $qresults[$game["user_left"] - 1]->getUser();
                    $userRight = $qresults[$game["user_right"] - 1]->getUser();

                    $TournamentGroupGame->setUserLeft($userLeft);
                    $TournamentGroupGame->setUserRight($userRight);
                    
                    $this->em->persist($TournamentGroupGame);
                    $this->em->flush();
                }
            }
        }        
    }

    private function processGroupRound(\AppBundle\Entity\TournamentChampionsleague $tournament)
    {
        $usersBets = $tournament->getTournament()->getBets();
        $bets = array();
        foreach($usersBets as $bet)
            $bets[$bet->getMatch()->getId()][$bet->getUser()->getId()] = $bet;

        $currentRound = $tournament->getCurrentRound();
        $totalRounds = $tournament->getRealGroupRoundsCount();

        $roundMatches = $this->matchesRepository->findByTournamentAndRound($tournament->getTournament()->getId(), $currentRound, false, true);

        if(!$this->tournamentsHelper->isMatchesFinishedAndPointsCalculated($roundMatches, $bets))
            return null;

        $totalPointsForUsersByRound = $this->matchesRepository->findTotalPointsForUsersByRound($tournament->getTournament()->getId(), $currentRound, false, true);    
        $totalPointsByUsers = array();
        foreach($totalPointsForUsersByRound as $row)
        {
            $totalPointsByUsers[$row["user_id"]] = (float) $row["total_points"];
        }
        
        $resultsInPreviousRound = $this->groupResultsRepository->findByTournamentAndRound($tournament->getTournament()->getId(), $currentRound - 1);

        $resultsInPreviousRoundByUser = array();
        foreach($resultsInPreviousRound as $result)
            $resultsInPreviousRoundByUser[$result->getUser()->getId()] = $result;                
        
        $usersInGroups = array();
        $roundGames = $this->groupGamesRepository->findByTournamentAndRound($tournament->getTournament()->getId(), $currentRound);
        foreach($roundGames as $game)
        {
            $usersInGroups[$game->getGroupNum()][] = $game->getUserLeft();
            $usersInGroups[$game->getGroupNum()][] = $game->getUserRight();

            if(!isset($totalPointsByUsers[$game->getUserLeft()->getId()]))
                $totalPointsByUsers[$game->getUserLeft()->getId()] = 0;

            if(!isset($totalPointsByUsers[$game->getUserRight()->getId()]))
                $totalPointsByUsers[$game->getUserRight()->getId()] = 0;            

            $round = $game->getRound();
            $groupNum = $game->getGroupNum();
            $userLeftId = $game->getUserLeft()->getId();
            $userRightId = $game->getUserRight()->getId();
            
            //подводим итоги игры
            $scoreLeft = 0;
            $scoreRight = 0;
            
            if($totalPointsByUsers[$userLeftId] > $totalPointsByUsers[$userRightId])
            {
                $scoreLeft = self::POINTS_FOR_WIN;
                $scoreRight = self::POINTS_FOR_LOSS;
            }
            elseif($totalPointsByUsers[$userLeftId] < $totalPointsByUsers[$userRightId])
            {
                $scoreLeft = self::POINTS_FOR_LOSS;
                $scoreRight = self::POINTS_FOR_WIN;                
            }
            elseif($totalPointsByUsers[$userLeftId] == $totalPointsByUsers[$userRightId])
            {
                $scoreLeft = self::POINTS_FOR_DRAW;
                $scoreRight = self::POINTS_FOR_DRAW;
            }
            
            $game->setPointsLeft($totalPointsByUsers[$userLeftId]);
            $game->setPointsRight($totalPointsByUsers[$userRightId]);
            $game->setScoreLeft($scoreLeft);
            $game->setScoreRight($scoreRight);            
            
            $this->em->persist($game);
            $this->em->flush();            
        }

        foreach($usersInGroups as $groupNum => $users)
        {
            foreach($users as $user)
            {
                if(isset($resultsInPreviousRoundByUser[$user->getId()]))
                    $userResInPrevRound = $resultsInPreviousRoundByUser[$user->getId()];
                else
                    $userResInPrevRound = null;                                                

                $resultsForUserByRound = $this->tournamentRoundHelper->generateResultForUserByRound($user->getId(), $roundGames, $userResInPrevRound);                                

                $TournamentChampionsleagueGroupResult = new TournamentChampionsleagueGroupResult();
                $TournamentChampionsleagueGroupResult->setTournament($tournament->getTournament());
                $TournamentChampionsleagueGroupResult->setUser($user);
                $TournamentChampionsleagueGroupResult->setGroupNum($groupNum);
                $TournamentChampionsleagueGroupResult->setRound($round);                
                $TournamentChampionsleagueGroupResult->setTotalGames($resultsForUserByRound["total_games"]);
                $TournamentChampionsleagueGroupResult->setTotalWins($resultsForUserByRound["total_wins"]);
                $TournamentChampionsleagueGroupResult->setTotalDraws($resultsForUserByRound["total_draws"]);
                $TournamentChampionsleagueGroupResult->setTotalLoss($resultsForUserByRound["total_loss"]);
                $TournamentChampionsleagueGroupResult->setTotalGoalsAgainst($resultsForUserByRound["total_goals_against"]);
                $TournamentChampionsleagueGroupResult->setTotalGoalsScored($resultsForUserByRound["total_goals_scored"]);
                $TournamentChampionsleagueGroupResult->setTotalGoalsDifference($resultsForUserByRound["total_goals_difference"]);
                $TournamentChampionsleagueGroupResult->setTotalPoints($resultsForUserByRound["total_points"]);
                $TournamentChampionsleagueGroupResult->setUserRating($user->getRating());
                
                $this->em->persist($TournamentChampionsleagueGroupResult);
                $this->em->flush();                
            }
        }

        if($currentRound != $totalRounds)
        {
            $tournament->setCurrentRound($currentRound + 1);

            $this->em->persist($tournament);
            $this->em->flush();

            return null;
        }

        //если посчитали последний тур группового турнира, то генерируем пары для этапа плейофф
        $this->generatePairsForPlayoffFirstRound($tournament);

        $tournament->setStage(TournamentChampionsleague::STAGE_PLAYOFF);
        $tournament->setCurrentRound(1);

        $this->em->persist($tournament);
        $this->em->flush();
    }

    private function generatePairsForPlayoffFirstRound(\AppBundle\Entity\TournamentChampionsleague $tournament)
    {
        $lastGroupRound = $tournament->getRealGroupRoundsCount();
        $groupsResultsByGroups = null;
        $groupResultsInLastRound = $this->groupResultsRepository->findByTournamentAndRound($tournament->getTournament()->getId(), $lastGroupRound);
        foreach($groupResultsInLastRound as $result)
        {
            $groupsResultsByGroups[$result->getGroupNum()][] = $result;
        }

        $groupsCount = count($groupsResultsByGroups);        
        $usersOutOfGroup = $tournament->getRealUsersOutOfGroup();

        $pairs = array();
        for($groupNum = 1; $groupNum <= $groupsCount; $groupNum = $groupNum + 2)
        {
            $firstGroupNum = $groupNum;
            $secondGroupNum = $groupNum + 1;

            for($i = 0; $i < $usersOutOfGroup; $i = $i + 2)
            {
                $pairs[] = array("user_left" => $groupsResultsByGroups[$firstGroupNum][$i]->getUser(),
                                 "user_right" => $groupsResultsByGroups[$secondGroupNum][$i+1]->getUser()
                                );

                $pairs[] = array("user_left" => $groupsResultsByGroups[$firstGroupNum][$i+1]->getUser(),
                                 "user_right" => $groupsResultsByGroups[$secondGroupNum][$i]->getUser()
                                );
            }
        }

        foreach($pairs as $pair)
        {
            $TournamentPlayoffGame = new TournamentChampionsleaguePlayoffGame();
            $TournamentPlayoffGame->setTournament($tournament->getTournament());
            $TournamentPlayoffGame->setRound(1);
            $TournamentPlayoffGame->setUserLeft($pair["user_left"]);
            $TournamentPlayoffGame->setUserRight($pair["user_right"]);

            $this->em->persist($TournamentPlayoffGame);
            $this->em->flush();
        }

        $this->repository->removeOutFromGroupsUsersBets($tournament->getTournament()->getId());        
    }

    private function processPlayoffRound(\AppBundle\Entity\TournamentChampionsleague $tournament)
    {
        $usersBets = $tournament->getTournament()->getBets();
        $bets = array();
        foreach($usersBets as $bet)
            $bets[$bet->getMatch()->getId()][$bet->getUser()->getId()] = $bet;

        $totalRounds = $tournament->getRealPlayoffRoundsCount();
        $currentRound = $tournament->getCurrentRound();
        $roundMatches = $this->matchesRepository->findByTournamentAndRound($tournament->getTournament()->getId(), $currentRound, false, false);

        if(!$this->tournamentsHelper->isMatchesFinishedAndPointsCalculated($roundMatches, $bets))
            return null;
        
        $currentPlayoffRoundGames = $this->playoffGamesRepository->findByTournamentAndRound($tournament->getTournament()->getId(), $currentRound);

        $totalPointsByUsers = array();
        $totalGuessedResultsByUsers = array();
        $totalGuessedOutcomesByUsers = array();
        $totalGuessedDifferencesByUsers = array();
                        
        foreach($roundMatches as $match)
        {                
            foreach($bets[$match->getMatch()->getId()] as $userBet)
            {
                if(!isset($totalPointsByUsers[$userBet->getUser()->getId()]))
                    $totalPointsByUsers[$userBet->getUser()->getId()] = 0;
                
                if(!isset($totalGuessedResultsByUsers[$userBet->getUser()->getId()]))
                    $totalGuessedResultsByUsers[$userBet->getUser()->getId()] = 0;
                    
                if(!isset($totalGuessedOutcomesByUsers[$userBet->getUser()->getId()]))
                    $totalGuessedOutcomesByUsers[$userBet->getUser()->getId()] = 0;
                    
                if(!isset($totalGuessedDifferencesByUsers[$userBet->getUser()->getId()]))
                    $totalGuessedDifferencesByUsers[$userBet->getUser()->getId()] = 0;
                    
                $totalPointsByUsers[$userBet->getUser()->getId()] += $userBet->getPoints();
                
                if($this->pointsRuler->scoreGuessed($userBet, $match->getMatch()))
                {
                    $totalGuessedResultsByUsers[$userBet->getUser()->getId()]++;                        
                    $totalGuessedDifferencesByUsers[$userBet->getUser()->getId()]++;
                    $totalGuessedOutcomesByUsers[$userBet->getUser()->getId()]++;
                }
                elseif($this->pointsRuler->scoreDifferenceGuessed($userBet, $match->getMatch()))            
                {
                    $totalGuessedDifferencesByUsers[$userBet->getUser()->getId()]++;
                    $totalGuessedOutcomesByUsers[$userBet->getUser()->getId()]++;
                }
                elseif($this->pointsRuler->matchOutcomeGuessed($userBet, $match->getMatch()))            
                {
                    $totalGuessedOutcomesByUsers[$userBet->getUser()->getId()]++;
                }
            }
        }

        $pair = 1;
            
        $winners = array();            
        $loosers = array();
        foreach($currentPlayoffRoundGames as $game)
        {
            if(!isset($totalPointsByUsers[$game->getUserLeft()->getId()]))
                $totalPointsByUsers[$game->getUserLeft()->getId()] = 0;
            if(!isset($totalPointsByUsers[$game->getUserRight()->getId()]))
                $totalPointsByUsers[$game->getUserRight()->getId()] = 0;
                
            if(!isset($totalGuessedResultsByUsers[$game->getUserLeft()->getId()]))
                $totalGuessedResultsByUsers[$game->getUserLeft()->getId()] = 0;
            if(!isset($totalGuessedResultsByUsers[$game->getUserRight()->getId()]))
                $totalGuessedResultsByUsers[$game->getUserRight()->getId()] = 0;
                
            if(!isset($totalGuessedDifferencesByUsers[$game->getUserLeft()->getId()]))
                $totalGuessedDifferencesByUsers[$game->getUserLeft()->getId()] = 0;
            if(!isset($totalGuessedDifferencesByUsers[$game->getUserRight()->getId()]))
                $totalGuessedDifferencesByUsers[$game->getUserRight()->getId()] = 0;
                
            if(!isset($totalGuessedOutcomesByUsers[$game->getUserLeft()->getId()]))
                $totalGuessedOutcomesByUsers[$game->getUserLeft()->getId()] = 0;
            if(!isset($totalGuessedOutcomesByUsers[$game->getUserRight()->getId()]))
                $totalGuessedOutcomesByUsers[$game->getUserRight()->getId()] = 0;
            
            $game->setPointsLeft($totalPointsByUsers[$game->getUserLeft()->getId()]);
            $game->setPointsRight($totalPointsByUsers[$game->getUserRight()->getId()]);
            
            $game->setTotalGuessedResultsLeft($totalGuessedResultsByUsers[$game->getUserLeft()->getId()]);
            $game->setTotalGuessedResultsRight($totalGuessedResultsByUsers[$game->getUserRight()->getId()]);
            
            $game->setTotalGuessedDifferencesLeft($totalGuessedDifferencesByUsers[$game->getUserLeft()->getId()]);
            $game->setTotalGuessedDifferencesRight($totalGuessedDifferencesByUsers[$game->getUserRight()->getId()]);
            
            $game->setTotalGuessedOutcomesLeft($totalGuessedOutcomesByUsers[$game->getUserLeft()->getId()]);
            $game->setTotalGuessedOutcomesRight($totalGuessedOutcomesByUsers[$game->getUserRight()->getId()]);

            $game->setUserRatingLeft($game->getUserLeft()->getRating());
            $game->setUserRatingRight($game->getUserRight()->getRating());
            
            $this->em->persist($game);
            $this->em->flush();
                            
            if($game->getPointsLeft() > $game->getPointsRight())
            {
                $winners[] = $game->getUserLeft();
                $loosers[] = $game->getUserRight();
            }
            elseif($game->getPointsLeft() < $game->getPointsRight())
            {
                $winners[] = $game->getUserRight();
                $loosers[] = $game->getUserLeft();
            }
            elseif($game->getTotalGuessedResultsLeft() > $game->getTotalGuessedResultsRight())
            {
                $winners[] = $game->getUserLeft();
                $loosers[] = $game->getUserRight();
            }
            elseif($game->getTotalGuessedResultsLeft() < $game->getTotalGuessedResultsRight())
            {
                $winners[] = $game->getUserRight();
                $loosers[] = $game->getUserLeft();
            }
            elseif($game->setTotalGuessedDifferencesLeft() > $game->setTotalGuessedDifferencesRight())
            {
                $winners[] = $game->getUserLeft();
                $loosers[] = $game->getUserRight();
            }
            elseif($game->setTotalGuessedDifferencesLeft() < $game->setTotalGuessedDifferencesRight())
            {
                $winners[] = $game->getUserRight();
                $loosers[] = $game->getUserLeft();
            }
            elseif($game->setTotalGuessedOutcomesLeft() > $game->setTotalGuessedOutcomesRight())
            {
                $winners[] = $game->getUserLeft();
                $loosers[] = $game->getUserRight();
            }
            elseif($game->setTotalGuessedOutcomesLeft() < $game->setTotalGuessedOutcomesRight())
            {
                $winners[] = $game->getUserRight();
                $loosers[] = $game->getUserLeft();
            }
            elseif($game->getUserRatingLeft() > $game->getUserRatingRight())
            {
                $winners[] = $game->getUserLeft();
                $loosers[] = $game->getUserRight();
            }
            elseif($game->getUserRatingLeft() < $game->getUserRatingRight())
            {
                $winners[] = $game->getUserRight();
                $loosers[] = $game->getUserLeft();
            }
            elseif($game->getUserLeft()->getId() > $game->getUserRight()->getId())
            {
                $winners[] = $game->getUserLeft();
                $loosers[] = $game->getUserRight();
            }
            elseif($game->getUserLeft()->getId() < $game->getUserRight()->getId())
            {
                $winners[] = $game->getUserRight();
                $loosers[] = $game->getUserLeft();
            }
            
            //если не последний тур, то нужно сформировать пару победителей
            if($totalRounds != $currentRound)
            {
                if($pair % 2 ==  0)
                {
                    //create new pair
                    $TournamentPlayoffGame = new TournamentChampionsleaguePlayoffGame();
                    $TournamentPlayoffGame->setTournament($tournament->getTournament());
                    $TournamentPlayoffGame->setRound($game->getRound() + 1);
                    $TournamentPlayoffGame->setUserLeft($winners[0]);
                    $TournamentPlayoffGame->setUserRight($winners[1]);                    

                    $this->em->persist($TournamentPlayoffGame);
                    $this->em->flush();
                    
                    $tournament->setCurrentRound($game->getRound() + 1);
                    $this->em->persist($tournament);
                    $this->em->flush();
                    
                    //сбрасываем для генерации новой пары
                    $winners = array();
                    $loosers = array();
                    $pair = 1;
                    continue;
                }
                
                $pair++;                                
            }
            //в противном случае записать победителей в базу
            else
            {
                $TournamentResult = new TournamentResult();
                $TournamentResult->setTournament($tournament->getTournament());
                $TournamentResult->setUser($winners[0]);
                $TournamentResult->setPlace(1);
                $TournamentResult->setPoints(0);
                
                $this->em->persist($TournamentResult);
                $this->em->flush();
                
                $TournamentResult = new TournamentResult();
                $TournamentResult->setTournament($tournament->getTournament());
                $TournamentResult->setUser($loosers[0]);
                $TournamentResult->setPlace(2);
                $TournamentResult->setPoints(0);
                
                $this->em->persist($TournamentResult);
                $this->em->flush();
                
                $tournament->setCurrentRound(null);
                $this->em->persist($tournament);
                $this->em->flush();
                
            }
        }

        $this->repository->removeOutFromPlayoffUsersBets($tournament->getTournament()->getId(), $currentRound + 1);
    }
}