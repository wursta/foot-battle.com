<?php
namespace AppBundle\Services;

use Doctrine\ORM\EntityManager;

class TournamentFactory
{        
    public static function getService($format, $em, $pointsRuler, $partnerProgramHelper, $eventDispatcher)
    {        
        $className = "AppBundle\Services\Tournament".ucfirst($format)."Helper";
                
        $refClass = new \ReflectionClass($className);
        $object = $refClass->newInstance($em, $pointsRuler, $partnerProgramHelper, $eventDispatcher);
        
        return $object;
    }    
    
    public static function getRepository($format, $doctrine)
    {
        $repositoryName = "AppBundle\Entity\Tournament".ucfirst($format);        
        $object = $doctrine->getRepository($repositoryName);
        
        return $object;
    }
}