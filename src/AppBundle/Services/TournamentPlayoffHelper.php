<?php
namespace AppBundle\Services;

use Doctrine\ORM\EntityManager;

use AppBundle\Entity\TournamentPlayoff;
use AppBundle\Entity\TournamentPlayoffMatch;
use AppBundle\Entity\TournamentPlayoffGame;
use AppBundle\Entity\TournamentPlayoffQualificationResult;
use AppBundle\Entity\TournamentResult;

use AppBundle\Event\TournamentEvent;

use AppBundle\Services\PointsRuler;

class TournamentPlayoffHelper
{    
    private $em;
    private $repository;    
    private $matchesRepository;
    private $gamesRepository;
    private $qualificationResultsRepository;
    private $pointsRuler;
    private $rules;        

    const STAGE_QUALIFICATION = "QUALIFICATION";
    const STAGE_PLAYOFF = "PLAYOFF";

    public function __construct(EntityManager $em, PointsRuler $pointsRuler, $partnerProgramHelper, $eventDispatcher)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository('AppBundle\Entity\TournamentPlayoff');        
        $this->matchesRepository = $this->em->getRepository('AppBundle\Entity\TournamentPlayoffMatch');        
        $this->gamesRepository = $this->em->getRepository('AppBundle\Entity\TournamentPlayoffGame');
        $this->qualificationResultsRepository = $this->em->getRepository('AppBundle\Entity\TournamentPlayoffQualificationResult');        
        $this->pointsRuler = $pointsRuler;
        $this->rules = array(
            "min_qualification_rounds_count" => 1,
            "min_matches_in_qualification" => 1,
            "min_matches_in_game" => 1,
            "min_max_users_count" => 2,
            "max_max_users_count" => 64
        );        
    }

    public function getRepository()
    {
        return $this->repository;
    }

    public function getRules()
    {
        return $this->rules;
    }

    public function getRule($ruleName)
    {
        return $this->rules[$ruleName];
    }
    
    public function createTournament(\AppBundle\Entity\Tournament $initTournament)
    {
        $tournament = new TournamentPlayoff();
        $tournament->setTournament($initTournament);
        $tournament->setStage('QUALIFICATION');

        return $tournament;
    }
    
    public function validateStep2(\AppBundle\Entity\TournamentPlayoff $tournament, $formData)
    {
        if($formData["max_users_count"] < $this->getRule("min_max_users_count"))
            throw new \Exception("Максимальное кол-во участников не может быть меньше ".$this->getRule("min_max_users_count")."!");
            
        if($formData["max_users_count"] % 2 == 1)
            throw new \Exception("Максимальное кол-во участников в данном типе турнира не может быть нечётным числом!");
        
        if(count($tournament->getTournament()->getPlayoffMatches()) > 0)
        {
            if($formData["max_users_count"] < $tournament->getMaxUsersCount())
                throw new \Exception("Максимальное кол-во участников не может быть меньше ".$tournament->getMaxUsersCount()."!");
        }
        
        $tournament->setMaxUsersCount($formData["max_users_count"]);                                            
        
        if($formData["qualification_rounds_count"] < $this->getRule("min_qualification_rounds_count"))
            throw new \Exception("Количество туров в квалификации не может быть меньше ".$this->getRule("min_qualification_rounds_count")."!");                    

        if($formData["qualification_rounds_count"] < $tournament->getQualificationRoundsCount())
            throw new \Exception("Количество туров в квалификации не может быть меньше ".$tournament->getQualificationRoundsCount()."!");
        
        $tournament->setQualificationRoundsCount($formData["qualification_rounds_count"]);

        if($formData["matches_in_qualification"] < $tournament->getMatchesInQualification())
            throw new \Exception("Количество матчей в квалификации не может быть меньше ".$tournament->getMatchesInQualification()."!");
            
        $tournament->setMatchesInQualification($formData["matches_in_qualification"]);
        
        if($formData["matches_in_game"] < $this->getRule("min_matches_in_game"))
            throw new \Exception("Количество матчей в игре не может быть меньше ".$this->getRule("min_matches_in_game")."!");
            
        if($formData["matches_in_game"] < $tournament->getMatchesInGame())
            throw new \Exception("Количество матчей в игре не может быть меньше ".$tournament->getMatchesInGame()."!");
            
        $tournament->setMatchesInGame($formData["matches_in_game"]);                
    }

    public function validateStep3(\AppBundle\Entity\TournamentPlayoff $tournament)
    {
        if(count($tournament->getTournament()->getPlayoffMatches()) < 1)
            throw new \Exception("Нельзя опубликовать турнир пока не заполнен хотя бы один матч!");
        
        if(count($this->matchesRepository->findStartedByTournament($tournament->getTournament()->getId())) > 0)
            throw new \Exception("В турнире имеются уже начавишеся, оконченные или отменённые матчи!");
    }        
    
    public function distributeMatches(\AppBundle\Entity\TournamentPlayoff $tournament)
    {
        $matchesInPlayoffGame = $tournament->getMatchesInGame();
        $qualificationRoundsCount = $tournament->getQualificationRoundsCount();
        $matchesInQualification = $tournament->getMatchesInQualification();
        $maxUsersCount = $tournament->getMaxUsersCount();

        $distributedMatches = array();

        $distributedMatches["qualification_games"] = array_fill(1, $qualificationRoundsCount, array_fill(1, $matchesInQualification, null));

        $tournamentMatches = $tournament->getTournament()->getPlayoffMatches();        

        foreach($distributedMatches["qualification_games"] as $roundNum => &$roundMatches)
        {
            foreach($roundMatches as &$roundMatch)
            {
                foreach($tournamentMatches as $key => &$match)
                {
                    if(!$match->getIsQualificationMatch())
                        continue;

                    if($match->getRound() == $roundNum)
                    {
                        $roundMatch = $match;
                        unset($tournamentMatches[$key]);
                        break;
                    }
                }
            }
        }

        $usersCount = $maxUsersCount;        

        $playoffRoundsCount = round(log($usersCount, 2));

        if($tournament->getStage() == self::STAGE_PLAYOFF)
            $playoffRoundsCount = $tournament->getPlayoffRoundsCount();

        $playoffMatchesInRound = array_fill(1, $matchesInPlayoffGame, null);
        $distributedMatches["playoff_games"] = array_fill(1, $playoffRoundsCount, $playoffMatchesInRound);        

        foreach($distributedMatches["playoff_games"] as $roundNum => &$roundMatches)
        {
            foreach($roundMatches as &$roundMatch)
            {
                foreach($tournamentMatches as $key => &$match)
                {
                    if($match->getIsQualificationMatch())
                        continue;

                    if($match->getRound() == $roundNum)
                    {
                        $roundMatch = $match;
                        unset($tournamentMatches[$key]);
                        break;
                    }
                }
            }
        }

        $usersBets = $tournament->getTournament()->getBets();
        $distributedMatches["bets"] = array();
        foreach($usersBets as $bet)
        {
            $distributedMatches["bets"][$bet->getMatch()->getId()] = $bet;
        }

        return $distributedMatches;
    }

    public function addMatch(\AppBundle\Entity\TournamentPlayoff $tournament, \AppBundle\Entity\Match $match, $request)
    {        
        if(!$this->repository->matchIsUnique($tournament->getTournament()->getId(), $match->getId()))
            throw new \Exception("Нельзя добавить два одинаковых матча в один турнир!");        

        $matchToChangeId = $request->get('match_to_change');
        
        $tournamentMatch = new TournamentPlayoffMatch();
        if(!empty($matchToChangeId))
        {            
            $tm = $this->matchesRepository->findByTournamentAndMatch($tournament->getTournament()->getId(), $matchToChangeId);            
            if($tm)
                $tournamentMatch = $tm;
        }    

        $round = $request->get('round');
        if(!$round)
            throw new \Exception("Не указан тур!");

        $qualificationMatch = (bool) $request->get('qualification_match');        
        
        if(!$this->matchesRepository->isMatchDatetimeMoreThanInPreviousRounds($tournament->getTournament()->getId(), $match->getMatchDate(), $match->getMatchTime(), $round, $qualificationMatch))
            throw new \Exception("Данный матч начинается раньше, чем последние матчи в предыдущих турах!");

        if(!$this->matchesRepository->isMatchDatetimeLessThanInNextRounds($tournament->getTournament()->getId(), $match->getMatchDate(), $match->getMatchTime(), $round, $qualificationMatch))
            throw new \Exception("Данный матч начинается позже, чем первые матчи в последующих турах!");        
        
        $tournamentMatch->setRound($round);        
        $tournamentMatch->setTournament($tournament->getTournament());
        $tournamentMatch->setMatch($match);
        $tournamentMatch->setIsQualificationMatch($qualificationMatch);
        
        $this->em->persist($tournamentMatch);
        $this->em->flush();
        
        $tournament->getTournament()->addPlayoffMatch($tournamentMatch);

        
        
        $this->em->persist($tournament->getTournament());
        $this->em->flush();        
    }
    
    public function processRoundResults(\AppBundle\Entity\TournamentPlayoff $tournament)
    {
        $usersBets = $tournament->getTournament()->getBets();
        $bets = array();
        foreach($usersBets as $bet)
            $bets[$bet->getMatch()->getId()][$bet->getUser()->getId()] = $bet;        
            
        if($tournament->getStage() == self::STAGE_QUALIFICATION)
        {
            $roundsInQualification = $tournament->getQualificationRoundsCount();
            
            $playoffQualificationResults = $tournament->getTournament()->getPlayoffQualificationResults();
            $users = $tournament->getTournament()->getUsers();

            $roundResults = array_fill(1, $tournament->getQualificationRoundsCount(), array());
            foreach($playoffQualificationResults as $result)        
                $roundResults[$result->getRound()][$result->getUser()->getId()] = $result;

            $matches = $this->initTournamentQualificationMatchesArray($tournament->getQualificationRoundsCount(), $tournament->getMatchesInQualification(), $tournament->getTournament()->getPlayoffMatches());

            foreach($matches as $round => $matchesInRound)
            {                        
                if(!empty($roundResults[$round]))
                    continue;                            
                
                if(!$this->isAllMatchesInRoundFinishedAndPointsCalculated($matchesInRound, $bets))
                    continue;
                            
                foreach($users as $user)
                {
                    if(isset($roundResults[$round - 1][$user->getId()]))
                        $userResInPrevRound = $roundResults[$round - 1][$user->getId()];
                    else
                        $userResInPrevRound = null;
                    
                    $resultsForUserByRound = $this->generateQualificationResultForUserByRound($user->getId(), $matchesInRound, $bets, $userResInPrevRound);                
                    
                    $TournamentPlayoffQualificationResult = new TournamentPlayoffQualificationResult();                    
                    $TournamentPlayoffQualificationResult->setTournament($tournament->getTournament());
                    $TournamentPlayoffQualificationResult->setUser($user);
                    $TournamentPlayoffQualificationResult->setRound($round);
                    $TournamentPlayoffQualificationResult->setTotalPoints($resultsForUserByRound["total_points"]);
                    $TournamentPlayoffQualificationResult->setPointsInRound($resultsForUserByRound["points_in_round"]);
                    $TournamentPlayoffQualificationResult->setTotalGuessedResults($resultsForUserByRound["total_guessed_results"]);
                    $TournamentPlayoffQualificationResult->setTotalGuessedOutcomes($resultsForUserByRound["total_guessed_outcomes"]);
                    $TournamentPlayoffQualificationResult->setTotalGuessedDifferences($resultsForUserByRound["total_guessed_differences"]);
                    
                    $this->em->persist($TournamentPlayoffQualificationResult);
                    $this->em->flush();
                }
                
                if($roundsInQualification == $round)
                {
                    //распределить пары в первом раунде плей-офф
                    $resultsInLastRound = $this->qualificationResultsRepository->findByTournamentAndRound($tournament->getTournament()->getId(), $round);
                    
                    $usersCountChoices = array();                    
                    for($exp = 1; pow(2, $exp) <= $tournament->getMaxUsersCount(); $exp++)
                        $usersCountChoices[] = pow(2, $exp);                                        
                    
                    $usersCount = count($tournament->getTournament()->getUsers());
                    $usersCountInPlayoff = 0;
                    foreach($usersCountChoices as $i => $choice)
                    {
                        if($choice < $usersCount)
                            continue;
                            
                        if($choice == $usersCount)
                        {
                            $usersCountInPlayoff = $choice;
                            break;
                        }
                        
                        if($choice > $usersCount)
                        { 
                            $usersCountInPlayoff = $usersCountChoices[$i - 1];
                            break;
                        }
                    }                                        

                    $resultsInLastRound = array_slice($resultsInLastRound, 0, $usersCountInPlayoff);                                        
                    
                    $games = array();
                    for($i = 0; $i < $usersCountInPlayoff; $i = $i + 2)
                    {
                        $resultsForUserOne = array_shift($resultsInLastRound);
                        $resultsForUserTwo = array_pop($resultsInLastRound);                                                

                        $TournamentPlayoffGame = new TournamentPlayoffGame();
                        $TournamentPlayoffGame->setTournament($tournament->getTournament());
                        $TournamentPlayoffGame->setRound(1);
                        $TournamentPlayoffGame->setUserLeft($resultsForUserOne->getUser());
                        $TournamentPlayoffGame->setUserRight($resultsForUserTwo->getUser());

                        $this->em->persist($TournamentPlayoffGame);
                        $this->em->flush();

                        $tournament->setStage(self::STAGE_PLAYOFF);
                        $tournament->setCurrentPlayoffRound(1);
                        $tournament->setPlayoffRoundsCount(round(log($usersCountInPlayoff, 2)));
                        $this->em->persist($tournament);
                        $this->em->flush();
                    }

                    //удаляем ненужные раунды плейофф                    
                    $realPlayoffRoundsCount = $tournament->getPlayoffRoundsCount();                    
                    $playoffMatches = $this->matchesRepository->removeUnneededMatchesFromPlayoffByTournament($tournament->getTournament()->getId(), $realPlayoffRoundsCount);                    

                }
                
                //считаем очки только для одного тура.
                break;
            }            
        }
        elseif($tournament->getStage() == self::STAGE_PLAYOFF)
        {
            $currentPlayoffRoundGames = $this->gamesRepository->findByTournamentAndRound($tournament->getTournament()->getId(), $tournament->getCurrentPlayoffRound());
            
            $matchesInRound = $this->matchesRepository->findByTournamentAndRound($tournament->getTournament()->getId(), $tournament->getCurrentPlayoffRound(), false);
            
            if(count($matchesInRound) != $tournament->getMatchesInGame())
                return null;
                    
            if(!$this->isAllMatchesInRoundFinishedAndPointsCalculated($matchesInRound, $bets))
                return null;
            
            $totalPointsByUsers = array();
            $totalGuessedResultsByUsers = array();
            $totalGuessedOutcomesByUsers = array();
            $totalGuessedDifferencesByUsers = array();
                            
            foreach($matchesInRound as $match)
            {                
                foreach($bets[$match->getMatch()->getId()] as $userBet)
                {
                    if(!isset($totalPointsByUsers[$userBet->getUser()->getId()]))
                        $totalPointsByUsers[$userBet->getUser()->getId()] = 0;
                    
                    if(!isset($totalGuessedResultsByUsers[$userBet->getUser()->getId()]))
                        $totalGuessedResultsByUsers[$userBet->getUser()->getId()] = 0;
                        
                    if(!isset($totalGuessedOutcomesByUsers[$userBet->getUser()->getId()]))
                        $totalGuessedOutcomesByUsers[$userBet->getUser()->getId()] = 0;
                        
                    if(!isset($totalGuessedDifferencesByUsers[$userBet->getUser()->getId()]))
                        $totalGuessedDifferencesByUsers[$userBet->getUser()->getId()] = 0;
                        
                    $totalPointsByUsers[$userBet->getUser()->getId()] += $userBet->getPoints();
                    
                    if($this->pointsRuler->scoreGuessed($userBet, $match->getMatch()))
                    {
                        $totalGuessedResultsByUsers[$userBet->getUser()->getId()]++;                        
                        $totalGuessedDifferencesByUsers[$userBet->getUser()->getId()]++;
                        $totalGuessedOutcomesByUsers[$userBet->getUser()->getId()]++;
                    }
                    elseif($this->pointsRuler->scoreDifferenceGuessed($userBet, $match->getMatch()))            
                    {
                        $totalGuessedDifferencesByUsers[$userBet->getUser()->getId()]++;
                        $totalGuessedOutcomesByUsers[$userBet->getUser()->getId()]++;
                    }
                    elseif($this->pointsRuler->matchOutcomeGuessed($userBet, $match->getMatch()))            
                    {
                        $totalGuessedOutcomesByUsers[$userBet->getUser()->getId()]++;
                    }
                }
            }
            
            $pair = 1;
            
            $winners = array();            
            $loosers = array();
            foreach($currentPlayoffRoundGames as $game)
            {
                if(!isset($totalPointsByUsers[$game->getUserLeft()->getId()]))
                    $totalPointsByUsers[$game->getUserLeft()->getId()] = 0;
                if(!isset($totalPointsByUsers[$game->getUserRight()->getId()]))
                    $totalPointsByUsers[$game->getUserRight()->getId()] = 0;
                    
                if(!isset($totalGuessedResultsByUsers[$game->getUserLeft()->getId()]))
                    $totalGuessedResultsByUsers[$game->getUserLeft()->getId()] = 0;
                if(!isset($totalGuessedResultsByUsers[$game->getUserRight()->getId()]))
                    $totalGuessedResultsByUsers[$game->getUserRight()->getId()] = 0;
                    
                if(!isset($totalGuessedDifferencesByUsers[$game->getUserLeft()->getId()]))
                    $totalGuessedDifferencesByUsers[$game->getUserLeft()->getId()] = 0;
                if(!isset($totalGuessedDifferencesByUsers[$game->getUserRight()->getId()]))
                    $totalGuessedDifferencesByUsers[$game->getUserRight()->getId()] = 0;
                    
                if(!isset($totalGuessedOutcomesByUsers[$game->getUserLeft()->getId()]))
                    $totalGuessedOutcomesByUsers[$game->getUserLeft()->getId()] = 0;
                if(!isset($totalGuessedOutcomesByUsers[$game->getUserRight()->getId()]))
                    $totalGuessedOutcomesByUsers[$game->getUserRight()->getId()] = 0;
                
                $game->setPointsLeft($totalPointsByUsers[$game->getUserLeft()->getId()]);
                $game->setPointsRight($totalPointsByUsers[$game->getUserRight()->getId()]);
                
                $game->setTotalGuessedResultsLeft($totalGuessedResultsByUsers[$game->getUserLeft()->getId()]);
                $game->setTotalGuessedResultsRight($totalGuessedResultsByUsers[$game->getUserRight()->getId()]);
                
                $game->setTotalGuessedDifferencesLeft($totalGuessedDifferencesByUsers[$game->getUserLeft()->getId()]);
                $game->setTotalGuessedDifferencesRight($totalGuessedDifferencesByUsers[$game->getUserRight()->getId()]);
                
                $game->setTotalGuessedOutcomesLeft($totalGuessedOutcomesByUsers[$game->getUserLeft()->getId()]);
                $game->setTotalGuessedOutcomesRight($totalGuessedOutcomesByUsers[$game->getUserRight()->getId()]);
                
                $this->em->persist($game);
                $this->em->flush();
                                
                if($game->getPointsLeft() > $game->getPointsRight())
                {
                    $winners[] = $game->getUserLeft();
                    $loosers[] = $game->getUserRight();
                }
                elseif($game->getPointsLeft() < $game->getPointsRight())
                {
                    $winners[] = $game->getUserRight();
                    $loosers[] = $game->getUserLeft();
                }
                elseif($game->getTotalGuessedResultsLeft() > $game->getTotalGuessedResultsRight())
                {
                    $winners[] = $game->getUserLeft();
                    $loosers[] = $game->getUserRight();
                }
                elseif($game->getTotalGuessedResultsLeft() < $game->getTotalGuessedResultsRight())
                {
                    $winners[] = $game->getUserRight();
                    $loosers[] = $game->getUserLeft();
                }
                elseif($game->setTotalGuessedDifferencesLeft() > $game->setTotalGuessedDifferencesRight())
                {
                    $winners[] = $game->getUserLeft();
                    $loosers[] = $game->getUserRight();
                }
                elseif($game->setTotalGuessedDifferencesLeft() < $game->setTotalGuessedDifferencesRight())
                {
                    $winners[] = $game->getUserRight();
                    $loosers[] = $game->getUserLeft();
                }
                elseif($game->setTotalGuessedOutcomesLeft() > $game->setTotalGuessedOutcomesRight())
                {
                    $winners[] = $game->getUserLeft();
                    $loosers[] = $game->getUserRight();
                }
                elseif($game->setTotalGuessedOutcomesLeft() < $game->setTotalGuessedOutcomesRight())
                {
                    $winners[] = $game->getUserRight();
                    $loosers[] = $game->getUserLeft();
                }
                
                //если не последний тур, то нужно сформировать пару победителей
                if($tournament->getPlayoffRoundsCount() != $tournament->getCurrentPlayoffRound())
                {
                    if($pair % 2 ==  0)
                    {
                        //create new pair
                        $TournamentPlayoffGame = new TournamentPlayoffGame();
                        $TournamentPlayoffGame->setTournament($tournament->getTournament());
                        $TournamentPlayoffGame->setRound($game->getRound() + 1);
                        $TournamentPlayoffGame->setUserLeft($winners[0]);
                        $TournamentPlayoffGame->setUserRight($winners[1]);

                        $this->em->persist($TournamentPlayoffGame);
                        $this->em->flush();
                        
                        $tournament->setCurrentPlayoffRound($game->getRound() + 1);
                        $this->em->persist($tournament);
                        $this->em->flush();
                        
                        //сбрасываем для генерации новой пары
                        $winners = array();
                        $loosers = array();
                        $pair = 1;
                        continue;
                    }
                    
                    $pair++;                                
                }
                //в противном случае записать победителей в базу
                else
                {
                    $TournamentResult = new TournamentResult();
                    $TournamentResult->setTournament($tournament->getTournament());
                    $TournamentResult->setUser($winners[0]);
                    $TournamentResult->setPlace(1);
                    $TournamentResult->setPoints(0);
                    
                    $this->em->persist($TournamentResult);
                    $this->em->flush();
                    
                    $TournamentResult = new TournamentResult();
                    $TournamentResult->setTournament($tournament->getTournament());
                    $TournamentResult->setUser($loosers[0]);
                    $TournamentResult->setPlace(2);
                    $TournamentResult->setPoints(0);
                    
                    $this->em->persist($TournamentResult);
                    $this->em->flush();
                    
                    $tournament->setCurrentPlayoffRound(null);
                    $this->em->persist($tournament);
                    $this->em->flush();
                    
                }
            }
                                    
        }
    }
    
    public function processResults(\AppBundle\Entity\TournamentPlayoff $tournament)
    {
        return true;
    }

    public function isMaxUsersReached(\AppBundle\Entity\TournamentPlayoff $tournament)
    {
        return (count($tournament->getTournament()->getUsers()) >= $tournament->getMaxUsersCount());
    }

    public function isTournamentCanStart(\AppBundle\Entity\TournamentPlayoff $tournament)
    {
        return (count($tournament->getTournament()->getUsers()) >= $this->getRule('min_max_users_count'));
    }

    public function isTournamentOver(\AppBundle\Entity\TournamentPlayoff $tournament)
    {
        if($tournament->getStage() == self::STAGE_QUALIFICATION)
            return false;
        
        if(!$tournament->getCurrentPlayoffRound())
            return true;
            
        return false;        
    }

    public function onTournamentMatchSaved(\AppBundle\Event\TournamentEvent $event)
    {        
        $tournament = $event->getTournament();
        
        $this->_updateTournamentStartDatetime($tournament);
    }

    public function onTournamentUserAdded(\AppBundle\Event\TournamentEvent $event)
    {
        
    }

    public function onTournamentStarted(\AppBundle\Event\TournamentEvent $event)
    {

    }

    public function initTournamentQualificationMatchesArray($roundsCount, $matchesInRound, $tournamentMatches)
    {
        $matches = array();
        foreach(range(1, $roundsCount) as $round)
        {
            foreach(range(1, $matchesInRound) as $matchIndex)
            {
                $matches[$round][$matchIndex] = null;
                foreach($tournamentMatches as $key => $match)
                {
                    if(!$match->getIsQualificationMatch())
                        continue;

                    if($match->getRound() == $round)
                    {
                        $matches[$round][$matchIndex] = $match;
                        unset($tournamentMatches[$key]);
                        break;
                    }
                }
            }
        }
        
        return $matches;
    }

    public function initTournamentPlayoffMatchesArray($roundsCount, $matchesInRound, $tournamentMatches)
    {
        $matches = array();
        foreach(range(1, $roundsCount) as $round)
        {
            foreach(range(1, $matchesInRound) as $matchIndex)
            {
                $matches[$round][$matchIndex] = null;
                foreach($tournamentMatches as $key => $match)
                {
                    if($match->getIsQualificationMatch())
                        continue;

                    if($match->getRound() == $round)
                    {
                        $matches[$round][$matchIndex] = $match;
                        unset($tournamentMatches[$key]);
                        break;
                    }
                }
            }
        }
        
        return $matches;
    }

    private function _updateTournamentStartDatetime(\AppBundle\Entity\TournamentPlayoff $tournament)
    {        
        $minStartDatetime = $this->matchesRepository->getMinStartDatetime($tournament->getTournament()->getId());
        
        if($minStartDatetime)
        {
            $datetime = new \DateTime($minStartDatetime);
            $tournament->getTournament()->setStartDatetime($datetime);
            $this->em->persist($tournament->getTournament());
            $this->em->flush();
        }
    }    

    private function isAllMatchesInRoundFinishedAndPointsCalculated($matchesInRound, $bets)
    {
        foreach($matchesInRound as $match)
        {
            if(!$match)
                return false;
            
            if(!$match->getMatch()->getIsStarted())
                return false;
                
            if(!$match->getMatch()->getIsOver() && !$match->getMatch()->getIsRejected())
                return false;
                
            //если ниодин пользователь не сделал ставку на данный матч, то считаем что все ставки сделаны.
            if(!isset($bets[$match->getMatch()->getId()]))
                return true;
            
            //если ставки всё таки есть, то нужно проверить выставились ли за них очки
            foreach($bets[$match->getMatch()->getId()] as $bet)
            {
                if(!is_numeric($bet->getPoints()))
                    return false;
            }
        }
        
        return true;
    }

    private function generateQualificationResultForUserByRound($user_id, $matchesInRound, $bets, $userResInPrevRound)
    {
        $totalPoints = 0;
        $pointsInRound = 0;
        $totalGuessedResults = 0;
        $totalGuessedOutcomes = 0;
        $totalGuessedDifferences = 0;
        
        if($userResInPrevRound)
        {
            $totalPoints = $userResInPrevRound->getTotalPoints();
            $totalGuessedResults = $userResInPrevRound->getTotalGuessedResults();
            $totalGuessedOutcomes = $userResInPrevRound->getTotalGuessedOutcomes();
            $totalGuessedDifferences = $userResInPrevRound->getTotalGuessedDifferences();
        }
        
        foreach($matchesInRound as $tournamentMatch)
        {
            $match = $tournamentMatch->getMatch();
            
            if(!isset($bets[$match->getId()][$user_id]))
                continue;
            
            $bet = $bets[$match->getId()][$user_id];            
            
            $pointsInRound += $bet->getPoints();
                        
            if($this->pointsRuler->scoreGuessed($bet, $match))            
            {
                $totalGuessedResults++;            
                $totalGuessedDifferences++;
                $totalGuessedOutcomes++;
            }
            elseif($this->pointsRuler->scoreDifferenceGuessed($bet, $match))            
            {
                $totalGuessedDifferences++;
                $totalGuessedOutcomes++;
            }
            elseif($this->pointsRuler->matchOutcomeGuessed($bet, $match))            
            {
                $totalGuessedOutcomes++;
            }
            
        }
        
        $totalPoints += $pointsInRound;
        
        return array("total_points" => $totalPoints,
                     "points_in_round" => $pointsInRound,
                     "total_guessed_results" => $totalGuessedResults,
                     "total_guessed_outcomes" => $totalGuessedOutcomes,
                     "total_guessed_differences" => $totalGuessedDifferences);                
    }        
}