<?php
namespace AppBundle\Services;

use Doctrine\ORM\EntityManager;

use AppBundle\Entity\TournamentRound;
use AppBundle\Entity\TournamentRoundMatch;
use AppBundle\Entity\TournamentRoundGame;
use AppBundle\Entity\TournamentRoundResult;
use AppBundle\Entity\TournamentResult;

use AppBundle\Event\TournamentEvent;

use AppBundle\Services\PointsRuler;

class TournamentRoundHelper
{    
    private $em;
    private $repository;    
    private $matchesRepository;
    private $gamesRepository;
    private $pointsRuler;
    private $rules;    

    const POINTS_FOR_WIN = 3;
    const POINTS_FOR_LOSS = 0;
    const POINTS_FOR_DRAW = 1;

    public function __construct(EntityManager $em, PointsRuler $pointsRuler, $partnerProgramHelper, $eventDispatcher)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository('AppBundle\Entity\TournamentRound');        
        $this->matchesRepository = $this->em->getRepository('AppBundle\Entity\TournamentRoundMatch');
        $this->gamesRepository = $this->em->getRepository('AppBundle\Entity\TournamentRoundGame');
        $this->resultRepository = $this->em->getRepository('AppBundle\Entity\TournamentRoundResult');
        $this->pointsRuler = $pointsRuler;
        $this->rules = array(
            "min_matches_in_game" => 1,
            "min_max_users_count" => 2,
            "max_max_users_count" => 20            
        );        
    }

    public function getRepository()
    {
        return $this->repository;
    }

    public function getRules()
    {
        return $this->rules;
    }

    public function getRule($ruleName)
    {
        return $this->rules[$ruleName];
    }

    public function distributeMatches(\AppBundle\Entity\TournamentRound $tournament)
    {
        if(!$tournament->getTournament()->getIsStarted())
        {
            $usersCount = $tournament->getMaxUsersCount();
            $maxUsersCount = $tournament->getMaxUsersCount();
        }
        else
        {            
            $users = $tournament->getTournament()->getUsers();            
            $usersCount = count($users);            
            $maxUsersCount = $tournament->getMaxUsersCount();
        }
                                            
        $distributedMatches = array();
        $distributedMatches["games"] = array();

        if($usersCount >= $this->getRule('min_max_users_count'))
            $distributedMatches["games"] = $this->generateRoundRobinPairings($usersCount, 'user_left', 'user_right');                
        
        if(count($tournament->getTournament()->getUsers()) >= $this->getRule('min_max_users_count'))
        {
            $tournamentGames = $tournament->getTournament()->getRoundGames();
            
            foreach($distributedMatches["games"] as $roundIndex => &$round)
            {
                foreach($round as $gameIndex => &$game)
                {
                    if(!isset($users[$game["user_left"] - 1]) || !isset($users[$game["user_right"] - 1]))
                    {
                        unset($round[$gameIndex]);
                        continue;
                    }

                    $game["user_left"] = $users[$game["user_left"] - 1];
                    $game["user_right"] = $users[$game["user_right"] - 1];
                    $game["game"] = null;

                    foreach($tournamentGames as $key => $tournamentGame)
                    {
                        if($tournamentGame->getRound() == $roundIndex && 
                           $tournamentGame->getUserLeft()->getId() == $game["user_left"]->getId() && 
                           $tournamentGame->getUserRight()->getId() == $game["user_right"]->getId()
                           )
                        {
                            $game["game"] = $tournamentGame;
                            unset($tournamentGames[$key]);
                            break;
                        }
                    }
                }
            }
        }        
        
        $matches = $tournament->getTournament()->getRoundMatches();
        $matchesInGame = $tournament->getMatchesInGame();
        $roundsCount = $maxUsersCount - 1;
                
        $distributedMatches["matches"] = array_fill(1, $roundsCount, array());                
        
        foreach($matches as $match)
        {
            if(!isset($distributedMatches["matches"][$match->getRound()]))
                continue;
            
            array_push($distributedMatches["matches"][$match->getRound()], $match);
        }

        if(count($distributedMatches["matches"]) < $matchesInGame*$roundsCount)
        {                        
            foreach(range(1, $roundsCount) as $roundIndex)
            {                                
                foreach(range(0, ($matchesInGame - 1)) as $matchIndex)
                {
                    if(isset($distributedMatches["matches"][$roundIndex][$matchIndex]))
                        continue;

                    $distributedMatches["matches"][$roundIndex][$matchIndex] = null;
                }
            }            
        }                            

        $usersBets = $tournament->getTournament()->getBets();
        $distributedMatches["bets"] = array();
        foreach($usersBets as $bet)
        {
            $distributedMatches["bets"][$bet->getMatch()->getId()] = $bet;
        }

        return $distributedMatches;
    }        

    public function processRoundResults(\AppBundle\Entity\TournamentRound $tournament)
    {        
        $roundResults = $tournament->getTournament()->getRoundResults();
        $users = $tournament->getTournament()->getUsers();
    
        $usersBets = $tournament->getTournament()->getBets();        
        $bets = array();
        foreach($usersBets as $bet)        
            $bets[$bet->getMatch()->getId()][$bet->getUser()->getId()] = $bet;
    
        $resultsInPreviousRounds = array_fill(1, $tournament->getRealRoundsCount(), array());
        foreach($roundResults as $result)        
            $resultsInPreviousRounds[$result->getRound()][$result->getUser()->getId()] = $result;
    
        $totalPointsForUsersByRound = $this->matchesRepository->findTotalPointsForUsersByRound($tournament->getTournament()->getId());    
        $totalPoints = array();
        foreach($totalPointsForUsersByRound as $row)
        {
            $totalPoints[$row["round"]][$row["user_id"]] = (float) $row["total_points"];
        }
        
        $matches = $this->initTournamentMatchesArray($tournament->getRealRoundsCount(), $tournament->getMatchesInGame(), $tournament->getTournament()->getRoundMatches());                

        $tournamentGames = $tournament->getTournament()->getRoundGames();
        
        $games = array();
        foreach($tournamentGames as &$game)
        {
            if(!$this->isAllMatchesInRoundFinishedAndPointsCalculated($matches[$game->getRound()], $bets))
                continue;

            if(!isset($totalPoints[$game->getRound()][$game->getUserLeft()->getId()]))
                $totalPoints[$game->getRound()][$game->getUserLeft()->getId()] = 0;

            if(!isset($totalPoints[$game->getRound()][$game->getUserRight()->getId()]))
                $totalPoints[$game->getRound()][$game->getUserRight()->getId()] = 0;
               

            $round = $game->getRound();
            $userLeftId = $game->getUserLeft()->getId();
            $userRightId = $game->getUserRight()->getId();
            
            $scoreLeft = 0;
            $scoreRight = 0;
            
            if($totalPoints[$round][$userLeftId] > $totalPoints[$round][$userRightId])
            {
                $scoreLeft = self::POINTS_FOR_WIN;
                $scoreRight = self::POINTS_FOR_LOSS;
            }
            elseif($totalPoints[$round][$userLeftId] < $totalPoints[$round][$userRightId])
            {
                $scoreLeft = self::POINTS_FOR_LOSS;
                $scoreRight = self::POINTS_FOR_WIN;                
            }
            elseif($totalPoints[$round][$userLeftId] == $totalPoints[$round][$userRightId])
            {
                $scoreLeft = self::POINTS_FOR_DRAW;
                $scoreRight = self::POINTS_FOR_DRAW;
            }
            
            $game->setPointsLeft($totalPoints[$round][$userLeftId]);
            $game->setPointsRight($totalPoints[$round][$userRightId]);
            $game->setScoreLeft($scoreLeft);
            $game->setScoreRight($scoreRight);            
            
            $this->em->persist($game);
            $this->em->flush();
            
            $games[$game->getRound()][] = $game;
        }            

        foreach($matches as $round => $matchesInRound)
        {
            if(!empty($resultsInPreviousRounds[$round]))
                continue;                        
            
            if(!isset($games[$round]))
                continue;

            if(!$this->isAllMatchesInRoundFinishedAndPointsCalculated($matchesInRound, $bets))
                continue;
        
            foreach($users as $user)
            {
                if(isset($resultsInPreviousRounds[$round - 1][$user->getId()]))
                    $userResInPrevRound = $resultsInPreviousRounds[$round - 1][$user->getId()];
                else
                    $userResInPrevRound = null;                                                

                $resultsForUserByRound = $this->generateResultForUserByRound($user->getId(), $games[$round], $userResInPrevRound);                                

                $TournamentRoundResult = new TournamentRoundResult();
                $TournamentRoundResult->setTournament($tournament->getTournament());
                $TournamentRoundResult->setUser($user);
                $TournamentRoundResult->setRound($round);
                $TournamentRoundResult->setTotalGames($resultsForUserByRound["total_games"]);
                $TournamentRoundResult->setTotalWins($resultsForUserByRound["total_wins"]);
                $TournamentRoundResult->setTotalDraws($resultsForUserByRound["total_draws"]);
                $TournamentRoundResult->setTotalLoss($resultsForUserByRound["total_loss"]);
                $TournamentRoundResult->setTotalGoalsAgainst($resultsForUserByRound["total_goals_against"]);
                $TournamentRoundResult->setTotalGoalsScored($resultsForUserByRound["total_goals_scored"]);
                $TournamentRoundResult->setTotalGoalsDifference($resultsForUserByRound["total_goals_difference"]);
                $TournamentRoundResult->setTotalPoints($resultsForUserByRound["total_points"]);
                
                $this->em->persist($TournamentRoundResult);
                $this->em->flush();
            }            

            //считаем очки только для одного тура.            
            break;
        }        
    }
    
    public function processResults(\AppBundle\Entity\TournamentRound $tournament)
    {
        $lastRound = $tournament->getRealRoundsCount();
        $resultsInLastRound = $this->resultRepository->findByTournamentAndRound($tournament->getTournament()->getId(), $lastRound);        

        $place = 1;
        foreach($resultsInLastRound as $result)
        {
            $TournamentResult = new TournamentResult();
            $TournamentResult->setTournament($tournament->getTournament());
            $TournamentResult->setUser($result->getUser());
            $TournamentResult->setPlace($place);
            $TournamentResult->setPoints($result->getTotalPoints());            

            $this->em->persist($TournamentResult);
            $this->em->flush();

            $place++;
        }
    }
    
    public function initTournamentMatchesArray($roundsCount, $matchesInRound, $tournamentMatches)
    {
        $matches = array();
        foreach(range(1, $roundsCount) as $round)
        {
            foreach(range(1, $matchesInRound) as $matchIndex)
            {
                $matches[$round][$matchIndex] = null;
                foreach($tournamentMatches as $key => $match)
                {
                    if($match->getRound() == $round)
                    {
                        $matches[$round][$matchIndex] = $match;
                        unset($tournamentMatches[$key]);
                        break;
                    }
                }
            }
        }
        
        return $matches;
    }

    public function createTournament(\AppBundle\Entity\Tournament $initTournament)
    {
        $tournament = new TournamentRound();
        $tournament->setTournament($initTournament);

        return $tournament;
    }   

    public function validateStep2(\AppBundle\Entity\TournamentRound $tournament, $formData)
    {        
        
        if($formData["max_users_count"] < $this->getRule("min_max_users_count"))
            throw new \Exception("Максимальное кол-во участников не может быть меньше ".$this->getRule("min_max_users_count")."!");
            
        if($formData["max_users_count"] % 2 == 1)
            throw new \Exception("Максимальное кол-во участников в данном типе турнира не может быть нечётным числом!");
        
        if(count($tournament->getTournament()->getRoundMatches()) > 0)
        {
            if($formData["max_users_count"] < $tournament->getMaxUsersCount())
                throw new \Exception("Максимальное кол-во участников не может быть меньше ".$tournament->getMaxUsersCount()."!");
        }
        
        $tournament->setMaxUsersCount($formData["max_users_count"]);                                            
        
        if($formData["matches_in_game"] < $this->getRule("min_matches_in_game"))
            throw new \Exception("Количество матчей в игре не может быть меньше ".$this->getRule("min_matches_in_game")."!");
            
        if($formData["matches_in_game"] < $tournament->getMatchesInGame())
            throw new \Exception("Количество матчей в игре не может быть меньше ".$tournament->getMatchesInGame()."!");
            
        $tournament->setMatchesInGame($formData["matches_in_game"]);
    }

    public function validateStep3(\AppBundle\Entity\TournamentRound $tournament)
    {
        if(count($tournament->getTournament()->getRoundMatches()) < 1)
            throw new \Exception("Нельзя опубликовать турнир пока не заполнен хотя бы один матч!");
        
        if(count($this->matchesRepository->findStartedByTournament($tournament->getTournament()->getId())) > 0)
            throw new \Exception("В турнире имеются уже начавишеся, оконченные или отменённые матчи!");
    }
    
    public function addMatch(\AppBundle\Entity\TournamentRound $tournament, \AppBundle\Entity\Match $match, $request)
    {        
        if(!$this->repository->matchIsUnique($tournament->getTournament()->getId(), $match->getId()))
            throw new \Exception("Нельзя добавить два одинаковых матча в один турнир!");        

        $matchToChangeId = $request->get('match_to_change');
        
        $tournamentMatch = new TournamentRoundMatch();
        if(!empty($matchToChangeId))
        {            
            $tm = $this->matchesRepository->findByTournamentAndMatch($tournament->getTournament()->getId(), $matchToChangeId);            
            if($tm)
                $tournamentMatch = $tm;
        }    

        $round = $request->get('round');
        if(!$round)
            throw new \Exception("Не указан тур!");                    

        if(!$this->matchesRepository->isMatchDatetimeMoreThanInPreviousRounds($tournament->getTournament()->getId(), $match->getMatchDate(), $match->getMatchTime(), $round))
            throw new \Exception("Данный матч начинается раньше, чем последние матчи в предыдущих турах!");

        if(!$this->matchesRepository->isMatchDatetimeLessThanInNextRounds($tournament->getTournament()->getId(), $match->getMatchDate(), $match->getMatchTime(), $round))
            throw new \Exception("Данный матч начинается позже, чем первые матчи в последующих турах!");        
                        
        $tournamentMatch->setRound($round);        
        $tournamentMatch->setTournament($tournament->getTournament());
        $tournamentMatch->setMatch($match);
        
        $this->em->persist($tournamentMatch);
        $this->em->flush();

        $tournament->getTournament()->addRoundMatch($tournamentMatch);

        
        
        $this->em->persist($tournament->getTournament());
        $this->em->flush();        
    }
    
    public function isMaxUsersReached(\AppBundle\Entity\TournamentRound $tournament)
    {
        return (count($tournament->getTournament()->getUsers()) >= $tournament->getMaxUsersCount());
    }
    
    public function isTournamentCanStart(\AppBundle\Entity\TournamentRound $tournament)
    {
        return (count($tournament->getTournament()->getUsers()) >= $this->getRule('min_max_users_count'));
    }

    public function isTournamentOver(\AppBundle\Entity\TournamentRound $tournament)
    {    
        return ($tournament->getRealRoundsCount() == $this->resultRepository->getMaxResultRoundByTournament($tournament->getTournament()->getId()));
    }
    
    public function onTournamentMatchSaved(\AppBundle\Event\TournamentEvent $event)
    {        
        $tournament = $event->getTournament();
        
        $this->_updateTournamentStartDatetime($tournament);
    }
    
    public function onTournamentUserAdded(\AppBundle\Event\TournamentEvent $event)
    {
        
    }

    public function onTournamentStarted(\AppBundle\Event\TournamentEvent $event)
    {        
        
    }
    
    private function _updateTournamentStartDatetime(\AppBundle\Entity\TournamentRound $tournament)
    {        
        $minStartDatetime = $this->matchesRepository->getMinStartDatetime($tournament->getTournament()->getId());
        
        if($minStartDatetime)
        {
            $datetime = new \DateTime($minStartDatetime);
            $tournament->getTournament()->setStartDatetime($datetime);
            $this->em->persist($tournament->getTournament());
            $this->em->flush();
        }
    }
    
    /******************************************************************************
    * Round Robin Pairing Generator
    * Author: Eugene Wee
    * Date: 23 May 2005
    * Last updated: 13 May 2007
    * Based on an algorithm by Tibor Simko.
    *
    * Copyright (c) 2005, 2007 Eugene Wee
    *
    * Permission is hereby granted, free of charge, to any person obtaining a copy
    * of this software and associated documentation files (the "Software"), to deal
    * in the Software without restriction, including without limitation the rights
    * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    * copies of the Software, and to permit persons to whom the Software is
    * furnished to do so, subject to the following conditions:
    *
    * The above copyright notice and this permission notice shall be included in
    * all copies or substantial portions of the Software.
    *
    * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    * THE SOFTWARE.
    ******************************************************************************/
    public function generateRoundRobinPairings($num_teams, $first_team='first', $second_team='second') {
        // Do we have a positive number of players? If not, default to 4.
        $num_teams = ($num_teams > 0) ? (int)$num_teams : 4;
        // If necessary, round up number of players to nearest even number.
        $num_teams += $num_teams % 2;

        // Format for pretty alignment of pairings across rounds.
        $format = "%0" . ceil(log10($num_teams)) . "d";
        $pairing = "$format-$format ";

        $games = array();

        // Generate the pairings for each round.
        for ($round = 1; $round < $num_teams; $round++) {
            $players_done = array();
            // Pair each player except the last.
            for ($player = 1; $player < $num_teams; $player++) {
                if (!in_array($player, $players_done)) {
                    // Select opponent.
                    $opponent = $round - $player;
                    $opponent += ($opponent < 0) ? $num_teams : 1;
                    // Ensure opponent is not the current player.
                    if ($opponent != $player) {
                        // Choose colours.
                        if (($player + $opponent) % 2 == 0 xor $player < $opponent) {
                            // Player plays white.
                          $games[$round][$player][$first_team] = $player;
                          $games[$round][$player][$second_team] = $opponent;
                        } else {
                            // Player plays black.
                          $games[$round][$player][$first_team] = $opponent;
                          $games[$round][$player][$second_team] = $player;
                        }

                        // This pair of players are done for this round.
                        $players_done[] = $player;
                        $players_done[] = $opponent;
                    }
                }
            }

            // Pair the last player.
            if ($round % 2 == 0) {
              $opponent = ($round + $num_teams) / 2;
              // Last player plays white.
              $games[$round][$player][$first_team] = $num_teams;
              $games[$round][$player][$second_team] = $opponent;
            } else {
              $opponent = ($round + 1) / 2;
              // Last player plays black.
              $games[$round][$player][$first_team] = $opponent;
              $games[$round][$player][$second_team] = $num_teams;
            }
        }
        return $games;
    }        
    
    private function isAllMatchesInRoundFinishedAndPointsCalculated($matchesInRound, $bets)
    {
        foreach($matchesInRound as $match)
        {
            if(!$match)
                return false;
            
            if(!$match->getMatch()->getIsStarted())
            {
                return false;
            }


            if(!$match->getMatch()->getIsOver() && !$match->getMatch()->getIsRejected())
            {                                
                return false;                        
            }            

            //если ниодин пользователь не сделал ставку на данный матч, то пропускаем этот матч
            if(!isset($bets[$match->getMatch()->getId()]))
            {                
                //\Doctrine\Common\Util\Debug::dump($match->getMatch()->getId());
                //exit;
                //return true;
                continue;
            }

            //если ставки всё таки есть, то нужно проверить выставились ли за них очки
            foreach($bets[$match->getMatch()->getId()] as $bet)
            {
                if(!is_numeric($bet->getPoints()))
                {
                    return false;                                
                }
            }
        }
        
        return true;
    }
    
    public function generateResultForUserByRound($user_id, $gamesInRound, $userResInPrevRound)
    {
        $totalPoints = 0;        
        $totalGames = 0;
        $totalWins = 0;
        $totalDraws = 0;
        $totalLoss = 0;
        $totalGoalsScored = 0;
        $totalGoalsAgainst = 0;
        $totalGoalsDifference = 0;        

            
        if($userResInPrevRound)
        {
            $totalPoints = $userResInPrevRound->getTotalPoints();
            $totalGames = $userResInPrevRound->getTotalGames();            
            $totalWins = $userResInPrevRound->getTotalWins();
            $totalDraws = $userResInPrevRound->getTotalDraws();
            $totalLoss = $userResInPrevRound->getTotalLoss();
            $totalGoalsScored = $userResInPrevRound->getTotalGoalsScored();
            $totalGoalsAgainst = $userResInPrevRound->getTotalGoalsAgainst();            
        }                

        foreach($gamesInRound as $game)
        {
            $userLeftId = $game->getUserLeft()->getId();
            $userRightId = $game->getUserRight()->getId();
            
            if($userLeftId != $user_id && $userRightId != $user_id)
                continue;
            
            if($userLeftId == $user_id)
            {
                $totalPoints += $game->getScoreLeft();

                if($game->getScoreLeft() > $game->getScoreRight())
                    $totalWins++;
                elseif($game->getScoreLeft() < $game->getScoreRight())
                    $totalLoss++;
                elseif($game->getScoreLeft() == $game->getScoreRight())
                    $totalDraws++;

                $totalGoalsScored += $game->getPointsLeft();
                $totalGoalsAgainst += $game->getPointsRight();

            }
            elseif($userRightId == $user_id)
            {
                $totalPoints += $game->getScoreRight();

                if($game->getScoreLeft() < $game->getScoreRight())
                    $totalWins++;
                elseif($game->getScoreLeft() > $game->getScoreRight())
                    $totalLoss++;
                elseif($game->getScoreLeft() == $game->getScoreRight())
                    $totalDraws++;

                $totalGoalsScored += $game->getPointsRight();
                $totalGoalsAgainst += $game->getPointsLeft();
            }

            $totalGoalsDifference = $totalGoalsScored - $totalGoalsAgainst;

            $totalGames++;
        }        
        
        return array("total_points" => $totalPoints,                     
                     "total_games" => $totalGames,                     
                     "total_wins" => $totalWins,
                     "total_draws" => $totalDraws,
                     "total_loss" => $totalLoss,
                     "total_goals_scored" => $totalGoalsScored,
                     "total_goals_against" => $totalGoalsAgainst,
                     "total_goals_difference" => $totalGoalsDifference                     
                     );
    }
}