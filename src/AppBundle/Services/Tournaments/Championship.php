<?php
namespace AppBundle\Services\Tournaments;

use AppBundle\Entity\TournamentChampionshipResult;
use AppBundle\Entity\TournamentChampionshipRealTimeResult;
use AppBundle\Entity\TournamentResult;

use AppBundle\Services\LogHelper;

class Championship
{
    /**    
    * @var \AppBundle\Entity\TournamentChampionshipRepository
    */
    public $tournamentRep;  
    
    private $em;
    private $pointsRuler;
    private $logHelper;
    
    /**    
    * @var \AppBundle\Entity\TournamentChampionshipMatchRepository
    */
    private $matchesRep;
    
    /**
    * Минимальное колчичество туров в турнире
    */
    const MIN_ROUNDS_COUNT = 1;
    
    /**
    * Минимальное количество матчей в туре
    */
    const MIN_MATCHES_IN_ROUND = 1;
    
    /**
    * Минимально возможное количество участников в турнире
    */
    const MIN_USERS_COUNT = 2;
    
    /**
    * Максимально возможное количество участников в турнире
    */
    const MAX_USERS_COUNT = 100;
    
    public function __construct(\Doctrine\ORM\EntityManager $em, \AppBundle\Services\PointsRuler $pointsRuler)
    {
        $this->em = $em;        
        $this->pointsRuler = $pointsRuler;
        $this->logHelper = new LogHelper($this->em);
        
        $this->matchesRep = $this->em->getRepository('AppBundle\Entity\TournamentChampionshipMatch');
        $this->tournamentRep = $this->em->getRepository('AppBundle\Entity\TournamentChampionship');
    }
    
    /**
    * Валидация настроек турнира
    * 
    * @param \AppBundle\Entity\TournamentChampionship $championship
    * @throws \Exception
    * @return bool
    */
    public function validate(\AppBundle\Entity\TournamentChampionship $championship)
    {
      if($championship->getRoundsCount() < self::MIN_ROUNDS_COUNT)
      {        
        throw new \Exception("Количество туров не может быть меньше ".self::MIN_ROUNDS_COUNT."!");
        return false;
      }
      
      if($championship->getMaxUsersCount() < self::MIN_USERS_COUNT)
      {
        throw new \Exception("Количество участников не может быть меньше ".self::MIN_USERS_COUNT."!");
        return false;
      }
      
      if($championship->getMaxUsersCount() > self::MAX_USERS_COUNT)
      {
        throw new \Exception("Количество участников не может быть больше ".self::MAX_USERS_COUNT."!");
        return false;
      }
      
      if($championship->getMatchesInRound() < self::MIN_MATCHES_IN_ROUND)
      {
        throw new \Exception("Количество матчей в туре не может быть меньше ".self::MIN_MATCHES_IN_ROUND."!");
        return false;
      }
      
      return true;
    }
    
    /**
    * Разбивает список матчей по турам    
    * 
    * @param \AppBundle\Entity\TournamentChampionshipMatch[] $matches Массив c матчами
    * @param int $roundsCount Кол-во туров в турнире
    * @param int $matchesInRound Кол-во матчей в одном туре
    */
    public function chunkMatches($matches, $roundsCount, $matchesInRound)
    {
        $roundsMatches = array_fill(1, $roundsCount, array_fill(1, $matchesInRound, null));                
        
        foreach($roundsMatches as $roundNum => &$roundMatches)
        {        
            foreach($roundMatches as &$roundMatch)
            {                
                foreach($matches as $key => $match)
                {                                        
                    if($match->getRound() != $roundNum)
                        continue;
                        
                    $roundMatch = $match;
                    unset($matches[$key]);
                    break;
                }
            }
        }
        
        return $roundsMatches;
    }
    
    /**
    * Проверяет матчи турнира
    * 
    * @param \AppBundle\Entity\TournamentChampionship $championship
    * @throws \Exception 
    * @return bool
    */
    public function checkMatches(\AppBundle\Entity\TournamentChampionship $championship)
    {
      if(count($championship->getTournament()->getChampionshipMatches()) < 1)
      {
        throw new \Exception("Нельзя опубликовать турнир пока не заполнен хотя бы один матч!");
        return false;
      }
            
      if(count($this->matchesRep->findStartedByTournament($championship->getTournament()->getId())) > 0)
      {
        throw new \Exception("В турнире имеются уже начавишеся, оконченные или отменённые матчи!");
        return false;
      }
      
      return true;
    }
    
    /**
    * Генерирует список пользователей для результатов когда их ещё нет.
    * 
    * @param \AppBundle\Entity\TournamentChampionship $tournament
    * @return \AppBundle\Entity\TournamentChampionshipResult
    */
    public function initResults(\AppBundle\Entity\TournamentChampionship $tournament)
    {
        if(!$tournament->getTournament()->getIsStarted())
        {
            $results = array();
        
            $tournamentUsers = $this->em->getRepository('AppBundle\Entity\User')->findTournamentUsersOrderedByRating($tournament->getTournament()->getId());        
            $place = 1;
            foreach($tournamentUsers as $user)
            {
                $tmpResult = new TournamentChampionshipResult();
                $tmpResult->setTournament($tournament->getTournament());
                $tmpResult->setUser($user);
                $tmpResult->setRound(1);
                $tmpResult->setPlace($place);
                $tmpResult->setTotalPoints(0);                        
                $tmpResult->setPointsInRound(0);            
                $tmpResult->setTotalGuessedResults(0);
                $tmpResult->setTotalGuessedOutcomes(0);            
                $tmpResult->setTotalGuessedDifferences(0);                        
                $tmpResult->setUserRating($user->getRating());            
                
                $results[] = $tmpResult;
                $place++;
            }        
            
            return $results;
        }
        
        $results = $this->em->getRepository('AppBundle\Entity\TournamentChampionshipRealTimeResult')->findByTournament($tournament->getTournament()->getId());
                
        return $results;
    }
    
    /**
    * Генерирует результаты туров по крону
    * 
    * @param \AppBundle\Entity\Tournament $tournament
    */
    public function processResults(\AppBundle\Entity\Tournament $tournament)
    {
        /**        
        * @var \AppBundle\Entity\TournamentChampionship $championship
        */
        $championship = $this->em->getRepository("\AppBundle\Entity\TournamentChampionship")->findLazy($tournament->getId());
        
        $currentRound = $championship->getCurrentRound();
        $tournamentId = $tournament->getId();
        
        /**
        * Если нет необработанных матчей, которые закончились, то выходим
        */
        if(!$matchesIds = $this->em->getRepository("\AppBundle\Entity\TournamentChampionshipMatch")->findNonProcessedMatchesFinished($tournamentId, $currentRound))
            return true;
        
        //собираем idшники матчей
        $nonProcessedMatchesIds = array();        
        array_walk($matchesIds, function(&$matchData) use (&$nonProcessedMatchesIds){
            $nonProcessedMatchesIds[] = $matchData["id"];
        });        
        unset($matchesIds);                
        
        
        if(count($nonProcessedMatchesIds) == 0)
            return true;
                
        /**
        * Если среди необработанных матчей, которые закончились, есть необработанные ставки, то выходим
        */
        if(!$this->em->getRepository("\AppBundle\Entity\TournamentChampionshipMatch")->allBetsCalculated($tournamentId, $nonProcessedMatchesIds))
            return true;
        
        $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: SETTING POINTS FOR NON PROCCESSED MATCHES: ".serialize($nonProcessedMatchesIds));
        $this->em->flush();
        
        /**
        * Начисляем очки в онлайн таблицу
        */
        $rtResults = $this->em->getRepository("\AppBundle\Entity\TournamentChampionshipRealTimeResult")->findByTournament($tournamentId);
        $nonProcessedMatches = $this->em->getRepository("\AppBundle\Entity\TournamentChampionshipMatch")->findByIds($tournamentId, $nonProcessedMatchesIds);
        $roundBets = $this->__getBetsForEachUser($tournamentId, $nonProcessedMatchesIds);
        
        try
        {        
            foreach($rtResults as &$result)
            {                            
                $currentUserResult = $this->__getResultForUserByRound($result->getUser()->getId(), $nonProcessedMatches, $roundBets);
                
                $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: SETTING REAL TIME RESULTS: ".serialize(array("user" => $result->getUser()->getId(),
                                                                                                                            "results" => $currentUserResult)));
                
                $result->setPlace(0);
                $result->setTotalPoints($result->getTotalPoints() + $currentUserResult["total_points"]);
                $result->setTotalGuessedResults($result->getTotalGuessedResults() + $currentUserResult["total_guessed_results"]);
                $result->setTotalGuessedOutcomes($result->getTotalGuessedOutcomes() + $currentUserResult["total_guessed_outcomes"]);
                $result->setTotalGuessedDifferences($result->getTotalGuessedDifferences() + $currentUserResult["total_guessed_differences"]);            
                $result->setUserRating($result->getUser()->getRating());
                
                $this->em->persist($result);            
            }
        } catch(\Exception $ex)
        {
            $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: EXCEPTION ON SETTING REAL TIME RESULTS: ".$ex->getMessage());            
            $this->em->flush();
            return true;
        }
        
        $this->em->flush();
                
        foreach($nonProcessedMatches as &$tournamentMatch)
        {            
            $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: SETTING MATCH PROCCESSED: ".serialize(array("match_id" => $tournamentMatch->getMatch()->getId())));
            $tournamentMatch->setIsProcessed(true);
            $this->em->persist($tournamentMatch);
        }                
        
        $this->em->flush();
        
        unset($nonProcessedMatches);
        unset($tournamentMatch);
        unset($rtResults);
        
        /**
        * Сортируем и проставляем места
        */
        $rtResults = $this->em->getRepository("\AppBundle\Entity\TournamentChampionshipRealTimeResult")->findSorted($tournamentId);
        $place = 1;
        foreach($rtResults as &$result)
        {
            $result->setPlace($place);
            $this->em->persist($result);
            $place++;
        }
        
        $this->em->flush();
        
        unset($rtResults);
        unset($result);
        
        /**
        * Если не все матчи окончились, то выходим
        */
        if(!$this->em->getRepository("\AppBundle\Entity\TournamentChampionshipMatch")->allRoundMatchesFinished($tournamentId, $currentRound))
            return true;
        
        /**
        * Если все матчи в туре закончились, но за какие-то матчи ещё не начислены очки, то выходим
        */
        if(!$this->em->getRepository("\AppBundle\Entity\TournamentChampionshipMatch")->matchesPointsCalculated($tournamentId, $currentRound))
            return true;                
        
        $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: ALL MATCHES FINISHED AND PROCCESSED IN ROUND $currentRound!");
        
        /**
        * Так как тур окончен, то заносим результаты тура в таблицу результов по турам
        */
        $rtResults = $this->em->getRepository("\AppBundle\Entity\TournamentChampionshipRealTimeResult")->findByTournament($tournamentId);        
        foreach($rtResults as &$result)
        {
            $tourResult = new TournamentChampionshipResult();
            $tourResult->setTournament($result->getTournament());
            $tourResult->setUser($result->getUser());
            $tourResult->setRound($currentRound);
            $tourResult->setPlace($result->getPlace());
            $tourResult->setPointsInRound(0);
            $tourResult->setTotalPoints($result->getTotalPoints());
            $tourResult->setTotalGuessedResults($result->getTotalGuessedResults());
            $tourResult->setTotalGuessedOutcomes($result->getTotalGuessedOutcomes());
            $tourResult->setTotalGuessedDifferences($result->getTotalGuessedDifferences());
            $tourResult->setUserRating($result->getUser()->getRating());
            
            $this->em->persist($tourResult);
        }
        
        unset($rtResults);
        unset($result);
        
        $this->em->flush();
        
        /**
        * Переходим в следующий тур, если он есть.
        */        
        if($championship->getCurrentRound() + 1 <= $championship->getRoundsCount())
        {
            $championship->setCurrentRound($championship->getCurrentRound() + 1);
            $this->em->persist($championship);
            $this->em->flush();
            
            return null;
        }
        
        /**
        * Или заносим результаты в таблицу результатов для распределения призовых и заканчиваем турнир
        */        
        $lastRoundResults = $this->em->getRepository("\AppBundle\Entity\TournamentChampionshipResult")->findByTournamentAndRound($tournament->getId(), $currentRound);
        
        foreach($lastRoundResults as $championshipResult)
        {
            $results = new TournamentResult();
            $results->setTournament($championshipResult->getTournament());
            $results->setUser($championshipResult->getUser());
            $results->setPlace($championshipResult->getPlace());
            $results->setPoints($championshipResult->getTotalPoints());
            
            $this->em->persist($results);
        }
        unset($championshipResult);
        unset($lastRoundResults);
        
        $rtResults = $this->em->getRepository('AppBundle\Entity\TournamentChampionshipRealTimeResult')->findByTournament($tournament->getId());
        
        foreach($rtResults as $result)
        {
            $this->em->remove($result);
        }
        unset($rtResults);
        unset($result);
        
        $tournament->setIsOver(true);
        $tournament->setIsResultsReady(true);
        $tournament->setEndDatetime( new \DateTime() );
        
        $this->em->flush();        
    }
    
    /**
    * Обновляет время старта турнира
    * 
    * @param \AppBundle\Entity\Tournament $tournament
    */
    public function updateTournamentStartDatetime($tournament)
    {
      $startDatetime = $this->matchesRep->getMinStartDatetime($tournament->getId());
      
      if(!$tournament->getStartDatetime() || $tournament->getStartDatetime()->format("Y-m-d H:i:s") != $startDatetime)
      {
        if($startDatetime)
        {
            $datetime = new \DateTime($startDatetime);
            $tournament->setStartDatetime($datetime);          
        }
        else
        {
          $tournament->setStartDatetime(null);
        }
        
        $this->em->persist($tournament);
        $this->em->flush($tournament);
      }
    }
    
    /**
    * Проверяет возможность старта турнира
    * 
    * @param \AppBundle\Entity\Tournament $tournament
    * @return bool
    */
    public function isTournamentCanStart(\AppBundle\Entity\Tournament $tournament)
    {
      if(count($tournament->getUsers()) < self::MIN_USERS_COUNT)
        return false;
      
      return true;
    }
    
    /**
    * Event Handlers:
    */
    
    /**
    * Обработчик события добавления матча в турнир
    * 
    * @param \AppBundle\Event\TournamentEvent $event
    */
    public function onTournamentMatchAdded(\AppBundle\Event\TournamentEvent $event)
    {
      /**      
      * @var \AppBundle\Entity\TournamentChampionship
      */
      $championship = $event->getTournament();
            
      /**      
      * @var \AppBundle\Entity\Tournament
      */
      $tournament = $championship->getTournament();
      
      $this->updateTournamentStartDatetime($tournament);      
    }
    
    /**
    * Обработчик события удаления матча из турнира
    * 
    * @param \AppBundle\Event\TournamentEvent $event
    */
    public function onTournamentMatchDeleted(\AppBundle\Event\TournamentEvent $event)
    {
      /**      
      * @var \AppBundle\Entity\TournamentChampionship
      */
      $championship = $event->getTournament();
            
      /**      
      * @var \AppBundle\Entity\Tournament
      */
      $tournament = $championship->getTournament();
      
      $this->updateTournamentStartDatetime($tournament);
    }
    
    /**
    * Обработчик события старта турнира
    * 
    * @param \AppBundle\Event\TournamentEvent $event
    */
    public function onTournamentStartedHandler(\AppBundle\Event\TournamentEvent $event)
    {
        /**        
        * @var \AppBundle\Entity\TournamentChampionship $tournament
        */
        $tournament = $event->getTournament();
        
        $users = $this->em->getRepository("\AppBundle\Entity\User")->findChampionshipUsers($tournament->getTournament()->getId());
        
        $place = 1;
        foreach($users as $user)
        {
            $rtResult = new TournamentChampionshipRealTimeResult();
            $rtResult->setTournament($tournament->getTournament());
            $rtResult->setUser($user);
            $rtResult->setPlace($place);
            $rtResult->setUserRating($user->getRating());
            
            $this->em->persist($rtResult);
            
            $place++;
        }
        
        $this->em->flush();
    }
    
    public function onTournamentUserAddedHandler(\AppBundle\Event\TournamentEvent $event)
    {
        /**        
        * @var \AppBundle\Entity\TournamentChampionship $tournament
        */
        $tournament = $event->getTournament();
        
        $usersCount = $this->em->getRepository("\AppBundle\Entity\User")->findChampionshipUsersCount($tournament->getTournament()->getId());
        
        if($usersCount >= $tournament->getMaxUsersCount())
        {
            $tournament->getTournament()->setIsAccessible(false);
            
            $this->em->persist($tournament->getTournament());
            $this->em->flush();
        }
    }
    
    /**
    * Private:
    */
    
    /**
    * Получает результаты по id турнира и номеру тура и распредляет их по пользователям
    * 
    * @param int $tournament_id
    * @param int $round
    * @return array $results
    */
    private function _getResultsByRoundForEachUser($tournament_id, $round)
    {
        $results = array();
        if($round == 0)
            return $results;
        
        $roundResults = $this->em->getRepository("\AppBundle\Entity\TournamentChampionshipResult")->findByTournamentAndRound($tournament_id, $round);
        
        foreach($roundResults as $result)
            $results[$result->getUser()->getId()] = $result;        
        unset($result);
        
        return $results;
    }
    
    /**
    * Получает ставки участников по id турнира и номеру тура и распределяет их по матчам и пользователям
    * 
    * @param int $tournament_id
    * @param int $round
    * @return array $userBets
    */
    private function __getBetsForEachUser($tournament_id, $matchesIds)
    {
        $bets = $this->em->getRepository("\AppBundle\Entity\Bet")->findByTournamentAndMatchesIds($tournament_id, $matchesIds);
        
        $userBets = array();
        array_walk($bets, function(&$bet) use (&$userBets){
            $userBets[$bet->getMatch()->getId()][$bet->getUser()->getId()] = $bet;
        });
        
        return $userBets;
    }
    
    private function __getResultForUserByRound($user_id, $tournamentMatches, $bets)
    {
        $totalPoints = 0;        
        $totalGuessedResults = 0;
        $totalGuessedOutcomes = 0;
        $totalGuessedDifferences = 0;                
        
        foreach($tournamentMatches as $tournamentMatch)
        {
            $match = $tournamentMatch->getMatch();
            
            if($match->getIsRejected())
                continue;
            
            /**
            * Если пользователь не делал ставок на матч, не учитываем его
            */
            if(!isset($bets[$match->getId()][$user_id]))
                continue;
            
            $bet = $bets[$match->getId()][$user_id];
            
            if(is_null($bet->getPoints()))
            {
                throw new \Exception("Очки пользователю #".$user_id." за матч #".$match->getId()." ещё не начислены!");
                return false;
            }
            
            $totalPoints += $bet->getPoints();
                        
            if($this->pointsRuler->scoreGuessed($bet, $match))
            {
                $totalGuessedResults++;
                $totalGuessedDifferences++;
                $totalGuessedOutcomes++;

            }
            elseif($this->pointsRuler->scoreDifferenceGuessed($bet, $match))
            {
                $totalGuessedDifferences++;
                $totalGuessedOutcomes++;
            }
            elseif($this->pointsRuler->matchOutcomeGuessed($bet, $match))            
            {
                $totalGuessedOutcomes++;
            }
            
        }                
        
        return array("total_points" => $totalPoints,                     
                     "total_guessed_results" => $totalGuessedResults,
                     "total_guessed_outcomes" => $totalGuessedOutcomes,
                     "total_guessed_differences" => $totalGuessedDifferences);
    }
}