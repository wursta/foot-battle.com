<?php
namespace AppBundle\Services\Tournaments;

use AppBundle\Entity\TournamentChampionsleague;
use AppBundle\Entity\TournamentChampionsleagueQualificationRealTimeResult;
use AppBundle\Entity\TournamentChampionsleagueQualificationResult;
use AppBundle\Entity\TournamentChampionsleagueGroupResult;
use AppBundle\Entity\TournamentChampionsleagueGroupGame;
use AppBundle\Entity\TournamentChampionsleaguePlayoffGame;
use AppBundle\Entity\TournamentResult;

use AppBundle\Services\LogHelper;

class Championsleague
{
    /**    
    * @var \AppBundle\Entity\TournamentChampionsleagueRepository
    */
    public $tournamentRep;    
    
    private $em;
    
    /**    
    * @var \AppBundle\Services\PointsRuler
    */
    private $pointsRuler;
    
    /**    
    * @var \AppBundle\Services\Tournaments\Round
    */
    private $roundHelper;
    
    /**    
    * @var \AppBundle\Services\Tournaments\Playoff
    */
    private $playoffHelper;
    
    private $logHelper;
    
    /**    
    * @var \AppBundle\Entity\TournamentChampionsleagueMatchRepository
    */
    private $matchesRep;
    
    private $rules;    
    
    /**
    * Количество очков начисляемое за победу в игре
    */
    const POINTS_FOR_WIN = 3;
    
    /**
    * Количество очков начисляемое за поражение в игре
    */
    const POINTS_FOR_LOSS = 0;
    
    /**
    * Количество очков начисляемое за ничью в игре
    */
    const POINTS_FOR_DRAW = 1;
    
    /**
    * Минимальное количество туров квалификации
    */
    const MIN_QUALIFICATION_ROUNDS_COUNT = 1;
    
    /**
    * Минимальное количество матчей в туре квалификации
    */
    const MIN_MATCHES_IN_QUALIFICATION = 1;
    
    /**
    * Минимальное количество матчей в игре в групповом этапе
    */
    const MIN_MATCHES_IN_GROUP_GAME = 1;
    
    /**
    * Максимальное количество групп
    */
    const MAX_GROUPS_COUNT = 64;
    
    /**
    * Максимальное количество участников выходящих из группы
    */
    const MIN_USERS_OUT_OF_GROUP = 2;
    
    /**
    * Минимальное количество матчей в игре на этапе плейофф
    */
    const MIN_MATCHES_IN_PLAYOFF_GAME = 1;
    
    /**
    * Минимальное количество участников
    */
    const MIN_USERS_COUNT = 4;
    
    /**
    * Минимальное количество участников
    */
    const MAX_USERS_COUNT = 256;
    
    /**
    * Стандартное количество участников в одной группе
    */
    const STANDARD_USERS_IN_GROUP = 4;
    
    public function __construct(\Doctrine\ORM\EntityManager $em, \AppBundle\Services\PointsRuler $pointsRuler, \AppBundle\Services\Tournaments\Round $roundHelper, \AppBundle\Services\Tournaments\Playoff $playoffHelper)
    {
        $this->em = $em;        
        $this->pointsRuler = $pointsRuler;
        $this->roundHelper = $roundHelper;
        $this->playoffHelper = $playoffHelper;
        $this->logHelper = new LogHelper($this->em);
        
        $this->matchesRep = $this->em->getRepository('AppBundle\Entity\TournamentChampionsleagueMatch');
        $this->tournamentRep = $this->em->getRepository('AppBundle\Entity\TournamentChampionsleague');
        
        $this->rules = array(
            "min_qualification_rounds_count" => 1,
            "min_matches_in_qualification" => 1,
            "min_matches_in_game" => 1,
            "min_matches_in_playoff" => 1,
            "min_max_users_count" => 4,
            "max_max_users_count" => 256,
            "min_max_groups_count" => 1,
            "max_max_groups_count" => 64,
            "min_users_out_of_group" => 2
        );
    }
    
    public function getRule($ruleName)
    {
        return $this->rules[$ruleName];
    }
    
    /**
    * Валидация настроек турнира
    * 
    * @param \AppBundle\Entity\TournamentChampionsleague $championsleague
    * @throws \Exception
    * @return bool
    */
    public function validate(\AppBundle\Entity\TournamentChampionsleague $championsleague)
    {
      if($championsleague->getQualificationRoundsCount() < self::MIN_QUALIFICATION_ROUNDS_COUNT)
      {
        throw new \Exception("Количество туров квалификации не может быть меньше ".self::MIN_QUALIFICATION_ROUNDS_COUNT."!");
        return false;
      }
      
      if($championsleague->getMatchesInQualification() < self::MIN_MATCHES_IN_QUALIFICATION)
      {        
        throw new \Exception("Количество матчей в туре квалификации не может быть меньше ".self::MIN_MATCHES_IN_QUALIFICATION."!");
        return false;
      }
      
      if($championsleague->getMatchesInGame() < self::MIN_MATCHES_IN_GROUP_GAME)
      {
        throw new \Exception("Количество матчей в игре группового тура не может быть меньше ".self::MIN_MATCHES_IN_GROUP_GAME."!");
        return false;
      }
      
      if($championsleague->getMatchesInPlayoff() < self::MIN_MATCHES_IN_PLAYOFF_GAME)
      {
        throw new \Exception("Количество матчей в игре плей-офф не может быть меньше ".self::MIN_MATCHES_IN_PLAYOFF_GAME."!");
        return false;
      }
      
      if($championsleague->getMaxUsersCount() < self::MIN_USERS_COUNT)
      {
        throw new \Exception("Количество участников не может быть меньше ".self::MIN_USERS_COUNT."!");
        return false;
      }
      
      if($championsleague->getMaxUsersCount() < self::MIN_USERS_COUNT)
      {
        throw new \Exception("Количество участников не может быть меньше ".self::MIN_USERS_COUNT."!");
        return false;
      }
      
      //Дополнительные проверки на логику      
      if($championsleague->getMaxGroupsCount() > $championsleague->getMaxUsersCount()/2)
      {
        throw new \Exception("Количество групп не может превышать половины количества участников. В каждой группе должно быть минимум 2 участника.");
        return false;
      }
      
      if($championsleague->getUsersOutOfGroup() > $championsleague->getMaxUsersCount()/$championsleague->getMaxGroupsCount())
      {
        throw new \Exception("Количество участников выходящих из группы не может превышать количества участников в группе.");
        return false;
      }
            
      return true;
    }
    
    /**
    * Проверяет матчи турнира
    * 
    * @param \AppBundle\Entity\TournamentChampionsleague $championsleague
    * @throws \Exception 
    * @return bool
    */
    public function checkMatches(\AppBundle\Entity\TournamentChampionsleague $championsleague)
    {
      if(count($championsleague->getTournament()->getChampionsleagueMatches()) < 1)
      {
        throw new \Exception("Нельзя опубликовать турнир пока не заполнен хотя бы один матч!");
        return false;
      }
            
      if(count($this->matchesRep->findStartedByTournament($championsleague->getTournament()->getId())) > 0)
      {
        throw new \Exception("В турнире имеются уже начавишеся, оконченные или отменённые матчи!");
        return false;
      }
      
      return true;
    }
    
    /**
    * Разбивает список матчей по турам    
    * 
    * @param array $matches Массив матчей.
    * @param int $roundsCount Кол-во туров
    * @param int $matchesInRound Кол-во матчей в одном туре
    */
    public function chunkMatches($matches, $roundsCount, $matchesInRound)
    {        
        $roundsMatches = array_fill(1, $roundsCount, array_fill(1, $matchesInRound, null));
        
        foreach($roundsMatches as $roundNum => &$roundMatches)
        {        
            foreach($roundMatches as &$roundMatch)
            {                
                foreach($matches as $key => $match)
                {                                        
                    if($match->getRound() != $roundNum)
                        continue;
                        
                    $roundMatch = $match;
                    unset($matches[$key]);
                    break;
                }
            }
        }
        
        return $roundsMatches;
    }
    
    /**
    * Возвращает максимальное количество туров в групповом этапе
    * 
    * @param int $usersCount Кол-во участников
    * @param int $goupsCount Кол-во групп
    * @return int
    */
    public function getMaxGroupRoundsCount($usersCount, $groupsCount)
    {
      //Количество туров в случае достижения максимального колчичества участников
      $maxUsersRoundsCount = $this->roundHelper->getRoundsCount($usersCount/$groupsCount);
      
      //Колтичество туров в случае, если количество участников будет минимальным      
      $notMaxUserRoundsCount = $this->roundHelper->getRoundsCount(self::MIN_USERS_COUNT);
            
      return max(array($maxUsersRoundsCount, $notMaxUserRoundsCount));
    }
    
    /**
    * Возвращает максимальное количество туров в плейофф
    * 
    * @param int $usersCount Кол-во участников
    * @param int $groupsCount Кол-во групп
    * @param int $usersOutOfGroup Кол-во выходящих из группы участников
    * @return int
    */
    public function getMaxPlayoffRoundsCount($usersCount, $groupsCount, $usersOutOfGroup)
    {
      //Количество туров в случае достижения максимального колчичества участников
      $maxUsersRoundsCount = $this->playoffHelper->getPlayoffRoundsCount($groupsCount*$usersOutOfGroup);      
      
      //Колтичество туров в случае, если количество участников будет меньше максимального
      $usersCount = max($this->playoffHelper->getPossibleUsersCounts($usersCount-1));
      $notMaxUserRoundsCount = $this->playoffHelper->getPlayoffRoundsCount($usersCount);      
            
      return max(array($maxUsersRoundsCount, $notMaxUserRoundsCount));
    }
    
    /**
    * Генерирует список пользователей для результатов квалификации когда их ещё нет.
    * 
    * @param \AppBundle\Entity\TournamentChampionsleague $tournament
    * @return \AppBundle\Entity\TournamentChampionsleagueQualificationResult
    */
    public function initQualificationResults(\AppBundle\Entity\TournamentChampionsleague $tournament)
    {
        if(!$tournament->getTournament()->getIsStarted())
        {
            $results = array();
        
            $tournamentUsers = $this->em->getRepository('AppBundle\Entity\User')->findTournamentUsersOrderedByRating($tournament->getTournament()->getId());
            $place = 1;
            foreach($tournamentUsers as $user)
            {
                $tmpResult = new TournamentChampionsleagueQualificationResult();
                $tmpResult->setTournament($tournament->getTournament());
                $tmpResult->setUser($user);
                $tmpResult->setRound(1);
                $tmpResult->setPlace($place);
                $tmpResult->setTotalPoints(0);                        
                $tmpResult->setPointsInRound(0);            
                $tmpResult->setTotalGuessedResults(0);
                $tmpResult->setTotalGuessedOutcomes(0);            
                $tmpResult->setTotalGuessedDifferences(0);                        
                $tmpResult->setUserRating($user->getRating());            
                
                $results[] = $tmpResult;
                $place++;
            }        
            
            return $results;
        }
        
        $results = $this->em->getRepository('AppBundle\Entity\TournamentChampionsleagueQualificationRealTimeResult')->findByTournament($tournament->getTournament()->getId());
        
        return $results;
    }
    
    /**
    * Генерирует результаты туров по крону
    * 
    * @param \AppBundle\Entity\Tournament $tournament
    */
    public function processResults(\AppBundle\Entity\Tournament $tournament)
    {
        /**        
        * @var $championsleague \AppBundle\Entity\TournamentChampionsleage
        */
        $championsleague = $this->em->getRepository("\AppBundle\Entity\TournamentChampionsleague")->findLazy($tournament->getId());
        
        $currentRound = $championsleague->getCurrentRound();
        $currentStage = $championsleague->getStage();
                
        if($currentStage == TournamentChampionsleague::STAGE_QUALIFICATION)
            $this->processQualificationRound($championsleague, $currentRound);
        elseif($currentStage == TournamentChampionsleague::STAGE_GROUP)
            $this->processGroupRound($championsleague, $currentRound);
        elseif($currentStage == TournamentChampionsleague::STAGE_PLAYOFF)
            $this->processPlayoffRound($championsleague, $currentRound);
    }
    
    /**
    * Обновляет время старта турнира
    *
    * @param \AppBundle\Entity\Tournament $tournament
    */
    public function updateTournamentStartDatetime($tournament)
    {
      $startDatetime = $this->matchesRep->getMinStartDatetime($tournament->getId());
      
      if(!$tournament->getStartDatetime() || $tournament->getStartDatetime()->format("Y-m-d H:i:s") != $startDatetime)
      {
        if($startDatetime)
        {
            $datetime = new \DateTime($startDatetime);
            $tournament->setStartDatetime($datetime);          
        }
        else
        {
          $tournament->setStartDatetime(null);
        }
        
        $this->em->persist($tournament);
        $this->em->flush($tournament);
      }
    }
    
    /**
    * Проверяет возможность старта турнира
    * 
    * @param \AppBundle\Entity\Tournament $tournament
    * @return bool
    */
    public function isTournamentCanStart(\AppBundle\Entity\Tournament $tournament)
    {
      if(count($tournament->getUsers()) < self::MIN_USERS_COUNT)
        return false;
      
      return true;
    }
    
    /**
    * Static
    */
    
    /**
    * Возвращает возможные варианты количества участников в турнире проходящих в групповой этап
    * 
    * @param int $usersCnt Максимальное кол-во участников
    * 
    * @return array Массив с возможными вариантами кол-ва участников в групповой этап в формате [2, 4, 8, ...] до $usersCnt
    */
    public static function getPossibleUsersCounts($usersCnt = self::MAX_USERS_COUNT)
    {
      $countChoices = array();
      for($exp = 1; pow(2, $exp) <= $usersCnt; $exp++)
        $countChoices[pow(2, $exp)] = pow(2, $exp);
        
      return $countChoices;
    }
    
    /**
    * Возвращает возможные варианты количества групп на которые будут разбиты участники
    * 
    * @param int $groupsCnt Максимальное кол-во групп
    * 
    * @return array Массив с возможными вариантами кол-ва групп в групповом этапе в формате [2, 4, 8, ...] до $groupsCnt
    */
    public static function getPossibleGroupsCounts($groupsCnt = self::MAX_GROUPS_COUNT)
    {
      $countChoices = array();
      for($exp = 0; pow(2, $exp) <= $groupsCnt; $exp++)
        $countChoices[pow(2, $exp)] = pow(2, $exp);
        
      return $countChoices;
    }
    
    /**
    * Возвращает возможные варианты количества участников в турнире проходящих в плей-офф из групы
    * 
    * @param int $usersCnt Максимальное кол-во участников
    * 
    * @return array Массив с возможными вариантами кол-ва участников проходящих в плей-офф из групы в формате [2, 4, 8, ...] до $usersCnt
    */
    public static function getPossibleOutOfGroupUsersCounts($usersCnt = self::MAX_USERS_COUNT)
    {
      $countChoices = array();
      for($exp = 1; pow(2, $exp) <= $usersCnt; $exp++)
        $countChoices[pow(2, $exp)] = pow(2, $exp);
        
      return $countChoices;
    }
    
    /**
    * Private    
    */
    
    private function processQualificationRound(\AppBundle\Entity\TournamentChampionsleague $tournament, $currentRound)
    {
        $tournamentId = $tournament->getTournament()->getId();
                
        /**
        * Если нет необработанных матчей, которые закончились, то выходим
        */
        if(!$matchesIds = $this->em->getRepository("\AppBundle\Entity\TournamentChampionsleagueMatch")->findNonProcessedMatchesFinished($tournamentId, $currentRound, true, false))
            return true;
            
        //собираем idшники матчей
        $nonProcessedMatchesIds = array();        
        foreach($matchesIds as $matchData)
        {
            $nonProcessedMatchesIds[] = $matchData["id"];
        }        
        unset($matchesIds, $matchData);                
        
        
        if(count($nonProcessedMatchesIds) == 0)
            return true;
            
        /**
        * Если среди необработанных матчей, которые закончились, есть необработанные ставки, то выходим
        */
        if(!$this->em->getRepository("\AppBundle\Entity\TournamentChampionsleagueMatch")->allBetsCalculated($tournamentId, $nonProcessedMatchesIds))
            return true;
        
        $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: SETTING POINTS FOR NON PROCCESSED MATCHES: ".serialize($nonProcessedMatchesIds));
        
        /**
        * Начисляем очки в онлайн таблицу
        */
        $rtResults = $this->em->getRepository("\AppBundle\Entity\TournamentChampionsleagueQualificationRealTimeResult")->findByTournament($tournamentId);
        $nonProcessedMatches = $this->em->getRepository("\AppBundle\Entity\TournamentChampionsleagueMatch")->findByIds($tournamentId, $nonProcessedMatchesIds);
        $roundBets = $this->__getBetsForEachUser($tournamentId, $nonProcessedMatchesIds);
        
        try
        {        
            foreach($rtResults as &$result)
            {                            
                $currentUserResult = $this->__getQualificationResultForUserByRound($result->getUser()->getId(), $nonProcessedMatches, $roundBets);
                
                $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: SETTING REAL TIME RESULTS: ".serialize(array("user" => $result->getUser()->getId(),
                                                                                                                            "results" => $currentUserResult)));
                
                $result->setPlace(0);
                $result->setTotalPoints($result->getTotalPoints() + $currentUserResult["total_points"]);
                $result->setTotalGuessedResults($result->getTotalGuessedResults() + $currentUserResult["total_guessed_results"]);
                $result->setTotalGuessedOutcomes($result->getTotalGuessedOutcomes() + $currentUserResult["total_guessed_outcomes"]);
                $result->setTotalGuessedDifferences($result->getTotalGuessedDifferences() + $currentUserResult["total_guessed_differences"]);            
                $result->setUserRating($result->getUser()->getRating());
                
                $this->em->persist($result);            
            }
        } catch(\Exception $ex)
        {
            $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: EXCEPTION ON SETTING REAL TIME RESULTS: ".$ex->getMessage());            
            $this->em->flush();
            return true;
        }
        
        $this->em->flush();
                
        foreach($nonProcessedMatches as &$tournamentMatch)
        {            
            $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: SETTING MATCH PROCCESSED: ".serialize(array("match_id" => $tournamentMatch->getMatch()->getId())));
            $tournamentMatch->setIsProcessed(true);
            $this->em->persist($tournamentMatch);
        }                
        
        $this->em->flush();                
        
        unset($nonProcessedMatches);
        unset($tournamentMatch);
        unset($rtResults);                
        
        /**
        * Сортируем и проставляем места
        */
        $rtResults = $this->em->getRepository("\AppBundle\Entity\TournamentChampionsleagueQualificationRealTimeResult")->findSorted($tournamentId);
        $place = 1;
        foreach($rtResults as &$result)
        {
            $result->setPlace($place);
            $this->em->persist($result);
            $place++;
        }
        
        $this->em->flush();
        
        unset($rtResults);
        unset($result);
        
        /**
        * Если не все матчи окончились, то выходим
        */
        if(!$this->em->getRepository("\AppBundle\Entity\TournamentChampionsleagueMatch")->allRoundMatchesFinished($tournamentId, $currentRound, true, false))
            return true;
        
        /**
        * Если все матчи в туре закончились, но за какие-то матчи ещё не начислены очки, то выходим
        */
        if(!$this->em->getRepository("\AppBundle\Entity\TournamentChampionsleagueMatch")->matchesPointsCalculated($tournamentId, $currentRound, true, false))
            return true;                
        
        $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: ALL MATCHES FINISHED AND PROCCESSED IN ROUND $currentRound!");    
        
        /**
        * Так как тур окончен, то заносим результаты тура в таблицу результов по турам
        */
        $rtResults = $this->em->getRepository("\AppBundle\Entity\TournamentChampionsleagueQualificationRealTimeResult")->findByTournament($tournamentId);
        foreach($rtResults as &$result)
        {
            $tourResult = new TournamentChampionsleagueQualificationResult();
            $tourResult->setTournament($result->getTournament());
            $tourResult->setUser($result->getUser());
            $tourResult->setRound($currentRound);
            $tourResult->setPlace($result->getPlace());
            $tourResult->setPointsInRound(0);
            $tourResult->setTotalPoints($result->getTotalPoints());
            $tourResult->setTotalGuessedResults($result->getTotalGuessedResults());
            $tourResult->setTotalGuessedOutcomes($result->getTotalGuessedOutcomes());
            $tourResult->setTotalGuessedDifferences($result->getTotalGuessedDifferences());
            $tourResult->setUserRating($result->getUser()->getRating());
            
            $this->em->persist($tourResult);
        }
        
        unset($rtResults);
        unset($result);
        
        $this->em->flush();
        
        /**
        * Переходим в следующий тур, если он есть.
        */        
        if($currentRound + 1 <= $tournament->getQualificationRoundsCount())
        {
            $tournament->setCurrentRound($currentRound + 1);
            $this->em->persist($tournament);
            $this->em->flush();
            
            return null;
        }
        
        /**
        * Или генерируем данные для группового этапа
        */
        
        /**
        * Удаляем результаты реального времени за ненадобностью
        */
        $this->em->getRepository("\AppBundle\Entity\TournamentChampionsleagueQualificationRealTimeResult")->removeByTournament($tournament->getTournament()->getId());
        
        /**
        * Удаляем ставки пользователей, которые не прошли квалификацию на групповой этап и плейофф
        */
        $notInGroupStageUsersIds = $this->em->getRepository("\AppBundle\Entity\TournamentChampionsleagueQualificationResult")->findByTournamentUsersIdsNotInGroupsAndPlayoff($tournament->getTournament()->getId(), $currentRound, $tournament->getRealUsersOutOfQualification());
        $groupMatches = $this->em->getRepository("\AppBundle\Entity\TournamentChampionsleagueMatch")->findByTournamentAndRound($tournament->getTournament()->getId(), $currentRound, false, true);
        $playoffMatches = $this->em->getRepository("\AppBundle\Entity\TournamentChampionsleagueMatch")->findByTournamentAndRound($tournament->getTournament()->getId(), $currentRound, false, false);
        $matchesIds = array();
        foreach($groupMatches as $tournamentMatch)
            $matchesIds[] = $tournamentMatch->getMatch()->getId();
        foreach($playoffMatches as $tournamentMatch)
            $matchesIds[] = $tournamentMatch->getMatch()->getId();
        
        $this->em->getRepository("\AppBundle\Entity\Bet")->removeBetsByUsersIdsAndMatchesIds($tournament->getTournament()->getId(), $notInGroupStageUsersIds, $matchesIds);
        unset($notInGroupStageUsersIds, $groupMatches, $playoffMatches, $matchesIds, $tournamentMatch);
        
        /**
        * Находим пользователей прошедших квалификацию и распихиваем по группам
        */
        $placesForGroups = $this->em->getRepository("\AppBundle\Entity\TournamentChampionsleagueQualificationResult")->findUsersForGroupsByTournamentAndRound($tournament->getTournament()->getId(), $currentRound, $tournament->getRealUsersOutOfQualification());        
        
        $usersInGroups = array();        
        for($placeNum = 1; $placeNum <= $tournament->getRealUsersInGroup(); $placeNum++)
        {
            for($groupNum = 1; $groupNum <= $tournament->getRealGroupsCount(); $groupNum++)
            {
                $groupPlace = array_shift($placesForGroups);
                
                $groupUser = $groupPlace->getUser();
                $usersInGroups[$groupNum][] = $groupUser;
                
                $groupResult = new TournamentChampionsleagueGroupResult();
                $groupResult->setTournament($tournament->getTournament());
                $groupResult->setRound(1);
                $groupResult->setPlace($placeNum);
                $groupResult->setGroupNum($groupNum);
                $groupResult->setUser($groupUser);
                $groupResult->setUserRating($groupUser->getRating());
                
                $this->em->persist($groupResult);
            }
        }        
        
        $this->em->flush();
        
        unset($placesForGroups);        
        unset($groupNum);        
        unset($placeNum);
        unset($groupPlace);
        unset($groupUser);        
        
        /**
        * Генерируем пары для групп
        */
        for($groupNum = 1; $groupNum <= $tournament->getRealGroupsCount(); $groupNum++)
        {
            $groupPairs = $this->roundHelper->generateRoundRobinPairings($tournament->getRealUsersInGroup());
            
            foreach($groupPairs as $roundNum => $roundGames)
            {
                foreach($roundGames as $roundGame)
                {
                    $userLeft = (isset($usersInGroups[$groupNum][$roundGame["user_left"] - 1])) ? $usersInGroups[$groupNum][$roundGame["user_left"] - 1] : null;
                    $userRight =  (isset($usersInGroups[$groupNum][$roundGame["user_right"] - 1])) ? $usersInGroups[$groupNum][$roundGame["user_right"] - 1] : null;
                    
                    if(!$userLeft || !$userRight)
                        continue;
                        
                    $tournamentGame = new TournamentChampionsleagueGroupGame();                    
                    $tournamentGame->setTournament($tournament->getTournament());
                    $tournamentGame->setRound($roundNum);                    
                    $tournamentGame->setGroupNum($groupNum);                    
                    $tournamentGame->setUserLeft($userLeft);
                    $tournamentGame->setUserRight($userRight);
                    
                    $this->em->persist($tournamentGame);                
                }
            }
        }
        
        $this->em->flush();
        
        /*
        * Переводим турнир в стадию группового этапа
        */        
        $tournament->setStage(TournamentChampionsleague::STAGE_GROUP);
        $tournament->setCurrentRound(1);
        $this->em->persist($tournament);                
        
        $this->em->flush();
        
        return true;    
    }
    
    private function processGroupRound(\AppBundle\Entity\TournamentChampionsleague $tournament, $currentRound)
    {            
        $tournamentId = $tournament->getTournament()->getId();
                
        /**
        * Если нет необработанных матчей, которые закончились, то выходим
        */
        if(!$matchesIds = $this->em->getRepository("\AppBundle\Entity\TournamentChampionsleagueMatch")->findNonProcessedMatchesFinished($tournamentId, $currentRound, false, true))
            return true;
            
        //собираем idшники матчей
        $nonProcessedMatchesIds = array();        
        foreach($matchesIds as $matchData)
        {
            $nonProcessedMatchesIds[] = $matchData["id"];
        }        
        unset($matchesIds, $matchData);                
        
        
        if(count($nonProcessedMatchesIds) == 0)
            return true;
            
        /**
        * Если среди необработанных матчей, которые закончились, есть необработанные ставки, то выходим
        */
        if(!$this->em->getRepository("\AppBundle\Entity\TournamentChampionsleagueMatch")->allBetsCalculated($tournamentId, $nonProcessedMatchesIds))
            return true;
        
        $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: SETTING POINTS FOR NON PROCCESSED MATCHES: ".serialize($nonProcessedMatchesIds));
        
        /**
        * Начисляем мячи в игры
        */        
        $groupGames = $this->em->getRepository("AppBundle\Entity\TournamentChampionsleagueGroupGame")->findByTournamentAndRound($tournamentId, $currentRound);
        $nonProcessedMatches = $this->em->getRepository("\AppBundle\Entity\TournamentChampionsleagueMatch")->findByIds($tournamentId, $nonProcessedMatchesIds);
        $roundBets = $this->__getBetsForEachUser($tournamentId, $nonProcessedMatchesIds);
                
        foreach($groupGames as &$game)
        {
            $pointsLeft = intval($game->getPointsLeft());
            $pointsRight = intval($game->getPointsRight());
            
            foreach($roundBets as $matchId => $usersBets)
            {
                $user = $game->getUserLeft();                    
                // начисляем очки левому игроку, если он делал ставку
                if(isset($usersBets[$user->getId()]))
                {
                    $bet = $usersBets[$user->getId()];
                    
                    if(is_null($bet->getPoints()))
                    {
                        $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: EXCEPTION ON SETTING POINTS ON GAME RESULTS: "."Очки пользователю #".$user->getId()." за матч #".$matchId." ещё не начислены!");
                        return false;
                    }                                        
                    $pointsLeft += intval($bet->getPoints());
                }                
                $game->setPointsLeft($pointsLeft);
                
                $user = $game->getUserRight();                    
                // начисляем очки правому игроку, если он делал ставку
                if(isset($usersBets[$user->getId()]))
                {
                    $bet = $usersBets[$user->getId()];
                    
                    if(is_null($bet->getPoints()))
                    {
                        $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: EXCEPTION ON SETTING POINTS ON GAME RESULTS: "."Очки пользователю #".$user->getId()." за матч #".$matchId." ещё не начислены!");
                        return false;
                    }
                    
                    $pointsRight += intval($bet->getPoints());                    
                }
                $game->setPointsRight($pointsRight);
            } //foreach($roundBets as $matchId => $usersBets)
            
            $this->em->persist($game);
            
        } //foreach($groupGames as $game)
        
        $this->em->flush();
        
        unset($usersBets,$roundBets,$matchId,$groupGames,$game);        
        
        foreach($nonProcessedMatches as &$tournamentMatch)
        {
            $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: SETTING MATCH PROCCESSED: ".serialize(array("match_id" => $tournamentMatch->getMatch()->getId())));
            $tournamentMatch->setIsProcessed(true);
            $this->em->persist($tournamentMatch);
        }                
        
        $this->em->flush();
        
        /**
        * Если не все матчи окончились, то выходим
        */
        if(!$this->em->getRepository("\AppBundle\Entity\TournamentChampionsleagueMatch")->allRoundMatchesFinished($tournamentId, $currentRound, false, true))
            return true;
        
        /**
        * Если все матчи в туре закончились, но за какие-то матчи ещё не начислены очки, то выходим
        */
        if(!$this->em->getRepository("\AppBundle\Entity\TournamentChampionsleagueMatch")->matchesPointsCalculated($tournamentId, $currentRound, false, true))
            return true;                
        
        $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: ALL MATCHES FINISHED AND PROCCESSED IN GROUP STAGE IN ROUND $currentRound!");
        
        /**
        * Так как тур окончен, то заносим результаты тура в таблицу групповых результов по турам
        */
        $groupGames = $this->em->getRepository("AppBundle\Entity\TournamentChampionsleagueGroupGame")->findByTournamentAndRound($tournamentId, $currentRound);
        $results = $this->em->getRepository("AppBundle\Entity\TournamentChampionsleagueGroupResult")->findByTournamentAndRound($tournamentId, $currentRound);        
        $groupsResults = array();
        foreach($results as $result)
        {
            $groupsResults[$result->getUser()->getId()] = $result;
        }        
        unset($results, $result);
        
        foreach($groupGames as $game)
        {
            //начисляем очки левому пользователю
            $user = $game->getUserLeft();            
            $result = $groupsResults[$user->getId()];            
                        
            $result->setTotalGames($result->getTotalGames() + 1);
            $result->setUserRating($user->getRating());
            $result->setTotalGoalsScored($result->getTotalGoalsScored() + $game->getPointsLeft());
            $result->setTotalGoalsAgainst($result->getTotalGoalsAgainst() + $game->getPointsRight());
            $result->setTotalGoalsDifference($result->getTotalGoalsScored() - $result->getTotalGoalsAgainst());
                
            if($game->getPointsLeft() > $game->getPointsRight())
            {
                $result->setTotalPoints($result->getTotalPoints() + self::POINTS_FOR_WIN);
                $result->setTotalWins($result->getTotalWins() + 1);
            }
            elseif($game->getPointsLeft() < $game->getPointsRight())
            {
                $result->setTotalPoints($result->getTotalPoints() + self::POINTS_FOR_LOSS);
                $result->setTotalLoss($result->getTotalLoss() + 1);
            }
            elseif($game->getPointsLeft() == $game->getPointsRight())
            {
                $result->setTotalPoints($result->getTotalPoints() + self::POINTS_FOR_DRAW);
                $result->setTotalDraws($result->getTotalDraws() + 1);
            }        
            $this->em->persist($result);
            
            //начисляем очки правому пользователю
            $user = $game->getUserRight();            
            $result = $groupsResults[$user->getId()];            
                        
            $result->setTotalGames($result->getTotalGames() + 1);
            $result->setUserRating($user->getRating());
            $result->setTotalGoalsScored($result->getTotalGoalsScored() + $game->getPointsRight());
            $result->setTotalGoalsAgainst($result->getTotalGoalsAgainst() + $game->getPointsLeft());
            $result->setTotalGoalsDifference($result->getTotalGoalsScored() - $result->getTotalGoalsAgainst());
                
            if($game->getPointsLeft() < $game->getPointsRight())
            {
                $result->setTotalPoints($result->getTotalPoints() + self::POINTS_FOR_WIN);
                $result->setTotalWins($result->getTotalWins() + 1);
            }
            elseif($game->getPointsLeft() > $game->getPointsRight())
            {
                $result->setTotalPoints($result->getTotalPoints() + self::POINTS_FOR_LOSS);
                $result->setTotalLoss($result->getTotalLoss() + 1);
            }
            elseif($game->getPointsLeft() == $game->getPointsRight())
            {
                $result->setTotalPoints($result->getTotalPoints() + self::POINTS_FOR_DRAW);
                $result->setTotalDraws($result->getTotalDraws() + 1);
            }
            $this->em->persist($result);
        }
        
        $this->em->flush();
        unset($user,$result,$game,$groupGames,$groupsResults);
        
        /**
        * Сортируем результаты по регламенту в каждой группе
        */
        for($groupNum = 1; $groupNum <= $tournament->getRealGroupsCount(); $groupNum++)
        {
            $sortedResults = $this->em->getRepository("AppBundle\Entity\TournamentChampionsleagueGroupResult")->findSortedByTournamentAndGroupAndRound($tournamentId, $currentRound, $groupNum);
            $place = 1;
            foreach($sortedResults as $result)
            {
                $result->setPlace($place);
                $this->em->persist($result);
                $place++;
            }
        }
        $this->em->flush();
        unset($groupNum,$sortedResults,$result,$place);
        
        /**
        * Предзаполняем результаты для следующего тура        
        */
        $prevRoundResults = $this->em->getRepository("AppBundle\Entity\TournamentChampionsleagueGroupResult")->findByTournamentAndRound($tournamentId, $currentRound);
        foreach($prevRoundResults as $result)
        {
            $TournamentChampionsleagueGroupResult =  new TournamentChampionsleagueGroupResult();
            $TournamentChampionsleagueGroupResult->setTournament($result->getTournament());
            $TournamentChampionsleagueGroupResult->setGroupNum($result->getGroupNum());
            $TournamentChampionsleagueGroupResult->setUser($result->getUser());
            $TournamentChampionsleagueGroupResult->setRound($currentRound + 1);
            $TournamentChampionsleagueGroupResult->setPlace($result->getPlace());
            $TournamentChampionsleagueGroupResult->setTotalGames($result->getTotalGames());
            $TournamentChampionsleagueGroupResult->setTotalWins($result->getTotalWins());
            $TournamentChampionsleagueGroupResult->setTotalDraws($result->getTotalDraws());
            $TournamentChampionsleagueGroupResult->setTotalLoss($result->getTotalLoss());
            $TournamentChampionsleagueGroupResult->setTotalGoalsAgainst($result->getTotalGoalsAgainst());
            $TournamentChampionsleagueGroupResult->setTotalGoalsScored($result->getTotalGoalsScored());
            $TournamentChampionsleagueGroupResult->setTotalGoalsDifference($result->getTotalGoalsDifference());
            $TournamentChampionsleagueGroupResult->setTotalPoints($result->getTotalPoints());
            $TournamentChampionsleagueGroupResult->setUserRating($result->getUser()->getRating());            
            
            $this->em->persist($TournamentChampionsleagueGroupResult);
        }
        $this->em->flush();        
        unset($prevRoundResults,$result,$TournamentChampionsleagueGroupResult);
        
        /**
        * Переходим в следующий тур, если он есть.
        */
        if($currentRound + 1 <= $tournament->getRealGroupRoundsCount())
        {
            $tournament->setCurrentRound($currentRound + 1);
            $this->em->persist($tournament);
            $this->em->flush();
            return true;
        }
        
        /**
        * Или генерируем пары для плейофф
        */
        $prevRoundResults = $this->em->getRepository("AppBundle\Entity\TournamentChampionsleagueGroupResult")->findByTournamentAndRoundForPlayoff($tournamentId, $currentRound, $tournament->getRealUsersOutOfGroup());
        $usersByGroups = array();
        foreach($prevRoundResults as $groupResults)
        {
            $usersByGroups[$groupResults->getGroupNum()][] = $groupResults->getUser();
        }        
        
        unset($prevRoundResults, $groupResults, $i);
        
        for($groupNum = 1; $groupNum <= $tournament->getRealGroupsCount(); $groupNum = $groupNum + 2)
        {
            $firstGroupNum = $groupNum;
            $secondGroupNum = $groupNum + 1;

            for($i = 0; $i < $tournament->getRealUsersOutOfGroup(); $i = $i + 2)
            {
                $TournamentPlayoffGame = new TournamentChampionsleaguePlayoffGame();
                $TournamentPlayoffGame->setTournament($tournament->getTournament());
                $TournamentPlayoffGame->setRound(1);
                $TournamentPlayoffGame->setUserLeft($usersByGroups[$firstGroupNum][$i]);
                $TournamentPlayoffGame->setUserRight($usersByGroups[$secondGroupNum][$i+1]);

                $this->em->persist($TournamentPlayoffGame);
                
                $TournamentPlayoffGame = new TournamentChampionsleaguePlayoffGame();
                $TournamentPlayoffGame->setTournament($tournament->getTournament());
                $TournamentPlayoffGame->setRound(1);
                $TournamentPlayoffGame->setUserLeft($usersByGroups[$firstGroupNum][$i+1]);
                $TournamentPlayoffGame->setUserRight($usersByGroups[$secondGroupNum][$i]);

                $this->em->persist($TournamentPlayoffGame);                            
            }
        }
        
        $this->em->flush();
        
        $this->em->getRepository("AppBundle\Entity\TournamentChampionsleagueGroupResult")->removeUsersBetsThatNotInPlayoff($tournamentId, $currentRound, $tournament->getRealUsersOutOfGroup());
        
        /**
        * Переходим в стадию плей-офф
        */
        $tournament->setStage(TournamentChampionsleague::STAGE_PLAYOFF);
        $tournament->setCurrentRound(1);
        $this->em->persist($tournament);
        $this->em->flush();        
       
        return true;
    }        
    
    private function processPlayoffRound(\AppBundle\Entity\TournamentChampionsleague $tournament, $currentRound)
    {
        $tournamentId = $tournament->getTournament()->getId();
                
        /**
        * Если нет необработанных матчей, которые закончились, то выходим
        */
        if(!$matchesIds = $this->em->getRepository("\AppBundle\Entity\TournamentChampionsleagueMatch")->findNonProcessedMatchesFinished($tournamentId, $currentRound, false, false))
            return true;
            
        //собираем idшники матчей
        $nonProcessedMatchesIds = array();        
        foreach($matchesIds as $matchData)
        {
            $nonProcessedMatchesIds[] = $matchData["id"];
        }        
        unset($matchesIds, $matchData);                
        
        
        if(count($nonProcessedMatchesIds) == 0)
            return true;
            
        /**
        * Если среди необработанных матчей, которые закончились, есть необработанные ставки, то выходим
        */
        if(!$this->em->getRepository("\AppBundle\Entity\TournamentChampionsleagueMatch")->allBetsCalculated($tournamentId, $nonProcessedMatchesIds))
            return true;
        
        $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: SETTING POINTS FOR NON PROCCESSED MATCHES: ".serialize($nonProcessedMatchesIds));
        
        /**
        * Начисляем мячи в игры
        */        
        $playoffGames = $this->em->getRepository("AppBundle\Entity\TournamentChampionsleaguePlayoffGame")->findByTournamentAndRound($tournamentId, $currentRound);
        $nonProcessedMatches = $this->em->getRepository("\AppBundle\Entity\TournamentChampionsleagueMatch")->findByIds($tournamentId, $nonProcessedMatchesIds);
        $roundBets = $this->__getBetsForEachUser($tournamentId, $nonProcessedMatchesIds);
        
        /* @var $game \AppBundle\Entity\TournamentChampionsleaguePlayoffGame */
        foreach($playoffGames as &$game)
        {                                
            $pointsLeft = intval($game->getPointsLeft());
            $totalGuessedResultsLeft = intval($game->getTotalGuessedResultsLeft());
            $totalGuessedOutcomesLeft = intval($game->getTotalGuessedOutcomesLeft());
            $totalGuessedDifferencesLeft = intval($game->getTotalGuessedDifferencesLeft());
            
            $pointsRight = intval($game->getPointsRight());
            $totalGuessedResultsRight = intval($game->getTotalGuessedResultsRight());
            $totalGuessedOutcomesRight = intval($game->getTotalGuessedOutcomesRight());
            $totalGuessedDifferencesRight = intval($game->getTotalGuessedDifferencesRight());
            
            foreach($roundBets as $matchId => $usersBets)
            {
                $user = $game->getUserLeft();                    
                // начисляем очки левому игроку, если он делал ставку
                if(isset($usersBets[$user->getId()]))
                {
                    $bet = $usersBets[$user->getId()];
                    
                    if(is_null($bet->getPoints()))
                    {
                        $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: EXCEPTION ON SETTING POINTS ON GAME RESULTS: "."Очки пользователю #".$user->getId()." за матч #".$matchId." ещё не начислены!");
                        return false;
                    }                                        
                    $pointsLeft += intval($bet->getPoints());
                    
                    if($this->pointsRuler->scoreGuessed($bet, $bet->getMatch()))
                    {
                        $totalGuessedResultsLeft++;                        
                        $totalGuessedDifferencesLeft++;
                        $totalGuessedOutcomesLeft++;
                    }
                    elseif($this->pointsRuler->scoreDifferenceGuessed($bet, $bet->getMatch()))
                    {                        
                        $totalGuessedDifferencesLeft++;
                        $totalGuessedOutcomesLeft++;                        
                    }
                    elseif($this->pointsRuler->matchOutcomeGuessed($bet, $bet->getMatch()))
                    {
                        $totalGuessedOutcomesLeft++;                        
                    }                    
                }                
                $game->setPointsLeft($pointsLeft);                
                $game->setTotalGuessedResultsLeft($totalGuessedResultsLeft);
                $game->setTotalGuessedDifferencesLeft($totalGuessedDifferencesLeft);
                $game->setTotalGuessedOutcomesLeft($totalGuessedOutcomesLeft);
                $game->setUserRatingLeft($user->getRating());
                
                
                $user = $game->getUserRight();                    
                // начисляем очки правому игроку, если он делал ставку
                if(isset($usersBets[$user->getId()]))
                {
                    $bet = $usersBets[$user->getId()];
                    
                    if(is_null($bet->getPoints()))
                    {
                        $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: EXCEPTION ON SETTING POINTS ON GAME RESULTS: "."Очки пользователю #".$user->getId()." за матч #".$matchId." ещё не начислены!");
                        return false;
                    }
                    
                    $pointsRight += intval($bet->getPoints());
                    
                    if($this->pointsRuler->scoreGuessed($bet, $bet->getMatch()))
                    {
                        $totalGuessedResultsRight++;                        
                        $totalGuessedDifferencesRight++;
                        $totalGuessedOutcomesRight++;
                    }
                    elseif($this->pointsRuler->scoreDifferenceGuessed($bet, $bet->getMatch()))
                    {                        
                        $totalGuessedDifferencesRight++;
                        $totalGuessedOutcomesRight++;                        
                    }
                    elseif($this->pointsRuler->matchOutcomeGuessed($bet, $bet->getMatch()))
                    {
                        $totalGuessedOutcomesRight++;                        
                    }
                }
                $game->setPointsRight($pointsRight);
                $game->setTotalGuessedResultsRight($totalGuessedResultsRight);
                $game->setTotalGuessedDifferencesRight($totalGuessedDifferencesRight);
                $game->setTotalGuessedOutcomesRight($totalGuessedOutcomesRight);
                $game->setUserRatingRight($user->getRating());
            } //foreach($roundBets as $matchId => $usersBets)
            
            $this->em->persist($game);
            
        } //foreach($groupGames as $game)
        
        $this->em->flush();
        
        unset($usersBets,$roundBets,$matchId,$playoffGames,$game);        
        
        foreach($nonProcessedMatches as &$tournamentMatch)
        {
            $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: SETTING MATCH PROCCESSED: ".serialize(array("match_id" => $tournamentMatch->getMatch()->getId())));
            $tournamentMatch->setIsProcessed(true);
            $this->em->persist($tournamentMatch);
        }                
        
        $this->em->flush();
        
        /**
        * Если не все матчи игры окончились, то выходим
        */
        if(!$this->em->getRepository("\AppBundle\Entity\TournamentChampionsleagueMatch")->allRoundMatchesFinished($tournamentId, $currentRound, false, false))
            return true;
        
        /**
        * Если все матчи в туре закончились, но за какие-то матчи ещё не начислены очки, то выходим
        */
        if(!$this->em->getRepository("\AppBundle\Entity\TournamentChampionsleagueMatch")->matchesPointsCalculated($tournamentId, $currentRound, false, false))
            return true;                
        
        $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: ALL MATCHES FINISHED AND PROCCESSED IN ROUND $currentRound!");    
        
        /**
        * Так как тур окончен, то сохраняем победителей тура
        */
        $playoffGames = $this->em->getRepository("AppBundle\Entity\TournamentChampionsleaguePlayoffGame")->findByTournamentAndRound($tournamentId, $currentRound);
        
        $looseUsersIds = array();
        foreach($playoffGames as &$game)
        {
            if($game->getPointsLeft() > $game->getPointsRight())
            {                
                $game->setUserLeftIsWinner(true);
                $game->setUserRightIsWinner(false);
                $looseUsersIds[] = $game->getUserRight()->getId();
            }
            elseif($game->getPointsLeft() < $game->getPointsRight())
            {                
                $game->setUserLeftIsWinner(false);
                $game->setUserRightIsWinner(true);
                $looseUsersIds[] = $game->getUserLeft()->getId();
            }
            elseif($game->getTotalGuessedResultsLeft() > $game->getTotalGuessedResultsRight())
            {                
                $game->setUserLeftIsWinner(true);
                $game->setUserRightIsWinner(false);
                $looseUsersIds[] = $game->getUserRight()->getId();
            }
            elseif($game->getTotalGuessedResultsLeft() < $game->getTotalGuessedResultsRight())
            {                
                $game->setUserLeftIsWinner(false);
                $game->setUserRightIsWinner(true);
                $looseUsersIds[] = $game->getUserLeft()->getId();
            }
            elseif($game->getTotalGuessedDifferencesLeft() > $game->getTotalGuessedDifferencesRight())
            {                
                $game->setUserLeftIsWinner(true);
                $game->setUserRightIsWinner(false);
                $looseUsersIds[] = $game->getUserRight()->getId();
            }
            elseif($game->getTotalGuessedDifferencesLeft() < $game->getTotalGuessedDifferencesRight())
            {                
                $game->setUserLeftIsWinner(false);
                $game->setUserRightIsWinner(true);
                $looseUsersIds[] = $game->getUserLeft()->getId();
            }
            elseif($game->getTotalGuessedOutcomesLeft() > $game->getTotalGuessedOutcomesRight())
            {                
                $game->setUserLeftIsWinner(true);
                $game->setUserRightIsWinner(false);
                $looseUsersIds[] = $game->getUserRight()->getId();
            }
            elseif($game->getTotalGuessedOutcomesLeft() < $game->getTotalGuessedOutcomesRight())
            {                
                $game->setUserLeftIsWinner(false);
                $game->setUserRightIsWinner(true);
                $looseUsersIds[] = $game->getUserLeft()->getId();
            }
            elseif($game->getUserRatingLeft() > $game->getUserRatingRight())
            {                
                $game->setUserLeftIsWinner(true);
                $game->setUserRightIsWinner(false);
                $looseUsersIds[] = $game->getUserRight()->getId();
            }
            elseif($game->getUserRatingLeft() < $game->getUserRatingRight())
            {                
                $game->setUserLeftIsWinner(false);
                $game->setUserRightIsWinner(true);
                $looseUsersIds[] = $game->getUserLeft()->getId();
            }
            else
            {
                $tmp = array("left", "right");
                $randomNum = rand(0, 1);
                
                if($tmp[$randomNum] == "left")
                {                    
                    $game->setUserLeftIsWinner(true);
                    $game->setUserRightIsWinner(false);
                    $looseUsersIds[] = $game->getUserRight()->getId();
                }
                else
                {                 
                    $game->setUserLeftIsWinner(false);
                    $game->setUserRightIsWinner(true);
                    $looseUsersIds[] = $game->getUserLeft()->getId();
                    
                }
                
                unset($tmp, $randomNum);
            }
            
            $this->em->persist($game);            
        }
        
        $this->em->flush();
        
        /**
        * Если это последний тур, то записываем побидетелй в таблицу результатов для подсчёта
        */
        if($tournament->getRealPlayoffRoundsCount() == $currentRound)
        {
            $maxPlace = $this->em->getRepository("AppBundle\Entity\WinRule")->findMaxPlaceByFormatAndWinnersCount($tournament->getTournament()->getFormat()->getInternalName(), $tournament->getRealUsersCount());
            
            $place = 1;
            
            //Итерируем по раундам начиная с Финала
            for($round = $tournament->getRealPlayoffRoundsCount(); $round >= 1; $round--)
            {
                //Если текущее место больше чем определено, выходим
                if($place > $maxPlace)
                    break;
                
                //Достаём игры текущего раунда
                $playoffGames = $this->em->getRepository("AppBundle\Entity\TournamentChampionsleaguePlayoffGame")->findByTournamentAndRound($tournamentId, $round);                                
                
                //Если финал, то распределяем первое и второе место
                if($round == $tournament->getRealPlayoffRoundsCount())
                {
                    //В финале всегда одна игра
                    $game = $playoffGames[0];
                    
                    if($game->getUserLeftIsWinner())
                    {
                        $winnerUser = $game->getUserLeft();
                        $looserUser = $game->getUserRight();
                    }
                    elseif($game->getUserRightIsWinner())
                    {
                        $winnerUser = $game->getUserRight();
                        $looserUser = $game->getUserLeft();
                    }                    
                    
                    //первое место
                    $TournamentResult = new TournamentResult();
                    $TournamentResult->setTournament($tournament->getTournament());
                    $TournamentResult->setPlace($place);                    
                                                            
                    $TournamentResult->setUser($winnerUser);
                    
                    $this->em->persist($TournamentResult);
                    
                    $place++;
                    
                    //второе место
                    $TournamentResult = new TournamentResult();
                    $TournamentResult->setTournament($tournament->getTournament());
                    $TournamentResult->setPlace($place);
                                                            
                    $TournamentResult->setUser($looserUser);
                    
                    $this->em->persist($TournamentResult);
                    
                    $place++;
                    
                    continue;
                }
                
                foreach($playoffGames as $game)
                {
                    //Ставим на текущее место проигравших
                    $TournamentResult = new TournamentResult();
                    $TournamentResult->setTournament($tournament->getTournament());
                    $TournamentResult->setPlace($place);
                    
                    $looserUser = ($game->getUserLeftIsWinner()) ? $game->getUserRight() : $game->getUserLeft();
                    $TournamentResult->setUser($looserUser);
                    
                    $this->em->persist($TournamentResult);                    
                }
                
                $place++;
            }
            
            $this->em->flush();
            
            //Заканчиваем турнир
            $tournament->getTournament()->setIsOver(true);
            $tournament->getTournament()->setIsResultsReady(true);
            $tournament->getTournament()->setEndDatetime( new \DateTime() );
            
            $this->em->flush();
            
            return true;
        }
        
        /**
        * Если тур не последний, то нужно сгенерировать новые пары на новый тур
        */
        $playoffGames = $this->em->getRepository("AppBundle\Entity\TournamentChampionsleaguePlayoffGame")->findByTournamentAndRound($tournamentId, $currentRound);
                
        /**
        * Генерируем пары по схеме 1-3, 2-4, 5-7, 6-8 и т.д.
        */
        for($i = 0; $i < count($playoffGames); $i = $i + 4)
        {                        
            /**            
            * @var $firstGame \AppBundle\Entity\TournamentChampionsleaguePlayoffGame
            */
            $firstGame = $playoffGames[$i];
            /**            
            * @var $secondGame \AppBundle\Entity\TournamentChampionsleaguePlayoffGame
            */
            $secondGame = $playoffGames[$i + 2];
            
            /**
            * Если полуфинал, то генерируем по схеме 1-2.
            */
            if(count($playoffGames) <= 2)
            {
                $firstGame = $playoffGames[$i];
                $secondGame = $playoffGames[$i + 1];
            }
            
            $userLeft = null;
            $userRight = null;
            
            if($firstGame->getUserLeftIsWinner())            
                $userLeft = $firstGame->getUserLeft();
            elseif($firstGame->getUserRightIsWinner())
                $userLeft = $firstGame->getUserRight();
                
            if($secondGame->getUserLeftIsWinner())            
                $userRight = $secondGame->getUserLeft();
            elseif($secondGame->getUserRightIsWinner())
                $userRight = $secondGame->getUserRight();
                
            $game = new TournamentChampionsleaguePlayoffGame();
            $game->setTournament($tournament->getTournament());
            $game->setRound($currentRound + 1);
            $game->setUserLeft($userLeft);
            $game->setUserRight($userRight);
            
            $this->em->persist($game);                
            
            /**
            * Если полуфинал, то вторая пара не нужна
            */
            if(count($playoffGames) <= 2)
                continue;
            
            /**            
            * @var $firstGame \AppBundle\Entity\TournamentChampionsleaguePlayoffGame
            */
            $firstGame = $playoffGames[$i + 1];
            /**            
            * @var $secondGame \AppBundle\Entity\TournamentChampionsleaguePlayoffGame
            */
            $secondGame = $playoffGames[$i + 3];
            
            
            $userLeft = null;
            $userRight = null;
            
            if($firstGame->getUserLeftIsWinner())            
                $userLeft = $firstGame->getUserLeft();
            elseif($firstGame->getUserRightIsWinner())
                $userLeft = $firstGame->getUserRight();
                
            if($secondGame->getUserLeftIsWinner())            
                $userRight = $secondGame->getUserLeft();
            elseif($secondGame->getUserRightIsWinner())
                $userRight = $secondGame->getUserRight();
                
            $game = new TournamentChampionsleaguePlayoffGame();
            $game->setTournament($tournament->getTournament());
            $game->setRound($currentRound + 1);
            $game->setUserLeft($userLeft);
            $game->setUserRight($userRight);
            
            $this->em->persist($game);
        }
        
        $this->em->flush();
        unset($playoffGames, $i, $firstGame, $secondGame, $userLeft, $userRight, $game);
        
        /**
        * Удаляем ставки пользователей которые не прошли в следующий тур
        */
        $playoffNextRoundsMatches = $this->em->getRepository("\AppBundle\Entity\TournamentChampionsleagueMatch")->findIdsPlayoffNextRoundByTournament($tournamentId, $currentRound + 1);
        $matchesIds = array();
        foreach($playoffNextRoundsMatches as $match)
            $matchesIds[] = $match["id"];
        unset($playoffNextRoundsMatches, $match);
        
        $this->em->getRepository("\AppBundle\Entity\Bet")->removeBetsByUsersIdsAndMatchesIds($tournamentId, $looseUsersIds, $matchesIds);
        
        unset($matchesIds, $looseUsersIds);
        
        /**
        * Переходим в следующий тур
        */
        if($currentRound + 1 <= $tournament->getRealPlayoffRoundsCount())
        {
            $tournament->setCurrentRound($currentRound + 1);
            $this->em->persist($tournament);
            $this->em->flush();
            
            return true;
        }        
    }
    
    /**
    * Получает ставки участников по id турнира и номеру тура и распределяет их по матчам и пользователям
    * 
    * @param int $tournament_id
    * @param int $round
    * @return array $userBets
    */
    private function __getBetsForEachUser($tournament_id, $matchesIds)
    {
        $bets = $this->em->getRepository("\AppBundle\Entity\Bet")->findByTournamentAndMatchesIds($tournament_id, $matchesIds);
        
        $userBets = array();
        foreach($bets as $bet)
        {
            $userBets[$bet->getMatch()->getId()][$bet->getUser()->getId()] = $bet;
        }
        unset($bets, $bet);
        
        return $userBets;
    }
    
    private function __getQualificationResultForUserByRound($user_id, $tournamentMatches, $bets)
    {
        $totalPoints = 0;        
        $totalGuessedResults = 0;
        $totalGuessedOutcomes = 0;
        $totalGuessedDifferences = 0;                
        
        foreach($tournamentMatches as $tournamentMatch)
        {
            $match = $tournamentMatch->getMatch();
            
            if($match->getIsRejected())
                continue;
            
            /**
            * Если пользователь не делал ставок на матч, не учитываем его
            */
            if(!isset($bets[$match->getId()][$user_id]))
                continue;
            
            $bet = $bets[$match->getId()][$user_id];
            
            if(is_null($bet->getPoints()))
            {
                throw new \Exception("Очки пользователю #".$user_id." за матч #".$match->getId()." ещё не начислены!");
                return false;
            }
            
            $totalPoints += $bet->getPoints();
                        
            if($this->pointsRuler->scoreGuessed($bet, $match))
            {
                $totalGuessedResults++;
                $totalGuessedDifferences++;
                $totalGuessedOutcomes++;

            }
            elseif($this->pointsRuler->scoreDifferenceGuessed($bet, $match))
            {
                $totalGuessedDifferences++;
                $totalGuessedOutcomes++;
            }
            elseif($this->pointsRuler->matchOutcomeGuessed($bet, $match))            
            {
                $totalGuessedOutcomes++;
            }
            
        }                
        
        return array("total_points" => $totalPoints,                     
                     "total_guessed_results" => $totalGuessedResults,
                     "total_guessed_outcomes" => $totalGuessedOutcomes,
                     "total_guessed_differences" => $totalGuessedDifferences);
    }
    
    /**
    * Event Handlers:
    */
    
    /**
    * Обработчик события добавления матча в турнир
    * 
    * @param \AppBundle\Event\TournamentEvent $event
    */
    public function onTournamentMatchAdded(\AppBundle\Event\TournamentEvent $event)
    {
      /**      
      * @var \AppBundle\Entity\TournamentChampionsleague
      */
      $championsleague = $event->getTournament();                  
            
      /**      
      * @var \AppBundle\Entity\Tournament
      */
      $tournament = $championsleague->getTournament();
      
      $this->updateTournamentStartDatetime($tournament);      
    }
    
    /**
    * Обработчик события удаления матча из турнира
    * 
    * @param \AppBundle\Event\TournamentEvent $event
    */
    public function onTournamentMatchDeleted(\AppBundle\Event\TournamentEvent $event)
    {      
      /**      
      * @var \AppBundle\Entity\TournamentChampionsleague
      */
      $championsleague = $event->getTournament();
            
      /**      
      * @var \AppBundle\Entity\Tournament
      */
      $tournament = $championsleague->getTournament();
      
      $this->updateTournamentStartDatetime($tournament);
    }
    
    public function onTournamentStartedHandler(\AppBundle\Event\TournamentEvent $event)
    {
        /**        
        * @var \AppBundle\Entity\TournamentChampionsleague $tournament
        */
        $tournament = $event->getTournament();

        $realUsersCount = count($tournament->getTournament()->getUsers());        
        $tournament->setRealUsersCount($realUsersCount);
        
        /*
        * Если количество игроков максимально возможное, то применяем правила, установленные создателем турнира
        */ 
        if($realUsersCount == $tournament->getMaxUsersCount())
        {
            $tournament->setRealUsersOutOfQualification($tournament->getMaxUsersCount());            
            $tournament->setRealGroupsCount($tournament->getMaxGroupsCount());
            $tournament->setRealUsersInGroup($tournament->getMaxUsersCount()/$tournament->getMaxGroupsCount());
            $tournament->setRealUsersOutOfGroup($tournament->getUsersOutOfGroup());            
        }
        else
        {
            /**                        
            * Проставляем real_users_out_of_qualification - Максимальный выбор из возможного            
            */            
            $maxUsersCountChoices = array();
            for($exp = 2; pow(2, $exp) <= $this->getRule("max_max_users_count"); $exp++)
                $maxUsersCountChoices[pow(2, $exp)] = pow(2, $exp);
                
            foreach($maxUsersCountChoices as $choice)
            {                                
                if($choice > $realUsersCount)
                    break;

                $realUsersOutOfQualification = $choice;
            }
            
            $tournament->setRealUsersOutOfQualification($realUsersOutOfQualification);
            
            /**                        
            * real_users_in_group - По умолчанию: 4
            */
            $tournament->setRealUsersInGroup(4);
            
            /**                        
            * real_groups_count - Кол-во вышедших из квалификации/кол-во участников в группе
            */
            $tournament->setRealGroupsCount($tournament->getRealUsersOutOfQualification()/$tournament->getRealUsersInGroup());
            
            /**
            * real_users_out_of_group - По умолчанию: 2
            */
            $tournament->setRealUsersOutOfGroup(2);
        }
        
        /**
        * Выясняем реальное кол-во туров в групповом этапе        
        */
        $groupRoundsCount = ($tournament->getRealUsersInGroup() % 2 == 0) ? $tournament->getRealUsersInGroup() - 1 : $tournament->getRealUsersInGroup();
        $tournament->setRealGroupRoundsCount($groupRoundsCount);
        
        /**
        * Реальное кол-во участников плей-офф: Кол-во выходящих из группы*Кол-во групп
        */
        $realUsersCountInPlayoff = $tournament->getRealUsersOutOfGroup()*$tournament->getRealGroupsCount();
        $playoffRoundsCount = round(log($realUsersCountInPlayoff, 2));
        $tournament->setRealPlayoffRoundsCount($playoffRoundsCount);
        
        $this->em->persist($tournament);
        $this->em->flush();
        
        /**
        * Удаляем ненужные матчи и ставки
        */
        $this->em->getRepository('AppBundle\Entity\TournamentChampionsleagueMatch')->removeUnneededMatchesAndBets($tournament->getTournament()->getId(), $groupRoundsCount, $playoffRoundsCount);
        
        /**
        * Заполняем таблицу онлайн результатов для квалификации
        */
        $tournamentUsers = $this->em->getRepository('AppBundle\Entity\User')->findTournamentUsersOrderedByRating($tournament->getTournament()->getId());
        
        $place = 1;
        foreach($tournamentUsers as $user)
        {
            $rtResult = new TournamentChampionsleagueQualificationRealTimeResult();
            $rtResult->setTournament($tournament->getTournament());
            $rtResult->setUser($user);
            $rtResult->setPlace($place);
            $rtResult->setUserRating($user->getRating());
            
            $this->em->persist($rtResult);
            
            $place++;
        }
         
        $this->em->flush();
    }
    
    public function onTournamentUserAddedHandler(\AppBundle\Event\TournamentEvent $event)
    {
        /**        
        * @var \AppBundle\Entity\TournamentChampionsleague $tournament
        */
        $tournament = $event->getTournament();
        
        $usersCount = $this->em->getRepository("\AppBundle\Entity\User")->findChampionshipUsersCount($tournament->getTournament()->getId());
        
        if($usersCount >= $tournament->getMaxUsersCount())
        {
            $tournament->getTournament()->setIsAccessible(false);
            
            $this->em->persist($tournament->getTournament());
            $this->em->flush();
        }
    }
    
}