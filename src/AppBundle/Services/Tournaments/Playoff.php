<?php
namespace AppBundle\Services\Tournaments;

use AppBundle\Services\LogHelper;
use AppBundle\Entity\TournamentPlayoff;
use AppBundle\Entity\TournamentPlayoffQualificationRealTimeResult;
use AppBundle\Entity\TournamentPlayoffQualificationResult;
use AppBundle\Entity\TournamentPlayoffGame;
use AppBundle\Entity\TournamentResult;

class Playoff
{
    /**
    * @var \AppBundle\Entity\TournamentPlayoffRepository
    */
    public $tournamentRep;

    /**
    * @var \AppBundle\Entity\TournamentPlayoffGameRepository
    */
    private $gamesRep;

    /**
    * @var \AppBundle\Entity\BetRepository
    */
    private $betsRep;

    private $em;
    private $pointsRuler;
    private $logHelper;
    private $rules;
    private $playoffMap;

    /**
    * @var \AppBundle\Entity\TournamentPlayoffMatchRepository
    */
    private $matchesRep;

    /**
    * Количество очков начисляемое за победу в игре
    */
    const POINTS_FOR_WIN = 3;

    /**
    * Количество очков начисляемое за поражение в игре
    */
    const POINTS_FOR_LOSS = 0;

    /**
    * Количество очков начисляемое за ничью в игре
    */
    const POINTS_FOR_DRAW = 1;

    /**
    * Минимальное количество туров квалификации
    */
    const MIN_QUALIFICATION_ROUNDS_COUNT = 1;

    /**
    * Минимальное количество матчей в туре квалификации
    */
    const MIN_MATCHES_IN_QUALIFICATION = 1;

    /**
    * Минимальное количество матчей в игре
    */
    const MIN_MATCHES_IN_GAME = 1;

    /**
    * Минимальное количество участников
    */
    const MIN_USERS_COUNT = 2;

    /**
    * Минимальное количество участников
    */
    const MAX_USERS_COUNT = 256;

    public function __construct(\Doctrine\ORM\EntityManager $em, \AppBundle\Services\PointsRuler $pointsRuler)
    {
        $this->em = $em;

        $this->pointsRuler = $pointsRuler;
        $this->logHelper = new LogHelper($this->em);

        $this->matchesRep = $this->em->getRepository('AppBundle\Entity\TournamentPlayoffMatch');
        $this->tournamentRep = $this->em->getRepository('AppBundle\Entity\TournamentPlayoff');
        $this->gamesRep = $this->em->getRepository('AppBundle\Entity\TournamentPlayoffGame');
        $this->betsRep = $this->em->getRepository('AppBundle\Entity\Bet');

        $this->rules = array(
            "min_qualification_rounds_count" => 1,
            "min_matches_in_qualification" => 1,
            "min_matches_in_game" => 1,
            "min_matches_in_playoff" => 1,
            "min_max_users_count" => 2,
            "max_max_users_count" => 256
        );

        $this->playoffMap = array(
            "2" => array(
                "1" => "2"
            ),
            "4" => array(
                "1" => "4",
                "2" => "3"
            ),
            "8" => array(
                "1" => "8",
                "4" => "5",
                "2" => "7",
                "3" => "6"
            ),
            "16" => array(
                "1" => "16",
                "8" => "9",
                "5" => "12",
                "4" => "13",
                "3" => "14",
                "6" => "11",
                "7" => "10",
                "2" => "15",
            ),
            "32" => array(
                "1"  => "32",
                "16" => "17",
                "9"  => "24",
                "8"  => "25",
                "5"  => "28",
                "12" => "21",
                "13" => "20",
                "4"  => "29",
                "3"  => "30",
                "14" => "19",
                "11" => "22",
                "6"  => "27",
                "7"  => "26",
                "10" => "23",
                "15" => "28",
                "2"  => "31",
            )
        );
    }

    public function getRule($ruleName)
    {
        return $this->rules[$ruleName];
    }

    public function getPlayoffMap($usersCount)
    {
        return $this->playoffMap[$usersCount];
    }

    /**
    * Валидация настроек турнира
    *
    * @param \AppBundle\Entity\TournamentPlayoff $playoff
    * @throws \Exception
    * @return bool
    */
    public function validate(\AppBundle\Entity\TournamentPlayoff $playoff)
    {
      if($playoff->getQualificationRoundsCount() < self::MIN_QUALIFICATION_ROUNDS_COUNT)
      {
        throw new \Exception("Количество туров квалификации не может быть меньше ".self::MIN_QUALIFICATION_ROUNDS_COUNT."!");
        return false;
      }

      if($playoff->getMatchesInQualification() < self::MIN_MATCHES_IN_QUALIFICATION)
      {
        throw new \Exception("Количество матчей в туре квалификации не может быть меньше ".self::MIN_MATCHES_IN_QUALIFICATION."!");
        return false;
      }

      if($playoff->getMatchesInGame() < self::MIN_MATCHES_IN_GAME)
      {
        throw new \Exception("Количество матчей в игре плей-офф не может быть меньше ".self::MIN_MATCHES_IN_GAME."!");
        return false;
      }

      if($playoff->getMaxUsersCount() < self::MIN_USERS_COUNT)
      {
        throw new \Exception("Количество участников не может быть меньше ".self::MIN_USERS_COUNT."!");
        return false;
      }

      return true;
    }

    /**
    * Проверяет матчи турнира
    *
    * @param \AppBundle\Entity\TournamentPlayoff $playoff
    * @throws \Exception
    * @return bool
    */
    public function checkMatches(\AppBundle\Entity\TournamentPlayoff $playoff)
    {
      if(count($playoff->getTournament()->getPlayoffMatches()) < 1)
      {
        throw new \Exception("Нельзя опубликовать турнир пока не заполнен хотя бы один матч!");
        return false;
      }

      if(count($this->matchesRep->findStartedByTournament($playoff->getTournament()->getId())) > 0)
      {
        throw new \Exception("В турнире имеются уже начавишеся, оконченные или отменённые матчи!");
        return false;
      }

      return true;
    }

    /**
    * Разбивает список матчей по турам
    *
    * @param array $matches Массив матчей.
    * @param int $roundsCount Кол-во туров
    * @param int $matchesInRound Кол-во матчей в одном туре
    */
    public function chunkMatches($matches, $roundsCount, $matchesInRound)
    {
        $roundsMatches = array_fill(1, $roundsCount, array_fill(1, $matchesInRound, null));

        foreach($roundsMatches as $roundNum => &$roundMatches)
        {
            foreach($roundMatches as &$roundMatch)
            {
                foreach($matches as $key => $match)
                {
                    if($match->getRound() != $roundNum)
                        continue;

                    $roundMatch = $match;
                    unset($matches[$key]);
                    break;
                }
            }
        }

        return $roundsMatches;
    }

    /**
    * Возвращает кол-во туров в плей-офф в зависимости от кол-ва участников
    *
    * @param int $usersCount Кол-во участников
    * @return int
    */
    public function getPlayoffRoundsCount($usersCount)
    {
      return round(log($usersCount, 2));
    }

    /**
    * Генерирует список пользователей для результатов квалификации когда их ещё нет.
    *
    * @param \AppBundle\Entity\TournamentPlayoff $tournament
    * @return \AppBundle\Entity\TournamentPlayoffQualificationResult
    */
    public function initQualificationResults(\AppBundle\Entity\TournamentPlayoff $tournament)
    {
        if(!$tournament->getTournament()->getIsStarted())
        {
            $results = array();

            $tournamentUsers = $this->em->getRepository('AppBundle\Entity\User')->findTournamentUsersOrderedByRating($tournament->getTournament()->getId());
            $place = 1;
            foreach($tournamentUsers as $user)
            {
                $tmpResult = new TournamentPlayoffQualificationResult();
                $tmpResult->setTournament($tournament->getTournament());
                $tmpResult->setUser($user);
                $tmpResult->setRound(1);
                $tmpResult->setPlace($place);
                $tmpResult->setTotalPoints(0);
                $tmpResult->setPointsInRound(0);
                $tmpResult->setTotalGuessedResults(0);
                $tmpResult->setTotalGuessedOutcomes(0);
                $tmpResult->setTotalGuessedDifferences(0);
                $tmpResult->setUserRating($user->getRating());

                $results[] = $tmpResult;
                $place++;
            }

            return $results;
        }

        $results = $this->em->getRepository('AppBundle\Entity\TournamentPlayoffQualificationRealTimeResult')->findByTournament($tournament->getTournament()->getId());

        return $results;
    }

    /**
    * Проверяет участие пользователя в туре плей-офф
    *
    * @param int $user         Пользователь
    * @param int $tournamentId ID турнира
    * @param int $roundNum     Номер тура
    *
    * @return bool
    */
    public function userInPlayoffRound($userId, $tournamentId, $roundNum) {
        return $this->gamesRep->isUserInPlayoffRound($tournamentId, $userId, $roundNum);
    }

    /**
    * Возвращает массив из матчей со ставками участвующих игроков в игре
    *
    * @param \AppBundle\Entity\TournamentPlayoffGame $game
    *
    * @return array
    */
    public function getGameMatchesWithBets(\AppBundle\Entity\TournamentPlayoffGame $game)
    {
        $tournamentMatches = $this->matchesRep->findPlayoffByTournamentAndRound($game->getTournament()->getId(), $game->getRound());

        $matchesIds = array();
        foreach($tournamentMatches as $tMatch)
            $matchesIds[] = $tMatch->getMatch()->getId();

        /**
        * @var \AppBundle\Entity\Bet[]
        */
        $betsRowset = $this->betsRep->findBy(array(
            'tournament' => $game->getTournament()->getId(),
            'match' => $matchesIds,
            'user' => array($game->getUserLeft()->getId(), $game->getUserRight()->getId())
        ));

        $bets = array();
        foreach($betsRowset as $bet)
            $bets[$bet->getMatch()->getId()][$bet->getUser()->getId()] = $bet;

        $data = array();
        $userLeftId = $game->getUserLeft()->getId();
        $userRightId = $game->getUserRight()->getId();
        foreach($tournamentMatches as $tMatch)
        {
            $match = $tMatch->getMatch();
            $userLeftBet = (!isset($bets[$match->getId()][$userLeftId])) ? null : $bets[$match->getId()][$userLeftId];
            $userRightBet = (!isset($bets[$match->getId()][$userRightId])) ? null : $bets[$match->getId()][$userRightId];

            $data[] = array(
                'match' => array(
                    'id' => $match->getId(),
                    'isProcessed' => $tMatch->getIsProcessed(),
                    'homeTeamTitle' => $match->getHomeTeam()->getTitle(),
                    'guestTeamTitle' => $match->getGuestTeam()->getTitle(),
                    'scoreLeft' => $match->getScoreLeft(),
                    'scoreRight' => $match->getScoreRight(),
                ),
                'points' => array(
                    'userLeft' => ($userLeftBet) ? $userLeftBet->getPoints() : 0,
                    'userRight' => ($userRightBet) ? $userRightBet->getPoints() : 0
                )
            );
        }

        return $data;
    }

    /**
    * Генерирует результаты туров по крону
    *
    * @param \AppBundle\Entity\Tournament $tournament
    */
    public function processResults(\AppBundle\Entity\Tournament $tournament)
    {
        /**
        * @var $playoff \AppBundle\Entity\TournamentPlayoff
        */
        $playoff = $this->em->getRepository("\AppBundle\Entity\TournamentPlayoff")->findLazy($tournament->getId());

        $currentRound = $playoff->getCurrentRound();
        $currentStage = $playoff->getStage();

        if($currentStage == TournamentPlayoff::STAGE_QUALIFICATION)
            $this->processQualificationRound($playoff, $currentRound);
        elseif($currentStage == TournamentPlayoff::STAGE_PLAYOFF)
            $this->processPlayoffRound($playoff, $currentRound);
    }

    private function processQualificationRound(\AppBundle\Entity\TournamentPlayoff $tournament, $currentRound)
    {
        $tournamentId = $tournament->getTournament()->getId();

        /**
        * Если нет необработанных матчей, которые закончились, то выходим
        */
        if(!$matchesIds = $this->em->getRepository("\AppBundle\Entity\TournamentPlayoffMatch")->findNonProcessedMatchesFinished($tournamentId, $currentRound, true))
            return true;

        //собираем idшники матчей
        $nonProcessedMatchesIds = array();
        foreach($matchesIds as $matchData)
        {
            $nonProcessedMatchesIds[] = $matchData["id"];
        }
        unset($matchesIds, $matchData);


        if(count($nonProcessedMatchesIds) == 0)
            return true;

        /**
        * Если среди необработанных матчей, которые закончились, есть необработанные ставки, то выходим
        */
        if(!$this->em->getRepository("\AppBundle\Entity\TournamentPlayoffMatch")->allBetsCalculated($tournamentId, $nonProcessedMatchesIds))
            return true;

        $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: SETTING POINTS FOR NON PROCCESSED MATCHES: ".serialize($nonProcessedMatchesIds));

        /**
        * Начисляем очки в онлайн таблицу
        */
        $rtResults = $this->em->getRepository("\AppBundle\Entity\TournamentPlayoffQualificationRealTimeResult")->findByTournament($tournamentId);
        $nonProcessedMatches = $this->em->getRepository("\AppBundle\Entity\TournamentPlayoffMatch")->findByIds($tournamentId, $nonProcessedMatchesIds);
        $roundBets = $this->__getBetsForEachUser($tournamentId, $nonProcessedMatchesIds);

        try
        {
            foreach($rtResults as &$result)
            {
                $currentUserResult = $this->__getQualificationResultForUserByRound($result->getUser()->getId(), $nonProcessedMatches, $roundBets);

                $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: SETTING REAL TIME RESULTS: ".serialize(array("user" => $result->getUser()->getId(),
                                                                                                                            "results" => $currentUserResult)));

                $result->setPlace(0);
                $result->setTotalPoints($result->getTotalPoints() + $currentUserResult["total_points"]);
                $result->setTotalGuessedResults($result->getTotalGuessedResults() + $currentUserResult["total_guessed_results"]);
                $result->setTotalGuessedOutcomes($result->getTotalGuessedOutcomes() + $currentUserResult["total_guessed_outcomes"]);
                $result->setTotalGuessedDifferences($result->getTotalGuessedDifferences() + $currentUserResult["total_guessed_differences"]);
                $result->setUserRating($result->getUser()->getRating());

                $this->em->persist($result);
            }
        } catch(\Exception $ex)
        {
            $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: EXCEPTION ON SETTING REAL TIME RESULTS: ".$ex->getMessage());
            $this->em->flush();
            return true;
        }

        $this->em->flush();

        foreach($nonProcessedMatches as &$tournamentMatch)
        {
            $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: SETTING MATCH PROCCESSED: ".serialize(array("match_id" => $tournamentMatch->getMatch()->getId())));
            $tournamentMatch->setIsProcessed(true);
            $this->em->persist($tournamentMatch);
        }

        $this->em->flush();

        unset($nonProcessedMatches);
        unset($tournamentMatch);
        unset($rtResults);

        /**
        * Сортируем и проставляем места
        */
        $rtResults = $this->em->getRepository("\AppBundle\Entity\TournamentPlayoffQualificationRealTimeResult")->findSorted($tournamentId);
        $place = 1;
        foreach($rtResults as &$result)
        {
            $result->setPlace($place);
            $this->em->persist($result);
            $place++;
        }

        $this->em->flush();

        unset($rtResults);
        unset($result);

        /**
        * Если не все матчи окончились, то выходим
        */
        if(!$this->em->getRepository("\AppBundle\Entity\TournamentPlayoffMatch")->allRoundMatchesFinished($tournamentId, $currentRound, true))
            return true;

        /**
        * Если все матчи в туре закончились, но за какие-то матчи ещё не начислены очки, то выходим
        */
        if(!$this->em->getRepository("\AppBundle\Entity\TournamentPlayoffMatch")->matchesPointsCalculated($tournamentId, $currentRound, true))
            return true;

        $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: ALL MATCHES FINISHED AND PROCCESSED IN ROUND $currentRound!");

        /**
        * Так как тур окончен, то заносим результаты тура в таблицу результов по турам
        */
        $rtResults = $this->em->getRepository("\AppBundle\Entity\TournamentPlayoffQualificationRealTimeResult")->findByTournament($tournamentId);
        foreach($rtResults as &$result)
        {
            $tourResult = new TournamentPlayoffQualificationResult();
            $tourResult->setTournament($result->getTournament());
            $tourResult->setUser($result->getUser());
            $tourResult->setRound($currentRound);
            $tourResult->setPlace($result->getPlace());
            $tourResult->setPointsInRound(0);
            $tourResult->setTotalPoints($result->getTotalPoints());
            $tourResult->setTotalGuessedResults($result->getTotalGuessedResults());
            $tourResult->setTotalGuessedOutcomes($result->getTotalGuessedOutcomes());
            $tourResult->setTotalGuessedDifferences($result->getTotalGuessedDifferences());
            $tourResult->setUserRating($result->getUser()->getRating());

            $this->em->persist($tourResult);
        }

        unset($rtResults);
        unset($result);

        $this->em->flush();

        /**
        * Переходим в следующий тур, если он есть.
        */
        if($currentRound + 1 <= $tournament->getQualificationRoundsCount())
        {
            $tournament->setCurrentRound($currentRound + 1);
            $this->em->persist($tournament);
            $this->em->flush();

            return null;
        }

        /**
        * Или генерируем данные для этапа плей-офф
        */

        /**
        * Удаляем результаты реального времени за ненадобностью
        */
        $this->em->getRepository("\AppBundle\Entity\TournamentPlayoffQualificationRealTimeResult")->removeByTournament($tournament->getTournament()->getId());

        /**
        * Удаляем ставки пользователей, которые не прошли квалификацию на плейофф
        */
        $notInPlayoffStageUsersIds = $this->em->getRepository("\AppBundle\Entity\TournamentPlayoffQualificationResult")->findByTournamentUsersIdsNotInPlayoff($tournament->getTournament()->getId(), $currentRound, $tournament->getRealUsersOutOfQualification());
        if(count($notInPlayoffStageUsersIds) > 0)
        {
            $playoffMatches = $this->em->getRepository("\AppBundle\Entity\TournamentPlayoffMatch")->findPlayoffMatches($tournament->getTournament()->getId());

            $matchesIds = array();
            foreach($playoffMatches as $tournamentMatch)
                $matchesIds[] = $tournamentMatch->getMatch()->getId();

            $this->em->getRepository("\AppBundle\Entity\Bet")->removeBetsByUsersIdsAndMatchesIds($tournament->getTournament()->getId(), $notInPlayoffStageUsersIds, $matchesIds);
            unset($notInPlayoffStageUsersIds, $playoffMatches, $matchesIds, $tournamentMatch);
        }

        /**
        * Генерируем пары для плейофф
        */
        $qualificationResults = $this->em->getRepository("\AppBundle\Entity\TournamentPlayoffQualificationResult")->findByTournamentAndRound($tournamentId, $currentRound, $tournament->getRealUsersOutOfQualification());

        $playoffMap = $this->getPlayoffMap(count($qualificationResults));

        foreach($playoffMap as $userLeftPlace => $userRightPlace)
        {
            $userLeft = $qualificationResults[$userLeftPlace - 1]->getUser();
            $userRight = $qualificationResults[$userRightPlace - 1]->getUser();

            $TournamentPlayoffGame = new TournamentPlayoffGame();
            $TournamentPlayoffGame->setTournament($tournament->getTournament());
            $TournamentPlayoffGame->setRound(1);
            $TournamentPlayoffGame->setUserLeft($userLeft);
            $TournamentPlayoffGame->setUserRight($userRight);

            $this->em->persist($TournamentPlayoffGame);
        }

        $this->em->flush();

        /**
        * Переходим в стадию плей-офф
        */
        $tournament->setStage(TournamentPlayoff::STAGE_PLAYOFF);
        $tournament->setCurrentRound(1);
        $this->em->persist($tournament);
        $this->em->flush();

        return true;
    }

    private function processPlayoffRound(\AppBundle\Entity\TournamentPlayoff $tournament, $currentRound)
    {
        $tournamentId = $tournament->getTournament()->getId();

        /**
        * Если нет необработанных матчей, которые закончились, то выходим
        */
        if(!$matchesIds = $this->em->getRepository("\AppBundle\Entity\TournamentPlayoffMatch")->findNonProcessedMatchesFinished($tournamentId, $currentRound, false))
            return true;

        //собираем idшники матчей
        $nonProcessedMatchesIds = array();
        foreach($matchesIds as $matchData)
        {
            $nonProcessedMatchesIds[] = $matchData["id"];
        }
        unset($matchesIds, $matchData);


        if(count($nonProcessedMatchesIds) == 0)
            return true;

        /**
        * Если среди необработанных матчей, которые закончились, есть необработанные ставки, то выходим
        */
        if(!$this->em->getRepository("\AppBundle\Entity\TournamentPlayoffMatch")->allBetsCalculated($tournamentId, $nonProcessedMatchesIds))
            return true;

        $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: SETTING POINTS FOR NON PROCCESSED MATCHES: ".serialize($nonProcessedMatchesIds));

        /**
        * Начисляем мячи в игры
        */
        $playoffGames = $this->em->getRepository("AppBundle\Entity\TournamentPlayoffGame")->findByTournamentAndRound($tournamentId, $currentRound);
        $nonProcessedMatches = $this->em->getRepository("\AppBundle\Entity\TournamentPlayoffMatch")->findByIds($tournamentId, $nonProcessedMatchesIds);
        $roundBets = $this->__getBetsForEachUser($tournamentId, $nonProcessedMatchesIds);

        /* @var $game \AppBundle\Entity\TournamentPlayoffGame */
        foreach($playoffGames as &$game)
        {
            $pointsLeft = floatval($game->getPointsLeft());
            $totalGuessedResultsLeft = intval($game->getTotalGuessedResultsLeft());
            $totalGuessedOutcomesLeft = intval($game->getTotalGuessedOutcomesLeft());
            $totalGuessedDifferencesLeft = intval($game->getTotalGuessedDifferencesLeft());

            $pointsRight = floatval($game->getPointsRight());
            $totalGuessedResultsRight = intval($game->getTotalGuessedResultsRight());
            $totalGuessedOutcomesRight = intval($game->getTotalGuessedOutcomesRight());
            $totalGuessedDifferencesRight = intval($game->getTotalGuessedDifferencesRight());

            foreach($roundBets as $matchId => $usersBets)
            {
                $user = $game->getUserLeft();
                $ratingLeft = $user->getRating();
                // начисляем очки левому игроку, если он делал ставку
                if(isset($usersBets[$user->getId()]))
                {
                    $bet = $usersBets[$user->getId()];

                    if(is_null($bet->getPoints()))
                    {
                        $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: EXCEPTION ON SETTING POINTS ON GAME RESULTS: "."Очки пользователю #".$user->getId()." за матч #".$matchId." ещё не начислены!");
                        return false;
                    }
                    $pointsLeft += floatval($bet->getPoints());                    

                    if($this->pointsRuler->scoreGuessed($bet, $bet->getMatch()))
                    {
                        $totalGuessedResultsLeft++;
                        $totalGuessedDifferencesLeft++;
                        $totalGuessedOutcomesLeft++;
                    }
                    elseif($this->pointsRuler->scoreDifferenceGuessed($bet, $bet->getMatch()))
                    {
                        $totalGuessedDifferencesLeft++;
                        $totalGuessedOutcomesLeft++;
                    }
                    elseif($this->pointsRuler->matchOutcomeGuessed($bet, $bet->getMatch()))
                    {
                        $totalGuessedOutcomesLeft++;
                    }
                }

                $user = $game->getUserRight();
                $ratingRight = $user->getRating();
                // начисляем очки правому игроку, если он делал ставку
                if(isset($usersBets[$user->getId()]))
                {
                    $bet = $usersBets[$user->getId()];

                    if(is_null($bet->getPoints()))
                    {
                        $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: EXCEPTION ON SETTING POINTS ON GAME RESULTS: "."Очки пользователю #".$user->getId()." за матч #".$matchId." ещё не начислены!");
                        return false;
                    }

                    $pointsRight += floatval($bet->getPoints());                    

                    if($this->pointsRuler->scoreGuessed($bet, $bet->getMatch()))
                    {
                        $totalGuessedResultsRight++;
                        $totalGuessedDifferencesRight++;
                        $totalGuessedOutcomesRight++;
                    }
                    elseif($this->pointsRuler->scoreDifferenceGuessed($bet, $bet->getMatch()))
                    {
                        $totalGuessedDifferencesRight++;
                        $totalGuessedOutcomesRight++;
                    }
                    elseif($this->pointsRuler->matchOutcomeGuessed($bet, $bet->getMatch()))
                    {
                        $totalGuessedOutcomesRight++;
                    }
                }
            } //foreach($roundBets as $matchId => $usersBets)

            $game->setPointsLeft($pointsLeft);
            $game->setTotalGuessedResultsLeft($totalGuessedResultsLeft);
            $game->setTotalGuessedDifferencesLeft($totalGuessedDifferencesLeft);
            $game->setTotalGuessedOutcomesLeft($totalGuessedOutcomesLeft);            
            $game->setUserRatingLeft($ratingLeft);

            $game->setPointsRight($pointsRight);
            $game->setTotalGuessedResultsRight($totalGuessedResultsRight);
            $game->setTotalGuessedDifferencesRight($totalGuessedDifferencesRight);
            $game->setTotalGuessedOutcomesRight($totalGuessedOutcomesRight);
            $game->setUserRatingRight($ratingRight);

            $this->em->persist($game);

        } //foreach($groupGames as $game)

        $this->em->flush();

        unset($usersBets,$roundBets,$matchId,$playoffGames,$game);

        foreach($nonProcessedMatches as &$tournamentMatch)
        {
            $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: SETTING MATCH PROCCESSED: ".serialize(array("match_id" => $tournamentMatch->getMatch()->getId())));
            $tournamentMatch->setIsProcessed(true);
            $this->em->persist($tournamentMatch);
        }

        $this->em->flush();

        /**
        * Если не все матчи игры окончились, то выходим
        */
        if(!$this->em->getRepository("\AppBundle\Entity\TournamentPlayoffMatch")->allRoundMatchesFinished($tournamentId, $currentRound, false))
            return true;

        /**
        * Если все матчи в туре закончились, но за какие-то матчи ещё не начислены очки, то выходим
        */
        if(!$this->em->getRepository("\AppBundle\Entity\TournamentPlayoffMatch")->matchesPointsCalculated($tournamentId, $currentRound, false))
            return true;

        $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: ALL MATCHES FINISHED AND PROCCESSED IN ROUND $currentRound!");

        /**
        * Так как тур окончен, то сохраняем победителей тура
        */
        $playoffGames = $this->em->getRepository("AppBundle\Entity\TournamentPlayoffGame")->findByTournamentAndRound($tournamentId, $currentRound);

        $looseUsersIds = array();
        foreach($playoffGames as &$game)
        {
            if($game->getPointsLeft() > $game->getPointsRight())
            {
                $game->setUserLeftIsWinner(true);
                $game->setUserRightIsWinner(false);
                $looseUsersIds[] = $game->getUserRight()->getId();
            }
            elseif($game->getPointsLeft() < $game->getPointsRight())
            {
                $game->setUserLeftIsWinner(false);
                $game->setUserRightIsWinner(true);
                $looseUsersIds[] = $game->getUserLeft()->getId();
            }
            elseif($game->getTotalGuessedResultsLeft() > $game->getTotalGuessedResultsRight())
            {
                $game->setUserLeftIsWinner(true);
                $game->setUserRightIsWinner(false);
                $looseUsersIds[] = $game->getUserRight()->getId();
            }
            elseif($game->getTotalGuessedResultsLeft() < $game->getTotalGuessedResultsRight())
            {
                $game->setUserLeftIsWinner(false);
                $game->setUserRightIsWinner(true);
                $looseUsersIds[] = $game->getUserLeft()->getId();
            }
            elseif($game->getTotalGuessedDifferencesLeft() > $game->getTotalGuessedDifferencesRight())
            {
                $game->setUserLeftIsWinner(true);
                $game->setUserRightIsWinner(false);
                $looseUsersIds[] = $game->getUserRight()->getId();
            }
            elseif($game->getTotalGuessedDifferencesLeft() < $game->getTotalGuessedDifferencesRight())
            {
                $game->setUserLeftIsWinner(false);
                $game->setUserRightIsWinner(true);
                $looseUsersIds[] = $game->getUserLeft()->getId();
            }
            elseif($game->getTotalGuessedOutcomesLeft() > $game->getTotalGuessedOutcomesRight())
            {
                $game->setUserLeftIsWinner(true);
                $game->setUserRightIsWinner(false);
                $looseUsersIds[] = $game->getUserRight()->getId();
            }
            elseif($game->getTotalGuessedOutcomesLeft() < $game->getTotalGuessedOutcomesRight())
            {
                $game->setUserLeftIsWinner(false);
                $game->setUserRightIsWinner(true);
                $looseUsersIds[] = $game->getUserLeft()->getId();
            }
            elseif($game->getUserRatingLeft() > $game->getUserRatingRight())
            {
                $game->setUserLeftIsWinner(true);
                $game->setUserRightIsWinner(false);
                $looseUsersIds[] = $game->getUserRight()->getId();
            }
            elseif($game->getUserRatingLeft() < $game->getUserRatingRight())
            {
                $game->setUserLeftIsWinner(false);
                $game->setUserRightIsWinner(true);
                $looseUsersIds[] = $game->getUserLeft()->getId();
            }
            else
            {
                $tmp = array("left", "right");
                $randomNum = rand(0, 1);

                if($tmp[$randomNum] == "left")
                {
                    $game->setUserLeftIsWinner(true);
                    $game->setUserRightIsWinner(false);
                    $looseUsersIds[] = $game->getUserRight()->getId();
                }
                else
                {
                    $game->setUserLeftIsWinner(false);
                    $game->setUserRightIsWinner(true);
                    $looseUsersIds[] = $game->getUserLeft()->getId();

                }

                unset($tmp, $randomNum);
            }

            $this->em->persist($game);
        }

        $this->em->flush();

        /**
        * Если это последний тур, то записываем побидетелeй в таблицу результатов для подсчёта
        */
        if($tournament->getPlayoffRoundsCount() == $currentRound)
        {
            $maxPlace = $this->em->getRepository("AppBundle\Entity\WinRule")->findMaxPlaceByFormatAndWinnersCount($tournament->getTournament()->getFormat()->getInternalName(), $tournament->getRealUsersOutOfQualification());

            $place = 1;

            //Итерируем по раундам начиная с Финала
            for($round = $tournament->getPlayoffRoundsCount(); $round >= 1; $round--)
            {
                //Если текущее место больше чем определено, выходим
                if($place > $maxPlace)
                    break;

                //Достаём игры текущего раунда
                $playoffGames = $this->em->getRepository("AppBundle\Entity\TournamentPlayoffGame")->findByTournamentAndRound($tournamentId, $round);

                //Если финал, то распределяем первое и второе место
                if($round == $tournament->getPlayoffRoundsCount())
                {
                    //В финале всегда одна игра
                    $game = $playoffGames[0];

                    if($game->getUserLeftIsWinner())
                    {
                        $winnerUser = $game->getUserLeft();
                        $looserUser = $game->getUserRight();
                    }
                    elseif($game->getUserRightIsWinner())
                    {
                        $winnerUser = $game->getUserRight();
                        $looserUser = $game->getUserLeft();
                    }

                    //первое место
                    $TournamentResult = new TournamentResult();
                    $TournamentResult->setTournament($tournament->getTournament());
                    $TournamentResult->setPlace($place);

                    $TournamentResult->setUser($winnerUser);

                    $this->em->persist($TournamentResult);

                    $place++;

                    //второе место
                    $TournamentResult = new TournamentResult();
                    $TournamentResult->setTournament($tournament->getTournament());
                    $TournamentResult->setPlace($place);

                    $TournamentResult->setUser($looserUser);

                    $this->em->persist($TournamentResult);

                    $place++;

                    continue;
                }

                //Если не финал, то итерируем по играм расставляем по местам проигравших
                foreach($playoffGames as $game)
                {
                    //Ставим на текущее место проигравших
                    $TournamentResult = new TournamentResult();
                    $TournamentResult->setTournament($tournament->getTournament());
                    $TournamentResult->setPlace($place);

                    $looserUser = ($game->getUserLeftIsWinner()) ? $game->getUserRight() : $game->getUserLeft();
                    $TournamentResult->setUser($looserUser);

                    $this->em->persist($TournamentResult);
                }

                $place++;
            }

            $this->em->flush();

            //Заканчиваем турнир
            $tournament->getTournament()->setIsOver(true);
            $tournament->getTournament()->setIsResultsReady(true);
            $tournament->getTournament()->setEndDatetime( new \DateTime() );

            $this->em->flush();

            return true;
        }

        /**
        * Если тур не последний, то нужно сгенерировать новые пары на новый тур
        */
        $playoffGames = $this->em->getRepository("AppBundle\Entity\TournamentPlayoffGame")->findByTournamentAndRound($tournamentId, $currentRound);

        /**
        * Генерируем пары по схеме 1-2, 3-4, 5-6, 7-8 и т.д.
        */
        for($i = 0; $i < count($playoffGames); $i = $i + 2)
        {
            /**
            * @var $firstGame \AppBundle\Entity\TournamentPlayoffGame
            */
            $firstGame = $playoffGames[$i];
            /**
            * @var $secondGame \AppBundle\Entity\TournamentPlayoffGame
            */
            $secondGame = $playoffGames[$i + 1];

            $userLeft = null;
            $userRight = null;

            if($firstGame->getUserLeftIsWinner())
                $userLeft = $firstGame->getUserLeft();
            elseif($firstGame->getUserRightIsWinner())
                $userLeft = $firstGame->getUserRight();

            if($secondGame->getUserLeftIsWinner())
                $userRight = $secondGame->getUserLeft();
            elseif($secondGame->getUserRightIsWinner())
                $userRight = $secondGame->getUserRight();

            $game = new TournamentPlayoffGame();
            $game->setTournament($tournament->getTournament());
            $game->setRound($currentRound + 1);
            $game->setUserLeft($userLeft);
            $game->setUserRight($userRight);

            $this->em->persist($game);
        }

        $this->em->flush();
        unset($playoffGames, $i, $firstGame, $secondGame, $userLeft, $userRight, $game);

        /**
        * Удаляем ставки пользователей которые не прошли в следующий тур
        */
        $playoffNextRoundsMatches = $this->em->getRepository("\AppBundle\Entity\TournamentPlayoffMatch")->findIdsPlayoffNextRoundByTournament($tournamentId, $currentRound + 1);
        $matchesIds = array();
        foreach($playoffNextRoundsMatches as $match)
            $matchesIds[] = $match["id"];
        unset($playoffNextRoundsMatches, $match);

        $this->em->getRepository("\AppBundle\Entity\Bet")->removeBetsByUsersIdsAndMatchesIds($tournamentId, $looseUsersIds, $matchesIds);

        unset($matchesIds, $looseUsersIds);

        /**
        * Переходим в следующий тур
        */
        if($currentRound + 1 <= $tournament->getPlayoffRoundsCount())
        {
            $tournament->setCurrentRound($currentRound + 1);
            $this->em->persist($tournament);
            $this->em->flush();

            return true;
        }
    }

    /**
    * Обновляет время старта турнира
    *
    * @param \AppBundle\Entity\Tournament $tournament
    */
    public function updateTournamentStartDatetime($tournament)
    {
      $startDatetime = $this->matchesRep->getMinStartDatetime($tournament->getId());

      if(!$tournament->getStartDatetime() || $tournament->getStartDatetime()->format("Y-m-d H:i:s") != $startDatetime)
      {
        if($startDatetime)
        {
            $datetime = new \DateTime($startDatetime);
            $tournament->setStartDatetime($datetime);
        }
        else
        {
          $tournament->setStartDatetime(null);
        }

        $this->em->persist($tournament);
        $this->em->flush($tournament);
      }
    }

    /**
    * Проверяет возможность старта турнира
    *
    * @param \AppBundle\Entity\Tournament $tournament
    * @return bool
    */
    public function isTournamentCanStart(\AppBundle\Entity\Tournament $tournament)
    {
      if(count($tournament->getUsers()) < self::MIN_USERS_COUNT)
        return false;

      return true;
    }

    /**
    * Static
    */

    /**
    * Возвращает возможные варианты количества участников в турнире проходящих в плей-офф
    *
    * @param int $usersCnt Максимальное кол-во участников
    *
    * @return array Массив с возможными вариантами кол-ва участников в плей-офф в формате [2, 4, 8, ...] до $usersCnt
    */
    public static function getPossibleUsersCounts($usersCnt = self::MAX_USERS_COUNT)
    {
      $usersCountChoices = array();
      for($exp = 1; pow(2, $exp) <= $usersCnt; $exp++)
        $usersCountChoices[pow(2, $exp)] = pow(2, $exp);

      return $usersCountChoices;
    }

    /**
    * Private
    */

    /**
    * Получает ставки участников по id турнира и номеру тура и распределяет их по матчам и пользователям
    *
    * @param int $tournament_id
    * @param int $round
    * @return array $userBets
    */
    private function __getBetsForEachUser($tournament_id, $matchesIds)
    {
        $bets = $this->em->getRepository("\AppBundle\Entity\Bet")->findByTournamentAndMatchesIds($tournament_id, $matchesIds);

        $userBets = array();
        foreach($bets as $bet)
        {
            $userBets[$bet->getMatch()->getId()][$bet->getUser()->getId()] = $bet;
        }
        unset($bets, $bet);

        return $userBets;
    }

    private function __getQualificationResultForUserByRound($user_id, $tournamentMatches, $bets)
    {
        $totalPoints = 0;
        $totalGuessedResults = 0;
        $totalGuessedOutcomes = 0;
        $totalGuessedDifferences = 0;

        foreach($tournamentMatches as $tournamentMatch)
        {
            $match = $tournamentMatch->getMatch();

            if($match->getIsRejected())
                continue;

            /**
            * Если пользователь не делал ставок на матч, не учитываем его
            */
            if(!isset($bets[$match->getId()][$user_id]))
                continue;

            $bet = $bets[$match->getId()][$user_id];

            if(is_null($bet->getPoints()))
            {
                throw new \Exception("Очки пользователю #".$user_id." за матч #".$match->getId()." ещё не начислены!");
                return false;
            }

            $totalPoints += $bet->getPoints();

            if($this->pointsRuler->scoreGuessed($bet, $match))
            {
                $totalGuessedResults++;
                $totalGuessedDifferences++;
                $totalGuessedOutcomes++;

            }
            elseif($this->pointsRuler->scoreDifferenceGuessed($bet, $match))
            {
                $totalGuessedDifferences++;
                $totalGuessedOutcomes++;
            }
            elseif($this->pointsRuler->matchOutcomeGuessed($bet, $match))
            {
                $totalGuessedOutcomes++;
            }

        }

        return array("total_points" => $totalPoints,
                     "total_guessed_results" => $totalGuessedResults,
                     "total_guessed_outcomes" => $totalGuessedOutcomes,
                     "total_guessed_differences" => $totalGuessedDifferences);
    }


    /**
    * Event Handlers:
    */

    /**
    * Обработчик события добавления матча в турнир
    *
    * @param \AppBundle\Event\TournamentEvent $event
    */
    public function onTournamentMatchAdded(\AppBundle\Event\TournamentEvent $event)
    {
      /**
      * @var \AppBundle\Entity\TournamentPlayoff
      */
      $playoff = $event->getTournament();

      /**
      * @var \AppBundle\Entity\Tournament
      */
      $tournament = $playoff->getTournament();

      $this->updateTournamentStartDatetime($tournament);
    }

    /**
    * Обработчик события удаления матча из турнира
    *
    * @param \AppBundle\Event\TournamentEvent $event
    */
    public function onTournamentMatchDeleted(\AppBundle\Event\TournamentEvent $event)
    {
      /**
      * @var \AppBundle\Entity\TournamentPlayoff
      */
      $playoff = $event->getTournament();

      /**
      * @var \AppBundle\Entity\Tournament
      */
      $tournament = $playoff->getTournament();

      $this->updateTournamentStartDatetime($tournament);
    }

    public function onTournamentStartedHandler(\AppBundle\Event\TournamentEvent $event)
    {
        /**
        * @var \AppBundle\Entity\TournamentPlayoff $tournament
        */
        $tournament = $event->getTournament();

        $realUsersCount = count($tournament->getTournament()->getUsers());

        /**
        * Проставляем real_users_out_of_qualification - Максимальный выбор из возможного
        */
        $maxUsersCountChoices = array();

        for($exp = 2; pow(2, $exp) <= $this->getRule("max_max_users_count"); $exp++)
            $maxUsersCountChoices[pow(2, $exp)] = pow(2, $exp);

        foreach($maxUsersCountChoices as $choice)
        {
            if($choice > $realUsersCount)
                break;

            $realUsersOutOfQualification = $choice;
        }

        $tournament->setRealUsersOutOfQualification($realUsersOutOfQualification);

        $playoffRoundsCount = round(log($realUsersOutOfQualification, 2));
        $tournament->setPlayoffRoundsCount($playoffRoundsCount);

        $this->em->persist($tournament);
        $this->em->flush();

        /**
        * Удаляем ненужные матчи и ставки
        */
        $this->em->getRepository('AppBundle\Entity\TournamentPlayoffMatch')->removeUnneededMatchesAndBets($tournament->getTournament()->getId(), $playoffRoundsCount);

        /**
        * Заполняем таблицу онлайн результатов для квалификации
        */
        $tournamentUsers = $this->em->getRepository('AppBundle\Entity\User')->findTournamentUsersOrderedByRating($tournament->getTournament()->getId());

        $place = 1;
        foreach($tournamentUsers as $user)
        {
            $rtResult = new TournamentPlayoffQualificationRealTimeResult();
            $rtResult->setTournament($tournament->getTournament());
            $rtResult->setUser($user);
            $rtResult->setPlace($place);
            $rtResult->setUserRating($user->getRating());

            $this->em->persist($rtResult);

            $place++;
        }

        $this->em->flush();
    }

    public function onTournamentUserAddedHandler(\AppBundle\Event\TournamentEvent $event)
    {
        /**
        * @var \AppBundle\Entity\TournamentChampionsleague $tournament
        */
        $tournament = $event->getTournament();

        $usersCount = $this->em->getRepository("\AppBundle\Entity\User")->findChampionshipUsersCount($tournament->getTournament()->getId());

        if($usersCount >= $tournament->getMaxUsersCount())
        {
            $tournament->getTournament()->setIsAccessible(false);

            $this->em->persist($tournament->getTournament());
            $this->em->flush();
        }
    }
}