<?php
namespace AppBundle\Services\Tournaments;

use AppBundle\Entity\TournamentRoundResult;
use AppBundle\Entity\TournamentRoundGame;
use AppBundle\Entity\TournamentResult;

use AppBundle\Services\LogHelper;

class Round
{
    /**
    * @var \AppBundle\Entity\TournamentRoundRepository
    */
    public $tournamentRep;

    private $em;
    private $pointsRuler;
    private $logHelper;

    /**
    * @var \AppBundle\Entity\TournamentRoundMatchRepository
    */
    private $matchesRep;

    /**
    * @var \AppBundle\Entity\BetRepository
    */
    private $betsRep;

    /**
    * Количество очков начисляемое за победу в игре
    */
    const POINTS_FOR_WIN = 3;

    /**
    * Количество очков начисляемое за поражение в игре
    */
    const POINTS_FOR_LOSS = 0;

    /**
    * Количество очков начисляемое за ничью в игре
    */
    const POINTS_FOR_DRAW = 1;

    /**
    * Минимальное количество матчей в игре
    */
    const MIN_MATCHES_IN_GAME = 1;

    /**
    * Минимальное количество участников
    */
    const MIN_USERS_COUNT = 2;

    /**
    * Минимальное количество участников
    */
    const MAX_USERS_COUNT = 100;

    public function __construct(\Doctrine\ORM\EntityManager $em, \AppBundle\Services\PointsRuler $pointsRuler)
    {
        $this->em = $em;

        $this->tournamentRep = $this->em->getRepository('AppBundle\Entity\TournamentRound');

        $this->pointsRuler = $pointsRuler;
        $this->logHelper = new LogHelper($this->em);

        $this->matchesRep = $this->em->getRepository('AppBundle\Entity\TournamentRoundMatch');
        $this->betsRep = $this->em->getRepository('AppBundle\Entity\Bet');
    }

    /**
    * Разбивает список матчей по турам
    *
    * @param array $matches Массив матчей.
    * @param int $usersCount Кол-во участников
    * @param int $matchesInRound Кол-во матчей в одном туре
    */
    public function chunkMatches($matches, $usersCount, $matchesInRound)
    {
        $roundsCount = ($usersCount % 2 == 0) ? $usersCount - 1 : $usersCount;

        $roundsMatches = array_fill(1, $roundsCount, array_fill(1, $matchesInRound, null));

        foreach($roundsMatches as $roundNum => &$roundMatches)
        {
            foreach($roundMatches as &$roundMatch)
            {
                foreach($matches as $key => $match)
                {
                    if($match->getRound() != $roundNum)
                        continue;

                    $roundMatch = $match;
                    unset($matches[$key]);
                    break;
                }
            }
        }

        return $roundsMatches;
    }

    /**
    * Валидация настроек турнира
    *
    * @param \AppBundle\Entity\TournamentRound $round
    * @throws \Exception
    * @return bool
    */
    public function validate(\AppBundle\Entity\TournamentRound $round)
    {
      if($round->getMaxUsersCount() < self::MIN_USERS_COUNT)
      {
        throw new \Exception("Количество участников не может быть меньше ".self::MIN_USERS_COUNT."!");
        return false;
      }

      if($round->getMaxUsersCount() > self::MAX_USERS_COUNT)
      {
        throw new \Exception("Количество участников не может быть больше ".self::MAX_USERS_COUNT."!");
        return false;
      }

      if($round->getMatchesInGame() < self::MIN_MATCHES_IN_GAME)
      {
        throw new \Exception("Количество матчей в игре не может быть меньше ".self::MIN_MATCHES_IN_GAME."!");
        return false;
      }

      return true;
    }

    /**
    * Проверяет матчи турнира
    *
    * @param \AppBundle\Entity\TournamentRound $round
    * @throws \Exception
    * @return bool
    */
    public function checkMatches(\AppBundle\Entity\TournamentRound $round)
    {
      if(count($round->getTournament()->getRoundMatches()) < 1)
      {
        throw new \Exception("Нельзя опубликовать турнир пока не заполнен хотя бы один матч!");
        return false;
      }

      if(count($this->matchesRep->findStartedByTournament($round->getTournament()->getId())) > 0)
      {
        throw new \Exception("В турнире имеются уже начавишеся, оконченные или отменённые матчи!");
        return false;
      }

      return true;
    }

    /**
    * Возвращает кол-во туров в зависимости от кол-ва участников
    *
    * @param int $usersCount Кол-во участников
    * @return int
    */
    public function getRoundsCount($usersCount)
    {
      if($usersCount % 2 == 0)
        return $usersCount;
      else
        return $usersCount - 1;
    }

    /**
    * Генерирует список пользователей для результатов когда их ещё нет.
    *
    * @param \AppBundle\Entity\TournamentRound $tournament
    * @return \AppBundle\Entity\TournamentRoundResult
    */
    public function initResults(\AppBundle\Entity\TournamentRound $tournament)
    {
        if(!$tournament->getTournament()->getIsStarted())
        {
            $results = array();

            $tournamentUsers = $this->em->getRepository('AppBundle\Entity\User')->findTournamentUsersOrderedByRating($tournament->getTournament()->getId());
            $place = 1;
            foreach($tournamentUsers as $user)
            {
                $tmpResult = new TournamentRoundResult();
                $tmpResult->setTournament($tournament->getTournament());
                $tmpResult->setUser($user);
                $tmpResult->setRound(1);
                $tmpResult->setPlace($place);
                $tmpResult->setTotalPoints(0);
                $tmpResult->setTotalGames(0);
                $tmpResult->setTotalWins(0);
                $tmpResult->setTotalDraws(0);
                $tmpResult->setTotalLoss(0);
                $tmpResult->setTotalGoalsScored(0);
                $tmpResult->setTotalGoalsDifference(0);
                $tmpResult->setTotalGoalsAgainst(0);
                $tmpResult->setUserRating($user->getRating());

                $results[] = $tmpResult;
                $place++;
            }

            return $results;
        }

        $results = $this->em->getRepository('AppBundle\Entity\TournamentRoundResult')->findByTournamentAndRound($tournament->getTournament()->getId(), $tournament->getCurrentRound() - 1);

        return $results;
    }

    public function initGames(\AppBundle\Entity\TournamentRound $tournament)
    {
        $games = $this->generateRoundRobinPairings($tournament->getMaxUsersCount());

        return $games;
    }

    /**
    * Обновляет время старта турнира
    *
    * @param \AppBundle\Entity\Tournament $tournament
    */
    public function updateTournamentStartDatetime($tournament)
    {
      $startDatetime = $this->matchesRep->getMinStartDatetime($tournament->getId());

      if(!$tournament->getStartDatetime() || $tournament->getStartDatetime()->format("Y-m-d H:i:s") != $startDatetime)
      {
        if($startDatetime)
        {
            $datetime = new \DateTime($startDatetime);
            $tournament->setStartDatetime($datetime);
        }
        else
        {
          $tournament->setStartDatetime(null);
        }

        $this->em->persist($tournament);
        $this->em->flush($tournament);
      }
    }

    /**
    * Проверяет возможность старта турнира
    *
    * @param \AppBundle\Entity\Tournament $tournament
    * @return bool
    */
    public function isTournamentCanStart(\AppBundle\Entity\Tournament $tournament)
    {
      if(count($tournament->getUsers()) < self::MIN_USERS_COUNT)
        return false;

      return true;
    }

    /**
    * Возвращает массив из матчей со ставками участвующих игроков в игре
    *
    * @param \AppBundle\Entity\TournamentRoundGame $game
    *
    * @return array
    */
    public function getGameMatchesWithBets(\AppBundle\Entity\TournamentRoundGame $game)
    {
        $tournamentMatches = $this->matchesRep->findByTournamentAndRound($game->getTournament()->getId(), $game->getRound());

        $matchesIds = array();
        foreach($tournamentMatches as $tMatch)
            $matchesIds[] = $tMatch->getMatch()->getId();

        /**
        * @var \AppBundle\Entity\Bet[]
        */
        $betsRowset = $this->betsRep->findBy(array(
            'tournament' => $game->getTournament()->getId(),
            'match' => $matchesIds,
            'user' => array($game->getUserLeft()->getId(), $game->getUserRight()->getId())
        ));

        $bets = array();
        foreach($betsRowset as $bet)
            $bets[$bet->getMatch()->getId()][$bet->getUser()->getId()] = $bet;

        $data = array();
        $userLeftId = $game->getUserLeft()->getId();
        $userRightId = $game->getUserRight()->getId();
        foreach($tournamentMatches as $tMatch)
        {
            $match = $tMatch->getMatch();
            $userLeftBet = (!isset($bets[$match->getId()][$userLeftId])) ? null : $bets[$match->getId()][$userLeftId];
            $userRightBet = (!isset($bets[$match->getId()][$userRightId])) ? null : $bets[$match->getId()][$userRightId];

            $data[] = array(
                'match' => array(
                    'id' => $match->getId(),
                    'isProcessed' => $tMatch->getIsProcessed(),
                    'homeTeamTitle' => $match->getHomeTeam()->getTitle(),
                    'guestTeamTitle' => $match->getGuestTeam()->getTitle(),
                    'scoreLeft' => $match->getScoreLeft(),
                    'scoreRight' => $match->getScoreRight(),
                ),
                'points' => array(
                    'userLeft' => ($userLeftBet) ? $userLeftBet->getPoints() : 0,
                    'userRight' => ($userRightBet) ? $userRightBet->getPoints() : 0
                )
            );
        }

        return $data;
    }

    /**
    * Event handlers
    */

    /**
    * Обработчик события добавления матча в турнир
    *
    * @param \AppBundle\Event\TournamentEvent $event
    */
    public function onTournamentMatchAdded(\AppBundle\Event\TournamentEvent $event)
    {
      /**
      * @var \AppBundle\Entity\TournamentRound
      */
      $round = $event->getTournament();

      /**
      * @var \AppBundle\Entity\Tournament
      */
      $tournament = $round->getTournament();

      $this->updateTournamentStartDatetime($tournament);
    }

    /**
    * Обработчик события удаления матча из турнира
    *
    * @param \AppBundle\Event\TournamentEvent $event
    */
    public function onTournamentMatchDeleted(\AppBundle\Event\TournamentEvent $event)
    {
      /**
      * @var \AppBundle\Entity\TournamentRound
      */
      $round = $event->getTournament();

      /**
      * @var \AppBundle\Entity\Tournament
      */
      $tournament = $round->getTournament();

      $this->updateTournamentStartDatetime($tournament);
    }

    public function onTournamentStartedHandler(\AppBundle\Event\TournamentEvent $event)
    {
        /**
        * @var \AppBundle\Entity\TournamentRound $tournament
        */
        $tournament = $event->getTournament();

        $users = $this->em->getRepository('AppBundle\Entity\User')->findTournamentUsersOrderedByRating($tournament->getTournament()->getId());

        $games = $this->generateRoundRobinPairings(count($users));

        $realRoundsCount = count($games);

        $tournament->setRealRoundsCount($realRoundsCount);
        $this->em->persist($tournament);

        $this->em->flush();

        $this->em->getRepository('AppBundle\Entity\TournamentRoundMatch')->removeMatchesAndBetsFromRound($tournament->getTournament()->getId(), ($realRoundsCount + 1));

        $place = 1;
        foreach($users as $user)
        {
            $tournamentResult = new TournamentRoundResult();
            $tournamentResult->setTournament($tournament->getTournament());
            $tournamentResult->setRound(1);
            $tournamentResult->setPlace($place);
            $tournamentResult->setUser($user);
            $tournamentResult->setUserRating($user->getRating());
            $this->em->persist($tournamentResult);

            $place++;
        }

        $this->em->flush();

        foreach($games as $roundNum => $roundGames)
        {
            foreach($roundGames as $game)
            {
                $userLeft = (isset($users[$game["user_left"] - 1])) ? $users[$game["user_left"] - 1] : null;
                $userRight =  (isset($users[$game["user_right"] - 1])) ? $users[$game["user_right"] - 1] : null;

                if(!$userLeft || !$userRight)
                    continue;

                $tournamentGame = new TournamentRoundGame();
                $tournamentGame->setTournament($tournament->getTournament());
                $tournamentGame->setRound($roundNum);
                $tournamentGame->setUserLeft($userLeft);
                $tournamentGame->setUserRight($userRight);

                $this->em->persist($tournamentGame);
            }
        }

        $this->em->flush();
    }

    public function onTournamentUserAddedHandler(\AppBundle\Event\TournamentEvent $event)
    {
        /**
        * @var \AppBundle\Entity\TournamentRound $tournament
        */
        $tournament = $event->getTournament();

        $usersCount = $this->em->getRepository("\AppBundle\Entity\User")->findChampionshipUsersCount($tournament->getTournament()->getId());

        if($usersCount >= $tournament->getMaxUsersCount())
        {
            $tournament->getTournament()->setIsAccessible(false);

            $this->em->persist($tournament->getTournament());
            $this->em->flush();
        }
    }

    /******************************************************************************
    * Round Robin Pairing Generator
    * Author: Eugene Wee
    * Date: 23 May 2005
    * Last updated: 13 May 2007
    * Based on an algorithm by Tibor Simko.
    *
    * Copyright (c) 2005, 2007 Eugene Wee
    *
    * Permission is hereby granted, free of charge, to any person obtaining a copy
    * of this software and associated documentation files (the "Software"), to deal
    * in the Software without restriction, including without limitation the rights
    * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    * copies of the Software, and to permit persons to whom the Software is
    * furnished to do so, subject to the following conditions:
    *
    * The above copyright notice and this permission notice shall be included in
    * all copies or substantial portions of the Software.
    *
    * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    * THE SOFTWARE.
    ******************************************************************************/
    public function generateRoundRobinPairings($num_teams, $first_team='user_left', $second_team='user_right') {
        // Do we have a positive number of players? If not, default to 4.
        $num_teams = ($num_teams > 0) ? (int)$num_teams : 4;
        // If necessary, round up number of players to nearest even number.
        $num_teams += $num_teams % 2;

        // Format for pretty alignment of pairings across rounds.
        $format = "%0" . ceil(log10($num_teams)) . "d";
        $pairing = "$format-$format ";

        $games = array();

        // Generate the pairings for each round.
        for ($round = 1; $round < $num_teams; $round++) {
            $players_done = array();
            // Pair each player except the last.
            for ($player = 1; $player < $num_teams; $player++) {
                if (!in_array($player, $players_done)) {
                    // Select opponent.
                    $opponent = $round - $player;
                    $opponent += ($opponent < 0) ? $num_teams : 1;
                    // Ensure opponent is not the current player.
                    if ($opponent != $player) {
                        // Choose colours.
                        if (($player + $opponent) % 2 == 0 xor $player < $opponent) {
                            // Player plays white.
                          $games[$round][$player][$first_team] = $player;
                          $games[$round][$player][$second_team] = $opponent;
                        } else {
                            // Player plays black.
                          $games[$round][$player][$first_team] = $opponent;
                          $games[$round][$player][$second_team] = $player;
                        }

                        // This pair of players are done for this round.
                        $players_done[] = $player;
                        $players_done[] = $opponent;
                    }
                }
            }

            // Pair the last player.
            if ($round % 2 == 0) {
              $opponent = ($round + $num_teams) / 2;
              // Last player plays white.
              $games[$round][$player][$first_team] = $num_teams;
              $games[$round][$player][$second_team] = $opponent;
            } else {
              $opponent = ($round + 1) / 2;
              // Last player plays black.
              $games[$round][$player][$first_team] = $opponent;
              $games[$round][$player][$second_team] = $num_teams;
            }
        }
        return $games;
    }

    /**
    * Генерирует результаты туров по крону
    *
    * @param \AppBundle\Entity\Tournament $tournament
    */
    public function processResults(\AppBundle\Entity\Tournament $tournament)
    {
        $tournamentId = $tournament->getId();

        /**
        * @var $tournamentRound \AppBundle\Entity\TournamentRound
        */
        $tournamentRound = $this->em->getRepository("\AppBundle\Entity\TournamentRound")->findLazy($tournamentId);
        $currentRound = $tournamentRound->getCurrentRound();

        /**
        * Если нет необработанных матчей, которые закончились, то выходим
        */
        if(!$matchesIds = $this->em->getRepository("\AppBundle\Entity\TournamentRoundMatch")->findNonProcessedMatchesFinished($tournamentId, $currentRound))
            return true;

        //собираем idшники матчей
        $nonProcessedMatchesIds = array();
        foreach($matchesIds as $matchData)
        {
            $nonProcessedMatchesIds[] = $matchData["id"];
        }
        unset($matchesIds, $matchData);


        if(count($nonProcessedMatchesIds) == 0)
            return true;

        /**
        * Если среди необработанных матчей, которые закончились, есть необработанные ставки, то выходим
        */
        if(!$this->em->getRepository("\AppBundle\Entity\TournamentRoundMatch")->allBetsCalculated($tournamentId, $nonProcessedMatchesIds))
            return true;

        $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: SETTING POINTS FOR NON PROCCESSED MATCHES: ".serialize($nonProcessedMatchesIds));

        /**
        * Начисляем мячи в игры
        */
        $roundGames = $this->em->getRepository("AppBundle\Entity\TournamentRoundGame")->findByTournamentAndRound($tournamentId, $currentRound);
        $nonProcessedMatches = $this->em->getRepository("\AppBundle\Entity\TournamentRoundMatch")->findByIds($tournamentId, $nonProcessedMatchesIds);
        $roundBets = $this->__getBetsForEachUser($tournamentId, $nonProcessedMatchesIds);

        foreach($roundGames as &$game)
        {
            $pointsLeft = floatval($game->getPointsLeft());
            $pointsRight = floatval($game->getPointsRight());

            foreach($roundBets as $matchId => $usersBets)
            {
                $user = $game->getUserLeft();
                // начисляем очки левому игроку, если он делал ставку
                if(isset($usersBets[$user->getId()]))
                {
                    $bet = $usersBets[$user->getId()];

                    if(is_null($bet->getPoints()))
                    {
                        $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: EXCEPTION ON SETTING POINTS ON GAME RESULTS: "."Очки пользователю #".$user->getId()." за матч #".$matchId." ещё не начислены!");
                        return false;
                    }
                    $pointsLeft += floatval($bet->getPoints());
                }
                //$game->setPointsLeft($pointsLeft);

                $user = $game->getUserRight();
                // начисляем очки правому игроку, если он делал ставку
                if(isset($usersBets[$user->getId()]))
                {
                    $bet = $usersBets[$user->getId()];

                    if(is_null($bet->getPoints()))
                    {
                        $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: EXCEPTION ON SETTING POINTS ON GAME RESULTS: "."Очки пользователю #".$user->getId()." за матч #".$matchId." ещё не начислены!");
                        return false;
                    }

                    $pointsRight += floatval($bet->getPoints());
                }
                //$game->setPointsRight($pointsRight);
            } //foreach($roundBets as $matchId => $usersBets)

            $game->setPointsLeft($pointsLeft);
            $game->setPointsRight($pointsRight);

            $this->em->persist($game);

        } //foreach($roundGames as $game)

        $this->em->flush();

        unset($usersBets,$roundBets,$matchId,$roundGames,$game);

        foreach($nonProcessedMatches as &$tournamentMatch)
        {
            $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: SETTING MATCH PROCCESSED: ".serialize(array("match_id" => $tournamentMatch->getMatch()->getId())));
            $tournamentMatch->setIsProcessed(true);
            $this->em->persist($tournamentMatch);
        }

        $this->em->flush();

        /**
        * Если не все матчи окончились, то выходим
        */
        if(!$this->em->getRepository("\AppBundle\Entity\TournamentRoundMatch")->allRoundMatchesFinished($tournamentId, $currentRound))
            return true;

        /**
        * Если все матчи в туре закончились, но за какие-то матчи ещё не начислены очки, то выходим
        */
        if(!$this->em->getRepository("\AppBundle\Entity\TournamentRoundMatch")->matchesPointsCalculated($tournamentId, $currentRound))
            return true;

        $this->logHelper->add("SYS", "CRON WORKING: tournaments:set:results: ALL MATCHES FINISHED AND PROCCESSED IN GROUP STAGE IN ROUND $currentRound!");

        /**
        * Так как тур окончен, то заносим результаты тура в таблицу групповых результов по турам
        */
        $roundGames = $this->em->getRepository("AppBundle\Entity\TournamentRoundGame")->findByTournamentAndRound($tournamentId, $currentRound);
        $results = $this->em->getRepository("AppBundle\Entity\TournamentRoundResult")->findByTournamentAndRound($tournamentId, $currentRound);
        $roundResults = array();
        foreach($results as $result)
        {
            $roundResults[$result->getUser()->getId()] = $result;
        }
        unset($results, $result);

        foreach($roundGames as $game)
        {
            //начисляем очки левому пользователю
            $user = $game->getUserLeft();
            $result = $roundResults[$user->getId()];

            $result->setTotalGames($result->getTotalGames() + 1);
            $result->setUserRating($user->getRating());
            $result->setTotalGoalsScored($result->getTotalGoalsScored() + $game->getPointsLeft());
            $result->setTotalGoalsAgainst($result->getTotalGoalsAgainst() + $game->getPointsRight());
            $result->setTotalGoalsDifference($result->getTotalGoalsScored() - $result->getTotalGoalsAgainst());

            if($game->getPointsLeft() > $game->getPointsRight())
            {
                $result->setTotalPoints($result->getTotalPoints() + self::POINTS_FOR_WIN);
                $result->setTotalWins($result->getTotalWins() + 1);
            }
            elseif($game->getPointsLeft() < $game->getPointsRight())
            {
                $result->setTotalPoints($result->getTotalPoints() + self::POINTS_FOR_LOSS);
                $result->setTotalLoss($result->getTotalLoss() + 1);
            }
            elseif($game->getPointsLeft() == $game->getPointsRight())
            {
                $result->setTotalPoints($result->getTotalPoints() + self::POINTS_FOR_DRAW);
                $result->setTotalDraws($result->getTotalDraws() + 1);
            }
            $this->em->persist($result);

            //начисляем очки правому пользователю
            $user = $game->getUserRight();
            $result = $roundResults[$user->getId()];

            $result->setTotalGames($result->getTotalGames() + 1);
            $result->setUserRating($user->getRating());
            $result->setTotalGoalsScored($result->getTotalGoalsScored() + $game->getPointsRight());
            $result->setTotalGoalsAgainst($result->getTotalGoalsAgainst() + $game->getPointsLeft());
            $result->setTotalGoalsDifference($result->getTotalGoalsScored() - $result->getTotalGoalsAgainst());

            if($game->getPointsLeft() < $game->getPointsRight())
            {
                $result->setTotalPoints($result->getTotalPoints() + self::POINTS_FOR_WIN);
                $result->setTotalWins($result->getTotalWins() + 1);
            }
            elseif($game->getPointsLeft() > $game->getPointsRight())
            {
                $result->setTotalPoints($result->getTotalPoints() + self::POINTS_FOR_LOSS);
                $result->setTotalLoss($result->getTotalLoss() + 1);
            }
            elseif($game->getPointsLeft() == $game->getPointsRight())
            {
                $result->setTotalPoints($result->getTotalPoints() + self::POINTS_FOR_DRAW);
                $result->setTotalDraws($result->getTotalDraws() + 1);
            }
            $this->em->persist($result);
        }

        $this->em->flush();
        unset($user,$result,$game,$roundGames,$roundResults);

        /**
        * Сортируем результаты по регламенту в каждой группе
        */
        $sortedResults = $this->em->getRepository("AppBundle\Entity\TournamentRoundResult")->findSortedByTournamentAndRound($tournamentId, $currentRound);
        $place = 1;
        foreach($sortedResults as $result)
        {
            $result->setPlace($place);
            $this->em->persist($result);
            $place++;
        }

        $this->em->flush();
        unset($sortedResults,$result,$place);

        /**
        * Предзаполняем результаты для следующего тура
        */
        $prevRoundResults = $this->em->getRepository("AppBundle\Entity\TournamentRoundResult")->findByTournamentAndRound($tournamentId, $currentRound);
        foreach($prevRoundResults as $result)
        {
            $TournamentRoundResult = new TournamentRoundResult();
            $TournamentRoundResult->setTournament($result->getTournament());
            $TournamentRoundResult->setUser($result->getUser());
            $TournamentRoundResult->setRound($currentRound + 1);
            $TournamentRoundResult->setPlace($result->getPlace());
            $TournamentRoundResult->setTotalGames($result->getTotalGames());
            $TournamentRoundResult->setTotalWins($result->getTotalWins());
            $TournamentRoundResult->setTotalDraws($result->getTotalDraws());
            $TournamentRoundResult->setTotalLoss($result->getTotalLoss());
            $TournamentRoundResult->setTotalGoalsAgainst($result->getTotalGoalsAgainst());
            $TournamentRoundResult->setTotalGoalsScored($result->getTotalGoalsScored());
            $TournamentRoundResult->setTotalGoalsDifference($result->getTotalGoalsDifference());
            $TournamentRoundResult->setTotalPoints($result->getTotalPoints());
            $TournamentRoundResult->setUserRating($result->getUser()->getRating());

            $this->em->persist($TournamentRoundResult);
        }
        $this->em->flush();
        unset($prevRoundResults,$result,$TournamentRoundResult);

        /**
        * Переходим в следующий тур, если он есть.
        */
        if($currentRound + 1 <= $tournamentRound->getRealRoundsCount())
        {
            $tournamentRound->setCurrentRound($currentRound + 1);
            $this->em->persist($tournamentRound);
            $this->em->flush();
            return true;
        }

        /**
        * Если это последний тур, то записываем побидетелй в таблицу результатов для подсчёта
        */
        if($tournamentRound->getRealRoundsCount() == $currentRound)
        {
            $lastRoundResults = $this->em->getRepository("AppBundle\Entity\TournamentRoundResult")->findByTournamentAndRound($tournamentId, $currentRound);

            foreach($lastRoundResults as $result)
            {
                $TournamentResult = new TournamentResult();
                $TournamentResult->setTournament($result->getTournament());
                $TournamentResult->setUser($result->getUser());
                $TournamentResult->setPlace($result->getPlace());
                $TournamentResult->setPoints($result->getTotalPoints());

                $this->em->persist($TournamentResult);
            }

            unset($lastRoundResults,$result);

            $tournamentRound->getTournament()->setIsOver(true);
            $tournamentRound->getTournament()->setIsResultsReady(true);
            $tournamentRound->getTournament()->setEndDatetime( new \DateTime() );

            $this->em->flush();

            return true;
        }
    }

    /**
    * Получает ставки участников по id турнира и номеру тура и распределяет их по матчам и пользователям
    *
    * @param int $tournament_id
    * @param int $round
    * @return array $userBets
    */
    private function __getBetsForEachUser($tournament_id, $matchesIds)
    {
        $bets = $this->em->getRepository("\AppBundle\Entity\Bet")->findByTournamentAndMatchesIds($tournament_id, $matchesIds);

        $userBets = array();
        foreach($bets as $bet)
        {
            $userBets[$bet->getMatch()->getId()][$bet->getUser()->getId()] = $bet;
        }
        unset($bets, $bet);

        return $userBets;
    }
}