<?php
namespace AppBundle\Services;

class TournamentsHelper
{
    private $em;
    
    /**    
    * @var \AppBundle\Entity\TournamentRepository
    */
    private $repository;
    
    /**
    * @var \AppBundle\Services\BalanceHelper
    */
    private $balanceHelper;

    /**
    * Статус турнира "Не начался"
    */
    const STATUS_NOT_STARTED = 'STATUS_NOT_STARTED';
    
    /**
    * Статус турнира "Отменён"
    */
    const STATUS_REJECTED = 'STATUS_REJECTED';
    
    /**
    * Статус турнира "Идёт"
    */
    const STATUS_GOES = 'STATUS_GOES';
    
    /**
    * Статус турнира "Окончен"
    */
    const STATUS_OVER = 'STATUS_OVER';
        
    
    public function __construct(\Doctrine\ORM\EntityManager $em, \AppBundle\Services\BalanceHelper $balanceHelper)
    {
        $this->em = $em;
        $this->repository = $em->getRepository('AppBundle\Entity\Tournament');
        $this->balanceHelper = $balanceHelper;
    }
    
    /**
    * Возвращает полный бюджет турнира
    * 
    * @param \AppBundle\Entity\Tournament $tournament
    * @return float
    */
    public function getTournamentBudget(\AppBundle\Entity\Tournament $tournament)
    {
        return $tournament->getInitBudget() + $tournament->getFee()*count($tournament->getUsers());
    }
    
    /**
    * Возвращает призовой фонд турнира (Полный бюджет - комиссия)
    * 
    * @param \AppBundle\Entity\Tournament $tournament
    * @return float
    */
    public function getTournamentPrizePool(\AppBundle\Entity\Tournament $tournament)
    {
      $budget = $this->getTournamentBudget($tournament);
      
      return round($budget - ($budget*$tournament->getCommission())/100, 2);
    }
    
    /**
    * Возвращает коммиссию системы за турнир
    *     
    * @param float $tournamentBudget Бюджет турнира
    * @param float $commissionPercent Процент коммиссии
    * @return float
    */
    public function getSystemCommision($tournamentBudget, $commissionPercent)
    {
        return round(($tournamentBudget*$commissionPercent)/100, 2);
    }
    
    /**
    * Возвращает статус турнира
    * 
    * @param \AppBundle\Entity\Tournament $tournament
    * @return int
    */
    public function getTournamentStatus(\AppBundle\Entity\Tournament $tournament)
    {
      if(!$tournament->getIsStarted())
        return self::STATUS_NOT_STARTED;
      
      if($tournament->getIsReject())
        return self::STATUS_REJECTED;
      
      if($tournament->getIsOver() && $tournament->getIsResultsReady() && $tournament->getIsWinPrizeDistributed())
        return self::STATUS_OVER;
        
      return self::STATUS_GOES;
    }
    
    /**
    * Проверяет возможность пользователя зайти на страницу просмотра турнира
    */
    public function userCanViewTournament($tournament, $user)
    {        
        if ($tournament->getTournament()->getIsReject() === true)
            return false;
        
        if($tournament && $tournament->getTournament()->getIsReady())
            return true;

        return false;
    }

    /**
    * Проверяет возможность пользователя отправить заявку на участие в турнире
    */
    public function userCanSendRequest($tournament, $user)
    {        
        if(!is_a($user, '\AppBundle\Entity\User'))
            return false;        

        if($this->requestWasSent($tournament, $user))
            return false;

        if($this->repository->isUserInTournament($tournament->getTournament()->getId(), $user->getId()))
            return false;
            
        if($this->isTournamentUsersReachMaxCount($tournament))
            return false;

        if(!$this->isUserHaveMoneyForRequest($tournament, $user))
            return false;

        return true;
    }

    public function isUserHaveMoneyForRequest($tournament, $user)
    {
        $fee = $tournament->getTournament()->getFee();

        $balance = $this->balanceHelper->getUserBalance($user);

        return ($balance - $fee >= 0);
    }

    /**
    * Проверяет является ли пользователь автором турнира
    */
    public function userIsAuthor($tournament, $user)
    {
        if(!is_a($user, '\AppBundle\Entity\User'))
            return false;

        return $tournament->getTournament()->getAuthor()->getId() == $user->getId();
    }

    /**
    *   Проверяет доступен ли турнир для пользователя
    */
    public function tournamentIsAccessibleForUser($tournament, $user)
    {
        if(!is_a($user, '\AppBundle\Entity\User'))
            return false;

        if($tournament->getTournament()->getIsStarted())
            return false;        

        return true;
    }

    /*
    *   Проверяет являетcя ли пользователь участником турнира
    */
    public function isUserInTournament($tournament, $user)
    {
        if(!is_a($user, '\AppBundle\Entity\User'))
            return false;

        if($this->repository->isUserInTournament($tournament->getTournament()->getId(), $user->getId()))
            return true;

        return false;
    }

    /*
    *   Проверяет достигло ли максимума количиство участников в турнире
    */
    public function isTournamentUsersReachMaxCount($tournament)
    {
        if(count($tournament->getTournament()->getUsers()) >= $tournament->getMaxUsersCount())
            return true;

        return false;
    }

    /**
    *   Проверяет не посылал ли уже пользователь заявку на турнир
    */
    public function requestWasSent($tournament, $user)
    {
        if(!is_a($user, '\AppBundle\Entity\User'))
            return false;

        if($this->repository->requestWasSent($tournament->getTournament()->getId(), $user->getId()))
            return true;

        return false;
    }

    /**
    *   Проверяет может ли пользователь делать ставки
    */
    public function userCanSetBet($tournament, $user)
    {
        if(!is_a($user, '\AppBundle\Entity\User'))
            return false;
        
        if(!$tournament || !$tournament->getTournament()->getIsReady() || $tournament->getTournament()->getIsOver() || $tournament->getTournament()->getIsResultsReady())
            return false;

        if(!$this->isUserInTournament($tournament, $user))
            return false;

        return true;
    }
    
    /**
    *   Проверяет может ли пользователь сделать ставку на данный матч
    */
    public function userCanSetBetOnMatch(\AppBundle\Entity\Match $match, $user)
    {
      if(!is_a($user, '\AppBundle\Entity\User'))
          return false;
      
      if($match && !$match->getIsStarted() &&  !$match->getIsOver() && !$match->getIsRejected())
        return true;
        
      return false;
    }
    
    /**
    *   Проверяет может ли пользователь увидеть ставки других участников да конкретный матч
    */
    public function userCanViewUsersBetsOnMatch(\AppBundle\Entity\Match $match, $user)
    {
      if(!is_a($user, '\AppBundle\Entity\User'))
          return false;
      
      if($match && $match->getIsStarted() && !$match->getIsRejected())
        return true;
        
      return false;
    }    
    
    public function userBetsWalk(&$bets)
    {
        $userBets = array();
        array_walk($bets, function(&$bet) use (&$userBets){
            $userBets[$bet->getMatch()->getId()] = $bet;
        });
        return $userBets;
    }

    /**
    * Проверяет, что переданные матчи окончены или отменены и очки за ставки на эти матчи начислены
    */
    public function isMatchesFinishedAndPointsCalculated($matches, $bets)
    {
        foreach($matches as $match)
        {
            if(!$match)
                return false;
            
            if(!$match->getMatch()->getIsStarted())
                return false;
                
            if(!$match->getMatch()->getIsOver() && !$match->getMatch()->getIsRejected())
                return false;
                
            //если ниодин пользователь не сделал ставку на данный матч, то считаем что все ставки сделаны.
            if(!isset($bets[$match->getMatch()->getId()]))
                return true;
            
            //если ставки всё таки есть, то нужно проверить выставились ли за них очки
            foreach($bets[$match->getMatch()->getId()] as $bet)
            {
                if(!is_numeric($bet->getPoints()))
                    return false;
            }
        }
        
        return true;
    }        

    public function isUserInTournamentPlayoff(\AppBundle\Entity\TournamentPlayoff $tournament, $user)
    {
        if(!is_a($user, '\AppBundle\Entity\User'))
          return false;

      return $this->em->getRepository('\AppBundle\Entity\TournamentPlayoffGame')->isUserInPlayoff($tournament->getTournament()->getId(), $user->getId());
    }    

    public function isUserInChampionsleaguePlayoffRound(\AppBundle\Entity\TournamentChampionsleague $tournament, $user, $round)
    {
        if(!is_a($user, '\AppBundle\Entity\User'))
          return false;

        if($tournament->getStage() != \AppBundle\Entity\TournamentChampionsleague::STAGE_PLAYOFF)
            return true;      

        if($round <= $tournament->getCurrentRound())
            return $this->em->getRepository('\AppBundle\Entity\TournamentChampionsleaguePlayoffGame')->isUserInPlayoffRound($tournament->getTournament()->getId(), $user->getId(), $round);
        else
            return $this->em->getRepository('\AppBundle\Entity\TournamentChampionsleaguePlayoffGame')->isUserInPlayoffRound($tournament->getTournament()->getId(), $user->getId(), $tournament->getCurrentRound());
    }
    
    /**
    * Event Handlers:
    */
    
    /**
    * Обработчик события старта турнира
    * 
    * @param \AppBundle\Event\TournamentEvent $event
    */
    public function onTournamentStarted(\AppBundle\Event\TournamentEvent $event)
    {
        /**        
        * @var AppBundle\Entity\Tournament $tournament
        */
        $tournament = $event->getTournament();
        
        $tournament->getTournament()->setIsAccessible(false);
        
        $this->em->persist($tournament->getTournament());
        $this->em->flush();
    }
    
    /**
    * Обработчик события отмены турнира
    * 
    * @param \AppBundle\Event\TournamentEvent $event
    */
    public function onTournamentRejectedHandler(\AppBundle\Event\TournamentEvent $event)
    {      
      /**      
      * @var \AppBundle\Entity\Tournament
      */
      $tournament = $event->getTournament()->getTournament();
      
      /**      
      * @var \AppBundle\Entity\User[]
      */
      $users = $tournament->getUsers();
      
      $fee = $tournament->getFee();
      
      foreach($users as $user)
      {
        $note = $this->balanceHelper->getReservFeeReturnNote($tournament, $fee);
        $bill = $this->balanceHelper->createTournamentFeeReturnBill($user, $fee, $note);
        $this->balanceHelper->processBill($bill);                
      }
    }
}