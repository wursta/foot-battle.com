<?php
namespace AppBundle\Services;

use Symfony\Component\Security\Core\User\UserInterface;

class UserHelper
{
    protected $em;

    /**
    * @var \AppBundle\Entity\UserRepository
    */
    protected $userRep;

    protected $mailer_helper;
    protected $balance_helper;
    protected $router;

    /**
    * Уровень партнёрской программы - 1
    */
    const PARTNER_LEVEL_1 = 1;

    /**
    * Уровень партнёрской программы - 2
    */
    const PARTNER_LEVEL_2 = 2;

    /**
    * Максимальная длина никнейма
    */
    const USERNAME_MAXLENGTH = 25;

    public function __construct(\Doctrine\ORM\EntityManager $em,
                                \AppBundle\Services\BalanceHelper $balance_helper,
                                \AppBundle\Services\MailerHelper $mailer_helper,
                                \Symfony\Bundle\FrameworkBundle\Routing\Router $router)
    {
        $this->em = $em;
        $this->userRep = $this->em->getRepository('AppBundle\Entity\User');
        $this->mailer_helper = $mailer_helper;
        $this->balance_helper = $balance_helper;
        $this->router = $router;
    }

    public function sendConfirmationEmailToUser(UserInterface $user)
    {
        $confirmationUrl = $this->router->generate('registration/confirm', array("hash" => $this->getUserHash($user)), true);

        $this->mailer_helper->sendConfirmationEmail($user->getEmail(), $user, $confirmationUrl);
    }

    public function sendTournamentConfirmationRequestEmailToUser(\AppBundle\Entity\User $user, \AppBundle\Entity\Tournament $tournament)
    {
        $tournamentUrl = $this->router->generate('tournament/view', array("_locale" => $user->getLocale(), "tournament_id" => $tournament->getId()), true);
        $this->mailer_helper->sendTournamentConfirmationRequestEmail($user->getEmail(), $tournamentUrl, $user, $tournament);
    }

    public function getUserHash($user)
    {
        return md5($user->getEmail());
    }

    public function generatePassword()
    {
        //Символы используемые в генерируемом пароле
        $possibleChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@";
        $password = '';

        //Длина генерируемого пароля
        $pwlen = 6;

        for($i = 0; $i < $pwlen; $i++) {
            $rand = rand(0, strlen($possibleChars) - 1);
            $password .= substr($possibleChars, $rand, 1);
        }

        return $password;
    }

    /**
    * Генерирует реферальный код пользователя
    *
    * @param int $userId ID пользователя
    * @return string Реферальный код
    */
    public function generateReferralCode($userId)
    {
      return substr(md5('referal_user_'.$userId), 0, 7);
    }

    /**
    * Возвращает пользователя по реферальному коду или false, если такого пользователя нет
    *
    * @param string $referalHash Код из реферальной ссылки на регистрацию
    *
    * @return \AppBundle\Entity\User
    */
    public function getUserByReferralCode($referralCode)
    {
      $user = $this->userRep->findUserByReferralCode($referralCode);

      if($user instanceof \AppBundle\Entity\User)
        return $user;
      else
        return false;
    }

    /**
    * Проверяет переданный email на валидность.
    *
    * @param string $email Проверяемый email.
    *
    * @return bool
    */
    public function isValidEmail($email)
    {
        return preg_match("/@/", $email);
    }

    /**
    * Проверка мэйла на уникальность
    *
    * @param string $email Email пользователя.
    *
    * @return bool
    */
    public function isUniqueEmail($email)
    {
        $user = $this->userRep->findOneBy(array(
            "email" => $email
        ));

        if(!$user)
            return true;

        return false;
    }

    /**
    * Проверят никнейм на уникальность
    *
    * @param string $username Никнейм
    * @param int $userId В случае профиля необходимо проверить существует ли пользователь с таким же никнеймом, кроме самого этого пользовтеля
    * @return bool
    */
    public function isUniqueUsername($username, $userId = false)
    {
      $user = $this->userRep->findUserByUsername($username, $userId);

      if($user)
        return false;

      return true;
    }

    /**
    * Валидирует никнейм на предмет наличия запрещённых символов и длину
    *
    * @param string $username
    * @return bool
    */
    public function isValidUsername($username)
    {
      if(preg_match('/[^a-zA-Z0-9_]/', $username) !== 0)
        return false;

      if(mb_strlen($username) > self::USERNAME_MAXLENGTH)
        return false;

      return true;
    }

    public function getForNormalizer(\AppBundle\Entity\User $user = null) {
        if(!$user)
            return array();

        return array(
            "id" => $user->getId(),
            "nick" => $user->getNick(),
            "avatar" => $user->getAvatar()
        );
    }

    /**
    * Handlers
    */
    public function onTournamentUserAdded($event)
    {
      /**
      * @var \AppBundle\Entity\User $user
      */
      $user = $event->getUser();

      $tournament = $event->getTournament();

      /**
      * @var \AppBundle\Entity\Tournament $tournament
      */
      $tournament = $tournament->getTournament();

      /**
      * Отправка письма об одобрении заявки пользователя создателем турнира
      */
      $this->sendTournamentConfirmationRequestEmailToUser($user, $tournament);
    }

    public function onTournamentRequestDismissed($event)
    {
      /**
      * @var \AppBundle\Entity\User $user
      */
      $user = $event->getUser();

      $tournament = $event->getTournament();
      /**
      * @var \AppBundle\Entity\Tournament $tournament
      */
      $tournament = $tournament->getTournament();

      $fee = $tournament->getFee();

      if($fee > 0)
      {
        $this->balance_helper->returnTournamentFee($user, $tournament, $fee);
      }
    }
}