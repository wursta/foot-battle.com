<?php
namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
* Класс с Unit-тестами для класса RegistrationController
* @coversDefaultClass RegistrationController
*/
class RegistrationControllerTest extends WebTestCase
{
    /**
    * Тест для проверки, что страница регистрации доступна и на ней нахождится форма,
    * а также что форма действительно регистрирует пользователя
    * @covers ::indexAction
    */
    public function testIndex()
    {
        $client = static::createClient();        
        
        /**        
        * @var \Symfony\Component\DomCrawler\Crawler
        */
        $crawler = $client->request('GET', '/ru/registration');
        
        $this->assertTrue($crawler->filter('form')->count() > 0);
        
        $form = $crawler->filter('#registration_user_save')->form();
        echo "<pre>";
        var_dump($form->getValues());
        echo "</pre>";
        exit;
        
        $form["registration[user][email]"] = "unit@test.com";
        $form["registration[user][username]"] = "Unit Test";
        $form["registration[user][password][password]"] = "Test";
        $form["registration[user][password][confirm]"] = "Test";
        $form["registration[terms]"] = "1";
        
        $crawler = $client->submit($form);
        
        echo $crawler->html();
        /*
        $postParams = array(
          "registration" => array(
            "user" => array(
              "email" => "unit@test.com",
              "username" => "Unittest User",
              "password" => array(
                "password" => "Test",
                "confirm" => "Test"
              ),
              "save" => "Зарегистрироваться"
            ),
            "terms" => 1
          )
        );
        $crawler = $client->request('POST', '/ru/registration', $postParams);
        
        echo $crawler->html();
        */
        
    }        
}
