<?php
namespace AppBundle;

final class TournamentEvents
{
    /**
     * Событие вызывается каждый раз когда происходит старт турнира.
     *
     * В обработчик события передаётся объект класса AppBunle\Event\TournamentEvent
     *
     * @var string
     */
    const TOURNAMENT_STARTED = 'tournament.started';
    const TOURNAMENT_CHAMPIONSHIP_STARTED = 'tournament.championship.started';
    const TOURNAMENT_ROUND_STARTED = 'tournament.round.started';
    const TOURNAMENT_PLAYOFF_STARTED = 'tournament.playoff.started';
    const TOURNAMENT_CHAMPIONSLEAGUE_STARTED = 'tournament.championsleague.started';
    
    /**
     * Событие вызывается каждый раз когда происходит отмена турнира.
     *
     * В обработчик события передаётся объект класса AppBunle\Event\TournamentEvent
     *
     * @var string
     */
    const TOURNAMENT_REJECTED = 'tournament.rejected';
    const TOURNAMENT_CHAMPIONSHIP_REJECTED = 'tournament.championship.rejected';
    const TOURNAMENT_ROUND_REJECTED = 'tournament.round.rejected';
    const TOURNAMENT_PLAYOFF_REJECTED = 'tournament.playoff.rejected';
    const TOURNAMENT_CHAMPIONSLEAGUE_REJECTED = 'tournament.championsleague.rejected';

    /**
     * Событие добавления матча в турнир.
     *
     * В обработчик события передаётся объект класса AppBunle\Event\TournamentEvent
     *
     * @var string
     */
    const CHAMPIONSHIP_MATCH_ADDED = 'tournament.championship.match.added';
    const ROUND_MATCH_ADDED = 'tournament.round.match.added';
    const PLAYOFF_MATCH_ADDED = 'tournament.playoff.match.added';
    const CHAMPIONSLEAGUE_MATCH_ADDED = 'tournament.championsleague.match.added';
    
    /**
     * Событие удаления матча из турнира.
     *
     * В обработчик события передаётся объект класса AppBunle\Event\TournamentEvent
     *
     * @var string
     */
    const CHAMPIONSHIP_MATCH_DELETED = 'tournament.championship.match.deleted';
    const ROUND_MATCH_DELETED = 'tournament.round.match.deleted';
    const PLAYOFF_MATCH_DELETED = 'tournament.playoff.match.deleted';
    const CHAMPIONSLEAGUE_MATCH_DELETED = 'tournament.championsleague.match.deleted';
    
    /**
     * Событие вызывается каждый раз когда происходит добавление пользователя в турнир.
     *
     * В обработчик события передаётся объект класса AppBunle\Event\TournamentEvent
     *
     * @var string
     */
    const CHAMPIONSHIP_USER_ADDED = 'tournament.championship.user.added';
    const ROUND_USER_ADDED = 'tournament.round.user.added';
    const PLAYOFF_USER_ADDED = 'tournament.playoff.user.added';
    const CHAMPIONSLEAGUE_USER_ADDED = 'tournament.championsleague.user.added';

    final public static function getTournamentStartedEvent($format)
    {
        switch($format)
        {
            case "championship":
                return self::TOURNAMENT_CHAMPIONSHIP_STARTED;
            break;

            case "round":
                return self::TOURNAMENT_ROUND_STARTED;
            break;

            case "playoff":
                return self::TOURNAMENT_PLAYOFF_STARTED;
            break;

            case "championsleague":
                return self::TOURNAMENT_CHAMPIONSLEAGUE_STARTED;
            break;
        }
    }
    
    final public static function getTournamentRejectedEvent($format)
    {
        switch($format)
        {
            case "championship":
                return self::TOURNAMENT_CHAMPIONSHIP_REJECTED;
            break;

            case "round":
                return self::TOURNAMENT_ROUND_REJECTED;
            break;

            case "playoff":
                return self::TOURNAMENT_PLAYOFF_REJECTED;
            break;

            case "championsleague":
                return self::TOURNAMENT_CHAMPIONSLEAGUE_REJECTED;
            break;
        }
    }
    
    final public static function getUserAddedEvent($format)
    {
        switch($format)
        {
            case "championship":
                return self::CHAMPIONSHIP_USER_ADDED;
            break;

            case "round":
                return self::ROUND_USER_ADDED;
            break;

            case "playoff":
                return self::PLAYOFF_USER_ADDED;
            break;

            case "championsleague":
                return self::CHAMPIONSLEAGUE_USER_ADDED;
            break;
        }
    }
}
?>
