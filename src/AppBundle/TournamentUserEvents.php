<?php
namespace AppBundle;

final class TournamentUserEvents
{    
    /**
     * Событие вызывается каждый раз когда происходит добавление пользователя в турнир.
     *
     * В обработчик события передаётся объект класса AppBunle\Event\UserEvent
     *
     * @var string
     */
    const TOURNAMENT_USER_ADDED = 'tournament.user.added';    
    
    /**
     * Событие вызывается каждый раз когда происходит отклонение заявки пользователя в турнир.
     *
     * В обработчик события передаётся объект класса AppBunle\Event\UserEvent
     *
     * @var string
     */
    const TOURNAMENT_REQUEST_DISMISSED = 'tournament.request.dismissed';
}
?>
