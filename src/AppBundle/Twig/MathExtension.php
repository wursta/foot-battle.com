<?php
namespace AppBundle\Twig;

class MathExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('playoff', array($this, 'playoff')),
        );
    }

    public function playoff($roundNum, $roundsCount){        
        return '1/'.pow(2, ($roundsCount - $roundNum));
    }

    public function getName()
    {
        return 'math_extension';
    }
}
?>