<?php
namespace AppBundle\Twig;

class PlayoffRoundNameExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('playoffRoundName', array($this, 'playoffRoundName')),
        );
    }
    
    public function playoffRoundName($roundNum, $totalRoundsCount, $finalName = 'Финал', $semiFinalName = 'Полуфинал', $otherFinalSubstr = 'финала'){
        $roundName = '1/'.pow(2, ($totalRoundsCount - $roundNum));
        
        if($roundName == '1/1')
          return $finalName;
        elseif($roundName == '1/2')
          return $semiFinalName;
        else
          return $roundName . ' ' . $otherFinalSubstr;        
    }

    public function getName()
    {
        return 'playoff_round_name_extension';
    }
}
?>