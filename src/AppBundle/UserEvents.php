<?php
namespace AppBundle;

final class UserEvents
{
  /**
   * Событие подтверждения регистрации пользователем
   *
   * В обработчик события передаётся объект класса AppBunle\Event\UserEvent
   *
   * @var string
   */
  const USER_REGISTRATION_CONFIRM = 'user.registration.confirm';
}