#!/bin/bash

#Параметры
ENV=$1

#Текущая директория
CURRENT_DIR=`dirname $0`

# Переходим в корневую директорию проекта
pushd $CURRENT_DIR/.. > /dev/null
BASE_DIR=`pwd`
popd > /dev/null

function checkParams()
{
    if [[ $ENV == "" ]]
    then
    echo "Usage: $0 environment"
    exit 1
    fi
}

function copyAppFile()
{
    echo "Create app file"

    if [[ $ENV = "prod" ]]
    then
        local appfile="app.php"
    else
        local appfile="app_$ENV.php"
    fi

    cp -f $BASE_DIR/tools/installer/$appfile $BASE_DIR/web/app.php
}

function buildApp()
{
    echo "Building application"

    copyAppFile
}

checkParams
buildApp