/**
* Need to deprecate
* 
* @type Object
*/
AppBundle = {
    Config: {
        _locale: null,
        routing: {
            leagues: '/json/leagues/',
            matches: '/json/matches/',
            set_match_bet_score: '/html/bet/set_form',
            users_bets_data: '/html/tournament/users_bets',
            last_matches_data: '/html/teams/last_matches'
        },
        text: {
            loading: 'Загрузка...'
        },
    },    
    Util: {
        loading: function(action, text)
        {        
            if(text == undefined)
                var text = AppBundle.Config.text.loading;
            
            switch(action)
            {
                case "show":
                    $('#loading_overflow').remove();
                    $('#loading_indicator').remove();
                    $('body').append('<div id="loading_overflow"></div><div id="loading_indicator"><span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> '+text+'</div>');
                break;
                
                case "hide":
                    $('#loading_overflow').remove();
                    $('#loading_indicator').remove();
                break;
            }
        },
        alertBox: function(title, text)
        {
            $('#alertBox').remove();
            $('body').append('<div id="alertBox" class="modal fade"><div class="modal-dialog modal-sm"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button><h4 class="modal-title">'+title+'</h4></div><div class="modal-body"><p>'+text+'</p></div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">OK</button></div></div></div></div>');
            $('#alertBox').modal('show');
        }
    }    
};

AppBundle.Leagues = {    
    store : null,
    load: function(params) {
        var self = AppBundle.Leagues;
        
        if(params.postParams == undefined)
            params.postParams = {};
        
        $.ajax({
            dataType: "JSON",
            url: AppBundle.Config.routing.leagues,
            method: "POST",
            data: params.postParams,
            success: function(data) {
                self.store = data;
                
                if(params.listeners.load != undefined)
                {
                    params.listeners.load(data);
                }
            }
        });
    },
    renderSelect: function(selectElement, params) {
        var self = AppBundle.Leagues;  
        
        if(self.store == null)
            return false;
        
        selectElement.empty();
        for(var i in self.store)
        {
            var league = self.store[i];
            var option = document.createElement("option");
            option.value = league.id;
            option.text = league.leagueName;
            if(params.value != undefined && params.value == league.id)
                option.selected = true;
            selectElement.append(option);
        }
    }
}

AppBundle.Matches = {
    store: null,
    tournamentId : null,
    round: null,    
    game: null,
    lastLeagueSelected: null,    
    load: function(params) {
        var self = AppBundle.Matches;
        
        if(params.postParams == undefined)
            params.postParams = {};
        
        if(params.listeners.beforeload != undefined)
        {
            params.listeners.beforeload();
        }
        
        $.ajax({
            dataType: "JSON",
            url: AppBundle.Config.routing.matches,
            method: "POST",
            data: params.postParams,
            success: function(data) {
                self.store = data;
                
                if(params.listeners.load != undefined)
                {
                    params.listeners.load(data);
                }
            }
        });
    },
    singleSelect: function(params) {
        var self = AppBundle.Matches;
        
        var targetButton = params.target;
        var modalWindow = $('#appbundle_matches_single_select');
        var dataContainer = $('#appbundle_matches_single_select_data_container');
        var leaguesSelect = $('#appbundle_matches_single_select_leagues_select');
        var matchesTable = $('#appbundle_matches_single_select_matches_list');        
        var findButton = $('#appbundle_matches_single_select_find_button');
        var loadingIndicator = $('#appbundle_matches_single_select_loading_indicator');
        
        if(modalWindow.length == 0 || leaguesSelect.length == 0 || matchesTable.length == 0)
            return false;
        
        if(params.tournamentId != undefined)
            self.tournamentId = params.tournamentId;
        
        if(params.round != undefined)
            self.round = params.round;                
            
        if(params.game != undefined)
            self.game = params.game;                
        
        AppBundle.Util.loading('show');                
        
        var onMatchesLoadHandler = function(records) {            
            AppBundle.Matches.renderSelectTable(matchesTable, {
                listeners: {
                    'select': function(match) {
                        $(targetButton).trigger('appbundle.matches.singleSelect.select', [match]);
                        modalWindow.modal('hide');
                    }
                }
            });
            loadingIndicator.hide();
            dataContainer.show();
            dataContainer.scrollTop(0);
        }
        
        var onMatchesBeforeLoadHandler = function() {
            dataContainer.hide();
            loadingIndicator.show();
        }
        
        findButton.on('click', function(event){
            self.lastLeagueSelected = leaguesSelect.val();
            AppBundle.Matches.load({
                postParams: {
                    leagueId: leaguesSelect.val(),
                    tournamentId: self.tournamentId                    
                },
                listeners: {
                    'beforeload': function() {
                        onMatchesBeforeLoadHandler();
                    },
                    'load': function(records) {
                        onMatchesLoadHandler(records);
                    }                            
                }
            });
        });
        
        AppBundle.Leagues.load({
            postParams: {
                tournamentId: self.tournamentId
            },
            listeners: {
                'load': function(records) {                    
                    AppBundle.Leagues.renderSelect(leaguesSelect, {
                        value : self.lastLeagueSelected
                    });
                    
                    AppBundle.Matches.load({
                        postParams: {
                            leagueId: leaguesSelect.val(),
                            tournamentId: self.tournamentId                            
                        },
                        listeners: {
                            'beforeload': function() {
                                onMatchesBeforeLoadHandler();
                            },
                            'load': function(records) {
                                onMatchesLoadHandler(records);
                                AppBundle.Util.loading('hide');
                                modalWindow.modal('show');
                            }                            
                        }
                    })                                        
                }
            }
        });
    },
    renderSelectTable: function(tableElement, params) {
        var self = AppBundle.Matches;  
        
        if(self.store == null)
            return false;
        
        tableElement.find('tbody').empty();
        
        for(var i in self.store)
        {                    
            var match = self.store[i];

            var match_date = '';
            if(match.match_date != null)
                match_date = match.match_date;

            var match_time = '';
            if(match.match_time != null)
                match_time = match.match_time;
            html = '<tr><td class="text-center">'+match.tour_number+'</td><td class="text-center">'+match_date+'</td><td class="text-center">'+match_time+'</td><td>'+match.home_team+'</td><td></td><td>'+match.guest_team+'</td><td><button type="button" class="btn btn-xs btn-primary" data-storeindex='+i+'>Выбрать</button></td></tr>';
            
            var trElement = $(html).appendTo(tableElement.find('tbody'));
            trElement.find('button').on('click', function(event){
                if(params.listeners.select != undefined)
                {
                    params.listeners.select(self.store[$(event.target).data('storeindex')]);
                }
            });            
        }
    }
};

AppBundle.Bets = {
  showSetBetWindow: function(params) {
    match = params.match;
    
    if(match == undefined)
      return false;
    
    tournament = false;
    if(params.tournament != undefined)
        tournament = params.tournament;
      
    var modalWindow = $('#appbundle_bets_set_score');
    var container = $('#appbundle_bets_set_score_container');
    
    if(modalWindow.length == 0 || container.length == 0)
      return false;
    
    AppBundle.Util.loading('show', 'Получение данных о матче...');    
    
    var postData = { match_id: match };
    
    if(tournament)
        postData.tournament_id = tournament;
    
    $.ajax({
            dataType: "html",
            url: AppBundle.Config.routing.set_match_bet_score,
            method: "POST",
            data: postData,
            success: function(content) {                                
                $(container).html(content);
                AppBundle.Util.loading('hide');
                modalWindow.modal('show');                
            }
    });        
  }, 
  showUsersBetsWindow: function(params) {
    match = params.match;
    
    if(match == undefined)
      return false;
      
    tournament = params.tournament;
    
    if(tournament == undefined)
      return false;
      
    var modalWindow = $('#appbundle_bets_user_bets');
    var container = $('#appbundle_bets_user_bets_container');
    
    if(modalWindow.length == 0 || container.length == 0)
      return false;
      
    AppBundle.Util.loading('show', 'Получение данных о ставках на матч...');    
    
    var postData = { tournament_id: tournament, match_id: match };
    
    $.ajax({
            dataType: "html",
            url: AppBundle.Config.routing.users_bets_data,
            method: "POST",
            data: postData,
            success: function(content) {                                
                $(container).html(content);
                AppBundle.Util.loading('hide');
                modalWindow.modal('show');                
            }
    });
  }
};

AppBundle.Teams = {
    showLastMatchesWindow: function(params) {
    leftTeam = params.leftTeam;
    
    if(leftTeam == undefined)
      return false;
    
    rightTeam = params.rightTeam;
    
    if(rightTeam == undefined)
      return false;    
      
    var modalWindow = $('#appbundle_teams_last_matches');
    var container = $('#appbundle_teams_last_matches_container');
    
    if(modalWindow.length == 0 || container.length == 0)
      return false;
      
    AppBundle.Util.loading('show', 'Получение последних матчей команд...');    
    
    var postData = { leftTeam: leftTeam, rightTeam: rightTeam };
    
    $.ajax({
            dataType: "html",
            url: AppBundle.Config.routing.last_matches_data,
            method: "POST",
            data: postData,
            success: function(content) {                                
                $(container).html(content);
                AppBundle.Util.loading('hide');
                modalWindow.modal('show');                
            },
            error: function(jqXHR, textStatus, errorThrown)
            {                
                AppBundle.Util.loading('hide');                
            }
    });
  }
};