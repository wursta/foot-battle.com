var App = {
    nggInstance: null,    
    elementsIds : {
        siteContainer: '#siteContainer'
    },    
    elements : {},
    
    init: function(){
        var self = this;
        
        self.onAjaxStartInit();
        //self.createNggInstance();        
    },
    
    createNggInstance: function()
    {
      self.nggInstance = angular.module('FootBattleApp', []);
    },
    
    getNggInstance: function()
    {
      var self = this;
      
      if(self.nggInstance == null)
        self.createNggInstance();
      
      return self.nggInstance;
    },
    
    getContainer : function(name) {
        var self = App;
        
        if(!self.elements[name])
            self.elements[name] = $(self.elementsIds[name]);
        
        return self.elements[name];
    },       
    
    onAjaxStartInit: function() {        
        $(document).ajaxStart(function(){
            Pace.restart();
        });
    },
    
    showError: function(errorMsg) {
      var self = App;
      
      alert(errorMsg);
    },
    
    getDateRangeSettings: function() {        
        return {
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "timePickerIncrement": 10,
            /*"ranges": {
                "Сегодня": Ranges.Today(),
                "Вчера": [
                    "2016-03-30T17:48:30.775Z",
                    "2016-03-30T17:48:30.775Z"
                ],
                "Последние 7 дней": [
                    "2016-03-25T17:48:30.775Z",
                    "2016-03-31T17:48:30.775Z"
                ],
                "Последние 30 дней": [
                    "2016-03-02T17:48:30.775Z",
                    "2016-03-31T17:48:30.775Z"
                ],
                "Этот месяц": [
                    "2016-02-29T21:00:00.000Z",
                    "2016-03-31T20:59:59.999Z"
                ],
                "Прошлый месяц": [
                    "2016-01-31T21:00:00.000Z",
                    "2016-02-29T20:59:59.999Z"
                ]
            },*/
            "locale": {
                "format": "DD.MM.YYYY",
                "separator": " - ",
                "applyLabel": "Применить",
                "cancelLabel": "Отмена",
                "fromLabel": "С",
                "toLabel": "По",
                "customRangeLabel": "Выбрать",
                "daysOfWeek": [
                    "Вс",
                    "Пн",
                    "Вт",
                    "Ср",
                    "Чт",
                    "Пт",
                    "Сб"
                ],
                "monthNames": [
                    "Январь",
                    "Февраль",
                    "Март",
                    "Апрель",
                    "Май",
                    "Июнь",
                    "Июль",
                    "Август",
                    "Сентябрь",
                    "Октябрь",
                    "Ноябрь",
                    "Декабрь"
                ],
                "firstDay": 1
            }            
        }
    }
}

App.init();