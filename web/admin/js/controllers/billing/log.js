BillingLog.getContainer('container').find('#daterange').daterangepicker(App.getDateRangeSettings());

BillingLog.getContainer('container').find('#daterange').on('apply.daterangepicker', function(start, end, label){
    console.log(start, end, label);
});

BillingLog.getContainer('container').on('click', '.pagination a', function(e){
    e.preventDefault();
    
    var aElem = this;
    var url = $(aElem).attr("href");
    var filterData = BillingLog.getContainer('container').find('.filter_form').serialize();    
    BillingLog.load(url, filterData);
});