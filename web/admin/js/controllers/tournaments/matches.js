var matchesCollectionUrl = TournamentMatches.getContainer('container').data('matchesurl');
var addMatchUrl = TournamentMatches.getContainer('container').data('addmatchurl');
var deleteMatchUrl = TournamentMatches.getContainer('container').data('deletematchurl');

TournamentMatches.getContainer('container').on('click', '.matchFindBtn', function(e){
  e.preventDefault();
  
  var btn = $(this);  
  
  if(btn.data("loading") == true)
        return false;
  
  var tournamentId = btn.data("tournamentid");
  var matchRow = btn.parents('.matchRow');
  var matchText = matchRow.find('.matchText');  
  
  btn.data("loading", true);
  MatchModal.open({
      target: btn,
      url: matchesCollectionUrl,
      params: { tournament_id:  tournamentId },
      onOpen: function() {        
        btn.data("loading", false);
      },
      onMatchSelect: function(selectedMatch) {
      
        TournamentMatches.add({
          url: addMatchUrl,
          tournamentId: tournamentId,
          matchId: selectedMatch.id, 
          roundNum: btn.data("roundnum"),
          qualificationMatch: btn.data("qualificationmatch"),
          groupMatch: btn.data("groupmatch"),
          success: function(data){
            
            $('#matchesModal').modal('hide');
            
            if(data.success)
            {
              var text = '(' + selectedMatch.date + ' ' + selectedMatch.time + ') ' + selectedMatch.homeTeam + ' &mdash; ' + selectedMatch.guestTeam;
              matchText.html(text);
              
              btn.removeClass('btn-info')
                 .addClass('btn-danger')
                 .removeClass('matchFindBtn')                 
                 .addClass('matchDeleteBtn')
                 .data('matchid', selectedMatch.id)
                 .html('<i class="glyphicon glyphicon-trash"></i> Удалить');                            
            }
            else
            {
              App.showError(data.msg);
            }
          }
        });
      
      }
  });    
});

TournamentMatches.getContainer('container').on('click', '.matchDeleteBtn', function(e){
  e.preventDefault();
  
  if(!confirm('Вы уверены, что хотите удалить данный матч из турнира? В случае, если на данный матч уже были сделаны прогнозы удаление не будет выполнено.'))
    return;
  
  var btn = $(this);  
  
  if(btn.data("loading") == true)
        return false;
  
  var tournamentId = btn.data("tournamentid");  
  var matchId = btn.data('matchid');
  var matchText = btn.parents('.matchRow').find('.matchText');
  
  btn.data("loading", true);
  
  TournamentMatches.remove({
    url: deleteMatchUrl,
    tournamentId: tournamentId,
    matchId: matchId,
    success: function(data) {
      btn.data("loading", false);
      
      if(data.success)
      {
        matchText.html('Матч не определён');
        
        btn.removeClass('btn-danger')
           .addClass('btn-info')
           .removeClass('matchDeleteBtn')                 
           .addClass('matchFindBtn')
           .data('matchid', false)
           .html('<i class="glyphicon glyphicon-search"></i> Найти');
      }
      else
      {
        App.showError(data.msg);
      }
    }
  });
  
});

App.getContainer('siteContainer').on('click', '#matchesModal form[name="match_search"] button[type="submit"]', function(e){
  e.preventDefault();
  
  var btn = $(this);
  var form = $(this).parents("form");
  
  MatchModal.search({
    url: matchesCollectionUrl,
    params: form.serialize()
  })
    
});