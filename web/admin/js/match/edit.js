var MatchEditForm = {
    elementsIds : {
        editFormModal: '#matchEditFormModal',        
        editForm: '#matchEditForm',        
        matchIdInput: '#matchIdInput',
        content: '#matchEditFormModalContent',
    },    
    elements : {},
    getContainer : function(name) {
        var self = MatchEditForm;
        
        if(!self.elements[name])
            self.elements[name] = $(self.elementsIds[name]);
        
        return self.elements[name];
    },
    
    load: function(actionUrl)
    {
        var self = MatchEditForm;
        
        App.Utils.Modal.showLoading(self.getContainer('editFormModal'));
        
        self.getContainer('editForm').attr('action', actionUrl);
        
        $.ajax({
            url: actionUrl,
            dataType: "html",
            success: function(data, textStatus, jqXHR) {                
                self.getContainer('content').html(data);                
            },
            error: function(jqXHR, textStatus, errorThrown) {                
                App.Utils.Modal.showError(self.getContainer('editFormModal'), errorThrown);                
            }
        });
    },
    
    submit: function() 
    {
        var self = MatchEditForm;
        
        var formData = MatchEditForm.getContainer('editForm').serialize();
        var actionUrl = MatchEditForm.getContainer('editForm').attr("action");
        
        $.post(actionUrl, formData).done(function(data) {
            if(data.success)
            {
                location.reload(true);
            }
        });
        
    }
}

MatchEditForm.getContainer('editFormModal').on('show.bs.modal', function(e){
  var button = $(e.relatedTarget)  
  var modal = $(this);
      
  var actionUrl = button.data("url");
  
  MatchEditForm.load(actionUrl);  
});

MatchEditForm.getContainer('editForm').on('submit', function(e){
    e.preventDefault();
    
    MatchEditForm.submit();
});

