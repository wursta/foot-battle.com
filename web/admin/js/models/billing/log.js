var BillingLog = {
    elementsIds : {
        container: '#billingLogContainer'
    },    
    elements : {},
    getContainer : function(name) {
        var self = BillingLog;
        
        if(!self.elements[name])
            self.elements[name] = $(self.elementsIds[name]);
        
        return self.elements[name];
    },
    
    load: function(url, filterData) {
        var self = BillingLog;
                
        $.ajax({
            url: url,
            dataType: "html",
            method: "POST",
            data: filterData,
            success: function(data, textStatus, jqXHR) {
                self.getContainer('container').html(data);                
            }
        });
    }
}