var MatchModal = {
    elementsIds : {
        container: '#matchesModal'
    },    
    elements : {},
    getContainer : function(name) {
        var self = MatchModal;
        
        if(!self.elements[name])
            self.elements[name] = $(self.elementsIds[name]);
        
        return self.elements[name];
    },
    
    open: function(config) {
      var self = MatchModal;
      
      var params = {};
      var onMatchSelect, onOpen;
      
      if(config.params != undefined)
        params = config.params
        
      if(config.onMatchSelect != undefined)
        onMatchSelect = config.onMatchSelect;
      
      if(config.onOpen != undefined)
        onOpen = config.onOpen;
      
      self.load({
        url: config.url,
        params: params,
        success: function(data){
          self.show({
            content: data,
            onOpen: onOpen,
            onMatchSelect: onMatchSelect
          });
        }
      });      
    },
    
    load: function(config) {
      var self = MatchModal;
      
      var url, params, successCallback;
      
      if(config.url != undefined)
            url = config.url;
      
      if(config.params != undefined)
        params = config.params
      
      if(config.success != undefined)
            successCallback = config.success;
      
      $.ajax({
            url: url,
            data: params,
            method: "POST",
            dataType: "html",
            success: successCallback
      });            
    },
    
    show: function(config) {
      var self = MatchModal;
      
      var content;
      
      if(config.content != undefined)
        content = config.content;
            
      var modal = App.Utils.Modal.create('matchesModal', {
          title: 'Поиск матчей',
          body: content,
          large: true,
          buttons: [
              {
                  text: 'Закрыть'
              }
          ]
      });
      
      $('#matchesModal').on('hidden.bs.modal', function(e){
          self.close('#matchesModal');
      });
      
      if(config.onOpen != undefined)
      {
        $('#matchesModal').on('show.bs.modal', function(e){
            config.onOpen();
        });
      }
      
      if(config.onMatchSelect != undefined)
      {
        $('#matchesModal').on('click', '.matchSelectBtn', function(e){
            e.preventDefault();
            
            var btn = $(this);
            
            var selectedMatch = {
              id: btn.data('matchid'),
              homeTeam: btn.data('hometeam'),
              guestTeam: btn.data('guestteam'),
              date: btn.data('date'),
              time: btn.data('time')
            }
            
            config.onMatchSelect(selectedMatch);            
        });
      }
      
      modal.modal('show');
    },
    
    close: function(btnId) {
        var self = MatchModal;
                
        $(btnId).off('click', '.matchSelectBtn');
                        
        $(btnId).remove();
    },
    
    search: function(config) {
      var self = MatchModal;
      
      var url, params;
      
      if(config.url != undefined)
            url = config.url;
      
      if(config.params != undefined)
        params = config.params
      
      var modalBody = $('#matchesModal').find('.modal-body');
      
      modalBody.text('Поиск матчей...');
      
      $.ajax({
            url: url,
            data: params,
            method: "POST",
            dataType: "html",
            success: function(data) {              
              modalBody.html(data);
            }
      });
    }
}