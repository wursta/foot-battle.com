var TournamentMatches = {
    elementsIds : {
        container: '#matchesContainer'
    },    
    elements : {},
    getContainer : function(name) {
        var self = TournamentMatches;
        
        if(!self.elements[name])
            self.elements[name] = $(self.elementsIds[name]);
        
        return self.elements[name];
    },
    
    add: function(config) {
      var self = TournamentMatches;
      
      var url, tournamentId, matchId, roundNum, qualificationMatch, groupMatch, success;
      
      if(config.url != undefined)
        url = config.url;
        
      if(config.tournamentId != undefined)
        tournamentId = config.tournamentId;
        
      if(config.matchId != undefined)
        matchId = config.matchId;
      
      if(config.roundNum != undefined)
        roundNum = config.roundNum;
        
      if(config.qualificationMatch != undefined)
        qualificationMatch = config.qualificationMatch;
        
      if(config.groupMatch != undefined)
        groupMatch = config.groupMatch;
        
      if(config.success != undefined)
        success = config.success;            
      
      $.ajax({
            url: url,
            data: { tournament_id: tournamentId, match_id: matchId, round_num: roundNum, qualification_match: qualificationMatch, group_match: groupMatch },
            method: "POST",
            dataType: "json",
            success: success
      });
    },
    
    remove: function(config) {
      
      var url, tournamentId, matchId, success;
      
      if(config.url != undefined)
        url = config.url;
      
      if(config.matchId != undefined)
        matchId = config.matchId;
        
      if(config.tournamentId != undefined)
        tournamentId = config.tournamentId;
      
      if(config.success != undefined)
        success = config.success;
      
      $.ajax({
            url: url,
            data: { tournament_id: tournamentId, match_id: matchId },
            method: "POST",
            dataType: "json",
            success: success
      });
    }
}