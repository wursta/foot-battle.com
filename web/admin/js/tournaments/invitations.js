var TournamentsUsersInvitations = {
    elementsIds : {
        searchForm: '#searchForm',
        searchResults: '#searchResults'
    },    
    elements : {},
    getContainer : function(name) {
        var self = TournamentsUsersInvitations;

        if(!self.elements[name])
            self.elements[name] = $(self.elementsIds[name]);
        
        return self.elements[name];
    },
    
    search: function(search_url, query) {
        var self = TournamentsUsersInvitations;
        
        $.ajax({
            url: search_url,
            method: "POST",
            dataType: "html",
            data: { query: query },
            success: function(data, textStatus, jqXHR) {
                self.getContainer('searchResults').html(data);                
            }            
        });
    }
}

TournamentsUsersInvitations.getContainer('searchForm').on('submit', function(e){
    e.preventDefault();
    
    var search_url = $(this).attr('action');
    var query = $(this).find('input[name="query"]').val();
    TournamentsUsersInvitations.search(search_url, query);    
})

