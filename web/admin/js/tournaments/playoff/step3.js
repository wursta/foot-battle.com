$('.matchFindButton').on('appbundle.matches.singleSelect.select', function(event, match){        
    var findButton = $(this);    
    var textSpan = $(this).parents('.matchRow').find('.matchText');
        
    var round = findButton.data('round');    
    var qualificationMatch = findButton.data('qualificationmatch');
    var matchToChange = findButton.data('match');
    
    var postData = {
        match_id: match.id,
        tournament_id : tournamentId,
        round: round,
        qualification_match: qualificationMatch
    }        
        
    var loadingText = 'Добавление матча в турнир...';
    if(matchToChange != undefined)
    {
        postData.match_to_change = matchToChange;
        loadingText = 'Замена матча в турнире...';
    }    
    
    AppBundle.Util.loading('show', loadingText);
    
    $.ajax({
            dataType: "JSON",
            url: addMatchURI,
            method: "POST",
            data: postData,
            success: function(data) {
                if(data.success == false)
                {
                    AppBundle.Util.loading('hide');
                    AppBundle.Util.alertBox('Ошибка', data.msg);
                    return false;
                }
                
                if(!match.match_time)
                    match.match_time = '[время не определено]';
                
                var text = '('+match.match_date+' '+match.match_time+') '+match.home_team+' - '+match.guest_team;                
                            
                textSpan.text(text);
                                
                if(!tournamentIsReady)
                {
                  findButton.data('match', match.id);
                  findButton.text('Изменить');
                }
                else
                {
                  findButton.remove();
                }
                                                
                AppBundle.Util.loading('hide');
            }
    });    
});