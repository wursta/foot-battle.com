var TransactionLog = {
    loadPage : function() {
        console.log(1);
    }
}

$('#transactionLogPagination li > a').click(function(e){
    e.preventDefault();
    
    var liElement = $(this).parent();
    
    $('#transactionLogLoading').removeClass('hidden');
    
    $.ajax({
        url: $(this).attr("href"),
        dataType: "html",                
        dataFilter: function(data, type) {
            console.log(data);
            return $(data).filter('#transactionLogTable');
        },
        success: function(data, textStatus, jqXHR) {                        
            $('#transactionLogTable').html($(data).html());
            liElement.addClass("active").siblings().removeClass("active");
            $('#transactionLogLoading').addClass('hidden');
        }
    });    
})