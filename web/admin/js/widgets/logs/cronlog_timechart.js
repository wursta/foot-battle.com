var CronLogTimeChart = {    
    elementsIds : {
        chart: '#cronTimeChart',        
    },    
    elements : {},
    getContainer : function(name) {
        var self = CronLogTimeChart;
        
        if(!self.elements[name])
            self.elements[name] = $(self.elementsIds[name]);
        
        return self.elements[name];
    },            
    
    show : function() {
        var self = CronLogTimeChart;
                        
        self.getContainer('chart').highcharts({
            title: false,
            chart: {
                type: 'column'
            },            
            xAxis: {
                type: 'datetime'
            },
            yAxis: {
                title: {
                    text: 'Время выполнения, сек.'
                }
            },            
            tooltip: {
                valueSuffix: 'сек.'
            },
            series: CronLogTimechartSeries
        });
    }
}