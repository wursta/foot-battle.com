$("#addRuleButton").click(function(e){
    e.preventDefault();

    var rulesFieldsList = $('#rules-fields-list');    

    var groupFieldWidget = groupFieldPrototype.replace(/__name__/g, fieldsCounter);
    var placeFieldWidget = placeFieldPrototype.replace(/__name__/g, fieldsCounter);
    var percentFieldWidget = percentFieldPrototype.replace(/__name__/g, fieldsCounter);
    fieldsCounter++;
    
    var newPlaceFieldTd = $('<td></td>').html(groupFieldWidget+placeFieldWidget);
    var newPercentFieldTd = $('<td></td>').html(percentFieldWidget);
    var removeButton = $('<td class="text-center" style="vertical-align:middle;"><span style="cursor:pointer;" class="remove-rule fa fa-trash text-danger"></span></td>');

    var newTr = $('<tr></tr>').append(newPlaceFieldTd).append(newPercentFieldTd).append(removeButton);
    newTr.appendTo(rulesFieldsList);

    $('.remove-rule').click(function(){
        $(this).parents('tr').remove();
    });
});

$('.remove-rule').click(function(){
    $(this).parents('tr').remove();
});