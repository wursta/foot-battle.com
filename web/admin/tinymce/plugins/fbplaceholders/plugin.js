tinymce.PluginManager.add('fbplaceholders', function(editor, url) {
    editor.addButton('select_fbplaceholder', {
        type: 'menubutton',
        text: '{}',
        icon: false,
        menu: [{
            text: 'Политики',
            menu: [{
                text: 'Схема подсчёта очков',
                onclick: function() {
                  editor.insertContent('{{ POINTS_SCHEME|default()raw| }}');
                }
            },
            {
                text: 'Распредления призовых',
                menu: [{
                    text: 'Чемпионат',
                    onclick: function() {
                      editor.insertContent('{{ CHAMPIONSHIP_WIN_DISTRIBUTION|default()|raw}}');
                    }
                },
                {
                    text: 'Круговой',
                    onclick: function() {
                      editor.insertContent('{{ ROUND_WIN_DISTRIBUTION|default()|raw}}');
                    }
                },
                {
                    text: 'Плей-офф',
                    onclick: function() {
                      editor.insertContent('{{ PLAYOFF_WIN_DISTRIBUTION|default()|raw}}');
                    }
                },
                {
                    text: 'Лига чемпионов',
                    onclick: function() {
                      editor.insertContent('{{ CHAMPIONSLEAGUE_WIN_DISTRIBUTION|default()|raw}}');
                    }
                }]
            },
            {
                text: 'Регламенты турниров',
                menu: [{
                    text: 'Чемпионат',
                    onclick: function() {
                      editor.insertContent('{{ TOURNAMENT_CHAMPIONSHIP_REGULATION|default()|raw}}');
                    }
                },
                {
                    text: 'Круговой',
                    onclick: function() {
                      editor.insertContent('{{ TOURNAMENT_ROUND_REGULATION|default()|raw }}');
                    }
                },
                {
                    text: 'Плей-офф',
                    onclick: function() {
                      editor.insertContent('{{ TOURNAMENT_PLAYOFF_REGULATION|default()|raw }}');
                    }
                },
                {
                    text: 'Лига чемпионов',
                    onclick: function() {
                      editor.insertContent('{{ TOURNAMENT_CHAMPIONSLEAGUE_REGULATION|default()|raw }}');
                    }
                }]
            }]
        }]
    });

});